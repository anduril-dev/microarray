import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.nfunk.jep.JEP;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class SetTransformer extends SkeletonComponent {
    public static final int MAX_INPUTS = 5;
    
    public static class IterationResult {
        final List<Set<String>> sets;
        final List<String> targetNames;
        final List<String> wildcards;
        
        public IterationResult() {
            this.sets = new ArrayList<Set<String>>();
            this.targetNames = new ArrayList<String>();
            this.wildcards = new ArrayList<String>();
        }
        
        public void addSet(Set<String> set, String targetName, String wildcard) {
            this.sets.add(set);
            this.targetNames.add(targetName);
            this.wildcards.add(wildcard);
        }
    }
    
    /** Collection of sets, as set name -> set contents */
    private Map<String, Set<String>> sets;
    
    /** Set annotations, as set name -> (name -> value). */ 
    private Map<String, Map<String, String>> annotationMap;
    
    /** List of defined annotation columns. These correspond to
     * the "name" keys in annotationMap. */
    private Set<String> annotationColumns;
    
    public SetTransformer() {
        this.sets = new TreeMap<String, Set<String>>();
        this.annotationMap = new HashMap<String, Map<String,String>>();
        this.annotationColumns = new LinkedHashSet<String>();
    }
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        final boolean includeOriginal = cf.getBooleanParameter("includeOriginal");
        
        readSets(cf, includeOriginal);
        JEP jep = makeJEP(sets);
        
        List<String> newSets = new ArrayList<String>();
        CSVParser transParser = new CSVParser(cf.getInput("transformation"));
        try {
            final int targetIndex = transParser.getColumnIndex("Target");
            final int definitionIndex = transParser.getColumnIndex("Definition");
            final int iterationIndex = transParser.getColumnIndex("IterationSet", false);
            
            for (int i=0; i<transParser.getColumnCount(); i++) {
                if (i == targetIndex || i == definitionIndex || i == iterationIndex) continue;
                this.annotationColumns.add(transParser.getColumnNames()[i]);
            }
            
            for (String[] row: transParser) {
                final String target = row[targetIndex];
                final String def = row[definitionIndex];
                
                Map<String, String> curAnnotation = new HashMap<String, String>();
                for (int i=0; i<row.length; i++) {
                    if (i == targetIndex || i == definitionIndex) continue;
                    curAnnotation.put(transParser.getColumnNames()[i], row[i]);
                }
                
                if (target.contains("*")) {
                    final String iterationSet = iterationIndex < 0 ? null : row[iterationIndex];
                    IterationResult result = evaluateIteration(cf, jep, def, sets, target, iterationSet);
                    if (result == null) return ErrorCode.INVALID_INPUT;
                    for (int i=0; i<result.sets.size(); i++) {
                        final String curTarget = result.targetNames.get(i);
                        final Set<String> curResult = result.sets.get(i);
                        final String curWildcard = result.wildcards.get(i);
                        this.sets.put(curTarget, curResult);
                        jep.addVariable(curTarget, curResult);
                        newSets.add(curTarget);
                        
                        Map<String, String> iterAnnotation = new HashMap<String, String>();
                        for (Map.Entry<String, String> entry: curAnnotation.entrySet()) {
                            String value = entry.getValue();
                            if (value != null) value = value.replace("*", curWildcard);
                            iterAnnotation.put(entry.getKey(), value);
                        }
                        this.annotationMap.put(curTarget, iterAnnotation);
                    }
                } else {
                    Functions.wildcard = null;
                    Set<String> result = evaluateExpression(cf, jep, def, sets, true);
                    if (result == null) return ErrorCode.INVALID_INPUT;
                    this.sets.put(target, result);
                    jep.addVariable(target, result);
                    newSets.add(target);
                    this.annotationMap.put(target, curAnnotation);
                }
            }
        } finally {
            transParser.close();
        }
        
        final int fixedColumns = 2;
        final int numColumns = fixedColumns + this.annotationColumns.size();
        String[] outColumns = new String[numColumns];
        outColumns[0] = "ID";
        outColumns[1] = "Members";
        int pos = 2;
        for (String annColumn: this.annotationColumns) {
            outColumns[pos] = annColumn;
            pos++;
        }
        CSVWriter writer = new CSVWriter(outColumns, cf.getOutput("result"));
        
        try {
            Collection<String> resultSets = includeOriginal ? sets.keySet() : newSets;
            for (String setID: resultSets) {
                writer.write(setID);
                writer.write(formatSet(sets.get(setID)));
                
                Map<String, String> curAnnotation = this.annotationMap.get(setID);
                for (String annColumn: this.annotationColumns) {
                    if (curAnnotation == null) writer.write(null);
                    else writer.write(curAnnotation.get(annColumn));
                }
            }
        } finally {
            writer.close();
        }
        
        return ErrorCode.OK;
    }
    
    public static String formatSet(Set<String> set) {
        StringBuffer sb = new StringBuffer();
        for (String value: set) {
            if (sb.length() > 0) sb.append(',');
            sb.append(value);
        }
        return sb.toString();
    }
    
    private void readSets(CommandFile cf, boolean populateAnnotation) throws IOException {
        for (int input=1; input<=MAX_INPUTS; input++) {
            final String inputName = "set"+input;
            if (!cf.inputDefined(inputName)) continue;

            CSVParser parser = new CSVParser(cf.getInput(inputName));
            final int idIndex = parser.getColumnIndex("ID");
            final int membersIndex = parser.getColumnIndex("Members");
            try {
                for (String[] row: parser) {
                    final String setID = row[idIndex];
                    final String membersStr = row[membersIndex];
                    if (setID == null) continue;
                    Set<String> set = new TreeSet<String>();
                    if (membersStr != null) {
                        for (String id: membersStr.split(",")) {
                            id = id.trim();
                            if (id.isEmpty()) continue;
                            set.add(id);
                        }
                    }
                    this.sets.put(setID, set);
                    
                    if (populateAnnotation) {
                        for (int i=0; i<row.length; i++) {
                            if (i == idIndex || i == membersIndex) continue;
                            String colName = parser.getColumnNames()[i];
                            this.annotationColumns.add(colName);
                            Map<String, String> annMap = this.annotationMap.get(setID);
                            if (annMap == null) {
                                annMap = new HashMap<String, String>();
                                this.annotationMap.put(setID, annMap);
                            }
                            annMap.put(colName, row[i]);
                        }
                    }
                }
            } finally {
                parser.close();
            }
        }
    }
    
    private JEP makeJEP(Map<String, Set<String>> sets) {
        JEP jep = new JEP();
        jep.setAllowUndeclared(false);
        for (Map.Entry<String, Set<String>> entry: sets.entrySet()) {
            jep.addVariable(entry.getKey(), entry.getValue());
        }
        Functions.addFunctions(jep, sets);
        return jep;
    }
    
    private IterationResult evaluateIteration(CommandFile cf, JEP jep, String expression,
            Map<String, Set<String>> sets, String target, String iterationSet) {
        expression = expression.replace('\'', '"');
        IterationResult result = new IterationResult();
        
        final Set<String> iteration;
        if (iterationSet == null) {
            iteration = sets.keySet();
        } else {
            iteration = evaluateExpression(cf, jep, iterationSet, sets, true);
            if (iteration == null) return null;
        }
        
        for (String name: iteration) {
            Functions.wildcard = name;
            Set<String> iterResult = evaluateExpression(cf, jep, expression, sets, false);
            if (iterResult != null) {
                result.addSet(iterResult, target.replaceAll("[*]", name), name);
            }
        }
        Functions.wildcard = null;
        
        if (result.sets.isEmpty()) {
            String msg = String.format(
                    "Invalid iterated expression %s (target %s): no results were produced",
                    expression, target);
            cf.writeError(msg);
            return null;
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private Set<String> evaluateExpression(CommandFile cf, JEP jep, String expression, Map<String, Set<String>> sets, boolean singleMode) {
        if (singleMode) {
            expression = expression.replace('\'', '"');
        }
        
        jep.parseExpression(expression);
        if (jep.hasError()) {
            String msg = String.format("Invalid expression %s: %s", expression, jep.getErrorInfo());
            cf.writeError(msg);
            return null;
        }
        
        Object result = jep.getValueAsObject();
        if (result == null || jep.hasError()) {
            if (singleMode) {
            String msg = String.format("Error evaluating expression %s: %s", expression, jep.getErrorInfo());
            cf.writeError(msg);
            }
            return null;
        } else {
            if (result instanceof Set) {
                return (Set<String>)result;
            } else if (result instanceof String) {
                String name = Functions.replaceWildcard((String)result);
                Set<String> set = sets.get(name);
                if (set == null && singleMode) {
                    String msg = String.format("Expression %s: set not found: %s", expression, name);
                    cf.writeError(msg);
                }
                return set;
            } else {
                String msg = String.format("Expression %s returned an invalid value: %s", expression, result);
                cf.writeError(msg);
                return null;
            }
        }
    }
    
    public static void main(String[] args) {
        new SetTransformer().run(args);
    }
}
