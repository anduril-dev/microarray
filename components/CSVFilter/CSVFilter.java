import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

public class CSVFilter extends SkeletonComponent {
    private String[] inColumns;
    private int idColumnIndex;
    private double nonMissing;
    private final Map<String, String> regexp;
    private final Map<String, Double> lowBound;
    private final Map<String, Double> highBound;
    private final Set<String> auxiliary;
    private boolean auxiliaryDefined;

    public CSVFilter() {
        this.regexp = new HashMap<String, String>();
        this.lowBound = new HashMap<String, Double>();
        this.highBound = new HashMap<String, Double>();
        this.auxiliary = new HashSet<String>();
    }
    
    @Override
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        final boolean negate = cf.getBooleanParameter("negate");

        CSVParser parser = new CSVParser(cf.getInput("csv"));
        inColumns = getInColumns(cf, parser, negate);
        if (inColumns.length == 0) {
            cf.writeError("No columns selected for output - at least one must be present");
            return ErrorCode.PARAMETER_ERROR;
        }

        this.auxiliaryDefined = cf.inputDefined("auxiliary");
        
        String[] outColumns = Arrays.copyOf(inColumns, inColumns.length);
        for (String rename: AsserUtil.split(cf.getParameter("rename"))) {
            if (rename.isEmpty()) continue;
            String[] tokens = rename.split("=");
            if (tokens.length != 2) {
                cf.writeError("Invalid rename parameter: "+rename);
                return ErrorCode.PARAMETER_ERROR;
            }

            final String oldName = tokens[0];
            final String newName = tokens[1];

            boolean found = false;
            for (int i=0; i<inColumns.length; i++) {
                if (inColumns[i].equals(oldName)) {
                    outColumns[i] = newName;
                    found = true;
                    break;
                }
            }
            if (!found) {
                cf.writeError("Can not rename column: column not found: "+oldName);
                return ErrorCode.PARAMETER_ERROR;
            }
        }
        
        try {
            initializeRowCriteria(cf, parser);
        } catch (Exception e){
            cf.writeError(e.getMessage());
            return ErrorCode.PARAMETER_ERROR;
        }

        CSVWriter writer = new CSVWriter(outColumns, cf.getOutput("csv"));
        int[]     colMap = AsserUtil.indicesOf(inColumns, parser.getColumnNames(), true, true);
        for (String[] line: parser) {
            boolean include = includeRow(line, parser);
            if (negate && (this.auxiliaryDefined || !this.regexp.isEmpty()
                    || !this.lowBound.isEmpty() || !this.highBound.isEmpty())) {
                include = !include;
            }
            if (!include) continue;
            
            for (int index: colMap) {
                writer.write(line[index], parser.hasQuotations(index));
            }
        }
        parser.close();
        writer.close();

        return ErrorCode.OK;
    }

    private String[] getInColumns(CommandFile cf, CSVParser parser, boolean negate) throws IOException {
        final String COL_LIST_KEY = "includeColumns";
        boolean COL_ORDER = Boolean.parseBoolean(cf.getParameter("colOrder"));
        String[] columns;
        columns = AsserUtil.split(cf.getParameter(COL_LIST_KEY));
        if (columns.length == 1) {
            if (columns[0].equals("*")) {
               columns = parser.getColumnNames();
            } else
            if (columns[0].isEmpty()) {
               columns = AsserUtil.EMPTY_ARRAY_STRING;
            }
        }

        if (cf.inputDefined(COL_LIST_KEY)) {
           List<String> cols = new ArrayList<String>(1000);
           CSVParser input = new CSVParser(cf.getInput(COL_LIST_KEY));
           if (COL_ORDER){
               for (String col: columns){
                   cols.add(col);
               }
               for (String[] line: input) {
                   if (!cols.contains(line[0])) cols.add(line[0]);
               }
               input.close();
           } else {
               for (String[] line: input) {
                   cols.add(line[0]);
               }
               input.close();
               for (String col: columns) {
                   if (!cols.contains(col)) cols.add(col);
               }
           }
           columns = cols.toArray(new String[cols.size()]);
        }
        
        if (negate) {
            Set<String> exclude = new HashSet<String>(columns.length * 2);
            for (String col: columns) exclude.add(col);
            
            List<String> include = new ArrayList<String>(parser.getColumnCount());
            for (String col: parser.getColumnNames()) {
                if (!exclude.contains(col)) include.add(col);
            }
            columns = include.toArray(new String[include.size()]);
        }
        
        return columns;
    }
    
    private void initializeRowCriteria(CommandFile cf, CSVParser parser) throws IOException {
        final String idColumn = cf.getParameter("idColumn");
        this.idColumnIndex = idColumn.isEmpty() ?
                0 : parser.getColumnIndex(idColumn);
        
        this.nonMissing = cf.getDoubleParameter("nonMissing");

        try {
            populateMap(cf.getParameter("regexp"), this.regexp, "regexp", false);
            populateMap(cf.getParameter("lowBound"), this.lowBound, "lowBound", true);
            populateMap(cf.getParameter("highBound"), this.highBound, "highBound", true);
        } catch (IllegalArgumentException e){
            throw e;
        }
        
        if (this.auxiliaryDefined) {
            CSVParser auxParser = new CSVParser(cf.getInput("auxiliary"));
            String auxColumn = cf.getParameter("matchColumn");
            final int auxColumnIndex = auxColumn.isEmpty() ?
                    0 : auxParser.getColumnIndex(auxColumn);
            for (String[] line: auxParser) {
                this.auxiliary.add(line[auxColumnIndex]);
            }
            auxParser.close();
        }
    }
    
    @SuppressWarnings("unchecked")
    private <T> void populateMap(String paramValue, Map<String,T> map, String label, boolean convInt) {
        for (String entry: paramValue.split("(?<!\\\\),")) { // Matches ',' not preceded by '\'.
            if (entry.isEmpty()) continue;
            String[] tokens = entry.split("=");
            if (tokens.length != 2) {
                String msg = String.format("Invalid %s entry: %s", label, entry);
                throw new IllegalArgumentException(msg);
            }
            
            final String column = tokens[0].replaceAll("\\\\,", ",");
            if (convInt) {
                Double ival = Double.parseDouble(tokens[1]);
                map.put(column, (T)ival);
            } else {
                map.put(column, (T)tokens[1].replaceAll("\\\\,", ","));
            }
        }
    }
    
    private boolean includeRow(String[] line, CSVParser parser) {
        for (Map.Entry<String, String> entry: regexp.entrySet()) {
            final int index = parser.getColumnIndex(entry.getKey());
            String value = line[index];
            if(value == null) return false;
            if (!value.matches(entry.getValue())) return false;
        }

        for (Map.Entry<String, Double> entry: lowBound.entrySet()) {
            final int index = parser.getColumnIndex(entry.getKey());
            if (line[index] == null) return false;
            double value = Double.parseDouble(line[index]);
            if (value < entry.getValue()) return false;
        }

        for (Map.Entry<String, Double> entry: highBound.entrySet()) {
            final int index = parser.getColumnIndex(entry.getKey());
            if (line[index] == null) return false;
            double value = Double.parseDouble(line[index]);
            if (value > entry.getValue()) return false;
        }
        
        if (this.auxiliaryDefined) {
            final String id = line[idColumnIndex];
            if (!auxiliary.contains(id)) return false;
        }
        
        if (nonMissing > 0) {
            int present = 0;
            for (String column: inColumns) {
                String value = line[parser.getColumnIndex(column)];
                if (value != null) present++;
            }
            if (nonMissing < 1){
                if ((double)present/(double)inColumns.length <= nonMissing) return false;
            } else {
                if (present < nonMissing) return false;
            }
        }
        
        return true;
    }
    
    public static void main(String[] args) {
        new CSVFilter().run(args);
    }
    
}
