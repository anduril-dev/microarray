#/usr/bin/env python
import sys
import anduril
from anduril.args import *
import shutil,os
import math

def copyfileorfolder(basename,source,target,link):
    ''' Copies a file or folder structure under target folder '''
    if link:
        os.symlink(os.path.join(source,basename),os.path.join(target,basename))
        return
    if os.path.isfile(os.path.join(source,basename)):
        shutil.copyfile(os.path.join(source,basename),os.path.join(target,basename))
        return
    if os.path.isdir(os.path.join(source,basename)):
        shutil.copytree(os.path.join(source,basename),os.path.join(target,basename))
        return
    raise RuntimeError(source+' was neither file nor folder.')

def portorder(inFiles,inFolder,outFolders,N,subset,link):
    ''' Copy files in port order '''
    outidx=0
    for row in inFiles:
        if subset==0:
            copyfileorfolder(row,inFolder,outFolders[outidx],link)
        else:
            if subset==outidx+1:
                copyfileorfolder(row,inFolder,outFolders[0],link)
        outidx=outidx+1
        if outidx+1>N:
            outidx=0

def fileorder(inFiles,inFolder,outFolders,N,subset,link):
    ''' Copy files in input file order '''

    bins=[int(math.floor(float(len(inFiles))/float(N)))]*int(N)
    binidx=0
    while sum(bins)<len(inFiles):
        bins[binidx]+=1
        binidx+=1
    offsets=list(offset(bins))
    offsets.insert(0,0)

    if subset==0:
        for outidx in range(N):
            for f in range(offsets[outidx], offsets[outidx]+bins[outidx]):
                copyfileorfolder(inFiles[f],inFolder,outFolders[outidx],link)
    else:
        outidx=subset-1
        for f in range(offsets[outidx], offsets[outidx]+bins[outidx]):
            copyfileorfolder(inFiles[f],inFolder,outFolders[0],link)

def offset(it):
    total = 0
    for x in it:
        total += x
        yield total

''' Splits a folder input in N outputs '''
inFolder = dir
outFolders=[]
cf = anduril.CommandFile.from_file(sys.argv[1])
for x in range(9):
    outFolders.append(cf.get_output('dir'+str(x+1)))
    os.mkdir(outFolders[x])
method = order.lower().strip()
if method not in ('port','file'):
    write_error("Writing order: \""+method+"\" not recognized.")
    sys.exit(1)
# limit of 9 outputs, happens only when subset is not used
if subset==0:
    if N>9:
        N=9
# list files, and remove hidden  (.files)
inFiles=sorted(filter(lambda x: not x.startswith('.'), os.listdir(inFolder)))
if method=='port':
    portorder(inFiles,inFolder,outFolders,N,subset,link)
if method=='file':
    fileorder(inFiles,inFolder,outFolders,N,subset,link)
    
