import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

import fi.helsinki.ltdk.csbl.javatools.graphMetrics.GraphMetrics;
import fi.helsinki.ltdk.csbl.javatools.graphMetrics.GraphML;
import fi.helsinki.ltdk.csbl.javatools.graphMetrics.VertexResult;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

import edu.uci.ics.jung.graph.Graph;

public class GraphMetricsComponent extends SkeletonComponent {
    
    public static double wrapDouble(double d) {
        if (d == Double.POSITIVE_INFINITY || d == Double.NEGATIVE_INFINITY) {
            return Double.NaN;
        } else {
            return d;
        }
    }
    
    public static double getRank(double value, List<Double> fullList) {
        if (fullList.size() < 1) return 0.0;
        if (value == Double.NaN) return 0.0;
        
        final double EPSILON = 1e-8;
        /* We search the index of value-EPSILON so that we get the left-size
        index of the search key, i.e. the smallest index where
        fullList[index] is approx. equal to value. */
        int index = Collections.binarySearch(fullList, value-EPSILON);
        if (index < 0) index = -index - 1;
        assert index >= 0 && index < fullList.size();
        return (double)index / (fullList.size()-1.0);
    }
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        GraphML reader = new GraphML(cf.getInput("graph"));
        reader.loadGraph();
        Graph<String,String> graph = reader.getGraph();
        GraphMetrics gm = new GraphMetrics(graph);

        final String nameAttribute = cf.getParameter("nameAttribute");        
        
        /* Write graph-specific metrics */
        final String[] GRAPH_COLUMNS = new String[] {"Metric", "Value"};
        CSVWriter graphWriter = new CSVWriter(GRAPH_COLUMNS,
            cf.getOutput("graphMetrics"));
        
        graphWriter.write("NumVertices");
        graphWriter.write(graph.getVertexCount());

        graphWriter.write("NumEdges");
        graphWriter.write(graph.getEdgeCount());
        
        graphWriter.write("Diameter");
        graphWriter.write(wrapDouble(gm.diameter()));
        
        graphWriter.write("MeanDegree");
        graphWriter.write(wrapDouble(gm.avgDegree()));
        
        graphWriter.write("MeanShortestPath");
        graphWriter.write(wrapDouble(gm.charasteristicPathLength()));
        graphWriter.close();
        
        /* Write vertex-specific metrics */
        final boolean normalize = cf.getParameter("normalize").equals("true");
        VertexResult clusteringResult = gm.clusteringCoefficients();
        VertexResult degreeResult = gm.degreeCentrality(normalize);
        VertexResult closenessResult = gm.closenessCentrality(normalize);
        VertexResult betweennessResult = gm.betweennessCentrality(normalize);
        VertexResult eigenvectorResult = gm.eigenvectorCentrality();
        
        List<Double> degreeList = new ArrayList<Double>(degreeResult.getMap().values());
        List<Double> closenessList = new ArrayList<Double>(closenessResult.getMap().values());
        List<Double> betweennessList = new ArrayList<Double>(betweennessResult.getMap().values());
        List<Double> eigenvectorList = new ArrayList<Double>(eigenvectorResult.getMap().values());
        
        Collections.sort(degreeList);
        Collections.sort(closenessList);
        Collections.sort(betweennessList);
        Collections.sort(eigenvectorList);
        
        final String[] VERTEX_COLUMNS = new String[] {
            "Vertex", "InDegree", "OutDegree", "ClusteringCoeff",
            "DegreeCentrality", "ClosenessCentrality",
            "BetweennessCentrality", "EigenvectorCentrality",
            "DegreeCentralityRank", "ClosenessCentralityRank",
            "BetweennessCentralityRank", "EigenvectorCentralityRank"
        };
        CSVWriter vertexWriter = new CSVWriter(VERTEX_COLUMNS,
            cf.getOutput("vertexMetrics"));
        
        List<String> vertexKeys = new ArrayList<String>(graph.getVertices());
        Collections.sort(vertexKeys);
        
        for (String vertexKey: vertexKeys) {
            String name;
            if (nameAttribute != null && !nameAttribute.isEmpty()) {
                name = reader.getVertexMetadata(vertexKey, nameAttribute);
                if (name == null) name = vertexKey;
            } else {
                name = vertexKey;
            }
            
            final double degree = wrapDouble(degreeResult.getVertexValue(vertexKey));
            final double closeness = wrapDouble(closenessResult.getVertexValue(vertexKey));
            final double betweenness = wrapDouble(betweennessResult.getVertexValue(vertexKey));
            final double eigenvector = wrapDouble(eigenvectorResult.getVertexValue(vertexKey));
            
            vertexWriter.write(name);
            vertexWriter.write(graph.inDegree(vertexKey));
            vertexWriter.write(graph.outDegree(vertexKey));
            vertexWriter.write(wrapDouble(clusteringResult.getVertexValue(vertexKey)));
            
            vertexWriter.write(degree);
            vertexWriter.write(closeness);
            vertexWriter.write(betweenness);
            vertexWriter.write(eigenvector);
            
            vertexWriter.write(getRank(degree, degreeList));
            vertexWriter.write(getRank(closeness, closenessList));
            vertexWriter.write(getRank(betweenness, betweennessList));
            vertexWriter.write(getRank(eigenvector, eigenvectorList));
        }
        
        vertexWriter.close();
        
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new GraphMetricsComponent().run(args);
    }
}
