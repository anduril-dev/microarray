library(componentSkeleton)
library(igraph)
library(utils)
library(XML)
# SSOAP is loaded in annotate.kegg only if necessary 

execute <- function(cf) {
    if (input.defined(cf, 'kgml')) {
        kegg.file <- get.input(cf, 'kgml')
    } else {
        pathway.id <- get.parameter(cf, 'pathwayID')
        if (nchar(pathway.id) == 0) {
            write.error(cf, sprintf('Neither the input pathway file nor the pathwayID parameter is given'))
            return(PARAMETER_ERROR)
        }
        if (length(grep('[a-zA-Z]{3}[0-9]+', pathway.id)) > 0) organism.len <- 3
        else if (length(grep('[a-zA-Z]{2}[0-9]+', pathway.id)) > 0) organism.len <- 2
        else {
            write.error(cf, sprintf('Can not parse pathway ID: %s', pathway.id))
            return(PARAMETER_ERROR)
        }
        
        organism <- substr(pathway.id, 1, organism.len)
        num.id <- substr(pathway.id, organism.len+1, nchar(pathway.id))
        kegg.url <- sprintf('ftp://ftp.genome.jp/pub/kegg/xml/organisms/%s/%s%s.xml', organism, organism, num.id)
        
        kegg.file <- tempfile()
        status <- download.file(kegg.url, kegg.file, 'internal')
        if (status != 0) {
            write.log(cf, sprintf('Downloading %s failed: got status code %s', kegg.url, status))
            return(1)
        }
    }

    graph <- parse.kgml(cf, kegg.file)
    target.dbs <- split.trim(get.parameter(cf, 'annotation'), ',')
    graph <- annotate.kegg(graph, target.dbs)
    
    write.graph(graph, get.output(cf, 'pathway'), format='graphml')
    
    write.log(cf, sprintf('Created a graph with %d vertices and %d edges',
        vcount(graph), ecount(graph)))
    
    return(0)
}

make.complex.id <- function(counter) {
    return(sprintf('complex-%s', counter))
}

# For a (protein) complex, add edges between all members of
# the complex.
# For e.g. vertices = (v1, v2, v3), create edges
# (v1,v2, v1,v3, v2,v3, v2,v1, v3,v1, v3,v2).
add.complex.edges <- function(graph, complex.vertices) {
    stopifnot(is.numeric(complex.vertices))
    if (length(complex.vertices) > 1) {
        m <- combn(as.integer(complex.vertices), 2)
        edges <- c(m, m[2:1,])
        graph <- add.edges(graph, edges, type='complex', subtypes='')
    }
    return(graph)
}

parse.kgml <- function(cf, kgml.filename) {
    stopifnot(file.exists(kgml.filename))
    root <- xmlRoot(xmlTreeParse(kgml.filename))
    
    g <- graph.empty(directed=TRUE)
    complex.counter <- 0
    
    g <- set.graph.attribute(g, 'name', xmlGetAttr(root, 'name', ''))
    g <- set.graph.attribute(g, 'organism', xmlGetAttr(root, 'org', ''))
    g <- set.graph.attribute(g, 'title', xmlGetAttr(root, 'title', ''))
    g <- set.graph.attribute(g, 'image', xmlGetAttr(root, 'image', ''))
    
    # Add normal entries, but not complex nodes yet. However,
    # normal entries that have multiple names are treated as
    # complexes.
    for (node in xmlElementsByTagName(root, 'entry', TRUE)) {
        entry.id <- xmlGetAttr(node, 'id', default='')
        entry.name <- xmlGetAttr(node, 'name', default='')
        entry.type <- xmlGetAttr(node, 'type', default='')
        if (entry.type == 'group') next
        
        node.names <- split.trim(entry.name, ' ')
        if (length(node.names) > 1) {
            complex.counter <- complex.counter + 1
            complex.id <- make.complex.id(complex.counter)
        } else {
            complex.id <- ''
        }
        
        if (vcount(g) > 0) prev.max.vertex <- max(V(g), na.rm=TRUE)
        else prev.max.vertex <- -1
        
        g <- add.vertices(g, length(node.names), networkID=entry.id, name=node.names,
            type=entry.type, complex=complex.id)
            
        if (length(node.names) > 1) {
            g <- add.complex.edges(g, c(V(g)[V(g) > prev.max.vertex]))
        }
    }
    
    # Since group entries are not inserted to the graph, the following
    # table (group ID -> list of component IDs) stores group information.
    groups <- list()
    
    # Handle complex entries (groups).
    for (node in xmlElementsByTagName(root, 'entry', TRUE)) {
        entry.id <- xmlGetAttr(node, 'id', default='')
        entry.name <- xmlGetAttr(node, 'name', default='')
        entry.type <- xmlGetAttr(node, 'type', default='')
        if (entry.type != 'group') next
        
        complex.counter <- complex.counter + 1
        elements <- xmlApply(node, function(child) if (xmlName(child) == 'component') xmlGetAttr(child, 'id', default=''))
        elements <- unlist(elements)
        vertices <- V(g)[ networkID %in% elements ]
        if (length(vertices) < length(elements)) {
            write.log(cf, sprintf('Warning: some entries for complex %s not found', entry.id))
        }
        groups[[entry.id]] <- elements
        V(g)[vertices]$complex <- sprintf('complex-%s', complex.counter)
        g <- add.complex.edges(g, vertices)
    }
    
    # Handle relations.
    for (node in xmlElementsByTagName(root, 'relation', TRUE)) {
        entry1 <- xmlGetAttr(node, 'entry1', default='')
        entry2 <- xmlGetAttr(node, 'entry2', default='')
        
        attrs <- list()
        attrs$type <- xmlGetAttr(node, 'type', default='')
        
        subtypes <- character()
        for (subtype.node in xmlElementsByTagName(node, 'subtype', TRUE)) {
            subtype.name <- xmlGetAttr(subtype.node, 'name', default='')
            subtypes <- c(subtypes, subtype.name)
        }
        attrs$subtypes <- paste(sort(subtypes), collapse=',')
        
        from <- V(g)[networkID == entry1]
        if (length(from) == 0) from <- V(g)[networkID %in% groups[[entry1]] ]
        if (length(from) > 0) from <- unique(from)
        if (length(from) == 0) {
            write.log(cf, sprintf('Warning: in edge %s-%s: first entry not found', entry1, entry2))
        }
        
        to <- V(g)[networkID == entry2]
        if (length(to) == 0) to <- V(g)[networkID %in% groups[[entry2]] ]
        if (length(to) > 0) to <- unique(to)
        if (length(to) == 0) {
            write.log(cf, sprintf('Warning: in edge %s-%s: second entry not found', entry1, entry2))
        }
        
        for (v1 in from) {
            for (v2 in to) {
                g <- add.edges(g, c(v1, v2), attr=attrs)
            }
        }
    }

    g <- simplify(g, edge.attr.comb='first')
    return(g)
}

### Annotation ###

kegg.cross.links <- function(soap.interface, target.db, key) {
    result <- try(soap.interface@functions$get_linkdb_by_entry(entry_id=key,
        db=target.db, offset=1, limit=100))
    if (class(result) == 'try-error') return(character(0))
    
    values <- sapply(result, function(r) r$entry_id2)
    # Strip "targetdb:" prefix from result (where present)
    values <- sub('^[^:]*:', '', values)
    return(values)
}

annotate.kegg <- function(graph, target.dbs, source.attr='name',
        target.attr.pattern='db_%s', wsdl.url='http://soap.genome.jp/KEGG.wsdl') {
    if (length(target.dbs) == 0) return(graph)
    
    library(SSOAP)
    
    cat('Fetching annotation. Missing entries may print an error; these are skipped.\n')
    kegg <- processWSDL(wsdl.url)
    soap.interface <- genSOAPClientInterface(def=kegg, nameSpaces='1.1')
    
    for (v in V(graph)) {
        source.id <- get.vertex.attribute(graph, source.attr, v)
        for (target.db in target.dbs) {
            target.ids <- kegg.cross.links(soap.interface, target.db, source.id)
            if (length(target.ids) == 0) next
            target.s <- paste(target.ids, collapse=',')
            target.attr <- sprintf(target.attr.pattern, target.db)
            graph <- set.vertex.attribute(graph, target.attr, v, target.s)
        }
    }
    return(graph)
}

main(execute)
