#!/usr/bin/python
import sys
import anduril
from anduril.args import *
import csv
import re

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

valueCols = values.split(',')
if (len(valueCols)>1 and (not useValueName)):
    useValueName=True
    msg='Multiple value columns selected, forcing useValueName=true'
    write_log(msg)
valueCols=[x.strip() for x in valueCols]
write_log('read: '+input_csv)
# read CSV
reader = anduril.TableReader(input_csv,column_types=str)
if (label not in reader.get_fieldnames()):
    write_error("Column \""+label+"\" not found.")
    sys.exit(1)
sourcevalue = {}
sourcevalue['label'] = []
for v in valueCols:
    sourcevalue[v] = []
    if (v not in reader.get_fieldnames()):
        msg="Column \""+v+"\" not found."
        print(msg)
        write_error(msg)
        sys.exit(1)
for row in reader:
    sourcevalue['label'].append( row.get(label) )
    for v in valueCols:
        sourcevalue[v].append( row.get(v) )
    
uniqlabel=natural_sort(set(sourcevalue['label']))
    
header = []
output={}
if useRowId:
    header.append('RowId')
    output['RowId']=[]

for v in valueCols:
    for l in uniqlabel:
        if useValueName:
            header.append(v+valueNameSeparator+l)
            output[v+valueNameSeparator+l]=[]
        else:
            header.append(l)
            output[l]=[]
# re-sort values in to columns        
maxlen=0  
for l in uniqlabel:
    idxs=[i for i,x in enumerate(sourcevalue['label']) if x == l]
    maxlen=max(len(idxs),maxlen)
    for v in valueCols:
        thislist=[sourcevalue[v][i] for i in idxs]
        if useValueName:
            key=v+valueNameSeparator+l
        else:
            key=l
        # Remove duplicate columns
        if removeDuplicates and thislist in output.values():
            del output[key]
            header.remove(key)
        else :
            output[key]=thislist

if useRowId:
    output['RowId']=['Row'+str(i) for i in range(1,maxlen+1)]
# append the missing values:        
for k in output.keys():
    if len(output[k])<maxlen:
        output[k].extend(['NA'] * (maxlen-len(output[k])))

# Header re-ordering if needed:
if len(order)>0:
    print('we should re-order the results here')
    if re.search('[^0-9,]',order):
        msg='Illegal characters in parameter "order". It can only contain numbers and commas. (not spaces)'
        print(msg)
        write_error(msg)
        sys.exit(1)
    order=eval('['+order+']')
    if len(uniqlabel)!=len(order):
        msg='Order list contains different number of elements than unique labels ('+str(len(uniqlabel))+')'
        print(msg)
        write_error(msg)
        sys.exit(1)
    
    # order needs to start from 0...
    order=[i-1 for i in order]
    for v in range(1,len(valueCols)):
        order.extend([i+len(uniqlabel) for i in order])
    keytmp=[]
    # ... unless we use the unique row index
    if useRowId:
        order=[i+1 for i in order]
        order.insert(0,0)
    for c in order:
        keytmp.append(header[c])
    header=keytmp
#write results        
writer=anduril.TableWriter(output_csv, header)
for r in range(maxlen):
    rowtmp={}
    for k in output.keys():
        rowtmp[k]=(output[k][r])
    writer.writerow(rowtmp)

