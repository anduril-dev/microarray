import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Interactome extends SkeletonComponent {

    public static final String WS_URL = "WS_URL";
    public static final String IS_EXTEND = "extend";
    public static final String INPUT_KEYS = "query";
    public static final String QUERY_TYPE = "findPartners";
    public static final String OUTPUT_GRAPH = "outputGraphML";
    public static final String OUTPUT_CSV = "outputCSV";
    public static final String OUTPUT_CSV_FILE = "interactions";
    public static final String OUTPUT_GRAPHML_FILE = "network";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        String  url             = cf.getParameter(WS_URL);
        boolean isQueryProtein  = Boolean.valueOf(cf.getParameter(QUERY_TYPE));
        boolean isOutputGraphML = Boolean.valueOf(cf.getParameter(OUTPUT_GRAPH));
        boolean isOutputCSV     = Boolean.valueOf(cf.getParameter(OUTPUT_CSV));
        boolean isExtend        = Boolean.valueOf(cf.getParameter(IS_EXTEND));

        // get input
        CSVParser keys = new CSVParser(cf.getInput(INPUT_KEYS));
        // output columes
        String[] cols = new String[]{"queryProtein", "interactingProtein"};

        //read and query
        List<String> queryList = new LinkedList<String>();
        Set<String>  querySet  = null;
        String       queryType;
        if (isQueryProtein) {
            int columnIdx = AsserUtil.indexOf(cf.getParameter("column"), keys.getColumnNames(), true, true);
            querySet = new HashSet<String>();
            while (keys.hasNext()) {
                String key = keys.next()[columnIdx];
                if (key != null) {
                    queryList.add(key);
                    querySet.add(key);
                }
            }
            queryType = "protein";
        } else {
            cols = new String[]{"InteractionPartnerA", "InteractionPartnerB"};
            int columnIdx  = AsserUtil.indexOf(cf.getParameter("column"),  keys.getColumnNames(), true, true);
            int columnIdx2 = AsserUtil.indexOf(cf.getParameter("column2"), keys.getColumnNames(), true, true);
            while (keys.hasNext()) {
                String[] line = keys.next();
                queryList.add(line[columnIdx] + '\t' + line[columnIdx2]);
            }
            queryType = "interaction";
        }
        if (isOutputGraphML) {
            Tools.copyStreamToFile(sendQuery(queryList, queryType, FORMAT_GRAPHML, isExtend, url),
                                   cf.getOutput(OUTPUT_GRAPHML_FILE));
        } else {
            Tools.writeString(cf.getOutput(OUTPUT_GRAPHML_FILE),
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
            "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\">\n"+
            "</graphml>");
        }

        ErrorCode status;
        if (isOutputCSV) {
           status = writeTable(cf, cols, sendQuery(queryList, queryType, FORMAT_PLAIN_TSV, isExtend, url), querySet);
        } else {
           CSVWriter out = new CSVWriter(cols, cf.getOutput(OUTPUT_CSV_FILE));
           out.close();
           status = ErrorCode.OK;
        }

        return status;
    }

    /**
     * @param queryList
     * @return reponse stream from postMethod
     * @throws java.io.IOException
     */
    private InputStream sendQuery(List<String> queryList,
                                  String       queryType,
                                  String       format,
                                  boolean      isExtended,
                                  String       url) throws IOException {
        if (queryList == null)
            return null;

        StringBuffer query = new StringBuffer(1024);
        for (int i = 0; i < queryList.size(); i++) {
            query.append(queryList.get(i)).append('\n');
        }

        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(url);
        method.addParameter("query",    query.toString());
        method.addParameter("format",   format);
        method.addParameter("extended", String.valueOf(isExtended));
        method.addParameter("type",     queryType);
        // Read the response header, which is redirected.
        int statusCode = client.executeMethod(method);
        System.out.println("Status code:" + statusCode);

        Header locationHeader = method.getResponseHeader("location");
        if (locationHeader != null) {
            String redirectLocation = locationHeader.getValue();
            System.out.println("redirected to:" + redirectLocation);
        }

        if (statusCode != HttpStatus.SC_OK) {
            System.err.println("Failed, got " + method.getStatusLine() + " for " + method.getURI());
            return null;
        }
        return method.getResponseBodyAsStream();
    }

    private static final String FORMAT_GRAPHML = "graphML";
    private static final String FORMAT_PLAIN_TSV = "plain_tsv";

    private ErrorCode writeTable(CommandFile cf, String[] cols, InputStream is, Set<String> querySet) throws IOException {
        FileOutputStream fOut   = new FileOutputStream(cf.getOutput(OUTPUT_CSV_FILE));
        CSVWriter        out    = new CSVWriter(cols, fOut);
        ErrorCode        status = ErrorCode.OK;

        if (is == null) {
           status = ErrorCode.ERROR;
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String         line;
            while (null != (line=br.readLine())) {
                String[] ss = line.split("\t");
                /* Order of proteins needs to be swapped if the second protein is
                 * a query protein and the first is not. */
                boolean swap = (querySet != null && !querySet.contains(ss[0]) && querySet.contains(ss[1]));
                if (swap) {
                    out.write(ss[1]);
                    out.write(ss[0]);
                } else {
                    out.write(ss[0]);
                    out.write(ss[1]);
                }
            }
            br.close();
        }

        out.close();
        return status;
    }

    public static void main(String[] args) {
        new Interactome().run(args);
    }
}
