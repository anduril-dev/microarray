import java.io.*;
import java.util.*;

import fi.helsinki.ltdk.csbl.asser.*;
import fi.helsinki.ltdk.csbl.asser.io.*;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

public class LiftOverRunner extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        File      regions    = cf.getInput("regions");
        File      chain      = cf.getInput("chain");
        File      regsOut    = cf.getOutput("regions");
        String    executable = cf.getParameter("command");
        int       minLength  = Integer.parseInt(cf.getParameter("minLength"));
        CSVParser in         = new CSVParser(regions);
        String[]  inColnames = in.getColumnNames();
        int[]     inCols     = AsserUtil.indicesOf(
                                 new String[] {
                                     cf.getParameter("colId"),
                                     cf.getParameter("colChr"),
                                     cf.getParameter("colStart"),
                                     cf.getParameter("colEnd")
                                 },
                                 inColnames, true, true
                               );

        File bed     = prepareBed(in, inCols);
        File newFile = File.createTempFile(LiftOverRunner.class.getName(),".new");
        File unMap   = File.createTempFile(LiftOverRunner.class.getName(),".umap");

        runLiftOver(bed, chain, newFile, unMap, executable, minLength, cf);
        in.close();
        prepareCSV(regions, newFile, regsOut, inCols);

        bed.delete();
        newFile.delete();
        unMap.delete();
        return ErrorCode.OK;
    }

    static private void prepareCSV(File  regions,
                                   File  newFile,
                                   File  regsOut,
                                   int[] cols) throws IOException {
       CSVParser in      = new CSVParser(regions);
       Scanner   newIn   = new Scanner(newFile);
       CSVWriter out     = new CSVWriter(in.getColumnNames(), regsOut);
       String[]  resLine = new String[4];
       boolean[] quotes  = new boolean[in.getColumnCount()];

       for (int i=0; i<quotes.length; i++) {
           quotes[i] = in.hasQuotations(i);
       }

       readLine(newIn, resLine);
       for (String[] line : in) {
           if (resLine[0] == null) break;
           while (line[cols[0]].equals(resLine[0])) {
              line[cols[1]] = resLine[1];
              line[cols[2]] = resLine[2];
              line[cols[3]] = resLine[3];
              for (int i=0; i<line.length; i++) {
                  out.write(line[i], quotes[i]);
              }
              readLine(newIn, resLine);
              if (resLine[0] == null) break;
           }
       }

       out.close();
       in.close();
    }

    static private void readLine(Scanner in, String[] cols) {
       if (in.hasNext()) {
          cols[1] = in.next().substring(3);
          cols[2] = in.next();
          cols[3] = in.next();
          cols[0] = in.next();
          in.nextLine();
       } else {
          cols[0] = null;
       }
    }

    static private File prepareBed(CSVParser in, int[] cols) throws IOException {
       File        bed = File.createTempFile(LiftOverRunner.class.getName(),".bed");
       PrintWriter out = new PrintWriter(bed);

       for (String[] line : in) {
           out.append("chr")
              .append(line[cols[1]])
              .append('\t')
              .append(line[cols[2]])
              .append('\t')
              .append(line[cols[3]])
              .append('\t')
              .append(line[cols[0]])
              .println();
       }

       out.close();
       return bed;
    }

    static private void runLiftOver(File        bed,
                                    File        chain,
                                    File        newFile,
                                    File        unMap,
                                    String      executable,
                                    int         minLength,
                                    CommandFile cf) throws IOException,
                                                           InterruptedException {
        StringBuffer  command = new StringBuffer(512);
        final Process liftOver;

        command.append(executable)
               .append(" -bedPlus=4 -tab -multiple -minChainT=")
               .append(minLength)
               .append(" -minChainQ=")
               .append(minLength)
               .append(' ')
               .append(bed.getAbsolutePath())
               .append(' ')
               .append(chain.getAbsolutePath())
               .append(' ')
               .append(newFile.getAbsolutePath())
               .append(' ')
               .append(unMap.getAbsolutePath());
        cf.writeLog("Launching: "+command);
        liftOver = Runtime.getRuntime()
                          .exec(command.toString());

        // This thread copies LiftOver output to the log
        Thread outputThread = new Thread() {
            private final InputStream in = liftOver.getInputStream(); 
            public void run() {
                try {
                  OutputStream out = System.out;
                  byte[]       buf = new byte[512];
                  int          got;

                  while ((got=in.read(buf)) >= 0) {
                      out.write(buf, 0, got);
                  }
                  in.close();
                  out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(ErrorCode.ERROR.getCode());
                }
            }
        };
        outputThread.setDaemon(true);
        outputThread.start();

        BufferedReader err = new BufferedReader(new InputStreamReader(liftOver.getErrorStream()));
        int status = liftOver.waitFor();
        // Print possible warnings...
        String line;
        while ((line=err.readLine()) != null) {
            cf.writeLog(line);
        }
        err.close();
        if (status != 0)
            throw new RuntimeException("LiftOver failed and returned "+status+'.');
        outputThread.join(60*1000);
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new LiftOverRunner().run(argv);
    }

}
