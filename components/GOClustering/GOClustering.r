library(componentSkeleton)
library(csbl.go)

create.goheatmap<-function(cf,expr_dir,annotation_dir,out_dir,draw.heatmap,taxonomy){
    if (is.na(expr_dir)) {
        expr_matrix <- NULL
    } else {
        write.log(cf, sprintf('Reading %s', expr_dir))
        expr_matrix <- LogMatrix.read(expr_dir)
        write.log(cf, sprintf('%s: %d x %d matrix', 'expr', nrow(expr_matrix), ncol(expr_matrix)))
    }

    if (input.defined(cf, 'similarityTable')) {
        set.prob.table(filename=get.input(cf, 'similarityTable'))
    } else {
        set.prob.table(organism=taxonomy, type='similarity')
    }

    anno    <- read.csv(annotation_dir, header=TRUE, sep="\t", quote="\"", dec=".", fill=TRUE, comment.char="#", as.is=TRUE)
    key.col <- colnames(anno)[1]
    genec   <- get.parameter(cf,'geneColumn','string')
    if (nchar(genec) == 0) genec <- key.col

    # name.map maps internal IDs (key column) to reporting names (e.g. gene names)
    name.map                  <- anno[,genec]
    names(name.map)           <- anno[,key.col]
    missing.gene.id           <- is.na(name.map)
    name.map[missing.gene.id] <- anno[missing.gene.id,key.col]

    goc   <- get.parameter(cf,'goColumn','string')
    anno  <- anno[,c(key.col,goc)]
    if (get.parameter(cf,'dropUnknown','boolean')) {
        orig.count    <- nrow(anno)
        anno          <- anno[!is.na(anno[,goc]),]
        anno          <- anno[anno[,goc]!="",]
        dropped.count <- orig.count - nrow(anno)
    } else {
        dropped.count <- 0
    }
    if (nrow(anno) < 1) {
        CSV.write(get.output(cf, 'clusters'), SetList.new(NULL))
        return("\n\\large{\\textbf{Too few annotated genes for the GO clustering!}}\n")
    }

    if (get.parameter(cf, 'asBitmap', 'boolean')) {
       outname      <- sprintf('GOClustering-%s-heatmap.png', get.metadata(cf, 'instanceName'))
       out.filename <- file.path(out_dir, outname)
       image.height <- round(7.00*2.54, 2)
       png.open(out.filename, width=1100, height=1100)
    } else {
       outname      <- sprintf('GOClustering-%s-heatmap.pdf', get.metadata(cf, 'instanceName'))
       out.filename <- file.path(out_dir, outname)
       image.height <- NULL
       pdf(out.filename, paper='special')
    }

    # This cludge is a work-around for the entities construction   
    temp_dir <- tempfile(pattern="file", tmpdir=tempdir())
    write.table(anno, file=temp_dir, sep="\t", row.names=FALSE, col.names=FALSE)
    rm(anno)

    ent              <- entities.from.text(temp_dir, key.type="unknown", separator=NULL, header=FALSE)
    compute.p.values <- get.parameter(cf, 'pvalues','boolean')

    map<-go.heatmap(expr_matrix, # temp_expr,
                    temp_dir,
                    go.cut             = get.parameter(cf, 'cutLimit','float'),
                    use.inform         = TRUE,
                    grayscale          = get.parameter(cf, 'grayScale','boolean'),
                    margins            = c(get.parameter(cf,'columnMargin','float'),get.parameter(cf,'rowMargin','float')),
                    compute.p.values   = compute.p.values,
                    xlab               = get.parameter(cf,'xLab','string'),
                    ylab               = get.parameter(cf,'yLab','string'),
                    show.cluster.names = get.parameter(cf, 'showClusterNames', 'boolean'),
                    heatmap.scale      = get.parameter(cf, 'heatmapScale'),
                    name.map           = name.map)
    
    dev.off()

    tex <- character()
    if (draw.heatmap) {
        if (get.parameter(cf, 'grayScale','boolean')) {
           colorDesc <- 'The lower expression values are indicated with black and the higher expressions with white.'
        } else {
           colorDesc <- 'The lower expression values are indicated with red and higher expressions with white.'
        }
        countDesc <- sprintf('The analysis is based on %d gene products. The median number of GO annotations per gene product is %.0f.',
                             length(ent), median(entities.go.sizes(ent)))
        if (dropped.count > 0) {
            countDesc <- paste(countDesc,
            sprintf('In addition, %d gene products had no GO annotation and were excluded from the analysis.',
                    dropped.count))
        }
        caption <- paste(
            'GO clustering dendrogram and expression heatmap.',
            countDesc,
            'The colors represent expression values of the gene products.',
            colorDesc,
            'The left dendrogram corresponds to GO based clustering, while the top dendrogram uses expression values for clustering.')
        tex <- c(tex, latex.figure(outname, caption=caption, image.height=image.height), '\\clearpage')
    }

    names     <- c()
    gene_list <- c()
    len       <- length(map$members)

    bind_matrix <- data.frame(CLUSTER=c(), SIZE=c(), GOID=c(), PRIORI=c(), INFO=c(), DESCRIPTION=c(),
                              stringsAsFactors = FALSE)
    decimal     <- get.parameter(cf,'decimal','float')
    for(i in 1:len) {
        go       <- ""
        names[i] <- paste("G",i,sep="")
        if (!is.data.frame(map$desc[[i]]) || ncol(map$desc[[i]]) == 0) {
            sub_matrix <- data.frame(CLUSTER = names[i], SIZE=length(map$members[[i]]), GOID = '', PRIORI = '', INFO = '',
                                     DESCRIPTION = '\\textit{[no interesting GO terms]}', stringsAsFactors = FALSE)
        } else {
            rownames(map$desc[[i]])   <- NULL
            colnames(map$desc[[i]])   <- c("GOID","PRIORI","INFO","DESCRIPTION")
            map$desc[[i]]$PRIORI      <- round(map$desc[[i]]$PRIORI, decimal)
            map$desc[[i]]$INFO        <- round(map$desc[[i]]$INFO,   decimal)
            map$desc[[i]]$DESCRIPTION <- latex.quote(map$desc[[i]]$DESCRIPTION)
            sub_matrix <- data.frame(CLUSTER = c(names[i],rep(" ",length(map$desc[[i]]$GOID)-1)),
                                     SIZE    = c(length(map$members[[i]]),rep(" ",length(map$desc[[i]]$GOID)-1)),
                                     map$desc[[i]],
                                     stringsAsFactors = FALSE)
        }
        bind_matrix <- rbind(bind_matrix, sub_matrix)

        for (j in 1:length(map$members[[i]])) {
            tmp <- map$members[[i]][j]
            tmp <- trim(tmp, ' \t\n\r"')
            tmp <- name.map[tmp]
            go  <- paste(go,tmp,sep=" ")
        }
        gene_list[i]<-go
    }

    bind_matrix           <- as.matrix(bind_matrix)
    rownames(bind_matrix) <- NULL
    tex                   <- c(tex, latex.tabular(bind_matrix, "lllllp{10cm}", long=TRUE, escape.content=FALSE))

    # If parameter geneColumn is not given, gene lists for clusters are not written to the latex 
    # document.
    if (nchar(get.parameter(cf,'geneColumn','string')) != 0) {
        export_table <- data.frame(CLUSTER=names,MEMBERS=gene_list,stringsAsFactors=FALSE)
        export_table <- as.matrix(export_table)
        rownames(export_table) <- NULL
        width <- get.parameter(cf,'ColumnWidth','float')
        tex   <- c(tex,latex.tabular(export_table, sprintf('lp{%dcm}',width),long = TRUE))
    }

    if (compute.p.values) {
        p.table <- data.frame(CLUSTER =paste("G", 1:len, sep=""),
                              MEDIANIC=map$median.ic,
                              PVALUE  =sprintf('%.4f', map$p.value),
                              stringsAsFactors=FALSE)
        tex <- c(tex, latex.tabular(p.table, 'lrr'))
    }
    
    # Write cluster member genes to output
    cluster_idlists <- SetList.new()
    for(i in 1:len) {
        cluster_idlists <- SetList.assign(cluster_idlists,
            sprintf('G%d', i),
            map$members[[i]])
    }
    CSV.write(get.output(cf, 'clusters'), cluster_idlists)

    return(tex)
}

create.heading <- function() {
    return(c(
'',
'Gene products are clustered (grouped) based on their Gene Ontology (GO) annotations. Genes that',
'have similar annotations belong to the same cluster. The distance between gene products is defined',
'using information-theoretical semantic similarity measures.',
'',
'In the table below, the most informative (most specific) common GO terms of each cluster are',
'displayed. All gene products in the cluster are annotated with the GO term mentioned. A GO term is',
'more informative if it occurs rarely in the GO annotations of the whole genome. The priori column',
'contains the a priori probability $p$ that a random gene product is annotated with the given GO',
'term. Information is defined as $-\\log_2 p$.',
'',
'Generally, clusters with (1) a large number of gene products and (2) common GO terms with high',
'information content are the most interesting.',
''))
}

execute <- function(cf) {
    if (!input.defined(cf, 'expr')){
        input.dir1 <- NA
    } else {
        input.dir1<-get.input(cf,'expr')
    }
    input.dir2 <- get.input(cf,'goAnnotations')
    out.dir    <- get.output(cf,'report')
    dir.create(out.dir, recursive=TRUE)
    draw.heatmap <- get.parameter(cf, 'drawHeatmap', 'boolean')
    organism     <- get.parameter(cf, 'organism', 'int')
    sectionType <- get.parameter(cf, 'sectionType', 'string')

    tex <- character()
    if(sectionType != "") {
        tex <- c(tex, sprintf('\\%s{%s}', sectionType, latex.quote(get.parameter(cf, 'title'))))
    }
    tex <- c(tex, create.heading(), create.goheatmap(cf,input.dir1,input.dir2,out.dir,draw.heatmap,organism))
    
    latex.write.main(cf, 'report', tex)

    return(0)
}

main(execute)
