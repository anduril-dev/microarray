In the heat map PDF file, gene names should follow clustering so
that Ax names are in the bottom cluster, Bx names in the next,
Cx in the next and D1 in the uppermost cluster.

Five genes have expression patterns that can be seen in the heat map.
The color in one or two sample groups should be dimmer than in the
rest of the sample groups for that gene.

A1: dim in S2 + S3
A5: dim in S1
B2: dim in S1 + S2
B3: dim in S2
C2: dim in S3
