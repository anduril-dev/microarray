library(componentSkeleton)

summarise_all <- function(input,summary.type, post,stringMode) {
    if (summary.type=='dimensions') {
        temp.len=  nrow(input)
        temp.cols= ncol(input)
        temp.res = cbind(temp.cols,temp.len)
        colnames(temp.res)=c('Columns','Rows')
        colnames(temp.res)=paste(colnames(temp.res),post,sep="")
        return(temp.res)
    }
    if (nrow(input)==0) {
        temp.res=matrix(,nrow=0,ncol=ncol(input)+1)
        colnames(temp.res)=paste(c(colnames(input),'Count'),post,sep="")
        print("No input rows")
        return(temp.res)
    } 
    
    temp.len = as.matrix(nrow(input))
    colnames(temp.len)=paste('Count',post,sep="")

    temp.res = eval(parse(text=paste('apply(input,2,function(colmn) ',
            ' ',
            summary.type,'(as.numeric(colmn),na.rm=TRUE )',
            ' )', sep='')))

    temp.res=matrix(temp.res,nrow=1)
    colnames(temp.res)=paste(colnames(input),post,sep="")
    temp.res=cbind(temp.res,temp.len)
    
    if (stringMode) {
        charCols=which(apply(input,2,function(colmn) all(is.na(as.numeric(colmn)))))
        for (c in charCols) {
            temp.res[c]=strMode(input[,c])
        }
    }
    
    return(temp.res)
}

summarise_labeled <- function(input, summary.type, post, cluster.col, stringMode) {
    if (summary.type=='dimensions') {
        temp.len = as.matrix(tapply(input[,cluster.col],input[,cluster.col],length))
        temp.cols= ncol(input)
        temp.res = cbind(as.matrix(rownames(temp.len)),temp.cols,temp.len)
        colnames(temp.res)=c(cluster.col,paste(c('Columns','Rows'),post,sep=""))
        return(temp.res)
    }
    if (nrow(input)==0) {
        temp.res=matrix(,nrow=0,ncol=ncol(input)+1)
        colnames(temp.res)=c(cluster.col, paste(c(colnames(input)[colnames(input)!=cluster.col],'Count'),post,sep=""))
        print("No input rows")
        return(temp.res)
    } 
    
    temp.len = as.matrix(tapply(input[,cluster.col],input[,cluster.col],length))
    
    colnames(temp.len)=paste('Count',post,sep="")
    temp.res = eval(parse(text=paste('apply(input[,-which(names(input)==cluster.col),drop=FALSE],
        2,function(colmn) tapply(as.numeric(colmn),input[,cluster.col,drop=FALSE],',
        summary.type,',na.rm=TRUE) )',sep='')))

    if (nrow(temp.len)==1) {
       # messes up if there is only 1 unique clusterCol label
        temp.res=matrix(temp.res,nrow=1,ncol=ncol(input)-1)
        colnames(temp.res)=colnames(input[,-which(names(input)==cluster.col),drop=FALSE])
        colnames(temp.res)=paste(colnames(temp.res),post,sep="")
        temp.res=cbind(input[1,cluster.col],temp.res,temp.len)
        colnames(temp.res)=c(cluster.col, colnames(temp.res)[-1])

    } else {
        ids=as.matrix(rownames(temp.res))
        rownames(ids)=ids
        colnames(ids)=matrix(cluster.col)
        colnames(temp.res)=paste(colnames(temp.res),post,sep="")
        temp.res=cbind(ids,temp.res,temp.len)
    }    
    
    if (stringMode) {
        clusterCol=which(colnames(input)==cluster.col)
        charCols=which(apply(input,2,function(colmn) all(is.na(as.numeric(colmn)))))
        charCols=charCols[charCols!=clusterCol]
        for (c in charCols) {
            c.name=colnames(input)[c]
            i=which(colnames(temp.res)==c.name)
            temp.res[,i]=tapply(input[,c],input[,clusterCol,drop=F],function(rows) strMode(rows))
        }
    }
    return(temp.res)
}

execute <- function(cf){
    # Read input
    input.file <- get.input(cf,'csv')
    
    # Read parameters
    summary.type <- get.parameter(cf, 'summaryType')
    cluster.col <- get.parameter(cf, 'clusterCol')
    post <- get.parameter(cf,'postString')
    counts <- get.parameter(cf,'counts','boolean')
    stringMode <- get.parameter(cf,'stringMode','boolean')
    # Create output
    output.summary <- get.output(cf,'summary')
    
    # Type checking
    
    if (length(which(c('mean','median','mode','var','max','min','sd','sum','IQR','mad','uhinge','lhinge','dimensions')==summary.type))==0){
        write.error(cf,paste('The Summary type ',summary.type,' is not available.',sep=''))
        return(PARAMETER_ERROR)
    }
    
    #Evaluating and calculating the Summaries
    
    input.temp=CSV.read(input.file)
    
    #Summarise all rows, if no labels are defined
    if (cluster.col=="") {
        temp.res <- summarise_all(input.temp, summary.type, post, stringMode)
    } else { # use labels
        #Check the validity of the clustering label
        if(length(which(names(input.temp)==cluster.col))==0){
            write.error(cf, paste('Error in file', input.file, ': Column "',cluster.col,'" does not exist', sep='' ))
            return(PARAMETER_ERROR)
        }
        temp.res <- summarise_labeled(input.temp, summary.type, post, cluster.col, stringMode)
    }
    rownames(temp.res)=c()   
    if (!counts) {
        temp.res=temp.res[,1:(ncol(temp.res)-1),drop=FALSE]
    }
    CSV.write(output.summary, temp.res)
}
uhinge <- function(x,...) {
    return(fivenum(x)[4])
}
lhinge <- function(x,...) {
    return(fivenum(x)[2])
}
mode <- function(x,...) {
    y<-table(x)
    return(as.numeric(labels(which(y==max(y))))[1])
}
strMode <- function(x,...) {
    y<-table(x)
    return(labels(which(y==max(y)))[1])
}

main(execute)
