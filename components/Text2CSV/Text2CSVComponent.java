import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;


public class Text2CSVComponent extends SkeletonComponent {

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		File textFile = cf.getInput("text");
		
		BufferedReader in = new BufferedReader( new FileReader(textFile));
		
		String sep = cf.getParameter("sep");
		String [] header ;
		
		boolean usePattern = cf.getBooleanParameter("usePattern");
		/*
		Pattern testp = Pattern.compile("([0-9]*)([a-z]*)");//"(rs[0-9]*)[0-9\\s\\.]*([a-z]*)");
		
		Matcher testm = testp.matcher("11063263regulation");
		
		if(testm.matches()){
		
			System.out.println("testing pattern:");
			for(int i = 1; i <= testm.groupCount(); i++){
				System.out.println(i+" : "+testm.group(i));
			}
		}
		*/
		//file contains column headers
		Pattern p = Pattern.compile(sep);
		if(cf.getParameter("header").equals("")){
			
			if(usePattern){
				
				Matcher m = p.matcher(in.readLine());
				header = new String [m.groupCount()];
				if(m.matches()){
					for(int i = 1; i <= m.groupCount(); i++){
						header[i-1] = m.group(i);
					}
				}
			}else{
				header = in.readLine().trim().split(sep);
			}
		}else{
			header = cf.getParameter("header").split(",");
		}
		
		CSVWriter out = new CSVWriter(header, cf.getOutput("csv"));
		String line = in.readLine();
		while(line != null){
			if(usePattern){
				
				Matcher m = p.matcher(line);

				if(m.matches()){
					
					for(int i = 0; i < header.length; i++){
					
						if(i + 1 <= m.groupCount())
							out.write(m.group(i+1));
						else
							out.write(null);
					}
				}
			}else{
				String [] values = line.trim().split(sep);
				//only matching number of values is inserted to file
				for(int i = 0; i < out.getColumnCount(); i++){

					if(i < values.length){
						if(values[i].startsWith("\"") && values[i].endsWith("\"")){
							values[i] = values[i].substring(1);
							values[i] = values[i].substring(0, values[i].length()-1);
						}
						out.write(values[i]);
					
					}else{
						out.write("");
					}
				}
			}
			line = in.readLine();
		}
		in.close();
		out.close();
		
		return ErrorCode.OK;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		new Text2CSVComponent().run(args);
	}

}
