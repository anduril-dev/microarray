library(componentSkeleton)

execute <- function(cf){
    # Inputs
    keysIn <- CSV.read(get.input(cf, 'csv'))

    # Parameters
    keyColumn <- get.parameter(cf, 'keyColumn', type = 'string')
    targetDbs <- get.parameter(cf, 'targetDB',  type = 'string')
    db        <- get.parameter(cf, 'arrayType', type = 'string')

    targetDbs <- unlist(strsplit(targetDbs, ','))
    if (keyColumn == "") keyColumn <- CSV.read.first.cell(get.input(cf, 'csv'))
    keys      <- keysIn[,keyColumn]

    # Database
    dbString <- character()
    if (db == "450"){
        library(IlluminaHumanMethylation450k.db)
        dbString <- "IlluminaHumanMethylation450k"
    } else if (db == "27"){
        library(IlluminaHumanMethylation27k.db)
        dbString <- "IlluminaHumanMethylation27k"
    } else if (db == "v3"){
        library(illuminaHumanv3.db)
        dbString <- "illuminaHumanv3"
    } else if (db == "v4"){
        library(illuminaHumanv4.db)
        dbString <- "illuminaHumanv4"
    } else {
        write.error(cf, c("Invalid arrayType:", db, "Must be one of \'27\', \'450\',\'v3\' or \'v4\'."))
        return(INVALID_INPUT)
    }

    # Annotations
    out <- data.frame(keyColumn = keys)
    for (target in 1:length(targetDbs)){
        targString <- paste(dbString, targetDbs[target], sep = "")
        annot <- mget(keys, get(targString), ifnotfound=NA)
        annotOut <- gsub("c\\(", "", as.character(annot))
        annotOut <- gsub("\\)", "", annotOut)
        annotOut <- gsub(", ", ",", annotOut)
        annotOut <- gsub("\\\"", "", annotOut)
        out <- cbind(out, as.character(annotOut))
    }
    colnames(out) <- c(keyColumn, targetDbs)
    CSV.write(get.output(cf, 'bioAnnotation'), out)
}

main(execute)
