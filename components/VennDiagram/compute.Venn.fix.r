## A redefinition of a function from the Vennerable package (v.2.0)
## which is compatible with R 2.13.0.
compute.Venn <- function (V, doWeights = TRUE, doEuler = FALSE, type)
{
    nSets <- NumberOfSets(V)
    if (nSets < 2) {
        stop("Not enough sets")
    }
    if (missing(type)) {
        type <- if (nSets == 2) {
            "circles"
        }
        else if (nSets == 3) {
            "circles"
        }
        else if (nSets == 4) {
            if (doWeights) 
                "ChowRuskey"
            else "squares"
        }
        else {
            if (doWeights) 
                "ChowRuskey"
            else "AWFE"
        }
    }
    C3 <- switch(type, AWFEscale = , battle = , cog = , 
        AWFE=compute.AWFE(V, type = type), ChowRuskey=compute.CR(V, 
            doWeights), circles = if (nSets == 2) {
            compute.C2(V, doWeights, doEuler)
        } else if (nSets == 3) {
            compute.C3(V, doWeights)
        } else {
            stop(sprintf("Type %s not implemented for %d sets", 
                type, nSets))
        }, squares = if (nSets == 2) {
            compute.S2(V, doWeights, doEuler)
        } else if (nSets == 3) {
            compute.S3(V, doWeights)
        } else if (nSets == 4) {
            compute.S4(V, doWeights)
        } else {
            stop(sprintf("Type %s not implemented for %d sets", 
                type, nSets))
        }, triangles = if (nSets == 3) {
            compute.T3(V, doWeights)
        } else {
            stop(sprintf("Type %s not implemented for %d sets", 
                type, nSets))
        }, ellipses = if (nSets == 4) {
            compute.E4(V, doWeights)
        } else {
            stop(sprintf("Type %s not implemented for %d sets", 
                type, nSets))
        })
    C3
}
