library(componentSkeleton)
library(Vennerable)

## Fix a compatibility problem with Vennerable 2.0 with R 2.13 and following.
if(installed.packages()["Vennerable","Version"] == "2.0"& as.numeric(version$minor) >= 13) {
    source("compute.Venn.fix.r")
}

## Constants
SUB.FIG.HEIGHT <- 2.5
SUB.FIG.WIDTH <- 2.5
ALLOWED.TYPES <- c('circles', 'squares', 'triangles', 'ellipses', 'AWFE', 'ChowRuskey', 'battle')
## Vennerable package cannot handle empty sets.
EMPTY.SETS.ERROR.MESSAGE <- "NO VENN DIAGRAM DRAWN\n\nFollowing sets are empty:\n"

execute <- function(cf) {
    ## Read in input and parameters.
    sets.input <- SetList.read(get.input(cf, 'sets'))
    sets.venn <- c(get.parameter(cf, 'sets1'), get.parameter(cf, 'sets2'), get.parameter(cf, 'sets3'), get.parameter(cf, 'sets4'))   
    set.names.venn <- c(get.parameter(cf, 'names1'), get.parameter(cf, 'names2'), get.parameter(cf, 'names3'), get.parameter(cf, 'names4'))
    section.title <- get.parameter(cf, 'sectionTitle')
    section.type <- get.parameter(cf, 'sectionType')
    cex.set.name <- as.numeric(get.parameter(cf, 'cexSetName'))
    cex.set.size <- as.numeric(get.parameter(cf, 'cexSetSize'))
    color.algorithm <- get.parameter(cf, 'colorAlgorithm', allowed.values=c('', 'signature', 'binary', 'sequential'))
    types <- get.parameter(cf, 'types', )
    do.weights <- get.parameter(cf, 'doWeights', type='boolean')
    increasing.line.width <- get.parameter(cf, 'increasingLineWidth', type='boolean')
    
    ## Parse sets, set names and types from parameters.
    set.names.venn <- strsplit(set.names.venn, ',')
    sets.venn <- strsplit(sets.venn, ',')
    types <- strsplit(types, ',')[[1]]
    
    ## Check for the validity of parameters sets, set names.
    for(i in 1:length(sets.venn)) {
        n.sets <- length(sets.venn[[i]])
        if(n.sets == 0) next
        n.names <- length(set.names.venn[[i]])

        ## Check that 2-9 sets are given to every Venn diagram.
        if(n.sets < 2 || n.sets > 9) {
            write.error(cf, sprintf("Invalid parameter value: sets%d. There can be 2 to 9 sets in one Venn diagram. Now sets%d has %d set(s): %s.", i, i, n.sets, sets.venn[[i]]))
            return(PARAMETER_ERROR)
        }

        ## There has to be as many names (if given!) as sets.
        if(n.sets != n.names && n.names != 0) {
            write.error(cf, sprintf("Invalid parameter values: sets%d and names%d. There has to be equal number of sets and names (if given) for the corresponding parameters. Now sets%d has %d set(s) (%s) and names%d has %d name(s) (%s).", i, i, i, n.sets, paste(sets.venn[[i]], collapse=","), i, n.names, paste(set.names.venn[[i]], collapse=",")))            
            return(PARAMETER_ERROR)
        }
        ## Check that the sets are found in the SetList input.
        for(set.id in sets.venn[[i]]) {
            if(is.null(SetList.get.members(sets.input, set.id))) {
                write.error(cf, sprintf("Invalid parameter value: sets%d. Set %s is not found in 'sets' input.", i, set.id))            
                return(PARAMETER_ERROR)
            }
        }
    }

    ## Check the validity of types.
    for(type in types) {
        if(!type %in% ALLOWED.TYPES) {
            write.error(cf, sprintf('Invalid parameter value: types. Type %s is not recognized.', type))            
                return(PARAMETER_ERROR)
        }
    }
    
    ## Create output folder for the component.
    output.dir <- get.output(cf, 'report')
    dir.create(output.dir, recursive=TRUE)

    ## Calculate sets in Venn diagrams.
    sets.output.file <- get.output(cf, 'sets') 
    sets.input <- remove.na.values(sets.input)
    venn.list <- list()
    for(sets.for.venn in sets.venn) {
        if(length(sets.for.venn) != 0) {
            venn.list <- c(venn.list, get.venn(sets.for.venn, sets.input))
        }
    }

    ## Create latex code.
    tex <- character()
    if(section.title != "") {
        tex <- sprintf('\\%s{%s}', section.type, section.title)
    }
    tex <- c(tex, create.venn.diagram(sets.input, sets.venn, venn.list, set.names.venn, cex.set.name, cex.set.size, get.metadata(cf, 'instanceName'), color.algorithm, types, do.weights, increasing.line.width, output.dir))
    latex.write.main(cf, 'report', tex)
    
    ## Write sets output.
    write.sets.output(venn.list, sets.output.file)
    return(0)
}

get.venn <- function(sets.venn, set.list) {
    input <- lapply(sets.venn, FUN= function(x) SetList.get.members(set.list, x))
    names(input) <- sets.venn
    ## No Venn diagram can be drawn if one of the sets is empty.
    if(all(lapply(input, length) > 0)) {
        venn <- Venn(input)
    }else {
        venn <- NA
    }
    return(venn)
}


create.venn.diagram <- function(sets.input, sets.venn.list, venn.list, set.names, cex.set.name, cex.set.size, instance.name, color.algorithm, types, do.weights, increasing.line.width, output.dir) {
    ## Draw diagrams
    tex <- character()
    tex <- c(tex, '\\begin{figure}[!ht]\\centering')
    fig.caption <- "Intersections of sets:"

    for(i in 1:length(venn.list)) {
        out.filename <- sprintf('VennDiagram-%s-sets%d.pdf', instance.name, i)
        out.file <- file.path(output.dir, out.filename)

        ## Sets in the Venn diagram.
        sets.venn <- sets.venn.list[[i]]
        
        pdf(file=out.file, paper='special', width=SUB.FIG.WIDTH, height=SUB.FIG.HEIGHT)
        
        ## If names were given for the sets use them.
        set.names.venn <- sets.venn
        if(length(set.names[[i]]) > 0) {
            set.names.venn <- set.names[[i]]
        }

        if(class(venn.list[[i]]) != "Venn") {
            old.par <- par()
            par(mar=c(0,0,0,0))
            plot(-1:1, -1:1, type = "n", axes=FALSE, ann=FALSE)
            set.members <- lapply(sets.venn.list[[i]], FUN= function(x) SetList.get.members(sets.input, x))
            empty.sets <- sets.venn.list[[i]][lapply(set.members, length) == 0]
            text(0,0, paste(EMPTY.SETS.ERROR.MESSAGE, paste(empty.sets, collapse="\n"), sep=""))
            par(old.par)
            dev.off()
        }else {
            type.index <- i %% length(types)
            if(type.index == 0) type.index <- length(types)
            type <- types[type.index]
            venn.diagram <- compute.Venn(venn.list[[i]], doWeights=do.weights, type=type)
            set.labels <- VennGetSetLabels(venn.diagram)
            set.labels$Label[match(sets.venn, set.labels$Label)] <- set.names.venn
            venn.diagram <- VennSetSetLabels(venn.diagram, set.labels)
            color.faces <- FALSE
            
            gpList <- NULL
            if(color.algorithm != "") {
                color.faces <- TRUE            
                gpList <- VennThemes(venn.diagram, colourAlgorithm=color.algorithm, increasing.line.width)
            }else {
                gpList <- VennThemes(venn.diagram, increasingLineWidth=increasing.line.width)
            }
            for(j in 1:length(gpList[["SetText"]])) {
                gpList[["SetText"]][[j]]$cex <- cex.set.name
            }
            for(j in 1:length(gpList[["FaceText"]])) {
                gpList[["FaceText"]][[j]]$cex <- cex.set.size
            }
            plot(venn.diagram, gp=gpList, show = list(Universe=F, Faces=color.faces))
            dev.off()
        }    
   
        ## Subfigure caption
        sub.caption <- paste(paste(set.names.venn[-length(set.names.venn)], collapse=", "), set.names.venn[length(set.names.venn)], sep=" and ")

        ## Subfigure label
        sub.label <- sprintf("fig:%s-%d", instance.name, i)
        tex <- c(tex, sprintf('\\subfloat[][]{\\includegraphics[keepaspectratio=true]{%s}\\label{%s}}', 
                out.filename, sub.label))
        if (i == 1 || i == 3) {
            tex <- c(tex, '\\hspace{8pt}')
        }        
        if (i == 2) {
            tex <- c(tex, '\\\\')
        }

        fig.caption <- paste(fig.caption, sprintf("\\protect\\subref{%s} %s",
            sub.label, latex.quote(sub.caption)))
    }
    
    fig.caption <- paste(fig.caption, ".", sep="")
    tex <- c(tex, sprintf('\\caption{%s}\\label{fig:%s}', fig.caption, 
            instance.name), '\\end{figure}')
    return(tex)
}

## Removes "NA" elements from the sets in the input SetList.
remove.na.values <- function(set.list) {
    for(set.id in SetList.get.ids(set.list)) {
        set.members <- SetList.get.members(set.list, set.id)
        set.members <- set.members[set.members != "NA"]
        set.list <- SetList.assign(set.list, set.id, set.members)
    }
    return(set.list)
}

## Writes the Venn diagram sets in to sets output.
write.sets.output <- function(venn.list, sets.output.file) {
    sets.output <- SetList.new()
    for(venn in venn.list) {
        if(class(venn) != "Venn") {
        }else {
            for(set.indicator in rownames(venn@IndicatorWeight)) {                
                set.members <- venn@IntersectionSets[[set.indicator]]
                # Parse set name for output
                set.names.index <- !(colnames(venn@IndicatorWeight) == ".Weight")
                set.names <- colnames(venn@IndicatorWeight)[set.names.index]
                membership.indicator <- venn@IndicatorWeight[set.indicator, set.names]
                output.set.name <- ""                    
                for(set.name in set.names) {
                    if(output.set.name != "")
                            output.set.name <- paste(output.set.name, "_AND_", sep="")
                    if(membership.indicator[set.name] == 0)
                        output.set.name <- paste(output.set.name, "NOT_", sep="")
                    output.set.name <- paste(output.set.name, set.name, sep="")
                }
                if(is.null(set.members)) set.members <- ""
                sets.output <- SetList.assign(sets.output, output.set.name, set.members)
            }
        }
    }
    CSV.write(sets.output.file, sets.output)
}

main(execute)
