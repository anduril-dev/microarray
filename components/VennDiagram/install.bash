#!/bin/bash

[ $UID -ne 0 ] && [ ! -d "$R_LIBS" ] && {
        echo Not run as root. Be sure to setup your R_LIBS folder.
}

echo '
setwd("/tmp/")
if (!require("Vennerable",character.only = TRUE))
{
    install.packages("Vennerable", repos="http://R-Forge.R-project.org")
}
' | R --slave --vanilla 

