#!/bin/sh

for BAD_FUNC in png q quit shell; do
	echo "R components that call $BAD_FUNC:"
	find -iname '*.r' | xargs grep "[^a-zA-Z0-9.]$BAD_FUNC[(]"
done
