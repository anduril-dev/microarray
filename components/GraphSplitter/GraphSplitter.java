import java.io.File;
import java.util.LinkedList;

import edu.uci.ics.jung.graph.Graph;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

import fi.helsinki.ltdk.csbl.javatools.graphMetrics.GraphML;
import fi.helsinki.ltdk.csbl.javatools.graphMetrics.SplitGraph;


public class GraphSplitter extends SkeletonComponent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	GraphSplitter().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		File	arrayDir=cf.getOutput("array");
		IndexFile	Idx=new	IndexFile();
		String	splitType=cf.getParameter("splitType");
		int	i=0;
		
		int	minimalSizeFilter=Integer.valueOf(cf.getParameter("filterLessThan"));
		arrayDir.mkdirs();
		GraphML reader = new GraphML(cf.getInput("graph"));
		reader.loadGraph();
		Graph<String,String> graph = reader.getGraph();
		LinkedList<Graph<String,String>>	graphs = null;
		if(splitType.equalsIgnoreCase("connected"))
			graphs=SplitGraph.conComponents(graph);
		if(splitType.equalsIgnoreCase("strongly"))
			graphs=SplitGraph.strongConComponents(graph);
		for(Graph<String,String>	subGraph:graphs)
		{
			if(subGraph.getVertexCount()>=minimalSizeFilter)
			{
				String	subGraphName="subgraph"+String.format("%04d", i++);
				File	subGraphFile=new	File(arrayDir.getAbsolutePath()+File.separator+subGraphName+".graphml");
				reader.saveGraph(subGraph, subGraphFile);
				
				Idx.add(subGraphName, new File(subGraphName+".graphml"));
			}
		}
		Idx.write(cf, "array");
		return ErrorCode.OK;
	}

}
