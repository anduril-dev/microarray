library(componentSkeleton)
library(igraph)

execute <- function(cf) {
    section.title <- get.parameter(cf, 'sectionTitle')
    section.type <- get.parameter(cf, 'sectionType')

    groups <- SampleGroupTable.read(get.input(cf, 'groups'))
    if (input.defined(cf, 'idlist')) {
        idlist <- SetList.read(get.input(cf, 'idlist'))
    } else {
        idlist <- NULL
    }

    out.dir <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)

    ## Create LaTeX code
    tex <- character()
    if (section.title != "") {
        tex <- sprintf('\\%s{%s}\n\\label{%s}',
                       section.type,
                       section.title,
                       get.metadata(cf, 'instanceName'))
    }    
    
    tex <- c(tex, create.group.table(groups,
                                     get.parameter(cf,'definWidth','float'),
                                     get.parameter(cf,'descWidth' ,'float')))
 
    if (!is.null(idlist)) tex <- c(tex, create.lists.table(idlist))
 
    latex.write.main(cf, 'report', tex)
    create.graph(cf, groups, idlist)
 
    return(0)
}

create.group.table <- function(groups, definWidth, descWidth) {
    group.ids <- SampleGroupTable.get.groups(groups)
    fr <- data.frame(Group=group.ids, Definition='', Description='', stringsAsFactors=FALSE)

    for (i in 1:length(group.ids)) {
        group.id <- group.ids[i]

        source.groups <- SampleGroupTable.get.source.groups(groups, group.id)

        type <- SampleGroupTable.get.type(groups, group.id)
        if (type == SAMPLE.GROUP.TABLE.MEDIAN) {
            def <- paste('median(', paste(source.groups, collapse=', '), ')', sep='')
        }
        else if (type == SAMPLE.GROUP.TABLE.RATIO) {
            def <- paste('ratio(', paste(source.groups, collapse='/'), ')', sep='')
        }
        else {
            def <- paste(source.groups, collapse=' ')
        }

        fr[i, 'Definition']  <- def
        fr[i, 'Description'] <- SampleGroupTable.get.description(groups, group.id)
    }

    return(latex.table(fr, sprintf('lp{%fcm}p{%fcm}',definWidth,descWidth), 'Sample groups'))
}

create.lists.table <- function(idlist) {
    fr <- data.frame(Group=NA, ListID=NA, Size=NA)
    for (i in 1:nrow(idlist)) {
        set.id <- idlist$ID[i]
        group.id <- idlist[i, 'SampleGroup']
        fr[i, 'Group'] <- group.id
        fr[i, 'ListID'] <- set.id 
        fr[i, 'Size'] <- length(SetList.get.members(idlist, set.id)) 
    }
    
    fr <- fr[order(fr$Group, fr$ListID),]
    fr$Size <- as.character(as.integer(fr$Size))
    colnames(fr) <- c('Group', 'List ID', 'Size')
    return(latex.table(fr, 'llr', 'ID lists'))
}

create.graph <- function(cf, groups, idlist) {
    g <- graph.empty(directed=TRUE)
    
    add.vertex <- function(group.id, label=NULL, shape=NULL) {
        stopifnot(is.character(group.id))
        if (is.null(label)) label <- group.id
        if (is.null(shape)) shape <- 'ellipse'
        return(add.vertices(g, 1, groupID=group.id, label=label, shape=shape))
    }
    
    # Add nodes
    for (group.id in SampleGroupTable.get.groups(groups)) {
        type <- SampleGroupTable.get.type(groups, group.id)
        if (type %in% SAMPLE.GROUP.TABLE.CENTRAL.MEASURE) {
            shape = 'box'
        }
        else if (type == SAMPLE.GROUP.TABLE.RATIO) {
            shape = 'diamond'
        }
        else {
            shape = 'ellipse'
        }
        label <- sprintf('%s (%s)', group.id, type)
        
        if (!is.null(idlist)) {
            items <- character()
            
            for (i in 1:nrow(idlist)) {
                if (idlist[i, 'SampleGroup'] == group.id) {
                    ids <- SetList.get.members(idlist, idlist$ID[i])
                    items <- c(items, sprintf('%s: %d', idlist$ID[i], length(ids)))
                }
            }
            if (length(items) > 0) {
                label <- paste(c(label, items), collapse='\\n') 
            }
        }
        
        g <- add.vertex(group.id, label, shape)
    }
    
    # Add edges
    for (group.id in SampleGroupTable.get.groups(groups)) {
        target.vertex <- V(g)[groupID == group.id]
        if (length(target.vertex) == 0) {
            g <- add.vertex(group.id)
            target.vertex <- V(g)[groupID == group.id]
        }
        
        source.groups <- SampleGroupTable.get.source.groups(groups, group.id)
        for (source.group in source.groups) {
            if (source.group != group.id) {
                source.vertex <- V(g)[groupID == source.group]
                if (length(source.vertex) == 0) {
                    g <- add.vertex(source.group)
                    source.vertex <- V(g)[groupID == source.group]
                }
                g <- add.edges(g, c(source.vertex, target.vertex))
            }
        }
    }
    
    write.graph(g, get.output(cf, 'graph'), format='graphml')
}

main(execute)
