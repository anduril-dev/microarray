import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Arrays;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;


public class JCSVJoin extends SkeletonComponent {
	
	Hashtable<File,Integer>	keyColumns=null;
	LinkedList<String>	columns;
	String	columnname="";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	JCSVJoin().run(args);
	}
	
	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		LinkedList<File>	csvs=new	LinkedList<File>();
		
		
		String[]	csvKeyColumns={};
		
		columns=new	LinkedList<String>();
		
		if(cf.getBooleanParameter("useKeys"))
		{
			keyColumns=new	Hashtable<File,Integer>();
			
			if(cf.getParameter("keyColumnNames").length()>0)
				csvKeyColumns=AsserUtil.split(cf.getParameter("keyColumnNames"), ",");
		}
		
		
		
		for(int	i=1;i<=8;i++)
		{
			if(cf.getInput("csv"+i)!=null){
				File	file=cf.getInput("csv"+i);
				csvs.add(file);
				CSVParser	csv=new	CSVParser(file);
				String	keycol="";
				if(keyColumns!=null)
				{
					if(i<=csvKeyColumns.length)
						if(csvKeyColumns[i-1].length()>0)
							keyColumns.put(file, csv.getColumnIndex(keycol=csvKeyColumns[i-1]));
						else{
							keyColumns.put(file, 0);
							keycol=csv.getColumnNames()[0];
						}
					else{
						keyColumns.put(file, 0);
						keycol=csv.getColumnNames()[0];
					}	
					if(columnname.length()<=0)
					{
						columnname=keycol;
						columns.add(columnname);
					}
				}
				for(String	colname:csv.getColumnNames())
				{
					if((!colname.equals(keycol))&&(!columns.contains(colname)))
						columns.add(colname);
				}
				csv.close();
			}
		}
		
		if(cf.getInput("csvDir")!=null)
		{
			File	csvDir=cf.getInput("csvDir");
			if(csvDir.isFile())
			{
				cf.getErrorsWriter().write("csvDir is not a directory!");
				return	ErrorCode.INVALID_INPUT;
			}
			csvs.addAll(readDir(csvDir,cf.getParameter("csvDirRegexp"),
					cf.getParameter("csvDirKeyColumnName")));
		}
		if(cf.getInputArrayIndex("array")!=null)
		{
			IndexFile	Idx=IndexFile.read(cf.getInputArrayIndex("array"));
			for(String	key:Idx)
			{
				File	file=Idx.getFile(key);
				csvs.add(file);
				CSVParser	csv=new	CSVParser(file);
				String	keycol="";
				if(keyColumns!=null){
					if(cf.getParameter("arrayKeyColumnName").length()>0)
						keyColumns.put(file, csv.getColumnIndex(keycol=cf.getParameter("arrayKeyColumnName")));
					else{
						keyColumns.put(file, 0);
						keycol=csv.getColumnNames()[0];
					}
					if(columnname.length()<=0)
					{
						columnname=keycol;
						columns.add(columnname);
					}
				}
				csvs.add(file);
				
				for(String	colname:csv.getColumnNames())
				{
					if((!colname.equals(keycol))&&(!columns.contains(colname)))
						columns.add(colname);
				}
				csv.close();
			}
		}
		
		if(keyColumns!=null){
			Hashtable<String,String[]>	table=new	Hashtable<String,String[]>();
			boolean		intersection=false;
			for(File	file:csvs)
			{
				CSVParser	csv=new	CSVParser(file);
				LinkedList<String>	keys2Remove=null;
				if(intersection)
					keys2Remove=new	LinkedList<String>(table.keySet());
				for(String[]	row:csv)
				{
					if(row[keyColumns.get(file)]==null)
						continue;
					String[]	line=table.get(row[keyColumns.get(file)]);
					if(line==null){
						if(!intersection){
							line=new	String[columns.size()];
							table.put(row[keyColumns.get(file)], line);
						}
						else
							continue;
					
					}
					else{
						if(keys2Remove!=null)
							keys2Remove.remove(row[keyColumns.get(file)]);
					}
					for(int	i=0;i<row.length;i++)
					{
						int	idx=columns.indexOf(csv.getColumnNames()[i]);
						if(idx!=-1)
							if(line[idx]==null)
								line[idx]=row[i];
					}
				}
				csv.close();
				if(keys2Remove!=null)
					for(String	key:keys2Remove)
					{
						table.remove(key);
					}
				intersection=cf.getBooleanParameter("intersection");
			}
			CSVWriter	outCSV=new	CSVWriter(columns.toArray(new String[columns.size()]),
					cf.getOutput("csv"),true);
			long	rowcount=0;
			for(String	key:table.keySet())
			{
				String[]	line=table.get(key);
				for(String	item:line)
				{
					outCSV.write(item);
				}
				rowcount++;
			}
			outCSV.flush();
			outCSV.close();
			if(rowcount<cf.getIntParameter("minRows")){
				cf.getErrorsWriter().write("Number of output rows is less than minRows="+
						cf.getParameter("minRows"));
				return	ErrorCode.ERROR;
			}
		}
		else
		{
			CSVWriter	outCSV=new	CSVWriter(columns.toArray(new String[columns.size()]),
					cf.getOutput("csv"),true);
			LinkedList<Long>	hashcodes=new	LinkedList<Long>();
			long	rowcount=0;
			for(File	file:csvs)
			{
				CSVParser	csv=new	CSVParser(file);
				
				for(String[]	row:csv)
				{
					String[]	line=new	String[columns.size()];
					for(int	i=0;i<row.length;i++)
					{
						int	idx=columns.indexOf(csv.getColumnNames()[i]);
						line[idx]=row[i];
					}
					String	str="";
					for(String	item:line)
					{
						str=str+(str.length()>0?"\t":"")+item;
					}
					long	hashcode=str.hashCode();
					if(hashcodes.contains(hashcode))
						continue;
					hashcodes.add(hashcode);
					for(String	item:line)
					{
						outCSV.write(item);
					}
					rowcount++;
				}
			}
			outCSV.flush();
			outCSV.close();
			if(rowcount<cf.getIntParameter("minRows")){
				cf.getErrorsWriter().write("Number of output rows is less than minRows="+
						cf.getParameter("minRows"));
				return	ErrorCode.ERROR;
			}
		}
		
		return ErrorCode.OK;
	}

	private		LinkedList<File>	readDir(File	dir,String	regexp,String	keyColumnName) throws IOException
	{
		LinkedList<File>	csvs=new	LinkedList<File>();
        File[] files = dir.listFiles();
        // Forcing same file order, which is not guaranteed by listFiles()        
        Arrays.sort(files);

		for(File	file:files)
		{
			if(file.isDirectory())
				csvs.addAll(readDir(file,regexp,keyColumnName));
			else
				if(file.getName().matches(regexp)){
					CSVParser	csv=new	CSVParser(file);
					String	keycol="";
					if(keyColumns!=null){
						if(keyColumnName.length()>0)
							keyColumns.put(file, csv.getColumnIndex(keycol=keyColumnName));
						else{
							keyColumns.put(file, 0);
							keycol=csv.getColumnNames()[0];
						}
						if(columnname.length()<=0){
							columnname=keycol;
							columns.add(columnname);
						}
					}
					csvs.add(file);
					
					for(String	colname:csv.getColumnNames())
					{
						if((!colname.equals(keycol))&&(!columns.contains(colname))){
							columns.add(colname);
						}
					}
					csv.close();
				}
		}
		return	csvs;
	}
}
