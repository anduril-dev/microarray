library(componentSkeleton)

MAX.EXPR <- 8

execute <- function(cf) {
    if (input.defined(cf, 'geneAnnotation')) {
        ann.table <- AnnotationTable.read(get.input(cf, 'geneAnnotation'))
        gene.column <- get.parameter(cf, 'geneColumn')
        if (nchar(gene.column) == 0) {
            write.error(cf, 'geneColumn is empty')
            return(PARAMETER_ERROR)
        }
        if (!(gene.column %in% colnames(ann.table))) {
            write.error(cf, sprintf('Gene column %s is not found in geneAnnotation', gene.column))
            return(PARAMETER_ERROR)
        }
    } else {
        ann.table <- NULL
    }
    
    fr <- data.frame(label=character(0), features=integer(0), genes=integer(0), stringsAsFactors=FALSE)
    colnames(fr) <- c('Description', 'Number of features', 'Number of distinct genes')
    
    for (i in 1:MAX.EXPR) {
        input.name <- paste('expr', i, sep='')
        if (!input.defined(cf, input.name)) next
        
        expr <- LogMatrix.read(get.input(cf, input.name))
        min.columns <- get.parameter(cf, paste('minColumns', i, sep=''), 'int')
        expr.title <- get.parameter(cf, paste('title', i, sep=''))
        
        if (min.columns < 0 || min.columns > ncol(expr)) {
            write.error(cf,
                sprintf('minColumns must be in range [0, number of columns]: is %d',
                ncol(expr)))
            return(PARAMETER_ERROR)
        }
        
        good <- sapply(1:nrow(expr),
            function(i) length(which(!is.na(expr[i,]))) >= min.columns)
        expr <- expr[good,,drop=FALSE]
        
        if (!is.null(ann.table) && nrow(expr) > 0) {
            gene.ids <- AnnotationTable.get.vector(ann.table, gene.column, rownames(expr))
            gene.ids <- gene.ids[!is.na(gene.ids)]
            distinct <- length(unique(gene.ids))
        } else {
            distinct <- 0
        }
        
        stopifnot(distinct <= nrow(expr))
        fr[nrow(fr)+1,] <- c(expr.title, nrow(expr), distinct)
    }
    
    # If distinct gene counting was not done, remove the corresponding
    # column from the result.
    if (is.null(ann.table)) {
        fr <- fr[,-3]
        columns <- 'lr'
    } else {
        columns <- 'lrr'
    }
    
    tex <- c('\\subsection{Number of genes and features}',
        latex.table(fr, columns, caption='Number of genes and features'))
    latex.write.main(cf, 'report', tex)
    
    return(0)
}

main(execute)
