library(componentSkeleton)

MAX.EXPR <- 3

format.gene <- function(cf, gene.id, expr.obj.list, annotation.table, groups) {
    gene.col <- get.parameter(cf, 'geneConversionColumn')
    if (nchar(gene.col) > 0) {
        gene.name <- AnnotationTable.get.vector(annotation.table, gene.col, gene.id)
        if (length(gene.name) == 0 || nchar(gene.name) == 0) {
            gene.name <- gene.id
        }
    } else {
        gene.name <- gene.id
    }
    
    tex <- c(sprintf('\\subsection{Gene: %s}', latex.quote(gene.name)), '')
    
    if (!is.null(annotation.table)) {
        ann.columns <- split.trim(get.parameter(cf, 'annotationColumns'), ',')
        for (ann.col in ann.columns) {
            val <- AnnotationTable.get.vector(annotation.table, ann.col, gene.id)
            if (length(val) == 0) next
            tex <- c(tex, sprintf('%s: %s\\\\', ann.col, latex.quote(val)))
        }
        tex <- c(tex, '')
    }
    
    expr.fr <- data.frame(Sample=NA, Expression=NA, Position=NA)
    colnames(expr.fr) <- c('Sample', 'Expression', 'Expression rank')
    
    for (i in 1:length(expr.obj.list)) {
        expr <- expr.obj.list[[i]]
        if (is.null(expr) || nrow(expr) == 0) next
        expr.columns <- split.trim(get.parameter(cf, 'exprColumns'), ',')
        if (identical(expr.columns, '*')) expr.columns <- colnames(expr)
        expr.columns <- intersect(expr.columns, colnames(expr))
            
        sub.expr <- expr[rownames(expr) == gene.id,,drop=FALSE]
        if (nrow(sub.expr) == 0) next
        for (sample.id in expr.columns) {
            desc <- sample.id
            if (!is.null(groups)) {
                group.desc <- SampleGroupTable.get.description(groups, sample.id)
                if (!is.null(group.desc)) desc <- sprintf('%s (%s)', group.desc, sample.id)
            }
            
            expr.val <- sub.expr[1, colnames(sub.expr) == sample.id]
            if (length(expr.val) == 0 || is.na(expr.val)) {
                expr.val <- NA
                expr.rank <- 'NA'
            } else {
                this.expr <- expr[,sample.id]
                this.expr <- this.expr[!is.na(this.expr)]
                expr.rank <- sprintf('%.0f%%',
                    100*length(which(this.expr < expr.val)) / length(this.expr))
            }
            expr.fr[nrow(expr.fr)+1,] <- c(
                desc,
                sprintf('%.2f', 2**expr.val),
                expr.rank)
        }
    }
    
    expr.fr <- expr.fr[!is.na(expr.fr[,1]),]
    
    caption <- 'Gene expression.'
    caption <- paste(caption, 
        'Expression rank gives the number of genes whose',
        'expression is lower than the given gene.')
    tex <- c(tex,
        latex.table(expr.fr, 'lrl', caption),
        '\\clearpage')
    
    return(tex)
}

execute <- function(cf) {
    gene.ids <- split.trim(get.parameter(cf, 'geneIDs'), ',')
    
    tex <- '\\section{Gene info}'

    if (input.defined(cf, 'geneAnnotation')) {
        annotation.table <- AnnotationTable.read(get.input(cf, 'geneAnnotation'))
    } else {
        annotation.table <- NULL
    }
    
    if (input.defined(cf, 'groups')) {
        groups <- SampleGroupTable.read(get.input(cf, 'groups'))
    } else {
        groups <- NULL
    }

    expr.list <- list()    
    for (i in 1:MAX.EXPR) {
        input.name <- paste('expr', i, sep='')
        if (!input.defined(cf, input.name)) next
        expr.list[[length(expr.list)+1]] <- LogMatrix.read(get.input(cf, input.name))
    }
    
    for (gene.id in gene.ids) {
        tex <- c(tex,
            format.gene(cf, gene.id, expr.list, annotation.table, groups))
    }
    latex.write.main(cf, 'report', tex)
    return(0)
}

main(execute)
