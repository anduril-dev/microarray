library(componentSkeleton)
require(sm)

MAX.INPUTS <- 3

create.boxplot <- function(cf, index, width.inches, height.inches, dataPoints, drawLegend) {
    input.name <- paste('matr', index, sep='')
    if (!input.defined(cf, input.name)) return(c())
    
    write.log(cf, sprintf('Reading %s', get.input(cf, input.name)))
    matr <- Matrix.read(get.input(cf, input.name))
    write.log(cf, sprintf('%s: %d x %d matrix', input.name, nrow(matr), ncol(matr)))

    plot.title           <- get.parameter(cf, paste('title', index, sep=''))
    plot.caption         <- get.parameter(cf, paste('caption', index, sep=''))
    plot.ylab            <- get.parameter(cf, paste('ylabel', index, sep=''))
    y.low                <- get.parameter(cf, 'yLow', 'float')
    y.high               <- get.parameter(cf, 'yHigh', 'float')
    plot.xvertical       <- get.parameter(cf, 'xvertical')
    plot.plottype        <- get.parameter(cf, 'plotType')
    plot.pngImage        <- get.parameter(cf, 'pngImage','boolean')
    plot.dpcm            <- get.parameter(cf, 'dpcm','float')
    plot.violinwidth     <- get.parameter(cf, 'violinWidth', 'float')
    plot.violinlinewidth <- get.parameter(cf, 'violinLineWidth', 'float')
    sample.titles        <- colnames(matr)
    
    items <- list()
    viollist <- vector("list",ncol(matr)+1)
    viollist[[1]]=vioplot2
    weights <- vector("numeric",ncol(matr))
    for (column in 1:ncol(matr)) {
        items[[column]] <- matr[,column]
        viollist[[column+1]] <- na.omit(matr[,column])
        weights[column] <- length(viollist[[column+1]])
    }
    weights <- plot.violinwidth*weights/max(weights)  
    if (plot.pngImage) {
        outname      <- sprintf('BoxPlot-%s-plot%d.png', get.metadata(cf, 'instanceName'), index)
    } else {
        outname      <- sprintf('BoxPlot-%s-plot%d.pdf', get.metadata(cf, 'instanceName'), index)
    }
    out.filename <- file.path(get.output(cf, 'report'), outname) 
    
    if (plot.pngImage) {
        aspect        <- width.inches / height.inches
        width.pixels <- as.integer(round(width.inches*2.54*plot.dpcm))
	height.pixels <- as.integer(round(width.pixels / aspect))
	png.open(out.filename, width=width.pixels, height=height.pixels)
    } else {
        pdf(out.filename, paper='special', width=width.inches, height=height.inches)
    }
    if (plot.xvertical) par(pch=20, cex=0.7, las=2) else par(pch=20, cex=0.7, las=0)
    ylim=NULL
    if (y.low!=y.high) {
        ylim <- c(y.low, y.high)
    }
    # User given free par()
    eval(parse(text=paste("par(",get.parameter(cf, 'plotPar'),")")))
    if (identical(plot.plottype, 'boxplot')) {
        boxplot(items,
                names   = sample.titles,
                main    = plot.title,
                outline = get.parameter(cf, 'outline','boolean'),
                range   = 0,
                ylab    = plot.ylab,
                ylim    = ylim)
    } else
    if (identical(plot.plottype, 'violin')) {
        viollist[['names']] <- sample.titles
        viollist[['wex']]   <- weights
        viollist[['ylab']]  <- plot.ylab
        viollist[['main']]  <- plot.title
        viollist[['lwd']]   <- plot.violinlinewidth
        viollist[['ylim']]  <- ylim
        mode(viollist)      <- 'call'
        eval(viollist)
    } else
    stop('Unknown plot type: ', plot.plottype)
    
    if(!is.na(dataPoints))
    {
		if(drawLegend)
		{
			if(length(dataPoints[1,])>1)
			{
				legend('topleft', c(as.character(dataPoints[,2])),text.col=2:(nrow(dataPoints)+1))
			}
		}
    	for (dataPoint in 1:nrow(dataPoints))
    	{
    		for(x in 1:ncol(matr))
    		{
#    			print(c(as.character(dataPoints[dataPoint,1]),":",x,"\n"))
    			points(x=c(x-0.2, x+0.2), y=rep(matr[as.character(dataPoints[dataPoint,1]),x],times=2), col=dataPoint+1, type='l', lwd=1, pch=1, lty=1)
    			if(!drawLegend)
    			{
    				if(length(dataPoints[dataPoint,])>1)
    				{
    					text(x=c(x-0.1),y=c(matr[as.character(dataPoints[dataPoint,1]),x]),
    						labels=c(as.character(dataPoints[dataPoint,2])),
    						col=dataPoint+1,
    						pos=3
    						)
    				}
    			}
    		}
    	}
    }
    
    if (plot.pngImage) {
        png.close()
    } else {
        dev.off()
    }
    caption <- paste(if (plot.title=='') plot.title else sprintf('%s. ',plot.title),
            if (plot.caption!='') sprintf('%s ',plot.caption),
            'The bold line shows the location of median. ',
            "The filled rectangle contains values between 25'th and 75'th percentile. ",
            'The extremes show locations of minimum and maximum.', sep='')    

    tex <- latex.figure(outname,
                        caption   = caption,
                        fig.label = sprintf('fig:%s_%d', get.metadata(cf, 'instanceName'), index))
    return(tex)
}

execute <- function(cf) {
    width.inches  <- get.parameter(cf,  'width', 'float') / 2.54
    height.inches <- get.parameter(cf, 'height', 'float') / 2.54
    
    drawLegend <- get.parameter(cf, "drawLegend", "boolean")
    
    out.dir <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)
    
    tex <- sprintf('\\subsection{Box plots}\\label{sec:%s}', get.metadata(cf, 'instanceName'))
    if (get.parameter(cf, 'pagebreak', 'boolean')) {
       tex <- c('\\clearpage{}', tex)
    }

	
	dataPoints <- NA
	if (input.defined(cf, "dataPoints"))
	{
		dataPoints <- read.csv(get.input(cf, "dataPoints"),,sep="\t")
	}

    for (i in 1:MAX.INPUTS) {
        tex <- c(tex, create.boxplot(cf, i, width.inches, height.inches, dataPoints, drawLegend))
    }

    latex.write.main(cf, 'report', tex)

    return(0)
}

vioplot2 <- function (x, ..., range = 1.5, h = NULL, ylim = NULL, names = NULL, 
    col = "white", border = "darkgray", lty = 1, 
    lwd = 1, rectCol = "black", colMed = "white", pchMed = 19, 
    at, add = FALSE, wex, drawRect = TRUE, ylab = "", main="") 
{
# This function is based on:
# A violin plot is a combination of a box plot and a kernel density plot.
# Version:	0.2
# Depends:	sm
# Published:	2005-10-29
# Author:	Daniel Adler
# Maintainer:	Daniel Adler <dadler at uni-goettingen.de>
# License:	BSD
# URL:	http://wsopuppenkiste.wiso.uni-goettingen.de/~dadler

    datas <- list(x, ...)
    n <- length(datas)
    if (missing(at)) 
        at <- 1:n
    if (missing(wex)) 
        wex <- rep(1,n)
    upper <- vector(mode = "numeric", length = n)
    lower <- vector(mode = "numeric", length = n)
    q1 <- vector(mode = "numeric", length = n)
    q3 <- vector(mode = "numeric", length = n)
    med <- vector(mode = "numeric", length = n)
    base <- vector(mode = "list", length = n)
    height <- vector(mode = "list", length = n)
    baserange <- c(Inf, -Inf)
    args <- list(display = "none")
    if (!(is.null(h))) 
        args <- c(args, h = h)
    for (i in 1:n) {
        data <- datas[[i]]
        data.min <- min(data)
        data.max <- max(data)
        q1[i] <- quantile(data, 0.25)
        q3[i] <- quantile(data, 0.75)
        med[i] <- median(data)
        iqd <- q3[i] - q1[i]
        upper[i] <- min(q3[i] + range * iqd, data.max)
        lower[i] <- max(q1[i] - range * iqd, data.min)
        est.xlim <- c(min(lower[i], data.min), max(upper[i], 
            data.max))
        smout <- do.call("sm.density", c(list(data, xlim = est.xlim), 
            args))
        hscale <- 0.4/max(smout$estimate) * wex[i]
        base[[i]] <- smout$eval.points
        height[[i]] <- smout$estimate * hscale
        t <- range(base[[i]])
        baserange[1] <- min(baserange[1], t[1])
        baserange[2] <- max(baserange[2], t[2])
    }
    if (!add) {
        xlim <- if (n == 1) 
            at + c(-0.5, 0.5)
        else range(at) + min(diff(at))/2 * c(-1, 1)
        if (is.null(ylim)) {
            ylim <- baserange
        }
    }
    if (is.null(names)) {
        label <- 1:n
    }
    else {
        label <- names
    }
    boxwidth <- 0.07 #* max(wex[i])
    if (!add) 
        plot.new()
    if (!add) {
            plot.window( xlim = xlim, ylim = ylim)
            axis(2)
            axis(1, at = at, label = label)
            title(main=main,ylab=ylab)
    }
    box()
        for (i in 1:n) {
            polygon(c(at[i] - height[[i]], rev(at[i] + height[[i]])), 
                c(base[[i]], rev(base[[i]])), col = col, border = border, 
                lty = lty, lwd = lwd)
            if (drawRect) {
                lines(at[c(i, i)], c(lower[i], upper[i]), lwd = lwd, 
                  lty = lty)
                rect(at[i] - boxwidth/2, q1[i], at[i] + boxwidth/2, 
                  q3[i], col = rectCol)
#                points(at[i], med[i], pch = pchMed, col = colMed)
               lines(c(at[i]-2*boxwidth, at[i]+2*boxwidth), c(med[i], med[i]), lwd = lwd, 
                  lty = lty)
            }
        }
   
    invisible(list(upper = upper, lower = lower, median = med, 
        q1 = q1, q3 = q3))
}

main(execute)
