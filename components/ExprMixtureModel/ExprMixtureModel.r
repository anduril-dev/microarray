library(componentSkeleton)
library('mixtools')

set.seed(12345) # Deterministic behaviour

# Mutual information score
mi.score <- function(p1, g1, g2) {
  n <- length(p1)
  if (n <= 1) return(NA)

  n1  <- sum(g1)
  n2  <- sum(g2)
  pX  <- c(n1, n2)/n
  pY  <- c(sum(p1), sum(1-p1))/n
  pXY <- rbind(
    c(sum(p1[g1]), sum(1-p1[g1])),
    c(sum(p1[g2]), sum(1-p1[g2]))
  )/n

  m <- 0
  for (x in 1:2) {
    for (y in 1:2) {
      if (pXY[x,y] > 0) {
         m <- m + (pXY[x,y]*log(pXY[x,y]/(pX[x]*pY[y])))
      }
    }
  }

  # Normalization
  hX <- -pX[1]*log(pX[1])-pX[2]*log(pX[2])
  #hY <- -pY[1]*log(pY[1])-pY[2]*log(pY[2])
  m  <- m/hX
  return(m)
}

overlap.err <- function(mu1, mu2, sigma1, sigma2, lambda1, lambda2) {
  integrate(function(x) {
              pmin(lambda1*dnorm(x, mean=mu1, sd=sigma1),
                   lambda2*dnorm(x, mean=mu2, sd=sigma2))
            },
            lower=-Inf, upper=Inf,
            stop.on.error=FALSE)
}

execute <- function(cf) {
    MAX_ITER      <- 2000
    instance.name <- get.metadata(cf, 'instanceName')
    samplesFile   <- get.output(cf, 'samplesets')
    exprT         <- CSV.read(get.input(cf, 'expr'))
    limitMinProp  <- get.parameter(cf, 'minProportion', 'float')
    limitMinDiff  <- get.parameter(cf, 'minDifference', 'float')
    limitMinInfo  <- get.parameter(cf, 'minInfo',       'float')
    limitMaxLogl  <- get.parameter(cf, 'maxLoglik',     'float')
    limitMaxOverl <- get.parameter(cf, 'maxOverlap',    'float')
    l             <- get.parameter(cf, 'sdPoint', 'float')
    csplit        <- function(x){unlist(strsplit(x=x, split=',', fixed=TRUE))}
    printFails    <- get.parameter(cf, 'showIfFail', 'boolean')

    section <- get.parameter(cf, 'sectionTitle')
    if (nchar(section) > 0) {
        section <- c(sprintf('\\%s{%s}', get.parameter(cf, 'sectionType'), section),
                     sprintf('\\label{%s}', instance.name))
    }

    if (input.defined(cf, 'annot')) {
       annotCols <- csplit(get.parameter(cf, 'annotCols', 'string'))
       annot     <- CSV.read(get.input(cf, 'annot'))
       annot     <- annot[match(exprT[,1],annot[,1]),annotCols,drop=FALSE]
    } else {
       annot <- NULL
    }

    if (input.defined(cf, 'groups')) {
       groupT <- CSV.read(get.input(cf, 'groups'))
       grp1   <- get.parameter(cf, 'group1', 'string')
       grp2   <- get.parameter(cf, 'group2', 'string')
       grp1   <- if (grp1 == "") { 1 } else { groupT[,'ID']==grp1 }
       grp2   <- if (grp2 == "") { 2 } else { groupT[,'ID']==grp2 }
       grp1t  <- sprintf( 'Blue ticks on x-axis represent %s samples.', latex.quote(groupT[grp1,'ID']))
       grp2t  <- sprintf('Black ticks on x-axis represent %s samples.', latex.quote(groupT[grp2,'ID']))
       grp1   <- csplit(groupT[grp1,'Members'])
       grp2   <- csplit(groupT[grp2,'Members'])
       grp1   <- (colnames(exprT)[-1] %in% grp1)
       grp2   <- (colnames(exprT)[-1] %in% grp2)
       rm(groupT)
    } else {
       grp1  <- NULL
       grp2  <- NULL
       grp1t <- ''
       grp2t <- ''
    }
    use.mi <- (!is.null(grp1) & !is.null(grp2))

    document.dir <- get.output(cf, 'report')
    dir.create(document.dir, recursive=TRUE)

    stats        <- array(dim=c(nrow(exprT),if(use.mi){12}else{11}))
    stats[,1]    <- exprT[,1]
    stats[,10]   <- 0
    document.out <- list()
    ic           <- 0
    figS         <- '\\begin{figure}[htb]'
    figE         <- '\\end{figure}\\FloatBarrier{}\n'
    figGap       <- '\\hspace{0.25cm}'
    document.out[1] <- figS
    cat('sample','\t',paste(colnames(exprT)[-1],collapse='\t'),'\n',sep='',file=samplesFile)
    for (g in 1:nrow(exprT)) {
      data     <- as.numeric(exprT[g,-1])
      naFilter <- !is.na(data)
      data     <- data[naFilter]
      fname    <- sprintf('%s-exprDist%i.pdf', instance.name, g)
      plotname <- file.path(document.dir, fname)

      nScore <- try(shapiro.test(data))
      if (class(nScore) == "try-error") {
         nScore$p.value <- NA
      }

      modelFit <- try( normalmixEM(data, lambda=c(0.8,0.2), k=2, maxit=MAX_ITER, epsilon=1e-7) )
      if (class(modelFit) == "try-error") {
         errMsg <- as.character(modelFit)
         errMsg <- trim(substr(errMsg, regexpr(":", errMsg)[1]+1, nchar(errMsg)))
         write.log(cf, sprintf('Could not process %s: %s', exprT[g,1], errMsg))
         m               <- median(data)
         d               <- sd(data)
         modelFit$mu     <- rep(m,2)
         modelFit$sigma  <- rep(d,2)
         modelFit$lambda <- c(1,0)
         modelFit$loglik <- NA
      } else {
         if (length(modelFit$all.loglik) > MAX_ITER) {
            errMsg <- 'not convergent'
         } else {
            errMsg <- NULL
         }
      }

      if (use.mi) {
         mi <- mi.score(modelFit$posterior[,1],
                        grp1[naFilter],
                        grp2[naFilter])
         stats[g,12] <- mi
      } else {
         mi <- NA
      }

      a <- if (modelFit$mu[1] < modelFit$mu[2]) 1:2 else 2:1
      stats[g,2:9] <- c(
        nScore$p.value,
        modelFit$loglik,
        modelFit$mu[a], modelFit$sigma[a], modelFit$lambda[a]
      )

      if (is.null(errMsg)) {
         oErr <- overlap.err(modelFit$mu[1],     modelFit$mu[2],
                             modelFit$sigma[1],  modelFit$sigma[2],
                             modelFit$lambda[1], modelFit$lambda[2])
         if (oErr$message == 'OK') {
            stats[g,11] <- oErr$value
            if (oErr$value > limitMaxOverl) next
         }
      }

      if ((min(modelFit$lambda) < limitMinProp) ||
          (modelFit$loglik > limitMaxLogl) ||
          (abs(modelFit$mu[1]-modelFit$mu[2]) < (limitMinDiff*min(modelFit$sigma))) ||
          (use.mi & (is.na(mi) | (mi < limitMinInfo)))) {
         if (printFails && !is.null(errMsg)) {
             # No model fit but shall be plotted
         } else {
             next
         }
      } else {
         stats[g,10] <- 1
      }

      if (is.null(errMsg)) {
         sP           <- rep(NA, length(naFilter))
         sP[naFilter] <- modelFit$posterior[,a[1]]
         cat(exprT[g,1],'\t',paste(sP,collapse='\t'),'\n',sep='',append=TRUE,file=samplesFile)
      }

      if (is.null(annot)) {
         gAnnot <- ''
      } else {
         gAnnot <- latex.quote(paste(colnames(annot), annot[g,],sep=': ',collapse=', '))
         gAnnot <- paste(gAnnot,', ',sep='')
      }

      pdf(plotname, width=7, height=6)
      if (is.null(errMsg)) {
         plot(modelFit, whichplots=2, main2=exprT[g,1], xlab2='expression signal')
         fitLegend <- sprintf('loglik: %g, overlap: %.4g', modelFit$loglik, oErr$value)
         if (!is.na(mi)) fitLegend <- sprintf('%s, information: %g', fitLegend, mi)
      } else {
         hist(data, freq=FALSE, main=exprT[g,1], xlab='expression signal')
         fitLegend <- sprintf('Model fit failed: \\textcolor{red}{%s}', errMsg)
      }
      m <- modelFit$mu[1]
      s <- modelFit$lambda[1]
      d <- modelFit$sigma[1]
      lines(rep(m,2), c(0,s*dnorm(m, mean=m, sd=d)), col='red')
      points(c(m-l*d, m+l*d), s*c(dnorm(m-l*d, mean=m, sd=d), dnorm(m+l*d, mean=m, sd=d)), col='red')
      m <- modelFit$mu[2]
      s <- modelFit$lambda[2]
      d <- modelFit$sigma[2]
      lines(rep(m,2), c(0,s*dnorm(m, mean=m, sd=d)), col='green')
      points(c(m-l*d, m+l*d), s*c(dnorm(m-l*d, mean=m, sd=d), dnorm(m+l*d, mean=m, sd=d)), col='green')
      m <- median(data)
      d <- sd(data)
      curve(dnorm(x, mean=m, sd=d), add=TRUE, lty=2)
      lines(rep(m,2), c(0,dnorm(m, mean=m, sd=d)), lty=2)
      points(c(m-l*d, m+l*d), c(dnorm(m-l*d, mean=m, sd=d), dnorm(m+l*d, mean=m, sd=d)))
      if (!is.null(grp2)) rug(data[grp2 & naFilter], ticksize=0.10, col='black')
      if (!is.null(grp1)) rug(data[grp1 & naFilter], ticksize=0.05, col='blue')
      invisible(dev.off())

      if (is.na(nScore$p.value)) {
         fitLegend <- paste('normality test failed,', fitLegend)
      } else {
         fitLegend <- paste(sprintf('%s: %.4e,', nScore$method, nScore$p.value),
                            fitLegend)
      }

      ic   <- ic+1
      fInd <- ic*2+1
      document.out[fInd] <-
        sprintf('\\subfloat[\\tiny{%sn: %i, %s}]{\\includegraphics[keepaspectratio=true,width=9cm]{%s}\\hyperdef{MixtureModel}{%s-%s}{}}',
                gAnnot, length(data), fitLegend, fname, instance.name, exprT[g,1])
      if ((ic+2)%%6 == 0) {
         document.out[fInd+1] <- paste(figE, figS, figGap)
      } else
      if (ic%%2 == 0) {
         document.out[fInd+1] <- '\\\\'
      } else
         document.out[fInd+1] <- figGap
    }
    rm(exprT)

    # Print statistics output
    stats <- as.data.frame(stats, stringsAsFactors=FALSE)
    colnames(stats)[1:11] <- c('gene','normality','loglik','m1mean','m2mean','m1sd','m2sd','m1prop','m2prop','selected','overlap')
    if (use.mi) colnames(stats)[12] <- 'mi'
    CSV.write(get.output(cf, 'stats'), stats)

    statPlotsDoc        <- '\\textbf{Summary statistics:}\\\\'
    stats[,'m2prop']    <- apply(stats[,8:9], MARGIN=1, FUN=min)
    stats[,'normality'] <- log(as.numeric(stats[,'normality']))
    colnames(stats)[9]  <- 'proportion'
    spArgs <- rbind(
      c('normality',  'log(normality) test scores', NA),
      c('loglik',     'model fit scores',           limitMaxLogl),
      c('proportion', 'minimum proportion',         limitMinProp),
      c('overlap',    'component overlaps',         limitMaxOverl),
      c('mi',         'mutual information',         limitMinInfo)
    )
    fInd <- 0
    for (col in intersect(spArgs[,1], colnames(stats))) {
      cArgs    <- spArgs[spArgs[,1]==col,]
      fname    <- sprintf('%s-%sDist.pdf', instance.name, col)
      plotname <- file.path(document.dir, fname)
      data     <- stats[,col]
      data     <- as.numeric(data[!is.na(data)])

      pdf(plotname, width=10, height=6)
      hist(data, freq=FALSE, main=sprintf('Distribution of %s', cArgs[2]), xlab=col)
      if (!is.na(cArgs[3])) abline(v=cArgs[3], col='red')
      invisible(dev.off())
      statPlotsDoc <- c(statPlotsDoc,
                        sprintf('\\includegraphics[keepaspectratio=true,width=9cm]{%s}%s',
                                fname, if((fInd<-fInd+1) %% 2==0){'\n'}else{''}))
    }
    rm(stats)

    # Prepare the output document
    document.out <- c(
       section,
       sprintf(paste('Total of %i genes are selected having an appropriate mixture model of two normal distributions.',
                     'This means that the log likelihood for the mixture model is below %g.',
                     'The minimum support of either normal distribution is %g and their means are at least %g times',
                     'the standard deviation of the less deviating distribution apart from each other.',
                     'Maximum overlap between the distributions is %g.\n'),
               ic, limitMaxLogl, limitMinProp, limitMinDiff, limitMaxOverl),
       'Below are the visualizations of the gene specific mixture models (red+green) and the expression distributions',
       'Black histogram represent the overall distribution of the expression and the black curve represents a normal distribution fit for it.',
       sprintf('For each normal distribution, points are shown at $\\pm{}%g \\cdot{}$standard deviation.', l),
       grp1t, grp2t,
       unlist(document.out),
       figE,
       statPlotsDoc
    )
    latex.write.main(cf, 'report', document.out)

    return(0)
}

main(execute)
