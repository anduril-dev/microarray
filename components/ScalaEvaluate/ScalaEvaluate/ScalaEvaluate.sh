    
source "$ANDURIL_HOME/bash/functions.sh" "$1"
export_command
tmpdir=$( gettempdir )

[[ -f "$input_script" ]] && cp "$input_script" "$tmpdir/script"
echo "$parameter_script" >> "$tmpdir/script"

[[ -s "$tmpdir/script" ]] || {
    writelog "No script defined"
    exit 1
}

for import in java.io.File 'scala.collection.immutable.{SortedMap,TreeMap}' \
              scala.collection.mutable.ArrayBuffer org.anduril.runtime.table._ \
              org.anduril.runtime.table.processor.ColumnStatistics; do
    printf 'import %s\n' "$import"
done > "$output_script"

for t in {1..5}; do
    [[ -f $( getinput table$t ) ]] && {
        printf 'val table%dFile = new File("%s")\n' $t $( getinput table$t ) 
        printf 'val table%d = Table.reader(table%dFile)\n' $t $t
        [[ $t -eq 1 ]] && printf 'import table1._\n' 
    } 
    [[ -f $( getinput table$t ) ]] || {
        printf 'val table%dFile = null\n' $t 
        printf 'val table%d = null\n' $t
    }
done >> "$output_script"

for i in {1..3}; do 
  [[ -e $( getinput var$i ) ]] && {
        printf 'val var%dFile = new File("%s")\n' $i $( getinput var$i ) 
    } || {
        printf 'val var%dFile = null\n' $i 
    }
done >> "$output_script"

for p in {1..5}; do
    printf 'val param%d = "%s"\n' $p $( getparameter param$i )
done >> "$output_script"

printf 'val tableOutFile = new File(\"%s\")\n' $( getoutput table ) >> "$output_script"
def="$parameter_defaultOutput"
[[ -z "$parameter_defaultOutput" ]] && def="null"
printf 'var tableOut: Table = %s\n' "$def" >> "$output_script"

for o in {1..3}; do
    printf 'val optOut%dFile = new File("%s")\n' $o $( getoutput optOut$o )
done >> "$output_script"

echo "{ /* Begin script */" >> "$output_script"
cat "$tmpdir/script" >> "$output_script"
echo "} /* End script */" >> "$output_script"

echo "if (!tableOutFile.exists()) {
    if (tableOut == null) {
	    new java.io.FileOutputStream(tableOutFile).close()
    } else {
	    tableOut.write(tableOutFile, Table.FormatCSV)
    }
}" >> "$output_script"

for o in {1..3}; do
    printf 'if (!optOut%dFile.exists()) {\n  new java.io.FileOutputStream(optOut%dFile).close()\n}\n' $o $o
done >> "$output_script"

BUNDLE_LIB=$( readlink -f ../../lib/java )


cd $( dirname $( readlink -f $output_script ) )
#~ cat "$output_script"
export CLASSPATH=$CLASSPATH:$ANDURIL_HOME/scala/anduril-scala.jar:\
$BUNDLE_LIB/:$BUNDLE_LIB/commons-primitives-1.0.jar:\
$BUNDLE_LIB/commons-math3-3.2.jar
set -x
which scala
scala -unchecked -deprecation -Xlint -cp "$CLASSPATH" $parameter_scalaFlags $output_script
#env
#~ exit 1
#~ 
