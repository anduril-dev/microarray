includeColumns(Seq("A2", "A3", "A4")) // A2 A3 A4
removeColumn("A3")                    // A2 A4
moveColumn("A4", 0)                   // A4 A2
renameColumn("A4", "A4_rename")       // A4_rename A2

filter(r => r("A4_rename") != "y3")   // filter row 3

val new1 = addColumnS("N1",
    (r: Row) =>
        if (r("A4_rename") == null) null
        else r("A4_rename") + "X")

moveColumn(new1, 1)                        // A4 N1 A2
modifyColumnD("A2", (v, r) => v*2)         // multiply A2 by 2
renameColumn(getColumn("N1"), "N1_rename") // A4_rename N1_rename A2
