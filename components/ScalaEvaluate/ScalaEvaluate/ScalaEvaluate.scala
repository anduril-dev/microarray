import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools
import java.io.{BufferedReader,BufferedWriter,File,FileReader,FileWriter}
import java.net.URLClassLoader
import org.anduril.runtime.{ComponentSkeleton,ErrorCode}
import org.anduril.runtime.table.Table
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Map

object ScalaEvaluate extends ComponentSkeleton {
    val NumTableInputs = 5
    val NumExtraParams = 5
    val NumVarInputs = 3
    val NumOptOutputs = 3

    /** Map from input tables (table1..table{NumTableInputs}) to format codes
      * (Table.Format*). */
    private lazy val tableFormats: Map[String,String] = parseTableFormats(param("format"))

    /** Constant for tableFormats to denote any table (default format). */
    private val AnyTable = "*"

    def run(): ErrorCode = {
	if (param("script").isEmpty && !inputDefined("script")) {
            writeError("No script provided")
	    return ErrorCode.PARAMETER_ERROR
	}

	val scriptFile = this.output("script")
	write(scriptFile)
	val status: Int = launch(scriptFile)

	if (status == 0) ErrorCode.OK else ErrorCode.ERROR
    }

    private def parseTableFormats(formatSpec: String): Map[String,String] = {
        val map = Map[String,String]()

        if (formatSpec.contains('=')) {
            map(AnyTable) = Table.FormatCSV
            for (tableSpec <- formatSpec.split(",")) {
                val tokens = tableSpec.split("=", 2)
                val table = tokens(0).trim
                val format = tokens(1).trim
                map(table) = format
            }
        } else {
            map(AnyTable) = if (formatSpec.isEmpty) Table.FormatCSV else formatSpec
        }

        map
    }

    private def getTableFormat(table: String): String = {
        this.tableFormats.getOrElse(
            table,
            this.tableFormats.getOrElse(AnyTable,
            Table.FormatCSV))
    }

    private def write(scriptFile: File) = {
	val writer = new BufferedWriter(new FileWriter(scriptFile))
	writeHeader(writer)
	writeScript(writer)
	writeFooter(writer)
	writer.close()
    }

    private def writeHeader(writer: BufferedWriter) = {
	val imports = Array(
	    "java.io.File",
	    "scala.collection.immutable.{SortedMap,TreeMap}",
            "scala.collection.mutable.ArrayBuffer",
	    "org.anduril.runtime.table._",
	    "org.anduril.runtime.table.processor.ColumnStatistics")

	imports.foreach { p => writer.write("import %s\n".format(p)) }

	for (i <- 1 to NumTableInputs) {
	    val port = "table"+i
	    if (this.inputDefined(port)) {
		val file = this.input(port)
		writer.write("val table%dFile = new File(\"%s\")\n".format(
		    i, file))
		writer.write("val table%d = Table.reader(table%dFile, \"%s\")\n".format(
		    i, i, getTableFormat(port)))
                writer.write("table%d.setName(\"%s\")\n".format(i, port))
		if (i == 1) writer.write("import table%d._\n".format(i))
	    } else {
		writer.write("val table%dFile: File = null\n".format(i))
		writer.write("val table%d: Table = null\n".format(i))
	    }
	}

	for (i <- 1 to NumVarInputs) {
	    val port = "var"+i
	    if (this.inputDefined(port)) {
		writer.write("val var%dFile = new File(\"%s\")\n".format(
		    i, this.input(port)))
	    } else {
		writer.write("val var%dFile: File = null\n".format(i))
	    }
	}

	for (i <- 1 to NumExtraParams) {
	    writer.write("val param%d = \"%s\"\n".format(
		i, this.param("param"+i)))
	}

	writer.write("val tableOutFile = new File(\"%s\")\n".format(this.output("table")))
        val defaultOutput = this.param("defaultOutput")
	writer.write("var tableOut: Table = %s\n".format(
            if (defaultOutput.isEmpty) "null" else defaultOutput))

	for (i <- 1 to NumOptOutputs) {
	    writer.write("val optOut%dFile = new File(\"%s\")\n".format(
		i, this.output("optOut"+i)))
	}
    }

    private def writeScript(writer: BufferedWriter) = {
        writer.write("{ /* Begin script */\n")

	if (this.inputDefined("script")) {
	    val reader = new BufferedReader(new FileReader(this.input("script")))
	    var line = reader.readLine()
	    while (line != null) {
		writer.write(line)
		writer.write('\n')
		line = reader.readLine()
	    }
	    reader.close()
	    writer.write("\n")
	}

	if (!this.param("script").isEmpty) {
	    writer.write(this.param("script"))
	    writer.write('\n')
	}

        writer.write("} /* End script */\n")
    }

    private def writeFooter(writer: BufferedWriter) = {
	writer.write("if (!tableOutFile.exists()) {\n")
	writer.write("  if (tableOut == null) {\n")
	writer.write("    new java.io.FileOutputStream(tableOutFile).close()\n")
	writer.write("  } else {\n")
	writer.write("    tableOut.write(tableOutFile, Table.FormatCSV)\n")
	writer.write("  }\n")
	writer.write("}\n")

	for (i <- 1 to NumOptOutputs) {
	    writer.write("if (!optOut%dFile.exists()) {\n".format(i))
	    writer.write("  new java.io.FileOutputStream(optOut%dFile).close()\n".format(i))
	    writer.write("}\n")
	}
    }

    private def launch(scriptFile: File): Int = {
        val classLoader = Thread.currentThread().getContextClassLoader().asInstanceOf[URLClassLoader]
        val urls = classLoader.getURLs()
        val classpath = urls.map(_.getPath).mkString(":")
	val env = new java.util.HashMap[String,String]()
	env.put("CLASSPATH", classpath)
        val cmd = ArrayBuffer("scala", scriptFile.getAbsolutePath(),
            "-unchecked", "-deprecation", "-Xlint")

        val extraFlags = this.param("scalaFlags")
        if (!extraFlags.isEmpty) cmd.appendAll(extraFlags.split("[ ]+"))

        val maxMemMB = Runtime.getRuntime().maxMemory() / (1024.0*1024)
        if (maxMemMB < 1000000) cmd.append("-J-Xmx%.0fM".format(maxMemMB))

	IOTools.launch(cmd.toArray, scriptFile.getParentFile(), env, "", "")
    }
}
