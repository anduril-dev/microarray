import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.LatexTools;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

public class MACS extends SkeletonComponent {
    
    final static String PVALUE_COLUMN_NAME = "-10*log10(pvalue)";
    final static String NEW_PVALUE_COLUMN_NAME = "pvalue";
    final static String FDR_COLUMN_NAME = "FDR(%)";
    final static String NEW_FDR_COLUMN_NAME = "fdr";
    final static String CHROMOSOME_COLUMN_NAME = "chr";
    final static String UNIQUE_ID_COLUMN_NAME = "id";
    final static String NO_MODEL_R_SCRIPT = "no_model.r";
    final static String NO_MODEL_PDF = "no_model.pdf";

    @Override    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        final String componentName = cf.getMetadata(cf.METADATA_INSTANCE_NAME);      
        // Read in the parameters.        
        final String bw = cf.getParameter("bw");
        final String mFold = cf.getParameter("mFold");
        final String pValue = cf.getParameter("pValue"); 
        final String tSize = cf.getParameter("tSize");
        final String fileFormat = cf.getParameter("fileFormat");
        final String gSize = cf.getParameter("gSize");
        final String macsCommand = cf.getParameter("macsCommand");
        final String pythonDir = cf.getParameter("pythonDir");
        final boolean useWiggle = "true".equals(cf.getParameter("useWiggle"));
        final boolean outputBedGraph = "true".equals(cf.getParameter("outputBedGraph"));
        final double fdrThreshold = Double.parseDouble(cf.getParameter("fdrThreshold"));
        final String uniqueIDPrefix = cf.getParameter("uniqueIDPrefix");
        final String sectionTitle = cf.getParameter("sectionTitle");
        final String sectionType = cf.getParameter("sectionType");
        final String keepDup = cf.getParameter("keepDup");
        final String shiftSize = cf.getParameter("shiftSize");
        final boolean noModel = "true".equals(cf.getParameter("noModel"));

        // Input and output files.
        File reads1 = cf.getInput("treatment");
        File reads2 = cf.getInput("control");
        File logFile = cf.getOutput("macsOutput");
        File peaks = cf.getOutput("peaks");
        File negativePeaks = cf.getOutput("negativePeaks");
        File report = cf.getOutput("report");
        File wiggle = cf.getOutput("wiggle");
        File bedGraph = cf.getOutput("bedGraph");

        // Generate control and treatment file path.    
        String treatmentFile = reads1.getAbsolutePath();
        String controlFile = reads2.getAbsolutePath();
        
        // Construct the command string to running MACS.
        String execCommand = macsCommand + " -t " + treatmentFile + " -c " + controlFile
            + " --pvalue=" + pValue + " --mfold=" + mFold + " --bw=" + bw + 
            " --tsize=" + tSize + " --gsize=" + gSize + " --name=" + componentName + 
            " --format=" + fileFormat + " --keep-dup=" + keepDup + " --shiftsize=" +
            shiftSize;
        if(outputBedGraph) {
            execCommand = execCommand + " --bdg";
        } else if(useWiggle) {
            execCommand = execCommand + " --wig";
        }
        if(noModel) {
            execCommand = execCommand + " --nomodel";
        }
        
        // Create output files
        if(!bedGraph.exists() && !bedGraph.mkdirs())
            throw new IOException("Cannot create: "+bedGraph.getAbsolutePath());
        if(!wiggle.exists() && !wiggle.mkdirs())
            throw new IOException("Cannot create: "+wiggle.getAbsolutePath());
        if(!report.exists() && !report.mkdirs())
            throw new IOException("Cannot create: "+report.getAbsolutePath());

        // Create a temporary directory for execution.
        File tempWD = cf.getTempDir();
        
        // Create environment for execution
        HashMap<String,String> executionEnvironment = null;
        if(!pythonDir.equals("")) {
             executionEnvironment = new HashMap<String,String>();
             Map<String,String> currentEnvironment = System.getenv();
             
             for(Map.Entry<String, String> entry : currentEnvironment.entrySet()) {
                String name = entry.getKey();
                String value = entry.getValue();
                if(name.equals("PYTHONPATH")) {
                    value = value + ":" + pythonDir;
                }
                executionEnvironment.put(name, value);
            }
        }

        // Execute MACS.
        System.out.println("Executing MACS in directory: " + tempWD);
        System.out.println("Execution command was:\n"+execCommand);
        int status =  IOTools.launch(execCommand, tempWD, executionEnvironment, "MACS:", "MACS_ERR:");
        if(status != 0) { // Something went wrong.
            // Delete unnessecary MACS output files.
            if(IOTools.rmdir(tempWD, true)) {
                System.out.println("Cannot remove temporary execution directory of MACS: " + tempWD);
            }
            System.out.println("Execution command was:\n"+execCommand);
            throw new RuntimeException("MACS failed and returned "+status+'.');
        }

        // Move MACS output files from working directory to correct output files.
        File peaksMacs = new File(tempWD + "/" + componentName + "_peaks.xls");
        MACS.copyCSV(peaksMacs, peaks, true, false, fdrThreshold, uniqueIDPrefix);
        
        File negativePeaksMacs = new File(tempWD + "/" + componentName + "_negative_peaks.xls");
        MACS.copyCSV(negativePeaksMacs, negativePeaks, true, true, fdrThreshold, uniqueIDPrefix);
        
        if(outputBedGraph) {
            File bedGraphMacs = new File(tempWD + "/" + componentName + "_MACS_bedGraph");
            Tools.copyDirectory(bedGraphMacs, bedGraph, true);
        } else if(useWiggle) {
            File wiggleMacs = new File(tempWD + "/" + componentName + "_MACS_wiggle");
            Tools.copyDirectory(wiggleMacs, wiggle, true);
        }        

        // Create pdf figure of the model fitting using the R script produced by MACS
        // or a pdf telling the model fitting was not done.
        if((new File(tempWD + "/" + componentName + "_model.r")).exists()) {
            IOTools.launch("R --vanilla --file=" + componentName + "_model.r", tempWD, null, "rStdout:", "rStderr:");
        }else {
            File noModelRScript = new File(System.getProperty("user.dir"), NO_MODEL_R_SCRIPT);
            IOTools.launch("R --vanilla --file=" + noModelRScript, tempWD, null, "rStdout:", "rStderr:");
            File noModelPDF = new File(tempWD, NO_MODEL_PDF);
            File modelPDF = new File(tempWD, componentName + "_model.pdf");
            Tools.copyFile(noModelPDF, modelPDF);
        }
        
        File modelPDF = new File(tempWD, componentName + "_model.pdf");
        File modelPDFReport = new File(report, componentName + "_model_fitting.pdf");      
        Tools.copyFile(modelPDF, modelPDFReport);

        // Delete unnessecary MACS output files.
        IOTools.rmdir(tempWD, true);

        // Create Latex report.
        String caption = "Model fitting results with command: " + execCommand;
        String label = "fig:" + componentName + "-modelFitting";
        createLatexReport(report, modelPDFReport, label, caption, sectionTitle, sectionType);

        return ErrorCode.OK;
    }

    public static void copyCSV(File inputFile, File outputFile, boolean skipComments, boolean isControl, double fdrThreshold, String uniqueIDPrefix) throws IOException{
        CSVParser input = new CSVParser(inputFile, skipComments);
        Iterator<String[]> lines = input.iterator();
        int chrIndex = input.getColumnIndex(CHROMOSOME_COLUMN_NAME);
        int fdrIndex = isControl ? -1 : input.getColumnIndex(FDR_COLUMN_NAME);
        int pValueIndex = input.getColumnIndex(PVALUE_COLUMN_NAME);
        int uniqueIDNumber = 1;
        
        // Create a csv writer using the new column names.
        String [] columnNames = input.getColumnNames();
        if(!isControl) {
            columnNames[fdrIndex] = NEW_FDR_COLUMN_NAME;
        }
        columnNames[pValueIndex] = NEW_PVALUE_COLUMN_NAME;
        if(!uniqueIDPrefix.equals("")) {
            String [] oldColumnNames = columnNames;
            columnNames = new String[input.getColumnCount() + 1];
            System.arraycopy(oldColumnNames, 0, columnNames, 1, input.getColumnCount());
            columnNames[0] = UNIQUE_ID_COLUMN_NAME; 
        }
        CSVWriter output = new CSVWriter(columnNames, outputFile);

        while(input.hasNext()) {
            String[] line = input.next();
            // Transform to raw p-values from -10*log10(pvalue).
            double pValue = Double.parseDouble(line[pValueIndex]);
            pValue = Math.pow(10, pValue/-10);
            line[pValueIndex] = String.valueOf(pValue);
            // Remove 'chr' prefix from chromosome names.
            line[chrIndex] = line[chrIndex].replaceFirst("chr", "");
            /* Transform FDR values from percentage to decimal values and filter. 
               Not in control peaks. */
            if(!isControl) {
                double fdrValue = Double.parseDouble(line[fdrIndex]) / 100;
                line[fdrIndex] = String.valueOf(fdrValue);
                if(fdrValue > fdrThreshold) continue;
            }
            
            // Write unique id if prefix given.
            if(!uniqueIDPrefix.equals("")) {
                output.write(uniqueIDPrefix + uniqueIDNumber, true);
                uniqueIDNumber++;
            }

            for(String column : line) {
                output.write(column, true);
            }
        }
        input.close();
        output.close();
        
    }
    
    public static void createLatexReport(File outDir, File modelPDF, String label, String caption, String sectionTitle, String sectionType) throws IOException {
        ArrayList<String> tex = new ArrayList<String>(); 
        if(!sectionTitle.isEmpty()) {
            tex.add(0, "\\" + sectionType + "{" + LatexTools.quote(sectionTitle) + "}");
        }   
        String captionQuoted = LatexTools.quote(caption);
        tex.addAll(LatexTools.formatFigure(modelPDF.getName(), label, captionQuoted, "", 18.0));
        LatexTools.writeDocument(outDir, tex);
    }
 
    public static void main(String[] args) {
        new MACS().run(args);
    }
}
