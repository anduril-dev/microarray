library(grid)
NO_MODEL_PDF <- "no_model.pdf"
pdf(NO_MODEL_PDF)
grid.text("MODEL WAS NOT CREATED.", gp=gpar(fontsize=20))
dev.off()
