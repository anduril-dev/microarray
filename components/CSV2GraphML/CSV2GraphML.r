library(componentSkeleton)
library(igraph)

# igraph 0.6 indexes vertices from 1; earlier from 0
if (packageVersion('igraph') >= '0.6') {
    index.base <<- 1
} else {
    index.base <<- 0
}

execute <- function(cf) {
    type <- get.parameter(cf, 'type')
	directed <- get.parameter(cf, 'directed')
    if (type == 'adjacency'){ 
		m <- Matrix.read(get.input(cf, 'matrix'))
		g <- from.adjacency(m, directed)
	} else if (type == 'adjancency-labels') {
	    m <- CSV.read(get.input(cf, 'matrix'))
	    m <- m[, 2:ncol(m)] # Remove vertex name columns
	    g <- from.adjacency.labels(m, directed)
	} else if (type == 'incidence'){
		m <- Matrix.read(get.input(cf, 'matrix'))
		g <- from.incidence(m)
	} else if (type == 'edgelist'){
		m <- CSV.read(get.input(cf, 'matrix'))
		g <- from.edgelist(m, directed)
	} else {
        write.error(cf, sprintf('Invalid "type" parameter: %s', type))
        return(PARAMETER_ERROR)
    }

    if (get.parameter(cf, 'edgeWeights', 'boolean')) {
        E(g)$label <- E(g)$weight
        E(g)$label[E(g)$label == 1] <- ''
    }
    
    max.width <- get.parameter(cf, 'maxEdgeWidth', 'float')
    if (max.width > 1) {
        max.weight <- max(E(g)$weight, na.rm=TRUE)
        if (max.weight > 0) {
            penwidth <- (E(g)$weight / max.weight) * max.width
            penwidth[penwidth<1] <- 1
            E(g)$penwidth <- penwidth
        }
    }
    
    write.graph(g, get.output(cf, 'graph'), 'graphml')
    return(0)
}

from.adjacency <- function(matr, directed) {
    if (directed) graph.mode <- 'directed'
    else graph.mode <- 'undirected'
    g <- graph.adjacency(matr, mode=graph.mode, weighted=TRUE)
    V(g)$label <- V(g)$name
    return(g)
}

from.adjacency.labels <- function(adj.labels, directed) {
    adj.matrix <- ifelse(is.na(adj.labels), 0, 1)
    g <- from.adjacency(adj.matrix, directed)
    
    # Set edge labels
    for (i in 1:nrow(adj.labels)) {
        for (j in 1:ncol(adj.labels)) {
            if (!is.na(adj.labels[i, j])) {
                g <- set.edge.attribute(g, 'Label', adj.labels[i, j], index=E(g, P=c(i-1+index.base, j-1+index.base)))
            }
        }
    }
    
    return(g)
}

from.incidence <- function(matr) {
    NUM.PLACES <- nrow(matr)
    NUM.TRANS <- ncol(matr)
    NUM.VERTICES <- NUM.PLACES + NUM.TRANS
    
    pre.m <- matr
    pre.m[pre.m<0] <- 0
    
    post.m <- matr
    post.m[post.m>0] <- 0
    post.m <- -post.m # transform to positive values
    
    adj.m <- matrix(0, NUM.VERTICES, NUM.VERTICES)
    adj.m[(NUM.PLACES+1):nrow(adj.m), 1:NUM.PLACES] <- t(pre.m)
    adj.m[1:NUM.PLACES, (NUM.PLACES+1):ncol(adj.m)] <- post.m
    
    g <- graph.adjacency(adj.m, weighted=TRUE)
    V(g)$shape <- c(rep('ellipse', NUM.PLACES), rep('box', NUM.TRANS))
    V(g)$label <- c(rownames(matr), colnames(matr))
    return(g)
}

from.edgelist <- function(csv, directed){
	g <- graph.edgelist(as.matrix(csv[,1:2]), directed=directed)
    V(g)$label <- V(g)$name
    cols <- colnames(csv)
    if (length(cols) > 2) for (cName in cols[-2:-1]) {
       g <- set.edge.attribute(g, cName, value=csv[,cName])
    }
	return(g)
}
main(execute)
