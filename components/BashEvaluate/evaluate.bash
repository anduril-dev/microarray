# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh" "$1"

# use these functions to read command file
script=$( getinput script )
scriptpar=$( getparameter script )
toscript=$( getoutput script )

stdoutfile=$( getoutput stdOut )
stderrfile=$( getoutput stdErr )

optOut1=$( getoutput optOut1 )
optOut2=$( getoutput optOut2 )
optOut3=$( getoutput optOut3 )

folder1=$( getoutput folder1 )
folder2=$( getoutput folder2 )
folder3=$( getoutput folder3 )

array1=$( getoutput array1 )
array1index=$( getoutput _index_array1 )
array2=$( getoutput array2 )
array2index=$( getoutput _index_array2 )
array3=$( getoutput array3 )
array3index=$( getoutput _index_array3 )

altcase=$( getoutput _choices )

touch "$optOut1" "$optOut2" "$optOut3" || echo error creating output files >> $errorfile
mkdir -p "$folder1" "$folder2" "$folder3" || echo error creating output directories >> $errorfile
mkdir -p "${array1}" "${array2}" "${array3}" || echo error creating output array >> $errorfile
echo -e '"Key"'"\t"'"File"' >> "${array1index}"
echo -e '"Key"'"\t"'"File"' >> "${array2index}"
echo -e '"Key"'"\t"'"File"' >> "${array3index}"

param1=$( getparameter param1 )
param2=$( getparameter param2 )
param3=$( getparameter param3 )
param4=$( getparameter param4 )
param5=$( getparameter param5 )
failerr=$( getparameter failOnErr )

var1=$( getinput var1 )
var2=$( getinput var2 )
var3=$( getinput var3 )
var4=$( getinput var4 )
var5=$( getinput var5 )
var6=$( getinput var6 )

param1=${param1//,/\\,}
param2=${param2//,/\\,}
param3=${param3//,/\\,}
param4=${param4//,/\\,}
param5=${param5//,/\\,}
# make sure all commas are escaped
# if folder names contain commas, this still fails.

if [ "$failerr" = "true" ]
then echo "set -e" >> "${toscript}.tmp"
fi
if [ ! -z "$scriptpar" ]
then echo -e source "${ANDURIL_HOME}/bash/functions.sh" \"${1}\"\\n >> "${toscript}.tmp"
     echo -e "${scriptpar}" >> "${toscript}.tmp"
else
    echo -e source "${ANDURIL_HOME}/bash/functions.sh" \"${1}\"\\n >> "${toscript}.tmp"
    cat "${script}" >> "${toscript}.tmp"
fi

exportarraykeys vararray
export_command

# The last \r replace is for DOS format script files... hopefully this works in DOS environment...
sed -e 's,@var1@,"'"$var1"'",g' -e 's,@var2@,"'"$var2"'",g' -e 's,@var3@,"'"$var3"'",g' \
 -e 's,@var4@,"'"$var4"'",g' -e 's,@var5@,"'"$var5"'",g' -e 's,@var6@,"'"$var6"'",g' \
 -e 's,@optOut1@,"'"$optOut1"'",g' -e 's,@optOut2@,"'"$optOut2"'",g' -e 's,@optOut3@,"'"$optOut3"'",g' \
 -e 's,@folder1@,"'"$folder1"'",g' -e 's,@folder2@,"'"$folder2"'",g' -e 's,@folder3@,"'"$folder3"'",g' \
 -e 's,@param1@,"'"$param1"'",g' -e 's,@param2@,"'"$param2"'",g' -e 's,@param3@,"'"$param3"'",g' \
 -e 's,@param4@,"'"$param4"'",g' -e 's,@param5@,"'"$param5"'",g' \
 -e 's,@arrayOut1@,"'"$array1"'",g' -e 's,@arrayIndex1@,"'"$array1index"'",g' \
 -e 's,@arrayOut2@,"'"$array2"'",g' -e 's,@arrayIndex2@,"'"$array2index"'",g' \
 -e 's,@arrayOut3@,"'"$array3"'",g' -e 's,@arrayIndex3@,"'"$array3index"'",g' \
 -e 's,@altcase@,"'"$altcase"'",g' \
 -e 's,\r$,,' "${toscript}.tmp" > "$toscript"
rm "${toscript}.tmp"

cd $( dirname "$1" )
touch "$stdoutfile"
touch "$stderrfile"
if [ "$( getparameter echoStdOut )" = "true" ]
then bash "$toscript" 2> "$stderrfile" | tee "$stdoutfile"
      exit_status=${PIPESTATUS[0]}
    if [ -s "$stderrfile" ]
    then echo "stdErr:"
         cat "$stderrfile"
    fi
else bash "$toscript" > "$stdoutfile" 2> "$stderrfile" 
     exit_status=$?
fi

if [ "$exit_status" -gt "0" ]
then to_file="$errorfile"
    echo "Exit status: $exit_status" >> "$to_file"
    echo "stdErr: " >> "$to_file"
    cat "$stderrfile" >> "$to_file"
fi

if [ ! -z "$altcase" ]
then if [ -f "$altcase" ]
     then echo "altCase" > "$altcase"
     else echo "defCase" > "$altcase"
     fi
fi

exit $exit_status
