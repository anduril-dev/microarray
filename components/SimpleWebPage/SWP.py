from anduril.args import *
from anduril.args import cf
from anduril import TableReader
import os,subprocess
import shutil,re

imagesearch=re.compile('.*\.jpg$|.*\.jpeg$|.*\.gif$|.*\.png$|.*\.svg$|.*\.pdf$',re.I)
vectorsearch=re.compile('.*\.svg$|.*\.pdf$',re.I)

def copytree(src, dst, symlinks=False):
    """Recursively copy a directory tree using shutil.copy2().

    Modified from shutil.copytree to overcome copystat -problems
    on NTFS / CIFS / other non-octect permission media
     ( uses Anduril logger )

    """
    names = os.listdir(src)
    os.makedirs(dst)
    errors = []
    permission_errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                copytree(srcname, dstname, symlinks)
            else:
                # Will raise a SpecialFileError for unsupported file types
                shutil.copyfile(srcname, dstname)
                try:
                    shutil.copystat(srcname, dstname)
                except:
                    # stats may not copy.
                    permission_errors.append(dstname)
                    pass
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except Error, err:
            errors.extend(err.args[0])
        except EnvironmentError, why:
            errors.append((srcname, dstname, str(why)))
    try:
        shutil.copystat(src, dst)
    except:
        # If permissions cannot be set, skip this
        permission_errors.append(dst)
        pass

    if len(permission_errors)>0:
        cf.write_log('Could not change permissions: '+', '.join(permission_errors))

    if errors:
        raise Error, errors

def copyfile(src, dst):
    """Copy a file with error reporting.

    """
    errors = []
    permission_errors = []
    try:
        # Will raise a SpecialFileError for unsupported file types
        shutil.copyfile(src, dst)
    except Error, err:
        errors.extend(err.args[0])
    except EnvironmentError, why:
        errors.append((src, dst, str(why)))
    try:
        shutil.copystat(src, dst)
    except:
        # If permissions cannot be set, skip this
        permission_errors.append(dst)
        pass

    if len(permission_errors)>0:
        cf.write_log('Could not change permissions: '+', '.join(permission_errors))

    if errors:
        raise Error, errors

def sort_nicely( l ): 
  """ Sort the given list in the way that humans expect. 
  """ 
  convert = lambda text: int(text) if text.isdigit() else text 
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
  l.sort( key=alphanum_key )         

def sizestring(size):
    ''' Returns human readable file size string '''
    for x in ['b','kb','Mb','Gb','Tb']:
        if size < 1024.0:
            if (x=='b') | (x=='kb'):
                return "%d%s" % (size, x)
            else:
                return "%3.1f%s" % (size, x)
        size /= 1024.0

def create_bitmap(infile,outfile,r,vector=False):
    ''' Creates thumbnails and returns the new name of the file '''
    
    res=r+'x'+r#+'>'
    if vector:
        outfile=outfile+'.jpg'
        convargs=['convert','-density','120x120',infile+'[0]','-background','white','-flatten','-resize',res,'-quality','97',outfile]
    else:
        convargs=['convert','-define','jpeg:size='+r+'x'+r,infile+'[0]','-background','white','-flatten','-resize',res,'-quality','85',outfile]
    convp=subprocess.call(convargs)
    bname=os.path.basename(outfile)
    return bname
        
def file_list_to_HTML(file_list,path,index,thumb_size,annotations,thumbnails):
    ''' Return a string containing a HTML version of a file list '''
    
    sort_nicely(file_list)
    files=[]
    files.append('<ul>')
    for f in file_list:
        if os.path.isdir(os.path.join(path,f)):
            f_item='\t<li><span class="folder"><span class="name"><a href="'+f+'/'+index+'">'+f+'</a></span>'
            files.append(f_item+get_annotation(f,annotations)+'</span>')
    for f in file_list:
        if f=='index.html':
            continue
        if not os.path.isdir(os.path.join(path,f)):
            f_item='\t<li><span class="item"><span class="name"><a href="'+f+'">'+f+'</a></span>'
            size='<span class="size">['+sizestring(os.path.getsize(os.path.join(path,f)))+']</span>'
            if thumbnails and imagesearch.match(f):
                t=create_bitmap(os.path.join(path,f),os.path.join(path,'.'+f),str(thumb_size),vector=vectorsearch.match(f))
                f_item='\t<li><span class="item"><div class="thumbnail"><a href="'+f+'"><img src="'+t+'"/></a></div><div><span class="name"><a href="'+f+'">'+f+'</a></span>'
                files.append(f_item+get_annotation(f,annotations)+size+'</div></a></span>')
            else:
                files.append(f_item+get_annotation(f,annotations)+size+'</span>')
    files.append('</ul>')

    return '\n'.join(files)

def get_annotation(file_name,annotations):
    ''' return annotattion for file from the annotation list '''
    
    if not annotations:
        return ''
    annot=[i[1] for i in annotations if i[0]==file_name]
    if len(annot)==0:
        return '<span class="annotation"></span>'
    return '<span class="annotation">'+annot[0]+'</span>'


# read source files
template_file='template.html'
style_file='style.css'

if style:
    style_file=style
# copy the input folder as output folder
if input_folder:
    copytree(input_folder,output_html,symlinks=True)
else:
    os.makedirs(output_html)
if input_array:
    for row in input_array.items():
        if os.path.isfile(row[1]):
            copyfile(row[1], os.path.join(output_html,row[0]))
        else:
            copytree(row[1], os.path.join(output_html,row[0]),symlinks=True)


# read the template and replace tags
out_index=open(os.path.join(output_html,'index.html'),'w')
template=open(template_file,'r').read()
template=template.replace('TEMPLATE:TITLE',title)
template=template.replace('TEMPLATE:STYLESTRING',contentStyle)
template=template.replace('TEMPLATE:STYLE',open(style_file,'r').read())
template=template.replace('TEMPLATE:CONTENTSTRING',contentString)
if content:
    template=template.replace('TEMPLATE:CONTENT',open(content,'r').read())
else:
    template=template.replace('TEMPLATE:CONTENT','')
template=template.replace('TEMPLATE:FOOTER',footer)
# read annotations file
if annotation:
    annotations=TableReader(annotation,type='list',column_types=str).read()
else:
    annotations=False

# create HTML for the file list
file_list=os.listdir(output_html)
list_HTML=file_list_to_HTML(file_list,output_html,index,size,annotations,thumbnails)
template=template.replace('TEMPLATE:FILES',list_HTML)

# create tableArray
expands=param_tableArrayExpand.split(',')
tableArrayList=[]

if input_tableArray:
    colN=0
    for col in input_tableArray.items():
        tableArrayList.append([])
        thisExpand=col[0] in expands
        file_list=os.listdir(col[1])
        sort_nicely(file_list)
        for row in file_list:
            sourceFile=os.path.join(col[1],row)
            targetBase=col[0]+"_"+row
            targetFile=os.path.join(output_html,targetBase)
            
            rowExpand=thisExpand
            # If source is image or folder, do not even try to append it in HTML
            if imagesearch.match(targetBase) or (not os.path.isfile(sourceFile)):
                rowExpand=False
            if rowExpand:
                tableArrayList[colN].append( open(sourceFile,'r').read() )
            else:
                if os.path.isfile(sourceFile):
                    copyfile(sourceFile, targetFile)
                else:
                    copytree(sourceFile, targetFile,symlinks=True)
                
                if param_thumbnails and imagesearch.match(sourceFile):
                    t=create_bitmap(sourceFile,
                                    os.path.join(output_html,'.'+targetBase),
                                    str(param_size),
                                    vector=vectorsearch.match(sourceFile))
                    tableArrayList[colN].append( '<a href="'+targetBase+'"><img src="'+t+'"/></a>' )
                else:
                    tableArrayList[colN].append( '<a href="'+targetBase+'">'+targetBase+'</a>' )
        colN+=1
                    
if len(tableArrayList)==0:
    template=template.replace('TEMPLATE:TABLEARRAY','')
else:
    tableArrayHTML='<table id="tableArray">'
    tableArrayHTML+="\n<tr>"
    tableArrayHTML+="\n  ".join(["<th>"+s[0]+"</th>" for s in input_tableArray.items() ])
    
    for row in map(list, zip(*tableArrayList)):
        tableArrayHTML+="\n<tr>"
        for col in row:
            tableArrayHTML+="\n  <td>"+col+"</td>"
    tableArrayHTML+="\n</table>"
    template=template.replace('TEMPLATE:TABLEARRAY',tableArrayHTML)
# write index
out_index.write(template)
    

