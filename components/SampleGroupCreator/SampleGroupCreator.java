import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class SampleGroupCreator extends SkeletonComponent {

    public static final int MAX_INPUTS = 3;
    public static final int MAX_PATTERNS = 9;
    public static final int MAX_SUB_PATTERNS = 9;

    private String[] patternTypes;
    private List<String> sampleNames;
    private Map<String, Group> groups;
    private List<String> orderedGroups;

    private enum PatternType {
        VERBATIM, RE, RE_LIST
    }

    private class Group {
        public final String groupID;
        public final String type;
        public final String description;
        public final List<String> members;

        public Group(String groupID, String type, String description) {
            this.groupID = groupID;
            this.type = type;
            this.description = description;
            this.members = new ArrayList<String>();
        }
    }

    public SampleGroupCreator() {
        this.groups = new HashMap<String, Group>();
        this.orderedGroups = new ArrayList<String>();
    }

    @Override
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        this.sampleNames = readSampleNames(cf);
        if (this.sampleNames == null) return ErrorCode.INVALID_INPUT;

        this.patternTypes = AsserUtil.split(cf.getParameter("patternTypes"));

        for (int i=1; i<=MAX_PATTERNS; i++) {
            String pattern = cf.getParameter("pattern" + i);
            String definition = cf.getParameter("definition" + i);
            if (pattern.isEmpty() || definition.isEmpty()) continue;

            String[] defTokens = definition.split(",");
            if (defTokens.length < 2) {
                cf.writeError(definition + ": Invalid definition");
                return ErrorCode.PARAMETER_ERROR;
            }
            final String groupName = defTokens[0];
            final String type = defTokens[1];
            final String description = defTokens.length >= 3 ? defTokens[2] : groupName;

            PatternType patternType;
            if (this.patternTypes.length < i) {
                patternType = PatternType.RE;
            } else {
                final String pType = this.patternTypes[i - 1];
                if (pType.equals("re") || pType.isEmpty()) patternType = PatternType.RE;
                else if (pType.equals("relist")) patternType = PatternType.RE_LIST;
                else if (pType.equals("verbatim")) patternType = PatternType.VERBATIM;
                else {
                    cf.writeError("Invalid pattern type: " + pType);
                    return ErrorCode.PARAMETER_ERROR;
                }
            }

            boolean ok;
            switch (patternType) {
            case RE:
                ok = evalRE(cf, pattern, groupName, type, description);
                if (!ok) return ErrorCode.PARAMETER_ERROR;
                break;
            case RE_LIST:
                ok = evalREList(cf, pattern, groupName, type, description);
                if (!ok) return ErrorCode.PARAMETER_ERROR;
                break;
            case VERBATIM:
                evalVerbatim(pattern, groupName, type, description);
                break;
            }
        }

        CSVWriter writer = new CSVWriter(new String[] { "ID", "Members", "Type",
                "Description" }, cf.getOutput("groups"));
        try {
            for (String groupID : orderedGroups) {
                Group group = groups.get(groupID);
                writer.write(group.groupID);
                writer.write(AsserUtil.collapse(group.members));
                writer.write(group.type);
                writer.write(group.description);
            }
        } finally {
            writer.close();
        }

        return ErrorCode.OK;
    }

    private List<String> readSampleNames(CommandFile cf) throws IOException {
        List<String> names = new ArrayList<String>();
        String[] columns = cf.getParameter("columns").split(",");

        for (int i=1; i<=MAX_INPUTS; i++) {
            String inputName = "data" + i;
            if (!cf.inputDefined(inputName)) continue;
            
            String column = columns.length >= i ? columns[i - 1] : "";
            CSVParser reader = new CSVParser(cf.getInput(inputName));
            try {
                if (column.isEmpty()) {
                    for (String colName : reader.getColumnNames()) {
                        names.add(colName);
                    }
                } else {
                    final int index = reader.getColumnIndex(column);
                    if (index < 0) {
                        cf.writeError(inputName + ": column not found: " + column);
                        return null;
                    }
                    for (String[] line : reader) {
                        names.add(line[index]);
                    }
                }
            } finally {
                reader.close();
            }
        }

        return names;
    }

    private String replaceSubPatterns(String target, Matcher matcher) {
        if (target.indexOf('$') < 0) return target;

        for (int groupIndex = 0; groupIndex <= MAX_SUB_PATTERNS; groupIndex++) {
            try {
                String subPattern = matcher.group(groupIndex);
                if (subPattern != null) {
                    target = target.replace("$" + groupIndex, subPattern);
                }
            } catch (IndexOutOfBoundsException e) {
                break; /* Sub-pattern not found */
            }
        }
        return target;
    }

    private boolean evalRE(CommandFile cf, String patternStr, String groupName,
            String type, String description) {
        Pattern pattern = Pattern.compile(patternStr);
        boolean ok = false;

        for (String sample : sampleNames) {
            Matcher matcher = pattern.matcher(sample);
            if (!matcher.matches()) continue;

            String groupID = replaceSubPatterns(groupName, matcher);
            Group group = groups.get(groupID);
            if (group == null) {
                String thisDesc = replaceSubPatterns(description, matcher);
                group = new Group(groupID, type, thisDesc);
                groups.put(groupID, group);
                orderedGroups.add(groupID);
            }
            group.members.add(sample);
            ok = true;
        }

        if (!ok) {
            cf.writeError(String.format(
                    "Invalid definition of %s: no matching samples found",
                    groupName));
        }
        return ok;
    }

    private boolean evalREList(CommandFile cf, String patternStr,
            String groupName, String type, String description) {
        String[] patterns = patternStr.split(",");
        if (patterns.length == 1) {
            return evalRE(cf, patternStr, groupName, type, description);
        }
        if (patterns.length > 2) {
            cf.writeError("Too many patterns in relist: "+patternStr);
            return false;
        }

        final Pattern pattern1 = Pattern.compile(patterns[0]);
        boolean ok = false;

        for (String sample1: sampleNames) {
            Matcher matcher1 = pattern1.matcher(sample1);
            if (!matcher1.matches()) continue;

            Pattern pattern2 = Pattern.compile(replaceSubPatterns(patterns[1],
                    matcher1));
            for (String sample2 : sampleNames) {
                Matcher matcher2 = pattern2.matcher(sample2);
                if (!matcher2.matches()) continue;

                String groupID = replaceSubPatterns(groupName, matcher1);
                Group group = groups.get(groupID);
                if (group == null) {
                    String thisDesc = replaceSubPatterns(description, matcher1);
                    group = new Group(groupID, type, thisDesc);
                    groups.put(groupID, group);
                    orderedGroups.add(groupID);
                }
                group.members.add(sample1);
                group.members.add(sample2);
                ok = true;
            }
        }

        if (!ok) {
            cf.writeError(String.format(
                    "Invalid definition of %s: no matching samples found",
                    groupName));
        }
        return ok;
    }

    private void evalVerbatim(String patternStr, String groupName, String type,
            String description) {
        Group group = new Group(groupName, type, description);
        groups.put(groupName, group);
        orderedGroups.add(groupName);

        for (String member: patternStr.split(",")) {
            if (member.isEmpty()) continue;
            if (!this.sampleNames.contains(member) && this.groups.get(member) == null) {
                System.err.println(String.format(
                        "Warning: In definition of %s: sample name %s not found in data",
                        groupName, member));
            }
            group.members.add(member);
        }
    }

    public static void main(String[] args) {
        new SampleGroupCreator().run(args);
    }

}
