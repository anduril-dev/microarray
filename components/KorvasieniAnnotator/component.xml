<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>KorvasieniAnnotator</name>
    <version>1.9</version>
    <doc>Converts gene, transcription, and translation identifiers using Korvasieni.
         This component can be used to annotate findings and to integrate
         information between various sources.

         An example list of <a href="databases.txt">supported (source and target) databases</a>
         includes a snapshot of known Ensembl links. You may prefix these databases with an underscore (_)
         for real database identifiers.
    </doc>
    <author email="Marko.Laakso@Helsinki.FI">Marko Laakso</author>
    <category>Annotation</category>
    <category>GO</category>
    <launcher type="java">
        <argument name="class" value="fi.helsinki.ltdk.csbl.asser.annotation.KorvasieniComponent" />
    </launcher>
    <requires type="jar" URL="http://dev.mysql.com/downloads/connector/j/">mysql-connector-java-5.1.6-bin.jar</requires>
    <inputs>
        <input name="sourceKeys" type="CSV">
            <doc>A list of source database keys.</doc>
        </input>
        <input name="connection" type="Properties" optional="true">
            <doc>Database connection can be defined using this file.
                 The definition of parameters: database.url, database.user,
                 database.password, database.timeout,
                 database.recycle, and database.driver can be found from the
                 documentation of Korvasieni.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="bioAnnotation" type="AnnotationTable">
            <doc>Table that contains columns for all types of annotation given in the parameter "types".
                 Indicator column is 1 if the input key is found from the database.</doc>
        </output>
    </outputs>
    <parameters>
            <parameter name="keyColumn" type="string" default="">
                <doc>Name of the key column withing sourceKeys file or an empty string for the first column.
                     See inputDB for further information about the DNA regions.</doc>
            </parameter>
            <parameter name="isListKey" type="boolean" default="false">
                <doc>Enables the automatic value splits for the comma separated key column</doc>
            </parameter>
            <parameter name="echoColumns" type="string" default="">
                <doc>A comma separated list of column names for the columns that will be copied to the output.
                     An asterisk (*) can be used to denote all columns except the keyColumn.</doc>
            </parameter>
            <parameter name="targetDB" type="string">
                <doc>Comma-separated list of annotation types.
                Possible values are all databases supported by Korvasieni.</doc>
            </parameter>
            <parameter name="inputDB" type="string" default="">
                <doc>Type of input keys. This must be a database supported by Korvasieni.
                     If the parameter is omitted, the component tries to derive the database from the type of geneID.
                     If this is not possible, an error is returned.
                     You may define three columns in form of chromosome:start-end in case the inputDB is .DNARegion.
                     This format provides a comfortable compatibility with DNARegion datatype.
                     The end positions can be left out if they would be the same as the start positions
                     (=single nucleotides).</doc>
            </parameter>
            <parameter name="inputType" type="string" default="Any">
                <doc>Ensembl object type for the input keys (Any, Gene, Transcript, Translation)</doc>
            </parameter>
            <parameter name="unique" type="boolean" default="false">
                <doc>This flag can be turned on in order to eliminate duplicate annotations.</doc>
            </parameter>
            <parameter name="maxHits" type="int" default="100000">
                <doc>Maximum number of target identifiers for a single source identifier</doc>
            </parameter>
            <parameter name="skipLevel" type="string" default="never">
                <doc>Skip result rows if the source identifier is unknown or target identifiers are
                     not available.
                     Possible values are:
                     never  (no filtering),
                     source (skip if the source ID is unknown),
                     target (skip if no target IDs are found),
                     any    (skip if any of the target IDs is missing).</doc>
            </parameter>
            <parameter name="rename" type="string" default="">
                <doc>Comma separated list of column renaming rules (oldname=newname)</doc>
            </parameter>
            <parameter name="indicator" type="boolean" default="true">
                <doc>Enables an indicator column that tells (=1) if the source key was matching
                     the database or not (=0).</doc>
            </parameter>
            <parameter name="primary" type="boolean" default="false">
                <doc>Skip secondary identifiers such as LGR identifiers.</doc>
            </parameter>
            <parameter name="goFilter" type="string" default="">
                <doc>A comma separated list of the Gene Ontology evidence codes that shall be excluded.
                     This parameter is only used for the 'GO' annotations.</doc>
            </parameter>
    </parameters>
</component>
