<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>ACGHsegment</name>
    <version>2.0</version>
    <doc> Segments Array CGH data with the Circular Binary Segmentation (CBS)
          algorithm and produces hard or soft copy number aberration calls.

          The 'geneAnnotation' input file must contain columns Chr, start, and end separately and
          the chromosomal numbers must be without the string 'chr' in front of the chromosome
          identifier. Valid chromosome identifiers are [1,2,..22, X, Y]. The actual names
          of the column names of the identifiers in the 'geneAnnotation' file are user definable. 

          Two output modes exist. The first one (hard) calls the copy number of a probe aberrated if
          is more than the median plus two standard deviations apart from the mean of the intensities
          of all the samples under scrutiny. In the second (soft) copy number aberration calls are made 
          probabilistically with either the 
          <a target="_blank" href="http://www.few.vu.nl/~mavdwiel/CGHcall.html">CGHcall</a> or the
          <a target="_blank" href="http://www.csdc.unifi.it/TASSO">FastCall</a> (part of the TASSO package)
          algorithm. Please note that FastCall only works on 32bit systems.

          Several plots can be produced based on user parameters. Example images can be found
          <a href="ACGHsegmentPlots.pdf">here</a>.

          The outputs are the dependant on the analysis procedure. The following matrices can be output:
          All segments with their respective copy number aberration calls, thresholds for significant copy
          number aberrations, and segment call probabilities.
    </doc>
    <author email="Riku.Louhimo@Helsinki.FI">Riku Louhimo</author>
    <category>Agilent</category>
    <category>Copy Number Analysis</category>
    <launcher type="R">
        <argument name="file" value="ACGHsegment.r" />
    </launcher>
    <requires URL="http://www.r-project.org/" type="manual">R</requires>
    <requires type="R-bioconductor" URL="http://www.bioconductor.org/packages/release/bioc/html/limma.html">limma</requires>
    <requires type="R-bioconductor" URL="http://www.bioconductor.org/packages/release/bioc/html/DNAcopy.html">DNAcopy</requires>
    <requires type="R-bioconductor" URL="http://www.bioconductor.org/packages/release/bioc/html/CGHcall.html">CGHcall</requires>
    <requires type="R-package"      URL="http://www.csdc.unifi.it/CMpro-v-p-89.html">TASSO</requires>
    <inputs>
        <input name="caseChan" type="CSV">
        	<doc> CSV file containing the normalized probe intensities for
                  the case channel. First row should have the probenames and
                  samples should be columnwise.
        	</doc>
        </input>
        <input name="geneAnnotation" type="AnnotationTable"> 
        	<doc> Probewise annotations as produced by the AgilenReader component. 
                  Only the probes present in the casechannel csv can be included.
                  Use CSVFilter if the probes do not otherwise match.
            </doc>
        </input>
    </inputs>
    <outputs>
        <output name="report" type="Latex">
            <doc>Latex report for the analysis.</doc>
        </output>
	<output name="segments" type="CSV">
	    <doc>Segmented probewise copy number change values. </doc>
	</output>
	<output name="tholds" type="CSV">
	    <doc>Thresholds that were used to define a segment aberrated. </doc>
	</output>
	<output name="lossProbs" type="CSV">
	    <doc>Probewise probability of a loss having occured in the 
                 segment to which the probe was assigned.
            </doc>
	</output>
	<output name="gainProbs" type="CSV">
	    <doc>Probewise probability of a gain having occured in the 
                 segment to which the probe was assigned.
            </doc>
	</output>
	<output name="normProbs" type="CSV">
	    <doc>Probewise probability of no copy number change in the 
                 segment to which the probe was assigned.
            </doc>
        </output>
        <output name="rawSegments" type="CSV">
            <doc>Probewise segment means.</doc>
        </output>
        <output name="frequency" type="CSV">
            <doc>Probewise copy-number alteration frequencies.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="chrColumn" type="string" default="Chr">
            <doc> Column name for chromosome identifiers in geneAnnotation. </doc>
        </parameter>
        <parameter name="bpStartCol" type="string" default="start">
            <doc> Column name for chromosome start basepair in geneAnnotation. </doc>
        </parameter>
        <parameter name="bpEndCol" type="string" default="end">
            <doc> Column name for chromosome end basepair in geneAnnotation. </doc>
        </parameter>
        <parameter name="plotChromosomes" type="string" default="0">
            <doc> Defines the plots that the user wants to output as a comma separated list. 
                  The whole genome is plotted by default for each sample. Inputting
                  a value different than 0 will generate additional plots for
                  these specific chromosomes from each sample.
            </doc>
        </parameter>
        <parameter name="plotEverySample" type="boolean" default="false">
            <doc> Enabling this will make the component print each sample
                  separately.
            </doc>
        </parameter>
        <parameter name="callProbabilities" type="boolean" default="false">
            <doc> Enabling this will make the component estimate probabilties for CNA segments 
                  via the CGHcall package. Only guaranteed to work when multiple chromosomes
                  are analyzed simultaneously.
            </doc>
        </parameter>
        <parameter name="callProbMethod" type="string" default="CGHCall">
            <doc> Method with which to call CNA segment probabilities. Must be either
                  CGHCall or FastCall. FastCall is significantly (30000 times) faster
                  while CGHCall is more accurate.
            </doc>
        </parameter>
        <parameter name="CGHCallPrior" type="string" default="auto">
            <doc> Only used if callProbMethod=CGHCall. Set the method
                  to determine prior probabilities to CGHCall algorithm. Must be one
                  of "auto", "all", "not all". 
            </doc>
        </parameter>
        <parameter name="CGHCallRobustsig" type="boolean" default="true">
            <doc> Only used if callProbMethod=CGHCall. Setting this to true enforces a lower bound
                  on the normal segments.
            </doc>
        </parameter>
        <parameter name="CGHCallMaxnumseg" type="int" default="100">
            <doc> Only used if callProbMethod=CGHCall. Maximum number of segments
                  on a sample to be used for fitting the probabilty model.
            </doc>
        </parameter>
        <parameter name="nSegFit" type="int" default="3000">
            <doc> Maximum number of segments used for fitting the mixture
                  model in CGHcall probability calculations. Disabled if callProbabilities=false.
                  Decreasing this lowers accuracy but can speed computation significantly.
            </doc>
        </parameter>
        <parameter name="upperLimit" type="float" default="0.0">
            <doc> Use this as the intensity limit for calling a segment gained.
                  Default value computes the mean of the sample set and estimates
                  the upper threshold for CNA call to be two standard deviations from it.
            </doc>
        </parameter>
        <parameter name="lowerLimit" type="float" default="0.0">
            <doc> Use this as the intensity limit for calling a segment lossed.
                  Default value computes the mean of the sample set and estimates
                  the lower threshold for CNA call to be two standard deviations from it.
            </doc>
        </parameter>
        <parameter name="undoSplits" type="string" default="sdundo">
            <doc> A character string specifying how change-points are to be
                  undone, if at all. Undoing change-points decreases the 
                  sensitivity of segmentation.
                  Choices are "none","prune", which uses 
                  a sum of squares criterion, and "sdundo" (default),
                  which undoes splits that are not at least this many SDs
                  apart. SD by default is 3.
            </doc>
        </parameter>
        <parameter name="undoSD" type="int" default="3">
            <doc> Only used if 'undoSplits=sdundo'. Defines how many
                  SDs two adjacent segments can be apart before they
                  are combined.
            </doc>
        </parameter>
        <parameter name="filterNA" type="boolean" default="true">
            <doc> Filter out probes with NA values. This is always true
                  when 'callProbabilities' is true.
            </doc>
        </parameter>
        <parameter name="alpha" type="float" default="0.01">
            <doc>P-value of CBS to accept a break point.</doc>
        </parameter>
        <parameter name="minWidth" type="int" default="2">
            <doc>Minimum number of probes for CBS to define a segment.
                 CBS only allows widths between 2 and 5.
            </doc>
        </parameter>
       <parameter name="outputAllSegs" type="boolean" default="false">
           <doc>Output all segmentation results even if non-significant or non-aberrated.</doc>
       </parameter>
    </parameters>
</component>
