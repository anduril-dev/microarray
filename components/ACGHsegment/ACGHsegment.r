library(componentSkeleton)
library(limma)
library(DNAcopy)

execute <- function(cf) {

   # Initiate component
   InstanceName  <- get.metadata(cf, 'instanceName')
   geneAnnot     <- AnnotationTable.read(get.input(cf, 'geneAnnotation'))
   case          <- LogMatrix.read(get.input(cf, 'caseChan'))
   out.dir       <- get.output(cf, 'report')
   docName       <- file.path(out.dir, LATEX.DOCUMENT.FILE)
   GWS           <- get.parameter(cf, 'plotEverySample', type='boolean')
   dir.create(out.dir, recursive=TRUE)

   # Parameters
   plotChromosomes   <- get.parameter(cf, 'plotChromosomes')
   upperLimit        <- get.parameter(cf, 'upperLimit', type='float')
   lowerLimit        <- get.parameter(cf, 'lowerLimit', type='float')
   undoSplits        <- get.parameter(cf, 'undoSplits', type='string')
   undoSD            <- get.parameter(cf, 'undoSD',     type='int')
   callProbabilities <- get.parameter(cf, 'callProbabilities', type='boolean')
   probMethod        <- get.parameter(cf, 'callProbMethod', type='string')
   chr.column        <- get.parameter(cf, 'chrColumn',      type='string')
   start.column      <- get.parameter(cf, 'bpStartCol',     type='string')
   end.column        <- get.parameter(cf, 'bpEndCol',       type='string')
   filter.na         <- get.parameter(cf, 'filterNA',       type='boolean')
   alpha             <- get.parameter(cf, 'alpha',          type='float')
   min.width         <- get.parameter(cf, 'minWidth',       type='int')

   # Check parameters
   if ((any(as.numeric(unlist(strsplit(plotChromosomes,","))) %in% (0:24))) == FALSE) {
      write.error(cf, 'Valid values for parameter plotChromosomes are [0-24], X or Y')
      return(INVALID_INPUT)
   }
   if (!(min.width %in% c(2:5))) {
      write.error(cf, 'Parameter minWidth must be between 2 and 5.')
      return(INVALID_INPUT)
   }

   # Check input
   if ((chr.column %in% names(geneAnnot)) == FALSE && 
       (start.column %in% names(geneAnnot)) == FALSE){
        write.error(cf, c('Invalid column names for chromosomal annotations. Current columns are ',
                           colnames(geneAnnot),' and parameters were ', chr.column, start.column, end.column))
        return(INVALID_INPUT)
   }
   if (!all(as.character(geneAnnot[,1]) %in% rownames(case)) || !all(rownames(case) %in% as.character(geneAnnot[,1]))){
      err1 <- geneAnnot[which((as.character(geneAnnot[,1]) %in% rownames(case)) == FALSE),1]
      err2 <- rownames(case)[which((rownames(case) %in% as.character(geneAnnot[,1])) == FALSE)]
      write.error(cf, c('caseChan and geneAnnotation ID rows differ. Check probes: ', err1, err2))
      return(INVALID_INPUT)
   }

   if (callProbabilities) filter.na <- TRUE

   chr   <- geneAnnot[,which(colnames(geneAnnot) == chr.column)[1]]
   start <- geneAnnot[,which(colnames(geneAnnot) == start.column)[1]]
   end   <- as.numeric(geneAnnot[,which(colnames(geneAnnot) == end.column)[1]])

   # Transform X and Y chromosomes to numbers.
   # Must be filtered for array CGH data but needed for SNP copy number data.
   if (any(chr == "X") == TRUE) {
     chr <- gsub("[X]", "23", chr)
   } 
   if (any(chr == "Y") == TRUE) {
     chr <- gsub("[Y]", "24", chr)
   }

   # Make sure vectors are numeric. That is, not character vectors.
   Chr      <- as.numeric(chr)
   Position <- as.numeric(start)

   # Drop probes with samples that have NA's
   if (filter.na){
       for (i in 1:ncol(case)){
           rowNulls <- which(is.na(case[,i]))
           if (length(rowNulls) > 0) rownames(case)[rowNulls] <- NA
       }

       notnulls  <- which(!is.na(rownames(case)))
       Chr       <- Chr[notnulls]
       Position  <- Position[notnulls]
       end       <- end[notnulls]
       case      <- case[notnulls,]
   }


   # Start segmentation algorithm
   # ----------------------------
   write.log(cf, "Segmenting CGH files...")
   CNA <- CNA(cbind(case), Chr, Position, data.type="logratio", sampleid = colnames(case))
   smoothed.CNA.object <- smooth.CNA(CNA)
   set.seed(589031)
   segment.smoothed.CNA.object <- segment(smoothed.CNA.object, 
                                          undo.splits = undoSplits,
                                          undo.SD     = undoSD,
                                          verbose     = 1,
                                          alpha       = alpha,
                                          min.width   = min.width)
   write.log(cf, "Segmentating CGH files...done.")

   cnaFreqs <- glFrequency(segment.smoothed.CNA.object, threshold=0.5)
   rownames(cnaFreqs) <- rownames(case)[order(Chr, Position)]
   CSV.write(get.output(cf, 'frequency'), as.matrix(cnaFreqs), first.cell=colnames(geneAnnot)[1])

   # Output raw segments
   # -------------------
   res <- matrix(0, nrow = nrow(segment.smoothed.CNA.object$data), ncol = ncol(segment.smoothed.CNA.object$data))
   rownames(res) <- rownames(case)[order(Chr, Position)]
   colnames(res) <- colnames(segment.smoothed.CNA.object$data)
   res[,1] <- Chr[order(Chr, Position)]
   res[,2] <- Position[order(Chr, Position)]
   segs <- cbind(segment.smoothed.CNA.object$output, segment.smoothed.CNA.object$segRows)
   for (i in 3:ncol(res)){
       sampl <- segs[which(segs[,'ID'] == colnames(res)[i]),]
       for (j in 1:nrow(sampl)){
           startRow <- as.numeric(sampl[j,'startRow'])
           endRow   <- as.numeric(sampl[j,'endRow'])
           res[startRow:endRow,i] <- sampl[j,'seg.mean']
       }
   }
   CSV.write(get.output(cf, 'rawSegments'), as.matrix(res), first.cell=colnames(geneAnnot)[1])

   # Estimate call probabilities by CGHcall or FastCall
   callProbs <- FALSE
   if (callProbabilities){
      callProbs <- callProbabilities(case=case, chr=Chr, start=Position, end=end, 
                                     chr.column=chr.column, start.column=start.column, end.column=end.column,
                                     segment=segment.smoothed.CNA.object, cf=cf, method=probMethod, docName=docName, out.dir=out.dir)
   }
   # Construct empty output if no probabilities were / could be estimated
   if (!callProbs){
      gainProbs <- data.frame(t(rep('NA', ncol(case)+5)))
      lossProbs <- data.frame(t(rep('NA', ncol(case)+5)))
      normProbs <- data.frame(t(rep('NA', ncol(case)+5)))
      colnames(gainProbs) <- c("ProbeName",chr.column,start.column,end.column,"meanProb",colnames(case)[1:ncol(case)])
      colnames(lossProbs) <- c("ProbeName",chr.column,start.column,end.column,"meanProb",colnames(case)[1:ncol(case)])
      colnames(normProbs) <- c("ProbeName",chr.column,start.column,end.column,"meanProb",colnames(case)[1:ncol(case)])
      CSV.write(get.output(cf,'gainProbs'), gainProbs)
      CSV.write(get.output(cf,'lossProbs'), lossProbs)
      CSV.write(get.output(cf,'normProbs'), normProbs)
   }

   Chr2      <- Chr[order(Chr, Position)]
   Position2 <- Position[order(Chr, Position)]
   case      <- case[order(Chr, Position),,drop=FALSE]
   Chr <- Chr2
   Position <- Position2

   # Calculate aberration call thresholds
   if (upperLimit == 0.0 && lowerLimit == 0.0) {
     write.log(cf, "Calculating vectors of predicted and observed intensities...")
     for (i in 1:length(segment.smoothed.CNA.object$output$num.mark)){
        for (j in 1:segment.smoothed.CNA.object$output$num.mark[i]) {
          if (i == 1 && j == 1) predValues <- segment.smoothed.CNA.object$output$seg.mean[i]
          else predValues <- c(predValues, segment.smoothed.CNA.object$output$seg.mean[i]) 
        }
     }
     obsValues <- as.numeric(unlist(segment.smoothed.CNA.object$data[3:length(segment.smoothed.CNA.object$data)]))
     write.log(cf, "Calculating vectors of predicted and observed intensities...done")
     tholds <- c(median(predValues, na.rm=TRUE)+2*sd(obsValues-predValues, na.rm=TRUE),median(predValues, na.rm=TRUE)-2*sd(obsValues-predValues, na.rm=TRUE))
   } else {
     tholds <- c(upperLimit, lowerLimit)
   }
   CSV.write(get.output(cf, 'tholds'), as.data.frame(tholds))
   CNA.output <- matrix(ncol=8)

   # --------------------------
   # Use thresholds for copy 
   # number aberration calls
   # --------------------------
   for (i in 1:nrow(segment.smoothed.CNA.object$output)){
       probeindeces <- which(Chr == segment.smoothed.CNA.object$output[i,2])
       probeStarts  <- which(Position[probeindeces] >= segment.smoothed.CNA.object$output[i,3])
       probeEnds    <- which(Position[probeindeces] <= segment.smoothed.CNA.object$output[i,4])
       probeList    <- intersect(probeStarts,probeEnds)
       probeindeces <- probeindeces[probeList]
       probenames   <- paste(rownames(case)[probeindeces], collapse=",")

       if (segment.smoothed.CNA.object$output[i,6] > tholds[1]) {
         call <- 1
       } else if (segment.smoothed.CNA.object$output[i,6] < tholds[2]) {
         call <- -1
       } else {
         call <- 0
       }
       if (call == 1 || call == -1 || get.parameter(cf, 'outputAllSegs', type='boolean')) {
         if (nrow(CNA.output) == 1 && is.na(CNA.output[1])) {
            CNA.output <- rbind(c(as.character(segment.smoothed.CNA.object$output[i,1:6]),
            	                  as.character(call),
                                  probenames[1]))
         } else {
            CNA.output <- rbind(CNA.output, c(as.character(segment.smoothed.CNA.object$output[i,1:6]),
                                              as.character(call),
                                              probenames[1]))
         }
       }
   }
   colnames(CNA.output) <- c(colnames(segment.smoothed.CNA.object$output), "CNA_call", "probes")
   CSV.write(get.output(cf, 'segments'), data.frame(CNA.output))
   
   # Plotting every chromosome separtely for each sample
   if (GWS){
     for (i in 1:length(colnames(case))){
       fig.name <- sprintf('%s-chromosomes_%i.png', InstanceName, i)
       fig.path <- file.path(out.dir, fig.name)
       png(fig.path, height=1000, width=1200, res=150)
       plot(subset(segment.smoothed.CNA.object, samplelist=i), plot.type = "s")
       invisible(dev.off())
        cat( latex.figure(fig.name, quote.capt = FALSE, image.scale=0.8),
             "\\clearpage",
             sep="\n", file=docName, append=TRUE)
     }
   
     # Plotting the whole genome with cnv segment means for each sample
     for (i in 1:length(colnames(case))){
       fig.name <- sprintf('%s-Segment_%i.png', InstanceName, i)
       fig.path <- file.path(out.dir, fig.name)
       png(fig.path, height=600, width=1000, res=150)
       par(mar=c(1, 1, 2, 1), oma=c(1, 1, 1, 1))
       plot(subset(segment.smoothed.CNA.object, samplelist=i), plot.type = "w", ylim=c(-5,5), pt.cex=1)
       abline(h=tholds[1], col="blue")
       abline(h=tholds[2], col="blue")
       capt <- paste("Chromosomes on x-axis")
       invisible(dev.off())
         cat( latex.figure(fig.name, caption=capt, quote.capt = FALSE, image.scale=1.0),
             #"\\clearpage",
             sep="\n", file=docName, append=TRUE)
     }
   }

   # Plotting alteration frequencies
   fig.name <- sprintf('%s-Frequency_%i.png', InstanceName, i)
   fig.path <- file.path(out.dir, fig.name)
   png(fig.path, height=1000, width=2000, res=120)
   plot(NA, ylim=c(-1,1), xlim=c(0,nrow(cnaFreqs)), ylab="CNA frequency", xlab="Chromosomes", axes=FALSE)
   axis(2, c(-1,-0.5,0,0.5,1))
   barplot(cnaFreqs[,4], col="red", add=TRUE, space=0, border="red", axes=FALSE)
   barplot(cnaFreqs[,5], col="blue", add=TRUE, space=0, border="blue", axes=FALSE)
   abline(h=0)
   labels <- vector()
   ticks <- vector()
   for (i in unique(cnaFreqs[,'chrom'])){
     ma <- max(cnaFreqs[which(cnaFreqs[,'chrom'] == i),'maploc'], na.rm=TRUE)
     mi <- min(cnaFreqs[which(cnaFreqs[,'chrom'] == i),'maploc'], na.rm=TRUE)
     if (i == unique(cnaFreqs[,'chrom'])[1]){
       labels[i] <- (which(cnaFreqs[,'maploc'] == ma)-which(cnaFreqs[,'maploc'] == mi))/2
     } else {
       labels[i] <- which(cnaFreqs[,'maploc'] == mi)+(which(cnaFreqs[,'maploc'] == ma)-which(cnaFreqs[,'maploc'] == mi))/2
     }
     ticks[i] <- which(cnaFreqs[,'maploc'] == ma)
   }
   ticks  <- na.omit(ticks)
   labels <- na.omit(labels)
   axis(1, at=c(0,ticks), labels=FALSE)
   axis(1, at=labels, labels=unique(cnaFreqs[,'chrom']), tick=FALSE)
   capt <- paste("Chromosomes on x-axis, alteration frequency on y-axis")
   invisible(dev.off())
   cat( latex.figure(fig.name, caption=capt, quote.capt = FALSE, image.scale=1.0),
        "\\clearpage",
        sep="\n", file=docName, append=TRUE)


   # Plotting certain chromosomes with cnv segment means for each sample
   chromes <- as.numeric(unlist(strsplit(plotChromosomes,",")))
   if (chromes[1] != 0) {
     for (i in 1:length(colnames(case))){
      for (j in 1:length(chromes)){
       fig.name <- sprintf('%s-Segment_%i_%i.png', InstanceName, i, chromes[j])
       fig.path <- file.path(out.dir, fig.name)
       png(fig.path, height=600, width=1000, res=150)
       par(mar=c(1, 1, 2, 1), oma=c(1, 1, 1, 1))
       plot(subset(segment.smoothed.CNA.object, samplelist=i, chromlist=chromes[j]), plot.type = "w", ylim=c(-5,5))
       abline(h=tholds[1], col="blue")
       abline(h=tholds[2], col="blue")
       capt <- paste("Chromosome",chromes[j])
       invisible(dev.off())
       if ((i %% 3) == 0) {
         cat( latex.figure(fig.name, caption=capt, quote.capt = FALSE, image.scale=0.65),
             "\\clearpage",
             sep="\n", file=docName, append=TRUE)
       } else {
         cat( latex.figure(fig.name, caption=capt, quote.capt = FALSE, image.scale=0.65),
             #"\\clearpage",
             sep="\n", file=docName, append=TRUE)
       }
      }
     }
  }
}

callProbabilities <- function(case, chr, start, end, chr.column, start.column, end.column, segment, cf, method, docName, out.dir){
    if (method == 'FastCall'){
        library(TASSO)
        # Transform data to be suitable for FastCall
        nroOfSamples <- length(colnames(segment$data[3:ncol(segment$data)]))
        segmResults  <- matrix(ncol=dim(case)[2], nrow=dim(case)[1])
        colnames(segmResults) <- colnames(case)
        for (i in 1:nroOfSamples){
            currentSample <- which(segment$output[,1] == colnames(segmResults)[i])
            for (j in 1:length(currentSample)){
                correctChr   <- which(chr   == segment$output[currentSample[j],2])
                correctLoci  <- intersect(which(start >= segment$output[currentSample[j],3]), which(start <= segment$output[currentSample[j],4]))
                correctProbe <- intersect(correctChr, correctLoci)
                segmResults[correctProbe,i] <- segment$output[currentSample[j],6]
            }
        }

        inputMatrix <- data.frame(rownames(case),chr,start,case[,1:ncol(case)], segmResults)

        results  <- FastCall(inputMatrix,
                             Nclass = 3,
                             sample.names=colnames(segment$data[3:ncol(segment$data)]))
        probgain <- results$Call[,(4+nroOfSamples):ncol(results$Call)]
        probloss <- results$Call[,(4+nroOfSamples):ncol(results$Call)]
        probnorm <- results$Call[,(4+nroOfSamples):ncol(results$Call)]

        for (i in 1:ncol(probgain)){
            gainCol <- which(results$Call[,(3+i)] ==  1)
            lossCol <- which(results$Call[,(3+i)] == -1)
            normCol <- which(results$Call[,(3+i)] ==  0)
            probgain[-gainCol,i] <- 0
            probloss[-lossCol,i] <- 0
            probnorm[-normCol,i] <- 0
        }

        averageGains  <- rowSums(probgain)/ncol(probgain)
        averageLosses <- rowSums(probloss)/ncol(probloss)
        averageNorms  <- rowSums(probnorm)/ncol(probnorm)

        gainProbs <- data.frame(results$Call[,1:3],
                                signif(averageGains,3), 
                                probgain)
        lossProbs <- data.frame(results$Call[,1:3],
                                signif(averageLosses,3), 
                                probloss)
        normProbs <- data.frame(results$Call[,1:3],
                                signif(averageNorms,3), 
                                probnorm)

        colnames(gainProbs) <- c("ProbeName",chr.column,start.column,"meanProb",substr(colnames(probgain), 13, nchar(colnames(probgain))))
        colnames(lossProbs) <- c("ProbeName",chr.column,start.column,"meanProb",substr(colnames(probloss), 13, nchar(colnames(probloss))))
        colnames(normProbs) <- c("ProbeName",chr.column,start.column,"meanProb",substr(colnames(probnorm), 13, nchar(colnames(probnorm))))
        CSV.write(get.output(cf,'gainProbs'), gainProbs)
        CSV.write(get.output(cf,'lossProbs'), lossProbs)
        CSV.write(get.output(cf,'normProbs'), normProbs)
        return(TRUE)
    } else if (method == 'CGHCall') {
        library(CGHcall)
        # Transform data to be suitable for CGHcall
        cghRawPre <- data.frame(rownames(case),chr,start,end,case[,1:ncol(case)])
        cghRawPre <- cghRawPre[order(cghRawPre[,2], cghRawPre[,3]),]
        commonProbes <- which(as.vector(cghRawPre[,1]) %in% rownames(segment$data))
        if (length(commonProbes) < 1) return(FALSE)
        cghRawPre <- cghRawPre[commonProbes,]
        cghRaw    <- make_cghRaw(cghRawPre)

        numclone <- segment$output$num.mark
        smrat <- segment$output$seg
        numsmrat <- cbind(smrat, numclone)
        repdata <- function(row) {
           rep(row[1], row[2])
        }

        makelist <- apply(numsmrat, 1, repdata)
        joined <- unlist(makelist)
        rm(makelist)
        joined    <- matrix(joined, ncol = ncol(cghRaw), byrow = FALSE)
        joined    <- CGHcall:::.assignNames(joined, cghRaw)
        segmented <- CGHcall:::.segFromRaw(cghRaw, joined)

        postsegnormalized.data <- postsegnormalize(segmented)

        nSegFit <- get.parameter(cf, 'nSegFit',      type='int')
        prior   <- get.parameter(cf, 'CGHCallPrior', type='string')
        results <- CGHcall(postsegnormalized.data, prior = prior, nsegfit=nSegFit, minlsforfit=0.5, nclass=3)
        results <- ExpandCGHcall(results,postsegnormalized.data)

        fig.name <- sprintf('%s-CGHcall.png', get.metadata(cf, 'instanceName'))
        fig.path <- file.path(out.dir, fig.name)
        png(fig.path, height=1000, width=2200, res=150)
        summaryPlot(results)
        invisible(dev.off())
        cat( latex.figure(fig.name, quote.capt = FALSE, image.width=12),
             "\\clearpage",
             sep="\n", file=docName, append=TRUE)

        averageGains  <- rowSums(probgain(results))/ncol(probgain(results))
        averageLosses <- rowSums(probloss(results))/ncol(probloss(results))
        averageNorms  <- rowSums(probnorm(results))/ncol(probnorm(results))

        # Output probabilities for gain, normal and loss in separate files
        gainProbs <- data.frame(rownames(probgain(results)),
                            featureData(results)$Chromosome,
                            featureData(results)$Start,
                            featureData(results)$End,
                            signif(averageGains,3),
                            probgain(results))
        lossProbs <- data.frame(rownames(probloss(results)),
                            featureData(results)$Chromosome,
                            featureData(results)$Start,
                            featureData(results)$End,
                            signif(averageLosses,3),
                            probloss(results))
        normProbs <- data.frame(rownames(probnorm(results)),
                            featureData(results)$Chromosome,
                            featureData(results)$Start,
                            featureData(results)$End,
                            signif(averageNorms,3),
                            probnorm(results))
        colnames(gainProbs) <- c("ProbeName",chr.column,start.column,end.column,"meanProb",colnames(probgain(results)))
        colnames(lossProbs) <- c("ProbeName",chr.column,start.column,end.column,"meanProb",colnames(probloss(results)))
        colnames(normProbs) <- c("ProbeName",chr.column,start.column,end.column,"meanProb",colnames(probnorm(results)))

        CSV.write(get.output(cf,'gainProbs'), gainProbs)
        CSV.write(get.output(cf,'lossProbs'), lossProbs)
        CSV.write(get.output(cf,'normProbs'), normProbs)
        return(TRUE)
    }
    return(FALSE)
}

main(execute)
