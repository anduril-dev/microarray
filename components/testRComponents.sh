COMPONENT_SRCS=`find . -iname '*.R'`

OUT_FILE=RComponentTest.txt
echo 'Test report for the R components' >  $OUT_FILE
echo '--------------------------------' >> $OUT_FILE

for SRC_FILE in $COMPONENT_SRCS
do
R --slave <<EOF
library('codetools')
main <- function(x) { 'Do not execute anything!' }
sink('$OUT_FILE', append=TRUE)

cat('\nScript $SRC_FILE:\n')
source('$SRC_FILE')

testTargets <- setdiff(ls(), 'main')
for (target in testTargets) {
  targetF <- get(target)
  if (class(targetF) == "function") {
     cat(sprintf('Validating %s...\n', target))
     checkUsage(targetF)
  }
}
sink()
EOF
done
