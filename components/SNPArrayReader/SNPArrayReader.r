library(componentSkeleton)
library(crlmm)
library(Biobase)

execute <- function(cf) {

   inputDir    <- get.input(cf, 'dataDir')
   samplenames <- as.matrix(CSV.read(get.input(cf, 'sampleNames')))
   useAffy5    <- get.parameter(cf, 'useAffy5')
   conf.limit  <- get.parameter(cf, 'confLimit')
   s2n.limit   <- get.parameter(cf, 'S2NoiseLimit')
   file.type   <- get.parameter(cf, 'fileType',       type='string')
   illu.cdf    <- get.parameter(cf, 'illuminaCDF',    type='string')
   fileCol     <- get.parameter(cf, 'fileCol',        type='string')
   sampleCol   <- get.parameter(cf, 'sampleCol',      type='string')
   useSymbols  <- get.parameter(cf, 'useSymbols',     type='boolean')
   getCopyNum  <- get.parameter(cf, 'getCopyNumbers', type='boolean')

   if (nchar(fileCol) > 0) {
       fileCol <- which(colnames(samplenames) == fileCol)
   } else {
       fileCol <- 1
   }

   if (nchar(sampleCol) > 0) {
       sampleCol <- which(colnames(samplenames) == sampleCol)
   } else {
       sampleCol <- 1
   }

   # Test for correct file type. Only allow 'A'ffymetrix or 'I'llumina
   if (!(file.type %in% c('A','I'))) {
        write.error(cf, paste('File type incorrectly specified:', file.type, 'Correct values: A, I'))
        return(INVALID_INPUT)
   }

   if (getCopyNum && file.type == 'I'){
        write.log(cf, paste('Cannot call copy numbers from Illumina arrays. Continuing SNP calling.'))
   }

   # Test for correct Illumina manifest file
   if (!(illu.cdf %in% c('human370v1c', 'human550v3b', 'human650v3a', 'human1mv1c', 
                         'human370quadv3c', 'human610quadv1b', 'human660quadv1a',
                         'human1mduov3b', 'humanomni1quadv1b', 'humanomniexpress12v1b'))) {
       write.error(cf, "Illumina cdf file incorrect")
       return(INVALID_INPUT)
   }

   # Process data
   if (file.type == 'I'){
       cdfLib <- paste(illu.cdf,"Crlmm", sep="")
       library(cdfLib, character.only=TRUE)
       results    <- crlmmIlluminaV2(path=inputDir, arrayNames=as.character(samplenames[,fileCol]), cdf=illu.cdf, SNRMin=s2n.limit)
   } else {
       inputFiles <- list.celfiles(inputDir, full.names = TRUE)
       inputFiles <- inputFiles[which(basename(inputFiles) %in% as.character(samplenames[,fileCol]))]

       if (length(inputFiles) == 0) {
           write.error(cf, 'No files defined in sampleNames found from dataDir.')
           return(INVALID_INPUT)
       }
       if (useAffy5) {
         library(genomewidesnp5Crlmm)
         if (getCopyNum){
             library(ff)
             ldPath(get.temp.dir(cf))
             results    <- genotype(inputFiles, batch=rep("batch1", length(inputFiles)), cdf="genomewidesnp5", SNRMin=s2n.limit)
             min.samp   <- 10
             if (length(inputFiles) < 10){ min.samp <- length(inputFiles) }
             results.ok <- crlmmCopynumber(results, MIN.SAMPLES=min.samp)
             if (!results.ok) write.error(cf, 'Error in copy number calling: exiting...')
         } else {
             results <- crlmm(inputFiles, cdf="genomewidesnp5", SNRMin=s2n.limit)
         }
       } else {
         library(genomewidesnp6Crlmm)
         if (getCopyNum){
             library(ff)
             ldPath(get.temp.dir(cf))
             results    <- genotype(inputFiles, batch=rep("batch1", length(inputFiles)), cdf="genomewidesnp6", SNRMin=s2n.limit)
             min.samp   <- 10
             if (length(inputFiles) < 10){ min.samp <- length(inputFiles) }
             results.ok <- crlmmCopynumber(results, MIN.SAMPLES=min.samp)
             if (!results.ok) write.error(cf, 'Error in copy number calling: exiting...')
         } else {
             results <- crlmm(inputFiles, cdf="genomewidesnp6", SNRMin=s2n.limit)
         }
       }
   }

   snps <- calls(results)

   if (nrow(samplenames) == ncol(snps)) {
     for (i in 1:nrow(samplenames)){
       sname <- which(colnames(snps) == samplenames[i,fileCol])
       colnames(snps)[sname] <- samplenames[i,sampleCol]
       if (getCopyNum){ 
         #colnames(copynumber)[sname] <- samplenames[i,sampleCol]
         sampleNames(results)[sname] <- samplenames[i,sampleCol]
       }
     }
   } else {
     write.log(cf, sprintf("Number of samplenames differ from number of samples in data. "))
     write.log(cf, sprintf("Using filenames as samplenames. "))
   }

   # Filter low signal samples and low confidence marker probes
   for (j in 1:ncol(snps)){
     conf <- which(confs(results[,j]) < conf.limit)
     snps[conf,j] <- NA
     #if (getCopyNum){ 
     #  copynumber[conf,j] <- NA 
     #}
   }

   if (useSymbols){
       snps[which(snps == 1)] <- 'AA'
       snps[which(snps == 2)] <- 'AB'
       snps[which(snps == 3)] <- 'BB'
   } else {
       snps[which(snps == 1)] <- 'AA'
       snps[which(snps == 2)] <- 'AT'
       snps[which(snps == 3)] <- 'TT'
   }

   # Write outputs
   markers <- as.data.frame(rownames(snps))
   colnames(markers) <- "markers"
   CSV.write(get.output(cf, 'markerNames'), markers)
   cnMarkers <- NA

   if (getCopyNum){
       cnMarkers <- rownames(totalCopynumber(results, j=1))
       probeAnnotation <- cbind(cnMarkers, chromosome(results), position(results))
       colnames(probeAnnotation) <- c("marker", "chromosome", "position")
   } else {
       probeAnnotation <- data.frame(NA)
   }

   write.log(cf, "Writing genotypeMatrix...")
   output <- get.output(cf, 'genotypeMatrix')
   write.table(t(c("Samples", rownames(snps))), file=output, quote=FALSE, sep='\t', col.names=FALSE, row.names=FALSE)
   for (i in 1:ncol(snps)){ 
     write.table(t(c(colnames(snps)[i], snps[,i])), file=output, quote=FALSE, sep='\t', col.names=FALSE, row.names=FALSE, append=TRUE)
   }

   write.log(cf, "Writing copynumberMatrix...")
   cnOutput <- get.output(cf, 'copynumberMatrix')
   if (getCopyNum){
     write.table(t(c("markers", sampleNames(results))), file=cnOutput, quote=FALSE, sep='\t', col.names=FALSE, row.names=FALSE)
     fnames <- as.vector(cnMarkers)
     step   <- 10000
     len    <- length(fnames)
     ind    <- seq(to=len-1, from=1, by=step)

     for (i in ind){
       ss     <- i:min(i+step, len)
       tCN    <- totalCopynumber(results, i = ss)
       mNames <- fnames[ss]
       for (j in 1:length(mNames)){
           write.table(t(c(mNames[j], tCN[j,])), file=cnOutput, quote=FALSE, sep='\t', col.names=FALSE, row.names=FALSE, append=TRUE)
       }
     }
     if (len != nrow(probeAnnotation)){
        write.log(cf, "Warning: Number of probes with annotations does not match number of probes in copynumberMatrix.")
     }
   } else {
     CSV.write(cnOutput, data.frame(NA))
   }

   CSV.write(get.output(cf, 'probeAnnotation'),  probeAnnotation)
}

main(execute)
