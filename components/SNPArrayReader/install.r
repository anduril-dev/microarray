

main <- function(argv) {
    if (length(argv) != 1) {
        cat('Usage: Rscript install.r <component-root-dir>\n')
        cat('  Copies a CEL file and two IDAT files from the hapmapsnp6 and hapmap370k packages into test directory.\n')
        quit(status=1)
    }
    
    target  <- file.path(argv[[1]], 'testcases', 'case1', 'input', 'dataDir', 'chip1.cel')
    target2 <- file.path(argv[[1]], 'testcases', 'case2', 'input', 'dataDir', 'chip1.cel')
    target3 <- file.path(argv[[1]], 'testcases', 'case3', 'input', 'dataDir', 'chip1_Grn.idat')
    target4 <- file.path(argv[[1]], 'testcases', 'case3', 'input', 'dataDir', 'chip1_Red.idat')

    if (any( !file.exists(c(target,target2,target3,target4)) ) ) {
    
    library(crlmm)
    library(hapmapsnp6)
    library(hapmap370k)
    celpath <- system.file("celFiles", package="hapmapsnp6")
    filename <- list.celfiles(path=celpath, full.names=TRUE)[[1]]

    illupath <- system.file("idatFiles", package="hapmap370k")
    filenameI1 <- list.files(path=illupath, full.names=TRUE)[[1]]
    filenameI2 <- list.files(path=illupath, full.names=TRUE)[[2]]
    
    
    file.copy(filename, target)
    file.copy(filename, target2)
    file.copy(filenameI1, target3)
    file.copy(filenameI2, target4)
    }
    return(invisible(NULL))
}

if (!interactive()) main(commandArgs(TRUE))
