<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>SNPArrayReader</name>
    <version>1.7</version>
    <doc> Imports genotype data from Illumina SNP and Affymetrix SNP 6.0 or 5.0 files. </doc>
    <author email="Riku.Louhimo@Helsinki.FI">Riku Louhimo</author>
    <category>Data Import</category>
    <category>Affy</category>
    <category>Illumina</category>
    <category>SNP</category>
    <launcher type="R">
        <argument name="file" value="SNPArrayReader.r" />
    </launcher>
    <requires URL="http://www.r-project.org/" type="manual">R</requires>
    <requires type="R-bioconductor" URL="http://www.bioconductor.org/packages/release/bioc/html/crlmm.html">crlmm</requires>
    <requires type="R-package" URL="http://www.bioconductor.org/packages/2.8/data/annotation/html/genomewidesnp5Crlmm.html">genomewidesnp5Crlmm</requires>
    <requires type="R-package" URL="http://www.bioconductor.org/packages/2.8/data/annotation/html/genomewidesnp6Crlmm.html">genomewidesnp6Crlmm</requires>
    <requires type="R-package" URL="http://bioconductor.uib.no/2.7/data/annotation/html/human370v1cCrlmm.html">human370v1cCrlmm</requires>
    <inputs>
        <input name="dataDir" type="BinaryFolder">
            <doc>Input file directory.</doc>
        </input>
        <input name="sampleNames" type="CSV">
            <doc> This file should contain the wanted
		  names for the samples. 
            </doc>
        </input>
    </inputs>
    <outputs>
        <output name="genotypeMatrix" type="SNPMatrix">
    	  <doc> Matrix containing the markers as columns and
    		the samples as rows. </doc>
    	</output>
    	<output name="markerNames" type="IDList">
    	  <doc> List containing the markernames. </doc>
    	</output>
    	<output name="copynumberMatrix" type="LogMatrix">
    	  <doc> Matrix containing normalized locus specific
                logarithmic copy number intensities.
          </doc>
    	</output>
    	<output name="probeAnnotation" type="CSV">
    	  <doc> Annotations for markers.
          </doc>
    	</output>
    </outputs>
    <parameters>
        <parameter name="getCopyNumbers" type="boolean" default="false">
          <doc>Call copy numbers from copy number proces on AffyMetrix arrays.</doc>
        </parameter>
    	<parameter name="confLimit" type="float" default="0.95">
    	  <doc> Markers with a lower confidence value are set NA.
    	  </doc>
    	</parameter>
    	<parameter name="S2NoiseLimit" type="int" default="5">
    	  <doc> Samples with lower Signal-to-Noise ratio are dropped.
    	  </doc>
    	</parameter>
    	<parameter name="useAffy5" type="boolean" default="false">
    	  <doc> Set this to true if input datatype is Affy SNP 5.0. Ignored
                    if fileType='I'.
    	  </doc>
    	</parameter>
    	<parameter name="fileType" type="string">
    	  <doc> Set the type of input files. Valid values are 'I' for Illumina data
                    and 'A' for Affymetrix data.
    	  </doc>
    	</parameter>
    	<parameter name="fileCol" type="string" default="">
    	  <doc> Column in sampleNames for file names. Default is the first column.
                See also the sampleCol parameter.
    	  </doc>
    	</parameter>
	    <parameter name="sampleCol" type="string" default="">
    	  <doc> Column in sampleNames for sample (human readable) names. Default
                is the first column. See also the fileCol parameter.
    	  </doc>
    	</parameter>
    	<parameter name="useSymbols" type="boolean" default="true">
              <doc> Set the form of the output genotypes. If true then mock genotypes
                    'AA', 'AB' and 'BB' are used. If false, pseudo-real genotypes 'AA',
                    'AT' and 'TT' are used.
              </doc>
    	</parameter>
    	<parameter name="illuminaCDF" type="string" default="human370v1c">
    	  <doc> Illumina chip manifest, one of 'human370v1c', 'human550v3b', 'human650v3a', 
                'human1mv1c', 'human660quadv1a', 'human1mduov3b', 'humanomni1quadv1b', 'humanomniexpress12v1b',
                'human370quadv3c', 'human610quadv1b'.
    	  </doc>
    	</parameter>
    </parameters>
</component>
