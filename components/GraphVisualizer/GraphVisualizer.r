library(componentSkeleton)
library(igraph)

execute <- function(cf) {
    myName <- get.metadata(cf, 'instanceName')
    graph <- read.graph(get.input(cf, 'graph'), format='graphml')
    if (get.parameter(cf, 'simplify')) {
        graph <- simplify(graph)
    }

    nodeCount <- vcount(graph)
    write.log(cf, sprintf('%d nodes, %d edges, is directed: %s',
        nodeCount, ecount(graph), is.directed(graph)))

    out.dir <- get.output(cf, 'figure')
    if (!file.exists(out.dir)) dir.create(out.dir, recursive=TRUE)

    minSize <- get.parameter(cf, 'minSize', 'int')
    if (nodeCount < minSize) {
       write.log(cf, 'Too few nodes to render an image.')
       tex <- paste('%',
                    sprintf('%% Skipped a graph with %d nodes: %s',
                            nodeCount, myName),
                    '%', sep='\n')
       latex.write.main(cf, 'figure', tex)
       return(0)
    }
    
    layout.type <- get.parameter(cf, 'layout') 
    if (layout.type == 'hierarchical') {
        graphviz.exec <- get.parameter(cf, 'dot')
    } else if (layout.type == 'spring') {
        graphviz.exec <- get.parameter(cf, 'neato')
    } else if (layout.type == 'spring2') {
        graphviz.exec <- get.parameter(cf, 'fdp')
    } else if (layout.type == 'radial') {
        graphviz.exec <- get.parameter(cf, 'twopi')
    } else if (layout.type == 'circular') {
        graphviz.exec <- get.parameter(cf, 'circo')
    } else {
        write.error(cf, paste('Invalid layout parameter:', layout.type))
        return(PARAMETER_ERROR)
    }
    
    pdf.file    <- sprintf('%s-graph.pdf', myName)
    ps2pdf.exec <- get.parameter(cf, 'ps2pdf')
    pdf.output  <- file.path(out.dir, pdf.file)
    
    title.attributes <- split.trim(get.parameter(cf, 'titleAttribute'), ',')
    for (title.attribute in title.attributes) { 
        if (title.attribute %in% list.vertex.attributes(graph)) {
            value <- get.vertex.attribute(graph, title.attribute)
            graph <- set.vertex.attribute(graph, 'label', value=value)
            break
        }
    }

    title.attributes <- split.trim(get.parameter(cf, 'edgeTitle'), ',')
    for (title.attribute in title.attributes) { 
        if (title.attribute %in% list.edge.attributes(graph)) {
            value <- get.edge.attribute(graph, title.attribute)
            graph <- set.edge.attribute(graph, 'label', value=value)
            break
        }
    }

    graph <- set.attribute(cf, graph, 'arrowhead', FALSE, TRUE)
    graph <- set.attribute(cf, graph, 'arrowtail', FALSE, TRUE)
    graph <- set.attribute(cf, graph, 'bgcolor', FALSE, FALSE, TRUE)
    graph <- set.attribute(cf, graph, 'edgecolor', FALSE, TRUE, attr.name='color')
    graph <- set.attribute(cf, graph, 'fillcolor', TRUE, FALSE)
    graph <- set.attribute(cf, graph, 'fontcolor', TRUE, TRUE)
    graph <- set.attribute(cf, graph, 'margin', TRUE, FALSE)
    graph <- set.attribute(cf, graph, 'nodecolor', TRUE, FALSE, attr.name='color')
    graph <- set.attribute(cf, graph, 'overlap', FALSE, FALSE, TRUE)
    graph <- set.attribute(cf, graph, 'rankdir', FALSE, FALSE, TRUE)
    graph <- set.attribute(cf, graph, 'size', FALSE, FALSE, TRUE)
    graph <- set.attribute(cf, graph, 'shape', TRUE, FALSE)
    graph <- set.attribute(cf, graph, 'splines', FALSE, FALSE, TRUE)
    
    height.val <- get.parameter(cf, 'height', 'float')
    if (height.val > 0) {
        graph <- set.vertex.attribute(graph, 'height', value=height.val)
    }
    
    width.val <- get.parameter(cf, 'width', 'float')
    if (width.val > 0) {
        graph <- set.vertex.attribute(graph, 'width', value=width.val)
    }

    fontsize <- get.parameter(cf, 'fontsize', 'float')
    if (fontsize > 0) {
        graph <- set.vertex.attribute(graph, 'fontsize', value=fontsize)
        graph <- set.edge.attribute(graph, 'fontsize', value=fontsize)
    }
    
    if (!is.null(V(graph)$fillcolor)) {
        fc <- V(graph)$fillcolor
        has.fc <- !is.na(fc) & nchar(fc) > 0
        V(graph)$style <- ''
        V(graph)[has.fc]$style <- 'filled'
    }
    
    graphviz.file <- tempfile()
    dot.source <- format.dot(graph)
    cat(dot.source, file=graphviz.file)
    execute.graphviz(cf, graphviz.file, graphviz.exec, ps2pdf.exec, pdf.output)
    
    caption <- get.parameter(cf, 'reportCaption')
    if (nchar(caption) == 0) caption <- NULL
    report.width  <- get.parameter(cf, 'reportWidth',  'float')
    report.height <- get.parameter(cf, 'reportHeight', 'float')
    tex <- latex.figure(pdf.file, caption=caption, image.width=report.width, image.height=report.height, quote.capt=FALSE,
                        fig.label=sprintf('fig:%s',myName))
    if (nchar(tmp<-get.parameter(cf, 'latexHead')) > 0) tex <- c(tmp, tex)
    if (nchar(tmp<-get.parameter(cf, 'latexTail')) > 0) tex <- c(tex, tmp)
    latex.write.main(cf, 'figure', tex)
    
    return(0)
}

format.dot <- function(graph) {
    stopifnot(is.igraph(graph))
    
    append.attribute <- function(dot.lines, attr.name, value) {
        if (!is.null(value) && !is.na(value) && nchar(value) > 0) {
            if (is.character(value)) value <- paste('"', value, '"', sep='')
            else value <- format(value, scientific=FALSE)
            dot.lines <- c(dot.lines, sprintf('    %s=%s', attr.name, value))
        }
        return(dot.lines)        
    }
    
    dot <- character()
    
    if (is.directed(graph)) {
        graph.header <- 'digraph'
        edge.sep <- '->'
    } else {
        graph.header <- 'graph'
        edge.sep <- '--'
    }
    
    dot <- c(dot, sprintf('%s G {', graph.header))
    
    dot <- c(dot, 'graph [')
    for (attr.name in list.graph.attributes(graph)) {
        value <- get.graph.attribute(graph, attr.name)
        dot <- append.attribute(dot, attr.name, value)
    }
    dot <- c(dot, '];')
    
    for (vertex in V(graph)) {
        dot <- c(dot, sprintf('  %s [', vertex))
        for (attr.name in list.vertex.attributes(graph)) {
            value <- get.vertex.attribute(graph, attr.name, vertex)
            dot <- append.attribute(dot, attr.name, value)
        }
        dot <- c(dot, '  ];')
    }

    for (edge in E(graph)) {
        vertices <- get.edge(graph, edge)
        from <- vertices[1] 
        to <- vertices[2]
        dot <- c(dot, sprintf('  %s %s %s [', from, edge.sep, to))
        for (attr.name in list.edge.attributes(graph)) {
            value <- get.edge.attribute(graph, attr.name, edge)
            dot <- append.attribute(dot, attr.name, value)
        }
        dot <- c(dot, '  ];')
    }
    
    dot <- c(dot, '}')
    return(paste(dot, collapse='\n'))
}

set.attribute <- function(cf, graph, parameter.name, for.nodes, for.edges, for.graph=FALSE, attr.name=NULL) {
    stopifnot(is.logical(for.nodes))
    stopifnot(is.logical(for.edges))
    stopifnot(is.logical(for.graph))
    
    if (is.null(attr.name)) attr.name <- parameter.name
    
    value <- get.parameter(cf, parameter.name)
    if (!is.null(value) && nchar(value) > 0) {
        if (for.nodes) {
            graph <- set.vertex.attribute(graph, attr.name, value=value)
        }
        if (for.edges) {
            graph <- set.edge.attribute(graph, attr.name, value=value)
        }
        if (for.graph) {
            graph <- set.graph.attribute(graph, attr.name, value=value)
        }
    }
    return(graph)
}

execute.graphviz <- function(cf, graphviz.input, graphviz.executable, ps2pdf.executable, pdf.output.file, type='ps2') {
    cmd <- sprintf('%s -V', graphviz.executable)
    status <- system(cmd)
    if (status != 0) {
        stop(sprintf('Graphviz returned error status %d when called with: %s', status, cmd))
    }
    
    ps.output <- tempfile()
    cmd <- sprintf('%s -T%s "%s" -o "%s"', graphviz.executable, type, graphviz.input, ps.output)
    cmd <- gsub('\\', '/', cmd, fixed=TRUE)  
    write.log(cf, sprintf('Executing %s', cmd))
    status <- system(cmd)
    if (status != 0) {
        content <- paste(readLines(graphviz.input), sep='\n')
        write.log(cf, paste('Graphviz input:', content))
        stop(sprintf('Graphviz returned error status %d when called with: %s', status, cmd))
    }
    
    ## Output pdf first to a temp file to avoid chrashing of ps2pdf with filenames
    ## longer than 256 characters.
    pdf.output.temp <- tempfile()
    cmd <- sprintf('%s "%s" "%s"', ps2pdf.executable, ps.output, pdf.output.temp)
    cmd <- gsub('\\', '/', cmd, fixed=TRUE)
    write.log(cf, sprintf('Executing %s', cmd))
    status <- system(cmd)
    if (status != 0) {
        stop(sprintf('ps2pdf returned error status %d when called with: %s', status, cmd))
    }

    cmd <- sprintf('cp "%s" "%s"', pdf.output.temp, pdf.output.file)
    cmd <- gsub('\\', '/', cmd, fixed=TRUE)
    write.log(cf, sprintf('Executing %s', cmd))
    status <- system(cmd)
    if (status != 0) {
        stop(sprintf('cp returned error status %d when called with: %s', status, cmd))
    }
}

main(execute)
