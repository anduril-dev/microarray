library(componentSkeleton)

combine.columns <- function(matr, fun=median) {
    n <- nrow(matr)
    m2 <- sapply(1:n, function(i) do.call(fun, list(matr[i,], na.rm=TRUE)))
    return(matrix(m2))
}

combine.samples <- function(cf, group.type, expr, column.names, min.defined) {
    stopifnot(is.matrix(expr))
    stopifnot(is.character(column.names))
    
    columns <- match(column.names, colnames(expr))
    
    if (group.type %in% c('mean','median', 'sample')) {
        if (get.parameter(cf,'geometricMean','boolean')==FALSE) {
            expr.col <-switch(group.type,"mean"= combine.columns(expr[,columns, drop=FALSE],fun = function(x,na.rm) log2(mean(2^x,na.rm=na.rm))),
                          "median" = combine.columns(expr[,columns, drop=FALSE],fun = function(x,na.rm) log2(median(2^x,na.rm=na.rm))),
                          "sample" =  combine.columns(expr[,columns, drop=FALSE],fun= function(x,na.rm) log2(median(2^x,na.rm=na.rm))))
       }
       else {
            expr.col <-switch(group.type,"mean"= combine.columns(expr[,columns, drop=FALSE],fun = mean),
                          "median" = combine.columns(expr[,columns, drop=FALSE],fun = median),
                          "sample" =  combine.columns(expr[,columns, drop=FALSE],fun = median))                

       }
    }
    else if (group.type == 'ratio') {
        stopifnot(length(column.names) == 2)
        col1 <- column.names[1]
        col2 <- column.names[2]
        expr.col <- expr[,col1] - expr[,col2]
    } else {
        stop(sprintf('Invalid group type: %s', group.type))        
    }
    
    defined <- sapply(1:nrow(expr), function(i) length(which(!is.na(expr[i,columns,drop=FALSE]))) >= min.defined)
#	expr.col[is.na(expr.col)] <- 0
    expr.col[!defined] <- NA
    
    return(expr.col)
}

execute <- function(cf) {
    expr <- LogMatrix.read(get.input(cf, 'expr'))
    groups <- SampleGroupTable.read(get.input(cf, 'groups'))
    
    if (get.parameter(cf, 'includeOriginal', 'boolean')) {
        for (column in colnames(expr)) {
            groups <- SampleGroupTable.add.group(groups, column, SAMPLE.GROUP.TABLE.SAMPLE, column, column)
        }
    }

    group.ids <- split.trim(get.parameter(cf, 'groupIDs'), ',')
    if (length(group.ids) == 0) {
        group.ids <- SampleGroupTable.get.groups(groups)
    }
    
    threshold.ratio <- get.parameter(cf, 'thresholdRatio', 'float')
    if (threshold.ratio < 0 || threshold.ratio > 1) {
        write.error(cf, sprintf('thresholdRatio must be between 0 and 1 (is %s)', threshold.ratio)) 
        return(PARAMETER_ERROR)
    }
    
    new.expr <- matrix(NA, nrow(expr), length(group.ids))
    
    rownames(new.expr) <- rownames(expr)
    colnames(new.expr) <- group.ids
    
    for (i in 1:length(group.ids)) {
        target.id <- group.ids[i]
        samples <- SampleGroupTable.get.source.groups(groups, target.id)
        
        if (target.id %in% colnames(expr)) {
            index <- match(target.id, colnames(expr))
            new.expr[,i] <- expr[,index]
        }
        else {
            for (sample.id in samples) {
                if (!sample.id %in% colnames(expr)) {
                    write.error(cf,
                        sprintf('Sample %s in sample group for %s is not found in expression matrix', sample.id, target.id))
                    return(INVALID_INPUT)
                }
            }
            
            group.type <- SampleGroupTable.get.type(groups, target.id)
            if (is.null(group.type)) {
                write.error(cf, sprintf('Group not found in sample group table: %s', target.id))
                return(PARAMETER_ERROR)
            }
            if (group.type == SAMPLE.GROUP.TABLE.SAMPLE) {
                if (length(samples) != 1) {
                    write.error(cf, sprintf('Group %s: group type %s requires exactly 1 source group, got %d',
                        target.id, group.type, length(samples)))
                    return(INVALID_INPUT)
                }
            }
            else if (group.type == SAMPLE.GROUP.TABLE.RATIO) {
                if (length(samples) != 2) {
                    write.error(cf, sprintf('Group %s: group type %s requires exactly 2 source groups, got %d',
                        target.id, group.type, length(samples)))
                    return(INVALID_INPUT)
                }
            }
            
            threshold <- ceiling(length(samples) * threshold.ratio)
            write.log(cf, sprintf('Group %s: samples %s, threshold %s', target.id, paste(samples, collapse=','), threshold))
            new.column <- combine.samples(cf, group.type, expr, samples, threshold)
            new.expr[,i] <- new.column
            
            expr <- cbind(expr, new.column)
            colnames(expr)[ncol(expr)] <- target.id
        }
    }

    first.cell <- CSV.read.first.cell(get.input(cf, 'expr'))
    CSV.write(get.output(cf, 'expr'), new.expr, first.cell=first.cell)
    return(0)
}

main(execute)
