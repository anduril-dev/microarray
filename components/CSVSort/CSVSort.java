import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.ListIterator;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class CSVSort extends SkeletonComponent {
    
    private enum FieldType{
        STRING{
            public  String  toString(){
                return  "d";
//              return  "i";
            }
        },
/*      WORD{
            public  String  toString(){
                return  "";
            }
        },*/
        NATURAL{
            public  String  toString(){
                return  "n";
            }
        },
        REAL{
            public  String  toString(){
                return  "g";
            }
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        new CSVSort().run(args);
    }

    @Override
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        // TODO Auto-generated method stub
        
        String regex=cf.getParameter("regex");
        LinkedList<File> inputCSVs=new LinkedList<File>();
        File csv=cf.getInput("csv");
        File array=cf.getInputArrayIndex("array");
        IndexFile Idx;
        File tmp=cf.getTempDir();
        Hashtable<Integer,Boolean> sortAscending=new Hashtable<Integer,Boolean>();
        Hashtable<String,String> sortModes=new Hashtable<String,String>();
        Hashtable<String,String> fieldType=new Hashtable<String,String>();
        Hashtable<Integer,FieldType> keyType=new Hashtable<Integer,FieldType>();
        Hashtable<Integer,Boolean> ignoreCase=new Hashtable<Integer,Boolean>();
        String mode=cf.getParameter("mode");
        String types=cf.getParameter("types");
        String unique="";
        if (cf.getBooleanParameter("unique"))
        {   unique=" -u "; }
        
        String generalCmdLine=cf.getParameter("sortCmd")+unique+" --temporary-directory="+tmp.getAbsolutePath()+
        " --ignore-leading-blanks"+(cf.getBooleanParameter("stable")?" --stable":"");
        
        String  preformatCmdLine="sed -e \'s,\",,g\' -e \'s, ,%20,g\'";
        String  postformatCmdLine="sed \'s,%20, ,g\'";
        
        String  fieldsCmdLine="";
        String  preffixCmdLine;
        
        if(mode.length()>0)
        {
            String[]    modes=AsserUtil.split(mode, ",");
            for(String  keyMode:modes)
            {
                if(keyMode.contains("="))
                {
                    String[]    key_mode=AsserUtil.split(keyMode, "=");
                    key_mode[0]=key_mode[0].replace(" ", "");
                    key_mode[1]=key_mode[1].replace(" ", "");
                    sortModes.put(key_mode[0], key_mode[1]);
//              System.out.println("key_mode[0]="+key_mode[0]+"\nkey_mode[1]="+key_mode[1]);
                } else
                {
                    System.out.println("ERROR in CSVSort: wrong format for parameter \"mode\"."+
                            "Consult the component documentation.");
                    System.exit(1);
                }
            }
        }
        
        if(types.length()>0)
        {
            String[]    typesArray=AsserUtil.split(types, ",");
            for(String  type:typesArray)
            {
                if(type.contains("="))
                {
                    String[]    key_type=AsserUtil.split(type, "=");
                    key_type[0]=key_type[0].replace(" ", "");
                    key_type[1]=key_type[1].replace(" ", "");
                    fieldType.put(key_type[0], key_type[1]);
                } else
                {
                    System.out.println("ERROR in CSVSort: wrong format for parameter \"types\"."+
                            "Consult the component documentation.");
                    System.exit(1);
                }
            }
        }
        
        if(csv!=null) 
        { 
            inputCSVs.add(csv);
        }
        if(array!=null)
        {
            Idx=IndexFile.read(array);
            System.out.println("We will merge/sort files:");
            if(csv!=null) 
            {
                System.out.println(csv.getAbsolutePath());
            }
            IndexFile.read(array);
            for(int i=0;i<Idx.size();i++)
            {
                if(Idx.getKey(i).matches(regex))
                {
                    inputCSVs.add(Idx.getFile(i));
                    System.out.println(Idx.getFile(i));
                }
            }
        }
        if(inputCSVs.size()<1)
        {
            return  ErrorCode.INVALID_INPUT;
        }
        CSVParser   firstCsv=new    CSVParser(inputCSVs.getFirst(),"\t",0);
        
        String  headerLine="";
        String  headerLine2="";
        
        String[] columns=firstCsv.getColumnNames();
        for(String field:columns)
        {
            headerLine+=(headerLine.length()>0?"\t":"")+
                        field;
            headerLine2+=(headerLine2.length()>0?"\t":"")+
                        "\\\""+field+"\\\"";
        }
        
        FileWriter CSVWriter=new FileWriter(cf.getOutput("csv"));
        CSVWriter.write(headerLine+"\n");
        CSVWriter.close();
        
        String fileListStr;
        if(inputCSVs.size()==1)
        {
            fileListStr=" "+(cf.getBooleanParameter("preprocess")?"":inputCSVs.getFirst().getAbsolutePath());
            preffixCmdLine=cf.getBooleanParameter("preprocess")?("cat "+inputCSVs.getFirst().getAbsolutePath()+
                    " | "+preformatCmdLine+" | "):"";
        }
        else
        {
            FileWriter  fileListWriter=new  FileWriter(tmp.getAbsolutePath()+File.separator+"_filelist");
            preffixCmdLine="";
            for(File    file:inputCSVs)
            {
                if(cf.getBooleanParameter("preprocess"))
                {
                    String  tmpFileName=cf.getTempDir().getAbsolutePath()+File.separator+file.getName();
                    preprocessCSV(file,new  File(tmpFileName));
                    fileListWriter.write(tmpFileName+"\0");
                } else
                {
                    fileListWriter.write(file.getAbsolutePath()+"\0");
                }
            }
            fileListWriter.close();
            fileListStr=" --files0-from="+tmp.getAbsolutePath()+File.separator+"_filelist";
        }
        
        LinkedList<Integer> keyPos=new LinkedList<Integer>();
        String keyColumnsStr=cf.getParameter("keyColumns");
        LinkedList<String> ignoreCaseFields=new LinkedList<String>();
        String[] ignoreCaseStr;
        
        if(cf.getParameter("ignoreCase").length()>0)
        {
            ignoreCaseStr=AsserUtil.split(cf.getParameter("ignoreCase"),",");
            for(String  field:ignoreCaseStr)
            {
                ignoreCaseFields.add(field);
            }
        }
        
        String[]    keyColumns;
        if(keyColumnsStr.length()==0)
        {
            keyColumns=new  String[]{firstCsv.getColumnNames()[0]};
        } else
        {
            keyColumns=AsserUtil.split(keyColumnsStr, ",");
        }   
        
        for(String  column:keyColumns)
        {
            int pos;
            keyPos.add(pos=(firstCsv.getColumnIndex(column)+1));
            boolean asc;
            if(!sortModes.containsKey(column))
            {
                asc=true;
            } else
            {
                asc=!sortModes.get(column).equalsIgnoreCase("des");
//                  System.out.println("------"+sortModes.get(column)+"-------");
            }
            sortAscending.put(pos,asc);
            FieldType   type;
//              System.out.println("***"+column+"***");
            if(!fieldType.containsKey(column))
            {
                type=FieldType.NATURAL;
            } else
            {
                String typeStr=fieldType.get(column);
                if(typeStr==null)
                {   type=FieldType.NATURAL; }
                else if(typeStr.equalsIgnoreCase("string"))
                {   type=FieldType.STRING; }
//              else if(typeStr.equalsIgnoreCase("word"))
//              {         type=FieldType.WORD; }
                else if(typeStr.equalsIgnoreCase("natural"))
                {   type=FieldType.NATURAL; }
                else if(typeStr.equalsIgnoreCase("real"))
                {   type=FieldType.REAL; }
                else 
                {   type=FieldType.NATURAL; }
            }
//              System.out.println(">>>"+type+"<<<");
            keyType.put(pos,type);
            ignoreCase.put(pos, ignoreCaseFields.contains(column));
        }
        
        // Generating fields command line args substring
        for(Integer pos:keyPos)
        {
            String  posStr=pos.toString()+keyType.get(pos).toString()+
                            (ignoreCase.get(pos)?"f":"")+
                            (sortAscending.get(pos)?"":"r");
            fieldsCmdLine+=" --key="+posStr+","+posStr;
        }
        
        // Checking first if files are sorted
        FileWriter choicesWriter=new FileWriter(cf.getOutput("_choices"));
        if(!cf.getBooleanParameter("skipSortCheck"))
        {
            boolean sorted=true;
            ListIterator<File>  itr=inputCSVs.listIterator();
            while(sorted&&(itr.hasNext()))
            {
                File    file=itr.next();
                String  checkCMD="tail --lines=+2 "+file.getAbsolutePath()+" | "+
                            (cf.getBooleanParameter("preprocess")?(preformatCmdLine+" | "):"")+
                            generalCmdLine+" --check"+fieldsCmdLine;
                FileWriter  cmdFile=new FileWriter(tmp.getAbsolutePath()+File.separator+"_cmd.sh");
                cmdFile.write(checkCMD);
                cmdFile.close();
                
                System.out.println("Invoking: "+checkCMD);
                int value=IOTools.launch("bash "+tmp.getAbsolutePath()+File.separator+"_cmd.sh");
                System.out.println("The call returned value "+value);
                sorted=(value==0);
            }
            if(sorted)
            { 
                choicesWriter.write("sorted");
            } else 
            {   
                choicesWriter.write("unsorted");
            }
        } else
        {
            choicesWriter.write("unknown");
        }
        choicesWriter.close();
        
        // Sorting/merging CSVs
        if(!cf.getBooleanParameter("noSort"))
        {
            String sortCMD=preffixCmdLine+generalCmdLine+fieldsCmdLine+
                        fileListStr+
                        (cf.getBooleanParameter("preprocess")?
                                (" | "+postformatCmdLine):"")+
                        " | grep -v \""+headerLine+"\" "+
                        " | grep -v \""+headerLine2+"\">>"+cf.getOutput("csv").getAbsolutePath();
            FileWriter cmdFile=new FileWriter(tmp.getAbsolutePath()+File.separator+"_cmd.sh");
            cmdFile.write(sortCMD);
            cmdFile.close();
            System.out.println("Invoking: "+sortCMD);
            int value=IOTools.launch("bash "+tmp.getAbsolutePath()+File.separator+"_cmd.sh");
            System.out.println("The call returned value "+value);
        }
        return ErrorCode.OK;
    }

    void preprocessCSV(File src,File dest)
    {
        try 
        {
            CSVParser srcCSV=new  CSVParser(src,"\t",0);
            CSVWriter destCSV=new CSVWriter(preprocessStr(srcCSV.getColumnNames()), dest,false);
            
            for(String[] line:srcCSV)
            {
                for(String entry:preprocessStr(line))
                {
                    destCSV.write(entry,false);
                }
            }
            destCSV.close();
            srcCSV.close();
        } catch (IOException e) 
        {
            // TODO Auto-generated catch block
            // In the case of IOException, the component does not fail! Anduril will keep on to the next component.
            e.printStackTrace();
            System.exit(ErrorCode.ERROR.getCode());
        }
/*      try {
            System.out.println("dest: "+dest.getAbsolutePath());
            CSVParser   tempCSV=new CSVParser(dest);
            for(String[]    line:tempCSV){
                for(String  entry:line)
                {
                    System.out.print(entry+"\t");
                }
                System.out.println();
            }
            tempCSV.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }
    
    String preprocessStr(String    str)
    {
        return str.replace("\"", "").replace(" ", "%20");
    }
    
    String postprocessStr(String   str)
    {
        return str.replace("%20", " ");
    }
    
    String[] preprocessStr(String[] strs)
    {
        String[] resStrs=new String[strs.length];
        for(int i=0;i<strs.length;i++)
        {
            resStrs[i]=preprocessStr(strs[i]);
        }
        return resStrs;
    }
    
    String[] postprocessStr(String[] strs)
    {
        String[] resStrs=new String[strs.length];
        for(int i=0;i<strs.length;i++)
        {
            resStrs[i]=postprocessStr(strs[i]);
        }
        return resStrs;
    }
    
}
