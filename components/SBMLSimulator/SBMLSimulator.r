library(componentSkeleton)

run.simulator <- function(cf) {
    ode.exec <- get.parameter(cf, 'odeSolverExecutable')
    start.time <- get.parameter(cf, 'startTime', 'float')
    end.time <- get.parameter(cf, 'endTime', 'float')
    report.step <- get.parameter(cf, 'reportStep', 'int')
    
    if (start.time > end.time) {
        write.error(cf, 'Start time is greater than end time')
        return(NULL)
    }
    
    sbml <- get.input(cf, 'model')
    
    ode.cmd <- sprintf('%s -n -z --time %s --printstep %d %s', ode.exec, end.time, report.step, sbml)
    ode.cmd <- gsub('\\', '/', ode.cmd, fixed=TRUE)
    handle <- pipe(ode.cmd)
    content <- readLines(handle)
    close(handle)
    content <- content[nchar(content) > 0]
    if (length(content) == 0) {
        write.error(cf, 'SBML ODE Solver returned no results')
        return(NULL)
    }
    
    if (substr(content[1], 1, 1) == '#') {
        content[1] <- substr(content[1], 2, nchar(content[1]))
    }
    ids <- split.trim(content[1], ' ')
    
    first.index <- match('##CONCENTRATIONS', content) + 1
    last.index <- length(content) - match('##CONCENTRATIONS', rev(content))
    content <- content[first.index:last.index]

    conn <- textConnection(paste(content, sep='\n'))
    sim.table <- as.matrix(read.table(conn))
    close(conn)
    
    t.col <- sim.table[,1]
    sim.table <- sim.table[t.col >= start.time,]
    
    colnames(sim.table) <- ids
    rownames(sim.table) <- sprintf('R%d', 1:nrow(sim.table))
    
    return(sim.table)
}

execute <- function(cf) {
    sim.table <- run.simulator(cf)
    if (is.null(sim.table)) return(1)

    tex <- sprintf('\\%s{Simulation results}', get.parameter(cf, 'sectionType'))
    
    if (ncol(sim.table) > 1) {
        include.species <- split.trim(get.parameter(cf, 'includeSpecies'), ',')
        if (identical(include.species, '*')) {
            include.species <- colnames(sim.table)[2:ncol(sim.table)]
        }
        if (!(colnames(sim.table)[1] %in% include.species)) {
            include.species <- c(colnames(sim.table)[1], include.species)
        }
        sim.table <- sim.table[,include.species,drop=FALSE]
        
        n.species <- ncol(sim.table)-1
        line.colors <- c("#000000", "#E41A1C", "#377EB8", "#4DAF4A", "#984EA3", "#FF7F00", "#A65628", "#F781BF", "#999999")
        line.types <- c(rep(1, length(line.colors)), rep(2, length(line.colors)), rep(4, length(line.colors)))
        line.colors <- rep(line.colors, max(1, ceiling(n.species/length(line.colors))))
        line.types <- rep(line.types, max(1, ceiling(n.species/length(line.types))))

        out.dir <- get.output(cf, 'plot')
        if (!file.exists(out.dir)) dir.create(out.dir)
        plot.file <- sprintf('%s-plot.pdf', get.metadata(cf, 'instanceName'))
        pdf(file.path(out.dir, plot.file))
        
        min.y <- min(sim.table[,2:ncol(sim.table)], na.rm=TRUE)
        max.y <- max(sim.table[,2:ncol(sim.table)], na.rm=TRUE)        
        x <- sim.table[,1]
        first <- TRUE
        
        for (i in 2:ncol(sim.table)) {
            y <- sim.table[,i]
            par(col=line.colors[i-1], lty=line.types[i-1])
            if (first) {
                y.log <- get.parameter(cf, 'yLog', 'boolean')
                if (y.log) log.param <- 'y'
                else log.param <- ''
                plot(x, y, type='l',
                    ylim=c(min.y, max.y), xlab='Time', ylab='',
                    log=log.param, main=get.parameter(cf, 'plotTitle'))
                first <- FALSE
            } else {
                lines(x, y)
            }
        }

        leg.pos <- get.parameter(cf, 'legendPosition')
        leg.ncol <- get.parameter(cf, 'legendColumns', 'int')
        legend(x=leg.pos, legend=colnames(sim.table)[2:ncol(sim.table)],
            col=line.colors, lty=line.types, box.lty=1, lwd=1, text.col='black',
            ncol=leg.ncol)

        dev.off()
        tex <- c(tex,
            latex.figure(plot.file, caption='Molecular species as a function of time.'))
    }
    
    CSV.write(get.output(cf, 'timepoints'), sim.table)    
    latex.write.main(cf, 'plot', tex)
    return(0)
}

main(execute)
