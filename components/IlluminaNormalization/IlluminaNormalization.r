library(componentSkeleton)

execute <- function(cf) {
  library(lumi)

  # Read input parameters
  data.raw	<- get.input(cf, 'exprRaw')
  norm.method	<- get.parameter(cf, 'method')

  # Read data in and set column "RowName" as row names 
  data		<- read.delim(data.raw, row.names="RowName", as.is=FALSE) 

  # Do normalization
  data.norm	<- lumiN(as.matrix(data), method=norm.method)

  CSV.write(get.output(cf, 'exprNorm'), data.norm)

  return(0)
}

main(execute)


  
