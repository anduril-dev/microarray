library(componentSkeleton)

execute <- function(cf) {
    logratio <- LogMatrix.read(get.input(cf, 'logratio'))
    
    threshold <- get.parameter(cf, 'threshold', 'float')
    if (threshold < 1) {
        write.error(cf, sprintf('Threshold must be >= 1 (is %s)', threshold))
        return(1)
    }
    threshold <- log2(threshold)

    stable.threshold <- get.parameter(cf, 'stableThreshold', 'float')
    output.stable <- FALSE    
    if(stable.threshold != 0) {    
        output.stable <- TRUE    
        if (stable.threshold < 1) {
            write.error(cf, sprintf('stableThreshold must be >= 1 (is %s)', stable.threshold))
            return(1)
        }
    }
    stable.threshold <- log2(stable.threshold)
    
    name.over <- get.parameter(cf, 'nameOver')
    name.under <- get.parameter(cf, 'nameUnder')
    name.deg <- get.parameter(cf, 'nameDeg')
    name.stable <- get.parameter(cf, 'nameStable')

    good  <- !is.na(logratio)
    over  <- good & (logratio >=  threshold)
    under <- good & (logratio <= -threshold)
    stable <- good & (logratio < stable.threshold & logratio > -stable.threshold)

    idlist <- data.frame(ID=character(0), Members=character(0),
        SampleGroup=character(0), Type=character(0),
        stringsAsFactors=FALSE)

    genes <- rownames(logratio)
    for (i in 1:ncol(logratio)) {
        group.id   <- colnames(logratio)[i]
        this.over  <- genes[over[,i,drop=FALSE]]
        this.under <- genes[under[,i,drop=FALSE]]
        this.deg   <- sort(union(this.over, this.under))
        
        idlist[nrow(idlist)+1,] <- c(sprintf(name.over, group.id),
            paste(this.over, collapse=','), group.id, 'fcOver')

        idlist[nrow(idlist)+1,] <- c(sprintf(name.under, group.id),
            paste(this.under, collapse=','), group.id, 'fcUnder')

        idlist[nrow(idlist)+1,] <- c(sprintf(name.deg, group.id),
            paste(this.deg, collapse=','), group.id, 'fcDeg')
        
        if(output.stable) {
            this.stable <- genes[stable[,i,drop=FALSE]]
            idlist[nrow(idlist)+1,] <- c(sprintf(name.stable, group.id),
                paste(this.stable, collapse=','), group.id, 'fcStable')
        }
    }

    CSV.write(get.output(cf, 'deg'), idlist)

    return(0)
}

main(execute)
