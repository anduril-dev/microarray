<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>MeapQuantification</name>
    <version>2.0</version>
    <doc>Quantify expression data on exon, splicing variant or gene level.
         Normaliziation has to be done beforehand by 'MeapNormalization'.
         Processing is run in parallel using the R Message Passing Interface
         (Rmpi) library to enable faster execution. 

         It supports Affymetrix GeneChip Exon 1.0 ST and Gene 1.0 ST arrays.
         This version is build on latest meap library (2.0.0) and Ensembl v70.
    </doc>
    <author email="ping.chen@helsinki.fi">Ping Chen</author>
    <category>Meap</category>
    <launcher type="R">
        <argument name="file" value="MeapQuantification.r" />
    </launcher>
    <requires>R</requires>
    <requires type="R-package">meap</requires>
    <inputs>
        <input name="normalized" type="CSV">
            <doc> A CSV file contains a list of paths of all normalized data.
            </doc>
        </input>
	<input name="dbConnect" type="CSV" optional="true">
            <doc>Database configuration file stores database connect details.
		 This file is required when generating expression data on 'SpliceVariant' or 'Gene' level.
	    </doc>
        </input>
    </inputs>
    <outputs>
        <output name="ExonExpr" type="LogMatrix">
            <doc> Exon expression matrix. </doc>
        </output>
         <output name="SpliceVariantExpr" type="LogMatrix">
            <doc> Expression matrix for splicing variants. </doc>
        </output>
        <output name="GeneExpr" type="LogMatrix">
            <doc> Gene expresssion matrix. </doc>
        </output>
    </outputs>
    <parameters>
       <parameter name = "annotation" type = "string" default = "/opt/sandbox/pchen/MEAPAnnotation_Human_v62">
            <doc> The path of exon array annotation file. </doc>
       </parameter>
       <parameter name = "exprType" type = "string" default = "Exon">
            <doc> A comma-separated string indicating the types of expression matrix to be generated. 
                  It can be 'Exon', 'SpliceVariant' or 'Gene'. 'Exon' and 'SpliceVariant' level expression 
                  are only for Exon 1.0 ST array. 'Gene' level expression supports both Exon 1.0 ST and Gene 1.0 ST arrays.</doc>
       </parameter>
       <parameter name = "arrayType" type = "string" default = "exon">
            <doc>The type of GeneChip array ("exon": GeneChip Exon ST 1.0
                 array, "gene": GeneChip Gene ST 1.0 array). </doc>
       </parameter>
       <parameter name = "method" type = "string" default = "medianpolish">
            <doc>The summarization method for exon and gene level expression. 
                 The default is 'medianpolish'. It can also be 'mean'. </doc>
       </parameter>
       <parameter name = "metaData" type = "string">
                <doc> The directory for storing meta data. The meta data will be removed after the execution. It is highly recommend to set this path by user.
                      If empty string is given, the data will be stored in the temporary folder in the execution folder, as given by Anduril.</doc>
       </parameter>
       <parameter name = "NProcess" type = "int" default = "5">
                <doc> The number of processes are invoked in parallel execution.</doc>
       </parameter>
       <parameter name = "filter" type = "boolean" default = "true">
                <doc> Filter transcripts in Ensembl database that do not pass the quality control. The default is 'TRUE'. 
		      This parameter is used in generating expression data on 'SpliceVariant' or 'Gene' level.
		</doc>
       </parameter>
       <parameter name = "cutoff" type = "float" default = "0.5">
                <doc> The percentage of transcripts with probes for gene level summarization.
                      If a probe does not map to the number of transcripts defined, the probe 
                      will be removed from the summarization. The cutoff value should be 0-1.
                </doc>
       </parameter>
       <parameter name = "remove" type = "boolean" default = "false">
                <doc>A boolean for removing all meta data. If normalized data are under different folder from the 'metaData', they will be also removed.</doc>
       </parameter>
       <parameter name = "version" type = "int" default = "70">
                <doc>Ensembl version number.</doc>
       </parameter>
    </parameters>
</component>

