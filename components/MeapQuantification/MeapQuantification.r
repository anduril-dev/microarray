library(componentSkeleton)
library(meap)

execute <- function(cf){

	norm.obj <- as.vector(CSV.read(get.input(cf,'normalized'))[,1])
	if(input.defined(cf, 'dbConnect')){
                db <- CSV.read(get.input(cf, 'dbConnect'))
                host <- db$host
                database <- db$database
                user <- db$user
                passwd <- db$password
                if(is.null(passwd) || is.na(passwd)){
                        passwd <- ""
                }
                port <- db$port
                if(is.null(port) || is.na(port)){
                        port <- NULL
                }
        }
	annotation <- get.parameter(cf,'annotation')
        exprType <- split.trim(get.parameter(cf,'exprType'),",")
        arrayType <- get.parameter(cf,'arrayType')
        metaData <- get.parameter(cf,'metaData')
        if (identical(metaData,'')) {
            metaData <- get.temp.dir(cf)
        }
        nprocess <- get.parameter(cf,'NProcess',type="int")
        filter <- get.parameter(cf,'filter',type="boolean")
        cutoff <- get.parameter(cf,'cutoff',type="float")
	remove <- get.parameter(cf,'remove',type="boolean")
        version <- get.parameter(cf,'version',type="int")
	summary.method <- get.parameter(cf,'method',type="string")

        exon.expr.file <- get.output(cf,'ExonExpr')
        splice.expr.file <- get.output(cf,'SpliceVariantExpr')
        gene.expr.file <- get.output(cf,'GeneExpr')

	# Check empty files
        if(length(norm.obj)==0){
                write.table("ExonID",exon.expr.file,sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
                write.table("TranscriptID",splice.expr.file,sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
                write.table("GeneID",gene.expr.file,sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
                return(0)
        }

	# Change execution directory
        cur.dir <- tempdir()
        setwd(cur.dir)
        write.log(cf,paste("Current working directory is set to ",cur.dir,sep=""))

	# Start summarization
	if("Exon" %in% exprType){
		write.log(cf,"Start to run exon level data summerization ...")
		exon.expr <- meap.exon.summarize(norm.obj, annotation, eps=0.01, maxiter=10, nprocess, remove.metadata=FALSE,
					         summary.method=summary.method)
		write.table(cbind(ExonID=rownames(exon.expr),exon.expr),exon.expr.file,sep="\t",row.names=FALSE,quote=FALSE)
	}else{
		 write.table("# 'Exon' expression type was not selected",exon.expr.file,sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
	}

	if("SpliceVariant" %in% exprType){
		write.log(cf,"Start to run transcript level summerization ...")
		if(!("Exon" %in% exprType)){
			exon.expr <- meap.exon.summarize(norm.obj, annotation, eps=0.01, maxiter=10, nprocess, remove.metadata=FALSE,
							 summary.method=summary.method)
		}

		trans.expr <- meap.expr.exonToTranscript(exon.expr=exon.expr,exon.expr.file=NULL,ex.host=host,ex.dbname=database,ex.usrname=user,
							 ex.passwd=passwd,ex.port=port,metadata.dir=metaData,nprocess=nprocess,transcript.filter=filter,
							 version=version)
		trans.expr <- cbind(TranscriptID=rownames(trans.expr),trans.expr)
                write.table(trans.expr,splice.expr.file,sep="\t",row.names=FALSE,quote=FALSE)
	}else{
		write.table("# 'SpliceVariant' expression type was not selected",splice.expr.file,sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
	}

	if("Gene" %in% exprType){
		write.log(cf,"Start to run gene level summerization ...")  
		gene.expr <- meap.gene.summarize(norm.obj,annotation,host,database,user,passwd,port,metaData,cutoff,
                                                 nprocess,remove.metadata=FALSE,transcript.filter=filter,array.type=arrayType,
						 summary.method=summary.method,version=version)
            
                gene.expr <- cbind(GeneID=rownames(gene.expr),gene.expr)
                write.table(gene.expr,gene.expr.file,sep="\t",row.names=FALSE,quote=FALSE)
        }else{
                write.table("# 'Gene' expression type was not selected",gene.expr.file,sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
	}

	if(remove){
		unlink(norm.obj)
		unlink(paste(metaData,"/*",sep=""))
	}
}

main(execute)
