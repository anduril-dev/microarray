import java.util.*;
import java.io.*;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

/* 
 * TumorscapeReader connects to zip-file representation of the
 * Tumorscape database via a ZipStreamReader. 
 * 
 * @author Riku Louhimo
 */
public class TumorscapeReader extends SkeletonComponent {
    
    File queryGeneFile;
    File database;
    String databaseURL;
    boolean readEntire = false;
    ArrayList<ArrayList<String>> results;
    FTPClient ftp;

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        CSVParser queryGenes = null;
        ftp = new FTPClient();

        if (cf.inputDefined("database")){
            databaseURL = "file://"+cf.getInput("database").getAbsolutePath();
        } else {
            databaseURL   = cf.getParameter("databaseLocation");
        }

        cf.writeLog("- connecting to "+databaseURL);
        try {
            if (cf.inputDefined("queryGenes")){
                queryGeneFile = cf.getInput("queryGenes");
                queryGenes = new CSVParser(queryGeneFile);
                results = executeQuery(databaseURL, queryGenes);
            } else {
                readEntire = true;
                results = executeQuery(databaseURL, readEntire);
            }
        } catch (MalformedURLException me){
            cf.writeError(me+" Database URL is invalid. Perhaps you need to start with file:/// ?");
            return ErrorCode.INVALID_INPUT;
        } catch (IOException ie){
            cf.writeError(ie+" Database could not be accessed. Location has changed or resource is unavailable. ");
            return ErrorCode.ERROR;
        } finally {
            if (ftp.isConnected()){
                ftp.logout();
                ftp.disconnect();
            }
        }
        
        cf.writeLog("Done querying...writing results. ");
        CSVWriter writer = null;
        try {
            String idColumnHeader = "ID";
            if (queryGenes != null) idColumnHeader = queryGenes.getColumnNames()[0];
            String[] columnHeaders = { idColumnHeader, "AberrationType", "CancerType", "PeakFrequency", "q-value", "GenesInPeak" };
            writer = new CSVWriter(columnHeaders, cf.getOutput("features") , false);
            for (int i = 0; i < results.get(0).size(); i++) {
                writer.write(results.get(0).get(i));
                writer.write(results.get(1).get(i));
                writer.write(results.get(2).get(i));
                writer.write(results.get(3).get(i), false);
                writer.write(results.get(4).get(i), false);
                writer.write(results.get(5).get(i), false);
            }
        } finally {
            if (writer != null) writer.close();
        }
        
        return ErrorCode.OK;
    }

    /**
     * This method handles the parsing of text files within the database
     * zip file, and collects the annotation for the queryGenes.
     * 
     * @param databaseURL Location of the database
     * @param queryGenes List of genes of interest
     * @return Annotated list of genes
     * 
     * @throws MalformedURLException
     * @throws IOException
     */
    private ArrayList<ArrayList<String>> executeQuery(String databaseURL, CSVParser queryGenes) 
                                            throws MalformedURLException, IOException{
        ArrayList<ArrayList<String>> results = new ArrayList<ArrayList<String>>();
        ArrayList<String> resGenes        = new ArrayList<String>(1000);
        ArrayList<String> ampSubset       = new ArrayList<String>(1000);
        ArrayList<String> ampSubsetFreq   = new ArrayList<String>(1000);
        ArrayList<String> cnaType         = new ArrayList<String>(1000);
        ArrayList<String> qValue          = new ArrayList<String>(1000);
        ArrayList<String> nroGenes        = new ArrayList<String>(1000);

        results.add(resGenes);
        results.add(cnaType);
        results.add(ampSubset);
        results.add(ampSubsetFreq);
        results.add(qValue);
        results.add(nroGenes);

        String dbSubstr = new URL(databaseURL).getHost();
        String locSubstr = new URL(databaseURL).getPath();
        ZipInputStream zin;
        ByteArrayOutputStream bos = new ByteArrayOutputStream(30000000);
        byte[] localZip = new byte[30000000];

        if (databaseURL.startsWith("ftp")){
            ftp.connect(dbSubstr);
            int reply = ftp.getReplyCode();
            if(!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                throw(new IOException("FTP server refused connection"));
            }
            ftp.login("anonymous", "anonymous");
            System.out.println(ftp.getReplyString());
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            ftp.enterLocalPassiveMode();
            ftp.retrieveFile(locSubstr, bos);
            System.out.println(ftp.getReplyString());
            localZip = bos.toByteArray();
            System.out.println("localZip");
            ftp.completePendingCommand();
            System.out.println("localZip");
            bos.close();
            ftp.logout();
            ftp.disconnect();
        }
 
        while (queryGenes.hasNext()){
            String gene = queryGenes.next()[0];
            if (databaseURL.startsWith("ftp")){
                zin = new ZipInputStream(new ByteArrayInputStream(localZip));
            } else {
                zin = new ZipInputStream(new URL(databaseURL).openConnection().getInputStream());
            }
            ZipEntry ze = zin.getNextEntry();
            while (ze.isDirectory()) ze = zin.getNextEntry(); //Jump over directory listing
            
            search: while (zin.available() != 0){
                String[] databaseEntry = ze.getName().split("/");
                databaseEntry = databaseEntry[1].split("\\.");
                String databaseGene = databaseEntry[0];
                if (databaseGene.equals(gene)){
                    //resGenes.add(gene);
                    BufferedReader br = new BufferedReader(new InputStreamReader(zin));
                    String line;
                    while ((line = br.readLine()) != null){
                         if (line.contains("Frequency Amplified")) {
                            while(line.length() > 1){
                                line = br.readLine();
                                String[] splitCancerLine = line.split("\t");
                                if (line.contains("Yes")) {
                                    resGenes.add(gene);
                                    ampSubset.add(splitCancerLine[1]);
                                    nroGenes.add(splitCancerLine[5].trim());
                                    qValue.add(splitCancerLine[6]);
                                    ampSubsetFreq.add(splitCancerLine[7]);
                                    cnaType.add("amplification");
                                }
                            }
                         }
                         if (line.contains("Frequency Deleted")) {
                            parse: while(line.length() > 1){
                                line = br.readLine();
                                if (line == null) break parse;
                                String[] splitCancerLine = line.split("\t");
                                if (line.contains("Yes")) {
                                    resGenes.add(gene);
                                    ampSubset.add(splitCancerLine[1]);
                                    nroGenes.add(splitCancerLine[5]);
                                    qValue.add(splitCancerLine[6]);
                                    ampSubsetFreq.add(splitCancerLine[7]);
                                    cnaType.add("deletion");
                                }
                             }
                         }
                    }
                    break search;
                }
                ze  = zin.getNextEntry();
            }
            zin.close();
        }
        return results;
    }
    
    
    /**
     * This method handles the parsing of text files within the database
     * zip file, and collects the annotation for the queryGenes.
     * 
     * @param databaseURL Location of the database
     * @param queryGenes List of genes of interest
     * @return Annotated list of genes
     * 
     * @throws MalformedURLException
     * @throws IOException
     */
    private ArrayList<ArrayList<String>> executeQuery(String databaseURL, boolean readEntire) 
                                            throws MalformedURLException, IOException{
        ArrayList<ArrayList<String>> results = new ArrayList<ArrayList<String>>(1000);
        ArrayList<String> resGenes        = new ArrayList<String>();
        ArrayList<String> ampSubset       = new ArrayList<String>();
        ArrayList<String> ampSubsetFreq   = new ArrayList<String>();
        ArrayList<String> cnaType         = new ArrayList<String>();
        ArrayList<String> qValue          = new ArrayList<String>();
        ArrayList<String> nroGenes        = new ArrayList<String>();
    
        results.add(resGenes);
        results.add(cnaType);
        results.add(ampSubset);
        results.add(ampSubsetFreq);
        results.add(qValue);
        results.add(nroGenes);
        
        String dbSubstr = new URL(databaseURL).getHost();
        String locSubstr = new URL(databaseURL).getPath();
        ZipInputStream zin;
        
        if (databaseURL.startsWith("ftp")){
            ftp.connect(dbSubstr);
            int reply = ftp.getReplyCode();
            if(!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                throw(new IOException("FTP server refused connection"));
            }
            ftp.login("anonymous", "anonymous");
            System.out.println(ftp.getReplyString());
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            ftp.enterLocalPassiveMode();
            zin = new ZipInputStream(ftp.retrieveFileStream(locSubstr));
        } else {    
            zin = new ZipInputStream(new URL(databaseURL).openConnection().getInputStream());
        }
    
        ZipEntry ze = zin.getNextEntry();
        while (ze.isDirectory()) ze = zin.getNextEntry(); //Jump over directory listing
            while (zin.available() != 0){
                String[] databaseEntry = ze.getName().split("/");
                databaseEntry = databaseEntry[1].split("\\.");
                String databaseGene = databaseEntry[0];
                
                BufferedReader br = new BufferedReader(new InputStreamReader(zin));
                String line;
                while ((line = br.readLine()) != null){
                     if (line.contains("Frequency Amplified")) {
                        while(line.length() > 1){
                            line = br.readLine();
                            String[] splitCancerLine = line.split("\t");
                            if (line.contains("Yes")) {
                                 resGenes.add(databaseGene);
                                 ampSubset.add(splitCancerLine[1]);
                                 nroGenes.add(splitCancerLine[5].trim());
                                 qValue.add(splitCancerLine[6]);
                                 ampSubsetFreq.add(splitCancerLine[7]);
                                 cnaType.add("amplification");
                            }
                         }
                     }
                     if (line.contains("Frequency Deleted")) {
                        parse: while(line.length() > 1){
                            line = br.readLine();
                            if (line == null) break parse;
                            String[] splitCancerLine = line.split("\t");
                            if (line.contains("Yes")) {
                                resGenes.add(databaseGene);
                                ampSubset.add(splitCancerLine[1]);
                                nroGenes.add(splitCancerLine[5]);
                                qValue.add(splitCancerLine[6]);
                                ampSubsetFreq.add(splitCancerLine[7]);
                                cnaType.add("deletion");
                            }
                        }
                     }
                }
                ze  = zin.getNextEntry();
            }
        zin.close();
        if (ftp.isConnected()) {
            ftp.completePendingCommand();
        }
        return results;
    }

    public static void main(String[] args) {
        new TumorscapeReader().run(args);
    }
}
