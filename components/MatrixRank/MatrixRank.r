library(componentSkeleton)

execute <- function(cf) {
    matr <- Matrix.read(get.input(cf, 'matrix'))
    
    columns <- get.parameter(cf, 'columns')
    if (columns == '*') columns <- colnames(matr)
    else columns <- split.trim(columns, ',')
    matches <- match(columns, colnames(matr))
    if (any(is.na(matches))) {
        bad <- columns[is.na(matches)]
        stop(paste('Column(s) not found in matrix:', paste(bad, collapse=', ')))
    }
    
    rank.matrix <- matrix(NA, nrow(matr), length(columns))
    colnames(rank.matrix) <- columns
    rownames(rank.matrix) <- rownames(matr)
    
    for (group.id in columns) {
        matr.column <- matr[,group.id]
        names(matr.column) <- rownames(matr)
        matr.column <- matr.column[!is.na(matr.column)]
        matr.column <- sort(matr.column)
        n <- length(matr.column)
        if (n > 1) {
            ranks <- (0:(n-1)) / (n-1)
            rank.matrix[names(matr.column), group.id] <- ranks
        }
    }
    
    CSV.write(get.output(cf, 'ranks'), rank.matrix, first.cell='Rank')
    return(0)
}

main(execute)
