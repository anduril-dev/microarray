# set -x  # Used for debugging, prints each line before executing it
set -e  # Script exits at first error, otherwise the next line is 
        # executed even after a command fails 

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"

# use these functions to read command file
infile=$( getinput in ) 
outfile=$( getoutput out )
inencodin=$( getparameter inEncoding )
outencod=$( getparameter outEncoding )

ICONV=$( which iconv )
if ( $ICONV --version | grep -i iconv )
    then echo "iconv program found."
else
    msg="iconv not found. ( $ICONV )"
    writeerror "$msg"
    exit 1
fi

FILE=$( which file )
if ( $FILE --version | grep -i file )
    then echo "file program found."
else
    msg="file program not found. ( $FILE )"
    writeerror "$msg"
    exit 1
fi

if [ $inencodin == ""]; then
  fileEnAll=$( file -bi "$infile" )
  fileEn=(${fileEnAll//=/ })
  inencod=${fileEn[2]}
  writelog "Found encoding $inencod"
else
  inencod=$inencodin
fi

iconv -f "$inencod" -t "$outencod" "$infile" > "$outfile"

writelog "Converted character encoding from $inencod to $outencod."

exit 0
