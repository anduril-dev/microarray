library(componentSkeleton)

execute <- function(cf) {
  data          <- CSV.read(get.input(cf, 'data'))
  width.inches  <- get.parameter(cf,     'width', 'float') / 2.54
  height.inches <- get.parameter(cf,    'height', 'float') / 2.54
  xlab          <- get.parameter(cf,    'xlabel', 'string')
  ylab          <- get.parameter(cf,    'ylabel', 'string')
  titleT        <- get.parameter(cf,     'title', 'string')
  caption       <- get.parameter(cf,   'caption', 'string')
  nameCol       <- get.parameter(cf,     'names', 'string')
  pageBreak     <- get.parameter(cf, 'pageBreak', 'boolean')

  out.dir <- get.output(cf, 'report')
  fname   <- sprintf('%s-RR.pdf', get.metadata(cf, 'instanceName'))
  outname <- file.path(out.dir, fname)
  dir.create(out.dir, recursive=TRUE)

  if (nchar(titleT) > 0) {
     tex <- paste('\\subsection{',titleT,'}',sep="")
  } else {
     tex <- ""
  }

  if (nrow(data) < 1) {
     tex <- c(tex, '\\textit{There are no risk values to be shown!}\n\n')
     if (pageBreak) { tex <- c(tex, '\\clearpage') }
     latex.write.main(cf, 'report', tex)
     return(0)
  }

  tex <- c(tex, latex.figure(fname,
                             caption    = caption,
                             quote.capt = FALSE,
                             fig.label  = sprintf('fig:%s',get.metadata(cf, 'instanceName'))))

  lowL  <- data[,get.parameter(cf, 'lCI')]
  highL <- data[,get.parameter(cf, 'uCI')]
  mids  <- data[,get.parameter(cf, 'RR')]
  N     <- length(mids)
  nnaL  <- !(is.na(lowL) | is.na(highL))

  if (nchar(nameCol) > 0) {
     nameLst <- data[, nameCol]
     xlab    <- ""
  } else {
     nameLst <- NULL
  }

  pdf(outname, paper='special', width=width.inches, height=height.inches)
  par(pch=20, cex=0.7, las=2)
  plot(numeric(), numeric(),
       xlab=xlab, ylab=ylab, main=titleT,
       xlim=c(0.5,N+0.5), ylim=c(min(lowL[nnaL]),max(highL[nnaL])),
       log="y", axes=is.null(nameLst), frame.plot=TRUE)
  if (!is.null(nameLst)) {
     axis(side=1, at=1:N, labels=nameLst, crt=2, cex.axis=0.7)
     axis(side=2)
  }
  abline(h=1, col="green")
  for (i in 1:N) {
      if (!nnaL[i]) {
         lines(c(i-0.5,i+0.5),c(1.05,0.95), col='gray')
         lines(c(i-0.5,i+0.5),c(0.95,1.05), col='gray')
         next
      }
      if (lowL[i] > 1) {
         col <- "red"
      } else
      if (highL[i] < 1) {
         col <- "blue"
      } else {
         col <- "black"
      }
      lines(c(i,i),        c(lowL[i],highL[i]), col=col)
      lines(c(i-0.5,i+0.5),c(mids[i],mids[i]),  col=col)
  }
  invisible(dev.off())

  if (pageBreak) { tex <- c(tex, '\\clearpage') }
  latex.write.main(cf, 'report', tex)

  return(0)
}

main(execute)
