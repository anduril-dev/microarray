library(componentSkeleton)
library(meap)

execute <- function(cf){

		cur.dir <- tempdir()
		setwd(cur.dir)
		write.log(cf,paste("Current working directory is set to ",cur.dir,sep=""))
	
        expr <- get.input(cf,'expr')
        glist <- CSV.read(get.input(cf,'geneList'))[,1]

        db <- CSV.read(get.input(cf, 'dbConnect'))
        host <- db$host
        database <- db$database
        user <- db$user
        passwd <- db$password
        if(is.na(passwd)){
                passwd <- ""
        }
        port <- db$port
        if(is.na(port)){
                port <- NULL
        }

        sample <- SampleGroupTable.read(get.input(cf,'sample'))
        sample.group1 <- SampleGroupTable.get.source.groups(sample, "Group1")
        sample.group2 <- SampleGroupTable.get.source.groups(sample, "Group2")
        sample.group <- list(g1=sample.group1,g2=sample.group2)

        outFolder <- get.output(cf,'report')

        geneColumn <- get.parameter(cf,'geneColumn',type="int")
        metaData <- get.parameter(cf,'metaData',type="string")
        sig.type <- get.parameter(cf,'SigType',type="string")
        paired <- get.parameter(cf,'paired',type="boolean")
        alternative <- get.parameter(cf,'alternative',type="string")
        upCol <- get.parameter(cf,'upCol',type="string")
        downCol <- get.parameter(cf,'downCol',type="string")
        HeatColNum <- get.parameter(cf,'HeatColNum',type="int")
        species <- get.parameter(cf,'species',type="string")
        geneFilter <- get.parameter(cf,'geneFilter',type="boolean")
        transcriptFilter <- get.parameter(cf,'transcriptFilter',type="boolean")
        nprocess <- get.parameter(cf,'NProcess',type="int")
        combine.type <- SampleGroupTable.get.type(sample, "Group1")
        
        
        # Visualization
        meap.splice.visual(exon.expr=NULL, exon.expr.file=expr, glist,
                          metaData, sample.group=sample.group,
                          sig.type=sig.type,
                          color.up.regulate=upCol,
                          color.down.regulate=downCol,
                          heat.color.numbers=HeatColNum,
                          paired=paired,
                          alternative=alternative,
                          combine.type=combine.type,
                          ex.host=host,
                          ex.database=database,
                          ex.username=user,
                          ex.passwd=passwd,
                          ex.port = port,
                          ex.driver="MySQL",
                          nprocess=nprocess,
                          species=species,
                          output.dir=outFolder,
                          gene.filter=geneFilter,
                          transcript.filter=transcriptFilter)

}

main(execute)
