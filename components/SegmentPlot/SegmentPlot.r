library(componentSkeleton)

execute <- function(cf) {

  InstanceName  <- get.metadata(cf, 'instanceName')
  chrSeg        <- CSV.read(get.input(cf, 'chrSegments'))
  excludeChr    <- get.parameter(cf, 'excludeChr')
  lwidth        <- get.parameter(cf, 'lineWidth')
  signiThold    <- get.parameter(cf, 'sThold', type='float')
  drawSampleIds <- get.parameter(cf, 'drawSampleIDs', type='boolean')
  out.dir       <- get.output(cf, 'plot')
  docName       <- file.path(out.dir, LATEX.DOCUMENT.FILE)
  dir.create(out.dir, recursive=TRUE)

  sampleCol     <- get.parameter(cf, 'sampleColumn')
  chrSeg$Sample <- chrSeg[,which(colnames(chrSeg) == sampleCol)]
  if (any(colnames(chrSeg) == sampleCol) == FALSE) {
      write.error(cf, 'sampleColumn not found in chrSegments')
      return(INVALID_INPUT)
  }
  bpStartColumn <- get.parameter(cf, 'bpStartColumn')
  chrSeg$start  <- chrSeg[,which(colnames(chrSeg) == bpStartColumn)]
  if (any(colnames(chrSeg) == bpStartColumn) == FALSE) {
      write.error(cf, 'bpStartColumn not found in chrSegments')
      return(INVALID_INPUT)
  }
  bpEndColumn   <- get.parameter(cf, 'bpEndColumn')
  chrSeg$end    <- chrSeg[,which(colnames(chrSeg) == bpEndColumn)]
  if (any(colnames(chrSeg) == bpEndColumn) == FALSE) {
      write.error(cf, 'bpEndColumn not found in chrSegments')
      return(INVALID_INPUT)
  }
  chrColumn     <- get.parameter(cf, 'chrColumn')
  chrSeg$chr    <- chrSeg[,which(colnames(chrSeg) == chrColumn)]
  if (any(colnames(chrSeg) == chrColumn) == FALSE) {
      write.error(cf, 'chrColumn not found in chrSegments')
      return(INVALID_INPUT)
  }
  scoreColumn   <- get.parameter(cf, 'scoreColumn')
  callColumn    <- get.parameter(cf, 'callColumn')
  if (callColumn == "") {
     chrSeg$call <- rep(1, nrow(chrSeg))
  } else {
      if (any(colnames(chrSeg) == callColumn) == FALSE) {
          write.error(cf, 'callColumn not found in chrSegments')
          return(INVALID_INPUT)
      }
     chrSeg$call <- chrSeg[,which(colnames(chrSeg) == callColumn)]
  }
  if (scoreColumn == "") {
     scoreFunc    <- NULL
     chrSeg       <- chrSeg[,c("Sample","chr","start","end","call")]
  } else {
     scoreFunc    <- eval(parse(text=get.parameter(cf, 'scoreMerge')))
     chrSeg$score <- chrSeg[,which(colnames(chrSeg) == scoreColumn)]
     chrSeg       <- chrSeg[,c("Sample","chr","start","end","call","score")]
  }

  chrSet <- unique(chrSeg$chr)
  chrSet <- setdiff(as.character(chrSet), unlist(strsplit(excludeChr, split=",")))
  chrSet <- chrSet[order(as.numeric(chrSet))]

  nchr <- length(chrSet)
  if (nchr < 10) {
    dimens <- c(3,3)
  } else if (nchr < 13){
    dimens <- c(6,2)
  } else if (nchr < 17){
    dimens <- c(4,4)
  } else if (nchr < 21){
    dimens <- c(5,4)
  } else if (nchr < 25){
    dimens <- c(6,4)
  } else {
    dimens <- c(6,5)
  }

  fig.name <- sprintf('%s-segments.png', InstanceName)
  fig.path <- file.path(out.dir, fig.name)
  png(fig.path, width=2000, height=2900, res=300)
  if (drawSampleIds){
      par(mfrow=dimens, mar=c(0.1, 0.8, 1, 0.1), oma=c(0.1, 0.1, 0.1, 0.1))
  } else {
      par(mfrow=dimens, mar=c(0.1, 0.1, 1, 0.1), oma=c(0.1, 0.1, 0.1, 0.1))
  }
  found <- matrix(ncol=7) # Init output

  gainsOrLosses <- get.parameter(cf, 'gainsOrLosses', type="int")

  levs <- unique(chrSeg$Sample)[order(unique(chrSeg$Sample))]
  if (gainsOrLosses == 1) {
    gains <- which(chrSeg$call == 1)
    chrSeg <- chrSeg[gains,]
  } else if (gainsOrLosses == -1) {
    gains <- which(chrSeg$call == -1)
    chrSeg <- chrSeg[gains,]
  }

  levels(chrSeg$Sample) <- levs
  id <- 1

  for (j in 1:length(chrSeg$chr)){
    chr1 <- which(chrSeg$chr == chrSet[j])
    if (length(chr1) == 0) next
    min <- 0
    max <- max(chrSeg$end[chr1], na.rm=TRUE)+1000
    plot(0,
        xlim=c(min,max), 
        ylim=c(1,nlevels(chrSeg$Sample)), 
        main=paste("Chr",chrSet[j]), type="n",
        xlab="", ylab="", sub="", 
        axes=FALSE, frame=TRUE)
    if (drawSampleIds){
        if (j %in% c(1,5,9,13,17,21)) axis(2, at=1:nlevels(chrSeg$Sample), labels=levels(chrSeg$Sample), tick=F, cex.axis=0.5, line=-0.8, las=1)
    }
    for (i in 1:length(chr1)) {
      y0 <- which(levels(chrSeg$Sample) == chrSeg$Sample[chr1[i]])
      y1 <- y0
      if (chrSeg$call[chr1[i]] == 1) {
        col <- "red"
      } else {
        col <- "blue"
      }
      segments(y0=y0,y1=y1,x0=chrSeg$start[chr1[i]],x1=chrSeg$end[chr1[i]], lwd=lwidth, col=col)
    }

    found.reg <- find.regions(chrSeg[chr1,], thold=signiThold, nlevels(chrSeg$Sample), scoreFunc)
    if (!is.na(found.reg$start) && !is.na(found.reg$end)){
      for (f in 1:length(found.reg$start)) {
        # Define rectangle regions
        polygon(c(found.reg$start[f],found.reg$start[f],found.reg$end[f],found.reg$end[f]),
                c(0,nlevels(chrSeg$Sample),nlevels(chrSeg$Sample),0), 
                lwd=0.4, border = rgb(255,153,0,100,max=255),
                col=rgb(255,153,0,100,max=255))
        # Construct output matrix
        if (nrow(found) == 1 && is.na(found[1])) {
          found <- rbind(c(paste("reg", id, sep=""), 
                                 1, chrSet[j], 
                                 found.reg$start[f], 
                                 found.reg$end[f], 
                                 format(found.reg$freq[f], digits=5, scientific=FALSE),
                                 format(found.reg$score[f], digits=5, scientific=FALSE)))
        } else {
          found <- rbind(found, c(paste("reg", id, sep=""), 
                                  1, chrSet[j], 
                                  found.reg$start[f], 
                                  found.reg$end[f], 
                                  format(found.reg$freq[f], digits=5, scientific=FALSE),
                                  format(found.reg$score[f], digits=5, scientific=FALSE)))
        }
        id <- id+1
      }
    }
  }
  colnames(found) <- c('ID', 'strand', 'chromosome', 'start', 'end', 'freq', 'score')

  invisible(dev.off())
  # OUTPUT
  if (gainsOrLosses == 1) {
    fig.capt <- paste("Red lines represent CN gain segments, areas highlighted with yellow have gains in more than ",signiThold," of samples.", sep="")
  } else if (gainsOrLosses == -1) {
    fig.capt <- paste("Blue lines represent CN loss segments, areas highlighted with yellow have losses in more than ",signiThold," of samples.", sep="")
  } else {
    fig.capt <- paste("Red lines represent CN gain segments, blue lines represent CN loss segments, areas highlighted with yellow have gains and losses in more than ",signiThold," of samples.", sep="")
  }
  cat( latex.figure(fig.name, caption = fig.capt, quote.capt = FALSE, image.scale=1),
       "\\clearpage",
       sep="\n", file=docName, append=TRUE)

  CSV.write(get.output(cf, 'SignRegion'), as.data.frame(found))
}

#---------------------------------------
# Find regions of interest
# Based on CohortComparator by M. Laakso
#---------------------------------------
find.regions <- function(chrSeg, thold, nroSamples, scoreFunc) {
  useScore <- !is.null(scoreFunc)
  if (useScore) {
     scoreMethod <- match.fun(scoreFunc)
  }

  # Sort before processing
  chrSeg <- chrSeg[order(chrSeg$start,chrSeg$end),]

  # Output
  regions.start <- 0
  regions.end   <- 0
  regions.freq  <- 0
  regions.score <- 0
  # Counters
  start <- rep(0, length(chrSeg$start))
  end   <- rep(0, length(chrSeg$end))

  for (i in 1:length(chrSeg$start)) {
    for (j in 1:length(chrSeg$start)) {
      if (chrSeg$start[j] <= chrSeg$start[i] && chrSeg$end[j] >= chrSeg$start[i]) start[i] <- start[i] + 1
      if (chrSeg$start[j] <= chrSeg$end[i]   && chrSeg$end[j] >= chrSeg$end[i])   end[i]   <- end[i]   + 1      
    }
  }

  # Algorithm starts
  bool <- FALSE
  i <- 0
  while (i < length(start)){
    i <- i+1
    startRatio <- start[i]/nroSamples
    if (startRatio >= thold && !bool) {
      bool   <- TRUE
      minLoc <- i
    }
    if (startRatio < thold && !bool) next
    if (chrSeg$start[minLoc] < regions.end[length(regions.end)]) {
      bool <- FALSE
      next
    }
    if (startRatio < thold && bool) {
      border <- 1
      filter <- which(chrSeg$end >= chrSeg$start[minLoc])
      #putativeEnds <- which(end/nroSamples >= thold)
      #filter <- intersect(filter,putativeEnds)
      if (length(filter) == 0) next
      end2   <- chrSeg$end[filter][order(chrSeg$end[filter])]
      filter <- filter[order(chrSeg$end[filter])]
      for (j in minLoc:length(start)){
        if ((chrSeg$start[minLoc] <= chrSeg$start[filter[border]]) && (chrSeg$end[minLoc] >= chrSeg$end[filter[border]]) && filter[border]!=minLoc){
              if ((chrSeg$end[minLoc] > chrSeg$end[filter[border]]) && (end[minLoc]/nroSamples >= thold)) {
                if (border < length(filter)) border <- border+1
                next
              }
        }
        if (end2[border] < chrSeg$start[j]){
            if (eval.freq(filter[border], j, chrSeg, thold, nroSamples, 1)){ # Check if interval end[border]-start[j] is nonetheless frequent
              if (border < length(filter)) border <- border+1
              next
            } else if (eval.freq(minLoc, filter[border], chrSeg, thold, nroSamples, 2) && (end[filter[border+1]]/nroSamples >= thold)){ # Check if interval end[minLoc]-end[border] is nonetheless frequent
              if (border < length(filter)) border <- border+1
              next
            } else if (eval.freq(filter[border], filter[border+1], chrSeg, thold, nroSamples, 3) && j < length(start)){ # Check if interval end[border]-end[border+1] is nonetheless frequent
              if (border < length(filter)) border <- border+1
              next
            } else {
              regions.start <- c(regions.start, chrSeg$start[minLoc])
              regions.end   <- c(regions.end,   max(end2[border]))
              regions.freq  <- c(regions.freq,  start[minLoc]/nroSamples)
              if (useScore) {
                regions.score <- c(regions.score, scoreMethod(chrSeg$score[minLoc]))
              }
            }
            i <- j-border #j-1
            bool <- FALSE
        } # END IF-ELSE
        if (!bool) break
      } # END FOR
    } else if (startRatio >= thold && bool && i==length(start)) { # If all remaining segments are frequent
      border <- 1
      filter <- which(chrSeg$end >= chrSeg$start[minLoc])
      end2   <- chrSeg$end[filter][order(chrSeg$end[filter])]
      filter <- filter[order(chrSeg$end[filter])]
      for (j in minLoc:length(start)){
        if ((chrSeg$start[minLoc] <= chrSeg$start[filter[border]]) && (chrSeg$end[minLoc] >= chrSeg$end[filter[border]]) && filter[border]!=minLoc){
              if ((chrSeg$end[minLoc] > chrSeg$end[filter[border]]) && (end[minLoc]/nroSamples >= thold)) {
                if (border < length(filter)) border <- border+1
                next
              }
        }
        if (end2[border] < chrSeg$start[j]){
            if (eval.freq(filter[border], j, chrSeg, thold, nroSamples, 1)){ # Check if interval end[border]-start[j] is nonetheless frequent
              if (border < length(filter)) border <- border+1
              next
            } else if (eval.freq(minLoc, filter[border], chrSeg, thold, nroSamples, 2)){ # Check if interval end[minLoc]-end[border] is nonetheless frequent
              if (border < length(filter)) border <- border+1
              next
            } else if (eval.freq(filter[border], filter[border+1], chrSeg, thold, nroSamples, 3)){ # Check if interval end[border]-end[border+1] is nonetheless frequent
              if (border < length(filter)) border <- border+1
              next
            } else {
              regions.start <- c(regions.start, chrSeg$start[minLoc])
              regions.end   <- c(regions.end,   max(end2[border]))
              regions.freq  <- c(regions.freq,  start[minLoc]/nroSamples)
              if (useScore) {
                regions.score <- c(regions.score, scoreMethod(chrSeg$score[minLoc]))
              }
            }
            i <- j-border #j-1
            bool <- FALSE
        } # END IF-ELSE
        if (!bool) break
      } # END FOR
    }
  } # END WHILE
  if (bool) {
    regions.start <- c(regions.start, min(chrSeg$start[minLoc], na.rm=TRUE))
    regions.end   <- c(regions.end,   max(chrSeg$end[which(end/nroSamples >= thold)], na.rm=TRUE))
    regions.freq  <- c(regions.freq,  start[minLoc]/nroSamples)
    if (useScore) {
       regions.score <- c(regions.score, scoreMethod(chrSeg$score[minLoc]))
    }
  }
  # Algorithm ends
  if (length(regions.start) >= 2) {
    if (!useScore){
      regions.score <- rep(NA, length(regions.start)) 
    }
    regions <- data.frame(regions.start[2:length(regions.start)],
                          regions.end  [2:length(regions.end)],
                          regions.freq [2:length(regions.freq)],
                          regions.score[2:length(regions.freq)])
  } else {
    regions <- data.frame(NA,NA,NA,NA) # If no regions were found, return null
  }
  colnames(regions) <- c('start','end','freq','score')
  return(regions)
}

find.follow <- function(start, thold, index, nroSamples) {
  ind <- which(start[index:length(start)]/nroSamples > thold)
  return(index+ind[1]-1)
}

# Compute freq of a snippet i.e. incomplete segment. 
# endend = 0: end-start
# endend = 1: end-end 
# endend = 2: end-end
eval.freq <- function(minLoc, j, chrSeg, thold, nroSamples, endend){
  start <- chrSeg$start
  end   <- chrSeg$end
  occur <- 0
  if (is.na(j)) return(FALSE)
  if (j > length(end) || minLoc > length(end)) return(FALSE)
  if (endend==1){
    if (end[minLoc] == start[j]) return(TRUE)
    for (i in 1:length(start)){
      if (end[minLoc] >= start[i] && start[j] <= end[i]) {
        occur <- occur+1
      }
      if (occur/nroSamples >= thold) return(TRUE)
    }
    return(FALSE)
  } else if (endend==2){
    for (i in 1:length(start)){
      if (end[minLoc] > start[j]) return(FALSE)
      if (end[minLoc] >= start[i] && end[j] <= end[i]){
        occur <- occur+1
      }
      if (occur/nroSamples >= thold) return(TRUE)
    }
    return(FALSE)
  } else if (endend==3){
    for (i in 1:length(start)){
      if (end[minLoc] >= start[i] && end[j] <= end[i]){
        occur <- occur+1
      }
      if (occur/nroSamples >= thold) return(TRUE)
    }
    return(FALSE)
  }

  return(FALSE)
}

main(execute)

