package fi.helsinki.ltdk.csbl.meme;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.asser.sequence.SeqProfile;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

public class MEMERunner extends SkeletonComponent {

    static public final String INPUT_SEQUENCES = "sequences";

    static public final String OUTPUT_MOTIFS      = "motifs";
    static public final String OUTPUT_ANNOTATIONS = "annot";
    static public final String OUTPUT_LOG         = "report";

    static public final String PARAM_LOCATION = "instDir";
    static public final String PARAM_MOTIFS   = "nmotifs";
    static public final String PARAM_MAX_W    = "maxw";
    static public final String PARAM_MIN_W    = "minw";
    static public final String PARAM_REVERSE  = "revcomp";
    static public final String PARAM_PALIND   = "pal";
    static public final String PARAM_CPU      = "cpu";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        File    sequences = cf.getInput(INPUT_SEQUENCES);
        File    motifDir  = cf.getOutput(OUTPUT_MOTIFS);
        File    annotFile = cf.getOutput(OUTPUT_ANNOTATIONS);
        File    logFile   = cf.getOutput(OUTPUT_LOG);
        String  component = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        String  location  = cf.getParameter(PARAM_LOCATION);
        int     motifs    = Integer.parseInt(cf.getParameter(PARAM_MOTIFS));
        int     minw      = Integer.parseInt(cf.getParameter(PARAM_MIN_W));
        int     maxw      = Integer.parseInt(cf.getParameter(PARAM_MAX_W));
        int     cpu       = Integer.parseInt(cf.getParameter(PARAM_CPU));
        boolean revcomp   = Boolean.parseBoolean(cf.getParameter(PARAM_REVERSE));
        boolean pal       = Boolean.parseBoolean(cf.getParameter(PARAM_PALIND));

        runMEME(sequences, logFile, location, motifs, minw, maxw, pal, revcomp, cpu);

        printMotifs(motifDir, annotFile, logFile, component);

        return ErrorCode.OK;
    }

    static private void printMotifs(File   dir,
                                    File   annot,
                                    File   log,
                                    String prefix) throws IOException {
        if (!dir.exists() && !dir.mkdirs())
            throw new IOException("Cannot create: "+dir.getAbsolutePath());

        final String[]  COLS = new String[] { "motif", "width", "sites", "llr", "E" };
        CSVWriter       aOut = new CSVWriter(COLS, annot);
        BufferedReader  in   = new BufferedReader(new FileReader(log));
        int             i    = 1;
        String          line;

        while ((line=in.readLine()) != null) {
            if (line.indexOf("position-specific probability matrix") > 5) {
                printMotif(in, new File(dir, prefix+i+".motif"));
                i++;
            }
            if (line.startsWith("MOTIF")) {
                StringTokenizer toks  = new StringTokenizer(line);
                String          width = null;
                String          sites = null;
                String          llr   = null;
                String          E     = null;
                while (toks.hasMoreTokens()) {
                    String t = toks.nextToken();
                    if (  "width".equals(t)) width = readAnnotation(toks); else
                    if (  "sites".equals(t)) sites = readAnnotation(toks); else
                    if (    "llr".equals(t))   llr = readAnnotation(toks); else
                    if ("E-value".equals(t))     E = readAnnotation(toks);
                }
                aOut.write(prefix+i);
                aOut.write(width, false);
                aOut.write(sites, false);
                aOut.write(llr,   false);
                aOut.write(E,     false);
            }
        }
        in.close();
        aOut.close();
    }

    static private String readAnnotation(StringTokenizer tok) {
        if (!"=".equals(tok.nextToken()))
            throw new IllegalStateException("Syntax error in MEME output. "+
                                            "The version may be incompatible with the runner.");
        return tok.nextToken();
    }

    static private void printMotif(BufferedReader in,
                                   File           name) throws IOException {
        List<SeqProfile> motif = new ArrayList<SeqProfile>(50);
        String           line;

        while ((line=in.readLine()) != null) {
            if (line.startsWith("---------")) {
                if (motif.size() > 0)
                    break;
                continue;
            }
            if (line.startsWith("letter-probability"))
                continue;
            StringTokenizer tok   = new StringTokenizer(line);
            SeqProfile      locus = new SeqProfile(Double.parseDouble(tok.nextToken()),
                                                   Double.parseDouble(tok.nextToken()),
                                                   Double.parseDouble(tok.nextToken()),
                                                   Double.parseDouble(tok.nextToken()),
                                                   false);
            motif.add(locus);
        }
        SeqProfile.writeMotif(motif.toArray(new SeqProfile[motif.size()]), name);
    }

    static private void runMEME(File       sequences,
                                final File logFile,
                                String     location,
                                int        motifs,
                                int        minw,
                                int        maxw,
                                boolean    pal,
                                boolean    revcomp,
                                int        cpu) throws IOException,
                                                       InterruptedException {
        StringBuffer  command = new StringBuffer(512);
        long          size    = sequences.length();
        final Process meme;

        String[] env = new String[] {
                "MEME_DIRECTORY="+location,
                "MEME_BIN="+location+"/bin",
                "MEME_LOGS="+location+"/LOGS",
                "MEME_DB="+location+"/db",
        };

        command.append(location)
               .append("/bin/meme ")
               .append(sequences.getAbsolutePath())
               .append(" -nostatus -text -dna -minw ")
               .append(minw)
               .append(" -maxw ")
               .append(maxw)
               .append(" -nmotifs ")
               .append(motifs)
               .append(" -maxsize ")
               .append(size);
        if (cpu > 1) {
            command.append(" -p ").append(cpu);
        }
        if (pal) {
            command.append(" -pal");
        }
        if (revcomp) {
            command.append(" -revcomp");
        }
        meme = Runtime.getRuntime()
                      .exec(command.toString(),
                            env);
        
        // This thread copies MEME output to the log file.
        Thread outputThread = new Thread() {
            private final InputStream in = meme.getInputStream(); 
            public void run() {
                try {
                  OutputStream out = new FileOutputStream(logFile);
                  byte[]       buf = new byte[512];
                  int          got;
                  
                  while ((got=in.read(buf)) >= 0) {
                      out.write(buf, 0, got);
                  }
                  in.close();
                  out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(ErrorCode.ERROR.getCode());
                }
            }
        };
        outputThread.setDaemon(true);
        outputThread.start();

        BufferedReader err = new BufferedReader(new InputStreamReader(meme.getErrorStream()));
        int status = meme.waitFor();
        // Print possible warnings...
        String line;
        while ((line=err.readLine()) != null) {
            System.out.println(line);
        }
        err.close();
        if (status != 0) {
            System.out.println("Execution command was:\n"+command.toString());
            throw new RuntimeException("MEME failed and returned "+status+'.');
        }
        outputThread.join(60*1000);
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new MEMERunner().run(argv);
    }

}
