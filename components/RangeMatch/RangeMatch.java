import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;


public class RangeMatch extends SkeletonComponent{

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {

		File positions = 	cf.getInput("positions");
		File ranges =		cf.getInput("ranges");
		
		String positionId = cf.getParameter("posID");
		String positionStart = cf.getParameter("posStart");
		String positionEnd = 	cf.getParameter("posEnd");
		
		String rangeId = 	cf.getParameter("rangeID");
		String rangeStart = cf.getParameter("rangeStart");
		String rangeEnd = 	cf.getParameter("rangeEnd");
		String rangeData = 	cf.getParameter("rangeData");
		
		boolean listType = cf.getBooleanParameter("outputList");
		
		String [] rangeCols = rangeData.split(",");
		
		CSVParser pos = 	new CSVParser(positions);
		CSVParser range = 	new CSVParser(ranges);
		
		int rID = -1;
		if(!rangeId.equals(""))
			rID = range.getColumnIndex(rangeId);
		
		int rs = range.getColumnIndex(rangeStart);
		int re = range.getColumnIndex(rangeEnd);
		
		int [] rd = new int[rangeCols.length];
		for(int i = 0; i < rd.length; i++){
			rd[i] = range.getColumnIndex(rangeCols[i]); 
		}

		int pID = -1;
		if(!positionId.equals(""))
			pID = pos.getColumnIndex(positionId);
		
		int ps = pos.getColumnIndex(positionStart);
		int pe = pos.getColumnIndex(positionEnd);
		
		HashMap<String, TreeMap<Double, ArrayList<String[]>>> rangeMap = new HashMap<String, TreeMap<Double, ArrayList<String[]>>>();
				
		//construct the map of ranges
		while(range.hasNext()){
			String [] line = range.next();
			try{
				String key = "NA";
				if(rID != -1){
					key = line[rID];
				}
				
				Double start= new Double(Double.parseDouble(line[rs]));
				Double end	= new Double(Double.parseDouble(line[re]));

				if(!rangeMap.containsKey(key)){
					rangeMap.put(key, new TreeMap<Double, ArrayList<String[]>>());
				
				}
			
				String [] dat = new String[rd.length];
				
				for(int i = 0; i < rd.length; i++){
					dat[i] = line[rd[i]];
				
				}
			
				addToMap(rangeMap, key, start, dat);//.toString());
				addToMap(rangeMap, key, end, dat);//.toString());
				
			}catch (NullPointerException e) {
				// no need to handle, this is in the case of NAs
			}
		}

		String [] heading = new String[pos.getColumnNames().length + rangeCols.length];
		
		int i = 0;
		for(String col : pos.getColumnNames()){
			heading[i] = col;
			i ++;
		}
		for(i = 0; i < rangeCols.length; i++){
			heading[pos.getColumnNames().length + i] = rangeCols[i];
		}
		
		CSVWriter out = new CSVWriter(heading, cf.getOutput("result"));
		
		//look the positions from the map
		while(pos.hasNext()){

			HashSet<String[]> result = new HashSet<String[]>();
			
			String [] line = pos.next();
			//first the id
			String key = "NA";
			if(pID != -1){
				key = line[pID];
			}
			
			if(rangeMap.containsKey(key)){
				
				try{
					Double start	= new Double(Double.parseDouble(line[ps]));
					Double end		= new Double(Double.parseDouble(line[pe]));
					result.addAll(locateRangeNames(rangeMap.get(key), start, true));
					result.addAll(locateRangeNames(rangeMap.get(key), end, false));
				
					//loop all positions between start and end, and collect the ranges within
					Double higher = rangeMap.get(key).higherKey(start);

					while(higher != null && higher < end){
						result.addAll(rangeMap.get(key).get(higher));
						higher = rangeMap.get(key).higherKey(higher);
					}
				
				}catch (NullPointerException e) {
					// no need to handle, this is in the case of NAs
				}

				if(listType){
					StringBuffer [] columns = new StringBuffer[rangeCols.length];
					for(String[] res : result){
						for(i = 0; i < columns.length; i++){
							if(columns[i] == null)
								if(res[i] == null)
									columns[i] = new StringBuffer("NA");
								else
									columns[i] = new StringBuffer(res[i]);
							else{
								if(res[i] == null)
									columns[i].append(",NA");
								else{
									columns[i].append(",");
									columns[i].append(res[i]);
								}
							}
						}
					}


					for(String posCols : line)
						out.write(posCols);

					for(StringBuffer sb : columns){
						if(sb == null|| sb.toString().equals("NA"))
							out.write(null);
						else
							out.write(sb.toString());
					}
				}else{
					for(String[] res : result){
						for(String posCols : line)
							out.write(posCols);

						for(i = 0; i < res.length; i++){
							out.write(res[i]);
						}
					}
				}
				
			}else{
				
				for(String posCols : line)
					out.write(posCols);
				
				for(i = 0; i < rangeCols.length; i++)
					out.write(null);
			}
		}
		out.close();
		return ErrorCode.OK;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new RangeMatch().run(args);

	}

	private void addToMap(HashMap<String, TreeMap<Double, ArrayList<String[]>>> rangeMap, String rangeID, Double position, String[] rangeName){

		if(rangeMap.get(rangeID).containsKey(position)){
			rangeMap.get(rangeID).get(position).add(rangeName);
		}else{
			rangeMap.get(rangeID).put(position, new ArrayList<String[]>());
			rangeMap.get(rangeID).get(position).add(rangeName);
		}
	}
	
	private HashSet<String[]> locateRangeNames(TreeMap<Double, ArrayList<String[]>> map,
						Double position, boolean lower){
		
		
		HashSet<String[]> result = new HashSet<String[]>();
		
		Double next = null;
		
		if(lower)
			next = map.lowerKey(position);
		else
			next = map.higherKey(position);
			

		while(next != null){
				
			ArrayList<String[]> tmp = new ArrayList<String[]>(map.get(next));
			ArrayList<String[]> tmp2 = new ArrayList<String[]>(map.get(next));
			
			//if the start and end are in the same position, remove both occurences
			
			for(String [] o : tmp2){
				if(tmp2.indexOf(o) != tmp2.lastIndexOf(o)){ //if the line has start and end in same position -> remove
					tmp.remove(o);
					tmp.remove(o);
				}
			}
			tmp.removeAll(result);
			result.removeAll(tmp2);
			result.addAll(tmp);

			if(lower)
				next = map.lowerKey(next);
			else
				next = map.higherKey(next);
		}
		if(map.containsKey(position)){
			result.addAll(map.get(position));		
		}	

		return result;
	}
	
}
