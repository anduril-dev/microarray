% sort data matrix in to a vector and calculate Log2 of the values
table.data=csvcell2num(table1.data);
table.data=sort(table.data(:));
table.data=[table.data, log2(table.data)];
table.columnheads={'Original','Log2'};

% plot the data in a figure (visible off for better performance)
x=(1:size(table.data,1))';
figureid=figure('Visible','off');
plot([x,x],table.data);
xlabel('index');
title('Test plot');

% write the figure at PNG file with accompanied latex document.tex
writefigurelatex(document.dir,figureid,'Plot name','png','Caption','ex_plot',instance.name);
document.out='This text is added after the figure';
