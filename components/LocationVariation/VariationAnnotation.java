import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import fi.helsinki.ltdk.csbl.VariationAnnotation.EnsemblMapping;
import fi.helsinki.ltdk.csbl.VariationAnnotation.EnsemblVariation;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;


public class VariationAnnotation extends SkeletonComponent {

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		
		EnsemblVariation ens = new EnsemblVariation(cf.getInput("variationDatabase"));
		
		String chr = cf.getParameter("chromosome");
		String start = cf.getParameter("start");
		String end = cf.getParameter("end");
		String rsId = cf.getParameter("rsId");
		String[] rowRange = cf.getParameter("rowRange").split("-");
		
		boolean frequencies = cf.getBooleanParameter("getFreq");
		
		int rangeS = -1;
		int rangeE = -1;
		
		if(rowRange.length > 1){
			try{
				rangeS = Integer.parseInt(rowRange[0]);
				rangeE = Integer.parseInt(rowRange[1]);
			}catch (NumberFormatException e) {
				System.err.println("Error in row ranges "+e);
				return ErrorCode.PARAMETER_ERROR;
			}
		}
		
		boolean onePos = false;
		
		if(end.equals("")) {
			end = start;
			onePos = true;
		}
		
		CSVParser in = new CSVParser(cf.getInput("locations"));
	
		CSVWriter out;
		CSVWriter pop = new CSVWriter(ens.getFrequenciesHeaders(), cf.getOutput("populations"));
		
		
		if(cf.getBooleanParameter("location2variation") == false)
			out = new CSVWriter(new String[]{rsId, "Chromosome", "Location"}, cf.getOutput("variations"));		
		else if(onePos)
			out = new CSVWriter(new String[]{chr, start, "Variation", "Consequence", "Polyphen", "Sift", "Peptide", "Codon"}, cf.getOutput("variations"));
		else
			out = new CSVWriter(new String[]{chr, start, end, "Variation", "Consequence","Polyphen", "Sift", "Peptide", "Codon"}, cf.getOutput("variations"));
		
		int row = 1;
		
		ArrayList<String> rsIdList = new ArrayList<String>();
		

		while(in.hasNext()){
			
			String [] line = in.next();
			
			if(cf.getBooleanParameter("location2variation") == false && row >= rangeS){
				rsIdList.add(line[in.getColumnIndex(rsId)]);
			}else if(rangeE != -1 && row > rangeE){
				break;
			}else if(row >= rangeS){
				
				int resultSize = 6;
				
				List<Object[]> res = ens.getVariations(line[in.getColumnIndex(chr)], 
								line[in.getColumnIndex(start)], 
								line[in.getColumnIndex(end)], 
								new String[]{EnsemblMapping.VARIATION.toString(),
											EnsemblMapping.CONSEQUENCE.toString(),
											EnsemblMapping.INTERNAL_VARIATION_ID.toString()}, 
								true, 
								new String[]{EnsemblMapping.SIFT.toString(), EnsemblMapping.POLYPHEN.toString(), 
												EnsemblMapping.PEPTIDE.toString(), EnsemblMapping.CODON.toString()});
			
				
				if(res.size() == 0){
					out.write(line[in.getColumnIndex(chr)]);
					out.write(line[in.getColumnIndex(start)]);
				
					if(!onePos)
						out.write(line[in.getColumnIndex(end)]);

					for(int i = 0; i < resultSize; i++)
						out.write("NA");
					
				
				}else{
					for(Object[] r: res){
						out.write(line[in.getColumnIndex(chr)]);
						out.write(line[in.getColumnIndex(start)]);
					
						if(!onePos)
							out.write(line[in.getColumnIndex(end)]);

						writeResult(Arrays.copyOfRange(r, 0, 2), out);
						writeResult(Arrays.copyOfRange(r, 3, resultSize + 1), out);
						
					}
					//get the populations
					if(frequencies){
						Long [] varIDs = new Long[res.size()];
					
						int i = 0;
						for(Object [] vs: res){
							varIDs[i] = (Long)vs[2];
							i++;
						}
					
						String[] population = new String[]{""};
						
						if(!cf.getParameter("population").equals("*")){
							population = cf.getParameter("population").split(",");
						}
						List<Object[]> frec = ens.getFrequencies(new String[]{}, varIDs, true, population, true);
						
						for(Object[] os : frec){
							writeResult(os, pop);
						}
					}
				}
				
			}
			row ++;
		}
		
		if(cf.getBooleanParameter("location2variation") == false){
			List<Object[]> res = ens.getLocations(rsIdList.toArray(new String[rsIdList.size()]),
					new String[]{EnsemblMapping.VARIATION.toString(),
								EnsemblMapping.CHROMOSOME.toString(),
								EnsemblMapping.LOCATION_START.toString()},
					false, new String[]{});

			for(Object [] o : res)
				writeResult(o, out);
		}
		
		pop.close();
		out.close();
		return ErrorCode.OK;
	}

	private void writeResult(Object[] o, CSVWriter out){
		for(int i = 0; i < o.length; i++){
			if(o[i] == null)
				out.write(null);
			else
				out.write(o[i].toString());
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		new VariationAnnotation().run(args);
	}

}
