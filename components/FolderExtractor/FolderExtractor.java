import java.io.File;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;

public class FolderExtractor extends SkeletonComponent {
    public static final int MAX_OUTPUTS = 9;
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        File folder = cf.getInput("folder");
        
        for (int i=1; i<=MAX_OUTPUTS; i++) {
            final File output = cf.getOutput("file"+i);
            final String filename = cf.getParameter("filename"+i);
            if (filename.isEmpty()) {
                /* Write empty file */
                output.createNewFile();
                continue;
            }
            
            File fullname = new File(folder, filename);
            if (!fullname.exists()) {
                cf.writeError("File does not exist: "+filename);
                return ErrorCode.PARAMETER_ERROR;
            }
            
            Tools.copyFile(fullname, output);
        }
        
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new  FolderExtractor().run(args);
    }
}
