#!/bin/bash

for i in 1 2 3 4
do
  sed -E 's/\& ([A-Za-z0-9 ]*) \& ([0-9]+) \& ([-]?)/\& \1 \& \2 \& {{\\-{0,1}}}/g' ../../../component-test/SVDAnalyzer/case$i/component/report/document.tex > testcases/case$i/expected-output/report/document.tex
  sed -E 's/"([0-9]+)"\t"([-]?)/"\1"\t"{{\\-{0,1}}}/g' ../../../component-test/SVDAnalyzer/case$i/component/resultTable.csv > testcases/case$i/expected-output/resultTable.csv
done
