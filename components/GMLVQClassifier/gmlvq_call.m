try
    addpath(genpath('LVQ'));
    cf=readcommandfile(commandfile);
    
    trainFile=getinput(cf,'data');
    validateFile=getinput(cf,'testData');
    testFile=getinput(cf,'classifyData');
    classifierFile=getinput(cf,'classifier');
    
    fclose(fopen(getoutput(cf,'importances'), 'w'));
    % validate inputs
    if and(inputdefined(cf,'classifier'),~inputdefined(cf,'classifyData'))
        writeerror(cf,'If Classifier given, needs also classifydata!')
        exit
    end
    if all([inputdefined(cf,'classifyData'), ~inputdefined(cf,'classifier'), ~inputdefined(cf,'data')])
        writeerror(cf,'If classifydata given, but no classifier, we are gong to need training data!')
        exit
    end
    if and(~inputdefined(cf,'classifier'),~inputdefined(cf,'data'))
        writeerror(cf,'If Classifier not given, need to have training data!')
        exit
    end
    
    % If no classifier, start training
    if strcmp(classifierFile,'')
        disp('Training...')
        [trainData,validateData]=gmlvq_preprocess(cf,'train');
        results=gmlvq_train_start(cf,trainData,validateData);
    
        lambda=gmlvq_importances(cf,results,trainData);
        writefilecsv(getoutput(cf,'importances'), lambda);
    end
    disp('Predicting...')
    if strcmp(testFile,'')
        % Make predictions with validation data
        prediction=gmlvq_predict_start(cf,results,validateData);
        if strcmp(validateFile,'')
            % if validation not set, use the training data.
            predictionFile=trainFile;
        else
            predictionFile=validateFile;
        end
    end
    
    if ~strcmp(testFile,'')
        % Make predictions with classifyData
        
        if ~strcmp(classifierFile,'')
            load( classifierFile );
        end
        
        [~,testData]=gmlvq_preprocess(cf,'predict',results);
        prediction=gmlvq_predict_start(cf,results,testData);
        
        predictionFile=testFile;
    end
    
    % Write prediction output
    predictionCSV=readcsvcell(predictionFile);
    predictionCSV.columnheads{end+1}=['Predicted' getparameter(cf,'classColumn','string')];
    if iscell(results.label_coding)
        prediction=results.label_coding(prediction);
    else
        prediction=num2csvcell(prediction);
    end
    predictionCSV.data(:,end+1)=prediction;
    writefilecsv(getoutput(cf,'predictedClasses'), predictionCSV);
    save( getoutput(cf,'classifier'), 'results' );

    confusion=gmlvq_confusion(cf,predictionCSV,results.label_coding);
    writefilecsv(getoutput(cf,'confusion'), confusion);

    evaluation=gmlvq_evaluation(cf,confusion);
    writefilecsv(getoutput(cf,'evaluation'), evaluation);
    

catch myError
    try
        whos
        writeerrorstack(cf, myError);
    catch ErrorAgain
        exit
    end
end
exit;  
