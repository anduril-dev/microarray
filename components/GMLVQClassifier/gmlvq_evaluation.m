function ev=gmlvq_evaluation(cf, confusion)
% Sokolova, M., & Lapalme, G. (2009). A systematic analysis of performance measures for classification tasks. Information Processing and Management, 45, p. 427-437.
% http://rali.iro.umontreal.ca/rali/sites/default/files/publis/SokolovaLapalme-JIPM09.pdf
% This function returns the list of "standard" classifier evaluation values

ev=struct('columnheads', [],'data',[]);
ev.columnheads={'precision', 'sensitivity', 'specificity', 'accuracy',...
                       'errorrate','F1score','precision_micro','sensitivity_micro','F1score_micro'};

classes=confusion.columnheads(2:end);
nClass=length(classes);
confusion=cellfun(@(x) str2num(x), confusion.data(:,2:end));

TP=zeros(nClass,1);
TN=TP;
FP=TP;
FN=TP;
accuracy=TP;
errorrate=TP;
precision=TP;
sensitivity=TP; % AKA recall
specificity=TP;

for c=1:nClass
    TP(c)=confusion(c,c);
    FP(c)=sum(  confusion( 1:nClass~=c , c ) );
    TN(c)=sum( sum(  confusion( 1:nClass~=c , 1:nClass~=c ) ) );
    FN(c)=sum(  confusion( c, 1:nClass~=c ) );

    accuracy(c)  = (TP(c)+TN(c))/(TP(c) + FP(c) + FN(c) + TN(c));  % average per-class accuracy
    errorrate(c) = (FP(c)+FN(c))/(TP(c) + FP(c) + FN(c) + TN(c));  %     "
    precision(c) = TP(c)/(TP(c)+FP(c));                            %     "
    sensitivity(c) = TP(c)/(TP(c)+FN(c));                          %     " ( or recall)
    specificity(c) = TN(c)/(TN(c)+FP(c));                          %

end

precisionmu = sum(TP) / (sum(TP)+sum(FP));   % Agreement of the data class labels with those of a classifiers if calculated from sums of per-text decisions
sensitivitymu = sum(TP) / (sum(TP)+sum(FN)); % Effectiveness of a classifier to identify class labels if calculated from sums of per-text decisions

betasq = 1*1;
F1   = ( betasq + 1 )*nanmean(precision)*nanmean(sensitivity) / (betasq * nanmean(precision) + nanmean(sensitivity) );  %  average per-class F1 score
F1mu = ( betasq + 1 )*precisionmu*sensitivitymu / (betasq * precisionmu + sensitivitymu );  %  F1 score

ev.data=[ nanmean(precision), nanmean(sensitivity), ...
          nanmean(specificity), nanmean(accuracy), ...
          nanmean(errorrate), F1, ...
          precisionmu, sensitivitymu, F1mu ];
ev.data=num2csvcell(ev.data);



