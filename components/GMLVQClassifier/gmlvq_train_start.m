function realresults=gmlvq_train_start(cf,trainData,validateData)

method=getparameter(cf,'method','string');
results = struct('model',[],'setting',[], 'classifierMethod',[], ...
                  'zscore_model',[],'trainError',[],'validateError',[], ...
                  'label_coding',[]);

results.label_coding=trainData.label_coding;
results.classifierMethod=method;
results.zscore_model=trainData.zscore_model;
projectionDimension = size(trainData.data,2);

iterTrainErr=2;
for iter=1:getparameter(cf,'iterations','float')
switch method
    case 'LGMLVQ'
        
        LGMLVQparams = struct('PrototypesPerClass', getparameter(cf,'prototypes','float'), ...
                              'dim',projectionDimension,'regularization',0);

        [results.model,results.setting,results.trainError] = LGMLVQ_train(trainData.data, ...
                                    trainData.labels,'dim',LGMLVQparams.dim,'PrototypesPerClass',LGMLVQparams.PrototypesPerClass,...
                                    'regularization',LGMLVQparams.regularization);

        estimatedValidateLabels = LGMLVQ_classify(validateData.data, results.model);
        results.validateError = mean( validateData.labels ~= estimatedValidateLabels );

    case 'GMLVQ'
    
        GMLVQparams = struct('PrototypesPerClass',getparameter(cf,'prototypes','float'), ...
                             'dim',projectionDimension,'regularization',0);

        [results.model,results.setting] = GMLVQ_train(trainData.data, ...
                                                  trainData.labels,'dim',GMLVQparams.dim, ...
                                                  'PrototypesPerClass',GMLVQparams.PrototypesPerClass,...
                                                  'regularization',GMLVQparams.regularization);
        estimatedTrainLabels = GMLVQ_classify(trainData.data, results.model);
        results.trainError = mean( trainData.labels ~= estimatedTrainLabels );
        estimatedValidateLabels = GMLVQ_classify(validateData.data, results.model);
        results.validateError = mean( validateData.labels ~= estimatedValidateLabels );

    case 'GRLVQ'

        GRLVQparams = struct('PrototypesPerClass',getparameter(cf,'prototypes','float'), ...
                             'regularization',0);

        [results.model,results.setting,results.trainError,results.validateError,costs] = GRLVQ_train(trainData.data, ...
                        trainData.labels,'PrototypesPerClass',GRLVQparams.PrototypesPerClass,...
                        'testSet',[validateData.data,validateData.labels], ...
                        'regularization',GRLVQparams.regularization); % ,'optimization','sgd'

    otherwise
        writeerror(cf,['Method ' method ' was not recognized'])
        return
end

if iterTrainErr>results.trainError
    realresults=results;
    iterTrainErr=results.trainError;
end

end
