library(componentSkeleton)

width.pixels  <- 1024

execute <- function(cf) {
  	library(lumi)
  	library(beadarray)

  	# Read input parameters
  	data		<- get.input(cf, 'expr')
  	plot.type	<- get.parameter(cf, 'plotType') #comma-separated list of plots
	title		<- get.parameter(cf, 'sectionTitle')
	width.cm    <- get.parameter(cf, 'width', 'float')
    height.cm   <- get.parameter(cf, 'height', 'float')


	if (width.cm == 0) {
   		write.error(cf, 'Parameter "width" is 0')
        return(INVALID_INPUT)
    }
    if (height.cm == 0) {
        write.error(cf, 'Parameter "height" is 0')
        return(INVALID_INPUT)
    }

	aspect        <- width.cm / height.cm
  	height.pixels <- as.integer(round(width.pixels / aspect))

	#plot.type vector
	plot.type	<- split.trim(plot.type,",")

	if (length(plot.type) == 0) {
		write.error(cf, 'plotType not defined')
		return(INVALID_INPUT)
	}

  	# Read data in and set column "RowName" as row names 
  	expr		<- read.delim(data, row.names="RowName", as.is=FALSE) 


	# ("Hash") list of plot names
	plotName	<- list("density" = "Density plot", "boxplot" = "Box plot", "pair" = "Scatter plot", "MAplot" = "MA-plot", "MAXYplot" = "MAXY-plot", "sampleRelation" = "Sample relations (hierarchical clustering)")

	# ("Hash") list of plot descriptions
	plotDescription <- list("density" = "Density plot indicates the distribution of variables.", "boxplot" = "The bold line shows the location of median. The filled rectangle contains values between 25'th and 75'th percentile. The extremes show locations of minimum and maximum.", "pair" = "Scatter plot illustrates the degree of sample correlation.", "MAplot" = "MA-plot visualizes intensity-dependent ratio of microarray data.", "MAXYplot" = "Scatter plots (in the lower left) illustrates the degree of sample correlation. MA-plots (in the upper right) visualizes intensity-dependent ratio of microarray data.", "sampleRelation" = "Hierarchical clustering is the assignment of a set of observations into subsets (clusters) so that observations in the same cluster are similar in some sense.")

  	# Create a LumiBatch-object
  	# se.exprs is nonsense, real data (bead sd's) not available!
  	lumiBatch	<- new("LumiBatch", exprs = as.matrix(expr), se.exprs = as.matrix(expr))
  
  	# For plot.type == "density | boxplot | sampleRelation" plot all samples into the same figure
  	# For plot.type == "pair | MAplot | MAXYplot" plot max 4 samples into the same figure
	out.dir <- get.output(cf, 'plot')
  	dir.create(out.dir, recursive = TRUE)
	instanceName <- get.metadata(cf, 'instanceName')
	tex <- sprintf('\\section{%s}', title)

	#For each plot
	for (pType in plot.type) {
    if (pType == "") {
		next
  	}

    write.log(cf, sprintf('Plotting %s...', pType))
	figurename <- paste(pType, "-", instanceName, "-figure.png", sep = "")
	section.title <- plotName[[pType]]
	figure.description <- plotDescription[[pType]]
	tex <- c(tex, sprintf('\\subsection{%s}', section.title))

  	if (pType=="density"|pType=="boxplot"|pType=="sampleRelation") {
		filename <- file.path(out.dir, figurename)  
  		png.open(filename, width = width.pixels, height = height.pixels)
		plot(lumiBatch, what = pType)
		tex <- c(tex, latex.figure(figurename, caption = figure.description, image.width = width.cm, fig.label = section.title))
		png.close()
	} else if (pType=="pair"|pType=="MAplot") {
		if (ncol(expr) < 2) {
			write.error(cf, 'at least two samples needed for Scatter plot, MA-plot and MAXY-plot')
			return(INVALID_INPUT)
		} else if (ncol(expr) > 4) {
			sampleAmount <- ncol(expr)
			if (sampleAmount %% 4 == 1 | sampleAmount %% 4 == 2) {
				x <- 1
				i <- 1
				while (i <= sampleAmount) {			
					j <- i+2
					if (j > sampleAmount) j <- sampleAmount
					figurename	<- paste(pType, "-", instanceName, "-figure", x, ".png", sep = "") 
  					filename 	<- file.path(out.dir, figurename)  
  					png.open(filename, width = width.pixels, height = height.pixels)
					plot(lumiBatch[,i:j], what = pType)
					tex <- c(tex, latex.figure(figurename, caption = figure.description, image.width = width.cm, fig.label = section.title))
					png.close()
					x <- x+1	 	
					i <- i+3
				}	
			}
			else {
				x <- 1
				i <- 1
				while (i <= sampleAmount) {			
					j <- i+3
					if (j > sampleAmount) j <- sampleAmount
					figurename	<- paste(pType, "-", instanceName, "-figure", x, ".png", sep = "") 
  					filename 	<- file.path(out.dir, figurename)  
  					png.open(filename, width = width.pixels, height = height.pixels)
					plot(lumiBatch[,i:j], what = pType)
					tex <- c(tex, latex.figure(figurename, caption = figure.description, image.width = width.cm, fig.label = section.title))
					png.close()
					x <- x+1	 	
					i <- i+4
				}
			}
		}
		else {
			filename 	<- file.path(out.dir, figurename)  
  			png.open(filename, width = width.pixels, height = height.pixels)
			plot(lumiBatch, what = pType)
			tex <- c(tex, latex.figure(figurename, caption = figure.description, image.width = width.cm, fig.label = section.title))
			png.close()	
		}	
	} else if (pType == "MAXYplot") {
		if (ncol(expr) < 2) {
			write.error(cf, 'at least two samples needed for Scatter plot, MA-plot and MAXY-plot')
			return(INVALID_INPUT)
		} else if (ncol(expr) > 4) {
			sampleAmount <- ncol(expr)
			if (sampleAmount %% 4 == 1 | sampleAmount %% 4 == 2) {
				x <- 1
				i <- 1
				while (i <= sampleAmount) {			
					j <- i+2
					if (j > sampleAmount) j <- sampleAmount
					figurename	<- paste(pType, "-", instanceName, "-figure", x, ".png", sep = "") 
  					filename 	<- file.path(out.dir, figurename)  
  					png.open(filename, width = width.pixels, height = height.pixels)
					plotMAXY(2^(as.matrix(expr)), arrays=i:j)
					tex <- c(tex, latex.figure(figurename, caption = figure.description, image.width = width.cm, fig.label = section.title))
					png.close()
					x <- x+1	 	
					i <- i+3
				}	
			}
			else {
				x <- 1
				i <- 1
				while (i <= sampleAmount) {			
					j <- i+3
					if (j > sampleAmount) j <- sampleAmount
					figurename	<- paste(pType, "-", instanceName, "-figure", x, ".png", sep = "") 
  					filename 	<- file.path(out.dir, figurename)  
  					png.open(filename, width = width.pixels, height = height.pixels)
					plotMAXY(2^(as.matrix(expr)), arrays=i:j)
					tex <- c(tex, latex.figure(figurename, caption = figure.description, image.width = width.cm, fig.label = section.title))
					png.close()
					x <- x+1	 	
					i <- i+4
				}
			}
		}
		else {
			filename 	<- file.path(out.dir, figurename)  
  			png.open(filename, width = width.pixels, height = height.pixels)
			plotMAXY(2^(as.matrix(expr)), arrays=1:(ncol(expr)))
			tex <- c(tex, latex.figure(figurename, caption = figure.description, image.width = width.cm, fig.label = section.title))
			png.close()	
		}	
	}
	#Pagebreak after each plot
	tex <- c(tex, '\\cleardoublepage{}')

	}#for

	latex.write.main(cf, 'plot', tex)
  	return(0)
}

main(execute)
