
import java.util.*;
import java.io.*;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

/* 
 * The purpose of this component is to integrate  
 * gene/transcript/other expression data with binary
 * explanatory data derived in some way (eg. ACGH)
 * 
 * This class implements the data integration method
 * proposed by Hautaniemi et al. in 
 * Journal of the Franklin Institute 2004 Jan-Mar;341(1-2):77-88
 * 
 * @author Riku Louhimo
 */
public class ExpExpIntegration extends SkeletonComponent {

    File E_file;
    File I_file;
    File outputFile;
    boolean gainData;
    ArrayList<Double> alpha = new ArrayList<Double>(5000);
    Vector<Double> weight = new Vector<Double>();
    ArrayList<String> id = new ArrayList<String>(5000);
    ArrayList<String> group0 = new ArrayList<String>(5000);
    ArrayList<String> group1 = new ArrayList<String>(5000);
    Random randomNumber = new Random(21474833457235L);

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        outputFile = cf.getOutput("Values");
        E_file = cf.getInput("exprMatrix");
        I_file = cf.getInput("labelMatrix");
        gainData = Boolean.parseBoolean(cf.getParameter("gainData"));
        int np = Integer.parseInt(cf.getParameter("nroOfperms"));
        int idColumn = Integer.parseInt(cf.getParameter("idColumn"));
        int minGroupSize = Integer.parseInt(cf.getParameter("minGroupSize"));
        double naLimit = Double.parseDouble(cf.getParameter("exprNALimit"));
        CSVParser csvE = new CSVParser(E_file);
        CSVParser csvI = new CSVParser(I_file);
        String[] columnNames = csvI.getColumnNames();
        String[] columnNamesE = csvE.getColumnNames();
        
        if (csvI.getColumnCount() != csvE.getColumnCount()){ 
            cf.writeError("Matrix dimensions do not match.");
            return ErrorCode.INVALID_INPUT;
        }
        
        for (int s = idColumn+1; s < columnNames.length; s++){
            if (!columnNames[s].equals(columnNamesE[s])){
                cf.writeError("Column names do not match. Probable cause: column names in different order");
                cf.writeError("Conflicting columns: exprMatrix "+columnNames[s]+" labelMatrix "+columnNamesE[s]);
                return ErrorCode.INVALID_INPUT;
            }
        }
        
        cf.writeLog("Starting permutation testing...");
        while (csvE.hasNext()) {
            // INITIATION
            String[] eValues = csvE.next();
            String[] iValues = csvI.next();
        
            int naCount = 0;
            int grp0size = 0;
            int grp1size = 0;
            String grpList0 = "NA";
            String grpList1 = "NA";
            if (iValues[idColumn] != null){
                for (int p=idColumn; p < eValues.length; p++){ // Calculate na frequency for measurements
                    if (eValues[p] == null){
                        naCount++;
                    }
                    if (Integer.parseInt(iValues[p]) == 0){ // Calculate size of group 0
                        grp0size++;
                        if (grp0size == 1){
                            grpList0 = columnNames[p];
                        }
                        if (grp0size > 1) grpList0 = grpList0.concat(","+columnNames[p]);
                    }
                    if (Integer.parseInt(iValues[p]) == 1){ // Calculate size of group 1
                        grp1size++;
                        if (grp1size == 1){
                            grpList1 = columnNames[p];
                        }
                        if (grp1size > 1) grpList1 = grpList1.concat(","+columnNames[p]);
                    }
                }
            }
            group0.add(grpList0);
            group1.add(grpList1);
            double naFreq = (double)naCount / (double)(eValues.length-idColumn);

            if (naFreq < naLimit && iValues[0] != null && grp0size >= minGroupSize && grp1size >= minGroupSize){ //iValues[1] == null means no explanatory data
                int counter = 0;
                // STATISTICAL CALCS
                double[] m0sd = getMeanAndSD(eValues, iValues, 0, idColumn);
                double[] m1sd = getMeanAndSD(eValues, iValues, 1, idColumn);
                if ((m0sd[1] + m1sd[1]) == 0){ // sum of SDs equals zero
                    weight.add(0.0);
                } else {
                    if (gainData){
                        weight.add((m1sd[0] - m0sd[0]) / (m0sd[1] + m1sd[1]));
                    } else {
                        weight.add(-1*((m1sd[0] - m0sd[0]) / (m0sd[1] + m1sd[1])));
                    }
                }
                // PERMUTATION TESTING
                for (int i = 0; i < np; i++) {
                    double Wi = weight.lastElement();
                    String[] perm = new String[iValues.length-idColumn];
                    for (int j=0; j < perm.length; j++){
                        perm[j] = iValues[j+idColumn];
                    }
                    permute(perm); // get one random permutation
                    double[] Pm0sd = getMeanAndSD(eValues, perm, 0, idColumn);
                    double[] Pm1sd = getMeanAndSD(eValues, perm, 1, idColumn);
                    double Wnew = (Pm1sd[0] - Pm0sd[0]) / (Pm0sd[1] + Pm1sd[1]);
                    if (Math.abs(Wnew) > Math.abs(Wi))
                        counter++;
                }
                if (counter == 0) counter++; // Don't allow p-value zero
                alpha.add((double) counter / (double) np);
                if (idColumn != 0) {
                    if (iValues[idColumn-1].equals(eValues[idColumn-1])){
                        id.add(iValues[idColumn-1]);
                    } else {
                        cf.writeError("Row IDs do not match.");
                        System.out.println(iValues[idColumn-1]);
                        System.out.println(eValues[idColumn-1]);
                        return ErrorCode.INVALID_INPUT;
                    }
                }
            } else {
                weight.add(Double.NaN);
                alpha.add(Double.NaN);
                if (idColumn != 0) id.add(iValues[idColumn-1]);
                //cf.writeLog(e.toString());
            }
        }
        // OUTPUT
        cf.writeLog("Permutation testing...done...Writing results...");
        CSVWriter writer = null;
        try {
            if (idColumn == 0){
                String[] columnHeaders = { "index", "weight", "alpha", "group0", "group1" };
                writer = new CSVWriter(columnHeaders, outputFile, false);
                for (int i = 0; i < weight.size(); i++) {
                    writer.write(i+1);
                    writer.write(weight.get(i));
                    writer.write(alpha.get(i));
                    writer.write(group0.get(i), false);
                    writer.write(group1.get(i), false);
                }
            } else {
                String idColumnHeader = csvI.getColumnNames()[idColumn-1];
                String[] columnHeaders = { idColumnHeader, "index", "weight", "alpha", "group0", "group1" };
                writer = new CSVWriter(columnHeaders, outputFile, false);
                for (int i = 0; i < weight.size(); i++) {
                    writer.write(id.get(i));
                    writer.write(i+1);
                    writer.write(weight.get(i));
                    writer.write(alpha.get(i));
                    writer.write(group0.get(i), false);
                    writer.write(group1.get(i), false);
                }                
            }
        } catch (Exception e) {
            throw (e);
        } finally {
            try {
                writer.close();
            } catch (NullPointerException e) {
                throw (e);
            }
        }
        return ErrorCode.OK;
    }

    // Utility methods
    private double[] getMeanAndSD(String[] E, String[] I, int indicator, int idColumn) {
        double[] meanAndSD = new double[2];
        double sd = 0;
        int samplesize = 0;
        for (int i = idColumn; i < I.length; i++) {
            if (Integer.parseInt(I[i]) == indicator) {
                if (E[i] != null){
                    meanAndSD[0] = meanAndSD[0] + Double.parseDouble(E[i]); // sum through the vector
                    samplesize++;
                } //Do nothing if value E[i] is NA
            }
        }
        meanAndSD[0] = meanAndSD[0] / (double)samplesize; // average over samples
        for (int i = idColumn; i < I.length; i++) {
            if (Integer.parseInt(I[i]) == indicator) {
                if (E[i] != null){
                    sd = sd + Math.pow(Double.parseDouble(E[i]) - meanAndSD[0], 2);
                }  //Do nothing if value E[i] is NA
            }
        }
        meanAndSD[1] = Math.sqrt(sd / (double)samplesize);
        return meanAndSD;
    }

    /*
     * Permutation algorithm based on Richard Durstenfeld's
     * version of the Fisher-Yates shuffle
     */
    private void permute(String[] array) {
        int inv = array.length;
        while (inv > 1) {
            inv--;
            int index = randomNumber.nextInt(inv + 1);
            String temp = array[index];
            array[index] = array[inv];
            array[inv] = temp;
        }
    }
    
    public static void main(String[] args) {
        new ExpExpIntegration().run(args);
    }
}
