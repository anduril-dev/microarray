import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.math3.stat.descriptive.rank.Median;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.anduril.component.CSVReader;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

public class IDConvert extends SkeletonComponent {
    public static final String NA_STRING = "NA";
    
    class LengthComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            if (o1 == null && o2 == null) return 0;
            else if (o1 == null) return -1;
            else if (o2 == null) return 1;
            
            final int size1 = o1.length();
            final int size2 = o2.length();
            if (size1 < size2) return -1;
            else if (size1 == size2) return 0;
            else return 1;
        }
    }
    
    enum CollapseNumeric { MEDIAN, MEAN, MAX, MIN, FIRST, MAJORITY, INDICATOR, CONSENSUS, SUM }
    enum CollapseString { FIRST, MAJORITY, CONSENSUS, LONGEST, SHORTEST }
    
    private CommandFile cf;
    private Map<String, String> convMap;
    private boolean split;
    private boolean splitConverted;
    private boolean originalWhenMissing;
    private boolean dropMissing;
    private String varMeasure;
    private double varThreshold;
    private CollapseNumeric collapseNumeric;
    private CollapseString collapseString;
    
    public IDConvert() {
        convMap = new HashMap<String, String>();
    }
    
    @Override
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        this.cf = cf;
        ErrorCode status = loadConvMap();
        if (status != ErrorCode.OK) return status;
        
        CSVReader parser = new CSVReader(cf.getInput("csv"));
        final String sourceColumn = cf.getParameter("sourceColumn");
        int sourceIndex;
        if (sourceColumn.isEmpty()) sourceIndex = 0;
        else {
            sourceIndex = parser.getColumnIndex(sourceColumn, false);
            if (sourceIndex < 0) {
                cf.writeError("sourceColumn is not found in csv: "+sourceColumn);
                return ErrorCode.PARAMETER_ERROR;
            }
        }
        
        final boolean unique = "true".equals(cf.getParameter("unique"));
        boolean ok = readParameters(cf);
        if (!ok) return ErrorCode.PARAMETER_ERROR;
        
        String[] outColumns = parser.getHeader().toArray(new String[] {});
        final String targetColumn = cf.getParameter("targetColumn");
        if (!targetColumn.isEmpty()) outColumns[sourceIndex] = targetColumn;
        
        CSVWriter writer = new CSVWriter(outColumns, cf.getOutput("csv"), false);
        if (unique) {
            convertUnique(parser, writer, sourceIndex);
        } else {
            convertNonUnique(parser, writer, sourceIndex);
        }
        parser.close();
        writer.close();
        
        return ErrorCode.OK;
    }
    
    private boolean readParameters(CommandFile cf) {
        this.split = cf.getBooleanParameter("split");
        this.splitConverted = cf.getBooleanParameter("splitConverted");
        this.originalWhenMissing = cf.getBooleanParameter("originalWhenMissing");
        this.dropMissing = cf.getBooleanParameter("dropMissing");
        this.varMeasure = cf.getParameter("varMeasure");
        this.varThreshold = cf.getDoubleParameter("varThreshold");
        
        String method = cf.getParameter("collapseString");
        if (method.equals("first")) {
            this.collapseString = CollapseString.FIRST;
        } else if (method.equals("majority")) {
            this.collapseString = CollapseString.MAJORITY;
        } else if (method.equals("consensus")) {
            this.collapseString = CollapseString.CONSENSUS;
        } else if (method.equals("longest")) {
            this.collapseString = CollapseString.LONGEST;
        } else if (method.equals("shortest")) {
            this.collapseString = CollapseString.SHORTEST;
        } else {
            cf.writeError("Invalid collapseString parameter: "+method);
            return false;
        }
        
        method = cf.getParameter("collapseNumeric");
        if (method.equals("median")) {
            this.collapseNumeric = CollapseNumeric.MEDIAN;
        } else if (method.equals("mean")) {
            this.collapseNumeric = CollapseNumeric.MEAN;
        } else if (method.equals("max")) {
            this.collapseNumeric = CollapseNumeric.MAX;
        } else if (method.equals("min")) {
            this.collapseNumeric = CollapseNumeric.MIN;
        } else if (method.equals("first")) {
            this.collapseNumeric = CollapseNumeric.FIRST;
        } else if (method.equals("majority")) {
            this.collapseNumeric = CollapseNumeric.MAJORITY;
        } else if (method.equals("indicator")) {
            this.collapseNumeric = CollapseNumeric.INDICATOR;
        } else if (method.equals("consensus")) {
            this.collapseNumeric = CollapseNumeric.CONSENSUS;
        } else if (method.equals("sum")) {
            this.collapseNumeric = CollapseNumeric.SUM;
        } else {
            cf.writeError("Invalid collapseNumeric parameter: "+method);
            return false;
        }
        
        return true;
    }
    
    private ErrorCode loadConvMap() throws IOException {
        CSVReader convParser = new CSVReader(cf.getInput("conversionTable"));
        
        final String keyColumn = cf.getParameter("keyColumn");
        final int keyIndex = keyColumn.isEmpty() ? 0
                : convParser.getColumnIndex(keyColumn, false);
        if (keyIndex < 0) {
            cf.writeError("keyColumn is not found in conversionTable: "+keyColumn);
            return ErrorCode.PARAMETER_ERROR;
        }
        
        final String conversionColumn = cf.getParameter("conversionColumn");
        final int valueIndex = conversionColumn.isEmpty() ? 0
                : convParser.getColumnIndex(conversionColumn, false);
        if (valueIndex < 0) {
            cf.writeError("conversionColumn is not found in conversionTable: "+conversionColumn);
            return ErrorCode.PARAMETER_ERROR;
        }
        
        for (List<String>row: convParser) {
            final String key = row.get(keyIndex);
            final String value = row.get(valueIndex);
            this.convMap.put(key, value);
        }
        convParser.close();
        
        return ErrorCode.OK;
    }
    
    private String convertValue(String value) {
        if (value == null) return null;
        if (this.split) {
            StringBuffer sb = new StringBuffer();
            for (String token: value.split(",")) {
                token = token.trim();
                if (token.isEmpty()) continue;
                String converted = convMap.get(token);
                if (converted == null) converted = NA_STRING;
                if (sb.length() > 0) sb.append(",");
                sb.append(converted);
            }
            return sb.toString();
        } else {
            String converted = convMap.get(value);
            if (converted == null && originalWhenMissing) return value;
            else return converted;
        }
    }
    
    private void convertNonUnique(CSVReader parser, CSVWriter writer, int sourceIndex) throws IOException {
        for (List<String> row: parser) {
            row.set(sourceIndex, convertValue(row.get(sourceIndex)));
            if(this.splitConverted && row.get(sourceIndex) != null) {
                 for (String id: row.get(sourceIndex).split(",")) {
                     id = id.trim();
                     if (id.isEmpty()) continue;
                     row.set(sourceIndex, id);
                     for (String s: row) writer.write(s, false);
                 }
            } else if (this.dropMissing && row.get(sourceIndex) == null) {
                // Do nothing;
            } else {
                for (String s: row) writer.write(s, false);
            }
        }
    }

    private void convertUnique(CSVReader parser, CSVWriter writer, int sourceIndex) throws IOException {
        final List<List<String>> rows = new ArrayList<List<String>>();
        final List<String> keys = new ArrayList<String>();
        final Map<String, ArrayIntList> duplicates = new HashMap<String, ArrayIntList>();
        
        int index = 0;
        for (List<String> row: parser) {
            String key = convertValue(row.get(sourceIndex));
            if (this.splitConverted && key != null) {
                for (String token: key.split(",")) {
                    token = token.trim();
                    if (token.isEmpty()) continue;
                    row = new ArrayList<String>(row);
                    row.set(sourceIndex, token);
                    ArrayIntList indices = duplicates.get(token);
                    if (indices == null) {
                        indices = new ArrayIntList();
                        duplicates.put(token, indices);
                        keys.add(token);
                    }
                    indices.add(index);
                    rows.add(row);
                    index++;
                }
            } else if (this.dropMissing && key == null){
                // Do nothing.
            } else {
                row = new ArrayList<String>(row);
                row.set(sourceIndex, key);
                ArrayIntList indices = duplicates.get(key);
                if (indices == null) {
                    indices = new ArrayIntList();
                    duplicates.put(key, indices);
                    keys.add(key);
                }
                indices.add(index);
                rows.add(row);
                index++;
            }
        }
        
        for (String key: keys) {
            ArrayIntList indices = duplicates.get(key);
            final int destIndex = indices.get(0);
            if (indices.size() > 1) {
                final int N_COLS = rows.get(destIndex).size();
                for (int col=0; col<N_COLS; col++) {
                    if (col != sourceIndex) {
                        String value = collapseColumn(rows, indices, col, key);
                        rows.get(destIndex).set(col, value);
                    }
                }
            }
            for (String s: rows.get(destIndex)) writer.write(s, false);
        }
    }
    
    private String collapseColumn(List<List<String>> rows, ArrayIntList indices, int col, String key) throws IOException{
        String consensusValue = null;
        Double dConsensusValue = null;
        boolean isNumeric = true;
        boolean equalStrings = true;
        boolean equalNumeric = true;
        List<String> strings = new ArrayList<String>();
        List<Double> numeric = new ArrayList<Double>();
        
        for (int i=0; i<indices.size(); i++) {
            int row = indices.get(i);
            final String value = rows.get(row).get(col);
            if (value != null && equalStrings) {
                if (consensusValue == null) consensusValue = value;
                else if (!value.equals(consensusValue)) {
                    consensusValue = null;
                    equalStrings = false;
                }
            }
            
            if (value != null) {
                strings.add(value);
                try {
                    double dvalue = Double.parseDouble(value);
                    numeric.add(dvalue);
                    if ( equalNumeric ) {
                        if (dConsensusValue == null) dConsensusValue = dvalue;
                        else if (dvalue!=dConsensusValue) {
                            dConsensusValue = null;
                            equalNumeric = false;
                        }
                    }
                } catch (NumberFormatException e) {
                    isNumeric = false;
                }
            }
        }
        
        if (equalStrings && !isNumeric) {
            return consensusValue;
        } else if (equalNumeric && isNumeric && this.collapseNumeric == CollapseNumeric.CONSENSUS) {
            if (dConsensusValue == null) return "NA";
            return ""+dConsensusValue;
        } else if (isNumeric) {
            return collapseNumeric(strings, numeric, col, key);
        } else {
            return collapseString(strings, col, key);
        }
    }
    
    private String collapseNumeric(List<String> strings, List<Double> numeric, int col, String key) {
        if (!this.varMeasure.isEmpty()) {
            if (this.varMeasure.equals("MAD")) {
                if (mad(numeric) > this.varThreshold) {
                    return null;
                }
            } else if(this.varMeasure.equals("variance")) {
                if (variance(numeric) > this.varThreshold) {
                    return null;
                }
            } else {
                throw new IllegalArgumentException("Invalid varMeasure parameter: "+this.varMeasure);
            }
        }
        
        switch(this.collapseNumeric) {
        case CONSENSUS:
            // Consensus has been handled earlier, if consensus is possible.
            String msg = String.format("Can not collapse duplicate values using 'consensus' method for ID %s, column %d: ",
                    key, col+1) + strings.toString();
            throw new IllegalArgumentException(msg);
        case FIRST:
            if (numeric.isEmpty()) return null;
            else return ""+numeric.get(0);
        case INDICATOR:
            if (numeric.isEmpty()) return null;
            Collections.sort(numeric);
            final double min = numeric.get(0);
            final double max = numeric.get(numeric.size()-1);
            final double THRESHOLD = 0.5;
            final boolean hasNeg = min < -THRESHOLD;
            final boolean hasPos = max > THRESHOLD;

            if (hasNeg && !hasPos) return "-1";
            else if (hasPos && !hasNeg) return "1";
            else if (!hasNeg && !hasPos) return "0";
            else return null;
        case MAJORITY:
            return majority(strings);
        case MAX:
            if (numeric.isEmpty()) return null;
            return ""+Collections.max(numeric);
        case MEAN:
            double meanVal = mean(numeric);
            if (Double.isNaN(meanVal)) return null;
            else return ""+meanVal;
        case SUM:
            double sumVal = sum(numeric);
            if (Double.isNaN(sumVal)) return null;
            else return ""+sumVal;
        case MEDIAN:
            double med = median(numeric);
            if (Double.isNaN(med)) return null;
            else return ""+med;
        case MIN:
            if (numeric.isEmpty()) return null;
            return ""+Collections.min(numeric);
        default:
            throw new IllegalArgumentException("Internal error: collapseNumeric not set");
        }
    }

    private String collapseString(List<String> strings, int col, String key) {
        switch(this.collapseString) {
        case CONSENSUS:
            // Consensus has been handled earlier, if consensus is possible.
            String msg = String.format("Can not collapse duplicate values using 'consensus' method for ID %s, column %d: ",
                    key, col+1) + strings.toString();
            throw new IllegalArgumentException(msg);
        case FIRST:
            if (strings.isEmpty()) return null;
            else return strings.get(0);
        case LONGEST:
            if (strings.isEmpty()) return null;
            else return Collections.max(strings, new LengthComparator());
        case MAJORITY:
            return majority(strings);
        case SHORTEST:
            if (strings.isEmpty()) return null;
            else return Collections.min(strings, new LengthComparator());
        default:
            throw new IllegalArgumentException("Internal error: collapseString not set");
        }
    }
    
    private static String majority(List<String> values) {
        class MajorityEntry implements Comparable<MajorityEntry> {
            public String value;
            public int freq;
            public int pos;
            
            public MajorityEntry(String value, int pos) {
                this.value = value;
                this.freq = 0;
                this.pos = pos;
            }
            
            public int compareTo(MajorityEntry other) {
                if (this.value.equals(other.value)) return 0;
                else if (this.freq < other.freq) return -1;
                else if (this.freq > other.freq) return 1;
                else if (this.pos < other.pos) return 1;
                else if (this.pos > other.pos) return -1;
                else return 0;
            }
        }
        
        HashMap<String,MajorityEntry> freqMap = new HashMap<String,MajorityEntry>();
        for (int i=0; i<values.size(); i++) {
            final String value = values.get(i);
            if (value == null) continue;
            MajorityEntry entry = freqMap.get(value);
            if (entry == null) {
                entry = new MajorityEntry(value, i);
                freqMap.put(value, entry);
            }
            entry.freq++;
        }
        
        if (freqMap.isEmpty()) return null;
        ArrayList<MajorityEntry> entries = new ArrayList<MajorityEntry>(freqMap.values());
        Collections.sort(entries);
        return entries.get(entries.size()-1).value;
    }
    
    private static double median(List<Double> values) {
        if (values.isEmpty()) return Double.NaN;
        ArrayDoubleList nonNullValues = new ArrayDoubleList();
        for(Double value: values) {
            if(value != null) {
                nonNullValues.add(value);
            }
        }
        return new Median().evaluate(nonNullValues.toArray());
    }
    
    private static double mean(List<Double> values) {
        if (values.isEmpty()) return Double.NaN;
        double sum = 0;
        int n = 0;
        for(Double value: values) {
            if(value != null) {
                sum += value;
                n += 1;
            }
        }
        return sum / n;
    }
    
    private static double sum(List<Double> values) {
        if (values.isEmpty()) return Double.NaN;
        double sum = 0;
        for(Double value: values) {
            if(value != null) {
                sum += value;
            }
        }
        return sum;
    }

    /** Calculates the Median Absolute Deviation (MAD) of the sample. Null values are ignored.
     *  
     *  @param values A list of values.
     *  @return The sample median absolute deviation. If the value list is empty then Double.NaN.
     */
    private static double mad(List<Double> values) {
        if(values.isEmpty()) return Double.NaN;
        double median = median(values);
        List<Double> deviations = new ArrayList<Double>();
        for(Double value: values) {
            if(value != null) {
                deviations.add(Math.abs(value - median));
            }
        }
        return median(deviations);
    }
    
    /** Calculates the sample variance. Null values are ignored.
     * 
     *  @param values A list of values.
     *  @return The sample variance.
     */
    private static double variance(List<Double> values) {
        if(values.isEmpty()) return Double.NaN;
        double mean = mean(values);
        double sumOfSquares = 0;
        int n = 0;
        for(Double value: values) {
            if(value != null) {
                sumOfSquares += Math.pow((value - mean), 2);
                n += 1;
            }
        }
        return sumOfSquares / (n - 1);
    }

    public static void main(String[] args) {
        new IDConvert().run(args);
    }

}
