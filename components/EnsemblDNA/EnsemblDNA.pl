#!usr/bin/perl

use strict;
use componentSkeleton;
use Encode::Unicode;
use EnsemblAPI;

sub toFastaSeq {
  my ( $seq ) = @_;
  
  my $fastaSeq = "";
  my $len = length($seq);
  my $i;
  if ($len % 60 == 0){
     for ($i=0;$i<$len-60;$i=$i+60){
        $fastaSeq = $fastaSeq.substr($seq,$i,60)."\n";
     }
     $fastaSeq = $fastaSeq.substr($seq,$i,60)."\n";
  }else{
     my $n = ($len-$len%60)/60+1;
     for ($i=0;$i<($n-1);$i++){
         $fastaSeq = $fastaSeq.substr($seq,$i*60,60)."\n";
     }
     $fastaSeq = $fastaSeq.substr($seq,$i*60,$len-($i-1)*60)."\n";
  }
  return $fastaSeq;
}

sub execute {
    my ($cf_ref) = @_;
    print "Reading database connection ...\n";
    my $db_dir = get_input($cf_ref,"connection");
    open(FILE_DB,$db_dir);
    my @lines = <FILE_DB>;
    my @tmp1;
    my $line;
    my %db_info;
    foreach $line (@lines){
        chomp($line);
        @tmp1 = split(/ *= */,$line);
        $db_info{$tmp1[0]} = $tmp1[1];
    }
    close(FILE_DB);
    
    print "Fetching sequences\n";
    my $region_dir;
    my @tmp2;

    $region_dir = get_input($cf_ref,"regions");

    open(FILE_REG,$region_dir);
    @tmp2 = <FILE_REG>;
    close(FILE_REG);

    my $times = @tmp2;
    my @tmp3;
    chomp($tmp2[0]);
    $tmp2[0]=~s/"//g;
    my @hash_keys = split(/\t/,$tmp2[0]);
    my $n = @hash_keys;
    my $j;

    my $output_dir = get_output($cf_ref,"sequences");
    open(FILE_OUTPUT,">$output_dir");

    my $csvFormat = get_parameter($cf_ref,"csvOutput");
    if($csvFormat eq "true"){
        print FILE_OUTPUT "ID\tsequence\n";
    }
	
    for (my $i = 1; $i < $times; $i++) {
        my $region_info;
        my @tmp3;
        my %hash_region;
        
        chomp($tmp2[$i]);
	$tmp2[$i]=~s/"//g;
        @tmp3 = split(/\t/,$tmp2[$i]);
        
        for ($j = 0; $j < $n; $j++) {
            $hash_region{$hash_keys[$j]} = $tmp3[$j];
        }        
        
	my $masked = get_parameter($cf_ref,"mask");

        if ($masked eq "false") {
            $masked = 0;
        } else {
            $masked = 1;
        }
        my $ensembl = new EnsemblAPI(1, %db_info);

	my $offset = get_parameter($cf_ref,"offset");
	my $length = get_parameter($cf_ref,"length");
 
        if($length < 0) { 
            print "Parameter length cannot be negative: ", $length, "\n";
            exit;
        }

        my $id     = $hash_region{$hash_keys[0]};
        my $strand = $hash_region{$hash_keys[1]};
        my $chr    = $hash_region{$hash_keys[2]};
	my $start;
	my $end;
  
	if($strand == 1){
		$start  = $hash_region{$hash_keys[3]} + $offset;
       		if ($length != 0) {
                $end = $start + $length;
            }else {
           	    $end = $hash_region{$hash_keys[4]};
       		}
	}elsif($strand == -1){
		$end = $hash_region{$hash_keys[4]} - $offset;
		if($length != 0){
			$start = $end - $length;
        }else{
			$start = $hash_region{$hash_keys[3]};
		}
	}
        my $seq = $ensembl->getSequence($chr, $start, $end, $strand, $masked);
        if($csvFormat eq "false"){
            my $fastaSeq = toFastaSeq($seq);
            print FILE_OUTPUT ">$id\n".$fastaSeq;
        }else {
            print FILE_OUTPUT "$id\t".$seq."\n";
        }
    }
    close(FILE_OUTPUT);
}

if(scalar @ARGV == 0){
    print "NO_COMMAND_FILE";
    exit;
}
my %cf = parse_command_file($ARGV[0]);
execute(\%cf);
