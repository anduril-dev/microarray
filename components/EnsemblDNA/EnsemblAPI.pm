
package EnsemblAPI;

use strict;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Utils::Sequence qw(reverse_comp);

sub new {
  my ($class, $extended, %dbname) = @_;
  #print "$class\t$extended\t$dbname\n";
  my $db = Bio::EnsEMBL::DBSQL::DBAdaptor->new(
    -user   => $dbname{'user'},
    -dbname => $dbname{'database'},
    -driver => $dbname{'driver'},
    -port   => $dbname{'port'},
    -host   => $dbname{'host'}
  );

  my $self = {
    _dbAdaptor  => $db,
    _prevLength => 55,
    _extended   => $extended
  };

  bless $self, $class;
  return $self;
}

sub toFastaSeq {
  my ( $seq ) = @_;
  
  my $fastaSeq = "";
  my $len = length($seq);
  my $i;
  if ($len % 60 == 0){
     for ($i=0;$i<$len-60;$i=$i+60){
	$fastaSeq = $fastaSeq.substr($seq,$i,60)."\n";
     }
     $fastaSeq = $fastaSeq.substr($seq,$i,60)."\n";
  }else{
     my $n = ($len-$len%60)/60+1;
     for ($i=0;$i<($n-1);$i++){
         $fastaSeq = $fastaSeq.substr($seq,$i*60,60)."\n";
     }
     $fastaSeq = $fastaSeq.substr($seq,$i*60,$len-($i-1)*60)."\n";
  }
  return $fastaSeq;
}

sub getGenes() {
  my ( $self ) = @_;

  my $adapt = $self->{_dbAdaptor}->get_GeneAdaptor();
  my @genes = @{$adapt->fetch_all_by_biotype('protein_coding')};
  #my @genes = @{$adapt->fetch_all_by_external_name('hCG_2026038')};

  return \@genes;
}

sub getSequence() {
  my ( $self, $chr, $start, $end, $strand, $masked ) = @_;

  my $adapt = $self->{_dbAdaptor}->get_SliceAdaptor();

  my $slice = $adapt->fetch_by_region('chromosome',$chr, $start, $end, $strand);
  if ($masked) {
     $slice = $slice->get_repeatmasked_seq();
  }
  if (!$slice) {
    # Chromosome was not found => return sequence of N's
    my $length = abs($end - $start) + 1;
    return "N" x $length;
  } else {
    return $slice->seq();
  }
}

sub getProcessTranscript {
  my ( $self, $tra, $minL, $maxL, $set ) = @_;

  my @exons = @{$tra->get_all_translateable_Exons()};
  my $exo = @exons[$#exons];
  if ($exo->phase() eq undef()) {
     return $set;
  }
  my $pha  = $exo->phase(); if ($pha < 0) { $pha = 0; }
  my $safe = abs($exo->cdna_coding_end($tra)-
                 $exo->cdna_coding_start($tra));
  if ($#exons > 0) {
     $safe += $self->{_prevLength};
  }

  my @seq = split(//, $tra->translateable_seq());
  my $len = $#seq;
  my $utr = $tra->three_prime_utr();
  if ($utr) {
     @seq = (@seq, split(//, $utr->seq()));
  }
  $safe   = $len-$safe;

  my $sub;
  my $i;
  my $k = 0;
  my $c = @seq[0];
  for ($i=0; $i<$len; $i++) {
      if (@seq[$i] eq $c) {
         $k++;
      } else {
         if ($k >= $minL) {
            $set = processRepeat($tra,  $safe, $self->{_extended},
                                 $maxL, $i-$k, $k,
                                 $len,  $set,  \@seq);
         }
         $c = @seq[$i];
         $k = 1;
      }
  }
  if ($k >= $minL) {
     $set = processRepeat($tra,  $safe, $self->{_extended},
                          $maxL, $i-$k, $k,
                          $len,  $set,  \@seq);
  }

  return $set;
}

sub processRepeat() {
  my ( $tra,  $safe,  $extended,
       $maxL, $start, $k,
       $end,  $set,   $seq ) = @_;
  my %hits = %{$set};

  my $gStart = int(($start)/3);
  my $gEnd   = int(($start+$k)/3);
  my @coList = $tra->pep2genomic($gStart, $gEnd);

  my $sLoc;
  if (@coList[0]->isa("Bio::EnsEMBL::Mapper::Coordinate")) {
     $sLoc = @coList[0];
     $gEnd = 0;
  } else {
     $sLoc = @coList[1];
     $gEnd = @coList[0]->length();
  }
  if ($sLoc->strand() == 1) {
     $gStart = $sLoc->start()+($start%3)+3-$gEnd;
     $gEnd   = $sLoc->end()  +(($start+$k)%3)+$gEnd;
  } else {
     $gStart = $sLoc->end()  -(($start)%3)-3+$gEnd;
     $gEnd   = $sLoc->start()-(($start+$k)%3)-$gEnd;
  }
  my $key = ($gStart)."-".($gEnd);

  if ($k > $maxL) {
     #print("* Too long hit at ",$key," (",$tra->stable_id,").\n");
     return \%hits;
  }

  my $stopO = nextStopCodon($start, 3, $seq);
  my $stopD = nextStopCodon($start, 1, $seq);
  my $stopI = nextStopCodon($start, 2, $seq);
  my $last  = @$seq-3;

  if ((($extended != 1) && ($stopD >= $safe)) ||
      ($stopD >= $end+2)                      ||
      ($stopD == undef())) {
     my $hit = join("", @$seq[$start .. $start+$k-1]);
     $hits{$key} = [$tra->stable_id(),
                    $hit,
                    ($stopO==undef())?">".($last-$safe).",>".($last-$end+2):
                                          ($stopO-$safe).",".($stopO-$end+2),
                    ($stopD==undef())?">".($last-$safe).",>".($last-$end+2):
                                          ($stopD-$safe).",".($stopD-$end+2),
                    ($stopI==undef())?">".($last-$safe).",>".($last-$end+2):
                                          ($stopI-$safe).",".($stopI-$end+2)];
  }
  return \%hits;
}

sub nextStopCodon() {
  my ( $start, $offset, $sequence ) = @_;
  my @seq = @$sequence;

  my $i = $start % 3;
  $i = ($i == 3) ? $start : $start-$i;
  $i += $offset;
  for (; $i < @seq; $i+=3) {
    if ((@seq[$i] eq "T") && (
          ((@seq[$i+1] eq "A") && ((@seq[$i+2] eq "A") ||
                                   (@seq[$i+2] eq "G"))) ||
          ((@seq[$i+1] eq "G") &&  (@seq[$i+2] eq "A")))) {
       return $i;
    }
  }

  return undef();
}



1;
