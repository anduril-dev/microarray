from anduril.args import *
from anduril.args import cf
from anduril.converters import tojson as j
import os

headerfile='head_flotr2.htmls'
if plotType=='m':
    headerfile='head_protovis.htmls'

header=open(headerfile,'rU').read()
for param in cf.get_parameter_list():
    header=header.replace('@'+param.upper()+'@',cf.get_parameter(param))

outfile=open(plot,'w')
outfile.write(header)
outfile.write('d=')
j.from_csv(data,output=outfile,columnsFieldName="columns",dataFieldName="data",format="plot")
outfile.write(';\n')
if plotType=='p':
    templatefile='foot_scatter.htmls'
if plotType=='l':
    templatefile='foot_line.htmls'
if plotType=='h':
    templatefile='foot_bar.htmls'
if plotType=='m':
    templatefile='foot_matrixdiagrams.htmls'

if template!=None:
    templatefile=template
footer=open(templatefile,'rU').read()
for param in cf.get_parameter_list():
    footer=footer.replace('@'+param.upper()+'@',cf.get_parameter(param))

outfile.write(footer)
outfile.close()
