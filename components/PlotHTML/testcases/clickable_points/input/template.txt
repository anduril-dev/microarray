

// store datapoint in variable
var activevalue;
var colors=@COLORS@;
// Draw the graph
  graph = Flotr.draw(
    container, [ 
      { data : d.data, 
        color : colors[0],
        points : { show : true },
        lines : { show : false},
        label : "@TITLE@",
      },
    ],
    {
        title : "@TITLE@",
        mouse : {
            track : true,
            relative : true,
            trackDecimals:4,
            trackFormatter: function(x) { activevalue=x.index; return d.data[x.index][2]; },
        },
        yaxis: { 
            title: d.columns[1],
            min: @YLOW@,
            max: @YHIGH@,
        },
        xaxis: { 
            title: d.columns[0],
            min: @XLOW@,
            max: @XHIGH@,
        },
    }
  );
  Flotr.EventAdapter.observe(container, 'flotr:click', function(position){
    window.location=encodeURIComponent(d.data[activevalue][2]);
  });

})(document.getElementById("chart"));
</script> 
</body></html>
