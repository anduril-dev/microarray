source "$ANDURIL_HOME/bash/functions.sh"
export_command

iscmd convert || exit 1
iscmd compare || exit 1
iscmd bc || exit 1

[ "$parameter_width" -gt 0 ] && resizeforce="!" || resizeforce=""
if [ "$parameter_width" = "0" ] 
then resizestring=""
else declare -a resizestring
     resizestring="-geometry ${parameter_width}x${parameter_width}${resizeforce}"
fi

tempdir=$( gettempdir )

convert ${resizestring} "${input_expected}[0]" "${tempdir}/expected.miff" 2>&1 >> "$logfile" 
convert ${resizestring} "${input_image}[0]" "${tempdir}/image.miff" 2>&1 >> "$logfile" 
convert ${resizestring} "${input_expected}[0]" "${tempdir}/expected.png" 2>&1 >> "$logfile" 
convert ${resizestring} "${input_image}[0]" "${tempdir}/image.png" 2>&1 >> "$logfile" 

correlation=$( compare -verbose -metric NCC "${tempdir}/expected.miff" \
 "${tempdir}/image.miff" null: 2>&1 | grep all: | sed 's@.*: \([0-9\.]\+\).*@\1@' )

greater=$( echo $correlation'>'$parameter_threshold | bc -l  )

if [ "$greater" = "0" ]
then writelog "Expecting / Got: file://${input_expected} file://${input_image}"
     writelog "Image correlation $correlation lesser than limit $parameter_threshold" 
     writelog "Images not similar"
else
    writelog "Images similar"
fi

# reverse exit code, if supposed to fail on similar
if [ "$parameter_failOnSimilar" = "true" ]
then exitvalue=$greater
else exitvalue=$(( 1 - $greater ))
fi

exit $exitvalue


