library(componentSkeleton)

execute <- function(cf) {

    algorithm <- get.parameter(cf, 'algorithm', type='string')
    out.dir   <- get.output(cf, 'plot')
    dir.create(out.dir, recursive=TRUE)
    if (algorithm == 'pint'){
        a <- pint(cf)
        return(a)
    } else if (algorithm == 'DRI'){
        a <- DRI(cf)
        return(a)
    } else if (algorithm == 'edira'){
        a <- ediraFUN(cf)
        return(a)
    } else if (algorithm == 'SIM'){
        a <- SIM(cf)
        return(a)
    } else if (algorithm == 'integrOmics'){
        a <- integrOmics(cf)
        return(a)
    } else if (algorithm == 'iCluster'){
        a <- iClust(cf)
        return(a)
    } else if (algorithm == 'SODEGIR'){
        a <- SODEGIR(cf)
        return(a)
    } else {
        write.error(cf, c("Value ",algorithm," for parameter algorithm is not defined"))
        return(INVALID_INPUT)
    }
    return(TRUE)
}

SIM <- function(cf){
    library(SIM)

    # Inputs
    cnaInput  <- LogMatrix.read(get.input(cf, 'cnaMatrix'))
    exprInput <- LogMatrix.read(get.input(cf, 'exprMatrix'))
    annot     <- AnnotationTable.read(get.input(cf, 'exprAnnotation'))

    # Parameters
    loc.col   <- get.parameter(cf, 'locusCol', type='string')

    # Input checks
    indVec    <- order(as.numeric(annot[,2]), as.numeric(annot[,loc.col]))
    annot     <- annot[indVec,]
    cnaInput  <- cnaInput[indVec,]
    exprInput <- exprInput[indVec,]

    stopifnot(rownames(cnaInput) == rownames(exprInput))
    stopifnot(colnames(cnaInput) == colnames(exprInput))
    stopifnot(rownames(cnaInput) == annot[,1])

    # Prepare input
    cnaInput  <- as.data.frame(cbind(annot[,1], annot[,2], annot[,loc.col], cnaInput))
    exprInput <- as.data.frame(cbind(annot[,1], annot[,2], annot[,loc.col], exprInput))

    colnames(cnaInput)[1:3]  <- c(colnames(annot)[c(1,2)], loc.col)
    colnames(exprInput)[1:3] <- c(colnames(annot)[c(1,2)], loc.col)

    cnaInput[,3:ncol(cnaInput)]   <- as.numeric(unlist(cnaInput[,3:ncol(cnaInput)]))
    exprInput[,3:ncol(exprInput)] <- as.numeric(unlist(exprInput[,3:ncol(exprInput)]))

    wrkdir <- tempdir()

    assemble.data(dep.data   = cnaInput,
                  indep.data = exprInput,
                  dep.ann    = c(colnames(annot)[c(1,2)], loc.col),
                  indep.ann  = c(colnames(annot)[c(1,2)], loc.col),
                  dep.id     = colnames(annot)[1], 
                  dep.chr    = colnames(annot)[2],
                  dep.pos    = loc.col,
                  dep.symb   = colnames(annot)[1],  
                  indep.id   = colnames(annot)[1],
                  indep.chr  = colnames(annot)[2], 
                  indep.pos  = loc.col, 
                  indep.symb = colnames(annot)[1], 
                  overwrite  = TRUE,
                  run.name = wrkdir)
    integrated.analysis(samples       = colnames(exprInput)[4:ncol(exprInput)],
                        input.regions = unique(annot[,2]), 
                        zscores       = TRUE, 
                        method        = "window",
                        run.name      = wrkdir)
    table.dep <- tabulate.top.dep.features(input.regions = unique(annot[,2]),
                                           adjust.method = "BY", # parameterize
                                           method        = "window", # parameterize
                                           significance  = 0.05, # parameterize
                                           run.name      = wrkdir)
    output <- 0
    for (i in 1:length(unique(annot[,2]))){
        if (i == 1){
            output <- table.dep[[i]]
        } else {
            output <- rbind(output, table.dep[[i]])
        }
    }
    output <- as.data.frame(output[,c(4,5,3,7,8,2,1)])
    colnames(output)[1] <- colnames(annot)[1]

    CSV.write(get.output(cf, 'concomitantGenes'), output)
    CSV.write(get.output(cf, 'concomitantGenes2'), output)
    return(TRUE)
}

integrOmics <- function(cf){
    library(mixOmics)

    # Inputs
    cnaInput  <- LogMatrix.read(get.input(cf, 'cnaMatrix'))
    exprInput <- LogMatrix.read(get.input(cf, 'exprMatrix'))
    annot     <- AnnotationTable.read(get.input(cf, 'exprAnnotation'))

    # Parameters
    loc.col   <- get.parameter(cf, 'locusCol', type='string')

    # Input checks
    indVec    <- order(as.numeric(annot[,2]), as.numeric(annot[,loc.col]))
    annot     <- annot[indVec,]
    cnaInput  <- cnaInput[indVec,]
    exprInput <- exprInput[indVec,]

    stopifnot(rownames(cnaInput) == rownames(exprInput))
    stopifnot(colnames(cnaInput) == colnames(exprInput))
    stopifnot(rownames(cnaInput) == annot[,1])

    # Analysis
    results <- spls(X = t(cnaInput), Y = t(exprInput), #Predict expression from copy number
                    keepX    = c(200, 200, 200),
                    ncomp    = 3,
                    mode     = "regression",
                    max.iter = 500, tol = 1e-06)

    output <- data.frame(rownames(results$loadings$X),results$loadings$X, results$loadings$Y)
    colnames(output)[1] <- colnames(annot)[1]
    pvalues <- 0
    cor <- 0
    for (i in 1:nrow(output)){
        corTest <- cor.test(as.numeric(output[i,2:4]), as.numeric(output[i,5:7]), alternative="greater")
        pvalues[i] <- corTest$p.value
        cor[i]     <- corTest$estimate
    }
    output <- cbind(output, pvalues, cor)
    colnames(output)[8:9] <- c("pvalue","pcc") # Check

    CSV.write(get.output(cf, 'concomitantGenes'), output)
    CSV.write(get.output(cf, 'concomitantGenes2'), output)
    return(TRUE)
}

iClust <- function(cf){
    library(iCluster)
    library(gplots)

    cnaInput  <- LogMatrix.read(get.input(cf, 'cnaMatrix'))
    exprInput <- LogMatrix.read(get.input(cf, 'exprMatrix'))
    annot     <- AnnotationTable.read(get.input(cf, 'exprAnnotation'))
    k         <- get.parameter(cf, 'iClusterK', type='int')
    loc.col   <- get.parameter(cf, 'locusCol', type='string')
    do.opt    <- get.parameter(cf, 'iClusterOpt', type='boolean')

    # Input checks
    indVec    <- order(as.numeric(annot[,2]), as.numeric(annot[,loc.col]))
    annot     <- annot[indVec,]
    cnaInput  <- cnaInput[indVec,]
    exprInput <- exprInput[indVec,]

    stopifnot(rownames(cnaInput) == rownames(exprInput))
    stopifnot(colnames(cnaInput) == colnames(exprInput))
    stopifnot(rownames(cnaInput) == annot[,1])

    input <- list(t(exprInput), t(cnaInput))

    # Choosing optimal k over small parameter space
    if (do.opt){
        if (k < 4) k <- 4
        opt.k <- k
        best.pod <- 1
        for (i in (k-2):(k+2)){
            fit <- iCluster(input,
                            k      = i,
                            lambda = c(0.2,0.2))
            curr.pod <- compute.pod(fit)
            if (curr.pod < best.pod){
                opt.k <- i
                best.pod <- curr.pod
            }
            write.log(cf, paste('Proportion of deviance (POD) score for k=', i, 'is', round(curr.pod, 5)))
        }
    } else {
        opt.k <- k
    }
    # Recalculate with optimal k and output
    fit <- iCluster(input,
                    k      = opt.k,
                    lambda = c(0.2,0.2))
    curr.pod <- compute.pod(fit)
    write.log(cf, paste('Proportion of deviance (POD) score for k=', opt.k, 'is', round(curr.pod, 5)))

    output <- cbind(fit$W[1:ncol(input[[1]]),], fit$W[(ncol(input[[1]])+1):nrow(fit$W),])
    rownames(output) <- colnames(input[[1]])
    # Plots
    out.dir  <- get.output(cf, 'plot')
    docName  <- file.path(out.dir, LATEX.DOCUMENT.FILE)

    # Separability
    fig.name <- sprintf('%s-iCluster-separability.png', get.metadata(cf, 'instanceName'))
    fig.path <- file.path(out.dir, fig.name)
    png(fig.path, height=1200, width=1200, res=150)
    plotiCluster(fit=fit, label=rownames(input[[1]]))
    invisible(dev.off())
    cat( latex.figure(fig.name, quote.capt = FALSE, image.width=12),
         "\\clearpage",
          sep="\n", file=docName, append=TRUE)
    # Expression heatmap
    heat.colors <- rev(redgreen(255))
    expMat <- input[[1]]
    expMat <- expMat[order(fit$clusters, decreasing=T), which(rowSums(output) != 0)]
    fig.name2 <- sprintf('%s-iCluster-exprHeatmap.png', get.metadata(cf, 'instanceName'))
    fig.path2 <- file.path(out.dir, fig.name2)
    png(fig.path2, height=1200, width=1200, res=150)
    heatmap.2(t(expMat), dendrogram="none",
              Colv  = F, Rowv = F, 
              col   = heat.colors,
              trace="none", density.info = "none")
    invisible(dev.off())
    nro.genes <- length(which(rowSums(output) != 0))
    capt <- paste("Heatmap of input expression values. Samples are ordered according to cluster separability.",
                  "Only the", nro.genes, "genes, which contribute to the clustering, have been plotted.")
    cat( latex.figure(fig.name2, caption = capt, quote.capt = FALSE, image.width=14),
          sep="\n", file=docName, append=TRUE)

    # CNA heatmap
    expMat <- input[[2]]
    expMat <- expMat[order(fit$clusters, decreasing=T),which(rowSums(output) != 0)]
    fig.name3 <- sprintf('%s-iCluster-cnaHeatmap.png', get.metadata(cf, 'instanceName'))
    fig.path3 <- file.path(out.dir, fig.name3)
    png(fig.path3, height=1200, width=1200, res=150)
    heatmap.2(t(expMat), dendrogram="none",
              Colv  = F, Rowv = F, 
              col   = heat.colors,
              trace="none", density.info = "none")
    invisible(dev.off())
    capt <- paste("Heatmap of input copy number values. Samples are ordered according to cluster separability.",
                  "Only the", nro.genes, "genes, which contribute to the clustering, have been plotted.")
    cat( latex.figure(fig.name3, caption = capt, quote.capt = FALSE, image.width=14),
         "\\clearpage",
          sep="\n", file=docName, append=TRUE)

    CSV.write(get.output(cf, 'concomitantGenes'), output)
    CSV.write(get.output(cf, 'concomitantGenes2'), output[which(rowSums(output) != 0),])
    return(TRUE)
}

SODEGIR <- function(cf){
    library(PREDA)

    # Inputs
    cnaInput  <- LogMatrix.read(get.input(cf, 'cnaMatrix'))
    exprInput <- LogMatrix.read(get.input(cf, 'exprMatrix'))
    annot     <- AnnotationTable.read(get.input(cf, 'exprAnnotation'))

    # Parameters
    nro.perm  <- get.parameter(cf, 'perm',     type='int')
    fdrLimit  <- get.parameter(cf, 'fdrLimit', type='float')
    loc.col   <- get.parameter(cf, 'locusCol', type='string')
    end.col   <- get.parameter(cf, 'endCol',   type='string')
    ss.thresh <- get.parameter(cf, 'SGRthold', type='float')

    # Input checks
    indVec    <- order(as.numeric(annot[,2]), as.numeric(annot[,loc.col]))
    annot     <- annot[indVec,]
    cnaInput  <- cnaInput[indVec,]
    exprInput <- exprInput[indVec,]

    stopifnot(rownames(cnaInput) == rownames(exprInput))
    stopifnot(colnames(cnaInput) == colnames(exprInput))
    stopifnot(rownames(cnaInput) == annot[,1])

    cnaInput  <- cbind(rownames(cnaInput), cnaInput)
    exprInput <- cbind(rownames(exprInput), exprInput)
    annot     <- cbind(annot, rep(1, nrow(annot)))
    colnames(annot)[5] <- "strand" # Parameterize

    colnames(cnaInput)[1] <- colnames(annot)[1]
    colnames(exprInput)[1] <- colnames(annot)[1]

    # Analysis
    GEStatisticsForPREDA <- StatisticsForPREDAFromdataframe(exprInput,
                                                            ids_column = 1,
                                                            testedTail="both")
    CNStatisticsForPREDA <- StatisticsForPREDAFromdataframe(cnaInput,
                                                            ids_column = 1,
                                                            testedTail="both")
    GenomicAnnot <- GenomicAnnotationsFromdataframe(annot,
                                                     chromosomesNumbers = unique(annot[,2]),
                                                     chromosomesLabels = unique(annot[,2]),
                                                     ids_column = 1,
                                                     chr_column = 2,
                                                     start_column = loc.col,
                                                     end_column = end.col,
                                                     strand_column = "strand",
                                                     PlusStrandString = "1")
    AnnotForPREDA <- GenomicAnnotations2GenomicAnnotationsForPREDA(GenomicAnnot, reference_position_type="median")
    SODEGIRGEDataForPREDA <- MergeStatisticAnnotations2DataForPREDA(GEStatisticsForPREDA,
                                                                    AnnotForPREDA,
                                                                    sortAndCleanNA = TRUE,
                                                                    quiet = TRUE, 
                                                                    MedianCenter = FALSE)
    SODEGIRCNDataForPREDA <- MergeStatisticAnnotations2DataForPREDA(CNStatisticsForPREDA,
                                                                    AnnotForPREDA,
                                                                    sortAndCleanNA = TRUE,
                                                                    quiet = TRUE,
                                                                    MedianCenter = FALSE)
    SODEGIRGEanalysisResults <- PREDA_main(SODEGIRGEDataForPREDA, nperms = nro.perm, verbose = FALSE, parallelComputations = FALSE) 
                                           #outputGenomicAnnotationsForPREDA = SODEGIRGEDataForPREDA,

    SODEGIRCNanalysisResults <- PREDA_main(SODEGIRCNDataForPREDA, 
                                           outputGenomicAnnotationsForPREDA = SODEGIRGEDataForPREDA,
                                           nperms = nro.perm, parallelComputations = FALSE,
                                           verbose = FALSE)

    pValues <- PREDAResults2dataframe(SODEGIRCNanalysisResults)
    pValues <- pValues[,c(1,2,3,4,6,which(substr(colnames(pValues), start=1, stop=6) == "pvalue"))]
    sNames  <- unlist(strsplit(colnames(pValues), split="\\."))
    colnames(pValues)[6:ncol(pValues)] <- sNames[seq(from=7, to=length(sNames), by=2)]

    # Integration
    SODEGIR_CN_GAIN <- PREDAResults2GenomicRegions(SODEGIRCNanalysisResults,
                                                   qval.threshold = fdrLimit,
                                                   smoothStatistic.tail = "upper",
                                                   smoothStatistic.threshold = ss.thresh) # Parameterize
    SODEGIR_CN_LOSS <- PREDAResults2GenomicRegions(SODEGIRCNanalysisResults,
                                                   qval.threshold = fdrLimit,
                                                   smoothStatistic.tail = "lower",
                                                   smoothStatistic.threshold = -1*ss.thresh) # Parameterize
    SODEGIR_GE_UP <- PREDAResults2GenomicRegions(SODEGIRGEanalysisResults,
                                                 qval.threshold = fdrLimit,
                                                 smoothStatistic.tail = "upper",
                                                 smoothStatistic.threshold = ss.thresh)
    SODEGIR_GE_DOWN <- PREDAResults2GenomicRegions(SODEGIRGEanalysisResults,
                                                   qval.threshold = fdrLimit,
                                                   smoothStatistic.tail = "lower",
                                                   smoothStatistic.threshold = -1*ss.thresh)

    # Find results
    sup <- list()
    sdown <- list()
    sgain <- list()
    sloss <- list()
    col1 <- 1
    col2 <- 1
    for (i in 1:(ncol(exprInput)-1)){
        if (!is.null(SODEGIR_GE_UP[[i]]) && !is.null(SODEGIR_CN_GAIN[[i]])){
            sup[[col1]]   <- SODEGIR_GE_UP[[i]]
            sgain[[col1]] <- SODEGIR_CN_GAIN[[i]]
            col1 <- col1+1
        }
        if (!is.null(SODEGIR_GE_DOWN[[i]]) && !is.null(SODEGIR_CN_LOSS[[i]])){
            sdown[[col2]] <- SODEGIR_GE_DOWN[[i]]
            sloss[[col2]] <- SODEGIR_CN_LOSS[[i]]
            col2 <- col2+1
        }
    }
    SODEGIR_GE_UP <- sup
    SODEGIR_GE_DOWN <- sdown
    SODEGIR_CN_GAIN <- sgain
    SODEGIR_CN_LOSS <- sloss

    outputDEL <- 0
    outputAMP <- 0
    # Output for dels
    if (length(SODEGIR_GE_DOWN) > 1 && length(SODEGIR_CN_LOSS) > 1){
        SODEGIR_DELETED        <- GenomicRegionsFindOverlap(SODEGIR_GE_DOWN, SODEGIR_CN_LOSS)
        names(SODEGIR_DELETED) <- names(SODEGIR_GE_DOWN)
        SDGsignature_deleted   <- try( computeDatasetSignature(SODEGIRGEDataForPREDA, genomicRegionsList = SODEGIR_DELETED, signature_qval_threshold = fdrLimit), silent=TRUE )
        if (class(SDGsignature_deleted) == "try-error"){
            outputDEL <- data.frame(ids=NA)
            write.log(cf, "No SODEGIR signature for deletions.")
        } else if (all(is.null(SDGsignature_deleted[[1]]))){
            outputDEL <- data.frame(ids=NA)
        } else {
            sigDel <- GenomicRegions2dataframe(SDGsignature_deleted[[1]])
            for (i in 1:nrow(sigDel)){
                if (i == 1){
                    outputDEL <- pValues[intersect(which(pValues[,2] == sigDel[i,1]), 
                                         intersect(which(pValues[,5] >= sigDel[i,2]), which(pValues[,5] <= sigDel[i,3]))),]
                } else {
                    outputDEL <- rbind(outputDEL, pValues[intersect(which(pValues[,2] == sigDel[i,1]), 
                                               intersect(which(pValues[,5] >= sigDel[i,2]), which(pValues[,5] <= sigDel[i,3]))),])
                }
            }
        }
    } else {
        outputDEL <- data.frame(ids=NA)
    }

    # Outputs for amps
    if (length(SODEGIR_GE_UP) > 1 && length(SODEGIR_CN_GAIN) > 1){
        SODEGIR_AMPLIFIED        <- GenomicRegionsFindOverlap(SODEGIR_GE_UP, SODEGIR_CN_GAIN)
        names(SODEGIR_AMPLIFIED) <- names(SODEGIR_GE_UP)
        SDGsignature_amplified   <- try( computeDatasetSignature(SODEGIRGEDataForPREDA, genomicRegionsList = SODEGIR_AMPLIFIED, signature_qval_threshold = fdrLimit), silent=TRUE )
        if (class(SDGsignature_amplified) == "try-error"){
            outputAMP <- data.frame(ids=NA)
            write.log(cf, "No SODEGIR signature for amplifications.")
        } else if (all(is.null(SDGsignature_amplified[[1]]))){
            outputAMP <- data.frame(ids=NA)
        } else {
            sigAmp <- GenomicRegions2dataframe(SDGsignature_amplified[[1]])
            for (i in 1:nrow(sigAmp)){
                if (i == 1){
                    outputAMP <- pValues[intersect(which(pValues[,2] == sigAmp[i,1]), 
                                         intersect(which(pValues[,5] >= sigAmp[i,2]), which(pValues[,5] <= sigAmp[i,3]))),]
                } else {
                    outputAMP <- rbind(outputAMP, pValues[intersect(which(pValues[,2] == sigAmp[i,1]), 
                                               intersect(which(pValues[,5] >= sigAmp[i,2]), which(pValues[,5] <= sigAmp[i,3]))),])
                }
            }
        }
    } else {
        outputAMP <- data.frame(ids=NA)
    }

    CSV.write(get.output(cf, 'concomitantGenes'),  outputAMP)
    CSV.write(get.output(cf, 'concomitantGenes2'), outputDEL)

    return(TRUE)
}

ediraFUN <- function(cf){
    library(edira)

    # INPUTs
    cnaInput     <- as.data.frame(LogMatrix.read(get.input(cf, 'cnaMatrix')))
    cnaRefInput  <- as.data.frame(LogMatrix.read(get.input(cf, 'cnaRefMatrix')))
    exprInput    <- as.data.frame(LogMatrix.read(get.input(cf, 'exprMatrix')))
    exprRefInput <- as.data.frame(LogMatrix.read(get.input(cf, 'exprRefMatrix')))
    annot        <- as.data.frame(AnnotationTable.read(get.input(cf, 'exprAnnotation')))

    # Parameters
    doSegment    <- get.parameter(cf, 'ediraSegment', type='boolean')
    max.seg      <- get.parameter(cf, 'ediraMaxseg' , type='int')
    loc.col      <- get.parameter(cf, 'locusCol',     type='string')

    # Input checks
    cnaInput     <- cnaInput[order(annot[,2], annot[,loc.col]),]
    exprInput    <- exprInput[order(annot[,2], annot[,loc.col]),]
    cnaRefInput  <- cnaRefInput[order(annot[,2], annot[,loc.col]),]
    exprRefInput <- exprRefInput[order(annot[,2], annot[,loc.col]),]
    annot        <- annot[order(annot[,2], annot[,loc.col]),]

    stopifnot(rownames(cnaInput)  == rownames(exprInput))
    stopifnot(colnames(cnaInput)  == colnames(exprInput))
    stopifnot(rownames(cnaInput)  == rownames(cnaRefInput))
    stopifnot(rownames(exprInput) == rownames(exprRefInput))
    stopifnot(rownames(cnaInput)  == annot[,1])

    # Prepare input
    cnaInput      <- list(data=cnaInput,     info=data.frame(chromosome = annot[,2], position   = annot[,loc.col]))
    exprInput     <- list(data=exprInput,    info=data.frame(chromosome = annot[,2], position   = annot[,loc.col]))
    cnaRefInput   <- list(data=cnaRefInput,  info=data.frame(chromosome = annot[,2], position   = annot[,loc.col]))
    exprRefInput  <- list(data=exprRefInput, info=data.frame(chromosome = annot[,2], position   = annot[,loc.col]))

    chromosomes <- unique(annot[,2])

    if (doSegment){
        cnaInput    <- segmentCN(cnaInput,    exprInput,    chrs = chromosomes, maxseg = max.seg)
        cnaRefInput <- segmentCN(cnaRefInput, exprRefInput, chrs = chromosomes, maxseg = max.seg)
        cnaRefInput  <- cnaRefInput+10^-6 # Circumvent bug when H0=true
        exprRefInput <- exprRefInput+10^-6 # Circumvent bug when H0=true
    }

    # Algorithm
    results <- edira(exprInput, #A
                     cnaInput,  #B
                     exprRefInput, #A_ref
                     cnaRefInput)  #B_ref

    # Write output
    output     <- matrix(0, ncol=4, nrow=length(results$test))
    output[,1] <- annot[,1]
    output[,2] <- results$eccc
    output[,3] <- results$eccc_var
    output[,4] <- results$test
    colnames(output) <- c("gene","eccc","ecccVar","p-value")

    CSV.write(get.output(cf, 'concomitantGenes'), output)
    CSV.write(get.output(cf, 'concomitantGenes2'), output)

    return(TRUE)
}

pint <- function(cf){
    library(pint)

    # INPUTs
    cnaInput  <- Matrix.read(get.input(cf, 'cnaMatrix'))
    exprInput <- LogMatrix.read(get.input(cf, 'exprMatrix'))
    annot     <- AnnotationTable.read(get.input(cf, 'exprAnnotation'))

    # Parameters
    windowSize <- get.parameter(cf, 'windowSize', type='int')
    method     <- get.parameter(cf, 'pintMethod', type='string')
    arm        <- get.parameter(cf, 'pintArm',    type='string')
    scoreLimit <- get.parameter(cf, 'scoreLimit', type='float')
    loc.col    <- get.parameter(cf, 'locusCol',   type="string")

    # Input checks
    indVec    <- order(as.numeric(annot[,2]), as.numeric(annot[,loc.col]))
    annot     <- annot[indVec,]
    cnaInput  <- cnaInput[indVec,]
    exprInput <- exprInput[indVec,]

    stopifnot(rownames(cnaInput) == rownames(exprInput))
    stopifnot(colnames(cnaInput) == colnames(exprInput))
    stopifnot(rownames(cnaInput) == annot[,1])

    # Create algorithm input
    rownames(annot) <- annot[,1]
    cnaMatrix   <- pint.data(cnaInput,  annot)
    exprMatrix  <- pint.data(exprInput, cnaMatrix[[2]])
    chromosomes <- unique(annot[,2])

    # Prepare output
    results <- matrix(,ncol=2,nrow=0)

    for (chr in 1:length(chromosomes)){
        # Algorithm
        if (arm %in% c("p","q")){
            model <- screen.cgh.mrna(exprMatrix,
                                     cnaMatrix,
                                     windowSize = windowSize,
                                     method = method,
                                     arm = arm,
                                     outputType = "data.frame",
                                     chr = chromosomes[chr],
                                     params = list(mySeed = 6084))
        } else {
            model <- screen.cgh.mrna(exprMatrix,
                                     cnaMatrix,
                                     windowSize = windowSize,
                                     method = method,
                                     outputType = "data.frame",
                                     chr = chromosomes[chr],
                                     params = list(mySeed = 6084))
        }
        tmpResults <- model[,c('geneName','dependencyScore')]
        results <- rbind(results, tmpResults[which(tmpResults[,2] >= scoreLimit),])
        results <- results[order(results[,2], decreasing = TRUE),]
    }
    colnames(results) <- c("gene","score")
    # Write output
    CSV.write(get.output(cf, 'concomitantGenes'),  results)
    CSV.write(get.output(cf, 'concomitantGenes2'), results)

    return(TRUE)
}

DRI <- function(cf){
    library(DRI)

    # INPUTs
    cnaInput  <- Matrix.read(get.input(cf, 'cnaMatrix'))
    exprInput <- LogMatrix.read(get.input(cf, 'exprMatrix'))
    annot     <- AnnotationTable.read(get.input(cf, 'exprAnnotation'))

    # Parameters
    method        <- get.parameter(cf, 'DRImethod',     type='string')
    fdrLimit      <- get.parameter(cf, 'fdrLimit',      type='float')
    transformType <- get.parameter(cf, 'transformType', type='string')
    DRIperm       <- get.parameter(cf, 'perm',          type='int')

    exprInput <- dri.impute(exprInput)

    if (method != "SAM"){
        obs  <- drcorrelate(cnaInput, exprInput, method=method)
        # Generate null distribution for FDR calculation
        null <- drcorrelate.null(cnaInput, exprInput, method=method, perm=DRIperm)
        # Identify the correlation cutoff corresponding to user set FDR
        n.cutoff <- dri.fdrCutoff(obs, null, targetFDR=fdrLimit, bt=TRUE)
        cutoff   <- n.cutoff[2]
        # Retrieve all genes that are significant at the determined cutoff, and
        # calculate gene-specific FDRs
        results  <- dri.sig_genes(cutoff, obs, null, annot[,1], annot[,1], annot[,2], annot[,4], bt=TRUE, method="drcorrelate")

        output <- matrix(0, ncol=3, nrow=nrow(results$positive))
        output[,1] <- results$positive[,3]
        output[,2] <- results$positive[,7]
        output[,3] <- results$positive[,8]
        colnames(output) <- c("gene","score","fdr")
    } else {
        labels <- as.numeric(unlist(CSV.read(get.input(cf, 'labels'))))
        obs    <- drsam(cnaInput, exprInput, labels, transform.type=transformType)
        # Generate null distribution for FDR calculation
        null   <- drsam.null(cnaInput, exprInput, labels, transform.type=transformType, k=DRIperm)
        # Identify the correlation cutoff corresponding to user set FDR
        n.cutoff <- dri.fdrCutoff(obs$test.summed, null, targetFDR=fdrLimit, bt=TRUE)
        cutoff   <- n.cutoff[2]
        # Retrieve all genes that are significant at the determined cutoff, and
        # calculate gene-specific FDRs
        results  <- dri.sig_genes(cutoff, obs, null, annot[,1], annot[,1], annot[,2], annot[,4], bt=TRUE, method="drsam")

        output <- matrix(0, ncol=3, nrow=nrow(results$positive))
        output[,1] <- results$positive[,3]
        output[,2] <- results$positive[,9]
        output[,3] <- results$positive[,10]
        colnames(output) <- c("gene","score","fdr")
    }

    # Write output
    CSV.write(get.output(cf, 'concomitantGenes'), as.data.frame(output))
    CSV.write(get.output(cf, 'concomitantGenes2'), as.data.frame(output))

    return(TRUE)
}

main(execute)
