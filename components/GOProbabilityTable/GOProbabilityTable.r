library(componentSkeleton)
library(csbl.go)

execute <- function(cf) {
    ent <- entities.from.text(get.input(cf, 'goAnnotation'))
    
    # Similarity table
    prob.table <- entities.to.prob.table(ent)
    write.prob.table(prob.table, get.output(cf, 'similarityTable'))
    
    # Enrichment table
    prob.table <- entities.to.enrichment.probs(ent)
    write.prob.table(prob.table, get.output(cf, 'enrichmentTable'))
    
    return(0)
}

main(execute)
