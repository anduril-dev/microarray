library(componentSkeleton)

execute <- function(cf) {
    n.columns <- get.parameter(cf, 'columns', 'int')
    n.rows <- get.parameter(cf, 'rows', 'int')
    col.name.pattern <- get.parameter(cf, 'columnNamePattern')
    row.name.pattern <- get.parameter(cf, 'rowNamePattern')
    first.cell <- get.parameter(cf, 'firstColumn')
    
    distribution <- get.parameter(cf, 'distribution')
    d.mean <- get.parameter(cf, 'mean', 'float')
    d.sd <- get.parameter(cf, 'sd', 'float')
    d.min <- get.parameter(cf, 'min', 'float')
    d.max <- get.parameter(cf, 'max', 'float')
    
    if (distribution == 'exponential' && d.mean == 0) {
        write.error(cf, 'For exponential distribution, mean must be non-zero; it is the inverse of rate')
        return(PARAMETER_ERROR)
    }
    
    n <- n.columns * n.rows
    matr <- matrix(switch(distribution,
            uniform=runif(n, min=d.min, max=d.max),
            normal=rnorm(n, mean=d.mean, sd=d.sd),
            exponential=rexp(n, 1/d.mean),
            poisson=rpois(n, lambda=d.mean)
        ), nrow=n.rows, ncol=n.columns)
        
    colnames(matr) <- sprintf(col.name.pattern, 1:ncol(matr))
    rownames(matr) <- sprintf(row.name.pattern, 1:nrow(matr))
    CSV.write(get.output(cf, 'matrix'), matr, first.cell=first.cell)
    return(0)
}

main(execute)
