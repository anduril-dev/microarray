import edu.uci.ics.jung.graph.Graph;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.javatools.graphMetrics.ConvertDirected2Undirected;
import fi.helsinki.ltdk.csbl.javatools.graphMetrics.GraphML;


public class Directed2Undirected extends SkeletonComponent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	Directed2Undirected().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		GraphML reader = new GraphML(cf.getInput("graph"));
		reader.loadGraph();
		Graph<String,String> directed = reader.getGraph();
		Graph<String,String>	underlying=ConvertDirected2Undirected.convert(directed);
		reader.saveGraph(underlying, cf.getOutput("graph"));
		
		return ErrorCode.OK;
	}
	
	

}
