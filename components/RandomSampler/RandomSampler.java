import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomDataImpl;
import org.apache.commons.math3.random.RandomGenerator;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class RandomSampler extends SkeletonComponent {
    private final RandomGenerator random;
    
    private int headerRows;
    
    private int headerColumns;
    private double numColumns;
    private boolean columnFraction;
    private boolean shuffleColumns;
    
    public RandomSampler() {
        this.random = new MersenneTwister();
    }
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        this.headerRows = cf.getIntParameter("headerRows");
        final double numRows = cf.getDoubleParameter("numRows");
        final boolean rowFraction = cf.getBooleanParameter("rowFraction");
        
        this.headerColumns = cf.getIntParameter("headerColumns");
        this.numColumns = cf.getDoubleParameter("numColumns");
        this.columnFraction = cf.getBooleanParameter("columnFraction");
        this.shuffleColumns = cf.getBooleanParameter("shuffleColumns");
        
        final boolean isCSV = this.headerColumns != 0 || this.numColumns != 1 || !this.columnFraction || this.shuffleColumns;

        File input = cf.getInput("input");
        final int lines = countLines(input);
        
        final int sampleLines;
        if (rowFraction) {
            if (numRows > 1) {
                cf.writeError("Fractional row count must be <= 1");
                return ErrorCode.PARAMETER_ERROR;
            }
            sampleLines = (int)Math.round((lines - this.headerRows) * numRows);
        } else {
            if (Math.abs(Math.rint(numRows) - numRows) > 1e-5) {
                cf.writeError("Row count must be an integer when fractional count is not used");
                return ErrorCode.PARAMETER_ERROR;
            }
            sampleLines = (int)numRows;
        }
        
        if (sampleLines < 0 || sampleLines > lines-this.headerRows) {
            String msg = String.format(
                    "numRows out of range: %d rows selected, %d rows available",
                    sampleLines, lines-this.headerRows);
            cf.writeError(msg);
            return ErrorCode.PARAMETER_ERROR;
        }
        
        ErrorCode status;
        if (isCSV) status = sampleCSV(cf, input, lines, sampleLines);
        else status = sampleTextFile(cf, input, lines, sampleLines);
        
        return status;
    }
    
    private int countLines(File file) throws IOException {
        int lines = 0;
        BufferedReader reader = new BufferedReader(new FileReader(file));
        try {
            while (reader.readLine() != null) {
                lines++;
            }
        } finally {
            reader.close();
        }
        
        return lines;
    }
    
    private ErrorCode sampleCSV(CommandFile cf, File input, int fileLines, int sampleLines) throws IOException {
        CSVParser parser = new CSVParser(input);
        
        if (this.headerRows != 1) {
            cf.writeError("headerRows must be 1 with CSV file input");
            return ErrorCode.PARAMETER_ERROR;
        }
        
        final int sampleColumns;
        if (this.columnFraction) {
            if (this.numColumns > 1) {
                cf.writeError("Fractional header count must be <= 1");
                return ErrorCode.PARAMETER_ERROR;
            }
            sampleColumns = (int)Math.round((parser.getColumnCount() - this.headerColumns) * this.numColumns); 
        } else {
            if (Math.abs(Math.rint(this.numColumns) - this.numColumns) > 1e-5) {
                cf.writeError("Column count must be an integer when fractional count is not used");
                return ErrorCode.PARAMETER_ERROR;
            }
            sampleColumns = (int)this.numColumns;
        }

        if (sampleColumns < 0 || sampleColumns > parser.getColumnCount()-this.headerColumns) {
            String msg = String.format(
                    "numColumns out of range: %d columns selected, %d columns available",
                    sampleColumns, parser.getColumnCount()-this.headerColumns);
            cf.writeError(msg);
            return ErrorCode.PARAMETER_ERROR;
        }
        
        final String[] selectedColumnNames = new String[sampleColumns+this.headerColumns];
        final int[] selectedColumns = selectColumns(parser.getColumnNames(), sampleColumns, selectedColumnNames);
        
        CSVWriter writer = new CSVWriter(selectedColumnNames, cf.getOutput("sample"));
        
        int remainingLines = fileLines - this.headerRows;
        int remainingSampleLines = sampleLines;
        
        try {
            for (String[] row: parser) {
                final double rnd = this.random.nextDouble();
                if (rnd*remainingLines <= remainingSampleLines) {
                    for (int index: selectedColumns) {
                        writer.write(row[index]);
                    }
                    remainingSampleLines--;
                }
                remainingLines--;
            }
        } finally {
            parser.close();
            writer.close();
        }
        
        if (remainingSampleLines != 0) {
            cf.writeError("Internal error: remaining sample rows = "+remainingSampleLines);
            return ErrorCode.ERROR;
        }
        
        return ErrorCode.OK;
    }
    
    private int[] selectColumns(String[] allColumns, int numColumns, String[] outColumnNames) {
        final int totalColumns = numColumns + this.headerColumns;
        List<Integer> indices = new ArrayList<Integer>(allColumns.length);
        for (int i=this.headerColumns; i<allColumns.length; i++) indices.add(i);
        
        RandomDataImpl random = new RandomDataImpl(this.random);
        Object[] selectedIndices = random.nextSample(indices, numColumns);
        if (!this.shuffleColumns) {
            Arrays.sort(selectedIndices);
        }
        
        int[] resultIndices = new int[totalColumns];
        for (int i=0; i<this.headerColumns; i++) {
            resultIndices[i] = i;
        }
        for (int i=0; i<numColumns; i++) {
            final int index = (Integer)selectedIndices[i];
            resultIndices[this.headerColumns+i] = index;
        }
        for (int i=0; i<resultIndices.length; i++) {
            outColumnNames[i] = allColumns[resultIndices[i]];
        }
        
        return resultIndices;
    }
    
    private ErrorCode sampleTextFile(CommandFile cf, File input, int fileLines, int sampleLines) throws IOException {
        int curLine = 0;
        int remainingLines = fileLines - this.headerRows;
        int remainingSampleLines = sampleLines;
        
        BufferedReader reader = new BufferedReader(new FileReader(input));
        BufferedWriter writer = new BufferedWriter(new FileWriter(cf.getOutput("sample")));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                if (curLine < this.headerRows) {
                    writer.write(line);
                    writer.newLine();
                } else {
                    final double rnd = this.random.nextDouble();
                    if (rnd*remainingLines <= remainingSampleLines) {
                        writer.write(line);
                        writer.newLine();
                        remainingSampleLines--;
                    }
                    remainingLines--;
                }
                curLine++;
            }
        } finally {
            reader.close();
            writer.close();
        }
        
        if (remainingSampleLines != 0) {
            cf.writeError("Internal error: remaining sample rows = "+remainingSampleLines);
            return ErrorCode.ERROR;
        }
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new RandomSampler().run(args);
    }
}
