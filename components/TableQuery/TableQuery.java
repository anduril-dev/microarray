import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hsqldb.jdbcDriver;
import org.h2.Driver;

import fi.helsinki.ltdk.csbl.anduril.component.CSVReader;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.CSVReader.ColumnType;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class TableQuery extends SkeletonComponent {

    public static final String STATEMENT_DELIMITER = "--<statement break>--";
    public static final int MAX_INPUTS = 15;
    public static final int DEFAULT_INDICES = 1;

    private static final String SQL_TYPE_STRING = "LONGVARCHAR";

    private Map<String, Map<String, String>> typeMap;
    private int[] numIndices;
    private boolean insertTransaction = false;
    private String createTablePrefix = "";
    
    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        Connection conn = connect(cf);
        if (conn == null) return ErrorCode.ERROR;

        IndexFile arrayIn = cf.inputDefined("tables") ? cf.readInputArrayIndex("tables") : new IndexFile();
        this.numIndices = readNumIndices(cf, arrayIn);
        if (this.numIndices == null) return ErrorCode.PARAMETER_ERROR;
        
        this.typeMap = readTypes(cf);
        if (this.typeMap == null) return ErrorCode.INVALID_INPUT;
        
        /* Read input tables and insert them to the database */
        long startTime = System.nanoTime();
        for (int i=1; i<=MAX_INPUTS; i++) {
            final String inputPort = "table"+i;
            boolean ok = readTable(cf, inputPort, cf.getInput(inputPort), conn, numIndices[i-1]);
            if (!ok) return ErrorCode.INVALID_INPUT;
        }
        int i=MAX_INPUTS;
        for (String table : arrayIn) {
            boolean ok = readTable(cf, table, arrayIn.getFile(table), conn, numIndices[i++]);
            if (!ok) return ErrorCode.INVALID_INPUT;
        }
        final long readTableTime = System.nanoTime() - startTime;;
        
        /* Execute query and write result to file */
        startTime = System.nanoTime();
        boolean ok = execQuery(cf, "query", conn);
        final long queryTime = System.nanoTime() - startTime;
        if (!ok) return ErrorCode.ERROR;
        
        final long memoryMB = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024*1024);
        cf.writeLog(String.format("Timing: import %.1f s, query+write %.1f s; memory used %d MB",
                readTableTime/1e9, queryTime/1e9, memoryMB));
        
        return ErrorCode.OK;
    }
    
    /**
     * Create a temporary in-memory database and connect to it.
     * On error, write error message to error log and return null.
     */
    private Connection connect(CommandFile cf) {
        final String engine = cf.getParameter("engine");
        final boolean memoryDB = cf.getBooleanParameter("memoryDB");

        String dbURL;
        String dbUser = "sa";

        final File dbFile;
        if (memoryDB) {
            dbFile = null;
        } else {
            dbFile = new File(cf.getTempDir(), "database");
            dbFile.getParentFile().mkdirs();
        }
        
        if (engine.equals("hsqldb")) {
            try {
                jdbcDriver.class.newInstance();
            } catch (Exception e) {
                cf.writeError("Failed to load HSQLDB JDBC driver: "+e);
                return null;
            }
            dbURL = "jdbc:hsqldb:" + (memoryDB ? "mem:memdb" : "file:" + dbFile.getAbsolutePath());
        } else if (engine.equals("h2")) {
            try {
                org.h2.Driver.class.newInstance();
            } catch (Exception e) {
                cf.writeError("Failed to load H2 JDBC driver: "+e);
                return null;
            }
            dbURL = "jdbc:h2:" + (memoryDB ? "mem:" : "file:" + dbFile.getAbsolutePath());
        } else if (engine.equals("sqlite")) {
            try {
                Class.forName("org.sqlite.JDBC");
            } catch (Exception e) {
                cf.writeError("Failed to load SQLite JDBC driver: "+e);
                return null;
            }
            
            dbURL = "jdbc:sqlite:" + (memoryDB ? ":memory:" : dbFile.getAbsolutePath());
            dbUser = "";
            this.insertTransaction = true;
            this.createTablePrefix = "TEMPORARY";
        } else {
            cf.writeError("Invalid SQL engine: "+engine);
            return null;
        }
        
        Connection conn;
        try {
            conn = DriverManager.getConnection(dbURL, dbUser, "");
        } catch(SQLException e) {
            cf.writeError("Failed to start SQL server: "+e);
            return null;
        }
        
        return conn;
    }
    
    /**
     * Read the numIndices parameter. Return an array
     * whose length is always MAX_INPUTS. Default value
     * is taken from DEFAULT_INDICES. Return null on
     * failure.
     */
    private int[] readNumIndices(CommandFile cf, IndexFile array) {
        int[] num = new int[MAX_INPUTS+array.size()];
        Arrays.fill(num, DEFAULT_INDICES);

        String[] tokens = AsserUtil.split(cf.getParameter("numIndices"));
        for (int i=0; i<tokens.length; i++) {
            final String param = tokens[i];
            if (param.isEmpty()) continue;
            try {
                num[i] = Integer.parseInt(param);
            } catch (NumberFormatException e) {
                cf.writeError("Invalid numIndices item: "+param);
                return null;
            }
        }
        return num;
    }
    
    /**
     * Read the columnTypes input into a map. Return null on
     * failure.
     */
    private Map<String, Map<String, String>> readTypes(CommandFile cf) throws IOException {
        Map<String, Map<String, String>> typeMap = new HashMap<String, Map<String,String>>();
        File inputFile = cf.getInput("columnTypes");
        if (inputFile == null) return typeMap;

        CSVReader reader = new CSVReader(inputFile);
        try {
            Map<String, String> lineMap;
            while ((lineMap = reader.nextMap()) != null) {
                final String table = lineMap.get("Table");
                final String column = lineMap.get("Column");
                final String type = lineMap.get("Type");
                if (table == null) {
                    cf.writeError("columnTypes: Table is not defined");
                    return null;
                }
                if (column == null) {
                    cf.writeError("columnTypes: Column is not defined");
                    return null;
                }
                if (type == null) {
                    cf.writeError("columnTypes: Type is not defined");
                    return null;
                }
                
                Map<String, String> tableMap = typeMap.get(table);
                if (tableMap == null) {
                    tableMap = new HashMap<String, String>();
                    typeMap.put(table, tableMap);
                }
                tableMap.put(column, type);
            }
        } finally {
            reader.close();
        }
        
        return typeMap;
    }
    
    /**
     * Return an entry from columnTypes input, or null
     * if not present.
     * @param table Table ID
     * @param column Column name
     */
    private String getUserDefinedType(String table, String column) {
        Map<String, String> tableMap = this.typeMap.get(table);
        if (tableMap == null) return null;
        return tableMap.get(column);
    }
    
    /**
     * Return an SQL type definition.
     */
    private String getSQLType(ColumnType type) {
        switch (type) {
        case BOOLEAN:
            return "BOOLEAN";
        case INT:
            return "INTEGER";
        case FLOAT:
            return "DOUBLE";
        case STRING:
            return SQL_TYPE_STRING;
        }
        return null;
    }
    
    /**
     * Return a Java SQL type identifier.
     */
    private int getJavaSQLType(ColumnType type) {
        switch (type) {
        case BOOLEAN:
            return Types.BOOLEAN;
        case INT:
            return Types.INTEGER;
        case FLOAT:
            return Types.DOUBLE;
        case STRING:
            return Types.VARCHAR;
        }
        return Types.NULL;
    }
    
    /**
     * Read table from CSV file and insert the schema and row data
     * into database. Also create indices for the table.
     * @return True on success
     */
    private boolean readTable(CommandFile cf, String relation, File inputFile, Connection conn, int indices) throws IOException {
        if (inputFile == null) return true;
        
        System.out.println("Parsing "+inputFile);
        
        List<String> columnNames = new ArrayList<String>();
        List<ColumnType> columnTypes = new ArrayList<ColumnType>();
        boolean ok = createSchema(cf, relation, inputFile, conn, columnNames, columnTypes);
        if (!ok) return false;

        System.out.println("Inserting data into "+relation);

        try {
            insertData(cf, conn, relation, inputFile, columnTypes);
        } catch(SQLException e) {
            String msg = String.format("Table %s: error inserting rows into SQL table: %s",
                    relation, e);
            cf.writeError(msg);
            return false;
        }
        
        return createIndices(cf, relation, columnNames, conn, indices);
    }
    
    /**
     * Infer column types of CSV file and create a table schema,
     * also inserting it to the database.
     * @return True on success
     */
    private boolean createSchema(CommandFile cf, String tableName, File tableFile, Connection conn,
            List<String> columnNames, List<ColumnType> columnTypes) throws IOException {
        columnNames.clear();
        columnTypes.clear();
        CSVReader reader = new CSVReader(tableFile);
        try {
            columnNames.addAll(reader.getHeader());
            columnTypes.addAll(reader.inferColumnTypes());
        } finally {
            reader.close();
        }
        
        StringBuffer stmt = new StringBuffer();
        stmt.append(String.format("CREATE %s TABLE %s (", this.createTablePrefix, tableName));
        for (int col=0; col<columnNames.size(); col++) {
            String columnName = columnNames.get(col);
            if (columnName.isEmpty()) {
                columnName = "__COLUMN" + (col+1);
                cf.writeLog(String.format(
                        "%s: Warning: column name %d is empty: renaming to %s",
                        tableName, col+1, columnName));
            }
            
            String sqlType = getUserDefinedType(tableName, columnName);
            if (sqlType == null) {
                sqlType = getSQLType(columnTypes.get(col));
            }
            if (sqlType == null) {
                String msg = String.format("Table %s: could not detect type of column %s",
                        tableName, columnName);
                cf.writeError(msg);
                return false;
            }
            
            if (col > 0) stmt.append(",");
            stmt.append(String.format(" \"%s\" %s ", columnName, sqlType));
        }
        
        stmt.append(");");
        cf.writeLog(stmt.toString());
        
        try {
            Statement sqlStatement = conn.createStatement();
            sqlStatement.execute(stmt.toString());
        } catch(SQLException e) {
            String msg = String.format("Table %s: error creating SQL table: %s; SQL statement is %s",
                    tableName, e, stmt.toString());
            cf.writeError(msg);
            return false;
        }
        
        return true;
    }
    
    /**
     * Read rows from CSV file and insert them to the existing SQL table.
     */
    private void insertData(CommandFile cf, Connection conn, String tableName, File inputFile, List<ColumnType> columnTypes) throws SQLException, IOException {
        CSVReader reader = new CSVReader(inputFile);
        final List<String> header;
        
        try {
            header = reader.getHeader();
            final int NUM_COLUMNS = header.size();

            StringBuffer stmt = new StringBuffer();
            stmt.append(String.format("INSERT INTO %s VALUES (", tableName));
            for (int col=0; col<NUM_COLUMNS; col++) {
                if (col > 0) stmt.append(" , ");
                stmt.append(" ? ");
            }
            stmt.append("); ");
            PreparedStatement statement = conn.prepareStatement(stmt.toString());
            
            if (this.insertTransaction) {
                Statement sqlStatement = conn.createStatement();
                sqlStatement.execute("BEGIN;");
            }
            
            for (List<String> row: reader) {
                for (int col=0; col<NUM_COLUMNS; col++) {
                    final String value = row.get(col);
                    statement.setObject(col+1, value, getJavaSQLType(columnTypes.get(col)));
                }
                statement.execute();
            }
            
            if (this.insertTransaction) {
                Statement sqlStatement = conn.createStatement();
                sqlStatement.execute("COMMIT;");
            }
        } finally {
            reader.close();
        }
    }
    
    /**
     * Create indices for a table. There are N indices,
     * where N = keyColumn. All indices are single-column
     * indices. Columns from 1 to N are used. 
     * @return True on success
     */
    private boolean createIndices(CommandFile cf, String tableName, List<String> columnNames, Connection conn, int keyColumns) throws IOException {
        if (keyColumns < 1) return true;
        
        for (int col=0; col<keyColumns; col++) {
            String indexName = String.format("index_%s_%d", tableName, col+1);
            String stmt = String.format("CREATE INDEX %s ON %s (\"%s\")",
                    indexName, tableName, columnNames.get(col));
            cf.writeLog(stmt.toString());
            try {
                Statement sqlStatement = conn.createStatement();
                sqlStatement.execute(stmt.toString());
            } catch(SQLException e) {
                String msg = String.format("Table %s: error creating index %s: %s; SQL statement is %s",
                        tableName, indexName, e, stmt.toString());
                cf.writeError(msg);
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Execute SQL query and write results to CSV file.
     * @return True on success
     */
    private boolean execQuery(CommandFile cf, String queryPort, Connection conn) throws IOException {
        String queryStr;
        if (cf.inputDefined(queryPort)) {
            if (!cf.getParameter("query").isEmpty()) {
                cf.writeError("Both the input file and the parameter query were provided. Exactly one of them must be present.");
                return false;
            }
            
            try {
                queryStr = cf.readInput(queryPort);
            } catch(IOException e) {
                cf.writeError("Error reading query input");
                return false;
            }
        } else {
            queryStr = cf.getParameter("query");
            if (queryStr.isEmpty()) {
                cf.writeError("Neither the input file or the parameter query was provided");
                return false;
            }
        }
        
        System.out.println("Executing query");
        
        ResultSet resultSet;
        ResultSetMetaData metadata;
        
        try {
            String[] queries = AsserUtil.split(queryStr, STATEMENT_DELIMITER);
            for (int i=0; i < queries.length-1; i++) {
                if (conn.createStatement().execute(queries[i])) {
                   cf.writeLog("Warning: the intermediate results ignored for:\n"+queries[i]);
                }
            }
            Statement sqlStatement = conn.createStatement();
            resultSet = sqlStatement.executeQuery(queries[queries.length-1]);
            metadata = resultSet.getMetaData();
        } catch(SQLException e) {
            String msg = String.format("Error executing SQL query: %s", e);
            cf.writeError(msg);
            return false;
        }
        
        System.out.println("Processing query results");

        String[] columnNames = null;
        try {
            columnNames = new String[metadata.getColumnCount()];
            for (int col=0; col<metadata.getColumnCount(); col++) {
                columnNames[col] = metadata.getColumnLabel(col+1);
            }
        } catch(SQLException e) {
            cf.writeError("Error adding column names to result table: "+e);
            return false;
        }
        
        CSVWriter writer = new CSVWriter(columnNames, cf.getOutput("table"));
        try {
            while (resultSet.next()) {
                for (int i=0; i<metadata.getColumnCount(); i++) {
                    String value = resultSet.getString(i+1);
                    writer.write(value);
                }
            }
        } catch(SQLException e) {
            String msg = String.format("Error adding row data into result CSV table: %s", e);
            cf.writeError(msg);
            return false;
        } finally {
            writer.close();
        }
        
        return true;
    }
    
    public static void main(String[] args) {
        new TableQuery().run(args);
    }
}
