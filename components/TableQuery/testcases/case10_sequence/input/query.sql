CREATE SEQUENCE seqROW_id
AS INTEGER
START WITH 1 INCREMENT BY 1;
--<statement break>--
SELECT 'row'||NEXT VALUE FOR seqROW_id AS "id",
       "value"
FROM   table1
ORDER  BY 2
