import java.sql.Connection;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.GregorianCalendar;

/**
 * Utility functions (stored procedures) that can be used in
 * HSQLDB queries.
 */
public class Functions {

    /**
     * Generates a representation of the given integer in base 16.
     * If length is greater than zero then too short representations are
     * prefixed with zeros.
     * @param value  Integer to be represented as text
     * @param length Minimum length of the representation or -1 for any length
     */
    public static String toHexString(long value, int length) {
       String text = Long.toHexString(value).toUpperCase();
       if (length > 0) {
          int l = length-text.length();
          if (l > 0) {
             char[] padding = new char[l];
             Arrays.fill(padding, '0');
             text = new String(padding)+text;
          }
       }
       return text;
    }

    /**
     * Construct date from year, month and day.
     * @param year Year is restricted to range 1000 .. 9999 in
     *  this function as a sanity check. 
     * @param month From 1 to 12.
     * @param day From 1 to 31.
     * @return SQL date object.
     */
    public static Date date(int year, int month, int day) {
        if (year < 1000 || year > 9999) {
            throw new IllegalArgumentException("Year is out of range: "+year);
        }
        if (month < 1 || month > 12) {
            throw new IllegalArgumentException("Month is out of range: "+month);
        }
        if (day < 1 || day > 31) {
            throw new IllegalArgumentException("Day is out of range: "+day);
        }
        return new Date(new GregorianCalendar(year, month-1, day).getTimeInMillis());
    }
    
    /**
     * Parse date from a string.
     * @param format Date format pattern, as defined by SimpleDateFormat.
     *  Example: "dd.MM.yyyy".
     * @param date Date string in the format defined by the pattern.
     *  Example: "30.10.2009".
     * @return SQL date object.
     */
    public static Date parseDate(String format, String date) {
        if (format == null) {
            throw new IllegalArgumentException("format must not be NULL");
        }
        if (date == null) return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setLenient(false);
            return new Date(sdf.parse(date).getTime());
        } catch (ParseException e) {
            throw new IllegalArgumentException("Could not parse date: "+e);
        }
    }
    
    /**
     * Return true if str matches the regular expression pattern. Return
     * NULL if either pattern or str is NULL.
     */
    public static Boolean grep(String pattern, String str) {
        if (pattern == null || str == null) return null;
        return str.matches(pattern);
    }
}
