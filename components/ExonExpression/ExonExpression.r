library(componentSkeleton)

map.expr.exon.to.probeset <- function(exon, probeset.expr, sample.groups, type = "median", show.unmatched=FALSE, annotation){
	probeset <- meap.map.exon.to.probeset(annotation, exon)
	
	if(nrow(probeset) == 0){
		if(show.unmatched == TRUE){
			len <- length(sample.groups)
			exon.expr <- rep(-1,len)
			return(list(exon, exon.expr))	
		}else{
			return()
		}
	}else{	
		probeset <- as.character(probeset[,1])
		tmp <- data.frame()
		for(p in probeset){
			flag <- rownames(probeset.expr) == p
			tmptmp <- probeset.expr[flag,sample.groups]
			tmp <- rbind(tmp, tmptmp)
		}
		if(nrow(tmp) == 0){
			if(show.unmatched == TRUE){
                 	       len <- length(sample.groups)
                        	exon.expr <- rep(-1,len)
                        	return(list(exon, exon.expr))
               		 }else{
                        	return()
                	}
		}
		if(type=="medianpolish"){
			res <- medpolish(tmp)
			exon.expr <- res$overall + res$col
		}else{
			exon.expr <- apply(tmp, 2, type)
		}
		return(list(exon, exon.expr))
	}
}

execute <- function(cf) {

	library(meap)

	# Read input parameters from command file
	write.log(cf, "Reading input and parameters from command file ...")
	expr.path <- get.input(cf,'expr')
	des.path <- get.input(cf,'sample')
	exon.path <- get.input(cf,'exon')
	exon.column <- get.parameter(cf, 'ExonColumn')
	exon.column <- as.numeric(exon.column)
	combine.type <- get.parameter(cf,'CombineType')
	show.unmatched <- get.parameter(cf,'ShowUnmatched')
	show.unmatched <- as.logical(show.unmatched)
	annotation <- get.parameter(cf,'annotation')

	write.log(cf, "Calculating exon expression ...")
	expr <- LogMatrix.read(expr.path)
	obj <- SampleGroupTable.read(des.path)
        groups <- SampleGroupTable.get.groups(obj)
        group1 <- SampleGroupTable.get.source.groups(obj,groups[1])
        group2 <- SampleGroupTable.get.source.groups(obj,groups[2])
	exon <- CSV.read(exon.path)
	exon <- exon[,exon.column]

	write.log(cf, "Fetching expression data ...")
	
	sample.groups <- c(group1,group2)
	
	write.log(cf, "Converting data format ...")
	res <- lapply(exon, map.expr.exon.to.probeset, probeset.expr = expr, sample.groups = sample.groups, type = combine.type, show.unmatched = show.unmatched, annotation=annotation)
	out <- data.frame()	
	elist <- c()
	for(i in res){
		out <- rbind(out,i[[2]])
		elist <- c(elist,i[[1]])
	}
	out <- cbind(elist,out)
	colnames(out) <- c("ExonID",sample.groups)
	

	write.log(cf, "Output converted expression matrix ...")
	write.table(out,get.output(cf,'ExonExpr'),quote=FALSE,sep="\t",row.names = FALSE)
}

main(execute)
