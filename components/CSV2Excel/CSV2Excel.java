import fi.helsinki.ltdk.csbl.anduril.component.CSVReader;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.record.CFRuleRecord;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFConditionalFormatting;
import org.apache.poi.hssf.usermodel.HSSFConditionalFormattingRule;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFontFormatting;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFPatternFormatting;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFSheetConditionalFormatting;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;

public class CSV2Excel extends SkeletonComponent {
    public static final String DEFAULT_NUMBER_FORMAT = "0.00";
    public static final int MAX_SHEETS = 9;
    
    private List<File> csvFiles;
    private List<String> sheetNames;
    private Map<String,Reference> references;
    private boolean enableFormulas;

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        this.csvFiles = new ArrayList<File>();
        this.sheetNames = new ArrayList<String>();
        
        this.references = readReferences(cf);
        this.enableFormulas = cf.getParameter("enableFormulas").equals("true");
        
        System.setProperty("java.awt.headless", "true");
        
        HSSFWorkbook workbook = new HSSFWorkbook();
        
        String[] sheetNameArray = cf.getParameter("sheetNames").split(",");
        for (int sheet=1; sheet<=MAX_SHEETS; sheet++) {
            String inputName = (sheet==1) ? "csv" : "csv"+sheet;
            if (cf.inputDefined(inputName)) {
                String name = sheetNameArray.length < sheet || sheetNameArray[sheet-1].isEmpty() ?
                        null : sheetNameArray[sheet-1];
                this.sheetNames.add(name);
                this.csvFiles.add(cf.getInput(inputName));
            }
        }
        
        if (cf.inputDefined("array")) {
            IndexFile index = cf.readInputArrayIndex("array");
            for (int i=0; i<index.size(); i++) {
                String name = index.getKey(i);
                if (name.matches("[0-9]{1,2}")) name = null;
                this.sheetNames.add(name);
                this.csvFiles.add(index.getFile(i));
            }
        }
        
        for (int i=0; i<this.csvFiles.size(); i++) {
            writeSheet(cf, this.csvFiles.get(i), this.sheetNames.get(i), workbook, i+1);
        }
        
        FileOutputStream out = new FileOutputStream(cf.getOutput("excelFile"));
        try { workbook.write(out); }
        finally { out.close(); }
        
        return ErrorCode.OK;
    }
    
    private void writeSheet(CommandFile cf, File csvContent, String sheetName, HSSFWorkbook workbook, int sheetnum) throws IOException {
        CSVParser keys = new CSVParser(csvContent);
        List<Style> styles = readStyles(cf, keys, sheetnum);

        HSSFSheet sheet;
        if (sheetName == null || sheetName.isEmpty()) {
            sheet = workbook.createSheet();
        } else {
            sheet = workbook.createSheet(sheetName);
        }
        
        int rownum = 0;
        HSSFRow row = null;

        final HSSFDataFormat dataFormat = workbook.createDataFormat();
        final HSSFCellStyle floatStyle = workbook.createCellStyle();
        floatStyle.setDataFormat(dataFormat.getFormat(DEFAULT_NUMBER_FORMAT));
        
        // write header line
        row = sheet.createRow(rownum);
        rownum++;
        String[] fieldNames = keys.getColumnNames();
        for (int colnum = 0; colnum < fieldNames.length; colnum++) {
            String fieldName = fieldNames[colnum];
            HSSFCell cell = row.createCell(colnum);
            cell.setCellValue(new HSSFRichTextString(fieldName));
            
            for (Style style: styles) {
                if (style.matchesCell(rownum, colnum+1) && !style.isConditional()) {
                    cell.setCellStyle(style.getStyle(workbook));
                    break;
                }
            }
        }

        // write other lines
        while (keys.hasNext()) {
            row = sheet.createRow(rownum);
            rownum++;

            String[] ss = keys.next();
            for (int colnum = 0; colnum < ss.length; colnum++) {
                String cellValue = ss[colnum];
                HSSFCell cell = row.createCell(colnum);
                
                /* If value is numeric, write it is numeric,
                 * otherwise, as a string. */
                if (cellValue == null) {
                    cellValue = cf.getParameter("missingValue");
                }
                
                if (this.enableFormulas && cellValue.startsWith("=")) {
                    /* Strip leading "=" */
                    cell.setCellFormula(cellValue.substring(1));
                } else {
                    try {
                        double numValue = Double.parseDouble(cellValue);
                        cell.setCellValue(numValue);
                        if (Math.rint(numValue) != numValue) {
                            cell.setCellStyle(floatStyle);
                        }
                    } catch (NumberFormatException e) {
                        cell.setCellValue(new HSSFRichTextString(cellValue));                    
                    }
                }
                
                for (Style style: styles) {
                    if (style.matchesCell(rownum, colnum+1) && !style.isConditional()) {
                        cell.setCellStyle(style.getStyle(workbook));
                        break;
                    }
                }
                
                final String columnName = keys.getColumnNames()[colnum];
                Reference ref = references.get(columnName);
                if (ref != null) {
                    int refIndex = keys.getColumnIndex(ref.getRefCol());
                    if (ss[refIndex] != null) {
                        String url = ref.formatURL(ss[refIndex]);
                        HSSFHyperlink link = new HSSFHyperlink(HSSFHyperlink.LINK_URL);;
                        link.setAddress(url);
                        cell.setHyperlink(link);
                    }
                }
            }
        }
        
        for (Style style: styles) {
            if (style.isConditional()) style.addConditionalFormatting(sheet, workbook);
        }
        
        // Adjust column widths so that they fit data nicely
        for (int i=0; i<keys.getColumnCount(); i++) {
            sheet.autoSizeColumn((short)i);
        }
        
        int frozenColumns = Integer.parseInt(cf.getParameter("frozenColumns"));
        int frozenRows = Integer.parseInt(cf.getParameter("frozenRows"));
        if (frozenColumns > 0 || frozenRows > 0) {
            sheet.createFreezePane(frozenColumns, frozenRows);
        }
    }

    private List<Style> readStyles(CommandFile cf, CSVParser inputParser, int sheetnum) throws IOException {
        List<Style> styles = new ArrayList<Style>();
        if (!cf.inputDefined("style")) return styles;
        
        CSVReader parser = new CSVReader(cf.getInput("style"));
        try {
            while (parser.hasNext()) {
                Map<String, String> row = parser.nextMap();
                
                /* See if the sheet is correct */
                String sheetStr = row.get("Sheet");
		Set<Integer> includeSheet = new HashSet<Integer>();
		Set<Integer> excludeSheet = new HashSet<Integer>();
		Style.parseIndexList(sheetStr, this.sheetNames, includeSheet, excludeSheet, "sheet");
		final boolean found =
		    (includeSheet.isEmpty() || includeSheet.contains(sheetnum)) &&
		    !excludeSheet.contains(sheetnum);
                
		if (found) {
                    Style style = Style.fromCSVRow(row, inputParser);
	            styles.add(style);
		}
            }
        } finally {
            parser.close();
        }
        
        return styles;
    }
    
    private Map<String,Reference> readReferences(CommandFile cf) throws IOException {
        Map<String,Reference> refs = new HashMap<String, Reference>();
        if (!cf.inputDefined("refs")) return refs;
        
        CSVReader parser = new CSVReader(cf.getInput("refs"));
        try {
            while (parser.hasNext()) {
                Map<String, String> row = parser.nextMap();
                Reference ref = new Reference(row.get("URL"),
                        row.get("refCol"), row.get("valueCol"));
                refs.put(ref.getValueCol(), ref);
            }
        } finally {
            parser.close();
        }
        
        return refs;
    }
    
    public static void main(String[] args) {
        new CSV2Excel().run(args);
    }
}

class Style {
    private Set<Integer> rows = new HashSet<Integer>();
    private Set<Integer> excludeRows = new HashSet<Integer>();
    private Set<Integer> columns = new HashSet<Integer>();
    private Set<Integer> excludeColumns = new HashSet<Integer>();
    private HSSFCellStyle style;
    
    private String format;
    private String align;
    private boolean bold;
    private boolean italic;
    private boolean underline;
    private int fontSize = -1;
    private String textColor;
    private String bgColor;
    private String fontName;
    private String[] border;
    
    /* Refers to CFRuleRecord.ComparisonOperator */
    private byte conditionOperator;
    private String conditionFormula1;
    private String conditionFormula2;
    
    /* In Excel, color indices 0x08 .. 0x40 can be customized (56 colors) */
    private static short colorIndex = 0x08;
    
    /* We need to cache the colors so that identical colors are not
     * added several times, as there are limited number of colors available. */
    private static Map<String,Short> seenColors = new HashMap<String, Short>();
    
    private Style() {}

    /** Parse an index list. Supported formats:
      * *,5,-7,Name,-Name,3..8,Name1..Name2,-Name1..Name2. */
    public static void parseIndexList(String indexList, List<String> names,
	Set<Integer> includeResult, Set<Integer> excludeResult, String description) {

	if (indexList == null || indexList.isEmpty() || "*".equals(indexList)) return;

	for (String index: indexList.split(",")) {
	    index = index.trim();

	    if ("*".equals(index)) {
		continue;
	    }

	    final boolean include = !index.startsWith("-");
	    if (!include) {
		index = index.substring(1);
	    }

	    String[] tokens = index.split("[.][.]");
	    int[] numbers = new int[tokens.length];
	    for (int i=0; i<tokens.length; i++) {
		final String token = tokens[i];
		try {
		    numbers[i] = Integer.parseInt(token);
		} catch (NumberFormatException e) {
		    if (names != null) {
			numbers[i] = names.indexOf(token) + 1; 
		    } else {
			throw new IllegalArgumentException(String.format(
			    "Invalid %s (names not supported): %s",
			    description, indexList));
		    }
		}
		if (numbers[i] < 1 || (names != null && numbers[i] > names.size())) {
		    throw new IllegalArgumentException(String.format(
			"Invalid %s: %s",
			description, index));
		}
	    }

	    if (numbers.length == 1) {
		if (include) includeResult.add(numbers[0]);
		else excludeResult.add(numbers[0]);
	    } else if (numbers.length == 2) {
		for (int i=numbers[0]; i<=numbers[1]; i++) {
		    if (include) includeResult.add(i);
		    else excludeResult.add(i);
		}
	    } else {
		throw new IllegalArgumentException(String.format(
		    "Invalid %s: %s",
		    description, index));
	    }
	}
    }
    
    public static Style fromCSVRow(Map<String,String> styleRow, CSVParser inputParser) {
        Style style = new Style();
        
        String row = styleRow.get("Row");
	parseIndexList(row, null, style.rows, style.excludeRows, "row");
        
        String column = styleRow.get("Column");
	parseIndexList(column, Arrays.asList(inputParser.getColumnNames()),
	    style.columns, style.excludeColumns, "column");

        if ("true".equals(styleRow.get("Bold"))) style.bold = true;
        if ("true".equals(styleRow.get("Italic"))) style.italic = true;
        if ("true".equals(styleRow.get("Underline"))) style.underline = true;
        
        String fontSizeStr = styleRow.get("FontSize");
        if (fontSizeStr != null && !fontSizeStr.isEmpty()) {
            style.fontSize = Integer.parseInt(fontSizeStr);
        }
        
        style.format = styleRow.get("Format");
        style.align = styleRow.get("Align");
        
        style.textColor = styleRow.get("TextColor");
        if ("".equals(style.textColor)) style.textColor = null;

        style.bgColor = styleRow.get("BGColor");
        if ("".equals(style.bgColor)) style.bgColor = null;

        style.fontName = styleRow.get("Font");
        if ("".equals(style.fontName)) style.fontName = null;
        
        String border = styleRow.get("Border");
        if (border != null && !border.isEmpty()) style.border = border.split(",");
        
        String condition = styleRow.get("Condition");
        if (condition != null && !condition.isEmpty()) {
            byte op;
            String[] tokens = condition.split(" ");
            if (tokens.length < 2 || tokens.length > 3) {
                throw new IllegalArgumentException("Invalid Condition: "+condition);
            }
            final String opStr = tokens[0];
            if (opStr.equals(">=")) op = CFRuleRecord.ComparisonOperator.GE;
            else if (opStr.equals("<=")) op = CFRuleRecord.ComparisonOperator.LE;
            else if (opStr.equals("==")) op = CFRuleRecord.ComparisonOperator.EQUAL;
            else if (opStr.equals("!=")) op = CFRuleRecord.ComparisonOperator.NOT_EQUAL;
            else if (opStr.equals(">")) op = CFRuleRecord.ComparisonOperator.GT;
            else if (opStr.equals("<")) op = CFRuleRecord.ComparisonOperator.LT;
            else if (opStr.equals("BETWEEN")) op = CFRuleRecord.ComparisonOperator.BETWEEN;
            else if (opStr.equals("NOTBETWEEN")) op = CFRuleRecord.ComparisonOperator.NOT_BETWEEN;
            else {
                throw new IllegalArgumentException("Invalid operator "+opStr+" in condition: "+condition);
            }
            style.conditionOperator = op;
            style.conditionFormula1 = tokens[1];
            if (tokens.length >= 3) style.conditionFormula2 = tokens[2];
        }
        
        return style;
    }
    
    public boolean isConditional() {
        return conditionFormula1 != null;
    }
    
    public boolean matchesCell(int row, int column) {
        if (row <= 0 || column <= 0) {
            throw new IllegalArgumentException("row and column must be >= 1");
        }
        if (!rows.isEmpty() && !rows.contains(row)) return false;
	if (excludeRows.contains(row)) return false;
        if (!columns.isEmpty() && !columns.contains(column)) return false;
        if (excludeColumns.contains(column)) return false;

        return true;
    }
    
    public boolean definesFont() {
        return this.bold
            || this.italic
            || this.underline
            || this.fontSize > 0
            || this.textColor != null
            || this.fontName != null;
    }
    
    public CellRangeAddress getBoundaries(HSSFSheet sheet) {
        int firstRow, lastRow, firstCol, lastCol;
        
        if (rows.isEmpty()) {
            firstRow = sheet.getFirstRowNum();
            lastRow = sheet.getLastRowNum();
        } else {
            firstRow = Collections.min(rows) - 1;
            lastRow = Collections.max(rows) - 1;
        }
	if (!excludeRows.isEmpty()) {
	    final int excludeTop = Collections.max(excludeRows);
	    firstRow = Math.max(firstRow, excludeTop);
	}
        
        if (columns.isEmpty()) {
            HSSFRow row = sheet.getRow(0);
            if (row == null) {
                firstCol = lastCol = 0;
            } else {
                firstCol = row.getFirstCellNum();
                lastCol = row.getLastCellNum();
            }
        } else {
            firstCol = Collections.min(columns) - 1;
            lastCol = Collections.max(columns) - 1;
        }
	if (!excludeColumns.isEmpty()) {
	    final int excludeTop = Collections.max(excludeColumns);
	    firstCol = Math.max(firstCol, excludeTop);
	}
        
        return new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
    }
    
    public static byte[] parseColor(String color) {
        final int RADIX = 16;
        if (color.startsWith("#")) color = color.substring(1);
        if (color.length() == 3) {
            /* ABC is converted to AABBCC */
            final String c1 = color.substring(0, 1);
            final String c2 = color.substring(1, 2);
            final String c3 = color.substring(2, 3);
            color = c1 + c1 + c2 + c2 + c3 + c3;
        }
        
        int red = Integer.parseInt(color.substring(0, 2), RADIX);
        int green = Integer.parseInt(color.substring(2, 4), RADIX);
        int blue = Integer.parseInt(color.substring(4, 6), RADIX);
        return new byte[] { (byte)red, (byte)green, (byte)blue };
    }
    
    public static HSSFColor addColor(HSSFWorkbook workbook, String color) {
        HSSFPalette palette = workbook.getCustomPalette();
        
        Short existing = seenColors.get(color);
        if (existing != null) {
            /* This color string was already seen */
            return palette.getColor(existing);
        } else {
            byte[] rgb = parseColor(color);
            final short thisColor = colorIndex;
            palette.setColorAtIndex(thisColor, rgb[0], rgb[1], rgb[2]);
            HSSFColor hssfColor = palette.getColor(thisColor);
            seenColors.put(color, thisColor);
            colorIndex++;
            return hssfColor;
        }
    }
    
    public HSSFCellStyle getStyle(HSSFWorkbook workbook) {
        if (this.style != null) return this.style;
        
        this.style = workbook.createCellStyle();
        
        if (format != null && !format.isEmpty()) {
            final HSSFDataFormat dataFormat = workbook.createDataFormat();
            this.style.setDataFormat(dataFormat.getFormat(format));
        }
        
        if (align != null && !align.isEmpty()) {
            short al;
            if (align.equals("left")) al = HSSFCellStyle.ALIGN_LEFT;
            else if (align.equals("right")) al = HSSFCellStyle.ALIGN_RIGHT;
            else if (align.equals("center")) al = HSSFCellStyle.ALIGN_CENTER;
            else if (align.equals("justify")) al = HSSFCellStyle.ALIGN_JUSTIFY;
            else {
                throw new IllegalArgumentException("Illegal Align value: "+align);
            }
            this.style.setAlignment(al);
        }
        
        if (definesFont()) {
            HSSFFont font = workbook.createFont();
            if (bold) font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            if (italic) font.setItalic(true);
            if (underline) font.setUnderline(HSSFFont.U_SINGLE);
            if (fontSize > 0) font.setFontHeightInPoints((short)fontSize);
            if (textColor != null) {
                font.setColor(addColor(workbook, textColor).getIndex());
            }
            if (fontName != null) font.setFontName(fontName);
            this.style.setFont(font);
        }

        if (bgColor != null) {
            this.style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            this.style.setFillForegroundColor(addColor(workbook, bgColor).getIndex());
        }
        
        if (border != null && border.length > 0) {
            final short BORDER_TYPE = HSSFCellStyle.BORDER_THIN;
            final short BORDER_COLOR = HSSFColor.AUTOMATIC.index; // usually black
            for (String b: border) {
                if (b.equals("left")) {
                    this.style.setBorderLeft(BORDER_TYPE);
                    this.style.setLeftBorderColor(BORDER_COLOR);
                } else if (b.equals("right")) {
                    this.style.setBorderRight(BORDER_TYPE);
                    this.style.setRightBorderColor(BORDER_COLOR);
                } else if (b.equals("top")) {
                    this.style.setBorderTop(BORDER_TYPE);
                    this.style.setTopBorderColor(BORDER_COLOR);
                } else if (b.equals("bottom")) {
                    this.style.setBorderBottom(BORDER_TYPE);
                    this.style.setBottomBorderColor(BORDER_COLOR);
                } else {
                    throw new IllegalArgumentException("Illegal Border definition: "+b);
                }
            }
        }

        return this.style;
    }
    
    public void addConditionalFormatting(HSSFSheet sheet, HSSFWorkbook workbook) {
        if (!isConditional()) return;
        
        HSSFSheetConditionalFormatting formatting = sheet.getSheetConditionalFormatting();
        HSSFConditionalFormattingRule rule = formatting.createConditionalFormattingRule(
                conditionOperator, conditionFormula1, conditionFormula2);
        
        HSSFFontFormatting fontFormatting = rule.createFontFormatting();
        fontFormatting.setFontStyle(italic, bold);
        if (underline) fontFormatting.setUnderlineType(HSSFFontFormatting.U_SINGLE);
        if (textColor != null && !textColor.isEmpty()) {
            fontFormatting.setFontColorIndex(addColor(workbook, textColor).getIndex());
        }
        /* setFontHeight takes 1/20th points */
        if (fontSize > 0) fontFormatting.setFontHeight(fontSize*20);
        
        if (bgColor != null && !bgColor.isEmpty()) {
            HSSFPatternFormatting patternFormatting = rule.createPatternFormatting();
            short color = addColor(workbook, bgColor).getIndex();
            patternFormatting.setFillPattern(HSSFPatternFormatting.SOLID_FOREGROUND);
            patternFormatting.setFillForegroundColor(color);
            patternFormatting.setFillBackgroundColor(color);
        }
        
        /* Search for an existing conditional rule whose range matches the
         * present range. If an existing rule is found, add this rule to
         * that. Otherwise, add a new rule to the sheet. */
        final CellRangeAddress range = getBoundaries(sheet);
        boolean found = false;
        for (int i=0; i<formatting.getNumConditionalFormattings(); i++) {
            HSSFConditionalFormatting cf = formatting.getConditionalFormattingAt(i);
            if (cf.getFormattingRanges().length == 0) continue;
            CellRangeAddress thisRange = cf.getFormattingRanges()[0];
            if (thisRange.getFirstColumn() == range.getFirstColumn()
                    && thisRange.getFirstRow() == range.getFirstRow()
                    && thisRange.getLastColumn() == range.getLastColumn()
                    && thisRange.getLastRow() == range.getLastRow()) {
                cf.addRule(rule);
                found = true;
                break;
            }
        }
        
        if (!found) {
            formatting.addConditionalFormatting(
                    new CellRangeAddress[] { getBoundaries(sheet) },
                    rule);
        }
    }
}

class Reference {
    private String url;
    private String refCol;
    private String valueCol;
    
    public Reference(String url, String refCol, String valueCol) {
        if (url == null) throw new IllegalArgumentException("URL is missing for refs");
        if (refCol == null) throw new IllegalArgumentException("refCol is missing for refs");
        if (valueCol == null) throw new IllegalArgumentException("valueCol is missing for refs");
        
        this.url = url;
        this.refCol = refCol;
        this.valueCol = valueCol;
    }
    
    public String getRefCol() { return refCol; }
    public String getValueCol() { return valueCol; }
    
    public String formatURL(String refColValue) {
        return url.replace("$ID$", refColValue);
    }
}
