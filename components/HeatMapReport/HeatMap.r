library(componentSkeleton)
library(pheatmap)
library(RColorBrewer)

execute <- function(cf) {
    # Inputs 
    matr <- read.table(get.input(cf, 'matr'),header=T,stringsAsFactors=F,sep="\t",row.names=1)

    # Cluster parameters
    distance.metric <- get.parameter(cf, 'distanceMetric', 'string')
    cluster.method  <- get.parameter(cf, 'clusterMethod', 'string')
    # LaTeX parameters
    section.title <- get.parameter(cf, 'sectionTitle')
    section.type  <- get.parameter(cf, 'sectionType')
    width.cm      <- get.parameter(cf, 'width', 'float') 
    caption       <- get.parameter(cf, 'caption', 'string')
    # Plotting parameters
    margin.samples <- get.parameter(cf, 'marginCol', 'int')
    margin.genes   <- get.parameter(cf, 'marginRow', 'int')
    fontSizeCol    <- get.parameter(cf, 'fontSizeCol', 'float')
    fontSizeRow    <- get.parameter(cf, 'fontSizeRow', 'float')
    scale          <- get.parameter(cf, 'scale')
    drawRownames   <- get.parameter(cf, 'drawRownames', 'boolean')
    drawColnames   <- get.parameter(cf, 'drawColnames', 'boolean')
    legends        <- get.parameter(cf, 'drawLegends',  'boolean')
    color.scheme   <- get.parameter(cf, 'colorScheme', 'string')
    plot.width     <- get.parameter(cf, 'plotWidth', 'int')
    plot.height    <- get.parameter(cf, 'plotHeight', 'int')
    custom   	   <- get.parameter(cf, 'custom', 'string')
   
    stopifnot(scale %in% c('row','column','none')) 
    heat.colors <- eval(parse(text=color.scheme))

    out.dir <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)
    plot.file <- sprintf('%s-heatmap.pdf', get.metadata(cf, 'instanceName'))
	
	n.all.genes <- nrow(matr)
	n.samples <- ncol(matr)


    if (input.defined(cf, 'rowLabels')){
        rowLabels <- read.table(get.input(cf, 'rowLabels'),header=T,stringsAsFactors=F,sep="\t",row.names=1, na.strings='')
		if(nrow(rowLabels)!=nrow(matr))
			stop("The number of rows in rowLabels input doesn't match the number of rows in matrix input")
		if(length(intersect(rownames(matr),rownames(rowLabels)))!=nrow(matr))
			stop("row labels don't match")
		rowLabels <- rowLabels[order(match(rownames(rowLabels),rownames(matr))),,drop=FALSE]
    } else{	
		 rowLabels <- NA
    }
    
    if (input.defined(cf, 'colLabels')){
        colLabels <- read.table(get.input(cf, 'colLabels'),header=T,stringsAsFactors=F,sep="\t",row.names=1, na.strings='')
		if(nrow(colLabels)!=ncol(matr))
			stop("The number of rows in colLabels input doesn't match the number of columns in matrix input")
		if(length(intersect(colnames(matr),rownames(colLabels)))!=ncol(matr))
			stop("col labels don't match")
		colLabels <- colLabels[order(match(rownames(colLabels),colnames(matr))),,drop=FALSE]
    }else{	
		  colLabels <- NA
    }
    distances <- unlist(strsplit(distance.metric,","))  
    stopifnot(length(distances)==2)
    cluster_rows <- T
    cluster_cols <- T
    if(distances[1] == 'NA') cluster_rows <- F
	if(distances[2] == 'NA') cluster_cols <- F
	command <- "pheatmap(matr, annotation_col=colLabels,
					 annotation_row = rowLabels,
					 clustering_distance_rows = distances[1],
					 clustering_distance_cols = distances[2],
					 cluster_rows = cluster_rows,
					 cluster_cols = cluster_cols,
					 show_colnames = drawColnames,
					 show_rownames = drawRownames,
					 fontsize_row = fontSizeRow,
					 fontsize_col = fontSizeCol,
					 scale = scale,
					 legend = legends,
					 color = heat.colors,
					 clustering_method = cluster.method,
					 filename =file.path(out.dir, plot.file),
					 width = plot.width,
					 height = plot.height"
	
	if(nchar(custom)==0){
	  command <- paste0(command,")")
	} else {
	  command <- paste0(command,",",custom,")")
	}
	setwd(get.temp.dir(cf))
	hmap <- eval(parse(text=command))				 
	

    # Construct LaTeX document fragment
    if (nchar(section.type) > 0) {
        tex <- sprintf('\\%s{%s}', section.type, section.title)
    } else {
        tex <- c()
    }
    if (caption == ""){
        caption <- paste(sprintf('Hierarchical clustering using %d samples and up to %d measurements for each sample.',
                                  n.samples, n.all.genes),
                         sprintf('The distance metric is %s.',distance.metric), 
                         'Clustering is done using',
                         sprintf('agglomerative clustering with %s linkage.', cluster.method)
                   )
    } 
    tex <- c(tex, latex.figure(plot.file, caption=caption, image.width=width.cm),'\\pagebreak')
    latex.write.main(cf, 'report', tex)


}

main(execute)
