# SPIA.r
# Anduril wrapper for SPIA algorithm.
# Requires SPIA package.
# Version 1.1
# Author Viljami Aittomaki


PATHWAY_DIAGRAM_URL_PREFIX = "http://www.kegg.jp/kegg-bin/show_pathway?"
PATHWAY_DIAGRAM_FORMAT_GENERAL = "/default%3dwhite/"
PATHWAY_DIAGRAM_UP_FORMAT = "%09red,grey/"
PATHWAY_DIAGRAM_DOWN_FORMAT = "%09blue,grey/"

library(componentSkeleton)
library(SPIA)

execute <- function(cf) {
    # Get parameters
    fcColumn <- get.parameter(cf, 'foldChangeColumn', type='string')
    nboot <- as.numeric(get.parameter(cf, 'nboot', type='int'))
    verb <- get.parameter(cf, 'verbose', type='boolean')
    organism <- get.parameter(cf, 'organism')
    
    # Get pathway data from SPIA package
    data.file <- file.path("extdata", paste(organism, "SPIA.RData", sep=""))
    load(file = system.file(data.file, package = "SPIA"))

    # Read and reformat input
    deg.matr <- LogMatrix.read(get.input(cf, 'deg'))
    ref <- CSV.read(get.input(cf, 'reference'))
    ref <- as.character(ref[,1])

    # Get correct expression column
    if(nchar(fcColumn) > 0) {
        # Check that fold-change column is present in deg
        if(!any(colnames(deg.matr) == fcColumn)) {
            write.error(cf, paste(c("Fold-change column \"",fcColumn,"\" not present in input deg!"), collapse=""));
            return(PARAMETER_ERROR)
        }
        deg <- as.vector(deg.matr[,colnames(deg.matr)==fcColumn])
    } else {
        # Defaults to first numeric column of deg if no fcColumn
        deg <- as.vector(deg.matr[,1])
    }
    names(deg) <- rownames(deg.matr)
    # Remove DEG's with ID NA
    if(any(is.na(names(deg)))) {
        write.log(cf, sprintf("Removed %d genes that had ID 'NA'.", sum(is.na(names(deg)))))
        deg <- deg[!is.na(names(deg))]
    }
    # Remove DEG's with fold change NA
    if(any(is.na(deg))) {
        write.log(cf, paste(c("Removed following genes that had NA fold change:", names(deg[is.na(deg)])), collapse="\n"))
        deg <- deg[!is.na(deg)]
    }

    # Prepare the output folder and the output paths
    pathwayCSVs.path <- get.output(cf, 'pathwayGenes')
    if (!file.exists(pathwayCSVs.path))
        dir.create(pathwayCSVs.path, recursive=TRUE)
    results.path <- get.output(cf, 'pathways')

    # If there are no genes to analyse write empty result list and return
    if(length(deg) == 0) {
        cat("Name\tID\tpSize\tNDE\ttA\tpNDE\tpPERT\tpG\tpGFdr\tpGFWER\tStatus\tKEGGurl",
            file=results.path)
        Array.write(cf, Array.new(), "pathwayGenes")
        return(0)
    }
    
    # Handle genes with multiple id's
    # Remove multiple id's and only use first one for both deg and reference
    multiple.ids <- grep(',', names(deg))
    for (i in multiple.ids) {
        names(deg)[i] <- unlist(strsplit(names(deg)[i], ','))[1]
    }
    multiple.ids <- grep(',', ref)
    for (i in multiple.ids) {
        ref[i] <- unlist(strsplit(ref[i], ','))[1]
    }

    # Check that all deg ID's are present in reference
    for (gene in names(deg)) {
        if(!any(ref == gene)) {
            write.error(cf, paste(c("Gene ID '",gene,"' (from input 'deg') is not present in input 'reference'!"), collapse=""))
            return(INVALID_INPUT)
        }
    }

    # Do SPIA analysis
    results <- spia(deg, ref, organism=organism,nB=nboot, verbose=verb)

    # Make pathway ids strings for later use. See below.
    results[,2] <- as.character(results[,2])

    # Create custom url:s to pahtway diagrams in kegg. Genes in the deg list
    # are marked to the diagram. Upregulated genes are marked with red
    # and down regulated in blue.
    KEGGurl <- character(nrow(results))
    if (nrow(results)>0) for(i in 1:nrow(results)) {
        # Add organism code to pathway id
        pathwayID <- paste(organism, results[i,2], sep="")
        
        KEGGurl[i] <- paste(PATHWAY_DIAGRAM_URL_PREFIX, pathwayID, PATHWAY_DIAGRAM_FORMAT_GENERAL, sep="")
        
        genesInPathway <- rownames(as.data.frame(path.info[names(path.info)==results[i,2]]))
        
        for(gene in genesInPathway) {
            if(gene %in% names(deg)) {  
                fc <- deg[match(gene,names(deg))]
                geneName <- paste(organism, ":", gene, sep="")
                if(fc > 0) {
                    KEGGurl[i] <- paste(KEGGurl[i], geneName, PATHWAY_DIAGRAM_UP_FORMAT, sep="")
                }else {
                    KEGGurl[i] <- paste(KEGGurl[i], geneName, PATHWAY_DIAGRAM_DOWN_FORMAT, sep="")
                }
            }
        }
    }
    
    # Remove SPIA generated urls and add custom urls to results
    results <- cbind(results[,-12], KEGGurl)
    
    # Write a csv-file for each result pathway. Contains genes and fold-changes for each pathway
    filenames <- character(nrow(results))
    pathwayIDs <- character(nrow(results))
    for(i in 1:nrow(results)) {
        # Get genes and their fold-changes for current pathway
        genesInPathway <- rownames(as.data.frame(path.info[names(path.info)==results[i,2]]))
        foldChanges    <- data.frame(Entrez = genesInPathway, FoldChange = numeric(length(genesInPathway)))

        if (nrow(foldChanges) > 0) {
           for(j in 1:nrow(foldChanges)) {
               if(genesInPathway[j] %in% names(deg)) {
                   foldChanges[j,2] <- deg[match(genesInPathway[j],names(deg))]
               }
           }

           # Add organism code to pathway id
           pathwayIDs[i] <- paste(organism, results[i,2], sep="")
           results[i,2] <- pathwayIDs[i]

           # Write genefile for current pathway
           filenames[i] <- paste(pathwayIDs[i], ".csv", sep="")
           CSV.write(file.path(pathwayCSVs.path, filenames[i]), foldChanges)
        }
    }
    Array.write(cf, Array.new(keys=pathwayIDs,files=filenames), "pathwayGenes")

    # Write pathway statistics
    CSV.write(results.path, results)
    
    return(0)
}

main(execute)

