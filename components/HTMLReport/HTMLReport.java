import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import fi.helsinki.ltdk.csbl.anduril.component.CSVReader;
import fi.helsinki.ltdk.csbl.anduril.component.Color;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;

public class HTMLReport extends SkeletonComponent {
    public static final int MAX_TABLES = 9;
    public static final int MAX_MATRICES = 9;
    public static final String CSS_FILE = "style.css";
    public static final String SCRIPTS_FILE = "scripts.js";
    public static final int NUM_COLORS = 63;
    
    public static final String LABELS_QUERY = "_QUERY";
    public static final String LABELS_GLOBAL = "_GLOBAL";
    public static final String LABELS_DESCRIPTION = "_DESCRIPTION";
    
    private Map<String,Table> tables;
    private Map<String,Matrix> matrices;
    private int maxRows;
    private boolean omitMissing;
    private List<String> enabledSummaries;
    private List<Color> colors;
    private String description;
    
    /** Nanoseconds spent in Velocity */
    private long velocityTime;
    
    public HTMLReport() throws Exception {
        this.tables = new TreeMap<String, Table>();
        this.matrices = new HashMap<String, Matrix>();
        this.velocityTime = 0;
        
        Velocity.setProperty("runtime.references.strict", "true");
        Velocity.setProperty("resource.loader", "file");
        Velocity.setProperty("file.resource.loader.cache", "true");
        Velocity.setProperty("file.resource.loader.modificationCheckInterval", "0");
        Velocity.init();
    }
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        final long startTime = System.nanoTime();
        
        String colorStart = cf.getParameter("colorStart");
        String colorMiddle = cf.getParameter("colorMiddle");
        String colorEnd = cf.getParameter("colorEnd");
        if (!colorStart.isEmpty() && !colorMiddle.isEmpty() && !colorEnd.isEmpty()) {
            this.colors = Color.interpolate(NUM_COLORS, new Color(colorStart),
                    new Color(colorMiddle), new Color(colorEnd));
        } else {
            this.colors = new ArrayList<Color>();
        }
        
        System.out.println("Reading input files...");
        
        /* Read tables */
        String[] tableLabels = cf.getParameter("tableLabels").split(",");
        for (int i=1; i<=MAX_TABLES; i++) {
            String id = "table"+i;
            String label = (tableLabels.length >= i) ? tableLabels[i-1] : id;
            if (label.isEmpty()) label = id;
            Table table = readTable(cf, id, label);
            if (table != null) tables.put(table.getName(), table);
        }
        
        /* Read matrices */
        String [] matrixLabels = cf.getParameter("matrixLabels").split(",");
        for (int i=1; i<=MAX_MATRICES; i++) {
            String id = "matrix"+i;
            String label = (matrixLabels.length >= i) ? matrixLabels[i-1] : id;
            Matrix matrix = readMatrix(cf, id, label);
            if (matrix != null) matrices.put(matrix.getName(), matrix);
        }

        this.enabledSummaries = new ArrayList<String>();
        for (String tableID: cf.getParameter("includeSummaries").split(",")) {
            if (tableID == null || tableID.isEmpty()) continue;
            if (tableID.equals("*")) {
                this.enabledSummaries.addAll(this.tables.keySet());
                break;
            }
            this.enabledSummaries.add(tableID);
        }
        
        this.omitMissing = cf.getParameter("omitMissing").equals("true");
        this.maxRows = Integer.parseInt(cf.getParameter("recordsPerPage"));

        boolean ok = readMapping(cf);
        if (!ok) return ErrorCode.INVALID_INPUT;
        
        ok = readMatrixMapping(cf);
        if (!ok) return ErrorCode.INVALID_INPUT;
        
        ok = readImageMapping(cf);
        if (!ok) return ErrorCode.INVALID_INPUT;
        
        ok = readRefs(cf);
        if (!ok) return ErrorCode.INVALID_INPUT;
        
        ok = readLabels(cf);
        if (!ok) return ErrorCode.INVALID_INPUT;
        
        for (Table table: tables.values()) {
            table.updateIndices();
        }
        
        System.out.println("Writing main page...");
        writeIndex(cf);

        File cssTarget = new File(cf.getOutput("report"), CSS_FILE);
        Tools.copyFile(new File(CSS_FILE), cssTarget);

        File scriptsTarget = new File(cf.getOutput("report"), SCRIPTS_FILE);
        Tools.copyFile(new File(SCRIPTS_FILE), scriptsTarget);
        
        for (Table table: tables.values()) {
            writeTable(cf, table);
        }
        
        Runtime.getRuntime().gc();
        final long totalTime = System.nanoTime() - startTime;
        final long memoryMB = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024*1024);
        cf.writeLog(String.format("Timing: %.2f s, out of which in Apache Velocity %.2f s",
                totalTime/1e9, this.velocityTime/1e9));
        cf.writeLog(String.format("Memory used: %d MB", memoryMB));
        
        return ErrorCode.OK;
    }
    
    private Table readTable(CommandFile cf, String inputName, String label) throws IOException {
        if (!cf.inputDefined(inputName)) return null;
        int digits = Integer.parseInt(cf.getParameter("digits"));
        
        CSVReader reader = new CSVReader(cf.getInput(inputName));
        try {
            Table table = new Table(inputName, label, new ArrayList<String>(reader.getHeader()), digits,
                    cf.getParameter("missingValue"));
            for (List<String> row: reader) {
                ArrayList<String> newRow = new ArrayList<String>(row);
                newRow.trimToSize();
                table.addRow(newRow);
            }
            return table;
        } finally {
            reader.close();
        }
    }
    
    private boolean readMapping(CommandFile cf) throws IOException {
        if (!cf.inputDefined("mapping")) return true;
        
        CSVReader reader = new CSVReader(cf.getInput("mapping"));
        try {
            while (reader.hasNext()) {
                Map<String,String> row = reader.nextMap();
                String tableName = row.get("Table");
                if (tableName == null) {
                    cf.writeError("Missing column from mapping: Table");
                    return false;
                }
                
                Table table = tables.get(tableName);
                if (table == null) {
                    cf.writeError("Table not found: "+tableName);
                    return false;
                }
                
                String keyColumn = row.get("KeyColumn");
                if (keyColumn != null) {
                    if (!table.containsColumn(keyColumn)) {
                        cf.writeError(table.getName()+": Invalid KeyColumn: column not found: "+keyColumn);
                        return false;
                    }
                    table.setKeyColumn(keyColumn);
                }
                
                String showColumn = row.get("ViewColumn");
                if (showColumn != null) {
                    if (!table.containsColumn(showColumn)) {
                        cf.writeError(table.getName()+": Invalid ViewColumn: column not found: "+showColumn);
                        return false;
                    }
                    table.setShowColumn(showColumn);
                }
                
                String foreignKeys = row.get("ForeignKeys");
                if (foreignKeys != null) {
                    for (String foreign: foreignKeys.split(",")) {
                        if (foreign.isEmpty()) continue;
                        String[] tokens = foreign.split("=");
                        if (tokens.length != 2) {
                            cf.writeError("Invalid foreign key definition: "+foreign);
                            return false;
                        }
                        final String fromColumn = tokens[0];
                        if (!table.containsColumn(fromColumn)) {
                            cf.writeError(table.getName()+": Invalid foreign key: column not found: "+fromColumn);
                            return false;
                        }
                        final String toTableName = tokens[1];
                        Table toTable = tables.get(toTableName);
                        if (toTable == null) {
                            cf.writeError("Invalid foreign key: target table not found: "+toTableName);
                            return false;
                        }
                        table.addForeignKey(fromColumn, toTable);
                    }
                }
                
                String summaryColumns = row.get("SummaryColumns");
                if (summaryColumns != null && !summaryColumns.equals("*")) {
                    List<String> summaryColumnsList = new ArrayList<String>();
                    for (String col: summaryColumns.split(",")) {
                        if (!table.containsColumn(col)) {
                            cf.writeError(table.getName()+": Invalid SummaryColumns: column not found: "+col);
                            return false;
                        }
                        summaryColumnsList.add(col);
                    }
                    table.setSummaryColumns(summaryColumnsList);
                }
                
                String ignoreColumns = row.get("IgnoreColumns");
                if (ignoreColumns != null) {
                    Set<String> columns = new HashSet<String>();
                    for (String col: ignoreColumns.split(",")) columns.add(col);
                    table.removeColumns(columns);
                }
                
                String sortColumns = row.get("SortColumns");
                if (sortColumns != null) {
                    Set<String> columns = new HashSet<String>();
                    for (String col: sortColumns.split(",")) {
                        if (col.equals("*")) {
                            columns.addAll(table.getColumnNames());
                            break;
                        }
                        if (!table.containsColumn(col)) {
                            cf.writeError(table.getName()+": Invalid SortColumns: column not found: "+col);
                            return false;
                        }
                        columns.add(col);
                    }
                    table.setSortColumns(columns);
                }
                
                String inlineTables = row.get("InlineTables");
                if (inlineTables != null) {
                    for (String name: inlineTables.split(",")) {
                        if (name.isEmpty()) continue;
                        Table inline = this.tables.get(name);
                        if (inline == null) {
                            cf.writeError(table.getName()+": Invalid InlineTables: table not found: "+name);
                            return false;
                        }
                        table.addInlineTable(inline);
                    }
                }
                
                String inlineColumns = row.get("InlineColumns");
                if (inlineColumns != null) {
                    List<String> columns = new ArrayList<String>();
                    for (String column: inlineColumns.split(",")) {
                        if (column.trim().isEmpty()) continue;
                        if (!table.containsColumn(column)) {
                            cf.writeError(table.getName()+": Invalid InlineColumns: column not found: "+column);
                            return false;
                        }
                        columns.add(column);
                    }
                    table.setInlineColumns(columns);
                }

                String aliasColumns = row.get("AliasColumns");
                if (aliasColumns != null) {
                    Set<String> columns = new HashSet<String>();
                    for (String column: aliasColumns.split(",")) {
                        if (column.trim().isEmpty()) continue;
                        if (!table.containsColumn(column)) {
                            cf.writeError(table.getName()+": Invalid AliasColumns: column not found: "+column);
                            return false;
                        }
                        columns.add(column);
                    }
                    table.setAliasColumns(columns);
                }
                
                String colorRange = row.get("ColorRange");
                if (colorRange != null) {
                    for (String entry: colorRange.split(",")) {
                        String[] topLevel = entry.split("=", 2);
                        if (topLevel.length < 2) {
                            cf.writeError(table.getName()+": Invalid color range: "+entry);
                            return false;
                        }
                        
                        final String column = topLevel[0];
                        if (!table.containsColumn(column)) {
                            cf.writeError(table.getName()+": Invalid color range: column not found: "+column);
                            return false;
                        }
                        
                        String[] limitTokens = topLevel[1].split(" ");
                        if (limitTokens.length < 2) {
                            cf.writeError(table.getName()+": Invalid color range: too few limit items in "+entry);
                            return false;
                        }
                        
                        double low = Double.parseDouble(limitTokens[0]);
                        double high = Double.parseDouble(limitTokens[1]);
                        boolean logarithm = limitTokens.length >= 3
                            && limitTokens[limitTokens.length-1].equals("log");
                        table.addColorRange(column, low, high, logarithm);
                    }
                }
            }
        } finally {
            reader.close();
        }
        return true;
    }
    
    private boolean readImageMapping(CommandFile cf) throws IOException {
        if (!cf.inputDefined("imageMapping")) return true;
        CSVReader reader = new CSVReader(cf.getInput("imageMapping"));
        try {
            while (reader.hasNext()) {
                Map<String,String> row = reader.nextMap();
                String tableName = row.get("Table");
                if (tableName == null) tableName = "table1";
                Table table = this.tables.get(tableName);
                if (table == null) {
                    cf.writeError("Image mapping: table not found: "+tableName);
                    return false;
                }
                
                String target = row.get("Target");
                String imageFile = row.get("ImageFile");
                if (target == null) {
                    cf.writeError("Image mapping: Target is missing");
                    return false;
                }
                if (imageFile != null) {
                	table.addImageMapping(target, imageFile, row.get("Label"));
                }
            }
        } finally {
            reader.close();
        }
        return true;
    }
    
    private boolean readRefs(CommandFile cf) throws IOException {
        if (!cf.inputDefined("refs")) return true;
        CSVReader reader = new CSVReader(cf.getInput("refs"));
        try {
            while (reader.hasNext()) {
                Map<String,String> row = reader.nextMap();
                String tableName = row.get("Table");
                if (tableName == null) tableName = "table1";
                Table table = this.tables.get(tableName);
                if (table == null) {
                    cf.writeError("URL references: table not found: "+tableName);
                    return false;
                }
                
                String url = row.get("URL");
                String column = row.get("Column");
                if (url == null) {
                    cf.writeError("URL references: URL is missing");
                    return false;
                }
                if (column == null) {
                    cf.writeError("URL references: Column is missing");
                    return false;
                }
                if (!table.containsColumn(column)) {
                    cf.writeError(table.getName()+": URL references: Column not found: "+column);
                    return false;
                }
                table.addURLReferences(column, url);
            }
        } finally {
            reader.close();
        }
        return true;
    }
    
    private Matrix readMatrix(CommandFile cf, String inputName, String label) throws IOException {
        if (!cf.inputDefined(inputName)) return null;
        final int threshold = Integer.parseInt(cf.getParameter("matrixThreshold"));
        CSVReader reader = new CSVReader(cf.getInput(inputName));
        try {
            Matrix matrix = new Matrix(inputName, label, threshold);
            matrix.setColumnNames(reader.getHeader().subList(1, reader.getHeader().size()));
            for (List<String> row: reader) {
                String rowName = row.get(0);
                double[] data = new double[row.size()-1];
                for (int i=1; i<row.size(); i++) {
                    final String strValue = row.get(i);
                    if (strValue == null) data[i-1] = Double.NaN;
                    else data[i-1] = Double.parseDouble(strValue);
                }
                matrix.addRow(rowName, data);
            }
            matrix.trim();
            return matrix;
        } finally {
            reader.close();
        }
    }
    
    private boolean readMatrixMapping(CommandFile cf) throws IOException {
        if (!cf.inputDefined("matrixMapping")) return true;
        CSVReader reader = new CSVReader(cf.getInput("matrixMapping"));
        try {
            while (reader.hasNext()) {
                Map<String,String> rowMap = reader.nextMap();
                
                String matrixName = rowMap.get("Matrix");
                if (matrixName == null) matrixName = "matrix1";
                Matrix matrix = this.matrices.get(matrixName);
                if (matrix == null) {
                    cf.writeError("matrixMapping: Matrix not provided: "+matrixName);
                    return false;
                }
                
                String tableName = rowMap.get("ColumnTable");
                if (tableName != null) {
                    Table table = this.tables.get(tableName);
                    if (table == null) {
                        cf.writeError("matrixMapping: Table not found: "+tableName);
                        return false;
                    }
                    table.addMatrixColumnLink(matrix);
                }

                tableName = rowMap.get("RowTable");
                if (tableName != null) {
                    Table table = this.tables.get(tableName);
                    if (table == null) {
                        cf.writeError("matrixMapping: Table not found: "+tableName);
                        return false;
                    }
                    table.addMatrixRowLink(matrix);
                }
            }
        } finally {
            reader.close();
        }
        return true;
    }
    
    private boolean readLabels(CommandFile cf) throws IOException {
        if (!cf.inputDefined("labels")) return true;
        CSVReader reader = new CSVReader(cf.getInput("labels"));
        
        try {
            while (reader.hasNext()) {
                Map<String,String> rowMap = reader.nextMap();
                
                String tableName = rowMap.get("Table");
                if (tableName == null) tableName = "table1";
                
                final String column = rowMap.get("Column");
                if (column == null) {
                    cf.writeError("labels: column not present");
                    return false;
                }
                
                final String label = rowMap.get("Label");

                if (tableName.equals(LABELS_GLOBAL)) {
                    if (column.equals(LABELS_DESCRIPTION)) {
                        this.description = label;
                        continue;
                    } else {
                        cf.writeError("labels: invalid column in "+tableName+": "+column);
                        return false;
                    }
                }
                
                Table table = this.tables.get(tableName);
                if (table == null) {
                    cf.writeError("labels: table not found: "+tableName);
                    return false;
                }
                
                if (column.equals(LABELS_QUERY)) table.setQueryLabel(label);
                else {
                    if (!table.containsColumn(column)) {
                        cf.writeError(table.getName()+": labels: column not found: "+column);
                        return false;
                    }
                    table.addColumnLabel(column, label);
                }
                
                final String lowLabel = rowMap.get("LowLabel");
                final String middleLabel = rowMap.get("MiddleLabel");
                final String highLabel = rowMap.get("HighLabel");
                if (lowLabel != null || middleLabel != null || highLabel != null) {
                    Table.ColorRange range = table.getColorRange(column);
                    if (range == null) {
                        cf.writeError(table.getName()
                                +": color range labels set for a column that has no color range: "+column);
                        return false;
                    }
                    range.lowLabel = lowLabel;
                    range.middleLabel = middleLabel;
                    range.highLabel = highLabel;
                }
            }
        } finally {
            reader.close();
        }
        
        return true;
    }
    
    private void renderVelocity(Template template, VelocityContext model, File target) throws Exception {
        final long start = System.nanoTime();
        
        FileWriter writer = new FileWriter(target);
        try {
            template.merge(model, writer);
        } finally {
            writer.close();
        }
        
        final long end = System.nanoTime();
        this.velocityTime += end - start;
    }
    
    private void writeIndex(CommandFile cf) throws Exception {
        File outDir = cf.getOutput("report");
        outDir.mkdirs();
        File target = new File(outDir, "index.html");
        String currentDate=new Date().toString(); 
        VelocityContext model = new VelocityContext();
        model.put("css", CSS_FILE);
        model.put("title", "Anduril results");
        model.put("tables", this.tables);
        model.put("enabledTables", this.enabledSummaries);
        model.put("description", this.description);
        model.put("date", currentDate);
        Template template = Velocity.getTemplate("index.html");
        renderVelocity(template, model, target);
        
        target = new File(outDir, "index_details.html");
        template = Velocity.getTemplate("index_details.html");
        model.put("title", "Attribute details");
        model.put("colorSlide", this.colors);
        renderVelocity(template, model, target);
    }
    
    private void writeTable(CommandFile cf, Table table) throws Exception {
        File outDir = cf.getOutput("report");
        outDir.mkdirs();
        
        File idMapDir = new File(outDir, table.getIDMapDirectory());
        idMapDir.mkdirs();
        writeTableIDMap(table, idMapDir);
        
        System.out.println("Writing "+table.getName()+": records...");
        File imageDir = cf.inputDefined("images") ? cf.getInput("images") : null;
        writeTableRecords(table, outDir, imageDir);
        
        System.out.println("Writing "+table.getName()+": summary...");
        writeTableIndex(table, outDir);
    }
    
    private void writeTableIndex(Table table, File outDir) throws Exception {
        if (!this.enabledSummaries.contains(table.getName())) return;

        final int NUM_ROWS = table.size();
        final int NUM_PAGES = (int) Math.ceil((double)NUM_ROWS / this.maxRows);
        Template template = Velocity.getTemplate("summary.html");
        
        Set<String> sortColumns = table.getSortColumns();
        sortColumns.add(null);
        
        for (String sortColumn: sortColumns) {
            for (boolean reverse: new boolean[] {true, false}) {
                if (sortColumn != null && !sortColumn.isEmpty()) {
                    table.sort(sortColumn, reverse);
                } else if (reverse) {
                    /* Skip the reversed unsorted case. */
                    continue;
                }
                
                for (int pageNumber=1; pageNumber<=NUM_PAGES; pageNumber++) {
                    int fromRow = (pageNumber-1)*this.maxRows;
                    int toRow = fromRow + this.maxRows;
                    if (toRow > NUM_ROWS) toRow = NUM_ROWS;

                    File target = new File(outDir, table.getIndexAddress(pageNumber, sortColumn, reverse, 0));
                    VelocityContext model = new VelocityContext();
                    model.put("css", CSS_FILE);
                    model.put("title", table.getLabel());
                    model.put("table", table);
                    model.put("tables", this.tables);
                    model.put("enabledTables", this.enabledSummaries);
                    model.put("fromRow", fromRow);
                    model.put("toRow", toRow);
                    model.put("sortColumn", sortColumn);
                    model.put("sortColumns", sortColumns);
                    model.put("reverseSort", reverse);
                    model.put("colorSlide", this.colors);
                    
                    model.put("currentPage", pageNumber);
                    model.put("maxPage", NUM_PAGES);
                    List<Integer> navPages = new ArrayList<Integer>();
                    final int SMALL_LIMIT = 10;
                    boolean constantNavBar = false;
                    if (NUM_PAGES > 1 && NUM_PAGES < SMALL_LIMIT) {
                        constantNavBar = true;
                        for (int i=1; i<=NUM_PAGES; i++) navPages.add(i);
                    }
                    else if (NUM_PAGES >= SMALL_LIMIT) {
                        if (pageNumber > 2) navPages.add(1); /* First */
                        if (pageNumber > 11) navPages.add(pageNumber-10); /* 10 back */
                        if (pageNumber > 1) navPages.add(pageNumber-1); /* Previous */
                        navPages.add(pageNumber);
                        if (pageNumber < NUM_PAGES) navPages.add(pageNumber+1); /* Next */
                        if (pageNumber < NUM_PAGES-10) navPages.add(pageNumber+10); /* 10 forward */
                        if (pageNumber < NUM_PAGES-1) navPages.add(NUM_PAGES); /* Last */
                    }
                    model.put("constantNavBar", constantNavBar);
                    model.put("navPages", navPages);
                    renderVelocity(template, model, target);
                }
            }
        }
    }
    
    private void writeTableRecords(Table table, File outDir, File imagesSourceDir) throws Exception {
        Template template = Velocity.getTemplate("record.html");
        
        for (int recordID=0; recordID<table.size(); recordID++) {
            String keyValue = table.getKeyValue(recordID);
            String showValue = table.getShowValue(recordID);
            String address = table.getRecordAddress(keyValue, 0);
            File targetFile = new File(outDir, address);
            File targetDir = targetFile.getParentFile();
            targetDir.mkdirs();
            
            VelocityContext model = new VelocityContext();
            model.put("css", "../"+CSS_FILE);
            model.put("title", table.getLabel()+": "+showValue);
            model.put("table", table);
            model.put("recordID", recordID);
            model.put("omitMissing", this.omitMissing);
            model.put("colorSlide", this.colors);
            renderVelocity(template, model, targetFile);
            
            for (Table.Image image: table.getImages(recordID)) {
                if (imagesSourceDir != null) {
                    File imageSource = new File(imagesSourceDir, image.imageFile);
                    File imageTarget = new File(targetDir, image.imageFile);
                    Tools.copyFile(imageSource, imageTarget);
                }
            }
        }
    }
    
    private void writeTableIDMap(Table table, File targetDir) throws IOException {
        // hex -> (id -> page)
        Map<String, SortedMap<String,String>> mapEntries = new HashMap<String, SortedMap<String,String>>();
        
        for (int i=0; i<Table.HASH_MODULUS; i++) {
            /* Pre-populate the table: all hash indices need to be
             * written to disk. */
            mapEntries.put(String.format("%02x", i), new TreeMap<String, String>());
        }
        
        for (int recordID=0; recordID<table.size(); recordID++) {
            String id = table.getKeyValue(recordID);
            if (id != null) {
                id = normalizeID(id);
                String hash = String.format("%02x", Table.idHashCode(id));
                Map<String,String> entries = mapEntries.get(hash);
                entries.put(id, table.getRecordAddress(recordID, 0));
            }
            
            String showName = table.getShowValue(recordID);
            if (showName != null) {
                showName = normalizeID(showName);
                if (!showName.equals(id)) {
                    String hash = String.format("%02x", Table.idHashCode(showName));
                    Map<String,String> entries = mapEntries.get(hash);
                    entries.put(showName, table.getRecordAddress(recordID, 0));
                }
            }
            
            for (String aliasColumn: table.getAliasColumns()) {
                String value = table.getValue(recordID, aliasColumn);
                if (value == null || value.isEmpty()) continue;
                for (String alias: value.split(",")) {
                    alias = normalizeID(alias);
                    if (alias.isEmpty()) continue;
                    String hash = String.format("%02x", Table.idHashCode(alias));
                    Map<String,String> entries = mapEntries.get(hash);
                    if (!entries.containsKey(alias)) {
                        entries.put(alias, table.getRecordAddress(recordID, 0));
                    }
                }
            }
        }
        
        for (Map.Entry<String,SortedMap<String,String>> entry: mapEntries.entrySet()) {
            File mapFile = new File(targetDir, entry.getKey());
            BufferedWriter writer = new BufferedWriter(new FileWriter(mapFile));
            try {
                if (!entry.getValue().isEmpty()) writer.write("\n");
                for (Map.Entry<String,String> mapEntry: entry.getValue().entrySet()) {
                    writer.write(String.format("%s\t%s\n",
                            mapEntry.getKey(), mapEntry.getValue()));
                }
            } finally {
                writer.close();
            }
        }
    }
    
    private String normalizeID(String id) {
        if (id == null) return null;
        return id.trim().toLowerCase();
    }
    
    public static void main(String[] args) throws Exception {
        new HTMLReport().run(args);
    }
}
