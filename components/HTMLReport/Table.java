import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.collections.primitives.ArrayIntList;

import fi.helsinki.ltdk.csbl.anduril.component.Color;

public class Table {
    
    public static class Image {
        public final String imageFile;
        public final String imageLabel;
        public Image(String imageFile, String imageLabel) {
            this.imageFile = imageFile;
            this.imageLabel = imageLabel;
        }
        public String getImageFile() { return this.imageFile; }
        public String getImageLabel() { return this.imageLabel; }
    }
    
    public static class ColorRange {
        public final double low;
        public final double high;
        public final boolean logarithm;
        public String lowLabel;
        public String middleLabel;
        public String highLabel;
        public ColorRange(double low, double high, boolean logarithm) {
            this.low = low;
            this.high = high;
            this.logarithm = logarithm;
        }
        public boolean hasLabels() {
            return (this.lowLabel != null && !this.lowLabel.isEmpty())
                || (this.middleLabel != null && !this.middleLabel.isEmpty())
                || (this.highLabel != null && !this.highLabel.isEmpty());
        }
        public String getLowLabel(String nullValue) {
            return this.lowLabel == null ? nullValue : this.lowLabel;
        }
        public String getMiddleLabel(String nullValue) {
            return this.middleLabel == null ? nullValue : this.middleLabel;
        }
        public String getHighLabel(String nullValue) {
            return this.highLabel == null ? nullValue : this.highLabel;
        }
        public double getLowLimit(boolean linear) {
            if (linear && this.logarithm) return Math.pow(2, this.low);
            else return this.low;
        }
        public double getMiddleLimit(boolean linear) {
            final double limit = (this.low + this.high) / 2.0;
            if (linear && this.logarithm) return Math.pow(2, limit);
            else return limit;
        }
        public double getHighLimit(boolean linear) {
            if (linear && this.logarithm) return Math.pow(2, this.high);
            else return this.high;
        }
    }
    
    public static class ColumnRange {
        public final int startPos;
        public int endPos;
        public final String label;
        public ColumnRange(int startPos, int endPos, String label) {
            this.startPos = startPos;
            this.endPos = endPos;
            this.label = label;
        }
        public String getLabel(String nullValue) {
            return label == null ? nullValue : label;
        }
        public int getLength() {
            return endPos - startPos + 1;
        }
        public boolean isEmpty() {
            return label == null;
        }
    }
    
    static final class RowComparator implements Comparator<Integer> {
        private final List<List<String>> content;
        private final ArrayIntList recordIDs;
        private final int columnIndex;
        private final boolean reverse;
        private final double[] numericCache;
        
        public RowComparator(List<List<String>> content, ArrayIntList recordIDs, int columnIndex, boolean reverse, double[] numericCache) {
            this.content = content;
            this.recordIDs = recordIDs;
            this.columnIndex = columnIndex;
            this.reverse = reverse;
            this.numericCache = numericCache;
        }
        @Override
        public int compare(final Integer index1, final Integer index2) {
            /* If both values are numeric, they are compared
             * as numeric. Otherwise, they are compared as
             * strings. Nulls are put after all other values. */
            final String value1 = content.get(index1).get(columnIndex);
            final String value2 = content.get(index2).get(columnIndex);
            if (value1 == null && value2 == null) {
                /* Both null: sort by record IDs, don't reorder when reverse
                 * sorting */
                final Integer id1 = recordIDs.get(index1);
                final Integer id2 = recordIDs.get(index2);
                return id1.compareTo(id2);
            }
            else if (value1 == null) return 1;
            else if (value2 == null) return -1;
            
            final double numeric1 = this.numericCache[index1];
            if (Double.isNaN(numeric1)) return reverse ? value2.compareTo(value1) : value1.compareTo(value2);
            final double numeric2 = this.numericCache[index2];
            if (Double.isNaN(numeric2)) return reverse ? value2.compareTo(value1) : value1.compareTo(value2);
            return reverse ? Double.compare(numeric2, numeric1) : Double.compare(numeric1, numeric2);
        }
    }
    
    public static final int HASH_MODULUS = 256;
    
    private String name;
    private String label;
    private String missingValue;
    private String numberFormat;
    private String queryLabel;
    
    private final List<List<String>> content;
    private ArrayIntList recordID2Physical;
    private ArrayIntList physical2RecordID;
    private Map<String,Integer> key2RecordID;
    private Map<Table,Map<Integer,ArrayIntList>> foreignIndex;
    
    private final List<String> columnNames;
    
    private Map<String, Table> foreignKeys;
    private Map<Table, String> reverseLinks;
    private List<Table> inlineTables;
    
    private String keyColumn;
    private String showColumn;
    private List<String> summaryColumns;
    private List<String> enabledColumnNames;
    private Set<String> sortColumns;
    private Map<String,String> columnLabels;
    private List<String> inlineColumns;
    private Set<String> aliasColumns;
    private Set<String> integerColumns;
    
    private Map<String, List<Image>> imageMap;
    private Set<Matrix> matrixColumnLinks;
    private Set<Matrix> matrixRowLinks;
    private Map<String, String> refs;
    
    private Map<String, ColorRange> colorRanges;

    public Table(String name, String label, List<String> columnNames, int digits, String missingValue) {
        this.name = name;
        this.label = label;
        this.columnNames = columnNames;
        this.missingValue = missingValue;
        
        this.recordID2Physical = new ArrayIntList();
        this.physical2RecordID = new ArrayIntList();
        this.key2RecordID = null;
        this.foreignIndex = null;
        
        this.columnLabels = new HashMap<String, String>();
        this.inlineColumns = null;
        this.summaryColumns = null;
        this.enabledColumnNames = new ArrayList<String>(columnNames);
        this.sortColumns = new HashSet<String>();
        this.foreignKeys = new HashMap<String, Table>();
        this.reverseLinks = new HashMap<Table, String>();
        this.inlineTables = new ArrayList<Table>();
        this.aliasColumns = new HashSet<String>();
        this.content = new ArrayList<List<String>>();
        this.imageMap = new HashMap<String,List<Image>>();
        this.refs = new HashMap<String, String>();
        this.matrixColumnLinks = new HashSet<Matrix>();
        this.matrixRowLinks = new HashSet<Matrix>();
        this.colorRanges = new HashMap<String, ColorRange>();
        
        this.numberFormat = "%." + digits + "g";
        
        this.integerColumns = new HashSet<String>(columnNames);
    }
    
    public String toString() {
        return this.name;
    }
    
    // Getters & setters ///////////////////////////////////////////////
    
    public String getName() {
        return name;
    }
    
    public List<String> getColumnNames() {
        return columnNames;
    }
    
    public boolean containsColumn(String columnName) {
        if (this.columnNames == null) return false;
        return this.columnNames.contains(columnName);
    }
    
    public List<String> getEnabledColumns(boolean inline) {
        if (inline) return getInlineColumns();
        else return enabledColumnNames;
    }
    
    public List<String> getEnabledColumns() {
        return getEnabledColumns(false);
    }
    
    public String getLabel() {
        return label != null ? label : name;
    }
    
    public List<String> getSummaryColumns() {
        if (this.summaryColumns == null) return getEnabledColumns(false);
        else return this.summaryColumns;
    }
    
    public void setSummaryColumns(List<String> summaryColumns) {
        this.summaryColumns = summaryColumns;
    }
    
    public String getKeyColumn() {
        if (this.keyColumn != null) return this.keyColumn;
        else return this.enabledColumnNames.get(0);
    }
    
    public void setKeyColumn(String column) {
        this.keyColumn = column;
    }
    
    public String getShowColumn() {
        if (this.showColumn != null) return this.showColumn;
        else return getKeyColumn();
    }
    
    public void setShowColumn(String column) {
        this.showColumn = column;
    }
    
    public void removeColumns(Set<String> columns) {
        if (this.enabledColumnNames != null) {
            this.enabledColumnNames.removeAll(columns);
        }
        if (this.summaryColumns != null) {
            this.summaryColumns.removeAll(columns);
        }
        setKeyColumn(getKeyColumn());
    }
    
    public String getQueryLabel() {
        if (this.queryLabel == null) return "Query "+getLabel()+":";
        else return this.queryLabel;
    }
    
    public void setQueryLabel(String label) {
        this.queryLabel = label;
    }
    
    public String getColumnLabel(String column) {
        return this.columnLabels.get(column);
    }
    
    public void addColumnLabel(String column, String label) {
        this.columnLabels.put(column, label);
    }
    
    public boolean hasColumnLabels() {
        return !this.columnLabels.isEmpty();
    }
    
    public Set<String> getSortColumns() {
        return this.sortColumns;
    }
    
    public void setSortColumns(Set<String> columnNames) {
        this.sortColumns = columnNames;
    }
    
    public void addInlineTable(Table table) {
        this.inlineTables.add(table);
    }
    
    public List<Table> getInlineTables() {
        return this.inlineTables;
    }
    
    public void setInlineColumns(List<String> columns) {
        this.inlineColumns = columns;
    }
    
    public List<String> getInlineColumns() {
        if (this.inlineColumns == null) return this.enabledColumnNames;
        else return this.inlineColumns;
    }
    
    public void setAliasColumns(Set<String> columns) {
        this.aliasColumns = columns;
    }
    
    public Set<String> getAliasColumns() {
        return this.aliasColumns;
    }
    
    // URLs ////////////////////////////////////////////////////////////    
    
    public String getHTMLPrefix() {
        return name;
    }
    
    public String getIndexAddress(int pageNumber, String sortColumn, boolean reverseSort, int ascendLevels) {
        if (pageNumber < 1) {
            throw new IllegalArgumentException("pageNumber must be >= 1");
        }
        
        int sortIndex;
        if (sortColumn == null || sortColumn.isEmpty()) {
            sortIndex = -1;
        } else {
            sortIndex = getColumnNames().indexOf(sortColumn);
            if (sortIndex < 0) {
                throw new IllegalArgumentException("Sort column not found: "+sortColumn);
            }
        }
        
        String address = getHTMLPrefix();
        if (sortIndex >= 0) {
            String key = reverseSort ? "R" : "S";
            address += "_"+key+sortIndex;
        }
        address += "_"+pageNumber+".html";
        
        for (int i=0; i<ascendLevels; i++) address = "../"+address;
        return address;
    }
    
    public String getRecordDirectory() {
        return getHTMLPrefix()+"_rec";
    }
    
    public String getIDMapDirectory() {
        return getHTMLPrefix()+"_idmap";
    }
    
    public String getRecordAddress(int recordID, int ascendLevels) {
        final String key = getKeyValue(recordID);
        return getRecordAddress(key, ascendLevels);
    }

    public String getRecordAddress(String keyValue, int ascendLevels) {
        if (keyValue != null) {
            keyValue = keyValue.replaceAll("[^a-zA-Z0-9_-]", "_");
        }
        String address = getRecordDirectory()+"/"+keyValue+".html";
        for (int i=0; i<ascendLevels; i++) address = "../"+address;
        return address;
    }
    
    public String getAttributeDetailsAddress() {
    	return "index_details.html#" + getName();
    }

    public static int idHashCode(String id) {
        int hash = 0;
        for (int i=0; i<id.length(); i++) {
            hash = (31*hash + id.charAt(i)) % Table.HASH_MODULUS;
        }
        return hash;
    }
    
    // Formatting //////////////////////////////////////////////////////
    
    public String formatNumber(final double value) {
        String formatted = String.format(Locale.ENGLISH, this.numberFormat, value);
        formatted = formatted.replace("e-0", "e-");
        formatted = formatted.replace("e+0", "e");
        return formatted;
    }
    
    public String formatSortKey(String sortColumn, boolean reverseSort) {
        if (sortColumn == null || sortColumn.isEmpty()) return "unsorted";
        else return sortColumn + (reverseSort ? " (descending)" : " (ascending)");
    }
    
    public String formatContents(int recordID, String column, int ascendLevels, boolean brief, boolean home) {
        int columnIndex = columnNames.indexOf(column);
        if (columnIndex < 0) {
            throw new IllegalArgumentException("Column not found: "+column);
        }
        
        final boolean isShowColumn = column.equals(getShowColumn());
        String content = isShowColumn ? getShowValue(recordID) : getRow(recordID).get(columnIndex);
        final String urlPattern = refs.get(column);
        
        if (!home && isShowColumn && content != null) {
            String s = String.format("<a href=\"%s\">%s</a>",
                    getRecordAddress(recordID, ascendLevels), content);
            if (!brief && urlPattern != null) {
                String url = urlPattern.replace("$ID$", content);
                s += String.format(" (<a href=\"%s\" class=\"external\">external</a>)", url);
            }
            return s;
        } else {
            Table foreign = getForeignKey(column);
            
            if (foreign != null && content != null) {
                StringBuffer sb = new StringBuffer();
                for (String id: content.split(",")) {
                    if (id == null) continue;
                    id = id.trim();
                    if (id.isEmpty()) continue;
                    if (sb.length() > 0) sb.append(" ");
                    if (foreign.hasKey(id)) {
                        sb.append(String.format("<a href=\"%s\">%s</a>",
                            foreign.getRecordAddress(id, ascendLevels),
                            foreign.getShowValue(id)));
                    } else {
                        sb.append(id);
                    }
                }
                if (sb.length() == 0) sb.append(this.missingValue);
                return sb.toString();
            } else if (urlPattern != null && content != null) {
                StringBuffer sb = new StringBuffer();
                for (String id: content.split(",")) {
                    if (id.isEmpty()) continue;
                    String url = urlPattern.replace("$ID$", id);
                    if (sb.length() > 0) sb.append(" ");
                    sb.append(String.format(
                            "<a href=\"%s\" class=\"external\">%s</a>",
                            url, id));
                }
                return sb.toString();
            } else {
                if (content == null || content.isEmpty()) {
                    return this.missingValue;
                }
                if (this.integerColumns.contains(column)) {
                    try {
                        Integer.parseInt(content);
                        return content; // Return integer as is 
                    } catch (NumberFormatException e1) {
                        // pass
                    }
                }
                try {
                    double numeric = Double.parseDouble(content);
                    String formatted = formatNumber(numeric); 
                    if (Math.rint(numeric) != numeric) {
                        return String.format(
                                "<span title=\"%s\">%s</div>",
                                numeric, formatted);
                    } else {
                        return formatted;
                    }
                } catch (NumberFormatException e) {
                    return content;   
                }
            }
        }
    }
    
    public String formatContents(int recordID, String column, int ascendLevels) {
        return formatContents(recordID, column, ascendLevels, true, false);
    }
    
    public String getShortColumnName(String column) {
        final int pos = column.indexOf(':');
        if (pos < 0) return column;
        else return column.substring(pos+1); 
    }
    
    public List<ColumnRange> getColumnRanges() {
        List<ColumnRange> ranges = new ArrayList<ColumnRange>();
        ColumnRange curRange = null;
        for (int col=0; col<getSummaryColumns().size(); col++) {
            String column = getSummaryColumns().get(col);
            final int sep = column.indexOf(':');
            final String label = sep < 0 ? null : column.substring(0, sep);
            if (curRange == null) {
                curRange = new ColumnRange(col, col, label);
                ranges.add(curRange);
            } else {
                boolean same = label == null ? curRange.label == null : label.equals(curRange.label);
                if (same) curRange.endPos++;
                else {
                    curRange = new ColumnRange(col, col, label);
                    ranges.add(curRange);
                }
            }
        }
        
        if (ranges.size() == 1 && ranges.get(0).label == null) {
            ranges.remove(0);
        }
        return ranges;
    }
    
    private String formatColorRangeLabel(String color, String label, double limit) {
        if (label != null && label.isEmpty()) label = null;
        if (label == null) {
            return String.format(
                    "<span style=\"background-color: %s\">%.2g </span>",
                    color, limit);
        } else {
            return String.format(
                    "<span style=\"background-color: %s\">%s (%.2g) </span>",
                    color, label, limit);
        }
    }
    
    public String formatColorRangeLabels(String column, List<Color> colorPalette) {
        ColorRange range = getColorRange(column);
        if (range == null) return "";
        
        final String low = formatColorRangeLabel(getColor(range, colorPalette, range.low),
                range.getLowLabel(""), range.getLowLimit(true));
        final double middleValue = (range.low + range.high) / 2.0;
        final String middle = formatColorRangeLabel(getColor(range, colorPalette, middleValue),
                range.getMiddleLabel(""), range.getMiddleLimit(true));
        final String high = formatColorRangeLabel(getColor(range, colorPalette, range.high),
                range.getHighLabel(""), range.getHighLimit(true));
        return low + middle + high;
    }
    
    // Accessing content ///////////////////////////////////////////////    
    
    public void addRow(List<String> row) {
        final int id = this.content.size();
        this.content.add(row);
        this.recordID2Physical.add(id);
        this.physical2RecordID.add(id);
        
        // Check if the row contains non-integer values and if yes,
        // remove the corresponding columns from the set of
        // integer-only columns.
        Iterator<String> iter = this.integerColumns.iterator();
        while (iter.hasNext()) {
            final String column = iter.next();
            final int colIndex = this.columnNames.indexOf(column);
            final String value = row.get(colIndex);
            if (value == null) continue;
            try {
                Integer.parseInt(value);
            } catch (NumberFormatException e) {
                iter.remove();
            }
        }
    }
    
    public int last() {
        return this.content.size() - 1;
    }
    
    public int size() {
        return this.content.size();
    }
    
    public List<String> getRow(int recordID) {
        return this.content.get(this.recordID2Physical.get(recordID));
    }

    public int[] getRecordIDs(int fromPhysicalIndex, int toPhysicalIndex) {
        return this.physical2RecordID.subList(fromPhysicalIndex, toPhysicalIndex).toArray();
    }
    
    public String getKeyValue(int recordID) {
        final int keyIndex = this.columnNames.indexOf(getKeyColumn());
        final String key = getRow(recordID).get(keyIndex);
        if (key != null) return key;
        else return getValue(recordID, getShowColumn());
    }
    
    public String getShowValue(int recordID) {
        String value = getValue(recordID, getShowColumn());
        if (value != null) return value;
        else return getKeyValue(recordID);
    }

    public String getShowValue(String keyValue) {
        Integer recordID = getRecordID(keyValue);
        if (recordID == null) return null;
        else return getShowValue(recordID);
    }
    
    public String getValue(int recordID, String columnName) {
        final int index = getColumnNames().indexOf(columnName);
        if (index < 0) return null;
        else return getRow(recordID).get(index);
    }
    
    public Integer getRecordID(String keyValue) {
        return this.key2RecordID.get(keyValue);
    }
    
    public boolean hasKey(String keyValue) {
        return this.key2RecordID.get(keyValue) != null;
    }
    
    public void sort(String columnName, boolean reverse) {
        
        final int index = getColumnNames().indexOf(columnName);
        if (index < 0) {
            throw new IllegalArgumentException("Sort column not found: "+columnName);
        }
        
        double[] numericCache = new double[this.size()];
        final Pattern pattern = Pattern.compile("^[.0-9+-]");
        for (int i=0; i<this.size(); i++) {
            List<String> row = this.content.get(i);
            final String value = row.get(index);
            if (value != null && pattern.matcher(value).find()) {
                try {
                    numericCache[i] = Double.parseDouble(value);
                } catch (NumberFormatException e) {
                    numericCache[i] = Double.NaN;
                }
            } else {
                numericCache[i] = Double.NaN;
            }
        }

        List<Integer> newPositions = new ArrayList<Integer>();
        for (int i=0; i<this.size(); i++) {
            newPositions.add(i);
        }
        
        RowComparator comp = new RowComparator(this.content, this.physical2RecordID, index, reverse, numericCache);
        Collections.sort(newPositions, comp);
        
        List<List<String>> oldContent = new ArrayList<List<String>>(this.content);
        ArrayIntList oldPhysical2RecordID = new ArrayIntList(this.physical2RecordID);
        // Reorder data structures that represent the physical structure
        for (int physical=0; physical<this.size(); physical++) {
            final int newPhysical = newPositions.get(physical);
            final int newRecordID = oldPhysical2RecordID.get(newPhysical);
            this.content.set(physical, oldContent.get(newPhysical));
            this.physical2RecordID.set(physical, newRecordID);
            this.recordID2Physical.set(newRecordID, physical);
        }
    }
    
    private void updateKeyIndex() {
        if (this.key2RecordID != null) return;
        
        this.key2RecordID = new HashMap<String, Integer>();
        final int keyColumnIndex = this.columnNames.indexOf(getKeyColumn());
        final int showColumnIndex = this.columnNames.indexOf(getShowColumn());
        
        for (int physical=0; physical<this.content.size(); physical++) {
            List<String> row = this.content.get(physical);
            String key = row.get(keyColumnIndex);
            if (key == null) key = row.get(showColumnIndex);
            final int recordID = this.physical2RecordID.get(physical);
            this.key2RecordID.put(key, recordID);
        }   
    }
    
    private void updateForeignIndices() {
        updateKeyIndex();
        if (this.foreignIndex != null) return;
        
        this.foreignIndex = new HashMap<Table,Map<Integer,ArrayIntList>>();
        if (this.foreignKeys.isEmpty()) return;
        
        for (Table foreignTable: this.foreignKeys.values()) {
            foreignTable.updateKeyIndex();
        }

        for (int physical=0; physical<this.content.size(); physical++) {
            List<String> row = this.content.get(physical);
            final int recordID = this.physical2RecordID.get(physical);
            
            for (Map.Entry<String,Table> entry: this.foreignKeys.entrySet()) {
                final String foreignColumn = entry.getKey();
                final int foreignIndex = getColumnNames().indexOf(foreignColumn);
                if (foreignIndex < 0) continue;
                
                final String value = row.get(foreignIndex);
                if (value == null) continue;
                final Table foreignTable = entry.getValue();
                
                Map<Integer,ArrayIntList> idMap = this.foreignIndex.get(foreignTable);
                if (idMap == null) {
                    idMap = new HashMap<Integer, ArrayIntList>();
                    this.foreignIndex.put(foreignTable, idMap);
                }
                
                for (String foreignKey: value.split(",")) {
                    if (foreignKey != null) {
                        foreignKey = foreignKey.trim();
                        if (foreignKey.isEmpty()) continue;
                        final Integer foreignRecordID = foreignTable.getRecordID(foreignKey);
                        if (foreignRecordID != null) {
                            ArrayIntList indices = idMap.get(foreignRecordID);
                            if (indices == null) {
                                indices = new ArrayIntList();
                                idMap.put(foreignRecordID, indices);
                            }
                            if (!indices.contains(recordID)) {
                                // .contains() can be slow if there are lots of IDs
                                indices.add(recordID);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void updateIndices() {
        updateKeyIndex();
        updateForeignIndices();
    }
    
    // Links to other tables ///////////////////////////////////////////    
    
    public void addForeignKey(String column, Table table) {
        foreignKeys.put(column, table);
        table.addReverseLink(this, column);
    }
    
    private void addReverseLink(Table fromTable, String fromColumn) {
        reverseLinks.put(fromTable, fromColumn);
    }
    
    public Table getForeignKey(String column) {
        return foreignKeys.get(column);
    }
    
    public int[] getForeignRecordIDs(Table fromTable, String keyValue) {
        updateForeignIndices();
        final Map<Integer,ArrayIntList> idMap = this.foreignIndex.get(fromTable);
        if (idMap == null) return new int[] {};
        final int recordID = fromTable.getRecordID(keyValue);
        final ArrayIntList ids = idMap.get(recordID);
        return (ids == null) ? new int[] {} : ids.toArray();
        
    }
    
    public Map<Table,String> getReverseLinks() {
        return this.reverseLinks;
    }
    
    // Images //////////////////////////////////////////////////////////
    
    public void addImageMapping(String targetID, String imageFile, String label) {
        List<Image> images = this.imageMap.get(targetID);
        if (images == null) {
            images = new ArrayList<Image>();
            this.imageMap.put(targetID, images);
        }
        images.add(new Image(imageFile, label));
    }
    
    public List<Image> getImages(int recordID) {
        List<Image> images = this.imageMap.get(getKeyValue(recordID));
        return (images == null) ? new ArrayList<Image>() : images;
    }
    
    public String formatImageTag(Image image, int ascendLevels) {
        final String RE = ".*[.](bmp|gif|jpeg|jpg|png)";
        final String imageRef = image.getImageFile();
        boolean isImage = imageRef.toLowerCase().matches(RE);
        String label = image.getImageLabel();
        final boolean hasLabel = label != null && !label.isEmpty();
        
        String actualRef = imageRef;
        if (ascendLevels > 0) {
            actualRef = getRecordDirectory()+"/"+actualRef;
            for (int i=0; i<ascendLevels; i++) actualRef = "../"+actualRef;
        }
        
        if (isImage) {
            String img = String.format("<img src=\"%s\" />", actualRef);
            if (hasLabel) return label+" "+img;
            else return img; 
        }
        else {
            if (!hasLabel) label = imageRef;
            return String.format("<a href=\"%s\">%s</a>", actualRef, label);
        }
    }
    
    // Matrices ////////////////////////////////////////////////////////
    
    public void addMatrixColumnLink(Matrix matrix) {
        this.matrixColumnLinks.add(matrix);
    }

    public void addMatrixRowLink(Matrix matrix) {
        this.matrixRowLinks.add(matrix);
    }
    
    public Set<Matrix> getMatrixColumnLinks() {
        return this.matrixColumnLinks;
    }
    
    public Set<Matrix> getMatrixRowLinks() {
        return this.matrixRowLinks;
    }
    
    // Colors //////////////////////////////////////////////////////////
    
    public void addColorRange(String column, double low, double high, boolean logarithm) {
        this.colorRanges.put(column, new ColorRange(low, high, logarithm));
    }
    
    public ColorRange getColorRange(String column) {
        return this.colorRanges.get(column);
    }
    
    public String getColor(ColorRange range, List<Color> colorPalette, double value) {
        if (range == null) return null;
        return Color.chooseColor(colorPalette, value, range.low, range.high).toHex();
    }
    
    public String getColor(int recordID, String column, List<Color> colorPalette) {
        ColorRange range = this.colorRanges.get(column);
        if (range == null) return null;
        
        String value = getValue(recordID, column);
        if (value == null) return null;
        double numeric;
        try {
            numeric = Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return null;
        }
        
        final double LOG2 = Math.log(2);
        if (range.logarithm) numeric = Math.log(numeric) / LOG2;
        return getColor(range, colorPalette, numeric);
    }
    
    public boolean hasColorRanges() {
        return !this.colorRanges.isEmpty();
    }
    
    // Other ///////////////////////////////////////////////////////////
    
    public void addURLReferences(String column, String urlPattern) {
        this.refs.put(column, urlPattern);
    }
    
    public static int arrayLength(int[] array) {
        return array.length;
    }
}

