function hashcode(str) {
	h = 0;
	for (var i=0, limit=str.length; i<limit; i++) {
		h = (31*h + str.charCodeAt(i)) % 256;
	}
	hs = h.toString(16);
	if (hs.length == 1) hs = "0" + hs;
	return hs
}

function do_submit(input_id, idmap_dir, record_dir, message_id) {
	var id = document.getElementById(input_id).value;
	// Trim whitespace & normalize case
	id = id.replace(/^\s+|\s+$/g,"").toLowerCase();
	var hash = hashcode(id);
	var url = idmap_dir + "/" + hash;
	var request = new XMLHttpRequest();
	var message_element = document.getElementById(message_id)
	message_element.innerHTML = "";
	request.open("GET", url, true);
	request.onreadystatechange = function(event) {
		if (request.readyState == 4) {
			var text = request.responseText;
			var error_msg = "Query entry not found: "+id;
			if (text == null) {
				message_element.innerHTML = error_msg;
			} else {
				var pos = text.indexOf("\n"+id+"\t");
				if (pos < 0) {
					message_element.innerHTML = error_msg;
				} else {
					var end_pos = text.indexOf("\n", pos+1);
					var target = text.substring(pos+id.length+2, end_pos);
					location.assign(target);
				}
			}
		}
	};
	request.send(null);
	return false; // When called from form.onsubmit, cancel submit
}

function do_summary_submit(text_input_id, url_pattern, cur_page, max_page) {
	var page = parseInt(document.getElementById(text_input_id).value, 10);
	if (isNaN(page)) page = cur_page;
	if (page > max_page) page = max_page;
	if (page < 1) page = 1;
	if (page != cur_page) {
		url = url_pattern.replace("999999", page);
		location.assign(url);
	}
	return false;
}
