library(componentSkeleton)
library(gaia)

execute <- function(cf) {
    # Parameters
    delSym    <- get.parameter(cf, 'delSymbol',   type='string')
    ampSym    <- get.parameter(cf, 'ampSymbol',   type='string')
    p.limit   <- get.parameter(cf, 'pLmit',       type='float')
    annotCols <- get.parameter(cf, 'annotCols',   type='string')
    segCols   <- get.parameter(cf, 'segmentCols', type='string')
    useAppro  <- get.parameter(cf, 'useApprox',   type='boolean')

    annotCols <- unlist(strsplit(annotCols, ','))
    segCols   <- unlist(strsplit(segCols, ','))

    # Inputs
    segMatrix  <- CSV.read(get.input(cf, 'segments'))
    annotation <- CSV.read(get.input(cf, 'probeAnnotation'))

    segMatrix  <- segMatrix[,segCols]
    annotation <- annotation[,annotCols]
    segTmp <- segMatrix

    segMatrix[-which(as.character(segTmp[,segCols[6]]) %in% c(delSym,ampSym)), segCols[6]] <- 0
    segMatrix[ which(as.character(segTmp[,segCols[6]]) == delSym), segCols[6]]             <- 1
    segMatrix[ which(as.character(segTmp[,segCols[6]]) == ampSym), segCols[6]]             <- 2

    annotation[which(annotation[,annotCols[2]] == 'X'),annotCols[2]] <- 23
    annotation[which(annotation[,annotCols[2]] == 'Y'),annotCols[2]] <- 24

    nroSamples <- length(unique(segMatrix[,segCols[1]]))

    # Gaia
    annotIn <- load_markers(annotation)
    segsIn  <- load_cnv(segMatrix, annotIn, nroSamples)

    # run GAIA with 5000 permutations
    write.log(cf, 'Starting calling alterations with GAIA...')
    set.seed(1295167)
    results <- runGAIA(segsIn, annotIn,
                       output_file_name = get.output(cf, 'regions'), 
                       num_iterations   = 5000,
                       #hom_threshold    = -1,
                       approximation    = useAppro)
    results[which(results[,'Aberration Kind'] == 1),'Aberration Kind'] <- -1
    results[which(results[,'Aberration Kind'] == 2),'Aberration Kind'] <-  1
    colnames(results) <- c(segCols[2], segCols[6], segCols[3:4], "bpLength", "qValue")
    CSV.write(get.output(cf, 'regions'), results, first.cell="ID")
    write.log(cf, '...finished GAIA.')
}

main(execute)
