library(componentSkeleton)

create.gene.frame <- function(cf, ids, group.id, expr,
        annotation, groups, sample.annotation, id.convert.column,
        gene.annotation.columns, sample.annotation.columns) {
    if (!(group.id %in% colnames(expr)) && !is.null(groups)) {
        expr.group.ids <- SampleGroupTable.get.source.groups(groups, group.id)
    }
    else {
        expr.group.ids <- group.id
    }
    
    matches <- match(expr.group.ids, colnames(expr))
    if (any(is.na(matches))) {
        bad <- expr.group.ids[is.na(matches)]
        stop(paste('Group not found in expr:', paste(bad, collapse=', '))) 
    }

    expr.matches <- match(ids, rownames(expr))
    if (!get.parameter(cf, 'allowMissingExpr', 'boolean')) {
        if (any(is.na(expr.matches))) {
            bad <- ids[is.na(expr.matches)]
            stop(paste('Gene IDs not found in expr:', paste(bad, collapse=', ')))
        }
    }
    
    if (nchar(id.convert.column) > 0) {
        table.ids <- AnnotationTable.get.vector(annotation, id.convert.column, ids)
        missing.ids <- is.na(table.ids)
        if (any(missing.ids)) table.ids[missing.ids] <- ids[missing.ids]
        names(table.ids) <- NULL
    }
    else {
        table.ids <- ids
    }
    
    fr <- data.frame(ID=table.ids, stringsAsFactors=FALSE)
    colnames(fr)[1] <- 'ID'
    
    order.column <- get.parameter(cf, 'sortColumn')
    if (nchar(order.column) == 0) order.column <- NULL
    
    column.classes <- split.trim(get.parameter(cf, 'columnOrder'), ',')
    for (column.class in column.classes) {
        if (column.class == 'expr') {
            expr.label <- get.parameter(cf, 'expressionLabel')
            
            if (length(expr.group.ids) > 1) {
                if (length(ids) > 0) {
                    all.expr <- expr[expr.matches, expr.group.ids, drop=FALSE]
                    med.expr <- sapply(1:nrow(all.expr), function(i) 2^median(all.expr[i,], na.rm=TRUE))
                } else {
                    med.expr <- numeric()
                }
                col.title <- sprintf('%s (median)', expr.label)
                fr[[col.title]] <- med.expr
                if (is.null(order.column)) order.column <- col.title
            }
            for (this.group.id in expr.group.ids) {
                gene.expr <- 2^expr[expr.matches, this.group.id]
                if (length(expr.group.ids) > 1) col.title <- sprintf('%s (%s)', expr.label, this.group.id)
                else col.title <- expr.label
                fr[[col.title]] <- gene.expr
                
                if (is.null(order.column)) order.column <- col.title
            }
        }
        else if (column.class == 'geneAnnotation') {
            if (!is.null(annotation)) {
                for (col.name in gene.annotation.columns) {
                    fr[[col.name]] <- AnnotationTable.get.vector(annotation, col.name, ids)
                }
            }
        }
        else if (column.class == 'sampleAnnotation') {
            if (!is.null(sample.annotation)) {
                good <- sample.annotation[,1] == group.id
                id.col <- colnames(sample.annotation)[2]
                for (col.name in sample.annotation.columns) {
                    sub.ann <- sample.annotation[good, c(id.col, col.name)]
                    values <- AnnotationTable.get.vector(sub.ann, col.name, ids)
                    fr[[col.name]] <- values
                }
            }
        }
        else {
            stop(paste('Invalid columnOrder entry:', column.class))
        }
    }
    
    if (!is.null(order.column)) {
        reverse <- (max(fr[[order.column]], na.rm=TRUE) > 1)
        fr <- fr[order(fr[[order.column]], decreasing=reverse),]
    }
    
    return(fr)
}

execute <- function(cf) {
    idlist <- SetList.read(get.input(cf, 'idlist'))
    
    expr <- NULL
    if (input.defined(cf, 'expr')) {
        expr <- LogMatrix.read(get.input(cf, 'expr'))
    }
    
    if (input.defined(cf, 'geneAnnotation')) {
        gene.annotation <- AnnotationTable.read(get.input(cf, 'geneAnnotation'))
        gene.annotation.columns <- split.trim(get.parameter(cf, 'annotationColumns'), ',')
        if (identical(gene.annotation.columns, '*')) {
            gene.annotation.columns <- colnames(gene.annotation)[2:ncol(gene.annotation)]
        }
    } else {
        gene.annotation <- NULL
        gene.annotation.columns <- NULL
    }

    if (input.defined(cf, 'sampleAnnotation')) {
        sample.annotation <- CSV.read(get.input(cf, 'sampleAnnotation'))
        sample.annotation.columns <- split.trim(get.parameter(cf, 'sampleAnnotationColumns'), ',')
        if (identical(sample.annotation.columns, '*')) {
            sample.annotation.columns <- colnames(sample.annotation)[3:ncol(sample.annotation)]
        }
    } else {
        sample.annotation <- NULL
        sample.annotation.columns <- NULL
    }

    groups <- NULL
    if (input.defined(cf, 'groups')) {
        groups <- SampleGroupTable.read(get.input(cf, 'groups'))
    }
    
    id.convert.column <- get.parameter(cf, 'idConvertColumn')
    
    out.dir <- get.output(cf, 'csvfiles')
    if (!file.exists(out.dir)) dir.create(out.dir, recursive=TRUE)
    
    set.pattern <- get.parameter(cf, 'setPattern')
    
    for (i in 1:nrow(idlist)) {
        set.id <- idlist$ID[i]
        if (nchar(set.pattern) > 0 && length(grep(set.pattern, set.id)) == 0) {
            next
        }
        group.id <- idlist[i, 'SampleGroup']
        if (is.null(group.id)) group.id <- get.parameter(cf, 'groupID')
        ids <- SetList.get.members(idlist, set.id)
        
        if (!is.null(ids)) {
            filename <- file.path(out.dir, sprintf('%s.csv', set.id))
            gene.frame <- create.gene.frame(cf, ids, group.id, expr,
                gene.annotation, groups, sample.annotation,
                id.convert.column, gene.annotation.columns, sample.annotation.columns)
            CSV.write(filename, gene.frame)
        }
    }
    
    if (has.errors()) return(INVALID_INPUT)
    else return(0)
}

main(execute)
