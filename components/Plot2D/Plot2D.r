library(componentSkeleton)

LIMIT.SPECIAL <- -0.000001

execute <- function(cf) {
    width.cm     <- get.parameter(cf, 'width', 'float')
    height.cm    <- get.parameter(cf, 'height', 'float')
    common.scale <- get.parameter(cf, 'commonScale')
    y.low        <- get.parameter(cf, 'yLow', 'float')
    y.high       <- get.parameter(cf, 'yHigh', 'float')
    x.low        <- get.parameter(cf, 'xLow', 'float')
    x.high       <- get.parameter(cf, 'xHigh', 'float')
    vpInf        <- get.parameter(cf, 'pInf', 'float')
    vnInf        <- get.parameter(cf, 'nInf', 'float')
    plotType     <- get.parameter(cf, 'plotType')
    pngImage     <- get.parameter(cf, 'pngImage')
    imageType    <- get.parameter(cf, 'imageType')
    leg.pos      <- get.parameter(cf, 'legendPosition')
    do.sort      <- get.parameter(cf, 'sort', 'boolean')
    draw.x       <- get.parameter(cf, 'drawX', 'boolean')
    dpcm         <- get.parameter(cf, 'dpCm', 'float')
    myName       <- get.metadata(cf, 'instanceName')
	
    width.pixels <- dpcm * width.cm
	
    if (width.cm == 0) {
        write.error(cf, 'Parameter "width" is 0')
        return(INVALID_INPUT)
    }
    if (height.cm == 0) {
        write.error(cf, 'Parameter "height" is 0')
        return(INVALID_INPUT)
    }
    if (!(plotType %in% c('p','l','h','bar','step','hist'))) {
        write.error(cf,paste('plotType',plotType,'not recognized'))
        return(INVALID_INPUT)
    }
	#width.inches  <- width.cm * 0.3937008
	#height.inches <- height.cm * 0.3937008
	
    #aspect        <- width.cm / height.cm
    #height.pixels <- as.integer(round(width.pixels / aspect))
	
    out.dir <- get.output(cf, 'plot')
    dir.create(out.dir, recursive=TRUE)

    tex <- character()
    section.title <- get.parameter(cf, 'sectionTitle')
    section.type <- get.parameter(cf, 'sectionType')
    if (nchar(section.type) > 0) {
        # if section type is defined, add escape in the beginning
        section.type=paste('\\',section.type,sep='')
    }
    if (nchar(section.title) > 0) {
        tex <- c(tex, sprintf('%s{%s}\\label{%s}',
                              section.type,
                              section.title,
                              myName))
    }

    y <- CSV.read(get.input(cf, 'y'))
	
    y.columns <- split.trim(get.parameter(cf, 'yColumns'), ',')
    if (identical(y.columns, '*')) y.columns <- colnames(y)
    if (nrow(y) < 1) {
       tex <- c(tex, "No Y-values available!")
       latex.write.main(cf, 'plot', tex)
       return(0)
    }

    if (input.defined(cf, 'x')) {
    
        x <- CSV.read(get.input(cf, 'x'))
        x.columns <- split.trim(get.parameter(cf, 'xColumns'), ',')
        if (identical(x.columns, '*')) x.columns <- colnames(x)
        if ((plotType=='bar') && (length(x.columns)==1)) {
            x.columns <- rep(x.columns,length(y.columns))
        }
        if (length(x.columns) != length(y.columns)) {
            write.error(cf, 'xColumns and yColumns must have the same number of items')
            return(PARAMETER_ERROR)
        }
        if ((nrow(x) != nrow(y)) && plotType!='hist') {
                write.error(cf, 'x and y must have the same number of rows')
                return(INVALID_INPUT)
        }
        if ((nrow(x) != nrow(y)+1) && plotType=='hist') {
                write.error(cf, 'x must have one row more than y')
                return(INVALID_INPUT)
        }
        
    } else {
        x.columns <- split.trim(get.parameter(cf, 'xColumns'), ',')
        if (!identical(x.columns, '*')) write.log(cf, sprintf('Warning: xColumns set, but input port x not defined!'))
        x         <- NULL
        x.columns <- NULL
    }

	
    skip <- integer()
    for (i in 1:length(y.columns)) {
        if (!is.numeric(y[,y.columns[i]])) {
            write.log(cf, sprintf('Skipping column %s in y: not numeric', y.columns[i]))
            skip <- c(skip, i)
            next
        }
        if (!is.null(x)) {
            if (plotType!='bar'){
            if (!is.numeric(x[,x.columns[i]])) {
                write.log(cf, sprintf('Skipping column %s in x: not numeric', x.columns[i]))
                skip <- c(skip, i)
                next
            }
            }
        }
    }
    if (length(skip) > 0) y.columns <- y.columns[-skip]
    y <- y[,y.columns,drop=FALSE]
    y.transform <- trim(get.parameter(cf, 'yTransformation'))
    if (nchar(y.transform) > 0) {
        y <- eval(parse(text=y.transform))
    }
    y[y== Inf] <- vpInf
    y[y==-Inf] <- vnInf
    min.y <- min(y, na.rm=TRUE)
    max.y <- max(y, na.rm=TRUE)

    if ((!is.null(x)) && (plotType!='bar')) {
        if (length(skip) > 0) x.columns <- x.columns[-skip]
        x <- x[,x.columns,drop=FALSE]
        if (!is.null(x)) {
            x.transform <- trim(get.parameter(cf, 'xTransformation'))
            if (nchar(x.transform) > 0) {
                x <- eval(parse(text=x.transform))
            }
        }
        x[x== Inf] <- vpInf
        x[x==-Inf] <- vnInf
        min.x <- min(x, na.rm=TRUE)
        max.x <- max(x, na.rm=TRUE)
    } else {
        min.x <- NULL
        max.x <- NULL
    }

    if (input.defined(cf, 'labels')) {
        labels <- CSV.read(get.input(cf, 'labels'))
        
        if(get.parameter(cf, 'labelColumn') == ''){
                labels <- labels[,1]
        }else{
                labels <- labels[, get.parameter(cf, 'labelColumn')]
        }
        
        if (length(labels) != length(y.columns)) {
                write.error(cf, 'yColumns and labels must have the same number of items')
                return(PARAMETER_ERROR)
        }
    } else {
        labels <- y.columns
    }
	
    #there might be same labels and they should be printed in same colors
    color.labels <- unique(labels)
    line.colors <- c('black', rainbow(length(color.labels)-1))
    
    if(get.parameter(cf, 'colorFunction') != ""){
        colorFunction <- sprintf(get.parameter(cf, 'colorFunction'), length(color.labels))
        line.colors <- eval(parse(text=colorFunction))
    }
	
    onePlot <- FALSE
    
    if (draw.x) xaxt <- 's'
    else xaxt <- 'n'
    
    ylab    <- get.parameter(cf, 'yLabel')
    main    <- get.parameter(cf, 'title')
    caption <- get.parameter(cf, 'caption')
    xlab    <- get.parameter(cf, 'xLabel')
    
    if(identical(imageType, 'single') || identical(imageType, 'pairs')) onePlot <- TRUE
    
    if(onePlot){
        name <- plot.image(width.cm, height.cm, width.pixels, pngImage, myName, 0, out.dir)
        tex <- c(tex, latex.figure(name, caption=caption, image.width=width.cm, fig.label=sprintf("%s-1", myName)))
    }
    
    if(identical(imageType, 'pairs')){
        #graphical parameters and ploting
        par(pch=20, cex=0.7)
        # User given free par()
        eval(parse(text=paste("par(",get.parameter(cf, 'plotPar'),")")))
        pairs( y[,y.columns], labels=y.columns, xaxt=xaxt, 
                main=main)
    } else { 
        plotnum <- 1
        for (i in 1:length(y.columns)) {
            j <- i
            y.column <- y[,y.columns[i]]
            if (do.sort) y.column <- sort(y.column)
            if (!is.null(x)) {
                x.column <- x[,x.columns[i]]
            } else {
                x.column <- c(1:length(y.column))
            }
            if(!onePlot){
                #use column names in titles etc, if each column is ploted in separate files
                ylab <- sprintf(get.parameter(cf, 'yLabel'), y.columns[i])
                if (is.null(x.columns)) {
                    main    <- sprintf(get.parameter(cf, 'title'), y.columns[i])
                    caption <- sprintf(get.parameter(cf, 'caption'), y.columns[i])
                    xlab    <- sprintf(get.parameter(cf, 'xLabel'), y.columns[i])
                } else {
                    main    <- sprintf(get.parameter(cf, 'title'), y.columns[i], x.columns[i])
                    caption <- sprintf(get.parameter(cf, 'caption'), y.columns[i], x.columns[i])
                    xlab    <- sprintf(get.parameter(cf, 'xLabel'), x.columns[i])
                }
                name <- plot.image(width.cm, height.cm, width.pixels, pngImage, myName, plotnum, out.dir)
                if ((plotnum %% 8) == 0){
                    tex <- c(tex, '\\clearpage')
                }
                tex <- c(tex, latex.figure(name, caption=caption, image.width=width.cm, fig.label=sprintf("%s-%d", myName, plotnum)))
                j <- 1
            }
            
            #graphical parameters and ploting
            par(pch=20, cex=0.7)
            # User given free par()
            eval(parse(text=paste("par(",get.parameter(cf, 'plotPar'),")")))
            color <- line.colors[color.labels == labels[j]]
            if (common.scale) {
                ylim <- compute.limits(y.column, y.low, y.high, min.y, max.y)
                xlim <- compute.limits(x.column, x.low, x.high, min.x, max.x)
            } else {
                ylim <- compute.limits(y.column, y.low, y.high, NA, NA)
                xlim <- compute.limits(x.column, x.low, x.high, NA, NA)
            }
            if (plotType=='bar') {
            # barplots
                if (common.scale) {
                    xlim <- compute.limits.bar(x.column, x.low, x.high, min.x, max.x)
                } else {
                    xlim <- compute.limits.bar(x.column, x.low, x.high, NA, NA)
                }
               if(j == 1){
                if (onePlot) {
                    barplot.names.arg<-x[,x.columns[1]]
                    barplot.height<-t(as.matrix(y))
                    barplot.col<-line.colors
                    write.log(cf, sprintf('Note: Limiting the x-values in a single plot is not supported.'))
                }else{
                    barplot.names.arg<-x.column[xlim[1]:xlim[2]]
                    barplot.height<-y.column[xlim[1]:xlim[2]]
                    barplot.col<-color
                }
                barplot(names.arg=barplot.names.arg, height=barplot.height,
                        main=main,  xaxt=xaxt,
                        xlab=xlab, ylab=ylab, xpd=FALSE,
                        xlim=NULL, ylim=ylim, col=barplot.col,beside=TRUE)
                }
            } else if (plotType=='hist') {
            # histogram plot
                h <- list(breaks=x.column,
                          counts=y.column, 
                          density=y.column,
                          mids=head(x.column,n=-1)+diff(x.column)/2,
                          xname=paste(x.column),
                          equidist=FALSE)
                attr(h,"class")<-"histogram"
                plot(h,
                        main=main,  xaxt=xaxt,
                        xlab=xlab, ylab=ylab, 
                        xlim=xlim, ylim=ylim, col=color, add=(j!=1))
            
            } else {
            # non-barplots
            if(j == 1){
                
                plot(x=x.column, y=y.column, 
                        t=plotType, main=main,  xaxt=xaxt,
                        xlab=xlab, ylab=ylab, 
                        xlim=xlim, ylim=ylim, col=color)
            } else {
                if(plotType == 'l'){
                    lines(x=x.column, y=y.column, col=color)
                } else {
                    points(x=x.column, y=y.column, t=plotType, col=color)
                }
            }
            } # end plot commands
            
            #add annotations to the plot
            if (input.defined(cf, 'plotAnnotator')) {
                if (is.null(x.columns)) { x.name <- NULL }
                else { x.name <- x.columns[i] }
                run.script(get.input(cf, 'plotAnnotator'), y.column, x.column, y.columns[i], x.name, cf)
            }
            if(!onePlot){
            if (leg.pos != 'off') {
                legend(x=leg.pos, legend=color.labels[i], fill='black',
                    text.col='black')
            }}
            if(!onePlot){
                if(pngImage){
                    png.close()
                }else{
                    dev.off()
                }
            }
            plotnum <- plotnum + 1
        }
    }

    if(onePlot){
        if (leg.pos != 'off') {
            legend(x=leg.pos, legend=color.labels, fill=line.colors[1:length(color.labels)], 
                    text.col='black')
        }
        if(pngImage){
            png.close()
        }else{
            dev.off()
        }
    }

    latex.write.main(cf, 'plot', tex)
    return(0)
}

compute.limits <- function(values, parameter.low, parameter.high, common.low, common.high) {
    if (is.null(values)) return(NULL)

    limits <- c(NA, NA)

    if (!identical(parameter.low, LIMIT.SPECIAL)) limits[1] <- parameter.low
    else if (is.numeric(common.low)) limits[1] <- common.low
    else limits[1] <- min(values, na.rm=TRUE)

    if (!identical(parameter.high, LIMIT.SPECIAL)) limits[2] <- parameter.high
    else if (is.numeric(common.high)) limits[2] <- common.high
    else limits[2] <- max(values, na.rm=TRUE)

    return(limits)
}
compute.limits.bar <- function(values, parameter.low, parameter.high, common.low, common.high) {
    if (is.null(values)) return(NULL)
    # The barplot requires limit indexes instead of actual values (for x-axis)
    limits <- compute.limits(values, parameter.low, parameter.high, common.low, common.high)
    limitsIndex <- c(NA, NA)
    if (length(which(values<limits[1]))==0) {
        limitsIndex[1]<-1
    } else {
        limitsIndex[1]<-tail(which(values<limits[1]),n=1)+1
    }
    if (length(which(values>limits[2]))==0) {
        limitsIndex[2]<-length(values)
    } else {
        limitsIndex[2]<-head(which(values>limits[2]),n=1)-1
    }
    # Incase automatic 
    if (identical(parameter.low, LIMIT.SPECIAL)) limitsIndex[1] <- 1
    if (identical(parameter.high, LIMIT.SPECIAL)) limitsIndex[2] <- length(values)
    return(limitsIndex)
}

run.script <- function(r.file, y.column, x.column, y.name, x.name, cf) {
    eval(parse(file=r.file))
}

plot.image <- function(width.cm, height.cm, width.pixels, png, instanceName, plotnum, out.dir){

    base.name <- sprintf('%s-%d', instanceName, plotnum)
    filename  <- file.path(out.dir, base.name)
    
    if(png){

        aspect        <- width.cm / height.cm
        height.pixels <- as.integer(round(width.pixels / aspect))
        
        png.open(paste(filename, '.png', sep=''), width=width.pixels, height=height.pixels)
            
    }else{

        width.inches  <- width.cm * 0.3937008
        height.inches <- height.cm * 0.3937008
        
        pdf(paste(filename, '.pdf', sep=''), paper='special', width=width.inches, height=height.inches)	
    }
    
    return(base.name)
}

main(execute)
