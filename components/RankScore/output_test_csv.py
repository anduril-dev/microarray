#!/usr/bin/python

import sys
import os
import random

try:
	num_samples=int(sys.argv[1])
	num_genes=int(sys.argv[2])
	header="GeneID\t"
	for i in range(num_samples):
		header+='"Sample%d"'%i
		if i!=num_samples-1:
			header+="\t"
	print(header)

	ranks=[]
	for i in range(num_samples):
		r=range(num_genes)
		random.shuffle(r)
		ranks.append(r)

	lines=""
	for i in range(num_genes):
		lines+='Gene%d\t'%i
		for j in range(num_samples):
			lines+='%d'%(ranks[j][i])
			if j!=num_samples-1:
				lines+="\t"
		lines+="\n"
	print(lines)

	f=open("classes.csv", "w")
	f.write("Class\n")
	for i in range(num_samples):
		f.write("%d\n"%random.randint(0,1))
	f.close()

except Exception, e:
	print("Usage: Number of samples and number of genes as parameter please.")
	print(e)
	
