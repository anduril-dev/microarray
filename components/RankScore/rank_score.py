#!/usr/bin/python

import component_skeleton.main
import csv
import os
import sys
import numpy

NA_constant = -1

def convert_nas(string):
	global NA_constant
	if "NA" in string:
		# Must be a number for efficiency in comparisons because we want a
		# numeric matrix and such that every rank is indifferent to it when
		# summed across respectively applied relative to < and > operations
		NA_constant-=1
		return NA_constant
	else:
		return float(string)

def safe_class_fraction(c, ranks, gene1, gene2):
	s=(float(len(c) - (ranks[gene1, c]==NA_constant).sum() ))
	s2=(float(len(c) - (ranks[gene2, c]==NA_constant).sum() ))
	if s==0 or s2==0:
		return float('nan')
	else:
		return 1./s

def msg(s):
	print(s)
	sys.stdout.flush()

msg("Parameters are %r"%(sys.argv, ))

def execute(cf):
	msg("RankScore executing.")

	scores = cf.get_output('scores')

	rank_file = cf.get_input('ranks')
	classes = cf.get_input('classes')
	
	genes = cf.get_input('genes')

	count_na = cf.get_parameter('count_na')
	if count_na=="true":
		count_na=True
	else:
		count_na=False

	msg ("\t - Reading rank matrix.")
	# Read rank matrix from the rank file
	reader = csv.reader(open(rank_file, 'r'), delimiter='\t')
	sample_names = reader.next()
	ranks = numpy.array([[convert_nas(col) for col in row[1:]] for row in reader if row])

	# Give a more descriptive name for number of genes
	n_genes = ranks.shape[0]

	msg ("\t - Reading gene names.")
	# Read gene names from the rank file
	reader = csv.reader(open(rank_file, 'r'), delimiter='\t')
	reader.next() # Skip header
	gene_names = [row[0] for row in reader if row]

	msg ("\t - Reading sample classes into an array.")
	# Read sample classes into an array
	reader = csv.reader(open(classes, 'r'), delimiter='\t')
	reader.next() # Skip header
	classes = [("".join(row)) for row in reader if row]
	class_names = list(set(classes))
	classes = [ class_names.index(c) for c in classes ] 

	msg ("\t - Reading genes of interest. Genes is %r"%genes)
	# Read genes of interest - otherwise all of genes are of interest
	if genes:
		msg ("\t - Reading genes of interest.")
		reader = csv.reader(open(genes, 'r'), delimiter='\t')
		reader.next() # Skip header
		genes = [row[0] for row in reader if row]

	# Form a gene range including indices for genes of interest
	if genes:
		gene_range = [gene_names.index(n) for n in genes] 
		msg ("\t - Set gene focus to a list of size %r"%gene_range)
	else:
		gene_range = range(n_genes)

	# Extract sample columns representing each class and their amounts
	c0 = numpy.array([i for i in range(len(classes)) if classes[i]==0])
	c1 = numpy.array([i for i in range(len(classes)) if classes[i]==1])

	msg ("\t - Determining normalization constants. Count NA: %r. Number of genes: %s"%(count_na, n_genes))
	if count_na:
		# Normalization constants for case where we don't reduce NAs from full class counts
		l0 = numpy.array(1./float(len(c0)))
		l1 = numpy.array(1./float(len(c1)))
	else:
		# Normalization constants for case where we reduce NAs from full class counts
		l0 = numpy.array( [ [ safe_class_fraction(c0, ranks, gene1, gene2) for gene1 in range(n_genes) ] for gene2 in range(n_genes) ] )
		l1 = numpy.array( [ [ safe_class_fraction(c1, ranks, gene1, gene2) for gene1 in range(n_genes) ] for gene2 in range(n_genes) ] )

	# Write class names to a CSV file so result may be interpreted
	f=open(cf.get_output('classes'), "w")
	f.write("Class\n")
	for c in class_names:
		f.write('"%s"\n'%c)
	f.flush()
	f.close()

	f=open(scores, "w")
	f.write("\t".join(["Gene"]+gene_names))
	f.write("\n")

	# Debug msgs
	'''
	f.write("count_na: %r"%count_na)
	f.write("Gene range: %r"%(gene_range,))
	f.write("l0: %r\n"%l0)
	f.write("l1: %r\n"%l1)
	f.write("\n")
	'''

	msg ("\t - Commencing main algorithm.")
	# Quadratic main algorithm ... Is it possible to optimize with e.g. dynamic programming?
	# At least there should be a symmetry between elements ij and ji...

	# Not sure if this "one liner" is a good idea but at least it avoids lots of function calls
	# It is basically just looping over the n_genes**2 matrix and calculating the score + printing it
	# line by line
	frm = "%.4f\t"*(n_genes-1) + "%.4f\n"

	msg("Has shape %r"%(ranks.shape,))
	if count_na: # In this case, the normalization constants are really constant
		[ f.write( ("%s\t"%gene_names[i] + frm)%tuple(
			[ ( ranks[i, c0] < ranks[j, c0] ).sum()*l0 - ( ranks[i, c1] < ranks[j, c1] ).sum()*l1 
				for j in range(n_genes) ]
					)) for i in gene_range ]
	else: # In this case, the normalization constants vary based on number of NAs in given row.
		[ f.write( (("%s\t"%gene_names[i] + frm)%tuple(
			[ ( ranks[i, c0] < ranks[j, c0] ).sum()*l0[i,j] - ( ranks[i, c1] < ranks[j, c1] ).sum()*l1[i, j]
				for j in range(n_genes) ]
					)).replace('nan', 'NA') ) for i in gene_range ]

	f.flush()
	f.close()

	# Would work in newer numpy - right now can't get a header... so writing by hand
	# numpy.savetxt(scores, m, delimiter="\t", header="\t".join(["Gene%d"%d for d in range(n_genes)]), newline="\n")

	return 0

component_skeleton.main.main(execute)

