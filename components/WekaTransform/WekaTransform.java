import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import weka.core.Instances;
import weka.core.OptionHandler;
import weka.filters.Filter;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.javatools.weka.WekaDataUtils;

/**
 * A wrapper for Weka filters for the CSBL pipeline.
 * 
 * @author Viljami Aittom&aulm;ki
 * @version 0.3
 */
public class WekaTransform extends SkeletonComponent {

    public static final String PARAMETER_FILTER =           "filter";
    public static final String PARAMETER_WEKA_PARAMETERS =  "wekaParameters";
    public static final String INPUT_DATA =                 "data";
    public static final String OUTPUT_DATA =                "transformedData";
    public static final String CLASS_ATTRIBUTE =            "classAttribute";
    public static final String INPUT_TEMPORARY_INPUT =      "tmpinput";
    public static final String OUTPUT_TEMPORARY_OUTPUT =    "tmpoutput";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        String filterName = cf.getParameter(PARAMETER_FILTER);
        String wekaParameters = cf.getParameter(PARAMETER_WEKA_PARAMETERS);
        
        File outputFile = cf.getOutput(OUTPUT_DATA);
        File inputFile = cf.getInput(INPUT_DATA);
        
        File tmpInputFile = File.createTempFile("wekaTransform", "tmp");
        File tmpOutputFile = File.createTempFile("wekaTransform", "tmp");
        
        String classAtrr = cf.getParameter(CLASS_ATTRIBUTE);
        


        // Clean CSV and store property line
        String propertyLine = WekaDataUtils.andurilToWeka(inputFile,
                tmpInputFile);
        
        //Load input
        Instances data = WekaDataUtils.readCSV(tmpInputFile, classAtrr);
        
        
        // Initialize filter
        Filter filter;
        
        try {
        	
            filter = (Filter) Class.forName(filterName).newInstance();
        } catch (ClassNotFoundException e) {
            cf.writeError("Unsupported filter class " + filterName + ".");
            
            return ErrorCode.PARAMETER_ERROR;
        }

        if (filter instanceof OptionHandler){
            List<String> argl         = new LinkedList<String>();
            Scanner scan = new Scanner(wekaParameters);
            while (scan.hasNext()) {
                argl.add(scan.next());
            }
            scan.close();
            
        	((OptionHandler) filter).setOptions(argl.toArray(new String[argl.size()]));
        }
        
        
        filter.setInputFormat(data);

        // Filter
        Instances filteredData = Filter.useFilter(data, filter);
        int numAttributes = filteredData.numAttributes();
        String[] cols = new String[numAttributes];
        for (int i = 0; i < numAttributes; i++)
            cols[i] = filteredData.attribute(i).name();

        CSVWriter out = new CSVWriter(cols, tmpOutputFile);

        for (int i = 0; i < data.numInstances(); i++) {
            for (int j = 0; j < numAttributes; j++) {

                if (filteredData.attribute(j).isNumeric())
                    out.write(filteredData.instance(i).value(j));
                else
                    out.write(filteredData.attribute(j).value(
                            (int) filteredData.instance(i).value(j)));
            }
        }
        out.close();

        WekaDataUtils.wekaToAnduril(tmpOutputFile, outputFile, propertyLine);
        tmpInputFile.delete();
        tmpOutputFile.delete();

        return ErrorCode.OK;
    }

    /**
     * @param argv Pipeline arguments
     */
    public static void main(String[] argv) {
        new WekaTransform().run(argv);
    }

}
