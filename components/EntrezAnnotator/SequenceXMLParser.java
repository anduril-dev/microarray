import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import fi.helsinki.ltdk.csbl.anduril.core.utils.XMLTools;

/**
 * Parse NCBI Entrez E-Utilities EFetch/FASTA XML files.
 * 
 * <pre>
 * XML format
 * TSeqSet ::= TSeq*
 * TSeq ::= TSeq_gi TSeq_sequence OtherAnnotation*
 * </pre>
 */
public class SequenceXMLParser extends XMLParserBase {
    public static final String ELEMENT_ID = "TSeq_gi";
    
    private List<Element> entries;
    private int entryIndex;

    public SequenceXMLParser(EntrezAnnotator annotator) {
        super(annotator, "efetch.fcgi", true);
    }
    
    @Override
    protected String getExtraURLSegment() {
        return "&rettype=fasta";
    }
    
    @Override
    protected void xmlReplacedEvent() {
        this.entries = XMLTools.getChildrenByName(getDocumentRoot(), "TSeq");
        if (this.entries.isEmpty()) setColumnNames(new ArrayList<String>());
        this.entryIndex = 0;
    }
    
    @Override
    public String parseNext(Map<String,String> destination) {
        destination.clear();
        if (this.entryIndex >= this.entries.size()) {
            return null;
        }
        
        final boolean assignColumnNames = (getColumnNames() == null);
        Element entryRoot = this.entries.get(this.entryIndex);
        this.entryIndex++;
        
        final String id = XMLTools.getSingleChildContents(entryRoot, ELEMENT_ID);
        
        for (Element child: XMLTools.getChildren(entryRoot)) {
            final String type = child.getNodeName();
            if (type.equals(ELEMENT_ID)) continue;
            
            String content = XMLTools.getElementContentsAsString(child);
            if (content == null || content.isEmpty()) content = child.getAttribute("value");
            if (content != null) {
                if (assignColumnNames) addColumnName(type);
                destination.put(type, content);
            }
        }
        
        return id;
    }
    
}