import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.helsinki.ltdk.csbl.anduril.core.utils.TextTools;
import fi.helsinki.ltdk.csbl.anduril.core.utils.XMLTools;

/**
 * Parse NCBI Entrez E-Utilities ELink XML files (LinkOut).
 *
 * <pre>
 * XML format
 * eLinkResult ::= LinkSet*
 * LinkSet     ::= DbFrom IdURlList
 * IdURlList   ::= IdUrlSet*
 * IdUrlSet    ::= Id ObjUrl*
 * ObjUrl      ::= Url LinkName* SubjectType* Category* Attribute* Provider 
 * Provider    ::= Name NameAbbr Id Url
 * </pre>
 */
public class LinkOutXMLParser extends XMLParserBase {
    public static final String COLUMN_URL = "URL";
    public static final String COLUMN_LINK_NAME = "LinkName";
    public static final String COLUMN_SUBJECT_TYPE = "SubjectType";
    public static final String COLUMN_CATEGORY = "Category";
    public static final String COLUMN_ATTRIBUTE = "Attribute";
    public static final String COLUMN_PROVIDER_NAME = "ProviderName";
    public static final String COLUMN_PROVIDER_ID = "ProviderID";
    public static final String COLUMN_PROVIDER_ABBR = "ProviderAbbr";
    
    private List<Element> entries;
    private int entryIndex;
    
    public LinkOutXMLParser(EntrezAnnotator annotator) {
        super(annotator, "elink.fcgi", false);
    }

    @Override
    protected String getExtraURLSegment() {
        StringBuffer extra = new StringBuffer("&cmd=llinks");

        if (!getAnnotator().getDBFrom().isEmpty()) {
            extra.append("&dbfrom=");
            extra.append(getAnnotator().getDBFrom());
        }
        
        return extra.length() > 0 ? extra.toString() : null;
    }
    
    @Override
    protected void xmlReplacedEvent() {
        this.entries = new ArrayList<Element>();
        NodeList list = getDocumentRoot().getElementsByTagName("ObjUrl");
        for (int i=0; i<list.getLength(); i++) {
            Node node = list.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element)node;
                this.entries.add(element);
            }
        }
        
        this.entryIndex = 0;
        addColumnName(COLUMN_URL);
        addColumnName(COLUMN_LINK_NAME);
        addColumnName(COLUMN_SUBJECT_TYPE);
        addColumnName(COLUMN_CATEGORY);
        addColumnName(COLUMN_ATTRIBUTE);
        addColumnName(COLUMN_PROVIDER_NAME);
        addColumnName(COLUMN_PROVIDER_ID);
        addColumnName(COLUMN_PROVIDER_ABBR);
    }
    
    @Override
    public String parseNext(Map<String, String> destination) {
        destination.clear();
        if (this.entryIndex >= this.entries.size()) {
            return null;
        }

        Element objUrl = this.entries.get(this.entryIndex);
        this.entryIndex++;

        Element parent = (Element)objUrl.getParentNode();
        final String id = XMLTools.getSingleChildContents(parent, "Id");
        
        destination.put(COLUMN_URL, XMLTools.getSingleChildContents(objUrl, "Url"));
        destination.put(COLUMN_LINK_NAME, getElementContent(objUrl, "LinkName"));
        destination.put(COLUMN_SUBJECT_TYPE, getElementContent(objUrl, "SubjectType"));
        destination.put(COLUMN_CATEGORY, getElementContent(objUrl, "Category"));
        destination.put(COLUMN_ATTRIBUTE, getElementContent(objUrl, "Attribute"));

        Element provider = XMLTools.getChildByName(objUrl, "Provider");
        destination.put(COLUMN_PROVIDER_NAME, XMLTools.getSingleChildContents(provider, "Name"));
        destination.put(COLUMN_PROVIDER_ID, XMLTools.getSingleChildContents(provider, "Id"));
        destination.put(COLUMN_PROVIDER_ABBR, XMLTools.getSingleChildContents(provider, "NameAbbr"));
        
        return id;
    }
    
    private String getElementContent(Element parent, String childName) {
        String content = TextTools.join(XMLTools.getChildrenContents(parent, childName), ",");
        content = content.trim();
        if (content.isEmpty()) content = null;
        return content;
    }
}