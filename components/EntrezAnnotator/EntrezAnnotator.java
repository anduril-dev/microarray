import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import fi.helsinki.ltdk.csbl.anduril.component.CSVReader;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class EntrezAnnotator extends SkeletonComponent {
    public static final String DEFAULT_OUT_COLUMN = "ID";
    
    private String db;
    private String baseURL;
    private String tool;
    private String email;
    private String dbfrom;
    
    private String keyColumn;
    private String outColumn;
    private List<String> attributes;
    private double queryDelay;
    private int batchSize;
    
    private XMLParserBase xmlParser;
    private CSVWriter resultWriter;
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        boolean ok = readParameters(cf);
        if (!ok) return ErrorCode.PARAMETER_ERROR;
        
        System.out.println("URL query format:");
        List<String> tmp = new ArrayList<String>();
        tmp.add("ID1");
        tmp.add("ID2");
        System.out.println(this.xmlParser.formatQueryURL(tmp));
        
        CSVReader queryReader = new CSVReader(cf.getInput("query"));
        try {
            ok = fetchAnnotations(cf, queryReader);
        } finally {
	    if (this.resultWriter == null) {
		this.resultWriter = new CSVWriter(
		    new String[] {DEFAULT_OUT_COLUMN}, 
                    cf.getOutput("annotation"));
	    }
	    this.resultWriter.close();
            queryReader.close();
        }
        
        return ok ? ErrorCode.OK : ErrorCode.INVALID_INPUT;
    }
    
    public String getDB() { return this.db; }
    public String getBaseURL() { return this.baseURL; }
    public String getTool() { return this.tool; }
    public String getEmail() { return this.email; }
    public String getDBFrom() { return this.dbfrom; }
    public List<String> getAttributes() { return this.attributes; }
    
    private boolean readParameters(CommandFile cf) {
        this.db = cf.getParameter("db").trim();
        this.baseURL = cf.getParameter("baseURL");
        this.tool = cf.getParameter("tool").trim();
        this.email = cf.getParameter("userEmail").trim();
        this.dbfrom = cf.getParameter("dbfrom").trim();
        this.keyColumn = cf.getParameter("keyColumn");
        
        try {
            this.batchSize = Integer.parseInt(cf.getParameter("batchSize"));
        } catch (NumberFormatException e) {
            cf.writeError("Invalid batchSize parameter");
            return false;
        }
        if (this.batchSize < 1) {
            cf.writeError("Parameter batchSize has invalid range: "+this.batchSize);
            return false;
        }
        
        final String mode = cf.getParameter("mode");
        if (mode.equals("summary")) {
            this.xmlParser = new SummaryXMLParser(this);
        } else if (mode.equals("link")) {
            this.xmlParser = new LinkXMLParser(this);
            if (this.dbfrom.isEmpty()) {
                cf.writeError("Parameter 'dbfrom' must be supplied with mode="+mode);
                return false;
            }
        } else if (mode.equals("linkout")) {
            this.xmlParser = new LinkOutXMLParser(this);
        } else if (mode.equals("sequence")) {
            this.xmlParser = new SequenceXMLParser(this);
        } else if (mode.equals("search")) {
            this.xmlParser = new SearchXMLParser(this);
            if (this.batchSize > 1) {
                this.batchSize = 1;
                System.out.println("Setting batchSize=1 for mode="+mode);
            }
        } else {
            cf.writeError("Invalid 'mode' parameter: "+mode);
            return false;
        }
        
        String attrStr = cf.getParameter("attributes");
        if (!"*".equals(attrStr)) {
            this.attributes = new ArrayList<String>();
            for (String attr: attrStr.split(",")) {
                this.attributes.add(attr);
            }
        } else {
            this.attributes = null;
        }

        this.outColumn = cf.getParameter("outColumn");
        if (this.outColumn.isEmpty()) {
            this.outColumn = this.keyColumn.isEmpty() ? DEFAULT_OUT_COLUMN : this.keyColumn;
        }
        
        try {
            this.queryDelay = Double.parseDouble(cf.getParameter("queryDelay"));
        } catch (NumberFormatException e) {
            cf.writeError("Invalid secBetweenQueries parameter");
            return false;
        }
        
        return true;
    }
    
    private boolean fetchAnnotations(CommandFile cf, CSVReader query) {
        final int keyIndex = this.keyColumn.isEmpty() ? 0 : query.getColumnIndex(this.keyColumn, false);
        if (keyIndex < 0) {
            cf.writeError("Key column not found in query: "+this.keyColumn);
            return false;
        }
        
        List<String> ids = new ArrayList<String>();
        
        for (List<String> row: query) {
            final String key = row.get(keyIndex);
            if (key == null) continue;
            for (String id: key.split(",")) {
                id = id.trim();
                if (id.isEmpty()) continue;
                ids.add(id);
                if (ids.size() >= this.batchSize) {
                    boolean ok = fetchBatch(cf, ids);
                    if (!ok) return false;
                    ids.clear();
                }
            }
        }
        
        if (!ids.isEmpty()) {
            boolean ok = fetchBatch(cf, ids);
            if (!ok) return false;
        }
        
        return true;
    }
    
    private boolean fetchBatch(CommandFile cf, List<String> ids) {
        this.xmlParser.setCurrentBatch(ids);
        String url = this.xmlParser.formatQueryURL(ids);
        
        URL urlObj;
        try {
            urlObj = new URL(url);
        } catch (MalformedURLException e) {
            cf.writeError("URL is invalid: "+url);
            return false;
        }

        try {
            URLConnection conn = null;
            if (this.queryDelay > 0 && this.xmlParser.getBatchCount() > 0) {
                try {
                    Thread.sleep((int)(this.queryDelay*1000));
                } catch (InterruptedException e) { }
            }
            try {
                conn = urlObj.openConnection();
                conn.connect();
                this.xmlParser.parseFile(conn.getInputStream());
            } finally {
                if (conn != null) conn.getInputStream().close();
            }
            
            Set<String> missingIDSet = new HashSet<String>(ids);
            Map<String,String> row = new HashMap<String, String>();
            String id;
            while ((id = xmlParser.parseNext(row)) != null) {
                List<String> attrColumns = (this.attributes == null)
                    ? xmlParser.getColumnNames() : this.attributes;
                
                if (this.resultWriter == null) {
                    this.resultWriter = new CSVWriter(
                            getColumnNames(attrColumns),
                            cf.getOutput("annotation"));
                }
                this.resultWriter.write(id);
                
                for (String attrColumn: attrColumns) {
                    this.resultWriter.write(row.get(attrColumn));
                }
                missingIDSet.remove(id);
            }
            
            List<String> attrColumns = (this.attributes == null)
                ? xmlParser.getColumnNames() : this.attributes;
            for (String missingID: missingIDSet) {
                this.resultWriter.write(missingID);
                for (int i=0; i<attrColumns.size(); i++) {
                    this.resultWriter.write(null);
                }
            }
        } catch (IOException e) {
            cf.writeError("I/O error when fetching annotations: "+e+" - Query = "+url);
            return false;
        } catch (ParserConfigurationException e) {
            cf.writeError("Result XML parse error: "+e+" - Query = "+url);
            return false;
        } catch (SAXException e) {
            cf.writeError("Result XML parse error: "+e+" - Query = "+url);
            return false;
        }
        
        return true;
    }
    
    private String[] getColumnNames(List<String> annotationColumns) {
        String[] names = new String[annotationColumns.size()+1];
        names[0] = this.outColumn;
        for (int i=0; i<annotationColumns.size(); i++) {
            names[i+1] = annotationColumns.get(i);
        }
        return names;
    }
    
    public static void main(String[] args) {
        new EntrezAnnotator().run(args);
    }
}
