import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import fi.helsinki.ltdk.csbl.anduril.core.utils.XMLTools;

/**
 * Base class for EntrezAnnotator mode handlers. Subclasses implement
 * functionality unique to each mode. The functionality includes extracting
 * information from the XML DOM tree and formatting query URLs. When the
 * query contains several batches, one instance of this class can be used to
 * parse all batches in succession; each call to {@link #parseFile(InputStream)}
 * replaces the previous DOM tree.
 * 
 * <p>
 * The usage pattern of this class is as follows:
 * <pre>
 * parser = unique instance of XMLParserBase subclass
 * for each batch:
 *     url = parser.formatQueryURL(batch)
 *     parser.parseFile(url.inputStream)
 *     while parser.parseNext() != null:
 *         write row to output CSV file
 * </pre>
 * </p>
 */
public abstract class XMLParserBase implements ErrorHandler {
    private EntrezAnnotator annotator;
    private Element root;
    private List<String> columnNames;
    private int batchCount;
    private List<String> currentBatch;
    
    private String urlCommand;
    private boolean formatCommaSeparatedIDList;
    
    /**
     * Initialize.
     * 
     * @param annotator The EntrezAnnotator instance.
     * @param urlCommand First part in the query URL after common base URL
     *  that identifies the E-Utilities service. Example: "esummary.fcgi".
     * @param formatCommaSeparatedIDList If true, query IDs should be
     *  appended into a comma-separated list. The single list is
     *  transmitted in one <code>id</code> URL parameter. If false,
     *  each ID should have its own <code>id</code> parameter.
     */
    public XMLParserBase(EntrezAnnotator annotator, String urlCommand, boolean formatCommaSeparatedIDList) {
        this.urlCommand = urlCommand;
        this.formatCommaSeparatedIDList = formatCommaSeparatedIDList;
        this.annotator = annotator;
        this.batchCount = 0;
    }
    
    public EntrezAnnotator getAnnotator() {
        return this.annotator;
    }
    
    /**
     * Return a string that is appended to the query URL.
     * Return null if no string is to be appended. 
     */
    protected abstract String getExtraURLSegment();
    
    /**
     * Format an E-Utilities query URL based on given list
     * of query terms.
     * @param terms Query terms. Depending on mode, these can
     *  be NCBI identifiers or search terms.
     */
    public String formatQueryURL(List<String> terms) {
        StringBuffer url = new StringBuffer(annotator.getBaseURL());
        url.append(this.urlCommand);
        url.append("?&retmode=xml&db=");
        url.append(annotator.getDB());
        
        url.append(formatQueryTerms(terms));
        
        if (!annotator.getTool().isEmpty()) {
            url.append("&tool=");
            url.append(annotator.getTool());
        }
        
        if (!annotator.getEmail().isEmpty()) {
            url.append("&email=");
            url.append(annotator.getEmail());
        }
        
        String extra = getExtraURLSegment();
        if (extra != null) url.append(extra);
        
        return url.toString();
    }
    
    /**
     * Format the part of query URL that specifies query terms.
     * Example output: "&id=100,200".
     * 
     * @param terms Query terms. Depending on mode, these can
     *  be NCBI identifiers or search terms.
     */
    protected String formatQueryTerms(List<String> terms) {
        StringBuffer idBuffer = new StringBuffer("&id=");
        
        for (int i=0; i<terms.size(); i++) {
            final String id = terms.get(i).trim();
            if (id.isEmpty()) continue;
            if (i > 0) {
                if (this.formatCommaSeparatedIDList) {
                    idBuffer.append(',');
                } else {
                    idBuffer.append("&id=");
                }
            }
            idBuffer.append(id);
        }
        
        return idBuffer.toString();
    }
    
    /**
     * Return the current batch number, starting from 1.
     */
    public int getBatchCount() {
        return this.batchCount;
    }
    
    /**
     * Parse an Entrez XML result file read from given input
     * stream. The parsed DOM tree can be accessed using
     * {@link #getColumnNames()} after this call. Invoke
     * {@link #xmlReplacedEvent()} after parsing is done.
     * This method can be called several times, parsing
     * a number of XML files.
     * 
     * @param inStream Input stream from which the XML is
     *  retrieved.
     */
    public void parseFile(InputStream inStream) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilder builder = XMLTools.getDocumentBuilder(null, this);
        Document doc = builder.parse(inStream);
        this.root = doc.getDocumentElement();
        this.columnNames = null;
        this.batchCount++;
        xmlReplacedEvent();
    }
    
    /**
     * Event handler that is invoked after a new XML file has
     * been parsed.
     */
    protected abstract void xmlReplacedEvent();
    
    /**
     * Return the next CSV row as a key-value map by traversing
     * the current DOM parse tree. Return the ID value of current
     * row, or null if there are no more records in this batch.
     * 
     * @param destination The map used to store annotation
     *  values. This must be non-null. The implementation must
     *  first clear the map before inserting values for the next
     *  row.
     */
    public abstract String parseNext(Map<String,String> destination);

    /**
     * Return the document root of the current parse tree,
     * or null if no document has yet been parsed.
     */
    public Element getDocumentRoot() {
        return this.root;
    }
    
    /**
     * Return annotation column names for the current batch.
     * This is generally defined (non-null) only after the first row
     * has been parsed using {@link #parseNext(Map)}. Column names
     * are cleared when a new XML document is parsed and are
     * re-assigned in parseNext.
     */
    public List<String> getColumnNames() {
        return this.columnNames;
    }
    
    /**
     * Set annotation column names for the current batch.
     * @param columnNames Column names.
     */
    protected void setColumnNames(List<String> columnNames) {
        this.columnNames = columnNames;
    }
    
    /**
     * Add an annotation column name to the current list of
     * column names. This is an alternative way to assign
     * column names.
     * @param name Column name.
     */
    protected void addColumnName(String name) {
        if (this.columnNames == null) {
            this.columnNames = new ArrayList<String>();
        }
        if (name != null) this.columnNames.add(name);
    }
    
    public void setCurrentBatch(List<String> batch) {
        this.currentBatch = batch;
    }
    
    public List<String> getCurrentBatch() {
        return this.currentBatch;
    }
    
    protected List<String> parseIdElements(Element root) {
        List<String> contents = new ArrayList<String>();

        NodeList list = root.getElementsByTagName("Id");
        for (int i=0; i<list.getLength(); i++) {
            Node node = list.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element)node;
                contents.add(XMLTools.getElementContentsAsString(element));
            }
        }
        
        return contents;
    }
    
    @Override
    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        System.err.println(e.getMessage());
    }

}