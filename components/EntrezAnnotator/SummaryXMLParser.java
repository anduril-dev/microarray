import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import fi.helsinki.ltdk.csbl.anduril.core.utils.XMLTools;

/**
 * Parse NCBI Entrez E-Utilities Summary XML files.
 * 
 * <pre>
 * XML format
 * eSummaryResult ::= DocSum*
 * DocSum ::= Id Item*
 * Item(Name, Type)
 * </pre>
 */
public class SummaryXMLParser extends XMLParserBase {
    public static final String TYPE_INTEGER = "Integer";
    public static final String TYPE_LIST = "List";
    public static final String TYPE_STRING = "String";
    public static final String TYPE_STRUCTURE = "Structure";
    
    private List<Element> entries;
    private int entryIndex;

    public SummaryXMLParser(EntrezAnnotator annotator) {
        super(annotator, "esummary.fcgi", true);
    }
    
    @Override
    protected String getExtraURLSegment() {
        return null;
    }
    
    @Override
    protected void xmlReplacedEvent() {
        this.entries = XMLTools.getChildrenByName(getDocumentRoot(), "DocSum");
        if (this.entries.isEmpty()) setColumnNames(new ArrayList<String>());
        this.entryIndex = 0;
    }
    
    @Override
    public String parseNext(Map<String,String> destination) {
        destination.clear();
        if (this.entryIndex >= this.entries.size()) {
            return null;
        }
        
        final boolean assignColumnNames = (getColumnNames() == null);
        Element entryRoot = this.entries.get(this.entryIndex);
        this.entryIndex++;
        
        final String id = XMLTools.getSingleChildContents(entryRoot, "Id");
        
        for (Element item: XMLTools.getChildrenByName(entryRoot, "Item")) {
            final String name = item.getAttribute("Name");
            final String content = parseItemValue(item);
            if (content != null) {
                if (assignColumnNames) addColumnName(name);
                destination.put(name, content);
            }
        }
        
        return id;
    }
    
    private String parseItemValue(Element item) {
        final String type = item.getAttribute("Type");
        
        if (type.equals(TYPE_INTEGER) || type.equals(TYPE_STRING)) {
            return XMLTools.getElementContentsAsString(item).trim();
        } else if (type.equals(TYPE_LIST) || type.equals(TYPE_STRUCTURE)) {
            StringBuffer content = new StringBuffer();
            final List<Element> childItems = XMLTools.getChildrenByName(item, "Item");
            if (childItems.size() > 1) content.append("(");
            
            /* allNamesSame is true if all child items have the same name.
             * In this case, node names are not printed to result. */
            boolean allNamesSame = true;
            String uniqueChildName = null;
            for (Element childItem: childItems) {
                final String childName = childItem.getAttribute("Name");
                if (uniqueChildName == null) {
                    uniqueChildName = childName;
                } else if (!uniqueChildName.equals(childName)) {
                    allNamesSame = false;
                    break;
                }
            }
            
            for (int i=0; i<childItems.size(); i++) {
                Element childItem = childItems.get(i);
                final String childValue = parseItemValue(childItem);
                if (childValue != null) {
                    if (i > 0) content.append(",");
                    if (!allNamesSame) {
                        final String childName = childItem.getAttribute("Name");
                        content.append(childName+"=");
                    }
                    content.append(childValue);
                }
            }
            
            if (childItems.size() > 1) content.append(")");
            return content.toString();
        } else {
            return null;
        }
    }
}