import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import fi.helsinki.ltdk.csbl.anduril.core.utils.TextTools;
import fi.helsinki.ltdk.csbl.anduril.core.utils.XMLTools;

/**
 * Parse NCBI Entrez E-Utilities ESearch XML files.
 * 
 * <pre>
 * XML format
 * eSearchResult ::= Count IdList TranslationSet QueryTranslation
 * IdList ::= Id*
 * TranslationSet ::= Translation*
 * Translation ::= From To
 * </pre>
 */
public class SearchXMLParser extends XMLParserBase {
    public static final String COLUMN_RESULT = "Result";
    public static final String COLUMN_TRANSLATION = "QueryTranslation";
    
    private int entryIndex;
    
    public SearchXMLParser(EntrezAnnotator annotator) {
        super(annotator, "esearch.fcgi", true);
    }

    @Override
    protected String formatQueryTerms(List<String> terms) {
        StringBuffer termBuffer = new StringBuffer("&term=");
        
        for (int i=0; i<terms.size(); i++) {
            String term = terms.get(i).trim();
            if (term.isEmpty()) continue;
            term = term.replace(' ', '+');
            term = term.replace('\'', '"');
            
            if (i > 0) termBuffer.append('+');
            termBuffer.append(term);
        }
        
        return termBuffer.toString();
    }
    
    @Override
    protected String getExtraURLSegment() {
        return null;
    }
    
    @Override
    protected void xmlReplacedEvent() {
        this.entryIndex = 0;
        this.addColumnName(COLUMN_RESULT);
        this.addColumnName(COLUMN_TRANSLATION);
    }
    
    @Override
    public String parseNext(Map<String,String> destination) {
        destination.clear();
        if (this.entryIndex > 0) return null;
        this.entryIndex++;
        
        final String query = getCurrentBatch().get(0);
        
        List<Element> idList = XMLTools.getChildrenByName(getDocumentRoot(), "IdList");
        String result;
        if (idList.isEmpty()) {
            result = null;
        } else {
            result = TextTools.join(parseIdElements(idList.get(0)), ",");
            if (result.isEmpty()) result = null;
        }
        destination.put(COLUMN_RESULT, result);
        
        String translation = XMLTools.getSingleChildContents(getDocumentRoot(), "QueryTranslation");
        if (translation.isEmpty()) {
            translation = null;
        } else {
            translation = translation.replace('"', '\'');
        }
        destination.put(COLUMN_TRANSLATION, translation);

        return query;
    }
    
}