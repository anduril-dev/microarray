import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import fi.helsinki.ltdk.csbl.anduril.core.utils.TextTools;
import fi.helsinki.ltdk.csbl.anduril.core.utils.XMLTools;

/**
 * Parse NCBI Entrez E-Utilities ELink XML files (internal links).
 *
 * <pre>
 * XML format
 * eLinkResult ::= LinkSet*
 * LinkSet ::= DbFrom IdList LinkSetDb*
 * IdList ::= Id*
 * LinkSetDb ::= DbTo LinkName Link*
 * Link ::= Id 
 * </pre>
 */
public class LinkXMLParser extends XMLParserBase {

    private List<Element> entries;
    private int entryIndex;
    
    public LinkXMLParser(EntrezAnnotator annotator) {
        super(annotator, "elink.fcgi", false);
    }

    @Override
    protected String getExtraURLSegment() {
        StringBuffer extra = new StringBuffer();
        
        if (!getAnnotator().getDBFrom().isEmpty()) {
            extra.append("&dbfrom=");
            extra.append(getAnnotator().getDBFrom());
        }
        
        List<String> attrs = getAnnotator().getAttributes();
        if (attrs != null && !attrs.contains("*")) {
            extra.append("&linkname=");
            extra.append(TextTools.join(getAnnotator().getAttributes(), ","));
        }
        
        return extra.length() > 0 ? extra.toString() : null;
    }
    
    @Override
    protected void xmlReplacedEvent() {
        this.entries = XMLTools.getChildrenByName(getDocumentRoot(), "LinkSet");
        if (this.entries.isEmpty()) setColumnNames(new ArrayList<String>());
        this.entryIndex = 0;
    }
    
    @Override
    public String parseNext(Map<String, String> destination) {
        destination.clear();
        if (this.entryIndex >= this.entries.size()) {
            return null;
        }

        final boolean assignColumnNames = (getColumnNames() == null);
        Element linkSet = this.entries.get(this.entryIndex);
        this.entryIndex++;
        
        List<Element> idList = XMLTools.getChildrenByName(linkSet, "IdList");
        final String id = TextTools.join(parseIdElements(idList.get(0)), ",");
        
        for (Element linkSetDb: XMLTools.getChildrenByName(linkSet, "LinkSetDb")) {
            final String linkName = XMLTools.getSingleChildContents(linkSetDb, "LinkName");
            if (assignColumnNames) addColumnName(linkName);
            final String content = TextTools.join(parseIdElements(linkSetDb), ",");
            destination.put(linkName, content);
        }
        
        return id;
    }
    
}