library(componentSkeleton)

execute <- function(cf) {
  myName  <- get.metadata(cf, 'instanceName')
  doc.dir <- get.output(cf,   'report')
  bibtex  <- get.output(cf,   'citations')
  bib     <- ''

  pkgs <- unlist(strsplit(get.parameter(cf, 'packages'), ',', fixed=TRUE))
  if (input.defined(cf, 'packageList')) {
     pkgs <- union(pkgs, CSV.read(get.input(cf, 'packageList'))[,1])
  }
  pkgs <- sort(pkgs)

  dir.create(doc.dir, recursive=TRUE)

  tex <- sprintf('\\%s{R configuration}', get.parameter(cf, 'sectionType'))

  for (pName in pkgs) {
      bId  <- paste(myName,pName,sep='')
      b    <- citation(pName)
      bTex <- toBibtex(b)
      c    <- 0
      if (!is.list(bTex)) {
         bTex <- list(bTex)
      }
      for (bTex in bTex) {
          bTex[1] <- sprintf('%s%s%s,', strsplit(bTex[1],',',fixed=TRUE), bId, if (c==0) '' else as.character(c))
          bib     <- paste(bib, paste(bTex, collapse='\n'), sep='\n')
          c       <- c+1
      }

      info      <- library(help=pName, character.only=TRUE)$info[[1]]
      descTable <- character()
      for (line in info) {
         d   <- regexpr(':',line)[1]
         key <- substr(line,   1, d-1)
         if (key == 'Package') next

         value     <- substr(line, d+1, nchar(line))
         value     <- latex.quote(trim(value))
         descTable <- rbind(descTable, c(key, value))
      }
      if (c > 1) {
         bId <- paste(bId, paste(bId,1:(c-1),sep='',collapse=','), sep=',')
      }
      tex <- c(tex,
               sprintf('\\textbf{\\hyperdef{%s}{%s}{%s}}~\\cite{%s}\\\\',
                       myName, pName, pName, bId),
               latex.tabular(descTable,
                             column.alignment = 'p{2.5cm}p{15.5cm}',
                             long             = FALSE,
                             escape.content   = FALSE),
               '\n\n')
  }

  latex.write.main(cf, 'report', tex)
  cat(file=bibtex, bib)
  return(0)
}

main(execute)
