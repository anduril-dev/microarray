library(componentSkeleton)
library(intCNGEan)

execute <- function(cf) {

    # INPUTs
    probloss   <- Matrix.read(get.input(cf, 'CNAProbLoss'))
    probgain   <- Matrix.read(get.input(cf, 'CNAProbGain'))
    probnorm   <- Matrix.read(get.input(cf, 'CNAProbNorm'))
    exprMatrix <- LogMatrix.read(get.input(cf, 'exprMatrix'))
    exprAnnot  <- AnnotationTable.read(get.input(cf, 'exprAnnotation'))

    exprAnnot2 <- as.matrix(exprAnnot[,2:4])
    rownames(exprAnnot2) <- exprAnnot[,1]
    exprAnnot  <- exprAnnot2

    # Parameters
    analysisType <- get.parameter(cf, 'analysisType',    type='string')
    statistic    <- get.parameter(cf, 'testStatistic',   type='string')
    fdr.limit    <- get.parameter(cf, 'FDRLimit',        type='float')
    nro.perms    <- get.parameter(cf, 'nperm',           type='int')
    nro.genes    <- get.parameter(cf, 'nGenesTune',      type='int')
    match.method <- get.parameter(cf, 'matchMethod',     type='string')

    # Parameter checks
    if (!(analysisType %in% c("univariate","regional"))) {
        write.error(cf, 'Parameter analysisType must be one of (regional,univariate).')
        return(INVALID_INPUT)
    }
    if (!(statistic %in% c("wcvm","wmw"))) {
        write.error(cf, 'Parameter testStatistic must be one of (wcvm,wmw).')
        return(INVALID_INPUT)
    }
    if (fdr.limit < 0 || fdr.limit > 1) {
        write.error(cf, 'Parameter FDRLimit must be between 0 and 1.')
        return(INVALID_INPUT)
    }

    # ExpressionSet conversions
    cnaFeatureData      <- data.frame(probnorm[,1:3], row.names = rownames(probnorm))
    cnaFeatureData      <- new(data=cnaFeatureData, 'AnnotatedDataFrame', dimLabels = c("featureNames", "featureColumns"))
    cnaDull             <- matrix(0,nrow=nrow(probnorm), ncol=(ncol(probnorm)-4))
    colnames(cnaDull)   <- colnames(probnorm)[5:ncol(probnorm)]
    rownames(cnaDull)   <- rownames(probnorm)

    # Data type conversions
    exprMatrix <- new('ExpressionSet', exprs=exprMatrix[,1:ncol(exprMatrix)], featureData=as(data.frame(exprAnnot[,1:ncol(exprAnnot)]), 'AnnotatedDataFrame'))
    CNAmatrix  <- new('cghCall', probloss    = probloss[,5:ncol(probloss)],
                                 probnorm    = probnorm[,5:ncol(probnorm)],
                                 probgain    = probgain[,5:ncol(probgain)],
                                 copynumber  = cnaDull, segmented = cnaDull, calls = cnaDull,
                                 featureData = cnaFeatureData)

    # Match probes from different platforms based on loci
    matchedMatrices <- intCNGEan.match(CNdata = CNAmatrix, CNbpend = 'yes',
                                       GEdata = exprMatrix, GEbpend = 'yes',
                                       method = match.method)

    # Tuning drops uninteresting probes and decides whether to test loss vs normal, or gain vs normal for each probe
    tunedMatrices <- intCNGEan.tune(matchedMatrices$CNdata.matched,matchedMatrices$GEdata.matched, test.statistic=statistic,
                                    nperm_tuning = nro.perms, ngenetune = nro.genes)

    # Integration
    results <- intCNGEan.test(tunedMatrices, 
                              analysis.type   = analysisType,
                              test.statistic  = statistic,
                              eff.p.val.thres = fdr.limit,
                              nperm           = nro.perms)

    # Fetch probenames for output
    probes <- rownames(fData(matchedMatrices$CNdata.matched))[as.numeric(results$gene.id)]

    # Write OUTPUTs
    CSV.write(get.output(cf, 'integrationResult'), as.data.frame(cbind(probes,results)))

    return(TRUE)
}

main(execute)
