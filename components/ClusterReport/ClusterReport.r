library(componentSkeleton)
library(cluster)
library(igraph)

writeGraphML <- function(d, filename) {
  g <- graph.empty(n=0, directed=TRUE)
  if (!is.null(d)) {
     idSeq       <- 0
     addVertices <- function(g,r,d) {
        for (n in d) {
            idSeq  <<- idSeq+1
            a       <- list(nid=idSeq)
            if (is.leaf(n)) {
               a$label <- attr(n,'label')
               a$shape <- 'plaintext'
            } else {
               a$shape <- 'point'
            }
            g       <- add.vertices(g, 1, attr=a)
            v       <- V(g)
            v       <- v[v$nid==idSeq]
            g       <- add.edges(g, c(r,v))
            if (!is.leaf(n)) {
               g <- addVertices(g,v,n)
            }
        }
        g
     }
     g <- add.vertices(g, 1, attr=list(nid=0,shape='point'))
     g <- addVertices(g,V(g),d)
  }
  write.graph(g, filename, format='graphml')
}

execute <- function(cf) {
    matr <- Matrix.read(get.input(cf, 'matr'))
    
    section.title <- get.parameter(cf, 'sectionTitle')
    section.type  <- get.parameter(cf, 'sectionType')
    width.cm      <- get.parameter(cf, 'width',        'float')
    show.dist     <- get.parameter(cf, 'showDistance', 'boolean')
    cex.text      <- get.parameter(cf, 'cexSampleText','float')
	marg          <- get.parameter(cf, 'margin','float')
	c.method      <- get.parameter(cf, 'clusterMethod', 'string')
    out.tree      <- get.output(cf, 'tree')
 
    stopifnot(section.type %in% c('', LATEX.SECTIONS))
 
    out.dir <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)
 
    if (nchar(section.type) > 0) {
        tex <- sprintf('\\%s{%s}', section.type, section.title)
    } else {
        tex <- c()
    }

    if (nrow(matr) < 1) {
       show.dist <- FALSE
       tex       <- c(tex, '\nNo data rows!\n')
       writeGraphML(NULL, out.tree)
    } else {
       res <- agnes(t(matr), method = c.method)
       d   <- as.dendrogram(res, hang=0.1)
 
       writeGraphML(d, out.tree)

       plot.file <- sprintf('%s-dendrogram.pdf', get.metadata(cf, 'instanceName'))
       pdf(file.path(out.dir, plot.file), height=4)
   
       par(mar=rep(marg,4), cex = cex.text)
       plot(d, main='', sub='', xlab='', ylab='', yaxt='n')
       dev.off()

       caption <- paste(
         sprintf('Hierarchical clustering using %d samples and up to %d measurements for each sample.',
                 ncol(matr), nrow(matr)),
         'If the data contains missing values, they are omitted in distance calculation.',
         'The distance metric is Euclidean. Clustering is done using',
          sprintf('agglomerative clustering with %s linkage.', c.method)
       )
       tex <- c(tex, latex.figure(plot.file, caption=caption, image.width=width.cm))
    }
 
    if (show.dist) {
       dist.matrix <- as.matrix(res$diss)
       # Normalize to range 0..1.
       max.val <- max(dist.matrix, na.rm=TRUE)
       if (is.finite(max.val)) dist.matrix <- dist.matrix / max.val
       if (get.parameter(cf, 'rotateLabels', 'boolean')) {
           # This use the rotating LaTeX package.
           colnames(dist.matrix) <- paste('\\begin{sideways}', latex.quote(colnames(dist.matrix)), '\\end{sideways}')
       }
       tex <- c(tex,
                latex.table(dist.matrix,
                            paste('l', rep('r', ncol(dist.matrix)), sep='', collapse=''),
                            caption='Distance matrix between samples, normalized to range [0, 1].',
                            use.row.names=TRUE, escape=FALSE)
       )
    }
 
    latex.write.main(cf, 'report', tex)
    return(0)
}

main(execute)
