import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

public class TextFileSplitter extends SkeletonComponent {
    private int headerRows;
    private int rowsPerRecord;
    private String keyPattern;
    private File input;
    private File output;
    private String fileExtension;
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        this.headerRows = cf.getIntParameter("headerRows");
        this.rowsPerRecord = cf.getIntParameter("rowsPerRecord");
        this.keyPattern = cf.getParameter("keyPattern");
        this.input = cf.getInput("file");
        this.output = cf.getOutput("array");
        
        int lastPos = this.input.getName().lastIndexOf('.');
        this.fileExtension = lastPos < 0 ? null : this.input.getName().substring(lastPos+1);
        
        final int numElements = cf.getIntParameter("numElements");
        int maxRecords = cf.getIntParameter("maxRecords");
        
        final String splitPatternStr = cf.getParameter("splitRegexp");
        final Pattern splitPattern = splitPatternStr.isEmpty() ?
                null : Pattern.compile(splitPatternStr);

        this.output.mkdirs();
        final IndexFile outIndex;
        if (numElements > 0) {
            if (maxRecords > 0) {
                cf.writeError("When numElements > 0, maxRecords must not be used");
                return ErrorCode.PARAMETER_ERROR;
            }
            if (splitPattern != null) {
                cf.writeError("When numElements > 0, splitRegexp must not be used");
                return ErrorCode.PARAMETER_ERROR;
            }
            outIndex = runModeFixed(cf, numElements);
        } else {
            if (maxRecords <= 0) maxRecords = Integer.MAX_VALUE;
            outIndex = runModeNonFixed(cf, maxRecords, splitPattern);
        }
        
        outIndex.write(cf, "array");
        
        return ErrorCode.OK;
    }
    
    // Common tools //
    
    private String[] readHeader(BufferedReader reader) throws IOException {
        final String[] header = new String[this.headerRows];
        for (int i=0; i<this.headerRows; i++) {
            header[i] = reader.readLine();
        }
        return header;
    }
    
    private BufferedWriter startNewElement(int recordNumber, String[] header, IndexFile outIndex) throws IOException {
        final String key = String.format(this.keyPattern, recordNumber);
        final String basename = this.fileExtension == null ? key : key+"."+this.fileExtension;
        final File outFile = new File(this.output, basename);
        outIndex.add(key, outFile);
        
        BufferedWriter writer = new BufferedWriter(new FileWriter(outFile)); 
        if (header != null) {
            for (String row: header) writer.write(row+"\n");
        }
        return writer;
    }

    // Mode 1: fixed number of output elements //    
    
    private IndexFile runModeFixed(CommandFile cf, int numElements) throws IOException {
        final int numRecords = getNumRecords(cf, numElements);
        int pendingRecords = numRecords;
        final IndexFile index = new IndexFile();

        final BufferedReader reader = new BufferedReader(new FileReader(this.input));
        try {
            final String[] header = readHeader(reader);
            
            for (int elem=0; elem<numElements; elem++) {
                BufferedWriter writer = startNewElement(elem+1, header, index);
                
                final int pendingElements = numElements - elem;
                int thisRecords = (int)Math.round((double)pendingRecords / pendingElements);
                thisRecords = Math.min(thisRecords, pendingRecords);
                pendingRecords -= thisRecords;

                rwloop:
                for (int rec=0; rec<thisRecords; rec++) {
                    for (int rowNum=0; rowNum<this.rowsPerRecord; rowNum++) {
                        final String line = reader.readLine();
                        if (line == null) break rwloop;
                        writer.write(line+"\n");
                    }
                }
                
                writer.close();
            }
        } finally {
            reader.close();
        }
        
        return index;
    }
    
    private int getNumRecords(CommandFile cf, int numElements) throws IOException {
        int numRows = 0;
        final BufferedReader reader = new BufferedReader(new FileReader(this.input));
        try {
            while (reader.readLine() != null) numRows++;
        } finally {
            reader.close();
        }
        
        if (numRows < this.headerRows) {
            cf.writeError(String.format("Too few rows (%d): at least %d required",
                    numRows, this.headerRows));
            return -1;
        }
        
        final int nonHeaderRows = numRows - this.headerRows;
        if ((nonHeaderRows % this.rowsPerRecord) != 0) {
            cf.writeError(String.format(
                    "Invalid number of non-header rows (%d): is not multiple of rowsPerRecord",
                    nonHeaderRows));
            return -1;
        }
        return nonHeaderRows / this.rowsPerRecord;
    }
    
    // Mode 2: non-fixed number of output elements //

    private IndexFile runModeNonFixed(CommandFile cf, int maxRecords, Pattern splitPattern) throws IOException {
        final BufferedReader reader = new BufferedReader(new FileReader(this.input));
        final IndexFile index = new IndexFile();
        
        int curRecordPos = 0; // position in current record
        int curRecord = 0; // position in current array element
        int curElement = 1;
        BufferedWriter curWriter = null;
        
        try {
            final String[] header = readHeader(reader);
            
            String line;
            while ((line = reader.readLine()) != null) {
                if (splitPattern != null && splitPattern.matcher(line).matches()) {
                    if (curWriter != null) {
                        curWriter.close();
                        curWriter = null;
                    }
                    curRecord = 0;
                    curRecordPos = 0;
                }

                if (curWriter == null) {
                    curWriter = startNewElement(curElement, header, index);
                    curElement++;
                }
                curWriter.write(line+"\n");
                curRecordPos++;

                if (curRecordPos >= this.rowsPerRecord) {
                    curRecord++;
                    curRecordPos = 0;
                }
                if (curRecord >= maxRecords) {
                    if (curWriter != null) {
                        curWriter.close();
                        curWriter = null;
                    }
                    curRecord = 0;
                    curRecordPos = 0;
                }
            }
        } finally {
            reader.close();
        }

        if (curWriter != null) curWriter.close();
        return index;
    }
    
    public static void main(String[] args) {
        new TextFileSplitter().run(args);
    }
}
