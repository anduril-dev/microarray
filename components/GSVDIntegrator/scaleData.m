function [cdnaScaled, cghScaled] = scaleData(cdnaData, cghData);
%
% scaleData  Scaling Algorithm For Array-CGH Data
%    [CDNASCALED, CGHSCALED] = SCALEDATA(CDNADATA, CGHDATA);
%
% The purpose of this algorithm is to scale array-CGH data to match
% the scale of cDNA microarray data.
%
% It is assumed that the genes in both sets of data are the same and the 
% resulting CGH data will be scaled so that the quantile-quantile plot
% for cDNA and CGH data will be linear.
%
%    Input and output arguments:
%    ---------------------------
%    cdnaData        : Original cDNA data after preprocessing in log2
%                      format (normalization & quality filtering)
%                         [p-by-m double precision]
%    cghData         : Original CGH data after preprocessing in log2 format
%                         [n-by-m double precision]
%
%    cdnaScaled      : Gene expression data in log2 format - (this output
%                      is the same as cdnaData, hence unchanged)
%                         [p-by-m double precision]
%    cghScaled       : Copy number data in log2 format 
%                         [n-by-m double precision]
%    
%    Parameters:
%    -----------
%    p               : number of cDNA ratios - this algorithm assumes p=n.
%    n               : number of CGH ratios
%    m               : number of samples
%
%    <<Version 1.0 (12 April 2005)>>
%      berger@ece.ucsb.edu

%Check the number of samples, m
if(size(cdnaData,2) ~= size(cghData,2)),error('number of samples must be the same');end

[p, m] = size(cdnaData);
[n, m] = size(cghData);

cdnaList = sort(reshape(cdnaData, p*m, 1));
cghList = sort(reshape(cghData, n*m, 1));

k = 5; % order of the polynomial fit

% Old method = take mean of the rows after sorting, then curve fit
%
% cdnaMeanS = mean(sort(cdnaData)')';
% cghMeanS = mean(sort(cghData)')';
% 
% [pol,S] = polyfit(cghMeanS, cdnaMeanS, k);
% cghScaled = polyval(pol, cghData);
% f = polyval(pol, cghMeanS); % Transformation function

% New method = sort all values, then curve fit 
[pol, S] = polyfit(cghList, cdnaList, k);
cghScaled = polyval(pol, cghData);
% f = polyval(pol, cghList); % Transformation function

cdnaScaled = cdnaData; % cDNA data is unscaled