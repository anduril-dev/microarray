% commandfile
try
    cf=componentskeletonmain(commandfile);  % this is needed to get the input and outputs
    gsvdIntegrator(cf);    % this is your function, cf must be sent to everywhere. (or use globals)
catch me  % let the rest be as it is, it is for error handling
    try
        writeerrorstack(cf, me);
    catch again
        exit
    end
end
exit;  % the script must exit matlab at the end!
