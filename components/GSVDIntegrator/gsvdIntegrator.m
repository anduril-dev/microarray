function gsvdIntegrator(cf)
% Implements the GSVD array cgh to expression integration algorithm
% Uses mostly code by Berger

exprMatrix = readcsvcell(getvariable(cf, 'input.exprMatrix'));
cnaMatrix  = readcsvcell(getvariable(cf, 'input.cnaMatrix'));
annotation = readcsvcell(getvariable(cf, 'input.exprAnnotation'));

output   = getvariable(cf, 'output.integrationResults');
outAnnot = getvariable(cf, 'output.annotations');
outCgh   = getvariable(cf, 'output.cghResults');

exprMatrixIn = str2double(exprMatrix.data(1:end,2:end));
cnaMatrixIn  = str2double(cnaMatrix.data(1:end,2:end));

[cdnaData, cghData] = scaleData(exprMatrixIn, cnaMatrixIn);

frac = getvariable(cf, 'parameter.fraction');
numRem = getvariable(cf, 'parameter.numRem');
dir = getvariable(cf, 'parameter.direction');

[cdnaRem, cghRem, IDsRem, indx] = gsvdShaving(cdnaData, cghData, annotation.data(1:end,1), frac, numRem, dir);

csvout.data        = cell(IDsRem.cgh);
csvout.columnheads = {'Genes'};
writefilecsv(output,  csvout);

annot.data        = cell(annotation.data(indx.cgh,:));
annot.columnheads = annotation.columnheads(1:end);
writefilecsv(outAnnot, annot);

rowmedian = median(cghRem');
cghResults = cat(2,rowmedian',cghRem);

cghRes.numdata     = num2cell(cghResults);
cghRes.data        = cellfun(@(x) sprintf('%g',x), cghRes.numdata,'UniformOutput',false);
cghRes.data        = cat(2, IDsRem.cgh, cghRes.data);
cghRes.columnheads = cat(2,'Genes','Median',cnaMatrix.columnheads(2:end));
writefilecsv(outCgh,  cghRes);

end
