function [K, theta] = analyzeGSVs(C, S, dir);
%
% analyzeGSVs  Analyze the Generalized Singular Values
%    [K, THETA] = ANALYZEGSVS(C, S);
%
% The purpose of this algorithm is to analyze the generalized singular
% values and then locate the proper direction to project the original data
% based on the generalized angle where abs(theta) is closest to zero.
%
%    Input and output arguments:
%    ---------------------------
%    C               :  The diagonal matrix C in the GSVD decomposition -
%                       see GSVD for further details.
%                         [m-by-m double precision, diagonal]
%    S               :  The diagonal matrix S in the GSVD decomposition -
%                       see GSVD for further details.
%                         [m-by-m double precision, diagonal]
%    dir             :  The direction to project onto
%                         valid input = 'min', 'zero', 'max'
%
%    K               :  The direction corresponding to the minimum
%                       magnitude theta
%                         [1-by-1 double precision: 1 <= K <= m]
%    theta           :  The generalized angles - see GSVD for further
%                       details.
%
%    Parameters:
%    -----------
%    m               : number of samples
%
%    <<Version 1.0 (12 April 2005)>>
%      berger@ece.ucsb.edu

s = diag(S);
c = diag(C);

theta = atan(c./s)-pi/4;

switch dir
    case 'zero'
        % theta = 0 Variation in copy number & gene expression
        [y, K] = min(abs(theta));
    case 'min'
        % min(theta) Variation in c.n.
        [y, K] = min(theta);
    case 'max'
        % max(theta) Variation in g.e.
        [y, K] = max(theta);
end