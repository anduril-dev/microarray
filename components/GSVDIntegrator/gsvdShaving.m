function [cdnaRem, cghRem, IDsRem, indx] = gsvdShaving(cdnaScaled, cghScaled, IDs, frac, numRem, dir);
%
% gsvdShaving  Generalized Singular Value Decomposition Shaving Algorithm
%    [CDNAREM, CGHREM, IDSREM, INDX] = GSVDSHAVING(CDNASCALED, CGHSCALED, IDS, FRAC, NUMREM, DIR);
%
%    Performs the GSVD shaving algorithm for cDNA and array-CGH microarray
%    data. Returns the remaining variant genes/clones and their gene
%    expression & copy number values.
%    
%    Input and output arguments:
%    ---------------------------
%    cdnaScaled      : Gene expression data in log2 format 
%                         [p-by-m double precision]
%    cghScaled       : Copy number data in log2 format 
%                         [n-by-m double precision]
%    IDs             : Gene/clone identifiers
%                         [(p+n)-by-1 cell array]
%    frac            : Fraction of genes to retain after each iteration
%                         [good default = 0.95]
%    numRem          : Minimum number of genes/clones to retain
%                         (actual number depends on frac and p+n)
%                         [must be > m-1]
%    dir             :  The direction to project onto
%                         valid input = 'min', 'zero', 'max'
%    
%    cdnaRem         : Remaining cDNA ratios
%                         [numRem-by-m double precision]
%    cghRem          : Remaining CGH ratios
%                         [numRem-by-m double precision]
%    IDsRem          : Gene/clone identifiers after shaving
%                         [2*numRem-by-1 cell array]
%    indx            : Index positions
%                         [2*numRem-by-1 double precision]
%    
%    Parameters:
%    -----------
%    p               : number of cDNA ratios
%    n               : number of CGH ratios
%    m               : number of samples
%
%    <<Version 1.0 (6 April 2005)>>
%      berger@ece.ucsb.edu

% Argument checking
error(nargchk(6, 6, nargin));
error(nargoutchk(4, 4, nargout));

R{1} = ([cghScaled;cdnaScaled]); % Form the data matrix
p = size(cghScaled,1);
n = size(cdnaScaled,1);
inds{1} = [1:p(1)+n(1)]';

% Perform the decomposition
[U,V,X,C,S] = gsvd(R{1}(1:p,:), R{1}(p + 1:p + n,:), 0);

% Analyze the GSVs
[K(1), theta(:,1)] = analyzeGSVs(C, S, dir);

% Project the original data in the direction of the Kth GSV
A{1} = R{1}*X(:,K);

[sA_01, ind_sA_01] = sort(A{1}(1:p(1)));
[sA_02, ind_sA_02] = sort(A{1}(p(1) + 1:p(1) + n(1)));

p(2) = round(frac*p(1)); % keep top/bot fraction of the mRNA levels
n(2) = round(frac*n(1)); % keep top/bot fraction of the copy number values

if mod(p(2),2) == 0,
        botCGH{1} = ind_sA_01(1:(p(2)/2));
        botcDNA{1} = ind_sA_02(1:(n(2)/2));
        topCGH{1} = ind_sA_01(end-(p(2)/2)+1:end);
        topcDNA{1} = ind_sA_02(end-(n(2)/2)+1:end);
    else
        botCGH{1} = ind_sA_01(1:ceil(p(2)/2));
        botcDNA{1} = ind_sA_02(1:ceil(n(2)/2));
        topCGH{1} = ind_sA_01(end-floor(p(2)/2)+1:end);
        topcDNA{1} = ind_sA_02(end-floor(n(2)/2)+1:end);
    end

% Re-form the data matrix with the shaved genes
R{2} = [R{1}(topCGH{1},:); R{1}(botCGH{1},:); R{1}(p(1) + topcDNA{1},:); R{1}(p(1) + botcDNA{1},:)];
inds{2} = [topCGH{1}; botCGH{1}; p(1) + topcDNA{1}; p(1) + botcDNA{1}];

[sInd, iInd] = sort(inds{2});
inds{2} = inds{2}(iInd);
R{2} = R{2}(iInd,:);

m = 2;
while p > numRem & n > numRem,
    [U,V,X,C,S] = gsvd(R{m}(1:p(m),:), R{m}(p(m) + 1:p(m) + n(m),:), 0);

    % Pick direction with equal significance in both directions
    [K(m), theta(:,m)] = analyzeGSVs(C, S, dir);

    % Project the data against the direction k
    A{m} = R{m}*X(:,K(m));
    [sA_01, ind_sA_01] = sort(A{m}(1:p(m)));
    [sA_02, ind_sA_02] = sort(A{m}(p(m) + 1:p(m) + n(m)));

    p(m+1) = round(p(1)*frac^(m)); % keep top fraction of the mRNA levels
    n(m+1) = round(n(1)*frac^(m)); % keep top fraction of the copy number values

    m = m+1;
    if mod(p(m),2) == 0,
        botCGH{m-1} = ind_sA_01(1:(p(m)/2));
        botcDNA{m-1} = ind_sA_02(1:(n(m)/2));
        topCGH{m-1} = ind_sA_01(end-(p(m)/2)+1:end);
        topcDNA{m-1} = ind_sA_02(end-(n(m)/2)+1:end);
    else
        botCGH{m-1} = ind_sA_01(1:ceil(p(m)/2));
        botcDNA{m-1} = ind_sA_02(1:ceil(n(m)/2));
        topCGH{m-1} = ind_sA_01(end-floor(p(m)/2)+1:end);
        topcDNA{m-1} = ind_sA_02(end-floor(n(m)/2)+1:end);
    end

    % Re-form the data matrix after shaving the genes
    R{m} = [R{m-1}(topCGH{m-1},:); R{m-1}(botCGH{m-1},:); R{m-1}(p(m-1) + topcDNA{m-1},:); R{m-1}(p(m-1) + botcDNA{m-1},:)];
    inds{m} = [inds{m-1}(topCGH{m-1}); inds{m-1}(botCGH{m-1}); inds{m-1}(p(m-1) + topcDNA{m-1}); inds{m-1}(p(m-1) + botcDNA{m-1})];

    [sInd, iInd] = sort(inds{m});
    inds{m} = inds{m}(iInd);
    R{m} = R{m}(iInd,:);

end

num = length(inds{m-1})/2;
IDs = cellstr(IDs);
IDsRem.cgh = IDs(inds{m-1}(1:num));
IDsRem.cdna = IDs(inds{m-1}(num+1:end) - p(1));
cghRem = cghScaled(inds{m-1}(1:num),:);
cdnaRem = cdnaScaled(inds{m-1}(num+1:end) - p(1),:);
indx.cgh = inds{m-1}(1:num);
indx.cdna = inds{m-1}(num+1:end) - p(1);