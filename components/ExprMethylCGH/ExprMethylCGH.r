library(componentSkeleton)
#library(CNAmet)

execute <- function(cf){
    # Inputs
    exprMatrix  <- Matrix.read(get.input(cf, 'exprMatrix'))
    if (input.defined(cf, 'cghMatrix')){
        cghMatrix   <- Matrix.read(get.input(cf, 'cghMatrix'))
    } else {
        cghMatrix <- NULL
    }
    if (input.defined(cf, 'methylMatrix')){
        metMatrix   <- Matrix.read(get.input(cf, 'methylMatrix'))
    } else {
        metMatrix <- NULL
    }

    # Parameters
    perm.times   <- get.parameter(cf, 'permutation',     type='int')
    na.limit     <- get.parameter(cf, 'naLimit',         type='float')
    gainData     <- get.parameter(cf, 'gainData',        type='boolean')
    favor.syn    <- get.parameter(cf, 'favorSynergetic', type='boolean')
    strictChecks <- get.parameter(cf, 'strictChecks',    type='boolean')
    strictLim    <- get.parameter(cf, 'strictLim',       type='float')
    set.seed(67717040)

    # I/O check
    if (!is.null(cghMatrix) && all(rownames(exprMatrix) == rownames(cghMatrix)) == FALSE){
        write.error(cf, 'Row IDs do not match between exprMatrix and cghMatrix. ')
        return(INVALID_INPUT)
    }
    if (!is.null(metMatrix) && all(rownames(exprMatrix) == rownames(metMatrix)) == FALSE){
        write.error(cf, 'Row IDs do not match between exprMatrix and metMatrix. ')
        return(INVALID_INPUT)
    }
    if (nrow(exprMatrix) < 1){
        write.error(cf, 'Input is empty: exprMatrix')
        return(INVALID_INPUT)
    }

    # Analysis call    
    results <- CNAmetInt(exprMatrix      = exprMatrix, 
                         cghMatrix       = cghMatrix, 
                         methylMatrix    = metMatrix, 
                         perms           = perm.times, 
                         na.limit        = na.limit,
                         gainData        = gainData,
                         favorSynergetic = favor.syn,
                         strictChecks    = strictChecks,
                         strictLim       = strictLim)

    # Outputs
    CSV.write(get.output(cf, 'statistics'), results, first.cell = "ID")

    return(0)
}

CNAmetInt <- function(exprMatrix, cghMatrix, methylMatrix, perms = 1000, na.limit = 0.1, gainData = TRUE, favorSynergetic = TRUE, strictChecks = FALSE, strictLim = 0.05){
    # Create output matrix
    results <- matrix(nrow=nrow(exprMatrix),ncol=9)
    if (is.null(methylMatrix)){
        methylMatrix <- matrix(NA, nrow=nrow(exprMatrix), ncol=ncol(exprMatrix))
    }
    if (is.null(cghMatrix)){
        cghMatrix <- matrix(NA, nrow=nrow(exprMatrix), ncol=ncol(exprMatrix))
    }

    # Start algorithm
    for (i in 1:nrow(exprMatrix)){
        metM1 <- which(methylMatrix[i,] == 1)
        metM0 <- which(methylMatrix[i,] == 0)
        cghM1 <- which(cghMatrix[i,] == 1)
        cghM0 <- which(cghMatrix[i,] == 0)
        iSect <- intersect(names(metM0), names(cghM0))

        if (length(iSect) > 0 && favorSynergetic){
            metM0 <- metM0[which(names(metM0) %in% iSect)]
            cghM0 <- cghM0[which(names(cghM0) %in% iSect)]
        }

        # Compute error coefficient
        if (!is.null(methylMatrix) && !is.null(cghMatrix) && favorSynergetic){
            eps    <- length(intersect(metM1,cghM1))/ncol(exprMatrix)
            ol.pct <- length(union(metM1,cghM1))/ncol(exprMatrix)
        } else {
            eps    <- 1
            ol.pct <- NA
        }

        # Check NA threshold
        if ((length(which(is.na(exprMatrix[i, metM1]))) +
            length(which(is.na(exprMatrix[i, metM0]))))/ncol(exprMatrix) > na.limit ||
            (length(which(is.na(exprMatrix[i, cghM1]))) +
            length(which(is.na(exprMatrix[i, cghM0])))/ncol(exprMatrix) > na.limit)){
            results[i,] <- rep(NA,9)
            next
        }
        # Compute weights
        metWeight <- (mean(exprMatrix[i,metM1], na.rm=TRUE) - mean(exprMatrix[i, metM0], na.rm=TRUE))/
                     (sd(exprMatrix[i, metM1], na.rm=TRUE)  + sd(exprMatrix[i, metM0], na.rm=TRUE))
        cghWeight <- (mean(exprMatrix[i,cghM1], na.rm=TRUE) - mean(exprMatrix[i, cghM0], na.rm=TRUE))/
                     (sd(exprMatrix[i, cghM1], na.rm=TRUE)  + sd(exprMatrix[i, cghM0], na.rm=TRUE))

        # Require all correct direction (e.g., deleted and downregulated) scores to be positive
        if (!gainData){
            metWeight <- -1*metWeight
            cghWeight <- -1*cghWeight
        }

        if (strictChecks){
            if (length(intersect(metM1,cghM1)) > 1 && length(intersect(cghM1,metM0)) > 1){
                t <- t.test(exprMatrix[i,intersect(metM1,cghM1)],
                            exprMatrix[i,intersect(cghM1,metM0)],
                            alternative="two.sided")
                if (t$p.value > strictLim) {
                    metWeight <- NA
                    cghWeight <- NA
                }
            }
        }

        # Compute sum score
        sumScore <- (abs(metWeight) + abs(cghWeight))*eps
        pValues <- labelPermute(metM1=metM1, metM0=metM0, cghM1=cghM1,
                                cghM0=cghM0, exprVector=exprMatrix[i,], perms=perms,
                                metWeight=metWeight, cghWeight=cghWeight, sumScore=sumScore,
                                gainData=gainData, eps=eps)
        results[i,] <- c(metWeight, pValues[1], cghWeight, pValues[2], sumScore, pValues[3], eps, ol.pct, NA)
        
        # Dispose of NA scores to enable comparisons
        if (is.na(metWeight)) metWeight <- 0
        if (is.na(cghWeight)) cghWeight <- 0

        if (metWeight >= 0 && cghWeight < 0){
            results[i,] <- c(metWeight, pValues[1], NA, NA, NA, NA, eps, ol.pct, NA)
        } 
        if (cghWeight >= 0 && metWeight < 0){
            results[i,] <- c(NA, NA, cghWeight, pValues[2], NA, NA, eps, ol.pct, NA)
        }
        if (cghWeight <= 0 && metWeight <= 0){
            results[i,] <- c(NA, NA, NA, NA, NA, NA, NA, NA, NA)
        }
    }
    # FDR
    not.na  <- which(is.na(results[,6]))
    scores  <- results[-not.na,6]
    # method='fdr' is an alias for the Benjamini-Hochberg method
    pFDR    <- p.adjust(scores, method="fdr")
    results[-not.na, ncol(results)] <- pFDR

    colnames(results) <- c("MW","MWPvalue","CW","CWPvalue","score","scorePvalue","epsilon","coverage","fdr")
    rownames(results) <- rownames(exprMatrix)
    return(results)
}

labelPermute <- function(metM1, metM0, cghM1, cghM0, exprVector, perms, metWeight, cghWeight, sumScore, gainData, eps){
    pValues <- c(0*metWeight,0*cghWeight,0*sumScore)
    metWeightP <- NA
    cghWeightP <- NA
    sumScoreP  <- NA
    for (p in 1:perms){
        met1 <- sample(exprVector[union(metM1, metM0)], length(exprVector[union(metM1, metM0)]))
        met0 <- met1[(length(metM1)+1):(length(metM1)+length(metM0))]
        met1 <- met1[1:length(metM1)]
        cgh1 <- sample(exprVector[union(cghM1, cghM0)], length(exprVector[union(cghM1, cghM0)]))
        cgh0 <- cgh1[(length(cghM1)+1):(length(cghM1)+length(cghM0))]
        cgh1 <- cgh1[1:length(cghM1)]
        if (!is.na(metWeight)) metWeightP <- (mean(met1, na.rm=TRUE) - mean(met0, na.rm=TRUE))/(sd(met1, na.rm=TRUE) + sd(met0, na.rm=TRUE))
        if (!is.na(cghWeight)) cghWeightP <- (mean(cgh1, na.rm=TRUE) - mean(cgh0, na.rm=TRUE))/(sd(cgh1, na.rm=TRUE) + sd(cgh0, na.rm=TRUE))
        if (!gainData){
            metWeightP <- -1*metWeightP
            cghWeightP <- -1*cghWeightP
        }
        sumScoreP <- (abs(metWeightP) + abs(cghWeightP))*eps
        if (!is.na(metWeight) && !is.na(metWeightP)){
            if (metWeightP > metWeight) pValues[1] <- pValues[1]+1
        }
        if (!is.na(cghWeight) && !is.na(cghWeightP)) {
            if (cghWeightP > cghWeight) pValues[2] <- pValues[2]+1
        }
        if (!is.na(sumScoreP)) {
            if (sumScoreP  > sumScore)  pValues[3] <- pValues[3]+1
        }
    }
    pValues <- pValues/perms
    return(pValues)
}

main(execute)
