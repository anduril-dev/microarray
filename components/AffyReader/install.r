argv <- commandArgs(TRUE)
if (length(argv) != 1) {
    cat('Usage: install.r <component-root-dir>\n')
    cat('  Copies a CEL file from the affydata package into test directory.\n')
    quit(status=1)
}
target <- file.path(argv[[1]], 'testcases', 'case1', 'input', 'affy', 'chip1.cel')
if (any( !file.exists(target)) ) {    
    library(affy)    
    celpath <- system.file('celfiles', package='affydata')
    filename <- list.celfiles(path=celpath, full.names=TRUE)[[1]]

    file.copy(filename, target)
}
invisible(NULL)
