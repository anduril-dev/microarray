library(componentSkeleton)

run.firma <- function(){

        verbose <- Arguments$getVerbose(-8, timestamp=TRUE)

        chipType <- "HuEx-1_0-st-v2"
        cdf <- AffymetrixCdfFile$byChipType(chipType)

        cs <- AffymetrixCelSet$byName("data", cdf=cdf)

        bc <- RmaBackgroundCorrection(cs, tag="coreR2")
        csBC <- process(bc,verbose=verbose)

        qn <- QuantileNormalization(csBC, typesToUpdate="pm")

        csN <- process(qn, verbose=verbose)

        plmEx <- ExonRmaPlm(csN, mergeGroups=FALSE)

        fit(plmEx, verbose=verbose)

        cesEx <- getChipEffectSet(plmEx)
        exFit <- extractDataFrame(cesEx, units=NULL, addNames=TRUE)
	
        return(exFit)
}



runFIRMA <- function(temp, cdf, filePath, samples, id.type){

	library(aroma.affymetrix)

	setwd(temp)
	
	# make directories
	system("mkdir rawData")
	system("mkdir rawData/data")
	system(paste("mkdir rawData/data/",cdf,sep=""))
	files <- paste(filePath,collapse=" ")
	system(paste("cp ",files," rawData/data/",cdf,sep=""))
	system("mkdir annotationData")
	system("mkdir annotationData/chipTypes")
	system(paste("mkdir annotationData/chipTypes/",cdf,sep=""))
	setwd(paste("annotationData/chipTypes/",cdf,sep=""))
	system("wget http://www.aroma-project.org/data/annotationData/chipTypes/HuEx-1_0-st-v2/HuEx-1_0-st-v2.cdf.gz ")
	system("gunzip HuEx-1_0-st-v2.cdf.gz")
	setwd(temp)

	# run firma
	ps.expr <- run.firma()

	ps.expr <- ps.expr[,-c(2,3,4,5)]
	id <- samples[,'SampleID']
	names(id) <- gsub(".CEL","",samples[,'Filename'])
  	colnames(ps.expr)[-1] <- id[colnames(ps.expr)][-1]
	colnames(ps.expr)[1] <- id.type

	flag <- is.na(ps.expr[,2])
	detach("package:aroma.affymetrix",unload=TRUE,force=TRUE)
	detach("package:componentSkeleton",unload=TRUE,force=TRUE)
	library(componentSkeleton)

	return(ps.expr[!flag,])
}


execute <- function(cf) {
  library(gcrma)
  library(plier)

  curr.wd <- getwd()
  write.log(cf,curr.wd)

  # Read input parameters...
  cel.path               <- get.input(cf, 'affy')
  array.type             <- get.parameter(cf, 'arrayType')
  cel.chip               <- get.parameter(cf, 'cdf')
  id.type                <- get.parameter(cf, 'idType')
  skip.cel.files         <- get.parameter(cf, 'skipCELFiles')
  normalization.method   <- get.parameter(cf,'normalizationMethod')
  quantile.normalization <- get.parameter(cf, 'quantileNormalization', type = "boolean")
  columns                <- c("SampleID", "Filename", "Description")
  samples                <- CSV.read(get.input(cf, 'sampleNames'), expected.columns=columns)
  suffix                 <- get.parameter(cf, 'suffixLength', 'int')
  metaData               <- get.parameter(cf, 'metaData')

  write.log(cf, "Reading CEL files...")
 
  # Read cel files defined in sample definition file from cel.path directory
  samples           <- samples[!is.na(samples[,"Filename"]),]
  all.files         <- list.files(cel.path)
  cel.files         <- all.files[grep("[.]CEL", all.files, ignore.case=TRUE)]
  
  if(!get.parameter(cf,'acceptCELOnly', type = 'boolean'))
	  cel.files = all.files
  
  defined.cel.files <- cel.files[cel.files %in% samples[,"Filename"]]
  skipped.cel.files <- setdiff(cel.files, defined.cel.files)

  if(length(skipped.cel.files) > 0) {
    if(skip.cel.files) {
      write.log(cf, paste("Skipped CEL-files not present in the sample definition file: ", paste(skipped.cel.files, collapse=" "), collapse=""))
    }else {
      write.error(cf, paste("Found CEL-files not defined in the sample definition file: ", paste(skipped.cel.files,collapse=" "), collapse=""))
      return(PARAMETER_ERROR)    
    }
  }

  if (length(defined.cel.files) == 0) {
     write.error(cf, paste("No CEL-files matching the sample definition file:",
                           paste(samples[,"Filename"], collapse=" "),
                           "found in the soure file directory", cel.path, sep=" "))  
     return(PARAMETER_ERROR)
  }

  eSet         <- ReadAffy(filenames=defined.cel.files, celfile.path=cel.path)
  if(normalization.method!="firma"){
	 eSet@cdfName <- cel.chip
  }

  # Write raw expression matrix.
  write.log(cf, "Writing exprRaw...")
  raw           <- log2(exprs(eSet))
  rownames(raw) <- paste('probe',rownames(raw),sep='')
  if(any(duplicated(samples[,"Filename"]))) {
    write.error(cf, paste("Found duplicated values in Filename column in the sampleNames input file: '", paste(samples[duplicated(samples[,"Filename"]), "Filename"], collapse=","), "'", sep=""))
    return(PARAMETER_ERROR) 
  }
  if(any(duplicated(samples[,"SampleID"]))) {
    write.error(cf, paste("Found duplicated values in SampleID column in the sampleNames input file: '", paste(samples[duplicated(samples[,"SampleID"]), "SampleID"], collapse=","), "'", sep=""))
    return(PARAMETER_ERROR) 
  }
  colnames(raw) <- samples[match(colnames(raw), samples[,"Filename"]), "SampleID"]
  CSV.write(get.output(cf, 'exprRaw'), raw)
  rm(raw)

  # Create sample group table
  groupT <- SampleGroupTable.new()
  for (i in 1:nrow(samples)) {
    sample.id <- samples[i,'SampleID']
    desc      <- samples[i,'Description']
    groupT    <- SampleGroupTable.add.group(groupT, sample.id, type=SAMPLE.GROUP.TABLE.SAMPLE, sample.id, desc)
  }

  write.log(cf, sprintf("Calculating %s normalization...", normalization.method))
  if(array.type=="geneArray"){
  	eSet <- switch(normalization.method,
                 "rma"   = exprs(rma(eSet, normalize = quantile.normalization)), 
                 "mas5"  = log2(exprs(mas5(eSet))),
                 "dChip" = log2(exprs(expresso(eSet,normalize.method="invariantset", bg.correct=FALSE, pmcorrect.method="pmonly", summary.method="liwong"))),
                 "plier" = exprs(justPlier(eset=eSet, normalize = quantile.normalization)),
                 "gcrma" = exprs(gcrma(eSet, type='mm')))
    if (suffix > 0) {
       genes          <- rownames(eSet)
       genes          <- sapply(genes, function(name) {substr(name,1,nchar(name)-suffix)})
       rownames(eSet) <- as.character(genes)
    }

  }else if(array.type=="exonArray"){
	eSet <- switch(normalization.method,
                 "rma"   = exprs(rma(eSet, normalize = quantile.normalization)), 
                 "plier" = exprs(justPlier(eset=eSet, normalize = quantile.normalization, norm.type = "pmonly", usemm = FALSE)),
		 "firma" = runFIRMA(temp=metaData, cdf=cel.chip, filePath=paste(cel.path,defined.cel.files,sep="/"),samples=samples, id.type=id.type))
  }

  write.log(cf, "Writing result outputs...")
  if(normalization.method!="firma"){
	write.log(cf, normalization.method)
  	rownames(samples) <- samples[,'Filename']
  	colnames(eSet)    <- samples[colnames(eSet),'SampleID']
	CSV.write(get.output(cf, 'expr'), eSet, first.cell=id.type)
  }else{
	setwd(curr.wd)
	system(paste("rm -rf ",metaData,sep=""))
	write.log(cf, normalization.method)
	write.log(cf,paste(nrow(eSet)))
	write.table(eSet,get.output(cf,'expr'),sep="\t",quote=FALSE,row.names=FALSE)
  }

  SampleGroupTable.write(get.output(cf, 'groups'), groupT)
  write.log(cf, "Exit...")

  return(0)
}

main(execute)
