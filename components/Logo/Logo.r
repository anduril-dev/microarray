library(componentSkeleton)
library(seqLogo)

grep.indices <- function(keys, source) {
  x <- numeric()
  for (k in keys) {
      x <- union(x, which(source==k))
  }
  x
}

execute <- function(cf) {
  myName  <- get.metadata(cf,  'instanceName')
  motifs  <- get.input(cf,     'motifs')
  out.dir <- get.output(cf,    'report')
  caption <- get.parameter(cf, 'caption')
  anSel   <- get.parameter(cf, 'annotCols')
  figN    <- get.parameter(cf, 'figN', type='int')
  dir.create(out.dir, recursive=TRUE)

  if (input.defined(cf, 'annot')) {
     motifInfo <- AnnotationTable.read(get.input(cf, 'annot'))
     if (nchar(anSel) > 1) {
        info.cols <- grep.indices(unlist(strsplit(anSel,",")), colnames(motifInfo))
     } else {
        info.cols <- 2:ncol(motifInfo)
     }
  } else {
     motifInfo <- NULL
  }

  if (input.defined(cf, 'subset')) {
     subselect <- CSV.read(get.input(cf, 'subset'))
     ssCol     <- get.parameter(cf, 'selectCol')
     if (nchar(ssCol) > 1) {
         subselect <- subselect[,ssCol]
     } else {
         subselect <- subselect[,1]
     }
     # Check for '.motif' files also
     subselect <- c(subselect, paste(subselect,".motif",sep=""))
  } else {
     subselect <- NULL
  }

  width    <- 400
  height   <- 300
  tex      <- character()
  figS     <- '\\begin{figure}[htb]\\centering'
  figE     <- '\\end{figure}\\FloatBarrier{}'
  figGap   <- '\\hspace{0.25cm}'
  tex      <- c(tex, figS)
  ic       <- 1
  fileList <- list.files(motifs)
  if (!is.null(subselect)) {
     fileList <- intersect(fileList,subselect)
  }
  fileList <- sort(fileList)
  for (filename in fileList) {
      write.log(cf, sprintf('Processing motif file: %s', filename))
      mName <- substr(filename, 1, gregexpr("\\.", filename)[[1]]-1)

      # Read allele frequences
      data <- read.table(file   = paste(motifs,'/', filename, sep=''),
                         header = TRUE)
      data <- t(as.matrix(data))
      cSum <- colSums(data)
      for (i in 1:length(cSum)) {
          data[,i] <- data[,i]/cSum[i]
      }
      data <- makePWM(data)

      # Create logo
      fname   <- sprintf('%s-%s.png', myName, mName)
      outname <- file.path(out.dir, fname)
      png.open(outname, width=width, height=height)
      seqLogo(data)
      png.close()

      mAnnot <- motifInfo[motifInfo[,1]==mName,]
      if (is.null(mAnnot) || (nrow(mAnnot) < 1)) {
         mAnnot <- latex.quote(mName)
      } else {
         mAnnot <- mAnnot[,info.cols,      drop=FALSE]
         mAnnot <- mAnnot[,!is.na(mAnnot), drop=FALSE]
         mAnnot <- paste(latex.quote(mName),
                         paste(latex.quote(names(mAnnot)),
                               "=",
                               latex.quote(as.character(mAnnot)),
                               collapse=", ", sep=""),
                         sep=":\\ ")
      }

      tex <- c(tex,
               sprintf('\\subfloat[\\tiny{%s}]{\\includegraphics[keepaspectratio=true,width=%scm]{%s}\\hyperdef{logo}{%s-%s}{}}',
                       mAnnot, '5.5', fname, myName, mName))
      if (ic%%figN == 0) {
         tex <- c(tex, figE, figS, figGap)
      } else
      if (ic%%3 == 0) {
         tex <- c(tex, '\\\\')
      } else
         tex <-c(tex, figGap)
      ic  <- ic + 1
  }
  tex <- c(tex, sprintf('\\caption{%s}\\label{fig:%s}', caption, myName), figE)

  latex.write.main(cf, 'report', tex)
  return(0)
}

main(execute)
