library(componentSkeleton)

width.pixels <- 1000

get.spatial.matrix <- function(chip.annotation, expr, index) {
    ids <- rownames(expr)
    rows <- AnnotationTable.get.vector(chip.annotation, 'Row', ids)
    cols <- AnnotationTable.get.vector(chip.annotation, 'Col', ids)
    matr <- matrix(NA, max(rows), max(cols))
    
    for (row.num in 1:nrow(expr)) {
        matr[rows[row.num], cols[row.num]] <- expr[row.num, index]
    }
    
    return(matr)
}

make.plots <- function(cf, chip.annotation, expr, out.dir, filename.prefix, sample.names, plot.title, width.pixels, height.pixels, width.cm,
        limits=NULL, color=NULL, use.image=FALSE, low.quantile=0.05, high.quantile=0.95) {
#	 if (is.null(color)) color <- rev(rainbow(32, start=0, end=2/6, gamma=2.2))
    if(require(RColorBrewer)) {
        if (is.null(color)) color <- rev(brewer.pal(9,'GnBu'))
        fgcol=rgb(1, 1, 1)
        bgcol=rgb(0, 0, 0)
    } else {
        if (is.null(color)) color <- gray.colors(32, 0.3, 1, 2.2)
        fgcol=rgb(0, 0, 0)
        bgcol=rgb(0.5, 0, 0)
    }
    
    tex <- c()
    
    for (i in 1:ncol(expr)) {
        spatial.matrix <- get.spatial.matrix(chip.annotation, expr, i)
        
        sample.name <- sample.names[i]
        base.name <- sprintf('%s-%s%s.png',
            get.metadata(cf, 'instanceName'), filename.prefix, sample.name)
        filename <- file.path(out.dir, base.name)
        
        if (nchar(plot.title) == 0) main <- sample.name
        else main <- sprintf('%s (%s)', plot.title, sample.name)
        
        png.open(filename, width=width.pixels, height=height.pixels)
        par(fg=fgcol,bg=bgcol)
        
        if (is.null(limits)) {
            this.limits <- quantile(c(spatial.matrix), probs=c(low.quantile, high.quantile),
                    na.rm=TRUE, names=FALSE, type=8)
            low <- this.limits[1]
            high <- this.limits[2]
            spatial.matrix[spatial.matrix < low] <- low
            spatial.matrix[spatial.matrix > high] <- high
        }
        else {
            low <- limits[1]
            high <- limits[2]
        }
        
#		spatial.matrix[is.na(spatial.matrix)] <- median(c(spatial.matrix), na.rm=TRUE)
        
        latex.caption <- main
        latex.caption <- sprintf('%s. Quantile limits: [%.1f, %.1f]. Number of empty spots: %d.',
            latex.caption, low, high,
            length(which(is.na(c(spatial.matrix)))))
        
        image(1:nrow(spatial.matrix), 1:ncol(spatial.matrix), spatial.matrix, main=main, col=color, xlab='Row', ylab='Column')
        
        png.close()
        tex <- c(tex, latex.figure(base.name, caption=latex.caption, image.width=width.cm, position='H'))
    }
    
    tex <- c(tex, '\\clearpage')
    return(tex)
}

execute <- function(cf) {
    gene.annotation <- AnnotationTable.read(get.input(cf, 'geneAnnotation'))
    
    width.cm <- get.parameter(cf, 'width', 'float')
    height.cm <- get.parameter(cf, 'height', 'float')

    if (width.cm == 0) {
        write.error(cf, 'Parameter width is 0')
        return(INVALID_INPUT)
    }
    if (height.cm == 0) {
        write.error(cf, 'Parameter height is 0')
        return(INVALID_INPUT)
    }
    
    low.quantile <- get.parameter(cf, 'lowQuantile', 'float')
    high.quantile <- get.parameter(cf, 'highQuantile', 'float')
    
    if (low.quantile < 0 || low.quantile > 1) {
        write.error(cf,
            sprintf('lowQuantile must be between 0 and 1 (is: %s)', low.quantile))
        return(PARAMETER_ERROR)
    }
    if (high.quantile < 0 || high.quantile > 1) {
        write.error(cf,
            sprintf('highQuantile must be between 0 and 1 (is: %s)', high.quantile))
        return(PARAMETER_ERROR)
    }
    
    aspect <- width.cm / height.cm
    height.pixels <- as.integer(round(width.pixels / aspect))
    
    sample.names <- NULL
    
    out.dir <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)
    
    # input name, output name, title parameter, limits
    params <- list(
        list('channel1', 'channelTitle1', NULL),
        list('channel2', 'channelTitle2', NULL),
        list('logratio', 'logratioTitle', NULL)) #c(-2, 2)))
        
    tex <- c()
    
    for (param in params) {
        input.name <- param[[1]]
        if (!input.defined(cf, input.name)) next
        
        filename.prefix <- paste(param[[1]], '-', sep='')
        plot.title <- get.parameter(cf, param[[2]], 'string')
        limits <- param[[3]]
        
        if (is.null(plot.title) || nchar(plot.title) == 0) {
            write.error(cf, sprintf('Title parameter %s must not be empty', param[[2]]))
            return(INVALID_INPUT)
        }
        
        expr <- LogMatrix.read(get.input(cf, input.name))
        sample.names <- colnames(expr)
        tex <- c(tex,
            sprintf('\\subsection{Spatial plots: %s}', plot.title),
            '',
            'Black color denotes low value and white color high value.',
            sprintf('All values below quantile %.2f are black and all values above', low.quantile),
            sprintf('quantile %.2f are white.', high.quantile),
            'Red color represents locations that have been filtered out, e.g. control probes.',
            '',
            make.plots(cf, gene.annotation, expr, out.dir, filename.prefix, sample.names,
                plot.title, width.pixels, height.pixels, width.cm, limits=limits,
                low.quantile=low.quantile, high.quantile=high.quantile))
                
        if (input.name == 'logratio') {
            weight <- matrix(1, nrow(expr), ncol(expr))
            colnames(weight) <- colnames(expr)
            rownames(weight) <- rownames(expr)
            weight[is.na(expr)] <- 0
            sample.names <- colnames(weight)
            tex <- c(tex,
                sprintf('\\subsection{Spatial plots: filtered spots}'),
                '',
                make.plots(cf, gene.annotation, weight, out.dir, 'weights-', sample.names,
                    'Filtered spots', width.pixels, height.pixels, width.cm, limits=c(0,1), use.image=TRUE,
                    low.quantile=low.quantile, high.quantile=high.quantile))
        }
    }
    
    latex.write.main(cf, 'report', tex)
    return(0)
}

main(execute)
