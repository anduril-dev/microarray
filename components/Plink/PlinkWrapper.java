import java.io.File;
import java.io.IOException;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;


public class PlinkWrapper extends SkeletonComponent {

	private enum PlinkNames{
		LD("--r2", new String[]{"plink.ld"}),
		HAPLOBLOCK("--blocks", new String[]{"plink.blocks", "plink.blocks.det"}),
		HARDY("--hardy", new String[]{"plink.hwe"});
		
		private String command; 
		private String [] resFiles;
		
		PlinkNames(String command, String [] resFiles){
			this.command = command;
			this.resFiles = resFiles;
		}
	}
	
	public static final String PLINK_LOGFILE = "plink.log";
	public static final int NUM_OF_OUTPUTS = 5;
	
	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
	
		//construct the command
		StringBuffer command = new StringBuffer(cf.getParameter("Plink")); 
		command.append(" ");
		
		
		//no map file, use only ped name
		if(cf.getInput("mapFile") == null){
			if(cf.getParameter("dataType").equals("binary")) {
				command.append("--bfile ");
			}else{
				command.append("--file ");
			}
			command.append(cf.getInput("pedFile").getPath().substring(0, cf.getInput("pedFile").getPath().lastIndexOf(".")));
			command.append(" ");
		}else{
			if(cf.getParameter("dataType").equals("binary")) {
				System.err.print("No separate map allowed with binary format. Use only ped file name.");
				return ErrorCode.PARAMETER_ERROR;
			}
			command.append("--ped ");
			command.append(cf.getInput("pedFile").getAbsolutePath());
			command.append(" ");
			command.append("--map ");
			command.append(cf.getInput("mapFile").getAbsolutePath());
			command.append(" ");
		}
		
		String [] outputFiles = cf.getParameter("outputFiles").split(",");
		
		if(cf.getParameter("type").equals("ld")){
			command.append(PlinkNames.LD.command);
			command.append(" ");
			outputFiles = PlinkNames.LD.resFiles;
			
		}else if(cf.getParameter("type").equals("haploblock")){
			command.append(PlinkNames.HAPLOBLOCK.command);
			command.append(" ");
			outputFiles = PlinkNames.HAPLOBLOCK.resFiles;
			
		}else if(cf.getParameter("type").equals("hardy")){
			command.append(PlinkNames.HARDY.command);
			command.append(" ");
			outputFiles = PlinkNames.HARDY.resFiles;
			
		}
		String para = cf.getParameter("parameters");
		
		//replace with the files
		for(int i = 1; i <= 3; i++){
			if(cf.getInput("file"+i) != null){
				para = para.replace("@f"+i+"@", cf.getInput("file"+i).getPath());
			}
		}
		
		command.append(para);
		
		File workingDir = cf.getOutput("result1").getParentFile();
		
		runCommand(command.toString(), workingDir);
		renameFiles(outputFiles, cf);
		
		//finally set the log-file name
		new File(cf.getOutput("result1").getParentFile()
				+ File.separator + PLINK_LOGFILE).renameTo(cf.getLogFile());
		
		return ErrorCode.OK;
	}

	private void runCommand(String command, File workingDir)throws IOException{
		System.out.println("Execute "+command);
			IOTools.launch(command,
				workingDir, null, "", "");
		
	}
	private void renameFiles(String [] resFiles, CommandFile cf) throws IOException{
		int i = 1;
		while(i <= resFiles.length && i <= NUM_OF_OUTPUTS){
			
			System.out.println("rename "+cf.getOutput("result" + i).getParentFile()
					+ File.separator + resFiles[i - 1] +" to "+cf.getOutput("result" + i));
			
			new File(cf.getOutput("result" + i).getParentFile()
					+ File.separator + resFiles[i - 1]).renameTo(cf.getOutput("result" + i));
			i++;
		}
		while(i <= NUM_OF_OUTPUTS){
			cf.getOutput("result"+i).createNewFile();
			i++;
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new PlinkWrapper().run(args);
	}

}
