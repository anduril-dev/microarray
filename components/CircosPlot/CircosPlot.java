import java.util.*;
import java.io.*;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.LatexTools;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;

/**
 * This component plots genomic data in circular form known as 
 * circos plots. The component modifies input data to suit the circos
 * perl library which is executed as an external program.
 * 
 * @author Riku Louhimo
 *
 */
public class CircosPlot extends SkeletonComponent {
 
    private String KARYOTYPE_FILE_NAME = "karyotype_hg19.txt";
    private String[] COMMAND = new String[4];
    private File   KARYOTYPE = null;
    private File   ANNOTATION = null;
    private File   PLOT_ANNOTATION = null;
    private File   PLOT_PARAMS = null;
    private File   LINKS = null;
    private File   HIGHLIGHTS = null;
    private File   outputDir = null;
    private File   TEMPDIR = null;
    private ArrayList<File> DATA = new ArrayList<File>();
    private String[] colors = {"green","dgreen","lime","yellow",
                               "dyellow","orange","dorange","red",
                               "dred","purple","dpurple"};
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        COMMAND[0] = cf.getParameter("circos");
        COMMAND[1] = "-conf";
        COMMAND[2] = cf.getOutput("circos").getAbsolutePath();
        COMMAND[3] = "-silent";
        
        if (cf.inputDefined("karyotype")) {
            KARYOTYPE = cf.getInput("karyotype");
        } else {
            KARYOTYPE = new File(KARYOTYPE_FILE_NAME);        
        }
        
        TEMPDIR = cf.getTempDir();
        
        String type     = cf.getParameter("type");
        double min      = cf.getDoubleParameter("min");
        double max      = cf.getDoubleParameter("max");
        double fontSize = cf.getDoubleParameter("fontSize");
        boolean bands   = cf.getBooleanParameter("drawBands");
        boolean autoScale   = cf.getBooleanParameter("autoYScale");
        boolean commonScale = cf.getBooleanParameter("commonYScale");
        String plotScale    = cf.getParameter("plotScale");
        boolean altColors = cf.getBooleanParameter("alternateColors");
        String circosLibrary = cf.getParameter("circosLibrary");
        
        outputDir = cf.getOutput("plot");
        if (!outputDir.exists() && !outputDir.mkdirs())
            throw new IOException("Cannot create output folder: "+outputDir.getCanonicalPath());
        
        List<String> latexOut = LatexTools.formatFigure(cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME)+"-circos-output.png",
                                                        null,
                                                        "Chromosomal circos plot of microarray data.",
                                                        "!ht", 9.0);
        LatexTools.writeDocument(outputDir, latexOut);
        
        /* Input data to-be-plotted. */
        DATA.add(removeHeaders(cf.getInput("data"), TEMPDIR, "data"));
        if (cf.inputDefined("data2")) DATA.add(removeHeaders(cf.getInput("data2"), TEMPDIR, "data2"));
        if (cf.inputDefined("data3")) DATA.add(removeHeaders(cf.getInput("data3"), TEMPDIR, "data3"));
        if (cf.inputDefined("data4")) DATA.add(removeHeaders(cf.getInput("data4"), TEMPDIR, "data4"));
        if (cf.inputDefined("data5")) DATA.add(removeHeaders(cf.getInput("data5"), TEMPDIR, "data5"));
        if (cf.inputDefined("data6")) DATA.add(removeHeaders(cf.getInput("data6"), TEMPDIR, "data6"));
        if (cf.inputDefined("data7")) DATA.add(removeHeaders(cf.getInput("data7"), TEMPDIR, "data7"));
        if (cf.inputDefined("data8")) DATA.add(removeHeaders(cf.getInput("data8"), TEMPDIR, "data8"));
        if (cf.inputDefined("data9")) DATA.add(removeHeaders(cf.getInput("data9"), TEMPDIR, "data9"));
        if (cf.inputDefined("data10")) DATA.add(removeHeaders(cf.getInput("data10"), TEMPDIR, "data10"));
        if (cf.inputDefined("data11")) DATA.add(removeHeaders(cf.getInput("data11"), TEMPDIR, "data11"));
        
        
        //automatically find the scale for data
        boolean findLimits = false;
        if (autoScale && commonScale){
            double [] limits = findUpperLower(DATA);
            min = limits[0];
            max = limits[1];
        } else if (autoScale){
            findLimits = true;
        }
        
        //then find positions for each plot
        double [] plotPosition = new double[DATA.size()*2];
        
        if (plotScale.matches("constant=.*") || plotScale.equals("relative")){
            double pos = 0;
            if (plotScale.matches("constant=.*")){
                pos = Double.parseDouble(plotScale.split("=")[1]);
            } else {
                pos = 0.9 / (double) plotPosition.length;
            }
            for (int i = 0; i < plotPosition.length; i = i+2){
                int index = i /2;
                plotPosition[i]   = 0.9 - ( index * pos);
                plotPosition[i+1] = 0.9 - ( (index+1) * pos) + 0.01;
            }
        } else {
            String [] positions = plotScale.split(",");
            if (positions.length != plotPosition.length){
                cf.writeError("Parameter PlotScale does not match to number of data files.");
                return ErrorCode.PARAMETER_ERROR;
            }
            for(int i = 0; i < plotPosition.length; i++){
                plotPosition[i] = Double.parseDouble(positions[i]);
            }
        }
        
        /* Data annotation file as in DNARegion. */
        if (cf.inputDefined("dataAnnotation")) ANNOTATION      = removeHeaders(cf.getInput("dataAnnotation"), TEMPDIR, "dataAnnotation");
        if (cf.inputDefined("plotAnnotation")) PLOT_ANNOTATION = removeHeaders(cf.getInput("plotAnnotation"), TEMPDIR, "plotAnnotation");
        if (cf.inputDefined("plotParams"))     PLOT_PARAMS     = cf.getInput("plotParams");
        if (cf.inputDefined("links"))          LINKS           = removeHeaders(cf.getInput("links"), TEMPDIR, "links");
        if (cf.inputDefined("highlights"))     HIGHLIGHTS      = removeHeaders(cf.getInput("highlights"), TEMPDIR, "highlights");
            
        createCircos(cf, KARYOTYPE, PLOT_PARAMS, PLOT_ANNOTATION, LINKS, DATA, HIGHLIGHTS,
                            min, max, fontSize, type, findLimits, plotPosition, altColors, circosLibrary);
        try {
            createIdeogram(cf.getOutput("ideogram"), bands);
        } catch (IOException e) {
            cf.writeError(e);
        }
        createTicks(cf);
        
        /* Execute circos library. */
        int success = IOTools.launch(COMMAND, null, null, "circos ", "circos ", null, null);
        if (success > 0){
            return ErrorCode.ERROR;
        }
        return ErrorCode.OK;
    }
    
    private File removeHeaders(File in, File tmpDir, String fileName) throws IOException{
        File out = new File(tmpDir.getAbsolutePath()+"/"+fileName);
        BufferedReader brIn = new BufferedReader(new FileReader(in));
        BufferedWriter bwOut = new BufferedWriter(new FileWriter(out));
        brIn.readLine(); //skip first line
        try {
            String line;
            while((line = brIn.readLine()) != null){
                bwOut.write(line);
                bwOut.newLine();
            }
        } catch (IOException ie){
            throw(ie);
        } finally {
            brIn.close();
            bwOut.close();
        }
        return out;
    }
    
        
    private void createTicks(CommandFile cf){
        String show_grid = "no";
        if (cf.getBooleanParameter("drawGrid")) show_grid = "yes";
        
        try {
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(cf.getOutput("ticks"))); 
            out.write("show_grid           = ");
            out.write(show_grid);
            out.append('\n').write("show_ticks          = yes");
            out.append('\n').write("show_tick_labels    = yes");
            out.append('\n').write("<ticks>");
            out.append('\n').write("tick_separation      = 3p");
            out.append('\n').write("label_separation     = 10p");
            out.append('\n').write("radius               = dims(ideogram,radius_outer)");
            out.append('\n').write("multiplier           = 1e-6");
            out.append('\n').write("grid_start     = 0.5r");
            out.append('\n').write("grid_end       = 0.975r");
            out.append('\n').write("grid_color     = dgrey");
            out.append('\n').write("grid_thickness = 2p");
            out.append('\n').write("<tick>");
            out.append('\n').write("grid = ");
            out.write(show_grid);
            out.append('\n').write("spacing        = 10u");
            out.append('\n').write("size           = 8p");
            out.append('\n').write("thickness      = 2p");
            out.append('\n').write("color          = black");
            out.append('\n').write("show_label     = yes");
            out.append('\n').write("label_size     = 20p");
            out.append('\n').write("label_offset   = 5p");
            out.append('\n').write("format         = %d");
            out.append('\n').write("</tick>");
            out.append('\n').write("</ticks>");
            out.close();
        } catch (IOException e){
            cf.writeError(e);
        }
    }
    
    private void createIdeogram(File ideogram, boolean bands) throws IOException{
        String show_bands = "no";
        if (bands) show_bands = "yes";
        
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(ideogram));
        out.write("<ideogram>");
        out.append('\n').write("<spacing>");
        out.append('\n').write("default = 10u");
        out.append('\n').write("break   = 2.5u");
        out.append('\n').write("axis_break_at_edge = yes");
        out.append('\n').write("axis_break         = yes");
        out.append('\n').write("axis_break_style   = 2");
        out.append('\n').write("<break_style 1>");
        out.append('\n').write("stroke_color = black");
        out.append('\n').write("fill_color   = blue");
        out.append('\n').write("thickness    = 0.25r");
        out.append('\n').write("stroke_thickness = 2");
        out.append('\n').write("</break>");
        out.append('\n').write("<break_style 2>");
        out.append('\n').write("stroke_color     = black");
        out.append('\n').write("stroke_thickness = 3");
        out.append('\n').write("thickness        = 1.5r");
        out.append('\n').write("</break>");
        out.append('\n').write("</spacing>");
        out.append('\n').write("thickness        = 100p");
        out.append('\n').write("stroke_thickness = 2");
        out.append('\n').write("stroke_color     = black");
        out.append('\n').write("fill             = yes");
        out.append('\n').write("fill_color       = black");
        out.append('\n').write("radius         = 0.75r");
        out.append('\n').write("show_label     = yes");
        out.append('\n').write("label_with_tag = yes");
        out.append('\n').write("label_font     = condensedbold");
        if (PLOT_ANNOTATION == null){
            out.append('\n').write("label_radius   = dims(ideogram,radius) + 75p");
        } else {
            out.append('\n').write("label_radius = dims(ideogram,radius_inner) - 100p");
        }
        out.append('\n').write("label_size     = 40p");
        out.append('\n').write("band_stroke_thickness = 2");
        out.append('\n').write("show_bands            = ");
        out.write(show_bands);
        out.append('\n').write("fill_bands            = ");
        out.write(show_bands);
        out.append('\n').write("</ideogram>");
        out.close();
    }
    
    private void createCircos(CommandFile cf, File Karyotype, 
                              File PlotParams, File PlotAnnotation, File Links,
                              ArrayList<File> Data, File Highlights, 
                              double min, double max, double fontSize,
                              String type, boolean findLimits, double [] plotPositions,
                              boolean altColors, String circosLibrary) {
        if (circosLibrary.equals("")) circosLibrary = "etc";
        try {
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(cf.getOutput("circos")));
            out.write("<colors>");
            out.append('\n').write(String.format("<<include %s/colors.conf>>", circosLibrary));
            out.append('\n').write(String.format("<<include %s/brewer.conf>>", circosLibrary));
            out.append('\n').write("</colors>");
            out.append('\n').write("<fonts>");
            out.append('\n').write(String.format("<<include %s/fonts.conf>>", circosLibrary));
            out.append('\n').write("</fonts>");
            out.append('\n').write("<<include ideogram.txt>>"); //+cf.getOutput("ideogram")+">>");
            out.append('\n').write("<<include ticks.txt>>"); //+cf.getOutput("ticks")+">>");
            out.append('\n').write("karyotype = "+Karyotype.getAbsolutePath());
            
            out.append('\n').write("<image>");
            out.append('\n').write("dir  = "+cf.getOutput("plot"));
            out.append('\n').write("file = "+cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME)+"-circos-output.png");
            out.append('\n').write("radius*         = 2000p");
            out.append('\n').write("background     = white");
            out.append('\n').write("angle_offset   = -90");
            out.append('\n').write("24bit = yes");
            out.append('\n').write("auto_alpha_colors = yes"); 
            out.append('\n').write("auto_alpha_steps  = 5"); 
            out.append('\n').write("</image>");
            
            // Start of plots block
            out.append('\n').write("<plots>");
            
            if (PlotParams == null){
                int posIndex = 0;
                int colIndex = 0;
                for (File datafile : Data){
                    if (findLimits){
                        ArrayList<File> fileList = new ArrayList<File>();
                        fileList.add(datafile);
                        double [] limits = findUpperLower(fileList);
                        min = limits[0];
                        max = limits[1];
                    }
                    out.append('\n').write("<plot>");
                    out.append('\n').write("show  = yes");
                    out.append('\n').write("type  = ");
                    out.write(type);
                    if (type.equals("histogram")){
                        out.append('\n').write("thickness = 3");
                        out.append('\n').write("extend_bin = no");
                        out.append('\n').write("fill_under = yes");
                        //out.append('\n').write("fill_color = vlblue");
                    } else if (type.equals("tile")){
                        out.append('\n').write("thickness = 35");
                        out.append('\n').write("padding = 8");
                        out.append('\n').write("orientation = out");
                        out.append('\n').write("layers = 15");
                        out.append('\n').write("margin = 0.02u");
                        out.append('\n').write("layers_overflow = grow");
                    }
                    out.append('\n').write("file  = ");
                    out.write(datafile.getAbsolutePath());
                    out.append('\n').write("glyph = rectangle");
                    out.append('\n').write("glyph_size = 3");
                    if (!altColors){
                        out.append('\n').write("fill_color = black");
                        out.append('\n').write("stroke_color = black");
                    } else {
                        out.append('\n').write("fill_color = ");
                        out.write(colors[colIndex]);
                        out.append('\n').write("stroke_color = ");
                        out.write(colors[colIndex]);
                        colIndex++;
                    }
                    out.append('\n').write("stroke_thickness = 1");
                    
                    out.append('\n').write("min   = ");
                    out.write(Double.toString(min));
                    out.append('\n').write("max   = ");
                    out.write(Double.toString(max));
                    
                    out.append('\n').write("r0    = ");
                    out.write(Double.toString(plotPositions[posIndex+1]));
                    out.write("r");
                    
                    out.append('\n').write("r1    = ");
                    out.write(Double.toString(plotPositions[posIndex]));
                    out.write("r");
                    if (cf.getBooleanParameter("showAxis")){
                        out.append('\n').write("axis = yes");
                        out.append('\n').write("axis_color = grey");
                        out.append('\n').write("axis_thickness = 2");
                        out.append('\n').write("axis_spacing   = ");
                        out.write(Double.toString((Math.abs(max)+Math.abs(min))/(Math.abs(max)+Math.abs(min))));
                    }
                    out.append('\n').write("</plot>");
                    
                    posIndex += 2;
                }
            } else {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(PlotParams));
                    String line;
                    int counter = 0;
                    while ((line = br.readLine()) != null){
                        out.append('\n').write(line);
                        if (line.equals("<plot>")){
                            out.append('\n').write("file = ");
                            out.write(Data.get(counter).getAbsolutePath());
                            counter++;
                        }
                    }
                    br.close();
                } catch (Exception e){
                    cf.writeError(e);
                }
            }
            if (PlotAnnotation != null){
                out.append('\n').write("<plot>");
                out.append('\n').write("type             = text");
                out.append('\n').write("color            = black");
                out.append('\n').write("file             = ");
                out.write(PlotAnnotation.getAbsolutePath());
                out.append('\n').write("r0 = 1r");
                out.append('\n').write("r1 = 1r+1000p");
                out.append('\n').write("show_links     = yes");
                out.append('\n').write("link_dims      = 0p,0p,5p,0p,1p");
                //out.append('\n').write("link_dims      = 1p,1p,2p,1p,1p");
                out.append('\n').write("link_thickness = 1p");
                out.append('\n').write("link_color     = red");
                out.append('\n').write("label_size     = ");
                out.write(Double.toString(fontSize)+"p");
                out.append('\n').write("label_font   = condensed");
                out.append('\n').write("padding  = 1p");
                out.append('\n').write("rpadding = 1p");
                out.append('\n').write("label_snuggle = yes");
                out.append('\n').write("max_snuggle_distance  = 5r");
                out.append('\n').write("snuggle_tolerance     = 0.25r");
                out.append('\n').write("snuggle_sampling      = 4");
                out.append('\n').write("snuggle_link_overlap_test = yes");
                out.append('\n').write("snuggle_link_overlap_tolerance = 2p");
                out.append('\n').write("snuggle_refine        = yes");
                out.append('\n').write("</plot>");
            }
            out.append('\n').write("</plots>");
            // End of plots block
           
            if (Links != null){
                out.append('\n').write("<links>");
                out.append('\n').write("z      = 0");
                out.append('\n').write("radius = 0.5r");
                out.append('\n').write("bezier_radius = 0.1r");
                out.append('\n').write("<link link>");
                out.append('\n').write("show         = yes");
                out.append('\n').write("color        = lgrey");
                out.append('\n').write("thickness    = 2");
                out.append('\n').write("file         = ");
                out.write(Links.getAbsolutePath());
                out.append('\n').write("record_limit = 2500");
                out.append('\n').write("</link>");
                out.append('\n').write("</links>");
            }
            
            if (Highlights != null){
                out.append('\n').write("<highlights>");
                out.append('\n').write("z = 0");
                out.append('\n').write("fill_color = yellow");
                out.append('\n').write("<highlight>");
                out.append('\n').write("file       = ");
                out.write(Highlights.getAbsolutePath());
                out.append('\n').write("r0         = 0.5r");
                out.append('\n').write("r1         = 1.0r - 100p");
                out.append('\n').write("</highlight>");
                out.append('\n').write("</highlights>");
            }
    
            String chromosomes = cf.getParameter("chr");
            String chrDD = "yes";
            if (!chromosomes.equals("") && chromosomes.charAt(0) != '-') chrDD = "no";
            out.append('\n').write("chromosomes = ");
            out.write(chromosomes.replaceAll(",", ";"));
            out.append('\n').write("chromosomes_units = 1000000");
            out.append('\n').write("chromosomes_display_default = ");
            out.write(chrDD);
            out.append('\n').write("chromosomes_scale = ");
            out.write(cf.getParameter("chrScale"));
            
            out.append('\n').write("anglestep       = 0.5");
            out.append('\n').write("minslicestep    = 10");
            out.append('\n').write("beziersamples   = 40");
            out.append('\n').write("debug           = no");
            out.append('\n').write("warnings        = no");
            out.append('\n').write("imagemap        = no");
            out.append('\n').write("file_delim      = \\t");
            out.append('\n').write("units_ok        = bupr");
            out.append('\n').write("units_nounit    = n");
            out.close();
        } catch (IOException e){
            cf.writeError(e);
        }
    }
   
    private double[] findUpperLower(ArrayList<File> files) throws IOException{
        
        boolean init = true;
        double min = 0;
        double max = 0;
        
        for(File dataFile : files){
            BufferedReader in = new BufferedReader(new FileReader(dataFile));
            
            String  line = in.readLine();
            while(line != null){
                
                String [] values = line.split("\t");
                try{
                    if(init){
                        min = Double.parseDouble(values[3]);
                        max = Double.parseDouble(values[3]);
                        init = false;
                    }else{
                        if(min > Double.parseDouble(values[3]))
                            min = Double.parseDouble(values[3]);
                        if(max < Double.parseDouble(values[3]))
                            max = Double.parseDouble(values[3]);
                    }
                }catch (NumberFormatException e) {
                    // continue
                }
                line = in.readLine();
            }
            in.close();
        }
        
        return new double[]{min, max};
    }
    public static void main(String[] args) {
        new CircosPlot().run(args);
    }
}
