#!/usr/bin/python
import sys
import anduril
from anduril.args import *
import shutil,os
import re
import string
import math

VALID_CHARS = "_.%s%s" % (string.ascii_letters, string.digits)

def mthorder(reader,outFiles,N):
    ''' Copy rows in Mth order '''
    outidx=0
    for row in reader:
        outFiles[outidx].writerow(row)
        outidx=outidx+1
        if outidx+1>N:
            outidx=0

def firstorder(reader,outFiles,N):
    ''' Copy rows in first filled order '''
    rows=len(reader.read())
    rowsperfile=math.ceil(float(rows)/float(N))
    rowidx=0
    for row in reader:
        outidx=int(float(rowidx)/float(rowsperfile))
        outFiles[outidx].writerow(row)
        rowidx=rowidx+1
        
def labelorder(reader,outFiles,uniqlabel,labelCol,matcher,includeLabel):
    ''' Copy rows in label order '''
    for row in reader:
        m=matcher.match(str(row[labelCol]))
        if m:
            outidx=uniqlabel.index(m.group(1))
            if not includeLabel :
                del row[labelCol]
            outFiles[outidx].writerow(row)

def valid_file_name(filename, oldnames=[]):
    ''' Returns a valid filename of a string. Adds a _x to duplicate '''
    newname=''.join(c for c in filename if c in VALID_CHARS)
    unique=True
    x=1
    for old in oldnames:
        if newname == old:
            unique=False
    
    if unique:
        return newname
    
    while not unique:
        for old in oldnames:
            if newname+'_'+str(x) == old:
                x+=1
                break
            unique=True
    
    return newname+'_'+str(x)

#inputs: csv
#output: array
#parameters: labelCol, regexp, N, order  first/mth

labelmatcher=re.compile(regexp)

# read CSV
reader=anduril.TableReader(csv,column_types=str)
array.set_type("csv")

if labelCol is not "":
    if (labelCol not in reader.fieldnames):
        write_error("Column \""+labelCol+"\" not found.")
        sys.exit()
    else:
        write_log("Column \""+labelCol+"\" found.")

if labelCol is "":
    outFiles=[]
    for x in range(N):
        fname=array[str(x+1)]
        outFiles.append(anduril.TableWriter(os.path.join(array.get_folder(),fname), reader.fieldnames))
    
    if order=="first":
        firstorder(reader, outFiles,N)
        sys.exit()
    if order=="sparse":
        mthorder(reader, outFiles,N)
        sys.exit()
    write_error("Splitting order "+order+" not recognized.")
    sys.exit()
else:
    # regex split
    labels=reader.get_column(labelCol)
    matches=[]
    for l in labels:
        m=labelmatcher.match(str(l))
        if m:
            matches.append(m.group(1))
    uniqlabel=sorted(set(matches))
    print('Unique labels: ',uniqlabel)
    outFileNames=[]
    for u in uniqlabel:
        fname=array[valid_file_name(u,outFileNames)]
        outFileNames.append(fname)
    outFiles=[]
    colNames = list(reader.fieldnames)
    
    if not includeLabelCol:
        colNames.remove(labelCol)    
    for x in outFileNames:
        outFiles.append(anduril.TableWriter(os.path.join(array.get_folder(),x), colNames))
    
    labelorder(reader,outFiles,uniqlabel,labelCol,labelmatcher,includeLabelCol)
