library(componentSkeleton)

DOCUMENT.NAME.PDF <- 'document.pdf'

execute <- function(cf) {
    if (input.defined(cf, 'header')) header.name <- file.path(get.input(cf, 'header'), LATEX.DOCUMENT.FILE)
    else header.name <- 'header.tex'

    if (input.defined(cf, 'footer')) footer.name <- file.path(get.input(cf, 'footer'), LATEX.DOCUMENT.FILE)
    else footer.name <- 'footer.tex'
    
    latex.exec  <- get.parameter(cf, 'latexExec')
    bibtex.exec <- get.parameter(cf, 'bibtexExec')
    verbose     <- get.parameter(cf, 'verbose', 'boolean')
    isFragment  <- get.parameter(cf, 'isFragment', 'boolean')
    
    out.dir <- get.output(cf, 'completeDocument')
    
    copy.dir(get.input(cf, 'document'), out.dir, exclude.names=LATEX.DOCUMENT.FILE)

    body.file <- file.path(get.input(cf, 'document'), LATEX.DOCUMENT.FILE)
    doc.body <- textfile.read(body.file)

    if (isFragment) {
       header <- textfile.read(header.name)
       footer <- textfile.read(footer.name)
       copy.dir(get.input(cf, 'header'),   out.dir, exclude.names=LATEX.DOCUMENT.FILE)
       copy.dir(get.input(cf, 'footer'),   out.dir, exclude.names=LATEX.DOCUMENT.FILE)
       document <- paste(header, doc.body, footer, sep='\n')
    } else {
       document <- doc.body
    }
    
    doc.file <- file.path(out.dir, LATEX.DOCUMENT.FILE)
    textfile.write(doc.file, document)

    latex.temp.dir <- tempdir()
    write.log(cf, sprintf('Temporary directory for latex: %s', latex.temp.dir))
    
    copy.dir(out.dir, latex.temp.dir) 
    setwd(latex.temp.dir)
    
    # Latex -> PDF
    
    if (!verbose){
         latex.cmd <- sprintf('%s --interaction=batchmode %s', latex.exec, LATEX.DOCUMENT.FILE)
    } else {
         latex.cmd <- sprintf('%s --interaction=nonstopmode %s', latex.exec, LATEX.DOCUMENT.FILE)
    }
    latex.cmd <- gsub('\\', '/', latex.cmd, fixed=TRUE)
    status <- system(latex.cmd)
    if (status != 0) {
        write.error(cf, sprintf('Latex return status: %s (command was: %s)', status, latex.cmd))
        return(1)
    }
    
    # BibTeX
    useRefs = get.parameter(cf, 'useRefs', type='boolean')
    if (useRefs) {
       if (grep('[.]tex$', LATEX.DOCUMENT.FILE)) {
        bibtex.file <- substr(LATEX.DOCUMENT.FILE, 1, nchar(LATEX.DOCUMENT.FILE)-4)
       } else {
        bibtex.file <- LATEX.DOCUMENT.FILE
       }
       bibtex.cmd <- sprintf('%s %s', bibtex.exec, bibtex.file)
       bibtex.cmd <- gsub('\\', '/', bibtex.cmd, fixed=TRUE)
       status <- system(bibtex.cmd)
       if (status != 0) {
        write.error(cf, sprintf('BibTeX return status: %s (command was: %s)', status, bibtex.cmd))
        return(1)
       }
    }

    status <- system(latex.cmd)
    if (status != 0) {
        write.error(cf, sprintf('Latex return status (run 2): %s (command was: %s)', status, latex.cmd))
        return(1)
    }
    status <- system(latex.cmd)
    if (status != 0) {
        write.error(cf, sprintf('Latex return status (run 3): %s (command was: %s)', status, latex.cmd))
        return(1)
    }
    
    write.log(cf, sprintf('Directory contents after latex: %s', paste(list.files(getwd()), collapse=' ')))
    
    # PDF to output directory
    pdf.file <- file.path(latex.temp.dir, DOCUMENT.NAME.PDF)
    if (!file.exists(pdf.file)) {
        write.error(cf, "PDF file doesn't exist")
        return(1)
    }
    file.copy(pdf.file, get.output(cf, 'document'))
    
    return(0)
}

main(execute)
