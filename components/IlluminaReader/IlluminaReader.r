library(componentSkeleton)

execute <- function(cf) {
  library(lumi)

  # Read input parameters
  file.path	<- get.input(cf, 'illumina')
  decimal	<- get.parameter(cf, 'decimal')
  convert.NuID	<- get.parameter(cf, 'convertNuID', type='boolean')
  transformation <- get.parameter(cf, 'transformation')
  probeAnnotation <- get.parameter(cf, 'probeAnnotation')
  columns	<- c("SampleID", "FileName", "Description")
  samples	<- CSV.read(get.input(cf, 'sampleNames'),
                            expected.columns=columns)

  write.log(cf, "Reading Illumina files...")

  # Read files defined in sample definition file from file.path directory
  files <- samples[!is.na(samples[,"FileName"]),]
  all.files <- list.files(file.path, full.names=TRUE)
  all.files.short <- list.files(file.path)
  defined.files <- all.files[all.files.short %in% files[,"FileName"]]

  if (length(defined.files) == 0) {
	write.error(cf, paste("No input files matching the sample definition file:", paste(files[,"FileName"], collapse=" "), "found in the source file directory", file.path, sep=" "))
	return(PARAMETER_ERROR)
  }


  if (nchar(probeAnnotation) == 0) {
  	lumiBatch <- lumiR.batch(defined.files, dec=decimal, convertNuID=convert.NuID)
	# Create probe annotation table
  	probeA <- pData(featureData(lumiBatch))
  }else {
  	lumiBatch <- lumiR.batch(defined.files, dec=decimal, convertNuID=convert.NuID, annotationColumn=c(split.trim(probeAnnotation,",")))
	# Create probe annotation table
  	probeA <- pData(featureData(lumiBatch))[,split.trim(probeAnnotation,","), drop=F]
  }

  # Transform data to log2 or VST
  lumiBatch.transformed <- lumiT(lumiBatch, method=transformation)
  exprTransformed <- exprs(lumiBatch.transformed)

  # Select samples defined in sample definition file 'samples'
  unknownSamples <- setdiff(c(as.vector(samples$SampleID)), colnames(exprTransformed))
  if (length(unknownSamples) > 0) {
     write.error(cf, sprintf('No measurement data for samples: %s.', paste(collapse=', ', unknownSamples)))
     return(INVALID_INPUT)
  }
  exprTransformed <- exprTransformed[,c(as.vector(samples$SampleID))]

  # Change chip ID to sample name
  colnames(exprTransformed) <- samples[samples$SampleID==(colnames(exprTransformed)),"Description"]

  # Create sample group table
  groupT <- SampleGroupTable.new()
  for (i in 1:nrow(samples)) {
    sample.id <- samples[i,'SampleID']
    desc      <- samples[i,'Description']
    groupT    <- SampleGroupTable.add.group(groupT, sample.id, type=SAMPLE.GROUP.TABLE.SAMPLE, sample.id, desc)
  }

 
  write.log(cf, "Writing result outputs...")
  CSV.write(get.output(cf, 'expr'), exprTransformed)
  CSV.write(get.output(cf, 'probeAnnotation'), probeA)
  SampleGroupTable.write(get.output(cf, 'groups'), groupT)

  return(0)
}

main(execute)


  
