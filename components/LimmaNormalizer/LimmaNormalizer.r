library(componentSkeleton)
library(limma)

two.channel <- function(cf) {
    RG <- new("RGList", list(
        G=2**LogMatrix.read(get.input(cf, 'green')),
        R=2**LogMatrix.read(get.input(cf, 'red')),
        Gb=2**LogMatrix.read(get.input(cf, 'greenBG')),
        Rb=2**LogMatrix.read(get.input(cf, 'redBG'))))

    bg.method <- get.parameter(cf, 'backgroundMethod')
    normexp.method <- get.parameter(cf, 'normexpMethod')
    RG <- backgroundCorrect(RG, method=bg.method, normexp.method=normexp.method)
    
    logratio.method <- get.parameter(cf, 'logratioMethod')
    span <- get.parameter(cf, 'loessSpan', 'float')
    iterations <- get.parameter(cf, 'loessIterations', 'int')
    MA <- normalizeWithinArrays(RG, method=logratio.method, span=span, iterations=iterations, bc.method="none")

    array.method <- get.parameter(cf, 'arrayMethod')
    MA <- normalizeBetweenArrays(MA, method=array.method)

    if (input.defined(cf, 'groups')) {
        groups <- SampleGroupTable.read(get.input(cf, 'groups'))
        for (i in 1:ncol(MA$M)) {
            green.sample <- colnames(RG$G)[i]
            red.sample <- colnames(RG$R)[i]
            group.id <- SampleGroupTable.find.group(groups, c(green.sample, red.sample), type='ratio', any.order=TRUE)
            if (is.null(group.id)) {
                write.error(cf, sprintf('No sample group found of type ratio for channels green=%s red=%s', green.sample, red.sample))
                return(INVALID_INPUT)
            }
            src.groups <- SampleGroupTable.get.source.groups(groups, group.id)
            if (identical(src.groups, c(green.sample, red.sample))) {
                # normalizeWithinArrays computes M = R - G, so we invert groups that are G - R
                MA$M[,i] <- -MA$M[,i]
            } else if (identical(src.groups, c(red.sample, green.sample))) {
                # Do nothing
            } else {
                stop(sprintf('Internal error: sample groups green=%s red=%s not found in table', green.sample, red.sample))
            }
            colnames(MA$M)[i] <- group.id
        }
    } else {
        default.direction <- get.parameter(cf, 'redMinusGreen', 'boolean')
        col.pattern <- get.parameter(cf, 'outColumnPattern')
        if (default.direction) {
            colnames(MA$M) <- sprintf(col.pattern, colnames(RG$R), colnames(RG$G))
        } else {
            MA$M <- -MA$M
            colnames(MA$M) <- sprintf(col.pattern, colnames(RG$G), colnames(RG$R))
        }
    }
    
    first.cell <- CSV.read.first.cell(get.input(cf, 'green'))
    CSV.write(get.output(cf, 'logratio'), MA$M, first.cell=first.cell)
    return(0)
}

one.channel <- function(cf) {

    ER <- new("EListRaw", list(
        E=2**LogMatrix.read(get.input(cf, 'green')),
        Eb=2**LogMatrix.read(get.input(cf, 'greenBG'))))
    
    bg.method <- get.parameter(cf, 'backgroundMethod')
    normexp.method <- get.parameter(cf, 'normexpMethod')
    ER <- backgroundCorrect(ER, method=bg.method, normexp.method=normexp.method)

    array.method <- get.parameter(cf, 'arrayMethod')
    E <- normalizeBetweenArrays(ER, method=array.method)

    first.cell <- CSV.read.first.cell(get.input(cf, 'green'))
    CSV.write(get.output(cf, 'logratio'), E$E, first.cell=first.cell)
    return(0)
}

execute <- function(cf) {
    if(get.input(cf,"red")=="" && get.input(cf,"redBG")=="") {
        return(one.channel(cf))
    } else if(get.input(cf,"red")!="" && get.input(cf,"redBG")!="") {
        return(two.channel(cf))
    } else {
        write.error(cf, "One of inputs 'red' and 'redBG' is missing. Both must be defined for two-channel data.")
        return(INVALID_INPUT)
    }
}

main(execute)
