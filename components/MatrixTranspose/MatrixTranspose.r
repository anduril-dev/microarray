library(componentSkeleton)

execute <- function(cf) {
    matr <- Matrix.read(get.input(cf, 'matrix'))
    row.name <- get.parameter(cf, 'rowName')
    CSV.write(get.output(cf, 'matrix'), t(matr), first.cell=row.name)
    return(0)
}

main(execute)
