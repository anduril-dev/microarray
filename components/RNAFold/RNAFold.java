import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.LatexTools;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.sequence.CSV2FASTA;
import fi.helsinki.ltdk.csbl.asser.io.FastaFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

public class RNAFold extends SkeletonComponent {
    
    final static String ENERGY_COL_NAME = "energy";
    final static String STRUCTURE_COL_NAME = "structure";
    final static String UNIQUE_ID_COL_NAME = "structure_id";
    final static String UNIQUE_ID_PREFIX = "struct_";
    
    @Override    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        final String componentName = cf.getMetadata(cf.METADATA_INSTANCE_NAME);      
        // Read in the parameters.
        final String seqColumn = cf.getParameter("seqColumn");
        final String idColumn = cf.getParameter("idColumn");
        final String program = cf.getParameter("program");
        final String e = cf.getParameter("e");
        final boolean noLP = cf.getParameter("noLP").equals("true");

        // Input and output files.
        File sequence = cf.getInput("sequence");
        File structure = cf.getOutput("structure");
                
        // Create a temporary directory for execution.
        File tempWD = cf.getTempDir();
        // Create a temp fasta input file for the folding program.
        File tempInput = new File(tempWD, "sequence_temp.fasta");
        try {
            RNAFold.createInputFastaFile(sequence, seqColumn, idColumn, tempInput);
        }catch (Exception exception) {
            cf.writeError(exception.getMessage());
            return ErrorCode.PARAMETER_ERROR;
        }
        // Create a temp output file for the folding program.
        File tempOutFold = new File(tempWD, "structure_temp.txt");

        // Check that the given program name is valid
        if(!program.equals("RNAsubopt")) {
            throw new IllegalArgumentException("Invalid value for parameter program: "+ program);   
        }

        // Construct the command string to run the secondary structure prediction algorithm.
        String command = program + " -e " + e;
        if(noLP) {
            command += " -noLP";
        }
        
        // Execute the secondary structure prediction program.
        System.out.println("Executing command '" + command + "' in directory: " + tempWD);
        int status = RNAFold.runCommand(command, null, tempWD, tempInput, tempOutFold, structure);
        if(status != 0) { // Something went wrong.
            // Delete unnessecary output files.
            if(IOTools.rmdir(tempWD, true)) {
                System.out.println("Cannot remove temporary execution directory of " + program + ":" + tempWD);
            }
            throw new RuntimeException(program+" failed and returned "+status+".\nExecution command was:\n");
           
        }

        // Move secondary structure output file from working directory to correct output file.
        RNAFold.createOutputCSVFile(tempOutFold, structure, sequence, seqColumn, idColumn);

        // Delete temporary workin directory.
        IOTools.rmdir(tempWD, true);

        return ErrorCode.OK;
    }
    
    public static int runCommand(String command, String[] env, File wd, File input, File out, File error) throws IOException, InterruptedException{
        Process exec = Runtime.getRuntime().exec(command);
        
        Thread stdOutThread = bindStreams(exec.getInputStream(), new FileOutputStream(out),
                                  out+" writer", false);
        Thread stdErrThread = bindStreams(exec.getErrorStream(), new FileOutputStream(error),
                                  error+" writer", false);
        
        Thread stdInThread = bindStreams(new FileInputStream(input), exec.getOutputStream(), input+" feed", true);
        // Wait for the process to finnish
        int status = exec.waitFor();
        stdOutThread.join(60*1000);
        stdErrThread.join(60*1000);   
        return status;
    }
    
    public static void createLatexReport(File outDir, File modelPDF, String caption, String sectionTitle, String sectionType) throws IOException {
        ArrayList<String> tex = new ArrayList<String>(); 
        if(sectionTitle != "") {
            tex.add(0, "\\" + sectionType + "{" + LatexTools.quote(sectionTitle) + "}");
        }   
        String captionQuoted = LatexTools.quote(caption);
        tex.addAll(LatexTools.formatFigure(modelPDF.getAbsolutePath(), "", captionQuoted, "", 18.0));
        LatexTools.writeDocument(outDir, tex);
    }
    
    private static void createInputFastaFile(File inputCSV, String seqColumn, String idColumn, File outputFasta) throws IOException {
        CSVParser input = new CSVParser(inputCSV);
        int seqIndex = input.getColumnIndex(seqColumn);
        int idIndex = input.getColumnIndex(idColumn);

        BufferedWriter output = new BufferedWriter(new FileWriter(outputFasta));
        
        while(input.hasNext()) {
            String[] line = input.next();
            if(line[seqIndex] != null) {
                String value = ">" + line[idIndex] + "\n" + line[seqIndex] + "\n";
                output.write(value);
            }
        }
        output.close();
        return;
    }
 
    private static void createOutputCSVFile(File inputFold, File outputCSV, File seqCSV, String seqColumn, String idColumn) throws IOException {
        BufferedReader input = new BufferedReader(new FileReader(inputFold));
        BufferedWriter output = new BufferedWriter(new FileWriter(outputCSV));
        
        String line = input.readLine();
        String uniqueID,id,sequence,structure,energy;
        int nRes = 0;

        // Write column names
        output.write(UNIQUE_ID_COL_NAME + "\t" + idColumn + "\t" + seqColumn + "\t" + STRUCTURE_COL_NAME + "\t" + ENERGY_COL_NAME + "\n");


        while(line != null) {
            nRes += 1;
            uniqueID = UNIQUE_ID_PREFIX + nRes;
            id = line.split(" ")[1];
            line = input.readLine();
            sequence = line.split(" ")[0];
            line = input.readLine();
            structure = line.split(" ")[0];
            energy = line.split(" ")[1];
            output.write(uniqueID + "\t" + id + "\t" + sequence + "\t" + structure + "\t" + energy + "\n");

            line = input.readLine();
            while(line != null && !line.substring(0,1).equals(">")) {
                nRes += 1;
                uniqueID = UNIQUE_ID_PREFIX + nRes;
                structure = line.split(" ")[0];
                energy = line.split(" ")[1];
                output.write(uniqueID + "\t" + id + "\t" + sequence + "\t" + structure + "\t" + energy + "\n");
                line = input.readLine();
            }           
        }
        
        // Add IDs with no sequence to the results file.
        CSVParser inputSeq = new CSVParser(seqCSV);
        int seqIndex = inputSeq.getColumnIndex(seqColumn);
        int idIndex = inputSeq.getColumnIndex(idColumn);

        while(inputSeq.hasNext()) {
            String[] lineArray = inputSeq.next();
            if(lineArray[seqIndex] == null) {
                nRes += 1;
                uniqueID = UNIQUE_ID_PREFIX + nRes;
                output.write(uniqueID + "\t" + lineArray[idIndex] + "\tNA\tNA\tNA\n");
            }        
        }   
            
        input.close();
        output.close();
    }
    /**
     * Copies data from the given source to the given sink.
     * This task is carried out by the thread returned. The thread is started
     * automatically.
     *
     * The virtual machine is turned off if this execution fails!
     *
     * @param acceptTermination Do not worry if the sink does not accept the whole source data.
     */
    private static Thread bindStreams(final InputStream  in,
                                      final OutputStream out,
                                      final String       name,
                                      final boolean      acceptTermination) {
        Thread outputThread = new Thread() {
            public void run() {
                try {
                  long   use = 0;
                  byte[] buf = new byte[1024];
                  int    got;

                  while ((got=in.read(buf)) >= 0) {
                      if (got > 0) {
                         try { out.write(buf, 0, got); }
                         catch (IOException e) {
                             System.err.println("Warning: target stream was closed before the end of the content. Only "+
                                                use+" bytes were written by "+name+'.');
                             if (acceptTermination)
                                return;
                             throw e;
                         }
                         use += got;
                      } else {
                         System.err.println("Zero bytes obtained.");
                      }
                  }
                  try { out.flush(); }
                  catch (IOException e) {
                      System.err.println("Warning: target stream was closed before the end of the content. Only "+
                                         use+" bytes were written by "+name+'.');
                      if (!acceptTermination)
                         throw e;
                  }
                  out.close();
                } catch (Exception e) {
                    System.err.println("I/O error in "+name+'.');
                    e.printStackTrace();
                    System.exit(ErrorCode.ERROR.getCode());
                }
            }
        };
        outputThread.setName(name);
        outputThread.setDaemon(true);
        outputThread.start();
        return outputThread;
    }
    public static void main(String[] args) {
        new RNAFold().run(args);
    }
}
