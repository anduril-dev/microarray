Calculates a cumulative score for each gene having some peaks assigned to it. The score is based on the distance of the peak. The closer or the more assigned peaks a gene has the better score it will obtain. The concept of the distance based weights and the following exponential function have been adopted from \cite{Ouyang2009}.

Total score of gene $g$ is calculated from its peak ($p$) distances ($d$):
\begin{eqnarray}
score_g^{linear}      &=& \sum_p\Bigg(w_p\times\max{\bigg(0,\frac{s-|d_{g,p}|}{s}\bigg)}\Bigg),\\
score_g^{exponential} &=& \sum_p\bigg(w_p\times{}e^{\frac{-|d_{g,p}|}{s}}\bigg).
\end{eqnarray}
Optional peak assignment specific input weights ($w_p$) are replaced with ones if not specified.
