
public enum DataTableFormat {
	
	GENE_SYMBOL ("Gene Symbol"),
	BAND 		("Band"),
	CANDIDATE_SNPS		("Candidate SNPs"),
////	OMIM		("Omim"),
////	GAD			("GAD"),
	SCORE		("confidence score(SD)"),
	KEYWORDS	("Keywords");
	
	private String value;
	
	private DataTableFormat(String defaultValue){
		value = defaultValue;
	}
	public String toString(){
		return value;
	}
	
	 public static String [] getFields(){
		 String [] fields = new String [5];
		 
		 fields [0] = GENE_SYMBOL.toString();
		 fields [1] = BAND.toString();
		 fields [2] = CANDIDATE_SNPS.toString();
		 fields [3] = SCORE.toString();
		 fields [4] = KEYWORDS.toString();
		 
		 return fields;
	 }   
	

}
