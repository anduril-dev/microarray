import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.httpRequest.HttpRequestUtil;


public class SNPs3DConnection {

	public static final String [] PRECALCULATED_DISEASES = 
		{"BLADDER CANCER",
		"BREAST CANCER",
		"CERVICAL CANCER",
		"COLORECTAL CANCER",
		"ESOPHAGEAL CANCER",
		"GASTRIC CANCER",
		"HEPATOCELLULAR CANCER",
		"LUNG CANCER",
		"MALIGNANT MELANOMA",
		"MULTIPLE ENDOCRINE NEOPLASIA",
		"NEUROFIBROMATOSIS",
		"PANCREATIC CANCER",
		"POLYCYSTIC KIDNEY DISEASE",
		"PROSTATE CANCER",
		"RETINOBLASTOMA",
		"TUBEROUS SCLEROSIS",
		"ALZHEIMER DISEASE",
		"ASTHMA",
		"DIABETES MELLITUS",
		"HYPERTENSION",
		"OBESITY"};

	SNPs3DLine aLine = null;  // new SNPs3DLine();
        double theThreshold = 0D;


        public SNPs3DConnection() {
                aLine = new SNPs3DLine();
                theThreshold = 0D;
        }

        public SNPs3DConnection(Double aThreshold) {
                aLine = new SNPs3DLine();                
                theThreshold = (aThreshold != null? aThreshold : 0D);
        }

	public void getGenes(String diseaseName, String outputFile) throws IOException{
		getGenes(diseaseName, openOutput(outputFile));
	}
	
	public void getGenes(String diseaseName, CSVWriter out) throws IOException{

		String address = "http://www.snps3d.org/modules.php";

		//first do the query via form
		HashMap<String, String> pars = new HashMap<String, String>();
		pars.put("name", "Candidate");
		pars.put("host", "");
		pars.put("key", diseaseName);
		
		try{
			BufferedReader rd = new BufferedReader(
                    new InputStreamReader(HttpRequestUtil.sendPostRequest(address, pars)));
			
            String line;

    		//then find the link for the address
 			String regexp = "<a href=\"(modules.php[?]name=Candidate&disease="+diseaseName.toUpperCase()+"&uid=[0-9/:]*)\">"+diseaseName.toUpperCase()+"</a><ul>";
 		
			Pattern p = Pattern.compile(regexp);
			
			
			while ((line = rd.readLine()) != null) {
				Matcher m = p.matcher(line);

				if(m.matches()){

					address = "http://www.snps3d.org/"+ m.group(1);
					address = address.replace(" ", "+");
				
					parseSNP3DOutput(HttpRequestUtil.sendHttpRequest(address), out, diseaseName);

				}
				
			}
			
		}catch(UnsupportedEncodingException e){}
		
		catch(MalformedURLException e){}
		
	}
	public void getPrecompiledGenes(String diseaseName, String outFile) throws IOException{
		getPrecompiledGenes(diseaseName, openOutput(outFile));
	}
	public void getPrecompiledGenes(String diseaseName, CSVWriter out) throws IOException{
		

		try{
			String address = "http://snps3d.org/modules.php";


			//first do the query via form
			HashMap<String, String> pars = new HashMap<String, String>();
			pars.put("name", "Candidate");
			pars.put("disease", diseaseName.toUpperCase());

			parseSNP3DOutput(HttpRequestUtil.sendGetRequest(address, pars), 
					out, diseaseName);

		}catch(UnsupportedEncodingException e){}
		catch(MalformedURLException e){}
		
	}
	
	private void lineFull(String disease, StringBuffer sb, Pattern[] rgxps, CSVWriter out) {
		String str = sb.toString();
		String res = matchAndGet(rgxps, str);
		if(res != null){
			
			aLine.theDesease = disease;
			
			int i = res.lastIndexOf(",");
			if ((i != -1) && (i + 1 == res.length())) {
				res = res.substring(0, i);
			}				
			aLine.add(res);
			
			if (aLine.isFull()) {
				if (aLine.getScore() >= theThreshold) {
					out.write(aLine.theDesease);
					for (String myStr : aLine.theLine) {
						out.write(myStr);
					}
				}
				aLine.clear();
			} 
				
			
			
			/*
			if(out.getColumn() == 1){
				out.write(disease);
			}

			int i = res.lastIndexOf(",");
			if ((i != -1) && (i + 1 == res.length())) {
				res = res.substring(0, i-1);
			}
			out.write(res);
			*/
		}
		sb.delete(0, sb.length());
		
	}
	
	
	private String matchAndGet(Pattern[] rgxps, String str){
		for(Pattern reg : rgxps){
			Matcher m = reg.matcher(str);
			//System.out.println(str+" matches? "+reg);
			if(m.matches()){
				//System.out.println(str+" matches "+reg);
				return m.group(1);
				
			}
			
		}
		return null;
	}
	
	public CSVWriter openOutput(String outFile) throws IOException{
		
		return openOutput(new File(outFile)); 
		
	}
	public CSVWriter openOutput(File outFile) throws IOException{
		
		String [] fileHeading = new String[DataTableFormat.values().length+1];
		fileHeading[0] = "Feature"; 
		int i = 1;
		for(DataTableFormat df : DataTableFormat.values()){
			fileHeading[i] = df.toString();
			i++;
		}

		return new CSVWriter(fileHeading, outFile);
		
	}
	private void parseSNP3DOutput(InputStream is, CSVWriter out, String disease) throws IOException{
		
		
		
		StringBuffer sb = new StringBuffer();
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		String line = in.readLine();
		Pattern [] rgxps = new Pattern [] {
				Pattern.compile("<td>\\s*<li>\\s*<font size=-4>\\s*<a href=.*>(.*)</a>\\s*</li>\\s*</font>\\s*</td>"),
				Pattern.compile("<td>\\s*<font size=-4>(.*)</font>\\s*</td>"),
				Pattern.compile("<td>\\s*<a href=.*><font color=red>(.*)</font>\\s*</a>\\s*</td>"),
				Pattern.compile("<td>(\\d+)</td>"),
				Pattern.compile("<td>\\s*<table>\\s*<tr>(.*)</tr>\\s*<tr>\\s*<image .*>\\s*</image>\\s*</tr>\\s*</table>\\s*</td>"),
				Pattern.compile("<td>\\s*<font size=-6>\\s*<a href=\\p{Punct}search/\\?q=geneid:.*&hLight=(.*)&limitShow=10\\p{Punct}>.*</a>\\s*</font>\\s*</td>")
		};
		
		boolean append = false;
		while(line != null){
			if(line.contains("<td>") && line.contains("</td>") 
					&& line.indexOf("</td>") > line.indexOf("<td>")){
				
				line = line.replaceAll("\\t", "");
				sb.append(line.substring(line.indexOf("<td>"), line.indexOf("</td>")+5));
			

				append = false;
				lineFull(disease, sb, rgxps, out);
				
			}else if(line.contains("<td>") || line.contains("</td>")){

				if(line.contains("<td>")){
					line = line.replaceAll("\\t", "");
					sb.append(line.substring(line.indexOf("<td>")));
					append = true;
		

				}else if(line.contains("</td>")){
					line = line.replaceAll("\\t", "");
					sb.append(line.substring(0, line.indexOf("</td>")+5));
					append = false;
					append = false;
					lineFull(disease, sb, rgxps, out);		
			
				}
			}else if (append){
				line = line.replaceAll("\\t", "");
				sb.append(line);				

			}

			line = in.readLine();
		}

	}
	
	public static void main(String [] args){
		
		if(args.length < 2){
			System.err.println("Define disease and output file, score threshold optional.");
			return;
		} 

                Double myThreshold = 0D;
                if (args.length >= 3) { 
                    try {
                        myThreshold = Double.valueOf(args[2]);
                    } catch (Exception e) {
                        myThreshold = 0D;
                    }
                }

		SNPs3DConnection sc = new SNPs3DConnection(myThreshold);
			
		try{
			sc.getPrecompiledGenes(args[0], args[1]);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	
	private class SNPs3DLine 
	{
		private int MAX_COLUMNS = 5;
		private Double theScore = 0D;
		public String theDesease = "";
		public List<String> theLine = null;
		
		public SNPs3DLine() {
			theLine = new ArrayList<String>();
		}
		
		public void add(String aCol) {
			theLine.add(aCol);
			
			if (theLine.size() == 4) {
				theScore = 0D;
				try {
					theScore = Double.valueOf(aCol);
				} catch (Exception e) {
					theScore = 0D;
				}
			}
		}

		public double getScore() {
			return theScore;
		}

		public boolean isFull() {
			return (theLine.size() == MAX_COLUMNS);
		}
		
		public boolean isEmpty() {
			return (theLine.size() == 0);
		}
		
		public void clear() {
			theLine = new ArrayList<String>();
			theDesease = "";
			theScore = 0D;
		}
	}
}
