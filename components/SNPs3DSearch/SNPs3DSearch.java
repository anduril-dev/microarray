import java.io.File;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;


public class SNPs3DSearch extends SkeletonComponent{

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		
		File features = cf.getInput("queryFeatures");
		File genes = cf.getOutput("associatedGenes");
                String thresholdStr = cf.getParameter("threshold");
                Double threshold = Double.valueOf(thresholdStr);
		
		SNPs3DConnection sc = new SNPs3DConnection(threshold);
		
		CSVParser cp = new CSVParser(features);
		
		
		CSVWriter out = sc.openOutput(genes);
		
		while(cp.hasNext()){
			String [] feature = cp.next();
			for(String f : feature){
				boolean isPre = false;
				for(String pre : SNPs3DConnection.PRECALCULATED_DISEASES){
					if(pre.equalsIgnoreCase(f)){
						sc.getPrecompiledGenes(f, out);
						isPre = true;
					}
				}
				if(!isPre){
					sc.getGenes(f, out);
				}
			}
		}
		out.close();
		
		return ErrorCode.OK;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new SNPs3DSearch().run(args);
	}

}
