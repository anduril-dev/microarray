import java.io.File;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import junit.framework.TestCase;


public class TestSNP3D extends TestCase {

	public void testSearch() throws Exception{
		SNPs3DConnection sc = new SNPs3DConnection();
		sc.getGenes("alcoholism", "D:\\alcoholism.txt");
	}
	
	public void testPrecalcSearch() throws Exception{
		SNPs3DConnection sc = new SNPs3DConnection();
		
		sc.getPrecompiledGenes("breast cancer", "D:\\alcoholism.txt");
	}
	public void testComponentCode() throws Exception {
		File features = new File("D:\\projects\\microarray\\components\\SNPs3DSearch\\testcases\\case1\\input\\queryFeatures");
		File genes = new File("D:\\projects\\microarray\\components\\SNPs3DSearch\\testcases\\case1\\expected-output\\res.txt");
		
		SNPs3DConnection sc = new SNPs3DConnection();
		
		CSVParser cp = new CSVParser(features);
		
		
		CSVWriter out = sc.openOutput(genes);
		
		while(cp.hasNext()){
			String [] feature = cp.next();
			for(String f : feature){
				boolean isPre = false;
				for(String pre : SNPs3DConnection.PRECALCULATED_DISEASES){
					if(pre.equalsIgnoreCase(f)){
						sc.getPrecompiledGenes(f, out);
						isPre = true;
					}
				}
				if(!isPre){
					sc.getGenes(f, out);
				}
			}
		}
		out.close();
	}
}
