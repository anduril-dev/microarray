package fi.helsinki.ltdk.csbl.weka;

import java.io.File;

import weka.clusterers.Clusterer;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.OptionHandler;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.javatools.weka.WekaDataUtils;

/**
 * A Weka clustering component for the CSBL microarray pipeline.
 * @author Viljami Aittom&aulm;ki and Anna-Maria Lahesmaa
 * @version 1.0
 */
public class WekaClusterer extends SkeletonComponent {

    public static final String PARAMETER_METHOD = "method";
    public static final String PARAMETER_WEKA_PARAMETERS = "wekaParameters";
    public static final String PARAMETER_ID_COLUMN = "idColumn";
    public static final String PARAMETER_COLUMNS_TO_REMOVE = "columnsToRemove";

    public static final String INPUT_EXPRESSION = "data";
    public static final String OUTPUT_CLUSTERS = "clusters";
    public static final String INPUT_TEMPORARY_INPUT = "tmpinput";
    public static final String OUTPUT_TEMPORARY_OUTPUT = "tmpoutput";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        // Set parameters, input and output
        String method = cf.getParameter(PARAMETER_METHOD);
        String[] wekaParameters = cf.getParameter(PARAMETER_WEKA_PARAMETERS).split("\\s+");
        String idColumn = cf.getParameter(PARAMETER_ID_COLUMN);
        String columnsToRemove = cf.getParameter(PARAMETER_COLUMNS_TO_REMOVE);
        File inputFile = cf.getInput(INPUT_EXPRESSION);
        File outputFile = cf.getOutput(OUTPUT_CLUSTERS);
        File tmpInputFile = new File(cf.getTempDir(), INPUT_TEMPORARY_INPUT);
        File tmpOutputFile = new File(cf.getTempDir(), OUTPUT_TEMPORARY_OUTPUT);
        
        // Convert NA's in input to ?'s
        WekaDataUtils.andurilToWeka(inputFile, tmpInputFile);

        Instances data = WekaDataUtils.readCSV(tmpInputFile);
        
        // Remove given attributes and id's from data
        if (idColumn.length() > 0) {
            if (data.attribute(idColumn.trim()) == null) {
                cf.writeError("Incorrect parameter \"idColumn\": column \"" + idColumn
                        + "\" not found in input data.");
                return ErrorCode.PARAMETER_ERROR;
            }
            columnsToRemove = idColumn + "," + columnsToRemove;
        }
        String[] columnsToRemoveArray = columnsToRemove.split(",");
        Instances filteredData = WekaDataUtils.removeColumns(data, columnsToRemoveArray);

        // Initialize clusterer
        method = "weka.clusterers." + method;
        Clusterer clusterer;
        try {
            clusterer = (Clusterer) Class.forName(method).newInstance();
        } catch (ClassNotFoundException e) {
            cf.writeError("No such classifier: \"" + method + "\"");
            return ErrorCode.PARAMETER_ERROR;
        }
        // Set clusterer options. WEKA DOES NOT THROW AN EXCEPTION FOR INCORRECT
        // OPTIONS EVEN THOUGH IT SHOULD!
        if (clusterer instanceof OptionHandler) {
            try {
            ((OptionHandler) clusterer).setOptions(wekaParameters);
            } catch (Exception e) {
                cf.writeError("Incorrect wekaParameters:");
                cf.writeError(e.getMessage());
                return ErrorCode.PARAMETER_ERROR;
            }
        }

        // Build clusterer
        clusterer.buildClusterer(filteredData);

        // Fetch cluster for all instances and write output
        Attribute idAttribute = null;
        String[] cols = { "index", "clusterId", "clusterProbs" };
        if (idColumn.length() > 0) {
            idAttribute = data.attribute(idColumn.trim());
            cols[0] = idColumn;
        }

        CSVWriter out = new CSVWriter(cols, tmpOutputFile);

        for (int i = 0; i < data.numInstances(); i++) {
            int clusterID = clusterer.clusterInstance(filteredData.instance(i)) + 1;
            double[] clusterProbs = clusterer.distributionForInstance(filteredData.instance(i));

            StringBuffer strBuf = new StringBuffer();
            for (double d : clusterProbs) {
                if (strBuf.length() > 0)
                    strBuf.append(",");
                strBuf.append(d);
            }
            String clusterProbStr = strBuf.toString();

            String instanceId = String.valueOf(i+1);
            if (idAttribute != null) {
                if (idAttribute.isNumeric())
                    instanceId = String.valueOf(data.instance(i).value(idAttribute));
                else
                    instanceId = idAttribute.value(
                        (int) data.instance(i).value(idAttribute));
            }

            out.write(instanceId);
            out.write(clusterID);
            out.write(clusterProbStr);
        }
        out.close();

        // Convert ?'s in output back to NA's and clean temp files
        WekaDataUtils.wekaToAnduril(tmpOutputFile, outputFile);
        tmpInputFile.delete();
        tmpOutputFile.delete();

        return ErrorCode.OK;
    }

    /**
     * @param argv Pipeline arguments
     */
    public static void main(String[] argv) {
        new WekaClusterer().run(argv);
    }

}
