library(componentSkeleton)
library(meap)

merge.sig <- function(x,res,sigcol){
	if(length(sigcol)==1){
		tt <- res[x,sigcol]
		return(median(tt))
	}else{
		tt <- res[x,sigcol]
		return(apply(tt,2,median))
	}
}


execute <- function(cf) {
	write.log(cf, "Reading inputs and parameters...")
	dee <- read.table(get.input(cf,'dee'),header=TRUE,sep="\t",stringsAsFactors=FALSE)
	dbconnect <- CSV.read(get.input(cf,'dbConnect'))
	out.path <-get.output(cf,'deg')

	ecol <- get.parameter(cf,'ExonColumn')
	sigcol <- get.parameter(cf,'sigColumn')	
	cutoff <- get.parameter(cf,'cutoff',type="float")
	
	sigcol <- strsplit(sigcol,",")[[1]]

	host <- dbconnect$host
	database <- dbconnect$database
	user <- dbconnect$user
	passwd <- dbconnect$password
	port <- dbconnect$port

	if(is.na(passwd)){
		passwd <- ""
	}

	if(is.na(port)){
		port <- NULL
	}else{
		port <- as.numeric(port)
	}
	
	write.log(cf, "Searching DEGs, please wait...")
	deeToGene <- meap.map.exon.to.gene(dee[,ecol],host=host,dbname=database,user=user,passwd=passwd,port=port)
	glist <- unique(deeToGene[,2])
	tmptmp <- split(deeToGene[,1],deeToGene[,2])
	numOfDEE <- lapply(tmptmp,length)
	numOfDEEvec <- unlist(numOfDEE)
	names(numOfDEEvec) <- names(numOfDEE)

	GeneToExon <- meap.map.gene.to.exon(glist,host=host,dbname=database,user=user,passwd=passwd,port=port,transcript.filter=FALSE)
	tmp <- split(GeneToExon[,1],GeneToExon[,2])
	numOfExon <- lapply(tmp,length)
	numOfExonvec <- unlist(numOfExon)
	names(numOfExonvec) <- names(numOfExon)

	DEGsScore <- data.frame(NumOfDEE=numOfDEEvec[glist],NumOfExon=numOfExonvec[glist],Score=numOfDEEvec[glist]/numOfExonvec[glist])
	
	stm <- "select * from deeToGene join dee on deeToGene.Exon=dee.ExonID"
	res <- sqldf(stm)[,-1]
	tmp <- split(1:nrow(res),res[,1])
	
	sig <- lapply(tmp,merge.sig,res,sigcol)
	sigVal <- do.call(rbind,sig)
	
	tt <- DEGsScore[rownames(sigVal),]
	sigVal <- cbind(sigVal,tt)
	
	ord <- order(sigVal[,"Score"],decreasing = TRUE)
	result <- sigVal[ord,]
	
	flag <- result[,"Score"]>=cutoff
	out <- result[flag,]
	
	write.log(cf, "Output DEGs...")
	write.table(out,out.path,sep="\t",quote=FALSE)
	
}

main(execute)
