library(componentSkeleton)

execute <- function(cf) {
    expr   <- LogMatrix.read(get.input(cf, 'expr'))
    low.N  <- get.parameter(cf, 'lowNumber', 'int')
    high.N <- get.parameter(cf, 'highNumber', 'int')	

    if (low.N < 0) {
       write.error(cf, sprintf('Number of low genes must be positive (is %s)', low.N))
       return(PARAMETER_ERROR)
    }
    if (high.N < 0) {
       write.error(cf, sprintf('Number of high genes must be positive (is %s)', high.N))
       return(PARAMETER_ERROR)
    }
    
    low.N <- min(low.N, nrow(expr))
    high.N <- min(high.N, nrow(expr))

    idlist <- data.frame(ID=character(0), Members=character(0),
        SampleGroup=character(0), Type=character(0), stringsAsFactors=FALSE)
    
    for (col.name in colnames(expr)) {
        column <- expr[,col.name]
        column <- column[!is.na(column)]
        column <- sort(column)
        
        this.low.N <- min(low.N, length(column))
        this.high.N <- min(high.N, length(column))
        
        if (this.low.N > 0) {
            low.ids <- names(column[1:this.low.N])
        } else {
            low.ids <- character(0)
        }

        if (this.high.N > 0) {
            n <- length(column)
            high.ids <- names(column[n:(n-this.high.N+1)])
        } else {
            high.ids <- character(0)
        }
        
        idlist[nrow(idlist)+1,] <- c(
            sprintf(get.parameter(cf, 'highID'), col.name),
            paste(high.ids, collapse=','),
            col.name, 'topMost')

        idlist[nrow(idlist)+1,] <- c(
            sprintf(get.parameter(cf, 'lowID'), col.name),
            paste(low.ids, collapse=','),
            col.name, 'bottomMost')
    }

    CSV.write(get.output(cf, 'deg'), idlist)	
    return(0)
}

main(execute)
