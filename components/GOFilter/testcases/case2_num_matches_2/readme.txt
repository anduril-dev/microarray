GO topology is current as of 2008-03-06.

Query terms (BP, BP, CC, MF, MF, MF)
- GO:0050896 (BP) - child: GO:0042221
- GO:0032501 (BP)
- GO:0016020 (CC) - child: GO:0005886
- GO:0005488 (MF) - child: GO:0005515
- GO:0030528 (MF)
- GO:0060089 (MF)

Extra GO terms that don't match any query term:
- GO:0065007 (BP)
- GO:0005576 (CC)
- GO:0003824 (MF)

G1:  match 1 term (BP), direct match: GO:0050896
G2:  match 1 term (BP), child-parent match: GO:0042221
G3:  match 1 term (CC), direct match: GO:0016020
G4:  match 1 term (CC), child-parent match: GO:0005886
G5:  match 1 term (MF), direct match: GO:0005488
G6:  match 1 term (MF), child-parent match: GO:0005515
G7:  no match (has annotation)
G8:  no match (has no annotation)
G9:  match 2 terms (BP, BP): GO:0032501,GO:0042221
G10: match 2 terms (BP, CC): GO:0032501,GO:0005886
G11: match 2 terms (CC, MF): GO:0005886,GO:0005515
G12: match 3 terms (BP, CC, MF): GO:0050896,GO:0005886,GO:0005488
G13: match 3 terms (MF, MF, MF): GO:0005515,GO:0030528,GO:0060089

numMatches=1: G1,G2,G3,G4,G5,G6,G9,G10,G11,G12,G13
numMatches=2: G9,G10,G11,G12,G13
numMatches=3: G12,G13

Only query matches:

"Gene"	"GO"
"G1"	"GO:0050896"
"G2"	"GO:0042221"
"G3"	"GO:0016020"
"G4"	"GO:0005886"
"G5"	"GO:0005488"
"G6"	"GO:0005515"
"G7"	""
"G8"	""
"G9"	"GO:0032501,GO:0042221"
"G10"	"GO:0032501,GO:0005886"
"G11"	"GO:0005886,GO:0005515"
"G12"	"GO:0050896,GO:0005886,GO:0005488"
"G13"	"GO:0005515,GO:0030528,GO:0060089"

Adding extra GO terms:

"Gene"	"GO"
"G1"	"GO:0050896"
"G2"	"GO:0042221"
"G3"	"GO:0065007,GO:0016020"
"G4"	"GO:0005886,GO:0005576"
"G5"	"GO:0005488,GO:0065007,GO:0005576"
"G6"	"GO:0005515,GO:0003824"
"G7"	"GO:0065007,GO:0005576,GO:0003824"
"G8"	""
"G9"	"GO:0003824,GO:0032501,GO:0042221"
"G10"	"GO:0003824,GO:0032501,GO:0005576,GO:0005886"
"G11"	"GO:0005886,GO:0065007,GO:0005515"
"G12"	"GO:0050896,GO:0005886,GO:0005488"
"G13"	"GO:0005515,GO:0003824,GO:0030528,GO:0060089,GO:0065007"

