library(componentSkeleton)
library(GO.db)
library(AnnotationDbi)

execute <- function(cf) {
    go.anno    <- AnnotationTable.read(get.input(cf, 'bioAnnotation'))
    matchCount <- get.parameter(cf, 'numMatches', 'int')
    go.column  <- get.parameter(cf, 'goColumn')
    set.id.pattern <- get.parameter(cf, 'setID')
    
    if (input.defined(cf, 'idlist')) {
        idlist <- SetList.read(get.input(cf, 'idlist'))
    } else {
        gene.column <- get.parameter(cf, 'geneColumn')
        if (nchar(gene.column) == 0) {
            gene.column <- colnames(go.anno)[1]
        }
        idlist <- SetList.new(gene.column, list(go.anno[,1]))
    }

    query.terms <- split.trim(get.parameter(cf, 'terms'), ',', fixed=TRUE)
    if (length(query.terms) == 0) {
        write.error(cf, 'terms is empty')
        return(PARAMETER_ERROR)
    }

    query.ontologies <- ids.to.ontologies(query.terms, NA)
    if (any(is.na(query.ontologies))) {
        bad <- query.terms[is.na(query.ontologies)]
        write.error(cf, paste('Unknown GO query term(s):', paste(bad, collapse=', ')))
        return(PARAMETER_ERROR)
    }

    output.sets <- SetList.new(sprintf(set.id.pattern, SetList.get.ids(idlist)))

    for (set.id in SetList.get.ids(idlist)) {
        gene.ids   <- SetList.get.members(idlist, set.id)
        output.ids <- character()

        all.annotation <- AnnotationTable.get.vector(go.anno, go.column, gene.ids)

        for (gene.id in gene.ids) {
            anno.terms <- split.trim(all.annotation[gene.id], ',', fixed=TRUE)
            anno.terms <- anno.terms[!is.na(anno.terms)]
            if (length(anno.terms) == 0) next
            hits <- 0
            for (i in 1:length(query.terms)) {
                query.go       <- query.terms[i]
                query.ontology <- query.ontologies[i]
                if (go.match(query.go, query.ontology, anno.terms)) {
                    hits <- hits + 1
                    if (hits >= matchCount) {
                        output.ids <- c(output.ids, gene.id)
                        break
                    }
                }
            }
        }

        if (!input.defined(cf, 'idlist')) {
           output.ids <- AnnotationTable.get.vector(go.anno, gene.column, output.ids)
        }
        output.sets <- SetList.assign(output.sets, sprintf(set.id.pattern, set.id), output.ids)
    }

    out.file <- get.output(cf, 'idlist')
    CSV.write(out.file, output.sets)
    return(0)
}

# Convert GO ID strings to ontologies.
ids.to.ontologies <- function(goids, missing.value='unknown') {
    sapply(mget(goids, GOTERM, ifnotfound=NA),
           function(t) if (!is.character(t) && !is.na(t)) Ontology(t) else missing.value)
}

# Expand every GO term with its ancestors. Return unique set of
# expanded terms that also include original terms.
expand.ancestors <- function(goids, ontology) {
    if (length(goids) == 0) return(character())
    stopifnot(is.character(goids))

    env       <- get(paste('GO', ontology, 'ANCESTOR', sep=''))
    exp.goids <- unlist(mget(goids, env, ifnotfound=NA))
    exp.goids <- exp.goids[!is.na(exp.goids)]
    exp.goids <- unique(c(goids, exp.goids))
    return(exp.goids)
}

go.match <- function(query.go, query.ontology, annotated.list) {
    annotated.ontologies <- ids.to.ontologies(annotated.list)
    good.goids           <- annotated.list[annotated.ontologies == query.ontology]
    expanded.goids       <- expand.ancestors(good.goids, query.ontology)
    matching             <- length(expanded.goids) > 0 && query.go %in% expanded.goids
    return(matching)
}

main(execute)
