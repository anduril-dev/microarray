
source "$ANDURIL_HOME/bash/functions.sh"
export_command
set -e

RSYNC=rsync
if ( "$RSYNC" --version | grep -i version )
    then echo "Rsync found."
else
    msg="$RSYNC not found."
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
fi

command=$( echo "$parameter_command" | sed -e "s,@inFile@,$input_file,g" \
    -e "s,@inFolder@,$input_folder,g" \
    -e "s,@outFile@,$output_file,g" \
    -e "s,@outFolder@,$output_folder,g" )

mkdir "$output_folder"
touch "$output_file"

echo "$RSYNC" $command
"$RSYNC" $command
