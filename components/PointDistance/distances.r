library(componentSkeleton)

execute <- function(cf){
# Get variables and test them:
    output.file <- get.output(cf,'csv')
    label<-get.parameter(cf, 'labelCol')
    pcol<-get.parameter(cf,'pointId')
    dcol<-get.parameter(cf,'dataId')
    if (xor(pcol=="",dcol=="")) {
        write.error(cf,paste('You must give both "pointId" and "dataId"',sep=''))
        return(PARAMETER_ERROR)
    }
    if (pcol!="" & dcol!="") { useid=TRUE } else { useid=FALSE }
    cols<-get.parameter(cf, 'cols')
    method<-get.parameter(cf,'distance')
    if (is.na(match(method,c('euclidean','manhattan','chebyshev','mahalanobis')))) {
        write.error(cf,paste('The distance type ',method,' is not available.',sep=''))
        return(PARAMETER_ERROR)
    }
    ref <- CSV.read(get.input(cf, 'points'))
    if (pcol!="") {
        if (is.na(match(pcol,colnames(ref)))) {
            write.error(cf,paste('row identifier ',pcol,' is not found in the points.',sep=''))
            return(PARAMETER_ERROR)
        }
    }
    columns <- split.trim(cols, ',')
    if (identical(columns, '*')) columns <- colnames(ref)
    
    nth <- get.parameter(cf,'nth','float')
    if ((nth<1) | (as.integer(nth)!=nth) ){
        write.error(cf,paste('Nth must be greater or equal to 1 and an integer.',sep=''))
        return(PARAMETER_ERROR)
    }
    
    data <- CSV.read(get.input(cf, 'data'))
    if (label!="") {
        if (is.na(match(label,colnames(data)))) {
            write.error(cf,paste('Cluster label ',label,' is not found in the data.',sep=''))
            return(PARAMETER_ERROR)
        }
        if (is.na(match(label,colnames(ref)))) {
            write.error(cf,paste('Cluster label ',label,' is not found in the points.',sep=''))
            return(PARAMETER_ERROR)
        }
    }
    if (dcol!="") {
        if (is.na(match(dcol,colnames(data)))) {
            write.error(cf,paste('row identifier ',dcol,' is not found in the data.',sep=''))
            return(PARAMETER_ERROR)
        }
    }
# End of getting variables and testing their fails    
    if (label=="") { # no labels, i.e. use all data.
        refs<-ref[,columns,drop=FALSE]
        datas<-data[,columns,drop=FALSE]
        otherout<-pointdist(refs,datas,nth,method)
        if (useid) {
            outdata <- cbind(ref[,pcol],data[otherout$id,dcol],
                otherout$mindist)
            colnames(outdata)<-cbind(paste('Point',pcol),paste('Data',dcol),'Distance')
        } else {
            outdata <- cbind(1:length(otherout$mindist),
                otherout$mindist,refs,otherout$coord)
            colnames(outdata)<-cbind('Row','Distance',t(paste('Point',(columns))),t(paste('Data',(columns))))
        }
        rownames(outdata)<-c()
    } else {  # Data has labels (cluster e.g.)
        uniqs <- sort(t(unique(ref[,label,drop=FALSE])))
        if (useid) {
            header<-cbind(label,paste('Point',pcol,sep=" "),paste('Data',dcol,sep=" "),'Distance')
        } else {
            header<-cbind('Row',label,'Distance',t(paste('Point',(columns),sep=" ")),t(paste('Data',(columns),sep=" ")))
        }
        outdata<-numeric()
        dim(outdata)<-c(0,length(header))
        
        for (u in 1:length(uniqs) ) {
            refrows<-which(ref[,label]==uniqs[u])
            refs<-ref[refrows,columns,drop=FALSE]
            datarows<-which(data[,label]==uniqs[u])
            datas<-data[datarows,columns,drop=FALSE]
            otherout<-pointdist(refs,datas,nth,method)
            if (useid) {
                outdata <- rbind(outdata,
                    cbind(rep(uniqs[u],length(otherout$mindist)),
                        ref[refrows,pcol],
                        data[datarows[otherout$id],dcol],
                        otherout$mindist))
            } else {
                outdata <- rbind(outdata,
                    cbind(1:length(otherout$mindist),rep(uniqs[u],length(otherout$mindist)),
                        otherout$mindist,refs,otherout$coord))
            }
        }
        
        colnames(outdata)<-header
        rownames(outdata)<-c()
    }
    
    CSV.write(output.file, outdata)
}
# Actual calculation function
pointdist <- function(ref,data,nth,method) {

    if (method=="euclidean") {
        eucmin<-function(point) {
            psum<-matrix(nrow=nrow(data),ncol=ncol(data))
            for (i in 1:length(point)) {
                psum[,i]<-point[i]-data[,i]
            }
            psum<-rowSums(psum^2)
            D<-sort(psum,index.return=TRUE)
            out<-matrix(c(D$x[nth],D$ix[nth]),nrow=1,ncol=2)
            return(out)
        }
        out<-apply(ref,1,function(x) eucmin(x))
        out<-list(mindist=sqrt(out[1,]),coord=data[out[2,],],id=out[2,])
        
        return(out)
    }
    if (method=="manhattan") {
        manmin<-function(point) {
            psum<-matrix(nrow=nrow(data),ncol=ncol(data))
            for (i in 1:length(point)) {
                psum[,i]<-point[i]-data[,i]
            }
            psum<-rowSums(abs(psum))
            D<-sort(psum,index.return=TRUE)
            out<-matrix(c(D$x[nth],D$ix[nth]),nrow=1,ncol=2)
            return(out)
        }
        out<-apply(ref,1,function(x) manmin(x))
        out<-list(mindist=out[1,],coord=data[out[2,],],id=out[2,])
        return(out)
    }
    if (method=="chebyshev") {
        chemin<-function(point) {
            psum<-matrix(nrow=nrow(data),ncol=ncol(data))
            for (i in 1:length(point)) {
                psum[,i]<-point[i]-data[,i]
            }
            psum<-apply(abs(psum),1,max)
            D<-sort(psum,index.return=TRUE)
            out<-matrix(c(D$x[nth],D$ix[nth]),nrow=1,ncol=2)
            return(out)
        }
        out<-apply(ref,1,function(x) chemin(x))
        out<-list(mindist=out[1,],coord=data[out[2,],],id=out[2,])
        return(out)
    }
    if (method=="mahalanobis") {
        out<-list(mindist=rep(0,nrow(ref)),coord=ref,id=rep(0,nrow(ref)))
        center=colMeans(data)
        Sdata=cov(data)
        D<-mahalanobis(ref,center,Sdata)
        center<-as.matrix(center)
        out$mindist<-D
        out$coord<-t(center[,rep(1,nrow(ref))])
        return(out)
    }

    stop(paste('Method',method,'not found'))
}


main(execute)
