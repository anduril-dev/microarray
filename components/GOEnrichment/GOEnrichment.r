library(componentSkeleton)
library(csbl.go)
library(grDevices)
library(igraph)

MIN.P.VALUE <- 1e-100

execute <- function(cf) {
    if (input.defined(cf, 'enrichmentTable')) {
        set.prob.table(filename=get.input(cf, 'enrichmentTable'))
    } else {
        organism <- get.parameter(cf, 'organism', 'int')
        set.prob.table(organism=organism, type='enrichment')
    }

    ent      <- entities.from.text(get.input(cf, 'goAnnotations'), 'id')
    go.sizes <- entities.go.sizes(ent)
    ent      <- ent[go.sizes > 0]

    test.alternative   <- 'greater'
    filter.fdr         <- get.parameter(cf, 'filterFDR',     'boolean')
    p.threshold        <- get.parameter(cf, 'threshold',     'float')
    priori.threshold   <- get.parameter(cf, 'maxPriori',     'float')
    freq.min.threshold <- get.parameter(cf, 'minFrequency',  'int')
    freq.max.threshold <- get.parameter(cf, 'maxFrequency',  'int')
    filter.parents     <- get.parameter(cf, 'filterParents', 'boolean')

    # Function given as argument; uses bound variables from the current scope.
    filter.func <- function(freqs) {
	freqs[freqs$priori <= priori.threshold & freqs$freq >= freq.min.threshold & freqs$freq <= freq.max.threshold,]
    }

    freqs <- frequencies(ent,
                         p.values          =TRUE,
                         combine.ontologies=TRUE,
                         fdr               =TRUE,
                         test.alternative  =test.alternative,
                         filter.func       =filter.func)

    result <- freqs$combined
    if (nrow(result) > 0) {
        # Column order of freqs$combined changes if less than three ontologies
        # are present in results => reorder columns explicitly.
        result <- freqs$combined[,c('goid', 'freq', 'proportion', 'p.value', 'priori', 'q.value', 'ontology')]
    }

    result$desc <- ids.to.descs(result$goid)
    if (filter.fdr) {
        result <- result[!is.na(result$q.value) & result$q.value <= p.threshold,]
        result <- result[order(result$q.value, result$p.value),]
    } else {
        result <- result[!is.na(result$p.value) & result$p.value <= p.threshold,]
        result <- result[order(result$p.value, result$priori),]
    }

    if (filter.parents && nrow(result) > 0) {
        seen.ids       <- c()
        remove.indices <- c()
        for (i in 1:nrow(result)) {
            this.goid <- result$goid[i]
            ontology  <- result$ontology[i]
            if (this.goid %in% seen.ids) remove.indices <- c(remove.indices, i)
            ancestors <- expand.ancestors(this.goid, ontology)
            seen.ids  <- unique(c(seen.ids, ancestors))
        }
        if (length(remove.indices) > 0) {
            result <- result[-remove.indices,]
        }
    }

    result <- go.specific.proteins(result, ent)

    COL.NAMES <- c('GOID', 'Frequency', 'Proportion', 'PValue', 'Priori',
        'PValueCorrected', 'Ontology', 'Description', 'IDs')

    if (nrow(result) == 0) {
        result <- data.frame(stringsAsFactors=FALSE)
        for (n in COL.NAMES) result[[n]] <- character(0)
    }

    write.graph(create.graph(cf, result, 'BP', ent, filter.fdr), get.output(cf, 'graphBP'),
        format='graphml')
    write.graph(create.graph(cf, result, 'CC', ent, filter.fdr), get.output(cf, 'graphCC'),
        format='graphml')
    write.graph(create.graph(cf, result, 'MF', ent, filter.fdr), get.output(cf, 'graphMF'),
        format='graphml')

    colnames(result) <- COL.NAMES
    CSV.write(get.output(cf, 'goTerms'), result)

    return(0)
}

go.specific.proteins <- function(go.frame, entities, go.column.name='goid', target.column.name='IDs') {
    go.frame[,target.column.name] <- rep(NA, nrow(go.frame))
    if (nrow(go.frame) == 0) return(go.frame)

    go.terms <- entities.get.GO(entities, 'all', TRUE)

    if (nrow(go.frame) > 0) {
        for (i in 1:nrow(go.frame)) {
            goid    <- go.frame[i, go.column.name]
            matches <- sapply(1:length(go.terms), function(i) if (goid %in% go.terms[[i]]) entities$key[i] else NA)
            matches <- matches[!is.na(matches)]
            go.frame[i, target.column.name] <- paste(matches, collapse=',')
        }
    }

    return(go.frame)
}

# Column descriptions for go.frame:
# - goid: GO ID
# - freq: number of entities annotated with this term or its descendants
# - proportion: relative frequency
# - p.value: p-value computed using Fisher's Exact Test from proportion
#   and priori
# - priori: relative frequency of the term in the reference entity set
#   (e.g. whole genome)
# - q.value: if fdr is TRUE, these are adjusted p-values (note: NOT q-values)
# - ontology: ontology of GO term (one of BP, CC, MF)
# - desc: textual description of the term
# - proteins: comma-separated list of protein IDs that are annotated with the
#   given GO term
# - total.network.freq: number of proteins in the network that have some
#   annotation in the given GO ontology. Proportion is computed using this
#   value and "freq".
create.graph <- function(cf, go.frame, ontology, entities, use.corrected.pvalue) {
    stopifnot(ontology %in% ONTOLOGIES)

    go.frame <- go.frame[go.frame$ontology == ontology,]

    if (nrow(go.frame) == 0) {
        g <- graph.empty()
        g <- add.vertices(g, 1, label=sprintf('No enriched GO terms in %s', ontology), shape='box')
        return(g)
    }

    # Add the root term to the graph.
    # Root terms for each ontology are hardcoded.
    if      (ontology == 'BP') root <- 'GO:0008150'
    else if (ontology == 'CC') root <- 'GO:0005575'
    else if (ontology == 'MF') root <- 'GO:0003674'

    if (!(root %in% go.frame$goid)) {
        sizes <- sapply(entities[[ontology]], function(l) length(l))
        total.freq <- length(which(sizes>0))
        go.frame[nrow(go.frame)+1, c('goid', 'freq', 'desc', 'p.value', 'q.value')] <-
            c(root, total.freq, GOTERM[[root]]@Term, 1, 1)
    }

    g                    <- graph.empty(n=nrow(go.frame))
    V(g)$goid            <- go.frame$goid
    V(g)$frequency       <- as.numeric(go.frame$freq)
    V(g)$description     <- go.frame$desc
    V(g)$pvalue          <- round(as.numeric(go.frame$p.value), 10)
    V(g)$pvalueCorrected <- round(as.numeric(go.frame$q.value), 10)

    url.pattern <- get.parameter(cf, 'urlPattern')
    if (nchar(url.pattern) > 0) {
        V(g)$URL <- sprintf(url.pattern, V(g)$goid)
    }

    if (use.corrected.pvalue) pvalue <- as.numeric(go.frame$q.value)
    else pvalue <- as.numeric(go.frame$p.value)
    pvalue[pvalue<MIN.P.VALUE] <- MIN.P.VALUE

    label <- V(g)$description
    label <- gsub('(.{10})[ ]', '\\1\\\\n', label)
    if (get.parameter(cf, 'includeGraphAttributes', 'boolean')) {
        label <- sprintf('%s\\nn=%s p=%.2g', label, go.frame$freq, pvalue)
    }
    V(g)$label <- label

    color.start <- get.parameter(cf, 'colorStart')

    color.end <- get.parameter(cf, 'colorEnd')
    if (nchar(color.start) > 0 && nchar(color.end) > 0) {
        color.middle <- get.parameter(cf, 'colorMiddle')
        low.p <- get.parameter(cf, 'colorMinP', 'float')
        if (low.p == 0) low.p <- min(pvalue, na.rm=TRUE)
        transformed.p <- log10(pvalue) / log10(low.p)
        transformed.p[transformed.p > 1] <- 1
        if (nchar(color.middle) > 0) {
            color.frame <- colorRamp(c(color.start, color.middle, color.end))(transformed.p)
        } else {
            color.frame <- colorRamp(c(color.start, color.end))(transformed.p)
        }
        V(g)$fillcolor <- sapply(1:nrow(color.frame),
            function(i) rgb(color.frame[i, 1], color.frame[i, 2], color.frame[i, 3], maxColorValue=255))
    }

    V(g)$shape <- 'box'

    for (vertex in V(g)) {
        goid <- V(g)[vertex]$goid
        ancestors <- intersect(expand.ancestors(goid, ontology), go.frame$goid)
        for (anc.goid in ancestors) {
            if (anc.goid == goid) next
            anc.vertex <- V(g)[goid == anc.goid]
            if (length(anc.vertex) == 0) {
                stop(sprintf('Internal error: Could not find a vertex in the graph: %s', anc.goid))
            }
            sp <- get.shortest.paths(g, anc.vertex, vertex, mode='out')
            if (length(unlist(sp[[1]])) == 0) {
                g <- add.edges(g, c(anc.vertex, vertex))
            }
        }
    }

    max.freq <- max(as.numeric(go.frame$freq), na.rm=TRUE)
    # Prune unnecessary edges
    for (vertex.from in V(g)) {
        for (vertex.to in neighbors(g, vertex.from, 'out')) {
            edge.conn <- edge.connectivity(g, vertex.from, vertex.to)
            if (edge.conn > 1) {
                g <- delete.edges(g, E(g, P=c(vertex.from, vertex.to)))
            } else {
                freq.to <- V(g)[vertex.to]$frequency
                if (length(freq.to) > 0) {
                    max.width <- get.parameter(cf, 'maxEdgeWidth', 'float')
                    pen.width <- (freq.to / max.freq) * max.width
                    if (pen.width < 1) pen.width <- 1
                    e <- E(g, P=c(vertex.from, vertex.to))
                    E(g)[e]$penwidth <- pen.width
                }
            }
        }
    }

    return(g)
}

main(execute)
