#!/bin/bash

[ $UID -ne 0 ] && [ ! -d "$R_LIBS" ] && {
        echo Not run as root. Be sure to setup your R_LIBS folder.
}

echo '
setwd("/tmp/")

x<-"csbl.go"
if (!require(x,character.only = TRUE))
{
    download.file(
      "http://csbi.ltdk.helsinki.fi/csbl.go/csbl.go_1.4.1.tar.gz", 
        "csbl.go.tgz"
        )
    install.packages("csbl.go.tgz", dep=TRUE, repos = NULL, type = "source")
}
' | R --slave --vanilla 

rm -f /tmp/csbl.go.tgz
