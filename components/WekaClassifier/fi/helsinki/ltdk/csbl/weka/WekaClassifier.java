package fi.helsinki.ltdk.csbl.weka;

import java.io.*;
import java.lang.Integer;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import weka.classifiers.CheckClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.LatexTools;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.javatools.weka.WekaDataUtils;

public class WekaClassifier extends SkeletonComponent {

    static public final String INPUT_REAL_DATA 		= "data";
    static public final String INPUT_TEST_DATA 		= "testdata";
    static public final String INPUT_CLASSIFY_DATA 	= "classifydata";
    static public final String INPUT_CLASSIFIER 	= "classifier";

    static public final String PARAM_CLASS_NAME       = "methodClass";
    static public final String PARAM_CLASS_COLUMN     = "classColumn";
    static public final String PARAM_ARGS             = "wekaParameters";
    static public final String PARAM_CROSS_VALIDATION = "crossValidation";
    static public final String PARAM_DATA_TYPE        = "dataType";
    static public final String PARAM_TEST_DATA_TYPE   = "testDataType";
    static public final String PARAM_RANDOM_SEED      = "randomSeed";
    static public final String COLUMNS_TO_REMOVE      = "columnsToRemove";
    static public final String INTERNAL_TESTS	      = "runInternalTests";
    static public final String SECTION_TITLE          = "sectionTitle";

    static public final String OUTPUT_CLASSIFIER       = "classifier";
    static public final String OUTPUT_REPORT           = "report";
    static public final String OUTPUT_CONFUSION_MATRIX = "confusion";
    static public final String OUTPUT_EVALUATION       = "evaluation";
    static public final String OUTPUT_PREDICTION       = "predictedClasses";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        File         realDName        = cf.getInput(INPUT_REAL_DATA);
        File         testDName        = cf.getInput(INPUT_TEST_DATA);
        File         classifyDName    = cf.getInput(INPUT_CLASSIFY_DATA);
        File         classifierObject = cf.getInput(INPUT_CLASSIFIER);
        File         cmFile           = cf.getOutput(OUTPUT_CONFUSION_MATRIX);
        File         evFile           = cf.getOutput(OUTPUT_EVALUATION);
        String       cName            = cf.getParameter(PARAM_CLASS_NAME);
        String       cCol             = cf.getParameter(PARAM_CLASS_COLUMN);
        String       args             = cf.getParameter(PARAM_ARGS);
        String       dataType         = cf.getParameter(PARAM_DATA_TYPE);
        String       testDataType     = cf.getParameter(PARAM_DATA_TYPE);
        String       columnsToRemove  = cf.getParameter(COLUMNS_TO_REMOVE);
        String       sectionTitle     = cf.getParameter(SECTION_TITLE);
        boolean      runInternalTests = Boolean.parseBoolean(cf.getParameter(INTERNAL_TESTS));
        String       instance         = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        String       randomSeedStr    = cf.getParameter(PARAM_RANDOM_SEED);
        Integer      randomSeed       = null;
        List<String> argl             = new LinkedList<String>();
        boolean      hasNA	          = cf.getBooleanParameter("processMissing");
        
        try {
            randomSeed = Integer.parseInt(randomSeedStr);
        } catch (Exception e) {
            // Cannot parse randomSeedStr to an integer value. No random seed is set.
            cf.writeLog("Invalid random seed: "+randomSeedStr);
        }

        String [] rCols = new String[] {};
        if(columnsToRemove.length() > 0)
      	  rCols = columnsToRemove.split(",");

        //we need to modify the data suitable for weka
        File wekaCSV;
        if(hasNA){
        	File tempFolder = cf.getTempDir();
        	wekaCSV = File.createTempFile("weka", ".txt", tempFolder);
        	WekaDataUtils.andurilToWeka(realDName, wekaCSV);
        }else{
        	wekaCSV = realDName;
        }
        Instances data = WekaDataUtils.readData(wekaCSV, cCol, dataType);
        data = WekaDataUtils.removeColumns(data, rCols);
        
        Scanner scan = new Scanner(args);
        while (scan.hasNext()) {
            argl.add(scan.next());
        }
        scan.close();

        Classifier classifier;
        
        if(classifierObject != null){
        	ObjectInputStream ois = new ObjectInputStream(new FileInputStream(classifierObject));
        	classifier = (Classifier)ois.readObject();
        }else{
        	classifier = Classifier.forName(cName, argl.toArray(new String[argl.size()]));
        	classifier.buildClassifier(data);
        }

        saveClassifier(cf.getOutput(OUTPUT_CLASSIFIER),
                       classifier);

        String evaluationReport;
        if (testDName == null) {
           int crossValidation = Integer.parseInt(cf.getParameter(PARAM_CROSS_VALIDATION));          
           if (crossValidation > data.numInstances()) {
              crossValidation = data.numInstances();
              cf.writeLog("Reducing cross-validation count to "+crossValidation+".");
           }
           evaluationReport = runEvaluation(classifier, data, crossValidation, cmFile,evFile, randomSeed);
        } else {
            Instances testData = WekaDataUtils.readData(testDName, cCol, testDataType);
            testData = WekaDataUtils.removeColumns(testData, rCols);

            evaluationReport = runEvaluation(classifier, testData, 0, cmFile, evFile, randomSeed);
        }

        report(cf.getOutput(OUTPUT_REPORT),
               classifier,
               instance,
               evaluationReport,
               sectionTitle,
               runInternalTests);

        // finally run the actual classification
        if (classifyDName != null){
        	Instances classifiedData = WekaDataUtils.readData(classifyDName, cCol, dataType);
        	classifiedData = WekaDataUtils.removeColumns(classifiedData, rCols);

        	String[] headings = new String[classifiedData.numAttributes()+1];
        	headings[0] = "PredictedClass";
        	for(int i = 0; i < classifiedData.numAttributes(); i++){
        		headings[i+1] = classifiedData.attribute(i).name();
        	}
        	CSVWriter out = new CSVWriter(headings, cf.getOutput(OUTPUT_PREDICTION));
        	for(int i = 0; i < classifiedData.numInstances(); i++){
        		Instance pIn = classifiedData.instance(i);
        		double predictedClass = classifier.classifyInstance(pIn);

        		out.write(data.classAttribute().value((int)predictedClass));
        		for(int j = 0; j < pIn.numAttributes(); j++) {
                    String value = pIn.toString(j);
                    if (value.charAt(0)=='\'' && value.charAt(value.length()-1)=='\'') {
                       value = value.substring(1,value.length()-1);
                    }
        			out.write(value);
            	}
        	}
        	out.close();
        } else {
        	//if no data is provided, create empty file
        	CSVWriter out = new CSVWriter(new String[]{}, cf.getOutput(OUTPUT_PREDICTION));
        	out.close();
        }

        return ErrorCode.OK;
    }

    static String runEvaluation(Classifier classifier,
                                Instances  data,
                                int        crossValidations,
                                File       cmFile,
                                File            evFile) {
        return runEvaluation(classifier, data, crossValidations, cmFile, evFile, (Integer)null);
    }
 
    static String runEvaluation(Classifier classifier,
                                Instances  data,
                                int        crossValidations,
                                File       cmFile,
                                File       evFile,
                                Integer    aRandomSeed) {
        StringBuffer buffer = new StringBuffer(1024);

        try {
          Evaluation evaluation = new Evaluation(data);
          if (crossValidations < 1) {
             evaluation.evaluateModel(classifier, data);
          } else {
             if (aRandomSeed != null) {
                 evaluation.crossValidateModel(classifier, data, crossValidations, new Random(aRandomSeed));
             } else {
                 evaluation.crossValidateModel(classifier, data, crossValidations, new Random());
             }
          }
          buffer.append(evaluation.toSummaryString(true));
          buffer.append('\n');
          buffer.append(evaluation.toClassDetailsString());
          buffer.append('\n');
          buffer.append(evaluation.toMatrixString());

          // Print confusion matrix
          double[][] cm     = evaluation.confusionMatrix();
          Attribute  cNames = data.classAttribute();
          String[]   cmCols = new String[cNames.numValues()+1];
          cmCols[0] = "realClass";
          for (int i=0; i<cm.length; i++) {
              cmCols[i+1] = cNames.value(i);
          }
          CSVWriter cmOut = new CSVWriter(cmCols, cmFile);
          for (int i=0; i<cm.length; i++) {
        	  
        	  //get the class real name for the index
              
        	  cmOut.write(cNames.value(i));
              for (int j=0; j<cm[i].length; j++) {
                  cmOut.write(cm[i][j]);
              }
          }

          cmOut.close();
          //print evaluation scores
          CSVWriter evOut = new CSVWriter(new String[]{"kappa", "K&B Relative", "K&B Information"}, evFile);
          evOut.write(evaluation.kappa());
          evOut.write(evaluation.KBRelativeInformation());
          evOut.write(evaluation.KBInformation());
          evOut.close();
        }
        catch (Exception e) {
          buffer.append(e.toString());
        }
        return buffer.toString();
    }

    static String runTest(Classifier classifier) {
        ByteArrayOutputStream buf    = new ByteArrayOutputStream(1024);
        CheckClassifier       tester = new CheckClassifier();

        tester.setClassifier(classifier);
        tester.setDebug(true);
        // This stream redirection hack is used because Weka API does
        // not give access to its output.
        PrintStream originalOut = System.out;
        PrintStream fakeOut     = new PrintStream(buf);
        System.setOut(fakeOut);
        tester.doTests(); // Actual functionality!
        fakeOut.close();
        System.setOut(originalOut);

        String res = buf.toString();
        return res;
    }

    /**
     * Generates a textual representation about the given classifier.
     *
     * @param  target      Report destination file
     * @param  classifier  The classifier of interest
     * @param  instance    Name of the component instance
     * @throws IOException If the report cannot be stored into the file
     */
    static public void report(File       target,
                              Classifier classifier,
                              String     instance,
                              String     evaluationReport,
                              String     sectionTitle,
                              boolean    runInternalTest) throws IOException {
    	
    	
      if (!target.mkdirs())
         throw new IOException("Cannot create report folder: "+
                               target.getCanonicalPath());
      File        docFile = new File(target, "document.tex");
      PrintStream out     = new PrintStream(new FileOutputStream(docFile));

      if (sectionTitle.length() > 0) {
      	out.println(String.format("{\\subsection{"+sectionTitle+"}\\label{sec:%s}",
                                LatexTools.quote(instance), instance));
      } else {
      	out.println(String.format("{\\subsection{Classifier:%s}\\label{sec:%s}",
            LatexTools.quote(instance), instance));
      }

      out.println("{\\tiny\\begin{verbatim}");
      
      out.println(classifier.toString());
      out.print(evaluationReport);
      
      out.println("\\end{verbatim}}");
      
      if(runInternalTest){
    		String testOutput = runTest(classifier);
    		out.println("Automated check for classifier capabilities and behaviour:");
    		out.print("{\\tiny\\begin{verbatim}");
    		out.print(testOutput);
    		out.println("\\end{verbatim}}");
    	}	
 		out.println("}");
      out.close();
    }

    /**
     * Dumps the given classifier to a file.
     *
     * @param target       Storage file
     * @param classifier   Classifier to be serialized
     * @throws IOException If the object cannot be serialized
     */
    static public void saveClassifier(File       target,
                                      Classifier classifier) throws IOException {
       ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(target));

       out.writeObject(classifier);
       out.close();
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new WekaClassifier().run(argv);
    }

}
