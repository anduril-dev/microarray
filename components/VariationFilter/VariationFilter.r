library(componentSkeleton)

execute <- function(cf) {
    matr  <- Matrix.read(get.input(cf, 'matrix'))
    first.cell <- CSV.read.first.cell(get.input(cf, 'matrix'))
    threshold <- get.parameter(cf, 'threshold', 'float')
    fraction  <- get.parameter(cf, 'fraction', 'boolean')
    filter.small <- get.parameter(cf, 'filterSmallVariation', 'boolean')
    
    col.names <- split.trim(get.parameter(cf, 'columns'), ',')
    if ('*' %in% col.names) col.names <- colnames(matr)

    good <- sapply(1:nrow(matr), function(i) length(which(!is.na(matr[i,col.names]))) > 1)
    matr <- matr[good,]
    sds  <- sapply(1:nrow(matr), function(i) sd(matr[i,col.names], na.rm=TRUE))
    
    if (fraction) {
        means <- sapply(1:nrow(matr), function(i) mean(matr[i,col.names], na.rm=TRUE))
        if (filter.small) good  <- (sds >= means*threshold)
        else good  <- (sds <= means*threshold)
    } else {
        if (filter.small) good  <- (sds >= threshold)
        else good  <- (sds <= threshold)
    }
    matr <- matr[good,,drop=FALSE]
 
    CSV.write(get.output(cf, 'matrix'), matr, first.cell=first.cell)
    return(0)
}

main(execute)
