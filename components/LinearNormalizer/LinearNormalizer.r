library(componentSkeleton)

execute <- function(cf) {
    matr <- CSV.read(get.input(cf, 'matrix'))
    target.mean <- get.parameter(cf, 'meanOfRange', 'float')
    target.range <- get.parameter(cf, 'lengthOfRange', 'float')
    method <- get.parameter(cf, 'method', 'string')
    group <- get.parameter(cf, 'group', 'string')
	excludeColumns <- get.parameter(cf, 'excludeColumns', 'string')
	
	origCols <- colnames(matr)
	excludeDat <- data.frame()
	
	if(excludeColumns != ""){
		exclude <- unlist(strsplit(excludeColumns, ','))
		excludeDat <- data.frame(matr[, exclude])
		colnames(excludeDat) <- exclude
		matr <- matr[,origCols != exclude]
	}
	
    if (group != "") {
        if(length(which(names(matr)==group))==0){
            
            write.error(cf, paste('Error in file ', get.input(cf, 'matrix'), ': Column "',group,'" does not exist', sep='' ))
            return(PARAMETER_ERROR)
        }
        groupcol=which(names(matr)==group)
        uniqgroup=unique(matr[,group])
    } else {
        groupcol=0
        uniqgroup='all'
    }
    
    if (ncol(matr) > 0 && nrow(matr) > 0) {
        for (i in 1:ncol(matr)) {
            if ( is.numeric(matr[,i]) && i!=groupcol ) {  # Make sure column is numeric and not the grouping column
                for (g in 1:length(uniqgroup)) {
                    if (groupcol==0) {
                        rows=1:nrow(matr)
                    } else {
                        rows=matr[,groupcol]==uniqgroup[g]
                    }
                    if (method=="median") {
                        this.mean <- median(matr[rows,i], na.rm=TRUE)
                        matr[rows,i] <- matr[rows,i] + (target.mean - this.mean)
                    } else if (method=="mean") { 
                        this.mean <- mean(matr[rows,i], na.rm=TRUE)
                        matr[rows,i] <- matr[rows,i] + (target.mean - this.mean)
                    } else if (method=="range") {
                        matr[rows,i] <- ((matr[rows,i]-max(matr[rows,i], na.rm=TRUE))/(max(matr[rows,i], na.rm=TRUE)-min(matr[,i], na.rm=TRUE)) +0.5+target.mean)*target.range
                    } else if (method=="z") {
                        matr[rows,i] <- (matr[rows,i]-mean(matr[rows,i], na.rm=TRUE))/sd(matr[rows,i], na.rm=TRUE) + target.mean
                    } else if (method=="zMedian") {
                        matr[rows,i] <- (matr[rows,i]-median(matr[rows,i], na.rm=TRUE))/sd(matr[rows,i], na.rm=TRUE) + target.mean
                    } else if (method=="unitVariance") {
                        matr[rows,i] <- matr[rows,i]/sd(matr[rows,i], na.rm=TRUE) + target.mean
                    } else {
                        stop(paste('Invalid method:', method))
                    }
                }
            }
        }
    }
    
    #first.cell <- CSV.read.first.cell(get.input(cf, 'matrix'))
	if(excludeColumns != ""){
		matr <- cbind(excludeDat, matr)[,origCols]
	}
    CSV.write(get.output(cf, 'matrix'), matr, first.cell=first.cell)
    return(0)
}

main(execute)
