biomart                   version
ensembl                   ENSEMBL GENES 75 (SANGER UK)
snp                       ENSEMBL VARIATION 75 (SANGER UK)
functional_genomics       ENSEMBL REGULATION 75 (SANGER UK)
vega                      VEGA 53  (SANGER UK)
fungi_mart_22             ENSEMBL FUNGI 22 (EBI UK)
fungi_variations_22       ENSEMBL FUNGI VARIATION 22 (EBI UK)
metazoa_mart_22           ENSEMBL METAZOA 22 (EBI UK)
metazoa_variations_22     ENSEMBL METAZOA VARIATION 22 (EBI UK)
plants_mart_22            ENSEMBL PLANTS 22 (EBI UK)
plants_variations_22      ENSEMBL PLANTS VARIATION 22 (EBI UK)
protists_mart_22          ENSEMBL PROTISTS 22 (EBI UK)
protists_variations_22    ENSEMBL PROTISTS VARIATION 22 (EBI UK)
msd                       MSD (EBI UK)
htgt                      WTSI MOUSE GENETICS PROJECT (SANGER UK)
REACTOME                  REACTOME (CSHL US)
WS220                     WORMBASE 220 (CSHL US)
biomart                   MGI (JACKSON LABORATORY US)
pride                     PRIDE (EBI UK)
prod-intermart_1          INTERPRO (EBI UK)
unimart                   UNIPROT (EBI UK)
biomartDB                 PARAMECIUM GENOME (CNRS FRANCE)
biblioDB                  PARAMECIUM BIBLIOGRAPHY (CNRS FRANCE)
Eurexpress Biomart        EUREXPRESS (MRC EDINBURGH UK)
phytozome_mart            Phytozome
metazome_mart             Metazome
HapMap_rel27              HAPMAP 27 (NCBI US)
cildb_all_v2              CILDB INPARANOID AND FILTERED BEST HIT (CNRS FRANCE)
cildb_inp_v2              CILDB INPARANOID (CNRS FRANCE)
experiments               INTOGEN EXPERIMENTS
oncomodules               INTOGEN ONCOMODULES
europhenomeannotations    EUROPHENOME
ikmc                      IKMC GENES AND PRODUCTS (IKMC)
EMAGE gene expression     EMAGE GENE EXPRESSION
EMAP anatomy ontology     EMAP ANATOMY ONTOLOGY
EMAGE browse repository   EMAGE BROWSE REPOSITORY
GermOnline                GERMONLINE
Sigenae_Oligo_Annotation_Ensembl_61 SIGENAE OLIGO ANNOTATION (ENSEMBL 61)
Sigenae Oligo Annotation (Ensembl 59) SIGENAE OLIGO ANNOTATION (ENSEMBL 59)
Sigenae Oligo Annotation (Ensembl 56) SIGENAE OLIGO ANNOTATION (ENSEMBL 56)
Breast_mart_69            BCCTB Bioinformatics Portal (UK and Ireland)
K562_Gm12878              Predictive models of gene regulation from processed high-throughput epigenomics data: K562 vs. Gm12878
Hsmm_Hmec                 Predictive models of gene regulation from processed high-throughput epigenomics data: Hsmm vs. Hmec
Pancreas63                PANCREATIC EXPRESSION DATABASE (BARTS CANCER INSTITUTE UK)
Public_OBIOMARTPUB        Multi-species: marker, QTL, SNP, gene, germplasm, phenotype, association, with Gene annotations
Public_VITIS              Grapevine 8x, stuctural annotation with Genetic maps (genetic markers..)
Public_VITIS_12x          Grapevine 12x, stuctural and functional annotation with Genetic maps (genetic markers..)
Prod_WHEAT                Wheat, stuctural annotation with Genetic maps (genetic markers..)
Public_TAIRV10            Arabidopsis Thaliana TAIRV10, genes functional annotation
Public_MAIZE              Zea mays ZmB73, genes functional annotation
Prod_POPLAR               Populus trichocarpa, genes functional annotation
Prod_POPLAR_V2            Populus trichocarpa, genes functional annotation V2.0
Prod_BOTRYTISEDIT         Botrytis cinerea T4, genes functional annotation 
Prod_                     Botrytis cinerea B0510, genes functional annotation 
Prod_SCLEROEDIT           Sclerotinia sclerotiorum, genes functional annotation 
Prod_LMACULANSEDIT        Leptosphaeria maculans, genes functional annotation
vb_mart_24                VectorBase Genes
vb_snp_mart_24            VectorBase Variation
expression                VectorBase Expression
ENSEMBL_MART_PLANT        GRAMENE 40 ENSEMBL GENES (CSHL/CORNELL US)
ENSEMBL_MART_PLANT_SNP    GRAMENE 40 VARIATION (CSHL/CORNELL US)
