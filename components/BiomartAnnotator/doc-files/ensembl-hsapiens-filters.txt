name                           description
chromosome_name                Chromosome name
start                          Gene Start (bp)
end                            Gene End (bp)
band_start                     Band Start
band_end                       Band End
marker_start                   Marker Start
marker_end                     Marker End
type                           Type
encode_region                  Encode region
strand                         Strand
chromosomal_region             Chromosome Regions (e.g 1:100:10000:-1,1:100000:200000:1)
with_ox_arrayexpress           with ArrayExpress ID(s)
with_illumina_humanwg_6_v1     with Illumina HumanWG 6 v1 probe ID(s)
with_illumina_humanwg_6_v2     with Illumina HumanWG 6 v2 probe ID(s)
with_illumina_humanwg_6_v3     with Illumina HumanWG 6 v3 probe ID(s)
with_illumina_humanht_12_v3    with Illumina Human HT 12 v3 probe ID(s)
with_illumina_humanht_12_v4    with Illumina Human HT 12 v4 probe ID(s)
with_illumina_humanref_8_v3    with Illumina Human HT 8 v3 probe ID(s)
with_phalanx_onearray          with Phalanx onearray probe ID(s)
with_codelink_codelink         with Codelink probe ID(s)
with_agilent_cgh_44b           with Agilent CGH 44b probe ID(s)
with_wikigene                  with WikiGene ID(s)
with_affy_primeview            with Affymetrix Microarray primeview ID(s)
with_efg_agilent_sureprint_g3_ge_8x60k with Efg agilent sureprint g3 ge 8x60k ID(s)
with_efg_agilent_sureprint_g3_ge_8x60k_v2 with Efg agilent sureprint g3 ge 8x60k v2 ID(s)
with_efg_agilent_wholegenome_4x44k_v1 with Efg agilent wholegenome 4x44k v1 ID(s)
with_efg_agilent_wholegenome_4x44k_v2 with Efg agilent wholegenome 4x44k v2 ID(s)
with_hgnc                      with HGNC ID(s)
with_ox_clone_based_vega_gene  with clone based VEGA gene ID(s)
with_ox_clone_based_ensembl_gene with clone based Ensembl gene ID(s)
with_ox_clone_based_ensembl_transcript with clone based Ensembl transcript ID(s)
with_ox_clone_based_vega_transcript with clone based VEGA transcript ID(s)
with_ottg                      with VEGA gene ID(s) (OTTG)
with_ottt                      with VEGA transcript ID(s) (OTTT)
with_shares_cds_and_utr_with_ottt with HAVANA transcript (where ENST identical to OTTT)
with_shares_cds_with_ottt      with HAVANA transcript (where ENST shares CDS with OTTT)
with_mirbase                   with miRBase ID(s)
with_rfamR                     with Rfam ID(s)
with_ccds                      with CCDS ID(s)
with_chembl                    with ChEMBL ID(s)
with_embl                      with EMBL ID(s)
with_entrezgene                with EntrezGene ID(s)
with_go_id                     with GO Term Accession(s)
with_merops                    with MEROPS ID(s)
with_mim_gene                  with MIM gene ID(s)
with_mim_morbid                with MIM disease ID(s)
with_ucsc                      with UCSC ID(s)
with_pdb                       with PDB ID(s)
with_refseq_peptide            with RefSeq protein ID(s)
with_refseq_peptide_predicted  with RefSeq predicted protein ID(s)
with_protein_id                with protein(Genbank) ID(s)
with_uniprotsptrembl           with UniProtKB/TrEMBL Accession(s)
with_unigene                   with UniGene ID(s)
with_uniprot_genename          with UniProt Genename ID(s)
with_uniprot_genename_transcript_name with Uniprot Genename Transcript Name(s)
with_exp_est_anatomicalsystem  with Expression est anatomical system
with_exp_est_associatedwith    with Expression est associated with
with_exp_est_celltype          with Expression est cell type
with_exp_est_developmentstage  with Expression est development stage
with_exp_est_experimentaltechnique with Expression est experimental technique
with_exp_est_microarrayplatform with Expression est microarray platform
with_exp_est_pathology         with Expression est pathology
with_exp_est_pooling           with Expression est pooling
with_exp_est_tissuepreparation with Expression est tissue preparation
with_exp_est_treatment         with Expression est treatment
with_exp_atlas_celltype        with GNF/Atlas cell type ID(s)
with_exp_atlas_organismpart    with GNF/Atlas organism part ID(s)
with_exp_atlas_diseasestate    with GNF/Atlas disease state ID(s)
with_dbass3                    with DBASS3 ID(s)
with_hpa                       with Human Protein Atlas ID(s)
with_affy_hc_g110              with Affymetrix Microarray hc g110 probeset ID(s)
with_affy_hg_focus             with Affymetrix Microarray hg Focus probeset ID(s)
with_affy_hg_u133a             with Affymetrix Microarray hg u133a probeset ID(s)
with_affy_hg_u133b             with Affymetrix Microarray hg u133b probeset ID(s)
with_affy_hg_u133a_2           with Affymetrix Microarray hg u133a 2 probeset ID(s)
with_affy_hg_u133_plus_2       with Affymetrix Microarray hg u133 plus 2 probeset ID(s)
with_affy_hg_u95a              with Affymetrix Microarray hg u95a probeset ID(s)
with_affy_hg_u95av2            with Affymetrix Microarray hg u95av2 ID(s) probeset
with_affy_hg_u95b              with Affymetrix Microarray hg u95b probeset ID(s)
with_affy_hg_u95c              with Affymetrix Microarray hg u95c probeset ID(s)
with_affy_hg_u95d              with Affymetrix Microarray hg u95d probeset ID(s)
with_affy_hg_u95e              with Affymetrix Microarray hg u95e probeset ID(s)
with_affy_u133_x3p             with Affymetrix Microarray u133 x3p probeset ID(s)
with_affy_hugenefl             with Affymetrix Microarray HuGeneFL probeset ID(s)
with_affy_hugene_1_0_st_v1     with Affymetrix Microarray hugene 1 0 st v1 probeset ID(s)
with_affy_hugene_2_0_st_v1     with Affymetrix Microarray hugene 2 0 st v1 ID(s)
with_affy_huex_1_0_st_v2       with Affymetrix Microarray huex 1 0 st v2 probeset ID(s)
with_ox_goslim_goa             with GOSlim GOA(s)
with_ox_ens_hs_gene            with Ensembl to LRG link gene IDs
with_ox_ens_hs_translation     with Ensembl to LRG link translation IDs
with_ox_ens_hs_transcript      with Ensembl to LRG link transcript IDs
with_ox_uniparc                with UniParc ID(s)
with_ox_ens_lrg_gene           with Ensembl LRG gene ID(s)
with_ox_ens_lrg_transcript     with Ensembl LRG transcript ID(s)
with_ox_hgnc_transcript_name   with HGNC transcript name(s)
with_ox_rfam_transcript_name   with Rfam transcript name(s)
with_ox_mirbase_transcript_name with miRBase transcript name(s)
with_ox_uniprotsptrembl        with UniProtSPTREMBL ID(s)
with_ox_uniprotswissprot       with UniProtSWISSPROT ID(s)
with_go_go                     with GO ID(s)
with_ox_refseq_mrna            with RefSeq mRNA ID(s)
with_ox_refseq_mrna_predicted  with RefSeq mRNA predicted ID(s)
with_ox_refseq_ncrna           with RefSeq ncRNA ID(s)
with_ox_refseq_ncrna_predicted with RefSeq ncRNA predicted ID(s)
ensembl_gene_id                Ensembl Gene ID(s) [e.g. ENSG00000139618]
ensembl_transcript_id          Ensembl Transcript ID(s) [e.g. ENST00000380152]
ensembl_peptide_id             Ensembl protein ID(s) [e.g. ENSP00000369497]
ensembl_exon_id                Ensembl exon ID(s) [e.g. ENSE00001508081]
hgnc_id                        HGNC ID(s) [e.g. 43668]
hgnc_symbol                    HGNC symbol(s) [e.g. ZFY]
hgnc_transcript_name           HGNC transcript name(s) [e.g. QRSL1P2-001]
arrayexpress                   ArrayExpress ID(s) [e.g. ENSG00000241328]
ccds                           CCDS ID(s) [e.g. CCDS10187]
chembl                         ChEMBL ID(s) ID(s) [e.g. CHEMBL1075092]
clone_based_ensembl_gene_name  Clone based Ensembl gene name(s) [e.g. AL691479.1]
clone_based_ensembl_transcript_name Clone based Ensembl transcript name(s) [e.g. AL691479.1-201]
clone_based_vega_gene_name     Clone based VEGA gene name(s) [e.g. RP11-815M8.1]
clone_based_vega_transcript_name Clone based VEGA transcript name(s) [e.g. RP11-815M8.1-001]
codelink                       Codelink probe ID(s) [e.g. GE550734]
dbass3_name                    DBASS3 Gene Name [e.g. PDE6B]
embl                           EMBL ID(s) [e.g. D87018]
ens_hs_gene                    Ensembl to LRG link gene IDs [e.g. ENSG00000108821]
ens_hs_transcript              Ensembl to LRG link transcript IDs [e.g. ENST00000225964]
ens_hs_translation             Ensembl to LRG link translation IDs [e.g. ENSP00000376544]
ens_lrg_gene                   LRG to Ensembl link gene IDs [e.g. LRG_3]
ens_lrg_transcript             LRG to Ensembl link transcript IDs [e.g. LRG_226t1]
entrezgene                     EntrezGene ID(s) [e.g. 115286]
go_id                          GO Term Accession(s) [e.g. GO:0005515]
goslim_goa_accession           GOSlim GOA Accessions(s) [e.g. GO:0005623]
hpa                            Human Protein Atlas Antibody ID [e.g. HPA002549]
shares_cds_with_ottt           HAVANA transcript (where ENST shares CDS with OTTT) [e.g. OTTHUMT00000088063]
shares_cds_and_utr_with_ottt   HAVANA transcript (where ENST identical to OTTT) [e.g. OTTHUMT00000088055]
merops                         MEROPS ID(s) [e.g. C19.028]
mim_gene_accession             MIM Gene Accession(s) [e.g. 611882]
mim_morbid_accession           MIM Morbid Accession(s) [e.g. 540000]
mirbase_id                     miRBase ID(s) [e.g. hsa-mir-137]
mirbase_accession              miRBase Accession(s) [e.g. MI0000454]
mirbase_transcript_name        miRBase transcript name [e.g. hsa-mir-877.2-201]
pdb                            PDB ID(s) [e.g. 1J47]
protein_id                     Protein (Genbank) ID(s) [e.g. BAA20017]
refseq_mrna                    Refseq mRNA ID(s) [e.g. NM_001195597]
refseq_mrna_predicted          Refseq Predicted mRNA ID(s) [e.g. XM_001125684]
refseq_ncrna                   Refseq ncRNA ID(s) [e.g. NR_002834]
refseq_ncrna_predicted         Refseq Predicted ncRNA ID(s) [e.g. XR_111404]
refseq_peptide                 Refseq protein ID(s) [e.g. NP_001005353]
refseq_peptide_predicted       Refseq predicted protein ID(s) [e.g. XP_005263247]
rfam                           Rfam ID(s) [e.g. RF00432]
rfam_transcript_name           Rfam transcript name(s) [e.g. U3.30-201]
ucsc                           UCSC ID(s) [e.g. uc010ajn.1]
uniprot_sptrembl               UniProt/TrEMBL Accession(s) [e.g. A2MYD1]
uniprot_swissprot              UniProt/Swissprot ID(s) [e.g. GAGE4_HUMAN]
uniprot_swissprot_accession    UniProt/Swissprot Accession(s) [e.g. Q13068]
unigene                        UniGene ID(s) [e.g. Hs.602394]
uniprot_genename               UniProt Genename ID(s) [e.g. V4-4]
uniparc                        UniParc ID(s) [e.g. UPI0000000AA1]
uniprot_genename_transcript_name Uniprot Genename Transcript Name ID(s) [e.g. SEPT1-202]
ottg                           VEGA Gene ID(s) (OTTG) [e.g. OTTHUMG00000036159]
ottt                           VEGA Transcript ID(s) (OTTT) [e.g. OTTHUMT00000088063]
wikigene_id                    WikiGene ID(s) [e.g. 115286]
wikigene_name                  WikiGene Name(s) [e.g. SLC25A26]
affy_hc_g110                   Affy hc g110 probeset ID(s) [e.g. 113_i_at]
affy_hg_focus                  Affy hg focus probeset ID(s) [e.g. 201612_at]
affy_hg_u95a                   Affy hg u95a probeset ID(s) [e.g. 32647_at]
affy_hg_u95av2                 Affy hg u95av2 probeset ID(s) [e.g. 32647_at]
affy_hg_u95b                   Affy hg u95b probeset ID(s) [e.g. 53925_at]
affy_hg_u95c                   Affy hg u95c probeset ID(s) [e.g. 61056_r_at]
affy_hg_u95d                   Affy hg u95d probeset ID(s) [e.g. 79632_at]
affy_hg_u95e                   Affy hg u95e probeset ID(s) [e.g. 79965_at]
affy_hg_u133a_2                Affy hg u133a 2 probeset ID(s) [e.g. 200874_s_at]
affy_hg_u133a                  Affy hg u133a probeset ID(s) [e.g. 200874_s_at]
affy_hg_u133b                  Affy hg u133b probeset ID(s) [e.g. 227057_at]
affy_hg_u133_plus_2            Affy hg u133 plus 2 probeset ID(s) [e.g. 241843_at]
affy_hugenefl                  Affy HuGene FL probeset ID(s) [e.g. M58525_s_at]
affy_hugene_1_0_st_v1          Affy HuGene 1_0 st v1 probeset ID(s) [e.g. 8016215]
affy_hugene_2_0_st_v1          Affy HuGene 2_0 st v1 probeset ID(s) [e.g. 16942487]
affy_huex_1_0_st_v2            Affy HuEx 1_0 st v2 probeset ID(s) [e.g. 4033465]
affy_primeview                 Affymetrix Microarray Primeview ID(s) [e.g. 11763890_at]
affy_u133_x3p                  Affy u133 x3p probeset ID(s) [e.g. Hs2.205326.1.A1_3p_at]
agilent_cgh_44b                Agilent CGH 44b probe ID(s) [e.g. A_14_P131077]
efg_agilent_sureprint_g3_ge_8x60k Agilent Sureprint G3 GE 8x60k probe ID(s) [e.g. A_33_P3356022]
efg_agilent_sureprint_g3_ge_8x60k_v2 Agilent Sureprint G3 GE 8x60k v2 probe ID(s) [e.g. A_24_P368544]
efg_agilent_wholegenome_4x44k_v1 Agilent WholeGenome 4x44k v1 probe ID(s) [e.g. A_32_P196615]
efg_agilent_wholegenome_4x44k_v2 Agilent WholeGenome 4x44k v2 probe ID(s) [e.g. A_33_P3356022]
illumina_humanwg_6_v1          Illumina HumanWG 6 V1 probe ID(s) [e.g. 0000940471]
illumina_humanwg_6_v2          Illumina HumanWG 6 V2 probe ID(s) [e.g. ILMN_1748182]
illumina_humanwg_6_v3          Illumina HumanWG 6 v3 probe ID(s) [e.g. ILMN_2103362]
illumina_humanref_8_v3         Illumina Human Ref 8 v3 probe ID(s) [e.g. ILMN_1768251]
illumina_humanht_12_v3         Illumina Human HT 12 v3 probe ID(s) [e.g. ILMN_1672925]
illumina_humanht_12_v4         Illumina Human HT 12 v4 probe ID(s) [e.g. ILMN_1768251]
phalanx_onearray               Phalanx OneArray probe ID(s) [e.g. PH_hs_0031946]
transcript_count               Transcript count >=
biotype                        Type
source                         Source (gene)
transcript_source              Source (transcript)
status                         Status (gene)
transcript_status              Status (transcript)
phenotype_description          Phenotype description
event_type                     Event Type
go_evidence_code               GO Evidence code
go_parent_term                 Parent term accession
go_parent_name                 Parent term name
anatomical_system_term         Anatomical system
development_stage_term         Development Stage
cell_type_term                 Cell type
pathology_term                 Pathology
atlas_celltype                 
atlas_diseasestate             
atlas_organismpart             
with_paralog_hsap              Paralogous Human Genes
with_homolog_vpac              Orthologous Alpaca Genes
with_homolog_acar              Orthologous Anole Lizard Genes
with_homolog_dnov              Orthologous Armadillo Genes
with_homolog_gmor              Orthologous Atlantic Cod Genes
with_homolog_ogar              Orthologous Bushbaby Genes
with_homolog_cele              Orthologous Caenorhabditis elegans Genes
with_homolog_fcat              Orthologous Cat Genes
with_homolog_amex              Orthologous Cave fish Genes
with_homolog_ggal              Orthologous Chicken Genes
with_homolog_ptro              Orthologous Chimpanzee Genes
with_homolog_psin              Orthologous Chinese softshell turtle Genes
with_homolog_cint              Orthologous Ciona intestinalis genes
with_homolog_csav              Orthologous Ciona savignyi Genes
with_homolog_lcha              Orthologous Coelacanth Genes
with_homolog_sara              Orthologous Common Shrew Genes
with_homolog_btau              Orthologous Cow Genes
with_homolog_cfam              Orthologous Dog Genes
with_homolog_ttru              Orthologous Dolphin Genes
with_homolog_apla              Orthologous Duck Genes
with_homolog_dmel              Orthologous Drosophila Genes
with_homolog_lafr              Orthologous Elephant Genes
with_homolog_mfur              Orthologous Ferret Genes
with_homolog_falb              Orthologous Flycatcher Genes
with_homolog_trub              Orthologous Fugu Genes
with_homolog_nleu              Orthologous Gibbon Genes
with_homolog_ggor              Orthologous Gorilla Genes
with_homolog_cpor              Orthologous Guinea Pig Genes
with_homolog_eeur              Orthologous Hedgehog Genes
with_homolog_ecab              Orthologous Horse Genes
with_homolog_dord              Orthologous Kangaroo Rat Genes
with_homolog_pmar              Orthologous Lamprey Genes
with_homolog_etel              Orthologous Lesser hedgehog tenrec Genes
with_homolog_mmul              Orthologous Macaque Genes
with_homolog_cjac              Orthologous Marmoset Genes
with_homolog_olat              Orthologous Medaka Genes
with_homolog_pvam              Orthologous Megabat Genes
with_homolog_mluc              Orthologous Microbat Genes
with_homolog_mmus              Orthologous Mouse Genes
with_homolog_mmur              Orthologous Mouse Lemur Genes
with_homolog_onil              Orthologous Nile tilapia Genes
with_homolog_mdom              Orthologous Opossum Genes
with_homolog_pabe              Orthologous Orangutan Genes
with_homolog_amel              Orthologous Panda Genes
with_homolog_sscr              Orthologous Pig Genes
with_homolog_opri              Orthologous Pika Genes
with_homolog_xmac              Orthologous Platyfish Genes
with_homolog_oana              Orthologous Platypus Genes
with_homolog_ocun              Orthologous Rabbit Genes
with_homolog_rnor              Orthologous Rat Genes
with_homolog_pcap              Orthologous Rock Hyrax Genes
with_homolog_oari              Orthologous Sheep Genes
with_homolog_chof              Orthologous Sloth Genes
with_homolog_locu              Orthologous Spotted gar Genes
with_homolog_itri              Orthologous Squirrel Genes
with_homolog_gacu              Orthologous Stickleback Genes
with_homolog_tsyr              Orthologous Tarsier Genes
with_homolog_shar              Orthologous Tasmanian Devil Genes
with_homolog_tnig              Orthologous Tetraodon Genes
with_homolog_tbel              Orthologous Tree Shrew Genes
with_homolog_mgal              Orthologous Turkey Genes
with_homolog_meug              Orthologous Wallaby Genes
with_homolog_xtro              Orthologous Xenopus Genes
with_homolog_scer              Orthologous Yeast Genes
with_homolog_tgut              Orthologous Zebra Finch Genes
with_homolog_drer              Orthologous Zebrafish Genes
with_profile                   with Protein feature pfscan ID(s)
with_tmhmm                     with Protein feature tmhmm ID(s)
with_tigrfam                   with Protein feature tigrfam ID(s)
with_superfamily               with Protein feature superfamily ID(s)
with_smart                     with Protein feature smart ID(s)
with_signalp                   with Protein feature signalp ID(s)
with_low_complexity            with Protein feature seg ID(s)
with_pirsf                     with Protein feature pirsf ID(s)
with_coil                      with Protein feature ncoils ID(s)
with_interpro                  with InterPro ID(s)
with_protein_feature_pfam      with PFAM ID(s)
with_protein_feature_prints    with PRINTS ID(s)
tigrfam                        TIGRfam ID(s) [e.g. TIGR00172]
superfamily                    Superfamily ID(s) [e.g. SSF47095]
smart                          SMART ID(s) [e.g. SM00398]
pirsf                          PIRSF ID(s) [e.g. PIRSF037653]
family                         Ensembl Protein Family ID(s) [e.g. ENSFM00250000000002]
pfam                           PFAM ID(s) [e.g. PF00046]
prints                         PRINTS ID(s) [e.g. PR00194]
profile                        PROFILE ID(s) [e.g. PS50313]
interpro                       Interpro ID(s) [e.g. IPR007087]
with_transmembrane_domain      Transmembrane domains
with_signal_domain             Signal domains
germ_line_variation_source     limit to genes with germline variation data sources
somatic_variation_source       limit to genes with somatic variation data sources
with_validated_snp             Associated with validated SNPs
so_parent_name                 Parent term name
