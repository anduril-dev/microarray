# Biomart.r
# Anduril component for getting annotations with biomaRt.
# Author Erkka Valo, Viljami Aittomaki, Kristian Ovaska

library(componentSkeleton)
library(biomaRt)

# Every N_QUERIES the progress is written to log file in non-patch mode.
N_QUERIES = 100

# Function for writing empty output
writeEmptyOut <- function(annotations.path, col.names) {    
    emptyMatrix           <- matrix(nrow=0, ncol=length(col.names))
    colnames(emptyMatrix) <- col.names
    CSV.write(annotations.path, emptyMatrix)
}

# Function for writing databases Properties file
writeDatabases <- function(databases.path, martname, marthost, martpath, dataset)
{
    martList       <- listMarts(host=marthost, path=martpath)
    version        <- as.character(martList[martList['biomart']==martname, 'version'])
    datasetList    <- listDatasets(useMart(martname, host=marthost, path=martpath))
    datasetVersion <- datasetList[datasetList['dataset']==dataset,'description']
    keys           <- c('version', 'dataset')
    values         <- c(version, datasetVersion)
    cat(paste(keys, values, sep='=', collapse='\n'), file=databases.path)
}

# Function for combining duplicate ID's in annotations to a unique list
collapseAttributes <- function(annotations, attributes, uniqResults, collapse) {
    collapsed.annotations <- character()
    for(attribute in attributes) {
        # If no annotation was found for given filter value.
        if (is.null(annotations) || nrow(annotations) == 0) {
            attribute.value <- "NA"
        } else {
            if (uniqResults) {
                attribute.value     <- setdiff(unique(annotations[,attribute]),'')
                if (collapse) {
                    attribute.value <- paste(attribute.value, collapse=',')
                }else{
                    attribute.value <- paste(attribute.value)
                }
            } else {
                if(collapse){
                    attribute.value <- paste(annotations[,attribute], collapse=',')
                }else{
                    attribute.value <- paste(annotations[,attribute])
                }
            }
        }
        collapsed.annotations <- c(collapsed.annotations, attribute.value)
    }
    collapsed.annotations
}

collapseAttributesBatch <- function(annotations, attributes, id.attribute, uniqResults) {
    if (nrow(annotations) == 0) {
        return(annotations)
    }
    if (!(id.attribute %in% colnames(annotations))) {
       print(sprintf("WARNING: The unexpected set of columns in response (%s) does not include %s.",
                    paste(colnames(annotations), collapse=", "),
                    id.attribute))
       colnames(annotations) <- attributes # Is this reliable enough or should we fail?
    }

    ids <- annotations[,id.attribute]
    dup.ids <- unique(ids[duplicated(ids)])
    remove.indices <- integer()
    
    for (id in dup.ids) {
        indices <- (1:nrow(annotations))[ids == id]
        
        for (attribute in attributes) {
            if (attribute == id.attribute) next
            if (uniqResults) {
                attribute.value <- paste(setdiff(unique(annotations[indices, attribute]), ''), collapse=',')
            } else {
                attribute.value <- paste(annotations[indices, attribute], collapse=',')
            }
            if (!identical(attribute.value, annotations[indices[1],attribute])) {
                annotations[indices[1],attribute] <- attribute.value
            }
        }
        remove.indices <- c(remove.indices, indices[2:length(indices)])
    }

    if (length(remove.indices) > 0) {
        annotations <- annotations[-remove.indices,]
    }
    for (i in 1:ncol(annotations)) {
        annotations[nchar(annotations[,i]) == 0, i] <- NA
    }
    return(annotations)
}

# Returns the filter values in a format suitable for getBM. If there
# is one filter, the value is a vector. If there are several filters,
# the value is a list, each containing values for one filter. The
# list elements are in the same order as filter columns.
getFilterValues <- function(cf, filter.df, filter.columns) {
    # If filter column isn't found in input report error and return.
    for (filter.column in filter.columns) {
        if(!filter.column %in% colnames(filter.df)) {
            write.error(cf, paste("Filter column '", filter.column, "' not found in input."))
            return(PARAMETER_ERROR)
        }
    }
    
    if (length(filter.columns) == 1) {
        filter.values <- filter.df[,filter.columns]
        # Remove NA and duplicate values
        filter.values <- filter.values[!is.na(filter.values),drop=FALSE]
        filter.values <- unique(filter.values)
        return(matrix(filter.values, nrow=length(filter.values), ncol=1))
    } else {
        return(filter.df[,filter.columns])
    }
}

# Main program
execute <- function(cf)
{
    # Get input and output files and parameters
    attributes            = unlist(strsplit(get.parameter(cf, 'attributes'), ","))
    filter.types          = split.trim(get.parameter(cf, 'filterTypes'), ',')
    filter.columns        = split.trim(get.parameter(cf, 'filterColumns'), ',')
    id.attribute          = get.parameter(cf, 'idAttribute')
    biomart.db.name       = get.parameter(cf, 'mart')
    biomart.db.host       = get.parameter(cf, 'martHost')
    biomart.db.path       = get.parameter(cf, 'martPath')
    dataset               = get.parameter(cf, 'dataset')
    batchSize             = get.parameter(cf, 'batchSize', 'int')
    batch                 = (batchSize > 1)
    uniqResults           = get.parameter(cf, 'uniq', 'boolean')
    filter.df             = NULL
    annotations.path      = get.output(cf, 'annotations')
    databases.path        = get.output(cf, 'databases')
    listLayout            = get.parameter(cf, "listLayout")

    if(get.input(cf, 'filter') != "") {
        filter.df <- CSV.read(get.input(cf, 'filter'))
    }else {
        filter.df <- as.data.frame(matrix(nrow=0, ncol=length(attributes), dimnames=list(c(), attributes)))
    }
    
    const.param <- split.trim(get.parameter(cf, 'constantFilters'), ',')
    if (length(const.param) > 0) {
       const.pairs      <- split.trim(const.param, '=')
       dim(const.pairs) <- c(2, length(const.param))
    } else {
       const.pairs      <- matrix(0,2,0)
    }
   

    if (batch) {
        ## Batch mode doesn't work with only 'constantFilters'.
        if(nrow(filter.df) < 1 & ncol(const.pairs) > 0) {
            write.error(cf, "Invalid input: in batch mode the input 'filter' must be defined if constant filters are given with parameter 'constantFilters'")
            return(INVALID_INPUT)
        }
        if (nchar(id.attribute) == 0 && length(filter.types) > 0) id.attribute <- filter.types
        if (!(id.attribute %in% attributes)) {
            attributes <- c(id.attribute, attributes)
        }
    }

    # Check that input table contains at least column (name)
    if(length(colnames(filter.df)) < 1) {
        write.error(cf, "Invalid input: filter table doesn't contain any column names.")
        return(INVALID_INPUT)
    }

    # Get filter column name (first column(s)) if none is given.
    if(length(filter.columns) == 0) {
        filter.columns <- colnames(filter.df)[1:length(filter.types)]
    }
    filter.types <- c(filter.types, const.pairs[1,])

    # Get filter values
    filter.values <- getFilterValues(cf, filter.df, filter.columns)

    # If filter list was empty, write an empty files (with proper headers) and return
    if(length(filter.values) < 1 & ncol(const.pairs) == 0) {
        writeEmptyOut(annotations.path, c(filter.columns, attributes))
        writeDatabases(databases.path,
                       biomart.db.name,
                       biomart.db.host,
                       biomart.db.path,
                       dataset)
        return(0)
    }

    # Connect to the specified BioMart database
    mart = try(useMart(biomart.db.name, host=biomart.db.host, path=biomart.db.path))
    if (class(mart) == 'try-error') {
        write.error(cf, paste("Invalid parameter value: no such BioMart database. ", mart))
        return(PARAMETER_ERROR)
    }
    # Select specified dataset from the BioMart database
    available.datasets = listDatasets(mart)
    mart = try(useDataset(dataset, mart=mart))
    if (class(mart) == 'try-error') {
        write.error(cf, sprintf("Invalid parameter value: no such dataset '%s' in BioMart database '%s'. Available datasets: %s. Error message: %s",
                                dataset, biomart.db.name, paste(available.datasets[,1], collapse=" "), mart))
        return(PARAMETER_ERROR)
    }
    
    # Check that the selected attributes are found for the selected database and dataset.
    if(!all(attributes %in% listAttributes(mart)[,"name"])) {
        attributes.not.found <- attributes[!attributes %in% listAttributes(mart)[,"name"]]
        write.error(cf, sprintf("Invalid parameter value: Attribute(s) '%s' not found for BioMart database '%s' and dataset '%s'.",
                                paste(attributes.not.found, collapse=", "), biomart.db.name, dataset))
        return(PARAMETER_ERROR)
    }

    # Check that the selected filter is found for the selected database and dataset.
    if(!all(filter.types %in% listFilters(mart)[,"name"])) {
        filter.types.not.found <- filter.types[!filter.types %in% listFilters(mart)[,"name"]]
        write.error(cf, sprintf("Invalid parameter value: Filter(s) '%s' not found for BioMart database '%s' and dataset '%s'.",
                                paste(filter.types.not.found, collapse=", "), biomart.db.name, dataset))
        return(PARAMETER_ERROR)
    }

    if (batch) {
        # Break query into smaller queries with batchSize rows.
        n.queries         <- ceiling(nrow(filter.values)/batchSize)
        annotation.matrix <- matrix()

        # Query results.
        for(i in 1:n.queries) {
            if (i == n.queries) {
                indices <- ((i-1)*batchSize+1):(nrow(filter.values))
            } else {
                indices <- ((i-1)*batchSize+1):(i*batchSize)
            }
            
            if (ncol(const.pairs) > 0) {
                fValues <- list()
                fValues[[1]] <- filter.values[indices,]
                for (j in 1:ncol(const.pairs)) {
                  fValues[[j+1]] <- const.pairs[2,j]
                }
            } else {
                fValues <- filter.values[indices,]
            }
            
            result.matrix <- getBM(attributes=attributes, filter=filter.types, values=fValues, mart=mart, uniqueRows=uniqResults)
            
            if(listLayout){
                result.matrix <- collapseAttributesBatch(result.matrix, attributes, id.attribute, uniqResults)
            }else if(nrow(result.matrix) > 0){
                if(uniqResults){
                    result.matrix <- unique(result.matrix)
                }
                result.matrix[result.matrix==''] <- NA
            }
            
            write.log(cf, sprintf("Processed %d rows\n", indices[length(indices)]))
            
            if(nrow(result.matrix) > 0){
                if(i == 1) {
                    annotation.matrix <- result.matrix
                }else {
                    annotation.matrix <- rbind(annotation.matrix, result.matrix)
                }
            }             
        }
        # Add filter values that were not found to the annotation table.
        if (any(!is.na(annotation.matrix))) {
        } else {
            write.error(cf, sprintf("No results after processing all batches.\n"))
            return(GENERIC_ERROR)
        }
        not.found           <- setdiff(filter.values, annotation.matrix[,id.attribute])
        nf.matrix           <- matrix(as.character(NA), nrow=length(not.found), ncol=ncol(annotation.matrix))
        nf.matrix[,1]       <- not.found
        colnames(nf.matrix) <- colnames(annotation.matrix)
        annotation.matrix   <- rbind(annotation.matrix, nf.matrix)

        colnames(annotation.matrix)[colnames(annotation.matrix) == id.attribute] <- filter.columns
    } else {
        fCount <- nrow(filter.values)

        # Create matrix for output.
        annotation.matrix <- matrix('', nrow=nrow(filter.values), ncol=length(filter.columns)+length(attributes))
        # Get annotations one by one. This is very slow.
        isMultiF <- (length(filter.types) > 1)

        library(RCurl)
        curlHandle <- getCurlHandle()
        for (i in 1:fCount) {
            if (i == 0) next;
            if (isMultiF) {
                filter.value <- list()
                if (fCount > 0) {
                   filter.value <- c(filter.value, filter.values[i,], const.pairs[2,])
                } else {
                   filter.value <- c(filter.value, const.pairs[2,])
                }
            } else {
                if (fCount == 0) {
                   filter.value <- const.pairs[2,]
                } else {
                   filter.value <- filter.values[i,]
                }
            }

            annotations <- getBM(attributes=attributes,
                                 filter    =filter.types,
                                 values    =filter.value,
                                 mart      =mart,
                                 uniqueRows=uniqResults,
                                 curl      =curlHandle)

            ## If there is just one attribute getBM will return a character 
            ## vector instead of a data.frame which is required for 
            ## collapseAttributes.
            if(!is.data.frame(annotations) && length(attributes)==1) {
                annotations <- data.frame(annotations)
                colnames(annotations) <- attributes
            }

            if(i %% N_QUERIES == 0 || i == fCount) {
                write.log(cf, sprintf("Processed %d rows\n", i))
            }

            if (listLayout){
                # Only unique ID's for output annotations (multiple annotations as comma sep. list)
                annotations.collapsed <- collapseAttributes(annotations, attributes, uniqResults, listLayout)
                if (fCount > 0) {
                   annotation.matrix[i,] <- c(unlist(filter.values[i,]), annotations.collapsed)
                } else {
                   annotation.matrix <- t(annotations.collapsed)
                }
            } else {
                if (fCount > 0) {
                   annotations <- cbind(filter.values[i,], annotations)
                }
                if (i == 1) {
                    annotation.matrix <- annotations
                }else{
                    annotation.matrix = rbind(annotation.matrix, annotations)
                }
            }
        }
        if (fCount > 0) {
            colnames(annotation.matrix) <- c(filter.columns, attributes)
        } else {
            colnames(annotation.matrix) <- attributes
        }
    }

    # Write output.
    CSV.write(annotations.path, annotation.matrix)
    
    # Write databases file
    writeDatabases(databases.path,
                   biomart.db.name,
                   biomart.db.host,
                   biomart.db.path,
                   dataset)
    return(0)
}
main(execute)
