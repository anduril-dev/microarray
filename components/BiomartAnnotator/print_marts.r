library(biomaRt)

OUT.DIR <- 'doc-files'

write.data.frame <- function(fr, format.pattern, columns, out.filename) {
    arg <- list(format.pattern)
    for (i in columns) {
        v <- c(colnames(fr)[i], as.character(fr[,i]))
        v[is.na(v)] <- ''
        arg[[length(arg)+1]] <- v
    }
    string <- paste(do.call(sprintf, arg), collapse='\n')
    out <- paste(OUT.DIR, out.filename, sep='/')
    cat(string, '\n', file=out, sep='')
    cat('Wrote', out, '\n')
}

write.data.frame(listMarts(), '%-25s %s', 1:2, 'marts.txt')
mart <- useMart('ensembl')
write.data.frame(listDatasets(mart), '%-30s %-12s %s', c(1,3,2), 'ensembl-datasets.txt')
mart <- useDataset('hsapiens_gene_ensembl', mart)
write.data.frame(listAttributes(mart), '%-35s %s', 1:2, 'ensembl-hsapiens-attrs.txt')
write.data.frame(listFilters(mart), '%-30s %s', 1:2, 'ensembl-hsapiens-filters.txt')
