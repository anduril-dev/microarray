library(componentSkeleton)

# Read an optional table
table.in <- function(cf, i) {
  tn <- paste('table',i,collapse="",sep="")
  if (input.defined(cf, tn)) {
     data <- CSV.read(get.input(cf, tn))
     write.log(cf, paste("Input ",tn," contains ",ncol(data)," columns and ",nrow(data)," rows.",sep=""))
  } else {
     data <- NULL
  }
  data
}

# Open connection to file
open.connection <- function(cf, i) {
    tn <- paste('table',i,collapse="",sep="")
    if (input.defined(cf, tn)) {
        data <- file(get.input(cf, tn), 'r')
    } else {
        data <- NULL
    }
    data
}

# Open connection to optional folder
get.folder <- function(cf, folder) {
    if(input.defined(cf, folder)) {
        data <- get.input(cf, folder)
    } else {
        data <- NULL
    }
    data
}

execute <- function(cf) {
    instance.name <- get.metadata(cf, 'instanceName')
    myName        <- instance.name # For backwards compability; may be removed in future
    as.con        <- get.parameter(cf, 'asConnection', 'boolean')
    table1        <- if(!as.con) table.in(cf, 1) else open.connection(cf, 1)
    table2        <- if(!as.con) table.in(cf, 2) else open.connection(cf, 2)
    table3        <- if(!as.con) table.in(cf, 3) else open.connection(cf, 3)
    table4        <- if(!as.con) table.in(cf, 4) else open.connection(cf, 4)
    table5        <- if(!as.con) table.in(cf, 5) else open.connection(cf, 5)
    var1          <- get.folder(cf, 'var1');
    var2          <- get.folder(cf, 'var2');
    var3          <- get.folder(cf, 'var3');
    param1        <- get.parameter(cf, 'param1')
    param2        <- get.parameter(cf, 'param2')
    param3        <- get.parameter(cf, 'param3')
    param4        <- get.parameter(cf, 'param4')
    param5        <- get.parameter(cf, 'param5')
    param6        <- get.parameter(cf, 'param6')
    param7        <- get.parameter(cf, 'param7')
    param8        <- get.parameter(cf, 'param8')
    param9        <- get.parameter(cf, 'param9')
    param10       <- get.parameter(cf, 'param10')
    src.file      <- get.input(cf, 'script')

    # Read in the input array into a list of data frames.    
    if(input.defined(cf, 'array')) {
        array.temp    <- Array.read(cf, 'array')
        array <- list()
        for(i in 1:Array.size(array.temp)) {
            key <- Array.getKey(array.temp, i)
            file <- Array.getFile(array.temp, key)
            if(as.con) {
                array[[key]] <- file(file, 'r')
            }else {
                array[[key]] <- CSV.read(file)
            }
        }
    }else {
        array <- NULL
    }

    # Prepare the document
    document.dir <- get.output(cf, 'document')
    dir.create(document.dir, recursive=TRUE)
    document.out <- sprintf('%s produced no \\LaTeX{} output.',
                            latex.quote(instance.name))

    # Prepare the optional outputs
    optOut1 <- ""
    optOut2 <- ""
    optOut3 <- ""

    # Create array output dir.
    array.out.dir <- get.output(cf, 'array')
    if (!file.exists(array.out.dir)) {
        dir.create(array.out.dir, recursive=TRUE)
    }

    # Execute the script
    source(src.file,local=TRUE)

    # Prepare the choice selection
    caseOut <- get.output(cf, '_choices')
    if (!exists('defCase') || defCase) {
        cat(file=caseOut, 'defCase')
    } else {
        cat(file=caseOut, 'altCase')
    }

    # If inputs opened as connection to files close them
    if(as.con) {
        if(!is.null(table1)) close(table1)
        if(!is.null(table2)) close(table2)
        if(!is.null(table3)) close(table3)    
        if(!is.null(table4)) close(table4)
        if(!is.null(table5)) close(table5)
    }

    # Save results
    if (!exists("table.out")) {
        write.error(cf, "No output (table.out) defined during the evaluation.")
        return(1)
    }
    CSV.write(get.output(cf, 'table'), table.out)

    if (exists('optOut1')) cat(optOut1, file=get.output(cf, 'optOut1'))
    if (exists('optOut2')) cat(optOut2, file=get.output(cf, 'optOut2'))
    if (exists('optOut3')) cat(optOut3, file=get.output(cf, 'optOut3'))

    latex.write.main(cf, 'document', document.out)
    
    # Write array output if it exists.
    array.out.object <- Array.new()    
    if (exists('array.out')) {        
        for(i in 1:length(array.out)) {
            key = names(array.out)[i]
            filename = paste(key, ".csv", sep="")
            CSV.write(paste(array.out.dir, "/", filename, sep=""), array.out[[i]])
            array.out.object <- Array.add(array.out.object, key, filename)
        }        
    }
    Array.write(cf, array.out.object, 'array')

    return(0)
}

main(execute)
