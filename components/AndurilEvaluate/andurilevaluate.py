#!/usr/bin/python
import sys,os
from anduril.args import *
import anduril
import subprocess,shlex
import shutil
import re

def uniquename(testList,newitem):
    ''' Returns a unique new list item, in the fashion of  1_"string" ... '''
    if not newitem in testList:
        return newitem
    keystring=re.compile(r"(\d*)_(.*)")
    i=0
    matches=keystring.match(newitem)
    if matches==None:
        i=1
        newitem=str(i)+"_"+newitem
    while newitem in testList:
        matches=keystring.match(newitem)
        i+=1
        newitem=str(i)+"_"+matches.group(1)
    return newitem    

def create_file(f,content=""):
    fid=open(f,'w')
    fid.write(content)
    fid.close()

cf = anduril.CommandFile.from_file(sys.argv[1])
input_folder=cf.get_input('inputs')

# Add the input rows at the beginning of script
scriptIn=""
if input_folder!=None:
    if expandInputs:
        for row in input_inputs:
            scriptIn+=row+'=INPUT(path="'+input_inputs[row]+'")\n'
    else:
        scriptIn+='inArray=INPUT(path="'+input_folder+'")\n'
        scriptIn+='inArray=inArray.in\n'

# append the user generated script
firstPath = None
for i, row in enumerate(input_script):
    scriptIn+= open(input_script[row],'rt').read()
    if i == 0:
        firstPath = input_script[row]

# write the script to file
create_file(output_script,scriptIn)

# Handle externalExecution
if externalExecution:
    # Make empty 'execution' output and replace variable with the real one
    os.mkdir(execution)
    execution = externalExecution
    if not os.path.isabs(execution):
        execution = os.path.join(os.path.dirname(firstPath), execution)
    if not keepExecution:
        print("Warning: keepExecution was set to true because externalExecution is used")
        write_log("Warning: keepExecution was set to true because externalExecution is used")
        keepExecution = True

if link and not keepExecution:
    write_error('When file linking is enabled, keepExecution must be true')
    sys.exit(1)

# run Anduril
andurilArgs=['anduril','run',output_script,'-d',execution,'--log',log]
extraCommands=shlex.split(commandline)
if extraCommands:
    andurilArgs.extend(extraCommands)
print(andurilArgs)
p=subprocess.call(andurilArgs)

# copy files to outputs array, and add the key/file pairs
#outputList = os.listdir(os.path.join(execution,'output'))
# Todo: nicer names, based on just the instance name?

if not os.path.exists(os.path.join(execution,'outArray/array/_index')):
    write_error('The execution folder did not have outArray/array/_index file.')
    write_error('Maybe you forgot the "outArray=ArrayCombiner(...)"?')
    sys.exit(1)

output_reader=anduril.table.TableReader(os.path.join(execution,'outArray/array/_index'), 
                                        type='dict', column_types=str)

for out_file in output_reader:
    fileName, fileExt = os.path.splitext(out_file['File'])
    target = os.path.join(outputs.get_folder(), out_file['Key']+fileExt)
    if link:
        os.symlink(out_file['File'], target) 
    else:
        if os.path.isfile(out_file['File']):
            shutil.copy2(out_file['File'], target)
        if os.path.isdir(out_file['File']):
            shutil.copytree(out_file['File'], target)
    outputs.write(out_file['Key'],out_file['Key']+fileExt)

# delete execution folder if needed
if not keepExecution:
    try:
        shutil.rmtree(execution)
    except OSError:
        # NFS sometimes needs two rmtrees?
        shutil.rmtree(execution)
    os.mkdir(execution)
