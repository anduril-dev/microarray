// This invalid model has a typo in "parameterX"
model Main
	Real x;
	parameterX Real y = 5;
equation
	der(x) = 1;
end Main;
