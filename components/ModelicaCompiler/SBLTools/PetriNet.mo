/*
Code is partially based on the following works:
[1] Pieter J. Mosterman, Martin Otter, Hilding Elmqvist.
Modeling Petri Nets As Local Constraint Equations For Hybrid Systems
Using Modelica. 1998.
[2] Stefan M.O. Fabricius. ExtendedPetriNets: Extensions to the Petri Net
Library in Modelica. 2001. http://www.modelica.org/libraries/ExtendedPetriNets
*/

// TODO: EventGenerator with synch=RANDOM_ONE sometimes skips one event

package PetriNet

import SBLTools.Random;

connector Arc
	Boolean canFire;
	Boolean firing;
	Integer canFireRate;
	Integer firingRate;
end Arc;

model Place
	constant Integer numInput = 1;
	constant Integer numOutput = 1;
	parameter Integer initTokens = 0;
	parameter Integer maxTokens = 2^29 - 1;
	input Arc[numInput] inArcs;
	output Arc[numOutput] outArcs;
	Integer tokens(start=initTokens);
protected
	Integer newTokens(start=initTokens);
	Boolean full;
algorithm
	newTokens := tokens;
	for i in 1:numInput loop
		when inArcs[i].firing then
			newTokens := newTokens + inArcs[i].firingRate;
		end when;
	end for;
	for i in 1:numOutput loop
		when outArcs[i].firing then
			newTokens := newTokens - outArcs[i].firingRate;
		end when;
	end for;
equation
	tokens = pre(newTokens);

	full = tokens > maxTokens - 0.99;
	inArcs.canFire = fill(not full, numInput);
	inArcs.canFireRate = fill(maxTokens - tokens, numInput);
	
//	assert(tokens >= 0, "tokens is negative");
//	assert(tokens <= maxTokens, "tokens is larger than maxTokens");
	
	for i in 1:numOutput loop
		outArcs[i].canFireRate = if i == 1
			then tokens
			else tokens - outArcs[i-1].firingRate;
		outArcs[i].canFire = if i == 1
			then tokens > 0.99
			else outArcs[i-1].canFire and outArcs[i].canFireRate > 0.99;
	end for;
end Place;

model Place01
	extends Place(numInput=0, numOutput=1);
end Place01;

model Place10
	extends Place(numInput=1, numOutput=0);
end Place10;

partial model BaseTransition
	constant Integer numInput = 1;
	constant Integer numOutput = 1;
	input Arc[numInput] inArcs;
	output Arc[numOutput] outArcs;
	Integer firingRate "Preferred number of firings when the transition is enabled" ;
protected
	Boolean firing "Event that indicates a firing; set by subclasses";
	Integer actualRate;
equation
	inArcs.firing = fill(firing, numInput);
	outArcs.firing = fill(firing, numOutput);

	when firing then
		if numOutput > 0 then
			actualRate = min({firingRate, min(inArcs.canFireRate), min(outArcs.canFireRate)});
			outArcs.firingRate = fill(actualRate, numOutput);
		else
			actualRate = min(firingRate, min(inArcs.canFireRate));
		end if;
		inArcs.firingRate = fill(actualRate, numInput);
	end when;

//	assert(actualRate >= 0, "actualRate is negative");
end BaseTransition;

model Transition "Transition that is synchronized to an external event (condition)"
	extends BaseTransition;
	Boolean condition;
equation
	if numOutput > 0 then
		firing = condition and AllTrue(inArcs.canFire) and AllTrue(outArcs.canFire);
	else
		firing = condition and AllTrue(inArcs.canFire);
	end if;
end Transition;

model DelayTransition "Timed transition that is fired only after a delay has passed since enabling"  
	extends BaseTransition;
	parameter Real delay = 1;
protected
	Boolean activated;
	Boolean delayPassed;
	Real lastActivationTime(start=-delay);
equation
	activated = AllTrue(inArcs.canFire) and AllTrue(outArcs.canFire) and not pre(firing);

	when activated then
		lastActivationTime = time;
	end when;

	delayPassed = time - delay > lastActivationTime;
	firing = activated and delayPassed;
	
//	assert(delay > 0, "delay must be positive");
end DelayTransition;

function AllTrue "Logical AND of vector"
	input Boolean[:] b;
	output Boolean result;
algorithm
	result := true;
	for i in 1:size(b, 1) loop
		result := result and b[i];
		if not result then
			return;
		end if;
	end for;
end AllTrue;

// Events

type Synchronization = enumeration(INCREASING, DECREASING, RANDOM_ONE, PERMUTATION, CLOCK);

model EventGenerator
	constant SBLTools.Random.RandomGenerator rng;
	parameter Integer N;
	parameter Synchronization synch;
	output Boolean[N] events;
protected
	Integer rndStream(start=-1);
	Boolean clock;
	Integer[N] order;
	Integer counter(start=-1);
equation
	when initial() then
		if synch == Synchronization.INCREASING then
			rndStream = Random.NewIntegerStreamRange(rng, 1, N, 1, Random.StreamType.CYCLIC);
		elseif synch == Synchronization.DECREASING then
			rndStream = Random.NewIntegerStreamRange(rng, N, 1, -1, Random.StreamType.CYCLIC);
		elseif synch == Synchronization.RANDOM_ONE then
			rndStream = Random.NewIntegerStreamRange(rng, 1, N, 1, Random.StreamType.SELECT_RANDOM);
		elseif synch == Synchronization.PERMUTATION then
			rndStream = Random.NewIntegerStreamRange(rng, 1, N, 1, Random.StreamType.PERMUTATION);
		elseif synch == Synchronization.CLOCK then
			rndStream = -1;
		end if;
	end when;
	
	clock = sample(0.01, 1);

	when clock and not initial() then
		counter = mod(pre(counter) + 1, N);
		for i in 1:N loop
			order[i] = Random.NextIntegerStreamElement(rng, rndStream);
		end for;
	end when;
	
	for i in 1:N loop
		if synch == Synchronization.RANDOM_ONE then
			events[i] = clock and not initial() and order[1] == i;
		elseif synch == Synchronization.CLOCK then
			events[i] = clock and not initial();
		else
			events[i] = clock and not initial() and order[i] == counter+1;
		end if;
	end for;
end EventGenerator;

end PetriNet;
