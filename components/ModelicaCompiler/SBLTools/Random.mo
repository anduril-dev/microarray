package Random

	class RandomGenerator
		extends ExternalObject;
		
		function constructor
			output RandomGenerator rng;
			external "C" rng = initialize();
			annotation(Library="librnd.a", Include="#include \"rnd.h\"");
		end constructor;
		
		function destructor
			input RandomGenerator rng;
			external "C" finalize(rng);
			annotation(Library="librnd.a", Include="#include \"rnd.h\"");
		end destructor;
	end RandomGenerator;
	
	// Elementary Mersenne Twister operations
	
	function RandomReal "Obtain a random real in range [0, 1)"
		input RandomGenerator rng;
		output Real value;
		external "C" value = random_double();
	end RandomReal;
	
	function RandomInteger "Obtain a random integer in range [-2^31, 2^31-1]"
		input RandomGenerator rng;
		output Integer value;
		external "C" value = random_int();
	end RandomInteger;
	
	// Discrete distributions

	function RandomBinomial "Obtain a random integer from the binomial distribution Bin(n, p). Running time: O(n*min(p, 1-p))."
	// Algorithm based on review (Section 2) in Kachitvichyanukul and Schmeiser:
	// Binomial random variate generation. Communications of the ACM, 1988(31)2, 216-222.
		input RandomGenerator rng;
		input Integer n "N parameter, number of random independent trials";
		input Real p "P parameter, probability of success of each trial";
		output Integer value;
	protected
		Real q, s, a, r;
		Integer x;
		Real u;
		Boolean invert;
	algorithm
		assert(n >= 1, "n must be >= 1");
		assert(p > 0 and p < 1, "p must be between 0 and 1 exclusive");
		
		if (p > 0.5) then
			invert := true;
			p := 1 - p;
		else
			invert := false;
		end if;
		
		q := 1 - p;
		s := p / q;
		a := (n+1) * s;
		r := q^n;
		
		u := RandomReal(rng);
		x := 0;
		
		while u > r loop
			u := u - r;
			x := x + 1;
			r := ((a/x) - s) * r;
		end while;
		
		value := if invert then n-x else x;
	end RandomBinomial;
	
	function RandomIntegerRange "Obtain a random integer in range [low, high]"
		input RandomGenerator rng;
		input Integer low "Low bound, inclusive";
		input Integer high "High bound, inclusive";
		output Integer value;
	algorithm
		assert(high >= low, "high must be >= low");
		value := integer(floor(low + (RandomReal(rng) * (high+1 - low))));
		assert(value >= low, "value is below low");
		assert(value <= high, "value is above high");
	end RandomIntegerRange;
	
	// Continuous distributions
	
	function RandomExponential "Obtain a real value from the exponential distribution Exp(lambda)"
		input RandomGenerator rng;
		input Real lambda "Lambda parameter, with expected value = 1/lambda";
		output Real value;
	protected
		constant Real low = 1e-12 "Prevents numeric instability at 0";
		Real rand;
	algorithm
		assert(lambda > 0, "lambda must be positive");
		rand := max(RandomReal(rng), low); 
		value := -log(rand) / lambda;
		assert(value > 0, "value is non-positive");
	end RandomExponential;
	
	function RandomNormal "Obtain a real value from the normal distribution N(mean, sd)"
	// Algorithm based on Joseph L. Leva: A fast normal random number generator.
	// ACM Transactions on Mathematical Software, 1992(18)4, 449-453.
	// TODO: check normality of results to ensure correct implementation.
		input RandomGenerator rng;
		input Real mean "Mean of the normal distribution";
		input Real sd "Standard deviation of the normal distribution";
		output Real value;
	protected
		Real u, v;
		Real x, y, Q;
		Boolean accept;
		constant Real low = 1e-12 "Prevents numeric instability at 0";
		constant Real vScale = 1.7156;
		constant Real s = 0.449871;
		constant Real t = -0.386595;
		constant Real a = 0.19600;
		constant Real b = 0.25472;
	algorithm
		assert(sd > 0, "sd must be positive");
		accept := false;
		while not accept loop
			u := max(RandomReal(rng), low);
			v := vScale * (max(RandomReal(rng), low) - 0.5);
			x := u - s;
			y := abs(v) - t;
			Q := x*x + y*(a*y - b*x);
			
			if Q < 0.27597 or not (Q > 0.27846 or v*v > -4*u*u*log(u)) then
				accept := true;
			end if;
		end while;
		value := (v/u)*sd + mean;
	end RandomNormal;
	
	function RandomUniform "Obtain a real value from the uniform distribution [low, high)"
		input RandomGenerator rng;
		input Real low "Low bound, inclusive";
		input Real high "High bound, exclusive";
		output Real value;
	algorithm
		assert(high >= low, "high must be >= low");
		value := low + (RandomReal(rng) * (high - low));
		assert(value >= low, "value is below low");
		assert(value < high, "value is above high");
	end RandomUniform;
	
	/*
	// Shuffling
	// Array assignment does not seem to work in OpenModelica so this is disabled.
	// Try permutations streams instead.
	
	function ShuffleReal "Shuffle an array of reals using Fisher-Yates algorithm"
		replaceable type T = Real;
		input RandomGenerator rng;
		input T[:] arr;
		output T[size(arr, 1)] shuffled;
	protected
		Integer index;
		T tmp;
	algorithm
		shuffled := arr;
		for i in size(arr, 1):-1:1 loop
			index := RandomIntegerRange(rng, 1, i);
			tmp := shuffled[i];
			shuffled[i] := shuffled[index];
			shuffled[index] := tmp;
		end for;
	end ShuffleReal;
	
	function ShuffleInteger "Shuffle an array of integers using Fisher-Yates algorithm"
		extends ShuffleReal(redeclare type T=Integer);
	end ShuffleInteger;
	*/

	// Streams
	
	// In C, CYCLIC = 1, PERMUTATION = 2 and SELECT_RANDOM = 3
	type StreamType = enumeration(
		CYCLIC "Produce array elements in cycles, but do not randomize",
		PERMUTATION "Permute the array and return each element of the permutation in turn",
		SELECT_RANDOM "Select a random array element on each call"
	);
	
	function NewIntegerStream
		input RandomGenerator rng;
		input Integer[:] arr;
		input StreamType streamType = StreamType.CYCLIC;
		output Integer id;
		external "C" id = new_stream_int(streamType, arr, size(arr, 1));
	end NewIntegerStream;

	function NewIntegerStreamRange
		input RandomGenerator rng;
		input Integer startValue;
		input Integer endValue;
		input Integer step = 1;
		input StreamType streamType = StreamType.CYCLIC;
		output Integer id;
		external "C" id = new_stream_int_range(streamType, startValue, endValue, step);
	end NewIntegerStreamRange;
	
	function NextIntegerStreamElement
		input RandomGenerator rng;
		input Integer streamID;
		output Integer element;
		external "C" element = next_stream_element(streamID);
	end NextIntegerStreamElement;
	
end Random;
