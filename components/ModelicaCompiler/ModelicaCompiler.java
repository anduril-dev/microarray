import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;

public class ModelicaCompiler extends SkeletonComponent {

    public final static String ENV_MODELICA_HOME = "OPENMODELICAHOME";
    public final static String ENV_MODELICA_LIBRARY = "OPENMODELICALIBRARY";
    public final static String RESOURCE_PATTERN = "[.](a|dll|h|lib|so)$";
    public final static String STDERR_PREFIX = "<<STDERR>>";
    
    public static class ModelicaFilter implements FilenameFilter {
        @Override
        public boolean accept(File dir, String name) {
            return name.endsWith(".mo") || name.endsWith(".mof");
        }
    }
    
    public static class LogHandler extends Handler {
        private static final Pattern errorPattern = Pattern.compile("^\\[(.*?)[.]mo:(.*?)\\] Error");
        
        private boolean errors = false;
        private StringBuffer content = new StringBuffer();
        
        @Override
        public void close() { }

        @Override
        public void flush() { }
        
        @Override
        public void publish(LogRecord record) {
            if (record == null) return;
            String msg = record.getMessage();
            
            if (errorPattern.matcher(msg).find()) {
                this.errors = true;
                System.out.println(msg);
            } else if (msg.startsWith(STDERR_PREFIX)) {
                System.out.println(msg.substring(STDERR_PREFIX.length()));
            } else {
                this.content.append(msg);
                if (!msg.endsWith("\n") && !msg.endsWith("\r")) this.content.append('\n');
            }
        }
        
        public boolean hasErrors() {
            return this.errors;
        }
        
        public String getContent() {
            return this.content.toString();
        }
    }
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        File sourceDir = cf.getInput("sourceModel");
        File binaryDir = cf.getOutput("binaryModel");
        binaryDir.mkdirs();

        File[] sourceModels = sourceDir.listFiles(new ModelicaFilter());
        if (sourceModels.length == 0) {
            cf.writeError("No Modelica model files found (*.mo, *.mof)");
            return ErrorCode.INVALID_INPUT;
        }
        
        String[] libraries = cf.getParameter("libraries").split(",");
        
        List<String> compileCommand = new ArrayList<String>();
        compileCommand.add(cf.getParameter("compilerCommand"));
        compileCommand.add("+s");
        for (int i=0; i<sourceModels.length; i++) {
            compileCommand.add(sourceModels[i].getName());
        }
        for (String lib: libraries) {
            lib = lib.trim();
            if (lib.isEmpty()) continue;
            compileCommand.add(lib);
        }

        Tools.copyDirectory(sourceDir, binaryDir);
  
        Map<String, String> env = getEnvironment(cf);
        
        Logger logger = Logger.getAnonymousLogger();
        logger.setUseParentHandlers(false);
        LogHandler handler = new LogHandler();
        logger.addHandler(handler);
        
        int status = IOTools.launch(compileCommand.toArray(new String[] {}),
                binaryDir, env, "", STDERR_PREFIX, null, logger);
        
        final String modelName = cf.getParameter("modelName");
        
        File flatDir = cf.getOutput("flatModel");
        flatDir.mkdirs();
        File flatFile = new File(flatDir, String.format("%s.mof", modelName));
        Tools.writeString(flatFile, handler.getContent());

        if (status != 0 || handler.hasErrors()) {
            cf.writeError("The model contains errors");
            return ErrorCode.INVALID_INPUT;
        }
        
        String[] searchPaths = env.get(ENV_MODELICA_LIBRARY).split(System.getProperty("path.separator"));
        copyLibraryResources(libraries, searchPaths, binaryDir);
        
        String[] makeCommand = new String[3];
        makeCommand[0] = cf.getParameter("makeCommand");
        makeCommand[1] = "-f";
        makeCommand[2] = String.format("%s.makefile", modelName);

        status = IOTools.launch(makeCommand, binaryDir, env, "", "");
        if (status != 0) {
            cf.writeError("Make failed");
            return ErrorCode.INVALID_INPUT;
        }

        File executable = new File(binaryDir, modelName);
        if (!executable.exists()) {
            executable = new File(binaryDir, String.format("%s.exe", modelName));
            if (!executable.exists()) {
                cf.writeError("Can not locate model executable");
                return ErrorCode.ERROR;
            }
        }
        
        writeProperties(executable.getName(), modelName, binaryDir);
        
        return ErrorCode.OK;
    }
    
    private Map<String, String> getEnvironment(CommandFile cf) {
        Map<String, String> env = new HashMap<String, String>(System.getenv());
        
        String cFlags = cf.getParameter("cFlags");
        if (!cFlags.isEmpty()) {
            env.put("MODELICAUSERCFLAGS", cFlags);
        }
        
        if (!env.containsKey(ENV_MODELICA_HOME) || env.get(ENV_MODELICA_HOME).isEmpty()) {
            final String[] UNIX_SEARCH_DIRS = {
                    "/usr", // default for OpenModelica 1.5
                    "/usr/local",
                    "/usr/share/openmodelica-1.4",
                    "/usr/share/openmodelica-1.4-dev",
            };
            for (String dir: UNIX_SEARCH_DIRS) {
                File omc = new File(dir, "/bin/omc");
                if (omc.canExecute()) {
                    System.out.println(String.format("%s is not set; setting it to %s", ENV_MODELICA_HOME, dir));
                    env.put(ENV_MODELICA_HOME, dir);
                    break;
                }
            }
        }
        
        String libEnv = env.get(ENV_MODELICA_LIBRARY);
        if (libEnv == null || libEnv.isEmpty()) {
            final String[] UNIX_SEARCH_DIRS = {
                    "/usr/share/omlibrary/modelicalib", // default for OpenModelica 1.5
                    "/usr/local/share/omlibrary/modelicalib",
                    "/usr/share/openmodelica-1.4/modelica-2.2",
                    "/usr/share/openmodelica-1.4-dev/modelica-2.2",
            };
            for (String dir: UNIX_SEARCH_DIRS) {
                File omc = new File(dir, "/Modelica");
                if (omc.isDirectory()) {
                    System.out.println(String.format("%s is not set; setting it to %s", ENV_MODELICA_LIBRARY, dir));
                    libEnv = dir;
                    break;
                }
            }
        }
        
        libEnv += System.getProperty("path.separator") + new File(".").getAbsolutePath();
        env.put(ENV_MODELICA_LIBRARY, libEnv);
        
        return env;
    }
    
    private void writeProperties(String executableName, String modelName, File binaryDir) throws IOException {
        Properties props = new Properties();
        props.put("binary.file", executableName);
        props.put("parameters.file", String.format("%s_init.txt", modelName));
        props.put("parameters.format", "OpenModelica");
        props.put("results.file", String.format("%s_res.plt", modelName));
        props.put("results.format", "Ptolemy");
        File propFile = new File(binaryDir, "config.ini");
        BufferedWriter writer = new BufferedWriter(new FileWriter(propFile));
        try {
            props.store(writer, null);
        } finally {
            writer.close();
        }
    }
    
    private void copyLibraryResources(String[] libraries, String[] searchPaths, File targetDir) throws IOException {
        for (String library: libraries) {
            
            File libDir = null;
            for (String path: searchPaths) {
                libDir = new File(path, library);
                if (libDir.isDirectory()) break;
                libDir = null;
            }
            if (libDir == null) continue;
            
            Pattern resourcePattern = Pattern.compile(RESOURCE_PATTERN);
            for (File file: libDir.listFiles()) {
                if (resourcePattern.matcher(file.getName()).find()) {
                    System.out.println("Copying resource "+file.getAbsolutePath());
                    Tools.copyFile(file, new File(targetDir, file.getName()));
                }
            }
        }
    }
    
    public static void main(String[] args) {
        new ModelicaCompiler().run(args);
    }
}
