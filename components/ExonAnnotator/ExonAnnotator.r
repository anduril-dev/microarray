library(componentSkeleton)
library(RMySQL)

execute <- function(cf) {
	# Read input parameters from command file
	write.log(cf, "Reading input and parameters from command file ...")
	query <- get.input(cf,'query')
	dbConnect.path <- get.input(cf, 'dbConnect')
	out.path <- get.output(cf, 'table')

	KeyColumn <- get.parameter(cf,'KeyColumn')
	input.type <- get.parameter(cf,'InputType')
	target.type <- get.parameter(cf,'TargetType')
	large <- as.logical(get.parameter(cf,'large'))

        if (KeyColumn == "") KeyColumn <- 1

	input.table <- CSV.read(query)
	input.key <- input.table[,KeyColumn]

	dbConnect <- CSV.read(dbConnect.path)
	host <- dbConnect$host
	database <- dbConnect$database
	user <- dbConnect$user
	passwd <- dbConnect$password
	port <- dbConnect$port

	if(is.na(passwd)){
                passwd <- ""
        }
        
        if(is.na(port)){
                port <- NULL
        }

	if(input.type == "Gene"){
	    if(!large){
		stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        from gene
		        join transcript on gene.gene_id = transcript.gene_id
		        join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        join exon on exon.exon_id = exon_transcript.exon_id
		        where gene.stable_id in ('"
		tmp <- paste(input.key, collapse = "','")
		stm <- paste(stm,tmp,"')",sep = "")

		write.log(cf, "Searching from database ...")
		m <- dbDriver("MySQL")
        	if(is.null(port)){
           	     con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
        	}else{
          	      con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
        	}
	
		res <- dbSendQuery(con, stm)
        	df <- fetch(res, n = -1)	
		colnames(df) <- c("Gene","Transcript","Exon")
	    }else{
		nloop <- (length(input.key)-length(input.key)%%50000)/50000
		flag <- length(input.key)%%50000
		meta_list <- list()
		for(i in 1:nloop){
			start <- (i-1)*50000+1
			end <- i*50000
			stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        	from gene
		       		join transcript on gene.gene_id = transcript.gene_id
		        	join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        	join exon on exon.exon_id = exon_transcript.exon_id
		        	where gene.stable_id in ('"
			tmp <- paste(input.key[start:end],collapse="','")
			stm <- paste(stm,tmp,"')",sep="")

			m <- dbDriver("MySQL")
			if(is.null(port)){
				con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
			}else{
				con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
			}
	
			res <- dbSendQuery(con, stm)
			meta_list[[i]] <- fetch(res, n = -1)
		}
		if(flag!=0){
			i <- i + 1
			start <- end + 1
			end <- length(input.key)
			stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        	from gene
		       		join transcript on gene.gene_id = transcript.gene_id
		        	join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        	join exon on exon.exon_id = exon_transcript.exon_id
		        	where gene.stable_id in ('"
			tmp <- paste(input.key[start:end],collapse="','")
			stm <- paste(stm,tmp,"')",sep="")

			m <- dbDriver("MySQL")
			if(is.null(port)){
				con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
			}else{
				con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
			}
			res <- dbSendQuery(con, stm)
			meta_list[[i]] <- fetch(res, n = -1)
		}
		df <- do.call(rbind,meta_list)
		colnames(df) <- c("Gene","Transcript","Exon")
		rm(meta_list)
		gc()
	    }

	}else if(input.type == "Transcript"){

		if(!large){
		stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        from gene
		        join transcript on gene.gene_id = transcript.gene_id
		        join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        join exon on exon.exon_id = exon_transcript.exon_id
		        where transcript.stable_id in ('"
		tmp <- paste(input.key, collapse = "','")
		stm <- paste(stm,tmp,"')",sep = "")

		write.log(cf, "Searching from database ...")
		m <- dbDriver("MySQL")
        	if(is.null(port)){
           	     con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
        	}else{
          	      con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
        	}
	
		res <- dbSendQuery(con, stm)
        	df <- fetch(res, n = -1)	
		colnames(df) <- c("Gene","Transcript","Exon")
	    }else{
		nloop <- (length(input.key)-length(input.key)%%50000)/50000
		flag <- length(input.key)%%50000
		meta_list <- list()
		for(i in 1:nloop){
			start <- (i-1)*50000+1
			end <- i*50000
			stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        	from gene
		       		join transcript on gene.gene_id = transcript.gene_id
		        	join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        	join exon on exon.exon_id = exon_transcript.exon_id
		        	where transcript.stable_id in ('"
			tmp <- paste(input.key[start:end],collapse="','")
			stm <- paste(stm,tmp,"')",sep="")

			m <- dbDriver("MySQL")
			if(is.null(port)){
				con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
			}else{
				con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
			}
	
			res <- dbSendQuery(con, stm)
			meta_list[[i]] <- fetch(res, n = -1)
		}
		if(flag!=0){
			i <- i + 1
			start <- end + 1
			end <- length(input.key)
			stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        	from gene
		       		join transcript on gene.gene_id = transcript.gene_id
		        	join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        	join exon on exon.exon_id = exon_transcript.exon_id
		        	where transcript.stable_id in ('"
			tmp <- paste(input.key[start:end],collapse="','")
			stm <- paste(stm,tmp,"')",sep="")

			m <- dbDriver("MySQL")
			if(is.null(port)){
				con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
			}else{
				con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
			}
			res <- dbSendQuery(con, stm)
			meta_list[[i]] <- fetch(res, n = -1)
		}
		df <- do.call(rbind,meta_list)
		colnames(df) <- c("Gene","Transcript","Exon")
		rm(meta_list)
		gc()
	    }

	}else if(input.type == "Exon"){
		
		if(!large){
		stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        from gene
		        join transcript on gene.gene_id = transcript.gene_id
		        join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        join exon on exon.exon_id = exon_transcript.exon_id
		        where exon.stable_id in ('"
		tmp <- paste(input.key, collapse = "','")
		stm <- paste(stm,tmp,"')",sep = "")

		write.log(cf, "Searching from database ...")
		m <- dbDriver("MySQL")
        	if(is.null(port)){
           	     con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
        	}else{
          	      con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
        	}
	
		res <- dbSendQuery(con, stm)
        	df <- fetch(res, n = -1)	
		colnames(df) <- c("Gene","Transcript","Exon")
	    }else{
		nloop <- (length(input.key)-length(input.key)%%50000)/50000
		flag <- length(input.key)%%50000
		meta_list <- list()
		for(i in 1:nloop){
			start <- (i-1)*50000+1
			end <- i*50000
			stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        	from gene
		       		join transcript on gene.gene_id = transcript.gene_id
		        	join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        	join exon on exon.exon_id = exon_transcript.exon_id
		        	where exon.stable_id in ('"
			tmp <- paste(input.key[start:end],collapse="','")
			stm <- paste(stm,tmp,"')",sep="")

			m <- dbDriver("MySQL")
			if(is.null(port)){
				con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
			}else{
				con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
			}
	
			res <- dbSendQuery(con, stm)
			meta_list[[i]] <- fetch(res, n = -1)
		}
		if(flag!=0){
			i <- i + 1
			start <- end + 1
			end <- length(input.key)
			stm <- "select gene.stable_id, transcript.stable_id, exon.stable_id
		        	from gene
		       		join transcript on gene.gene_id = transcript.gene_id
		        	join exon_transcript on exon_transcript.transcript_id = transcript.transcript_id
		        	join exon on exon.exon_id = exon_transcript.exon_id
		        	where exon.stable_id in ('"
			tmp <- paste(input.key[start:end],collapse="','")
			stm <- paste(stm,tmp,"')",sep="")

			m <- dbDriver("MySQL")
			if(is.null(port)){
				con <- dbConnect(m, username = user, password = "", host = host, dbname = database)
			}else{
				con <- dbConnect(m, username = user, password = passwd, host = host, dbname = database, port=port)
			}
			res <- dbSendQuery(con, stm)
			meta_list[[i]] <- fetch(res, n = -1)
		}
		df <- do.call(rbind,meta_list)
		colnames(df) <- c("Gene","Transcript","Exon")
		rm(meta_list)
		gc()
	    }

	}
	
	target.key <- split.trim(target.type,",")
	out <- df[,target.key]
	out <- unique(out)

	write.log(cf, "Output result ...")
	write.table(out,out.path,sep="\t",row.names=FALSE)
}

main(execute)
