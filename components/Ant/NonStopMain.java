import org.apache.tools.ant.Main;

/**
 * This class is used to disable System.exit() calls of the standard Ant code.
 */
public class NonStopMain extends Main {

	public NonStopMain() {
	}
	
	@Override
	protected void exit(int exitCode) {
       if (exitCode != 0) {
          throw new RuntimeException("Execution failed and returned "+exitCode+'.');
       }
	}
}
