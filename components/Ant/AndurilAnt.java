import java.io.*;
import java.util.*;

import org.apache.tools.ant.launch.Launcher;

import fi.helsinki.ltdk.csbl.anduril.component.*;

public class AndurilAnt extends SkeletonComponent {

	static public final int COUNT_INPUTS  = 10;
	static public final int COUNT_OUTPUTS = 10;

	private String commandFile;

	private void setCommandFilePath(String file) {
		commandFile = file;
	}

	protected ErrorCode runImpl(CommandFile cf) throws IOException {
		String       myName  = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
		String       target  = cf.getParameter("target").trim();
		List<String> antArgs = new ArrayList<String>();
		File         script  = cf.getInput("script");
        File         tmpDir  = cf.getTempDir();

		antArgs.add("-Dtarget.script");
		if (script == null) {
			script = new File(String.format("templates/%s.xml", cf.getParameter("template")));
			if (!script.exists()) {
				cf.writeError("Invalid template while no script is defined: "+script);
				return ErrorCode.PARAMETER_ERROR;
			}
		}
	    antArgs.add(script.getAbsolutePath());
		antArgs.add("-buildfile");     antArgs.add("default_body.xml");
		antArgs.add("-main");          antArgs.add("NonStopMain");
		antArgs.add("-Dcommand.file"); antArgs.add(commandFile);
        antArgs.add("-Dtemp.dir");     antArgs.add(tmpDir.getPath());
		if (!target.isEmpty()) {
			antArgs.add("-Dtarget.target"); antArgs.add(target);
		}

		Launcher.main(antArgs.toArray(new String[antArgs.size()]));

		// Populate unused outputs
		int outputCount = 0;
		for (int o=0; o<COUNT_OUTPUTS; o++) {
			File oPos = cf.getOutput("output"+o);
			if (oPos.exists()) {
				outputCount++;
			} else {
				Tools.writeString(oPos,
                                  "DO NOT USE THIS OUTPUT!\n"+
                                  "This component ("+myName+") does not produce output for output"+o+'.');
			}
		}
		cf.writeLog(outputCount+" output(s) produced by the script.");
		return ErrorCode.OK;
	}

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		AndurilAnt instance = new AndurilAnt();
		instance.setCommandFilePath(argv[0]);
		instance.run(argv);
	}

}
