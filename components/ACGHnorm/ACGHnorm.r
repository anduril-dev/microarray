library(componentSkeleton)

execute <- function(cf) {
    # Global parameters
    method  <- get.parameter(cf, 'method', type='string')
    # Global inputs
    case    <- LogMatrix.read(get.input(cf, 'casechannel'))
    control <- LogMatrix.read(get.input(cf, 'control'))

    if (!(method %in% c('loess','median','mode'))){
        write.error(cf, c('Invalid normalization method:', method,'Must be one of \'loess\', \'median\' or \'mode\'.'))
        return(INVALID_INPUT)
    }

    if (method == "loess"){
       library(limma)
       iterations  <- get.parameter(cf, 'iterations', type='int')
       span        <- get.parameter(cf, 'span',       type='float')
       
       RG   <- list(G = "gMeanSignal", R = "rMeanSignal")
       RG$G <- case
       RG$R <- control

       narrays <- ncol(case)
       for (j in 1:narrays) {
         y <- RG$G[, j]
         x <- RG$R[, j]
         fitted    <- loessFit(y, x, span = span, iterations = iterations)
         RG$G[, j] <- fitted$residuals
         RG$R[, j] <- fitted$fitted
       }

       CSV.write(get.output(cf, 'fit'),            RG$R,    first.cell="ProbeName")
       CSV.write(get.output(cf, 'casechannel'),    RG$G,    first.cell="ProbeName")
       CSV.write(get.output(cf, 'controlchannel'), control, first.cell="ProbeName")
    } else {
        library(CGHcall)
        adj.cellularity <- get.parameter(cf, 'adjustCellularity', type='boolean')
        write.log(cf, 'Cellularity adjustment deprecated... skipping.')
        admixture       <- CSV.read(get.input(cf, 'admixtureMatrix'))
        probeAnnot      <- AnnotationTable.read(get.input(cf, 'probeAnnotation'))

        idCol    <- get.parameter(cf, 'idCol',    type='string')
        if (idCol == "") idCol <- colnames(probeAnnot)[1]
        chrCol   <- get.parameter(cf, 'chrCol',   type='string')
        startCol <- get.parameter(cf, 'startCol', type='string')
        endCol   <- get.parameter(cf, 'endCol',   type='string')
        cols     <- c(idCol, chrCol, startCol, endCol)

        if (!all(probeAnnot[,idCol] == rownames(case))){
            write.error(cf, 'IDs of probeAnnotation and case input do not match.')
            return(INVALID_INPUT)
        }

        cgh.pre  <- case-control
        cgh.pre  <- cbind(probeAnnot[,cols], cgh.pre)
        cgh      <- make_cghRaw(cgh.pre)
        raw.data <- preprocess(cgh, nchrom=length(unique(probeAnnot[,chrCol])))

        result <- normalize(raw.data, method = method)

        CSV.write(get.output(cf, 'casechannel'),    as.matrix(copynumber(result)), first.cell=idCol)
        CSV.write(get.output(cf, 'controlchannel'), control)
        CSV.write(get.output(cf, 'fit'), matrix(NA, 1, 1))
    }
}

main(execute)
