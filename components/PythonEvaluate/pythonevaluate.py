#!/usr/bin/python
import anduril
from anduril.args import *
import sys,os

def createFile(f,content=""):
    fid=open(f,'w')
    fid.write(content)
    fid.close()

if table1:
    table1=anduril.TableReader(table1)
if table2:
    table2=anduril.TableReader(table2)
if table3:
    table3=anduril.TableReader(table3)
if table4:
    table4=anduril.TableReader(table4)
if table5:
    table5=anduril.TableReader(table5)

tableout=anduril.TableWriter(table,[])

optOut1File=optOut1
del optOut1
optOut2File=optOut2
del optOut2
optOut3File=optOut3
del optOut3

createFile(optOut1File)
createFile(optOut2File)
createFile(optOut3File)

if vararray:
    vars().update(vararray)

#A file is given to exec instead of str to get the
#tracebacks to work
if input_script:
    exec(open(input_script,'rt'))
elif param_script:
    exec(param_script)
else:
    print >>sys.stderr, "Both input script and parameter script unspecified. Must specify one."
    sys.exit(constants.GENERIC_ERROR)

try:
    optOut1
    createFile(optOut1File,optOut1)
except:
    pass
try:
    optOut2
    createFile(optOut2File,optOut2)
except:
    pass
try:
    optOut3
    createFile(optOut3File,optOut3)
except:
    pass

