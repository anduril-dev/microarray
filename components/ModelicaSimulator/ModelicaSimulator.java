import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class ModelicaSimulator extends SkeletonComponent {
    public static final String A_BINARY_FILE = "binary.file";
    public static final String A_PARAMS_FILE = "parameters.file";
    public static final String A_PARAMS_FORMAT = "parameters.format";
    public static final String A_RESULTS_FILE = "results.file";
    public static final String A_RESULTS_FORMAT = "results.format";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        File modelDir = cf.getInput("binaryModel");
        
        Properties props = loadConfig(cf, modelDir);
        if (props == null) {
            return ErrorCode.INVALID_INPUT;
        }
        
        File tempDir = createTempDir(cf);
        if (tempDir == null) {
            return ErrorCode.ERROR;
        }
        
        try {
            Map<String, String> params = readParameters(cf);
            if (params == null) return ErrorCode.INVALID_INPUT;
            writeInitFile(cf, props, params, modelDir, tempDir);
            boolean ok = runModel(cf, props, modelDir, tempDir);
            if (!ok) return ErrorCode.ERROR;
            ok = readResults(cf, props, tempDir);
            if (!ok) return ErrorCode.ERROR;
        } finally {
            for (File file: tempDir.listFiles()) {
                file.delete();
            }
            tempDir.delete();
        }
        
        return ErrorCode.OK;
    }
    
    public static File createTempDir(CommandFile cf) {
        final File sysTempDir = new File(System.getProperty("java.io.tmpdir"));
        if (!sysTempDir.exists()) {
            cf.writeError("System temp directory does not exist: "+sysTempDir.getAbsolutePath());
            return null;
        }
        
        final String name = String.format("ModelicaSimulator-%s-%d",
                cf.getMetadata("instanceName"), System.nanoTime());
        File tempDir = new File(sysTempDir, name);
        
        if (tempDir.exists()) {
            cf.writeError("Temporary directory already exists: "+tempDir.getAbsolutePath());
            return null;
        }
        
        boolean ok = tempDir.mkdir();
        if (!ok) {
            cf.writeError("Could not create temporary directory: "+tempDir.getAbsolutePath());
            return null;
        }
        
        return tempDir;
    }
    
    public static void fileCopy(File source, File target) throws IOException {
        String os = System.getProperty("os.name").toLowerCase();
        String copyProgram = os.startsWith("windows") ? "xcopy" : "cp";
        String[] command = new String[] {copyProgram, source.getAbsolutePath(), target.getAbsolutePath()};
        int status = IOTools.launch(command, null, null, null, null);
        if (status != 0) {
            throw new IOException(String.format(
                    "File copy failed: program: %s, status %d, source %s, target %s",
                    copyProgram, status, source.getAbsolutePath(), target.getAbsolutePath()));
        }
    }
    
    private Properties loadConfig(CommandFile cf, File modelDir) throws IOException {
        File propFile = new File(modelDir, "config.ini");
        if (!propFile.exists()) {
            cf.writeError("Configuration file config.ini not found in model directory");
            return null;
        }
        
        Properties props = new Properties();
        BufferedReader reader = new BufferedReader(new FileReader(propFile));
        try {
            props.load(reader);
        } finally {
            reader.close();
        }
        
        final String[] attrs = new String[] {
                A_BINARY_FILE,
                A_PARAMS_FILE, A_PARAMS_FORMAT,
                A_RESULTS_FILE, A_RESULTS_FORMAT};
        for (String attr: attrs) {
            if (!props.containsKey(attr)) {
                cf.writeError("Invalid config.ini: missing attribute "+attr);
                return null;
            }
        }
        
        return props;
    }
    
    private Map<String, String> readParameters(CommandFile cf) throws IOException {
        Map<String, String> params = new HashMap<String, String>();
        if (!cf.inputDefined("parameters")) return params;
        
        CSVParser parser = new CSVParser(cf.getInput("parameters"));
        try {
            if (parser.getColumnCount() < 2) {
                cf.writeError("Parameters file must contain at least two columns");
                return null;
            }
            String paramColumn = cf.getParameter("parameterColumn");
            final int paramIndex = paramColumn.isEmpty() ? 1 : parser.getColumnIndex(paramColumn);
            for (String[] row: parser) {
                params.put(row[0], row[paramIndex]);
            }
        } finally {
            parser.close();
        }
        
        return params;
    }
    
    private boolean writeInitFile(CommandFile cf, Properties props, Map<String, String> parameters,
            File sourceDir, File targetDir) throws IOException {
        String paramsName = props.getProperty(A_PARAMS_FILE);
        File inParams = new File(sourceDir, paramsName);
        File outParams = new File(targetDir, paramsName);
        
        if (!inParams.exists()) {
            cf.writeError("Parameters file does not exist: "+paramsName);
            return false;
        }
        
        final double startTime = cf.getDoubleParameter("startTime");
        final double endTime = cf.getDoubleParameter("endTime");
        if (endTime < startTime) {
            cf.writeError("startTime must be less than endTime");
            return false;
        }
        final double step = (endTime-startTime) / cf.getIntParameter("intervals");
        final double tolerance = cf.getDoubleParameter("tolerance");
        final String method = cf.getParameter("solverMethod");
        
        parameters.put("start value", String.valueOf(startTime));
        parameters.put("stop value", String.valueOf(endTime));
        parameters.put("step value", String.valueOf(step));
        if (tolerance > 0) parameters.put("tolerance", String.valueOf(tolerance));
        if (!method.isEmpty()) parameters.put("method", String.valueOf(method));
        
        BufferedReader reader = new BufferedReader(new FileReader(inParams));
        BufferedWriter writer = new BufferedWriter(new FileWriter(outParams));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split("//", 2);
                if (tokens.length > 1) {
                    String key = tokens[1].trim();
                    String newValue = parameters.get(key);
                    if (newValue != null) line = String.format("%s // %s", newValue, key);
                }
                writer.write(line+"\n");
            }
        } finally {
            reader.close();
            writer.close();
        }
        
        return true;
    }
    
    private boolean runModel(CommandFile cf, Properties props, File sourceDir, File targetDir) throws IOException {
        String binaryName = props.getProperty(A_BINARY_FILE);
        
        File inBinary = new File(sourceDir, binaryName);
        File outBinary = new File(targetDir, binaryName);
        fileCopy(inBinary, outBinary);
        
        final long startTime = System.nanoTime();
        int status = IOTools.launch("./"+binaryName, targetDir, null, "", "");
        final long duration = System.nanoTime() - startTime;
        cf.writeLog(String.format("Simulation took %.2f s", duration/1e9));
        
        if (status != 0) {
            cf.writeError("Simulation failed with error code "+status);
            return false;
        }
        
        return true;
    }
    
    private boolean readResults(CommandFile cf, Properties props, File runningDir) throws IOException {
        String resultsName = props.getProperty(A_RESULTS_FILE);
        File resultsFile = new File(runningDir, resultsName);
        if (!resultsFile.exists()) {
            cf.writeError("Results file does not exist: "+resultsName);
            return false;
        }
        
        final int resultDigits = cf.getIntParameter("digits"); 
        
        String timeDataset = null;
        List<String> datasetNames = new ArrayList<String>();
        SortedMap<Double, Map<String,Double>> values = new TreeMap<Double, Map<String,Double>>();
        
        Pattern variablePattern = Pattern.compile(cf.getParameter("outputVariables"));
        
        BufferedReader reader = new BufferedReader(new FileReader(resultsFile));
        try {
            boolean isFirstDataset = true;
            String curDataset = null;
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("#")) continue;
                line = line.trim();
                if (line.isEmpty()) continue;
                
                if (line.startsWith("DataSet:")) {
                    String dataset = line.split(":", 2)[1].trim();
                    if (isFirstDataset || variablePattern.matcher(dataset).matches()) {
                        if (isFirstDataset) {
                            timeDataset = dataset;
                            isFirstDataset = false;
                        }
                        curDataset = dataset;
                        datasetNames.add(dataset);
                    } else {
                        curDataset = null;
                    }
                } else if (curDataset != null && Character.isDigit(line.charAt(0))) {
                    String[] tokens = line.split(",", 2);
                    double time = Double.parseDouble(tokens[0].trim());

                    if (resultDigits >= 0) {
                        final double roundScale = Math.pow(10, resultDigits);
                        time = Math.round(time * roundScale) / roundScale;
                    }
                    
                    double value = Double.parseDouble(tokens[1].trim());
                    Map<String, Double> valueMap = values.get(time);
                    if (valueMap == null) {
                        valueMap = new HashMap<String, Double>();
                        values.put(time, valueMap);
                    }
                    valueMap.put(curDataset, value);
                    if (timeDataset != null && !curDataset.equals(timeDataset)) {
                        valueMap.put(timeDataset, time);
                    }
                } else {
                    // Ignore line
                }
            }
        } finally {
            reader.close();
        }
        
        CSVWriter writer = new CSVWriter(datasetNames.toArray(new String[] {}), cf.getOutput("timepoints"));
        try {
            for (Map.Entry<Double, Map<String,Double>> entry: values.entrySet()) {
                for (String dataset: datasetNames) {
                    Double value = entry.getValue().get(dataset);
                    if (value == null) writer.write(null);
                    else writer.write(value.doubleValue());
                }
            }
        } finally {
            writer.close();
        }
        
        return true;
    }
    
    public static void main(String[] args) {
        new ModelicaSimulator().run(args);
    }
}
