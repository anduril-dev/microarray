library(componentSkeleton)
#require(Design, warn.conflicts=FALSE)
require(rms, warn.conflicts=FALSE)

execute <- function(cf) {
  # Initialization...
  line.colors   <- c("blue","green","red","violet","black","yellow","brown","gray","pink")
  width.inches  <- 15 / 2.54
  height.inches <- 11 / 2.54
  minFreq       <- get.parameter(cf, 'minFreq',       type='float')
  minCount      <- get.parameter(cf, 'minCount',      type='int')
  strictLimits  <- get.parameter(cf, 'strictLimits',  type='boolean')
  p.limit       <- get.parameter(cf, 'PLimit',        type='float')
  conf.int      <- get.parameter(cf, 'confInt',       type='float')
  timeOutLimit  <- get.parameter(cf, 'timeOutLimit',  type='float')
  survRatePoint <- get.parameter(cf, 'survRatePoint', type='float')
  skipMissingGT <- get.parameter(cf, 'skipMissingGT', type='boolean')
  outputAllP    <- get.parameter(cf, 'outputAllPValues', type='boolean')
  showCI        <- get.parameter(cf, 'showCI',        type='boolean')
  cox           <- get.parameter(cf, 'useCox',        type='boolean')
  combine.com   <- get.parameter(cf, 'combineCommonHom', type='boolean')
  nullCalls     <- get.parameter(cf, 'nullCalls')
  modelVars     <- get.parameter(cf, 'modelVars',     type='string')
  censor.type   <- get.parameter(cf, 'censoring',     type='string')
  sTitle        <- get.parameter(cf, 'title')
  instanceName  <- get.metadata(cf, 'instanceName')
  out.dir       <- get.output(cf, 'report')
  stat.out      <- get.output(cf, 'statistics')
  docName       <- file.path(out.dir, LATEX.DOCUMENT.FILE)

  nullCalls <- unlist(strsplit(nullCalls,","))
  modelVars <- unlist(strsplit(modelVars,","))

  dir.create(out.dir, recursive=TRUE)

  if (input.defined(cf, 'annotations')) {
     markerInfo     <- AnnotationTable.read(get.input(cf, 'annotations'))
     markerInfo[,1] <- gsub('-','.',markerInfo[,1],fixed=TRUE)
     info.cols      <- 2:ncol(markerInfo)
  } else {
     markerInfo <- NULL
  }

  # Read survival times...
  write.log(cf, "Reading survival times...")
  colTime   <- get.parameter(cf, 'timeCol')
  colTime2  <- get.parameter(cf, 'timeCol2')
  colStatus <- get.parameter(cf, 'statusCol')
  colSample <- get.parameter(cf, 'sampleCol')
  colStart  <- get.parameter(cf, 'timeStartCol')
  survData  <- CSV.read(get.input(cf, 'survival'))

  if (colTime2 != "" && !cox){
    write.log(cf, "Left-truncation only enabled when useCox=true. Ignoring timeCol2.")
  }

  if (!(all(c(colStatus,colTime,colSample) %in% colnames(survData)))) {
    write.log(cf, "Defined columns not found in survival input. Exiting.")
    error.msg <- paste("Expected: ", paste(colStatus,colTime,colSample,colStart, collapse=","),
                       " Got: ",     paste(colnames(survData), collapse=","), sep=" ")
    write.log(cf, error.msg)
    return(INVALID_INPUT)
  }

  survData  <- survData[!is.na(survData[,colStatus]),]
  write.log(cf, sprintf("Survival data were available for %i samples.",nrow(survData)))

  # Drop useless columns
  rownames(survData) <- survData[,colSample]
  if (nchar(colStart) > 0) {
     survData[,colTime] <- survData[,colTime]-survData[,colStart]
  }
  if (length(modelVars) == 1 && colTime2 == "") {
      survData           <- survData[,c(colTime, colStatus, colSample)]
      colnames(survData) <- c('time', 'status', 'genotype')
  } else if (length(modelVars) > 1 && colTime2 == "") {
      survData           <- survData[,c(colTime, colStatus, colSample, modelVars[2:length(modelVars)])]
      colnames(survData) <- c('time', 'status', 'genotype', modelVars[2:length(modelVars)])
  } else if (length(modelVars) == 1 && cox && colTime2 != "") {
      survData           <- survData[,c(colTime, colTime2, colStatus, colSample)]
      colnames(survData) <- c('time', 'time2', 'status', 'genotype')
  }else if (length(modelVars) > 1 && cox && colTime2 != "") {
      survData           <- survData[,c(colTime, colTime2, colStatus, colSample, modelVars[2:length(modelVars)])]
      colnames(survData) <- c('time', 'time2', 'status', 'genotype', modelVars[2:length(modelVars)])
  }

  if (any(is.na(survData[,'time']))) {
    write.log(cf, "Survival times can only contain non-missing values. Exiting.")
    return(INVALID_INPUT)
  }

  # Cut study period after the time out limit i.e., right-censoring
  if (timeOutLimit >= 0 && nrow(survData) > 0) {
     longTimes                     <- (survData[, 'time'] > timeOutLimit)
     survData[longTimes, 'time']   <- timeOutLimit
     survData[longTimes, 'status'] <- 0
  } else {
     timeOutLimit <- max(survData[,'time'])
  }

  # Read genotypes...
  write.log(cf, "Reading genotypes...")
  genotypes <- read.table(file             = get.input(cf, 'genotypes'),
                          header           = TRUE,
                          row.names        = 1,
                          sep              = "\t",
                          comment.char     = "",
                          quote            = "",
                          stringsAsFactors = TRUE)
  write.log(cf, sprintf("Genotypes were available for %i samples.",nrow(genotypes)))
  # Make sure that the samples are in the same order as in survival matrix.
  g.match   <- match(rownames(survData), rownames(genotypes), nomatch=0)
  g.missing <- (g.match == 0)
  if (any(g.missing) > 0) {
     write.log(cf, paste("No genotypes available for: ",rownames(survData)[g.missing], sep=""))
     if (!skipMissingGT)
        return(INVALID_INPUT)
     survData <- survData[!g.missing,]
  }
  genotypes <- genotypes[g.match,]
  if (colTime2 == ""){
    surv.obj <- paste("Surv(time, status, type='", censor.type, "')", sep="")
  } else if (colTime2 != "" && cox){
    surv.obj <- paste("Surv(time, time+time2, status, type='", censor.type, "')", sep="")
  }
  surv.frm.pre  <- paste(surv.obj, "~", modelVars[1], sep=" ")
  surv.frm <- as.formula(surv.frm.pre)
  if (cox){
    if (length(modelVars) > 1){
        for (model in 2:length(modelVars)){
          surv.frm.pre <- paste(surv.frm.pre, modelVars[model], sep=" + ")
        }
    }
    surv.frm.cox <- as.formula(surv.frm.pre)
  }
  rm(g.match, g.missing)
  na.limit <- nrow(survData)*get.parameter(cf, 'naLimit', 'float')
  write.log(cf, sprintf("Expects less than %.3g missing values for each marker.", na.limit))

  # Start document and the report
  cat(sprintf("\\section{%s}\\label{sec:%s}", sTitle, instanceName),
      sep="\n", file=docName)
  cat(c("marker", "pvalue", "heteroOR", "homoOR", "survDiff","coxGtype1", "coxGtype2",
        "coxGtype1HazR","coxGtype2HazR","coxVarHazR","coxGtype1P","coxGtype2P","coxLogtestP","coxWaldtestP","coxVarP",
        "phP","nonPropVars","plot"),
      sep="\t", file=stat.out)

  write.log(cf, "Calculating survival comparisons...")
  mNames <- colnames(genotypes)
  for (mi in 1:length(mNames)) {
      if (nrow(survData) < 1) # Don't process empty survival times
        break
      marker = mNames[mi]
      # Calculate genotype frequencies
      g.types <- genotypes[,mi]
      if (!is.factor(g.types))
         g.types <- as.factor(g.types)
      for (nullCall in nullCalls) {
          levs <- levels(g.types)
          if (any(cHits <- (levs == nullCall))) {
             levs[cHits]     <- NA
             levels(g.types) <- levs
          }
      }
      if (nlevels(g.types) <  2) {
         write.log(cf, paste("No variation in", marker))
         next
      }
      g.calls  <- levels(g.types)
      if (nlevels(g.types) == 3) {
        if (combine.com){
            g.calls  <- levels(g.types)
            het.call <- sort(g.calls)[2]
            g.stat   <- sort(summary(g.types, na.rm=TRUE), decreasing = TRUE)
            g.stat   <- g.stat[names(g.stat) != "NA's"] # Drop missing value counts
            gn.calls <- names(g.stat)
            g.sum    <- sum(g.stat) # Relative statistics
            if (gn.calls[1] != het.call) {
               g.calls[g.calls == gn.calls[1]] <- het.call
               levels(g.types) <- g.calls
               g.calls         <- levels(g.types)
               g.calls[g.calls == het.call]    <- paste(het.call,gn.calls[1], sep=",")
               levels(g.types) <- g.calls
            } else { # If heterozygote is the most common genotype
               g.calls[g.calls == gn.calls[2]] <- het.call
               levels(g.types) <- g.calls
               g.calls         <- levels(g.types)
               g.calls[g.calls == het.call]    <- paste(het.call,gn.calls[2], sep=",")
               levels(g.types) <- g.calls
            }
        } else {
            g.calls  <- levels(g.types)
            het.call <- sort(g.calls)[2]
            g.stat   <- sort(summary(g.types, na.rm=TRUE))
            g.stat   <- g.stat[names(g.stat) != "NA's"] # Drop missing value counts
            gn.calls <- names(g.stat)
            g.sum    <- sum(g.stat) # Relative statistics
            # Possible merging of heterozygotes and rare homozygotes
            if (((g.stat[1] < minCount) || (g.stat[1]/g.sum[1] < minFreq)) &&
                (gn.calls[1] != het.call)) {
               g.calls[g.calls == gn.calls[1]] <- het.call
               levels(g.types) <- g.calls
               g.calls         <- levels(g.types)
               g.calls[g.calls == het.call]    <- paste(het.call,gn.calls[1], sep=",")
               levels(g.types) <- g.calls
            }
            if (strictLimits){
                if ((g.stat[1]+g.stat[2] < minCount) || ((g.stat[1]+g.stat[2])/g.sum[1] < minFreq)) next
            }
         }
      }

      # Statistical comparison of survival between the sample groups
      survData[,'genotype'] <- g.types
      if (cox) {
          survDataCox <- survData
          cox.test  <- coxph(surv.frm.cox, data=survDataCox)
          zph <- cox.zph(coxph(surv.frm.cox, data = survDataCox))$table
          cox.fit   <- survfit(cox.test, conf.int=conf.int)
          coef.cox  <- summary(cox.test)
          if (length(g.calls) < 3){
              cox.res  <- c(gsub('genotype','',rownames(coef.cox$coefficients)[1:(length(g.calls)-1)]), NA,
                            as.numeric(coef.cox$coefficients[1:(length(g.calls)-1),2]), NA,
                            as.numeric(coef.cox$coefficients[1:(length(g.calls)-1),5]), NA,
                            as.numeric(coef.cox$logtest['pvalue']), as.numeric(coef.cox$waldtest['pvalue']))

          } else {
              cox.res  <- c(gsub('genotype','',rownames(coef.cox$coefficients)[1:(length(g.calls)-1)]),
                            as.numeric(coef.cox$coefficients[1:(length(g.calls)-1),2]),
                            as.numeric(coef.cox$coefficients[1:(length(g.calls)-1),5]),
                            as.numeric(coef.cox$logtest['pvalue']), as.numeric(coef.cox$waldtest['pvalue']))
          }
          nonPropVars <- paste(rownames(zph)[which(zph[,'p'] <= 0.05)], collapse=",")
          if (nchar(nonPropVars) < 1) nonPropVars <- NA
          zph.res <- c(min(zph[rownames(coef.cox$coefficients)[1:(length(g.calls)-1)],'p'], na.rm=TRUE), nonPropVars)
          if (length(modelVars) == 1){
            var.res <- c(NA,NA)
          } else { # Only take the first model, should be updated
            var.res  <- c(coef.cox$coefficients[length(g.calls),2],
                          coef.cox$coefficients[length(g.calls),5])
          }
          p.cutoff <- min(coef.cox$coefficients[1:length(modelVars),5], na.rm=TRUE)
      } else {
          cox.res <- rep(NA, 8)
          var.res <- rep(NA, 2)
          zph.res <- rep(NA, 2)
      }
      if (colTime2 == ""){
        s.test   <- survdiff(surv.frm, data=survData)
        p.value  <- 1-pchisq(s.test$chisq, length(s.test$n)-1)
      } else {
        p.value <- NA
      }
      fit <- survfit(surv.frm, data=survData, conf.int=conf.int)
      g.strata <- fit$strata
      #g.calls  <- names(g.strata)

      if (!cox) p.cutoff <- p.value

      if (!outputAllP && p.cutoff > p.limit) # If not interesting enough
          next

      if (naL <- length(fit$na.action)) {
         if (naL > na.limit) {
            write.log(cf, sprintf("%i missing genotypes in %s.", naL, marker))
            next
         }
         missMsg <- paste("Total of ",naprint(fit$na.action),".",sep="")
      } else {
         missMsg <- ""
      }

      # Logistic regression
      hetLR <- NA; homLR <- NA
      if (length(g.calls) < 4) {
         lrmReport <- sprintf("Odds ratios have been estimated using logistic regression at $t\\approx%.4g$: \\\\\n",
                              timeOutLimit)
         if (length(g.calls) == 3) {
            if (g.strata[1] < g.strata[3]) {
               rareHz <- 1; wtHz <- 3; colOrd <- 3:1
            } else {
               rareHz <- 3; wtHz <- 1; colOrd <- 1:3
            }
            lrV1      <- logistic.regression(survData, g.calls[c(wtHz,2)])
            lrV2      <- logistic.regression(survData, g.calls[c(wtHz,rareHz)])
            lrmReport <- paste(lrmReport, lrV1[[1]], " and \n", lrV2[[1]], ".\n", sep="")
            hetLR     <- lrV1[[2]]
            homLR     <- lrV2[[2]]
         } else {
            if (nchar(g.calls[1]) > 2) {
               colOrd <- 2:1
            } else {
               nucs <- unlist(strsplit(g.calls[1], ""))
               if (nucs[1] == nucs[2]) { colOrd <- 1:2 } else { colOrd <- 2:1 }
            }
            lrV1      <- logistic.regression(survData, g.calls[colOrd] )
            lrmReport <- paste(lrmReport, lrV1[[1]], ".", sep="")
            hetLR     <- lrV1[[2]]
         }
      } else {
         lrmReport <- ""
         colOrd    <- 1:length(g.strata)
      }

      # Survival difference
      if (survRatePoint >= 0) {
         s.dist <- curv.distance(fit, survRatePoint)
      } else {
         s.dist <- c(NA, NA)
      }

      # Print result statistics
      markerNC <- gsub('_','-',  marker,fixed=TRUE)
      markerNC <- gsub('.', '',markerNC,fixed=TRUE)
      fig.name <- sprintf('%s-%s.pdf', instanceName, markerNC)
      if ((outputAllP && p.cutoff > p.limit) || (cox && zph.res[1] < 0.05)) fig.name <- 'NA' # Don't produce images

      cat(sprintf("\n%s\t%g\t%g\t%g\t%g\t%s\t%s\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%s\t%s",
                  marker, p.value, hetLR, homLR, s.dist[2]-s.dist[1], 
                  cox.res[1], cox.res[2], as.numeric(cox.res[3]), as.numeric(cox.res[4]), var.res[1],
                  as.numeric(cox.res[5]), as.numeric(cox.res[6]), as.numeric(cox.res[7]), as.numeric(cox.res[8]), var.res[2],
                  as.numeric(zph.res[1]), zph.res[2], fig.name),
          file=stat.out, append=TRUE)

      if (outputAllP && p.cutoff > p.limit) # If not interesting enough
          next

      # Prepare documentation...
      fig.path  <- file.path(out.dir, fig.name)
      rep.table <- c()
      for (curv in colOrd) {
          this.fit  <- fit[curv]
          n         <- this.fit$n.risk[1]
          n.ev      <- sum(this.fit$n.event)
          n.ce      <- n-n.ev
          rep.table <- c(rep.table, paste(
                         g.calls[curv],
                         n,
                         sprintf("%i (%.3g)",    n.ev,(n.ev/n)),
                         sprintf("%i (%.3g)\\\\",n.ce,(n.ce/n)),
                         sep=" & "))
      }
      hitAnnot <- markerInfo[markerInfo[,1]==marker,]
      if (is.null(hitAnnot) || (nrow(hitAnnot) < 1)) {
         hitAnnot <- ''
      } else {
         hitAnnot <- hitAnnot[,info.cols]
         hitAnnot <- hitAnnot[,!is.na(hitAnnot)]
         hitAnnot <- c('\n\n\\begin{center}Additional annotations:\\\\\n\\tiny{\\begin{tabular}{',rep('c',ncol(hitAnnot)),'}\n',
                       '\\textit{', paste(latex.quote(names(hitAnnot)), collapse="\\/} & \\textit{"), '\\/}\\\\\n',
                       paste(latex.quote(as.character(hitAnnot)), collapse=" & "),
                       '\\\\\n\\end{tabular}}\\end{center}\n')
         hitAnnot <- paste(hitAnnot,collapse="",sep="")
      }

      # Create graph...
      g.legend <- paste(g.calls, paste('(',fit$n,')',sep=''))
      pdf(fig.path, paper='special', width=width.inches, height=height.inches)
      par(pch=20, cex=0.7, las=2)
      plot(fit,
           col  = line.colors[colOrd],
           main = marker,
           xlab = "survival time")
      legend(x=timeOutLimit/30.0, y=0.05+0.06*length(g.legend), legend=g.legend, col=line.colors[colOrd], lty=1)
      fig.capt = "Kaplan-Meier curves for the selected genotypes."
      if (showCI && !cox) {
         lines(fit, col=line.colors[colOrd], lty=3, conf.int='only')
         fig.capt <- paste(fig.capt,
                           sprintf("Dashed lines represent %g confidence intervals for", conf.int),
                           "the curves with the corresponding color.")
      } else if (showCI && cox){
         lines(cox.fit, col="black", lty=3, conf.int='only')
         fig.capt <- paste(fig.capt,
                           sprintf("Dashed lines represent %g confidence intervals for", conf.int),
                           "cox regression model.")
      }
      if (survRatePoint >= 0) {
         abline(h=survRatePoint, col="grey", lty=2)
         if (!is.na(s.dist[2])) {
            lines(s.dist, c(survRatePoint, survRatePoint), col="black")
            fig.capt <- paste(fig.capt,
                              sprintf("Black horizontal line ($%.2f-%.2f=%.2f$) indicates the maximal survival difference at $y=%.2f$.",
                                      s.dist[2], s.dist[1], s.dist[2]-s.dist[1], survRatePoint))
         }
      }
      invisible(dev.off())

      markerLatex <- latex.quote(marker)
      cat(sprintf("\\subsection{Survival analysis of %s}\\label{sec:%s-%s}",
                  markerLatex, instanceName, markerNC),
          sprintf("This marker has a genotype specific effect with a p-value of %e.",p.value),
          missMsg,
          lrmReport,
          "{\\begin{table}[!h]\\caption{Summary of genotype frequencies with their proportions in parenthesis.}",
          sprintf("\\label{tab:%s-%s}", instanceName, markerNC),
          "\\begin{center}\\begin{tabular}{rrrr}",
          "\\hline\\textbf{genotype}&\\textbf{total N}&\\textbf{events}&\\textbf{censored}\\\\\\hline",
          rep.table,
          "\\hline\\end{tabular}\\end{center}\\end{table}}",
          latex.figure(fig.name, caption = fig.capt, quote.capt = FALSE),
          hitAnnot,
          "\\clearpage",
          sep="\n", file=docName, append=TRUE)
  }

  return(0)
}

logistic.regression <- function(survData, g.calls) {
  avail.set <- !is.na(survData[,'genotype'])
  survData[avail.set & survData[,'genotype']==g.calls[1],'gNumeric'] <- 1
  survData[avail.set & survData[,'genotype']==g.calls[2],'gNumeric'] <- 2
  lfit <- try(lrm(status ~ gNumeric, data=survData))
  retV <-list(2)
  if (class(lfit)[1] == 'try-error') {
    retV[[1]] <- paste("\\textit{",latex.quote(lfit[1]),"}",sep="")
    retV[[2]] <- NA
  } else {
    ors       <- exp(lfit$coef[2])
    retV[[1]] <- paste("$\\frac{P(event{\\mid}", g.calls[2], ")/P({\\neg}event{\\mid}", g.calls[2],
                       ")}{P(event{\\mid}", g.calls[1], ")/P({\\neg}event{\\mid}", g.calls[1],
                       ")} \\approx$ ", round(ors, 3),
                       sep="")
    retV[[2]] <- ors
  }
  retV
}

curv.distance <- function(fit, survRatioLimit) {
  v.max <- 0
  v.min <- fit$time[0]
  for (c in 1:length(fit$strata)) {
      accepts <- fit[c]$surv < survRatioLimit
      curv    <- fit[c]$time[accepts]
      v.max   <- max(v.max, curv[1])
      v.min   <- min(v.min, curv[1])
  }
  c(v.min, v.max)
}

main(execute)
