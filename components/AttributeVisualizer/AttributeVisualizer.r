library(cluster)
library(componentSkeleton)

execute <- function(cf) {
    annotation <- CSV.read(get.input(cf, 'annotation'))
    width.cm <- get.parameter(cf, 'width', 'float')
    
    id.column <- get.parameter(cf, 'idColumn')
    if (nchar(id.column) == 0) id.column <- colnames(annotation)[1]
    
    ann.columns <- split.trim(get.parameter(cf, 'annotationColumns'), ',')
    if (identical(ann.columns, '*')) ann.columns <- setdiff(colnames(annotation), id.column)
    
    ann.matrix <- annotation[,ann.columns]
    rownames(ann.matrix) <- annotation[,id.column]

    if (input.defined(cf, 'matrix')) {
        matr <- Matrix.read(get.input(cf, 'matrix'))
        ann.matrix <- ann.matrix[colnames(matr),] # Remove samples that are not in numeric matrix
        clusters <- agnes(t(matr))
        dend <- as.dendrogram(as.hclust(clusters))
    } else {
        dend <- NA
    }
    
    out.dir <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)
    
    # Colors: green (low) -> black (middle) -> red (high)
    COLOR.STEP <- 1/32
    color <- c(hsv(h=2/6, v=seq(1, 0, -COLOR.STEP)), hsv(h=0, v=seq(COLOR.STEP, 1, COLOR.STEP)))
    
    legend.file <- sprintf('%s-legend.pdf', get.metadata(cf, 'instanceName'))
    legend.width <- width.cm / 2.54
    legend.height <- max(0.7*ncol(ann.matrix), 2)
    
    pdf(file.path(out.dir, legend.file), width=legend.width, height=legend.height)
    plot.new()
    par(mar=c(0.3, 0.3, 0.3, 0.3))
    par(usr=c(0, legend.width, 0, legend.height))
    
    for (i in 1:ncol(ann.matrix)) {
        y <- (0.95 - ((i-1)/(ncol(ann.matrix)-1))*0.8) * legend.height
        text(0, y, colnames(ann.matrix)[i], adj=c(0, 1))
        x <- 0.3 * legend.width
        
        if (!is.numeric(ann.matrix[,i])) {
            f <- factor(ann.matrix[,i])
            ann.matrix[,i] <- as.integer(unclass(f))
            draw.legend.labels(x, y, legend.width, legend.width*0.02, as.character(levels(f)), color)
        } else {
            draw.color.bar(x, y, ann.matrix[,i], legend.width*0.02, legend.width*0.2, color)
        }
    }
    dev.off()
    ann.matrix <- as.matrix(ann.matrix)
    
    section.title <- get.parameter(cf, 'sectionTitle')
    section.type  <- get.parameter(cf, 'sectionType')
    
    row.margin <- get.parameter(cf, 'rowMargin', 'float')
    col.margin <- get.parameter(cf, 'columnMargin', 'float')
    plot.file <- sprintf('%s-figure.pdf', get.metadata(cf, 'instanceName'))
    pdf(file.path(out.dir, plot.file))
    result <- heatmap(as.matrix(ann.matrix), Rowv=dend, Colv=NA, scale='column', col=color,
        margin=c(row.margin, col.margin))
    dev.off()

    if (nchar(section.type) > 0) {
        tex <- sprintf('\\%s{%s}', section.type, section.title)
    } else {
        tex <- character()
    }
    
    heatmap.label <- sprintf('fig:%s:heatmap', get.metadata(cf, 'instanceName'))
    legend.label <- sprintf('fig:%s:legend', get.metadata(cf, 'instanceName'))
    
    heatmap.caption <- paste(
        'Clustering dendrogram (left) and attribute visualization (heat map).',
        'Each column in the heat map corresponds to one attribute.',
        sprintf('See Figure \\ref{%s} for color coding.', legend.label))
    legend.caption <- sprintf('Attribute legend for the heat map (Figure \\ref{%s}).', heatmap.label)
    tex <- c(tex,
        latex.figure(plot.file, caption=heatmap.caption, image.width=width.cm,
            fig.label=heatmap.label, quote.capt=FALSE),
        latex.figure(legend.file, caption=legend.caption, image.width=width.cm,
            fig.label=legend.label, quote.capt=FALSE))

    if (!is.null(result$rowInd) && nrow(ann.matrix) < 20) {
        tex <- c(tex, paste('% Row order:', paste(rownames(ann.matrix)[result$rowInd], collapse=' ')))
    }
            
    latex.write.main(cf, 'report', tex)
    return(0)
}

draw.color.bar <- function(x, y, values, bar.height, bar.width, color) {
    n <- length(color)
    for (i in 1:length(color)) {
        rect(x+(i/n-1/n)*bar.width, y-0.5*bar.height, x+(i/n)*bar.width*1.005, y+0.5*bar.height, col=color[i], border=NA)
    }
    rect(x, y-0.5*bar.height, x+bar.width, y+0.5*bar.height, col=NA, border=rgb(0, 0, 0))
    
    label.y <- y - bar.height*1.3
    low.label <- sprintf('%.1f', min(values, na.rm=TRUE))
    high.label <- sprintf('%.1f', max(values, na.rm=TRUE))
    text(x, label.y, low.label)
    text(x+bar.width, label.y, high.label)
}

draw.legend.labels <- function(x, y, max.x, box.size, box.labels, color) {
    item.width <- (max.x - x) / length(box.labels)
    for (i in 1:length(box.labels)) {
        box.x <- x + (i-1)*item.width
        # Scale 1 .. length(box.labels) to 1 .. length(color)
        index <- 1 + (i-1)*((length(color)-1) / (length(box.labels)-1))
        rect(box.x, y-0.5*box.size, box.x+box.size, y+0.5*box.size, col=color[index])
        text(box.x+box.size*0.5, y-box.size*1.3, box.labels[i])
    }
}

main(execute)
