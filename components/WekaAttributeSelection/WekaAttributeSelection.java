import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.core.Instances;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.javatools.weka.WekaDataUtils;

/**
 * A wrapper for Weka filters for the CSBL pipeline.
 * 
 * @author Sirkku Karinen
 * @version 0.1
 */
public class WekaAttributeSelection extends SkeletonComponent {

	public static final String CLASS =	"class";
    public static final String AS_CLASS =	"attributeSelection";
    public static final String AS_EVAL =	"attributeEvaluation";
    public static final String SELECTION_PARAMETERS =	"selectionParameters";
    public static final String EVALUATION_PARAMETERS =	"evaluationParameters";
    public static final String REMOVE_COLS =	"removeCols";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
    	
    	String searchClass = cf.getParameter(CLASS);
    	String asClass = cf.getParameter(AS_CLASS);
    	String asEvalClass = cf.getParameter(AS_EVAL);
    	String removeCols = cf.getParameter(REMOVE_COLS);

    	List<String> asPars = new LinkedList<String>();

        Scanner scan = new Scanner(cf.getParameter(SELECTION_PARAMETERS));
        while (scan.hasNext()) {
        	asPars.add(scan.next());
        }
        scan.close();

        List<String> asEvalPars = new LinkedList<String>();

        scan = new Scanner(cf.getParameter(EVALUATION_PARAMETERS));
        while (scan.hasNext()) {
        	asEvalPars.add(scan.next());
        }
        scan.close();
        
        // Load input
        Instances data = WekaDataUtils.readCSV(cf.getInput("data"), searchClass);
        
        // Initialize filter
        ASSearch asSer;
        ASEvaluation asEval;
        try {
            asSer = ASSearch.forName(asClass, asPars.toArray(new String[asPars.size()]));
        } catch (ClassNotFoundException e) {
            cf.writeError("Unsupported ASSearch class " + asClass + ".");
            return ErrorCode.PARAMETER_ERROR;
        }
        
        try {
            asEval = ASEvaluation.forName(asEvalClass, asEvalPars.toArray(new String[asEvalPars.size()]));
        } catch (ClassNotFoundException e) {
            cf.writeError("Unsupported ASEvaluation class " + asEvalClass + ".");
            return ErrorCode.PARAMETER_ERROR;
        }
       
        //remove some attributes
        String [] rCols = removeCols.split(",");
        data = WekaDataUtils.removeColumns(data, rCols);
                
        
        asEval.buildEvaluator(data);
        int[] selAttrs = asSer.search(asEval, data);
               
        //add also the class to results
        String[] cols = new String[selAttrs.length+1];
        cols[0] = data.attribute(searchClass).name();
        
        for (int i = 1; i <= selAttrs.length; i++)
            cols[i] = data.attribute(selAttrs[i-1]).name();

        CSVWriter out = new CSVWriter(cols, cf.getOutput("selectedData"));

        for (int i = 0; i < data.numInstances(); i++) {
        	//class first
            if (data.attribute(searchClass).isNumeric())
                out.write(data.instance(i).value(data.attribute(searchClass)));
            else
                out.write(data.attribute(searchClass).value(
                        (int) data.instance(i).value(data.attribute(searchClass))));
            
            for (int j : selAttrs) {
                if (data.attribute(j).isNumeric())
                    out.write(data.instance(i).value(j));
                else
                    out.write(data.attribute(j).value(
                            (int) data.instance(i).value(j)));
            }
        }
        out.close();

        return ErrorCode.OK;
    }

    /**
     * @param argv Pipeline arguments
     */
    public static void main(String[] argv) {
        new WekaAttributeSelection().run(argv);
    }

}
