# SigPathway.r
# Pipeline wrapper for sigPathway algorithm.
# Requires sigPathway package for bioconductor.
# Version 0.6.2
# Author Viljami Aittomaki

library(componentSkeleton)
library(sigPathway)

execute <- function(cf) {

    # Read expression input
    expr.path = get.input(cf, 'expr')
    # Check for correct input ID's
    idtype = CSV.read.first.cell(expr.path)
    if(idtype != 'EntrezGene') {
        errormsg = paste("Incorrect input ID type '", idtype, "'. Only EntrezGene id's are supported by SigPathway. Use Korvasieni component to convert.", sep='')
        write.error(cf, errormsg)
        return(INVALID_INPUT)
    }
    # Expression values must be in linear scale for SigPathway
    expr.mat = 2^LogMatrix.read(expr.path)

    # Read samplegroup input
    samplegroup.path = get.input(cf, 'samplegroup')
    samplegroup = CSV.read(samplegroup.path)
    samplegroup$Type = samplegroup$Description = NULL

    # Generate phenotype vector from samplegroup
    nsamples             = ncol(expr.mat)
    phenotype            = rep(NA,nsamples)
    phenotypeList        = strsplit(samplegroup$Members, ',')
    names(phenotypeList) = samplegroup$ID
    for(i1 in 1:nsamples) {
        for(i2 in 1:length(phenotypeList)) {
            if(colnames(expr.mat)[i1] %in% phenotypeList[[i2]])
                phenotype[i1] = names(phenotypeList)[i2]
        }
    }
    # Check for samples with no phenotype
    samplesLeftOut = is.na(phenotype)
    if(any(samplesLeftOut)) {
        write.log(cf, "Warning: Some samples don't have a group assigned!")
        write.log(cf, "Following samples were left out:")
        write.log(cf, cat(colnames(expr.mat)[samplesLeftOut]))
        expr.mat = expr.mat[,!samplesLeftOut]
        phenotype = phenotype[!samplesLeftOut]
    }
    
    # Get parameters
    allpathways         = get.parameter(cf, 'allpathways', type='boolean')
    alwaysUseRandomPerm = get.parameter(cf, 'alwaysRandPerm', type='boolean')
    maxNPS              = as.numeric(get.parameter(cf, 'maxNPS', type='int'))
    minNPS              = as.numeric(get.parameter(cf, 'minNPS', type='int'))
    npath               = as.numeric(get.parameter(cf, 'npath', type='int'))
    nsim                = as.numeric(get.parameter(cf, 'nsim', type='int'))
    randomseed          = as.numeric(get.parameter(cf, 'seed', type='int'))
    weightType          = get.parameter(cf, 'weightType', type='string')
    if(!(weightType == 'constant' || weightType == 'variable')) {
        write.error(cf, "Unknown weightType. Must be constant or variable.")
        return(PARAMETER_ERROR)
    }

    # Load pathway annotations
    write.log(cf, "Loading local pathway annotation file.")
    load('Genesets_EntrezGeneIDs.RData')
    
    # Handle genes with multiple id's
    # Remove multiple id's and only use first one
    multiple.ids = grep(',', rownames(expr.mat))
    for (i in multiple.ids) {
        rownames(expr.mat)[i] = unlist(strsplit(rownames(expr.mat)[i], ','))[1]
    }

    # Set seed and run SigPathway analysis
    write.log(cf, "Running SigPathway analysis.")
    if(randomseed != 0)
        set.seed(randomseed)

    results = runSigPathway(G.EGIDs, minNPS, maxNPS, expr.mat, phenotype, nsim = nsim,
        weightType = weightType, ngroups = length(unique(phenotype)), npath = npath, verbose = FALSE,
        allpathways = allpathways, annotpkg = NULL, alwaysUseRandomPerm = alwaysUseRandomPerm)
    
    results.frame = results$df.pathways
    results.frame$IndexG = NULL
    #Add a column with all the probes in each pathway
    results.frame$Probes = sapply(1:length(results$list.gPS),
        function(x) {paste(results$list.gPS[[x]]$Probes,collapse=',')})
    
    #Write output
    results.path = get.output(cf, 'pathways')
    CSV.write(results.path, results.frame)
    
    return(0)
}

main(execute)
