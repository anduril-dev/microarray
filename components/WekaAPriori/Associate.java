import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.javatools.weka.WekaDataUtils;
import weka.associations.Apriori;
import weka.associations.AprioriItemSet;
import weka.associations.ItemSet;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;


public class Associate extends SkeletonComponent {

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		double minSup = cf.getDoubleParameter("minSup");
		double maxSup = cf.getDoubleParameter("maxSup");
		double confidence = cf.getDoubleParameter("minConfidence");
		int numRules = cf.getIntParameter("maxRules");;
		boolean car = cf.getBooleanParameter("classRules");
		String classColumn = cf.getParameter("classCol");
		String removeCols = cf.getParameter("removeCols");
		File input = cf.getInput("data");
		File ouput = cf.getOutput("rules");
		
		CSVParser searchColList = null;
		
		if(cf.getInput("selectedColumns") != null)
			searchColList = new CSVParser(cf.getInput("selectedColumns"));
		
		CSVParser csvData = new CSVParser(input);
		
		
		String [] outHeading = new String[]{"Columns1","Values1","Columns2","Values2","Support1","Support2","Confidence"};
		
		String[] rCols = null;
		
		if(!removeCols.equals("")){
			rCols = removeCols.split(",");
			for(int i = 0; i < rCols.length; i++)
				rCols[i] = rCols[i].trim();
		}
		
		Instances data;
		CSVWriter rulesOut;
		
		Apriori associator = new Apriori();
		
		try{
			rulesOut = new CSVWriter(outHeading, ouput);
		}catch (IOException e) {
			System.err.println("Unable to write output file "+e);
			return ErrorCode.OUTPUT_IO_ERROR;
		}
		
		try{
			data = new Instances(WekaDataUtils.readCSV(input));
			if(rCols != null){
				data = WekaDataUtils.removeColumns(data, rCols);
			}
		}catch(Exception e){
			System.err.println("Problem reading the data "+e);
			return ErrorCode.INPUT_IO_ERROR;
		}
		
		associator.setLowerBoundMinSupport(minSup);
		associator.setUpperBoundMinSupport(maxSup);
		associator.setMinMetric(confidence);
		associator.setNumRules(numRules);
		associator.setOutputItemSets(true);
		
		FastVector[] res;
		
		//limit the association mining to subset of columns
		int iteration = 1;
		do{
			
			ArrayList<String> inCols =  new ArrayList<String>(Arrays.asList(csvData.getColumnNames()));
			if(searchColList != null){
				List<String> limitCols = Arrays.asList( searchColList.next()[0].split(","));
				
				//remove those columns that are already removed from the data
				for(String col : rCols)
					if(inCols.contains(col))
						inCols.remove(col);
				
				inCols.removeAll(limitCols);
				System.out.println("Using search columns "+iteration);
				iteration ++;

			}else{ 
				inCols.clear();
			}
			
			Instances selectedData = WekaDataUtils.removeColumns(data, inCols.toArray(new String[inCols.size()]));
			
			
			if(!car){
				try{
					associator.buildAssociations(selectedData);
				}catch(Exception e){
					System.err.println("Problem building associations "+e);
					return ErrorCode.ERROR;
				}

				res = associator.getAllTheRules();

			}else{

				selectedData.setClass(new Attribute(classColumn));

				try{
					res = associator.mineCARs(selectedData);
				}catch(Exception e){
					System.err.println("Problem building class association rules "+e);
					return ErrorCode.ERROR;
				}
			}

			/* this could be printed to document.tex
		System.out.println(associator.toString());
			 */

			if(!associator.getCar()){

				for (int i = 0; i < res[0].size(); i++) {
					createString(getRuleSet(((AprioriItemSet)res[0].elementAt(i)), selectedData), rulesOut);
					createString(getRuleSet(((AprioriItemSet)res[1].elementAt(i)), selectedData), rulesOut);

					rulesOut.write(((AprioriItemSet)res[0].elementAt(i)).support());
					rulesOut.write(((AprioriItemSet)res[1].elementAt(i)).support());
					rulesOut.write(((Double)res[2].elementAt(i)).doubleValue());
				}
			}
			else{
				for (int i = 0; i < res[0].size(); i++) {
					createString(getRuleSet((ItemSet)res[0].elementAt(i), associator.getInstancesNoClass()), rulesOut);
					createString(getRuleSet((ItemSet)res[1].elementAt(i), associator.getInstancesOnlyClass()), rulesOut);

					rulesOut.write(((ItemSet)res[0].elementAt(i)).support());
					rulesOut.write(((ItemSet)res[1].elementAt(i)).support());
					rulesOut.write(((Double)res[2].elementAt(i)).doubleValue());


				}
			}
		}while(searchColList != null && searchColList.hasNext());

	    rulesOut.close();
		return ErrorCode.OK;
	}
	private HashMap<String, String> getRuleSet(ItemSet set, Instances data){
		HashMap<String, String> map = new HashMap<String, String>();
		
		for(int i = 0; i < set.items().length; i++){
			if(set.items()[i] != -1){
				map.put(data.attribute(i).name(), data.attribute(i).value(set.items()[i]));
			}
		}
		
    	return map;
	}


	/**
	 * @param file can be null
	 * */
	private String createString(HashMap<String, String> itemSet, CSVWriter file){
		StringBuffer keys = new StringBuffer();
		
		int i = 0;
		for(String key : itemSet.keySet()){
    		if(i > 0){
    			keys.append(",");
    		}
			keys.append(key);
			
			i++;
    		
    	}
		
		
		i = 0;
		StringBuffer values = new StringBuffer();
		
		for(String value : itemSet.values()){
			if(i > 0){
    			values.append(",");
    		}
			values.append(value);
			
			i++;
    	}
		
		
		if(file != null){
			file.write(keys.toString());
			file.write(values.toString());
		}
		
		//keys.append("\t").append(values);
		
		return keys.append("\t").append(values).toString();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Associate().run(args);
	}

}
