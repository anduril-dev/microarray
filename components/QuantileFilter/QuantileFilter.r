library(componentSkeleton)

execute <- function(cf) {
    matrices <- list()
    matrices[[1]] <- Matrix.read(get.input(cf, 'matrix'))
    if (input.defined(cf, 'matrix2')) {
        matrices[[2]] <- Matrix.read(get.input(cf, 'matrix2'))
    }
    
    low.quantile <- get.parameter(cf, 'lowQuantile', 'float')
    if (low.quantile < 0 || low.quantile > 1) {
        write.error(cf, sprintf('lowQuantile must be between 0 and 1 (is: %s)', low.quantile)) 
        return(PARAMETER_ERROR)
    }
    high.quantile <- get.parameter(cf, 'highQuantile', 'float')
    if (high.quantile < 0 || high.quantile > 1) {
        write.error(cf, sprintf('highQuantile must be between 0 and 1 (is: %s)', high.quantile)) 
        return(PARAMETER_ERROR)
    }
    cols.independently <- get.parameter(cf, 'colsIndependently', 'boolean')
    
    N.ROW <- nrow(matrices[[1]])

    if(cols.independently) {
        for (col.index in 1:ncol(matrices[[1]])) {
            good <- rep(FALSE, N.ROW)
            for (matr.index in 1:length(matrices)) {
                if (low.quantile > 0) {
                    threshold <- quantile(matrices[[matr.index]][,col.index],
                        probs=low.quantile, na.rm=TRUE)
                    this.good <- matrices[[matr.index]][,col.index] >= threshold
                } else {
                    this.good <- rep(TRUE, N.ROW)
                }
                
                if (high.quantile < 1) {
                    threshold <- quantile(matrices[[matr.index]][,col.index],
                        probs=high.quantile, na.rm=TRUE)
                    this.good <- this.good & matrices[[matr.index]][,col.index] <= threshold
                }
                good <- good | this.good
            }
            
            for (matr.index in 1:length(matrices)) {
                matrices[[matr.index]][!good,col.index] <- NA
            }
        }
    }else {
        good <- rep(FALSE, N.ROW)
        for (matr.index in 1:length(matrices)) {
            high.good <- rep(FALSE, N.ROW)
            low.good <- rep(FALSE, N.ROW)
            for (col.index in 1:ncol(matrices[[1]])) {
                if (low.quantile > 0) {
                    threshold <- quantile(matrices[[matr.index]][,col.index],
                        probs=low.quantile, na.rm=TRUE)
                    this.low.good <- matrices[[matr.index]][,col.index] >= threshold
                }else {
                    this.low.good <- rep(TRUE, N.ROW)
                }
                
                if (high.quantile < 1) {
                    threshold <- quantile(matrices[[matr.index]][,col.index],
                        probs=high.quantile, na.rm=TRUE)
                        this.high.good <- matrices[[matr.index]][,col.index] <= threshold
                }else {
                    this.high.good <- rep(TRUE, N.ROW)
                }
                high.good <- high.good | this.high.good
                low.good <- low.good | this.low.good
            }
            good <- good | (high.good & low.good)
        }

        for (matr.index in 1:length(matrices)) {
            matrices[[matr.index]][!good,] <- NA
        }
    }

    if (get.parameter(cf, 'removeMissing', 'boolean')) {
        for (matr.index in 1:length(matrices)) {
            present <- sapply(1:nrow(matrices[[matr.index]]),
                function(i) !all(is.na(matrices[[matr.index]][i,])))
            matrices[[matr.index]] <- matrices[[matr.index]][present,,drop=FALSE]
        }
    }
    
    first.cell <- CSV.read.first.cell(get.input(cf, 'matrix'))
    CSV.write(get.output(cf, 'matrix'), matrices[[1]], first.cell=first.cell)
    if (length(matrices) < 2) {
        writeLines(character(0), get.output(cf, 'matrix2'))
    } else {
        first.cell <- CSV.read.first.cell(get.input(cf, 'matrix2'))
        CSV.write(get.output(cf, 'matrix2'), matrices[[2]], first.cell=first.cell)
    }
    
    return(0)
}

main(execute)
