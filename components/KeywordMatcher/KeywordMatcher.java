import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.anduril.component.CSVReader;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class KeywordMatcher extends SkeletonComponent {
    public static final double MAX_IC = 1.0;
    public static final double MAX_DISTANCE = 1.0;
    public static final double UNIQUE_KEYWORD_MIN_IC = 0.99;
    
    /** Information content map. The map is indexed
     * by annotation column name, which gives a map
     * from terms to ICs. ICs are normalized to range
     * 0 to 1. */
    private final Map<String, Map<String, Double>> icMap;
    
    private final Map<String, Map<String, Set<String>>> token2AnnotationID;
    private final Map<String, Map<String, Set<String>>> annotationID2tokenSet;
    
    private Pattern removePattern;
    private Pattern tokenizePattern;
    private Pattern trimPattern;
    
    private double matchDistance;
    private int maxMatches;
    private double pruneKeywordIC;
    
    private int numAnnotationRows = -1;

    /** Sort Map items primarily based on values and secondarily
     * on keys. Helper class for processQuery. */
    private class EntrySetComparator implements Comparator<Map.Entry<String,Double>> {
        @Override
        public int compare(Map.Entry<String, Double> entry1,
                Map.Entry<String, Double> entry2) {
            int value = entry1.getValue().compareTo(entry2.getValue());
            if (value == 0) value = entry1.getKey().compareTo(entry2.getKey());
            return value;
        }
    }
    
    public KeywordMatcher() {
        this.icMap = new HashMap<String, Map<String,Double>>();
        this.token2AnnotationID = new HashMap<String, Map<String, Set<String>>>();
        this.annotationID2tokenSet = new HashMap<String, Map<String, Set<String>>>();
    }
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        long start = System.nanoTime();
        
        readParameters(cf);
        readAnnotation(cf);
        
        if (cf.inputDefined("query")) processQuery(cf);
        else processAnnotationStats(cf);
        
        System.out.println(String.format("Time: %.1f s", (System.nanoTime()-start)/1e9));
        return ErrorCode.OK;
    }
    
    private void readParameters(CommandFile cf) {
        this.removePattern = Pattern.compile(cf.getParameter("removePattern"));
        if (this.removePattern.pattern().isEmpty()) this.removePattern = null;
        
        this.tokenizePattern = Pattern.compile(cf.getParameter("tokenizePattern"));
        if (this.tokenizePattern.pattern().isEmpty()) this.tokenizePattern = null;
        
        String trim = cf.getParameter("trimPattern");
        if (!trim.isEmpty()) {
            if (!trim.startsWith("^") || !trim.endsWith("$")) {
                trim = String.format("^(?:%s)|(?:%s)$", trim, trim);
            }
            this.trimPattern = Pattern.compile(trim);
        }
        
        this.matchDistance = Double.parseDouble(cf.getParameter("matchDistance"));
        this.maxMatches = Integer.parseInt(cf.getParameter("maxMatches"));
        this.pruneKeywordIC = Double.parseDouble(cf.getParameter("pruneKeywordIC"));
    }
    
    private void tokenize(String value, Set<String> destination) {
        destination.clear();
        if (value == null) return;
        
        if (this.removePattern != null) {
            value = this.removePattern.matcher(value).replaceAll(" ");
        }
        
        for (String token: this.tokenizePattern.split(value)) {
            token = token.trim();
            if (this.trimPattern != null) {
                token = this.trimPattern.matcher(token).replaceAll("");
            }
            if (token.isEmpty()) continue;
            token = token.toLowerCase();
            destination.add(token);
        }
    }
    
    private int[] parseColumnNames(String columnSpec, CSVReader reader) {
        String[] columnNames = columnSpec.split(",");
        final int[] indices = new int[columnNames.length];
        
        for (int i=0; i<columnNames.length; i++) {
            String columnName = columnNames[i].trim(); 
            if (columnName.isEmpty()) continue;
            if (columnName.equals("*")) {
                int[] indexArray = new int[reader.getHeader().size()];
                for (int j=0; j<indexArray.length; j++) {
                    indexArray[j] = j;
                }
                return indexArray;
            }
            indices[i] = reader.getColumnIndex(columnName);
        }
        
        return indices;
    }
    
    private void readAnnotation(CommandFile cf) throws IOException {
        Map<String,Integer> maxCounts = new HashMap<String, Integer>();
        CSVReader reader = new CSVReader(cf.getInput("annotation"));
        
        try {
            String keyColumn = cf.getParameter("annotationKeyColumn");
            final int keyIndex = keyColumn.isEmpty() ? 0 : reader.getColumnIndex(keyColumn);
            final int[] annIndices = parseColumnNames(cf.getParameter("annotationColumns"), reader);
            
            for (List<String> row: reader) {
                final String id = row.get(keyIndex);
                for (int index: annIndices) {
                    final String colName = reader.getHeader().get(index);
                    Map<String,Double> colMap = this.icMap.get(colName);
                    if (colMap == null) {
                        colMap = new HashMap<String, Double>();
                        this.icMap.put(colName, colMap);
                    }
                    
                    String value = row.get(index);
                    if (value == null) continue;
                    Set<String> tokens = new HashSet<String>();
                    tokenize(value, tokens);

                    Map<String, Set<String>> tokenMap = this.token2AnnotationID.get(colName);
                    if (tokenMap == null) {
                        tokenMap = new HashMap<String, Set<String>>();
                        this.token2AnnotationID.put(colName, tokenMap);
                    }
                    for (String token: tokens) {
                        Double count = colMap.get(token);
                        if (count == null) count = 0.0;
                        colMap.put(token, count+1);
                        Set<String> tokenIDSet = tokenMap.get(token);
                        if (tokenIDSet == null) {
                            tokenIDSet = new HashSet<String>();
                            tokenMap.put(token, tokenIDSet);
                        }
                        tokenIDSet.add(id);
                    }
                    
                    if (!tokens.isEmpty()) {
                        Integer count = maxCounts.get(colName);
                        if (count == null) count = 0;
                        maxCounts.put(colName, count+1);
                        
                        Map<String, Set<String>> id2TokenMap = this.annotationID2tokenSet.get(colName);
                        if (id2TokenMap == null) {
                            id2TokenMap = new HashMap<String, Set<String>>();
                            this.annotationID2tokenSet.put(colName, id2TokenMap);
                        }
                        id2TokenMap.put(id, tokens);
                    }
                }
            }
            this.numAnnotationRows = reader.getLineNumber() - 1;
        } finally {
            reader.close();
        }
        
        for (String colName: this.icMap.keySet()) {
            Integer maxCount = maxCounts.get(colName);
            Map<String,Double> colMap = this.icMap.get(colName);
            if (maxCount == null || colMap == null) continue;
            double logMax = Math.log(maxCount);
            
            for (Map.Entry<String,Double> entry: colMap.entrySet()) {
                final double ic = 1.0 - (Math.log(entry.getValue()) / logMax);
                colMap.put(entry.getKey(), ic);
                if (ic < this.pruneKeywordIC) {
                    this.token2AnnotationID.get(colName).remove(entry.getKey());
                }
            }
        }
    }
    
    private void processQuery(CommandFile cf) throws IOException {
        CSVReader queryReader = new CSVReader(cf.getInput("query"));
        
        final String[] RESULT_COLUMNS = new String[] {"QueryID", "TargetID", "TargetDistance", "Keywords"};
        CSVWriter resultWriter = new CSVWriter(RESULT_COLUMNS, cf.getOutput("mapping"));
        
        int count = 0;
        try {
            /* Reusable data structures */
            Set<String> queryTokens = new HashSet<String>();
            Map<String,Double> matchDistances = new HashMap<String, Double>();
            SortedSet<String> uniqueTerms = new TreeSet<String>();
            Set<String> candidateIDSet = new HashSet<String>();
            StringBuffer buffer1 = new StringBuffer();
            StringBuffer buffer2 = new StringBuffer();
        
            final String keyColumn = cf.getParameter("queryKeyColumn");
            final int keyIndex = keyColumn.isEmpty() ? 0 : queryReader.getColumnIndex(keyColumn);
            final int[] queryIndices = parseColumnNames(cf.getParameter("queryColumns"), queryReader);
            
            for (List<String> row: queryReader) {
                final String queryID = row.get(keyIndex);
                matchDistances.clear();
                uniqueTerms.clear();
                
                for (String annColumn: this.icMap.keySet()) {
                    Map<String,Double> icAnnMap = this.icMap.get(annColumn);
                    Map<String,Set<String>> tokenMap = this.annotationID2tokenSet.get(annColumn);
                    Map<String,Set<String>> token2IDMap = this.token2AnnotationID.get(annColumn);
                    if (icAnnMap == null || tokenMap == null || token2IDMap == null) continue;
                    
                    for (int queryIndex: queryIndices) {
                        tokenize(row.get(queryIndex), queryTokens);
                        if (queryTokens.isEmpty()) continue;
                        
                        candidateIDSet.clear();
                        for (String token: queryTokens) {
                            Double ic = icAnnMap.get(token);
                            if (ic != null && ic >= UNIQUE_KEYWORD_MIN_IC) {
                                uniqueTerms.add(token);
                            }
                            Set<String> tokenIDSet = token2IDMap.get(token);
                            if (tokenIDSet != null) candidateIDSet.addAll(tokenIDSet);
                        }
                        
                        for (String candidateID: candidateIDSet) {
                            final Set<String> annTokens = tokenMap.get(candidateID);
                            final double distance = getDistance(queryTokens, annTokens, icAnnMap);
                            count++;
                            if (distance <= this.matchDistance) {
                                Double prevDistance = matchDistances.get(candidateID);
                                if (prevDistance == null || prevDistance > distance) {
                                    matchDistances.put(candidateID, distance);
                                }
                                if (this.maxMatches == 1 && distance < 1e-9) {
                                    /* Early exit when optimal match encountered */
                                    break;
                                }
                            }
                        }
                    }
                }
                
                TreeSet<Map.Entry<String,Double>> entrySet =
                    new TreeSet<Map.Entry<String,Double>>(new EntrySetComparator());
                entrySet.addAll(matchDistances.entrySet());
                
                buffer1.setLength(0);
                buffer2.setLength(0);
                int index = 0;
                for (Map.Entry<String,Double> entry: entrySet) {
                    if (index > 0) {
                        buffer1.append(',');
                        buffer2.append(',');
                    }
                    buffer1.append(entry.getKey());
                    buffer2.append(String.format("%.6f", entry.getValue().doubleValue()));
                    index++;
                    if (index >= this.maxMatches) break;
                }
                resultWriter.write(queryID);
                resultWriter.write(buffer1.length() > 0 ? buffer1.toString(): null);
                resultWriter.write(buffer2.length() > 0 ? buffer2.toString() : null);
                
                buffer1.setLength(0);
                for (String term: uniqueTerms) {
                    if (buffer1.length() > 0) buffer1.append(',');
                    buffer1.append(term);
                }
                resultWriter.write(buffer1.length() > 0 ? buffer1.toString(): null);
            }
            
            final int queryRows = queryReader.getLineNumber() - 1;
            int maxCount = queryRows * this.numAnnotationRows
                * this.icMap.keySet().size() * queryReader.getHeader().size();
            System.out.println(String.format("Tried %d of %d possible combinations (%.1f %%)",
                    count, maxCount, 100.0*count/maxCount));
        } finally {
            queryReader.close();
            resultWriter.close();
        }
    }
    
    private void processAnnotationStats(CommandFile cf) throws IOException {
        CSVReader reader = new CSVReader(cf.getInput("annotation"));
        
        final String[] RESULT_COLUMNS = new String[] {"TargetID", "Keywords"};
        CSVWriter resultWriter = new CSVWriter(RESULT_COLUMNS, cf.getOutput("mapping"));
        
        try {
            String keyColumn = cf.getParameter("annotationKeyColumn");
            if (keyColumn.isEmpty()) keyColumn = reader.getHeader().get(0);
            final int keyIndex = reader.getColumnIndex(keyColumn);
            
            StringBuffer keywordBuffer = new StringBuffer();
            SortedSet<String> uniqueTerms = new TreeSet<String>();
            
            for (List<String> row: reader) {
                final String id = row.get(keyIndex);
                resultWriter.write(id);
            
                uniqueTerms.clear();
                for (String annColumn: this.annotationID2tokenSet.keySet()) {
                    Set<String> tokens = this.annotationID2tokenSet.get(annColumn).get(id);
                    Map<String, Double> ics = this.icMap.get(annColumn);
                    
                    if (tokens != null) {
                        for (String token: tokens) {
                            Double ic = ics.get(token);
                            if (ic != null && ic >= UNIQUE_KEYWORD_MIN_IC) {
                                uniqueTerms.add(token);
                            }
                        }
                    }
                }
                
                if (uniqueTerms.isEmpty()) {
                    resultWriter.write(null);
                } else {
                    keywordBuffer.setLength(0);
                    for (String term: uniqueTerms) {
                        if (keywordBuffer.length() > 0) keywordBuffer.append(',');
                        keywordBuffer.append(term);
                    }
                    resultWriter.write(keywordBuffer.toString());
                }
            }
        } finally {
            reader.close();
            resultWriter.close();
        }
    }
    
    /**
     * Compute Czekanowski-Dice distance of term sets using information
     * contents of the terms.
     */
    public final double getDistance(Set<String> terms1, Set<String> terms2, Map<String,Double> ics) {
        if (terms1.isEmpty() || terms2.isEmpty()) return MAX_DISTANCE;

        if (terms1.size() < terms2.size()) {
            /* Swap sets so that the smaller is assigned to term2.
             * This reduces the number of operations is latter loop. */
            Set<String> tmp = terms1;
            terms1 = terms2;
            terms2 = tmp;
        }
        
        double symDiffSum = 0;
        double unionSum = 0;
        double intersectSum = 0;
        
        for (String id: terms1) {
            final boolean inTerms2 = terms2.contains(id);
            final Double icObj = ics.get(id);
            final double ic = (icObj == null) ? MAX_IC : icObj.doubleValue();

            unionSum += ic;
            if (inTerms2) intersectSum += ic;
            else symDiffSum += ic;
        }
        
        for (String id: terms2) {
            final boolean inTerms1 = terms1.contains(id);
            if (!inTerms1) {
                final Double icObj = ics.get(id);
                final double ic = (icObj == null) ? MAX_IC : icObj.doubleValue();
                unionSum += ic;
                symDiffSum += ic;
            }
        }
        
        return symDiffSum / (unionSum + intersectSum);
    }
    
    public static void main(String[] args) {
        new KeywordMatcher().run(args);
    }
}
