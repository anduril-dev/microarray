library(componentSkeleton)

execute <- function(cf) {
	library(meap)

	expr <- read.table(get.input(cf, 'expr'), header=TRUE,sep="\t",stringsAsFactors=FALSE,row.names=get.parameter(cf,'IDcolumn','int'))
	samples <- read.table(get.input(cf,'sampleAnnotation'),header=TRUE,sep="\t",stringsAsFactors=FALSE,row.names=get.parameter(cf,'sampleIDCol','int'))

	sample.type <- get.parameter(cf,'sampleType')
	cont <- get.parameter(cf, 'contrasts')
	adjust <- get.parameter(cf,'adjust')
	sort.by <- get.parameter(cf, 'sortBy')
	pval <- get.parameter(cf, 'pvalue', 'float')
	lfc <- get.parameter(cf, 'lfc', 'float')
	number <- get.parameter(cf, 'number', 'float')
	cont <- strsplit(cont,",")[[1]]

	if(sample.type=="paired"){
		stopifnot(ncol(samples)==2)
	}else{
		stopifnot(ncol(samples)==1)
	}

	write.log(cf, "Running differential analysis...")

	result <- limma.stats(expr=expr, sampleAnnotation=samples, sample.type=sample.type, contrasts=cont,number=number,adjust.method=adjust,sort.by=sort.by,p.value=pval, lfc=lfc)
    result <- data.frame(ID=rownames(result), result)
	
	write.log(cf, "Writing result outputs...")
	write.table(result,get.output(cf, 'sigTarget'),sep="\t",quote=FALSE,row.names=FALSE)

	return(0)
}

main(execute)
