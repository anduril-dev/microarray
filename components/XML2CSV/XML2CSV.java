import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

import java.io.File;
import java.util.Hashtable;
import java.util.LinkedList;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XML2CSV extends SkeletonComponent {

	/**
	 * @param args
	 */
	LinkedList<Hashtable<String,String>>	table;
	LinkedList<String>	columnList;
	boolean	includeNonLeafContent;
	int	elementCode=0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	XML2CSV().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		String	newLineSplitter=cf.getParameter("newLineSplitter");
		
		includeNonLeafContent=cf.getBooleanParameter("includeNonLeafContent");
		table=new	LinkedList<Hashtable<String,String>>();
		columnList=new	LinkedList<String>();
		
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse (cf.getInput("xml"));
		
        Element	root=doc.getDocumentElement();
        
        root.normalize ();
        
        parseElement(root);       
        
        
        String[]	iniColumns={"#TID#","#PID#","#PPID#","#NAME#","#CONTENT#"};
        String[]	columns=new	String[iniColumns.length+columnList.size()];
        for(int	i=0;i<iniColumns.length;i++){
        	columns[i]=iniColumns[i];
        }
        for(int	i=0;i<columnList.size();i++)
        {
        	columns[iniColumns.length+i]=columnList.get(i);
        }
		CSVWriter	csv=new	CSVWriter(columns,cf.getOutput("csv"),true);
		if(cf.getBooleanParameter("includeSource")){
			csv.write(-1);
			csv.write("NA");
			csv.write("NA");
			csv.write("SOURCE_XML");
			csv.write(cf.getInput("xml").getAbsolutePath());
			for(int i=0;i<columnList.size();i++)
				csv.write("NA");
		}
		for(Hashtable<String,String>	row:table)
		{
			for(String	column:columns){
				if(row.containsKey(column)){
					csv.write(row.get(column).replaceAll("\n", newLineSplitter));
				}
				else
					csv.write("NA");
			}
		}
		csv.flush();
		csv.close();
		return ErrorCode.OK;
	}

	private void parseElement(Element element) {
		// TODO Auto-generated method stub
		parseElement(element,0,null);
	}

	private void parseElement(Element element,int	PID,int[]	PPID) {
		// TODO Auto-generated method stub
		NamedNodeMap	attributes=element.getAttributes();
		Hashtable<String,String>	row=new	Hashtable<String,String>();
//		int	TID=element.hashCode();
		int	TID=elementCode++;
		row.put("#TID#", Integer.toHexString(TID));
		if(PID>0)
			row.put("#PID#", Integer.toHexString(PID));
		if(PPID!=null){
			String	PPIDStr="";
			String	spl="";
			for(int	i=0;i<PPID.length;i++)
			{
				PPIDStr=PPIDStr+spl+Integer.toHexString(PPID[i]);
				spl=".";
			}
			row.put("#PPID#", PPIDStr);
		}
		if(element.getTagName()!=null)
			row.put("#NAME#", element.getTagName());
		NodeList	children=element.getChildNodes();
		boolean	isLeaf=true;
		for(int	i=0;(i<children.getLength())&&isLeaf;i++)
		{
			Node	subNode=children.item(i);
			if(subNode.getNodeType() == Node.ELEMENT_NODE){
				isLeaf=false;
			}
		}
		if(isLeaf||includeNonLeafContent)
			if(element.getTextContent()!=null)
				row.put("#CONTENT#", element.getTextContent());
		for(int	i=0;i<attributes.getLength();i++){
			Node	attribute=attributes.item(i);
			String	attrName=attribute.getNodeName();
			row.put(attrName, attribute.getNodeValue());
			if(!columnList.contains(attrName))
				columnList.add(attrName);
		}
		table.add(row);
		int[]	subPPID;
		if(PPID!=null){
			subPPID=new	int[PPID.length+1];
			for(int	i=0;i<PPID.length;i++)
				subPPID[i]=PPID[i];
			subPPID[PPID.length]=TID;
		}
		else
		{
			subPPID=new	int[1];
			subPPID[0]=TID;
		}
		
		for(int	i=0;i<children.getLength();i++)
		{
			Node	subNode=children.item(i);
			if(subNode.getNodeType() == Node.ELEMENT_NODE){
				parseElement((Element) subNode,TID,subPPID);
			}
		}
	}
	
	

}
