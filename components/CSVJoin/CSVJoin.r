library(componentSkeleton)

MAX.INPUTS <- 8

execute <- function(cf) {
    merged.csv <- NULL
    use.keys <- get.parameter(cf, 'useKeys', 'boolean')
    key.columns <- split.trim(get.parameter(cf, 'keyColumnNames'), ',')
    take.intersection <- get.parameter(cf, 'intersection', 'boolean')
    min.rows <- get.parameter(cf, 'minRows', 'int')
	csvs <- list()
	as.con        <- get.parameter(cf, 'asConnection', 'boolean')


    if (input.defined(cf, 'csvDir')) {
#        folderIn <- TRUE
        files <- list.files(get.input(cf, 'csvDir'), full.names=TRUE)
#        MAX.INPUTS <- length(files)
        for (i in 1:length(files))
        {
        	if(as.con) {
                csvs[[i]] <- file(files[i], 'r')
            }else {
                csvs[[i]] <- CSV.read(files[i])
            }
        }
    } else {
#        folderIn <- FALSE
    }
    
    if (input.defined(cf, 'array')) {
#        arrayIn <- TRUE
        array.temp    <- Array.read(cf, 'array')
#        array <- list()
        for(i in 1:Array.size(array.temp)) {
            key <- Array.getKey(array.temp, i)
            file <- Array.getFile(array.temp, key)
            if(as.con) {
                csvs[[key]] <- file(file, 'r')
            }else {
                csvs[[key]] <- CSV.read(file)
            }
        }
#        MAX.INPUTS <- length(files)
    } else {
#        arrayIn <- FALSE
    }

	for (i in 1:8)
	{
		input.name <- paste('csv', i, sep='')
		if (!input.defined(cf, input.name)) next
		if(as.con){
			csvs[[input.name]] <- file(get.input(cf, input.name), 'r')
		}
		else
		{
			csvs[[input.name]] <- CSV.read(get.input(cf, input.name))
		}
	}

	MAX.INPUTS <- length(csvs)

    for (i in 1:MAX.INPUTS) {
#        if (folderIn){
#            input.name <- files[i]
#            this.csv <- CSV.read(input.name)
#        } else {
#            input.name <- paste('csv', i, sep='')
#            if (!input.defined(cf, input.name)) next
#            this.csv <- CSV.read(get.input(cf, input.name))
#        }
  
		this.csv <- csvs[[i]]
        
        if (length(key.columns) >= i && nchar(key.columns[i]) > 0) {
            key.column <- key.columns[i]
            if (!identical(key.column, colnames(this.csv)[1])) {
                index <- match(key.column, colnames(this.csv))
                if (is.na(index)) {
                    write.error(cf, sprintf('Key column %s not found in %s', key.column, names(csvs[])[i]))
                }
                values <- this.csv[,key.column]
                this.csv <- this.csv[,-index,drop=FALSE]
                this.csv <- cbind(values, this.csv)
                colnames(this.csv)[1] <- key.column
            }
        }
        
        if (use.keys && any(duplicated(this.csv[,1]))) {
            write.log(cf,
                sprintf('Warning: %s contains duplicate values in the key column', names(csvs[])[i]))
        }
        
        if (is.null(merged.csv)) {
            merged.csv <- this.csv
            next
        }
        
        if (use.keys) {
            by.x <- colnames(merged.csv)[1]
            by.y <- colnames(this.csv)[1]
            REMOVE.SUFFIX <- '--remove--Y2X2lC2j6LhjlYI1hyBy--'
            merged.csv <- merge(merged.csv, this.csv, by.x=by.x, by.y=by.y,
                all=!take.intersection, suffixes=c('', REMOVE.SUFFIX))

            remove.indices <- grep(REMOVE.SUFFIX, colnames(merged.csv), fixed=TRUE)
            for (index in remove.indices) {
                # If the new values have NA's, take old values in their place.
                remove.name <- colnames(merged.csv)[index]
                keep.name <- substr(remove.name, 1, nchar(remove.name)-nchar(REMOVE.SUFFIX))
                
                remove.values <- merged.csv[,index]
                keep.values <- merged.csv[,keep.name]
                keep.values[is.na(keep.values)] <- remove.values[is.na(keep.values)]
                
                merged.csv[,keep.name] <- keep.values
            }
            if (length(remove.indices) > 0) {
                merged.csv <- merged.csv[,-remove.indices,drop=FALSE]
            }
        } else {
            missing.cols1 <- setdiff(colnames(merged.csv), colnames(this.csv))
            missing.cols2 <- setdiff(colnames(this.csv), colnames(merged.csv))
            merged.csv[,missing.cols2] <- NA
            this.csv[,missing.cols1] <- NA
            merged.csv <- rbind(merged.csv, this.csv)
            merged.csv <- merged.csv[!duplicated(merged.csv),,drop=FALSE]
        }
    }
    if (nrow(merged.csv) < min.rows) {
        write.error(cf, sprintf('Output contains too few rows. output rows: %d, minimum: %d',nrow(merged.csv),min.rows))
    }
    
    
    CSV.write(get.output(cf, 'csv'), merged.csv)
    return(0)
}

main(execute)
