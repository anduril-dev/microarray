library(componentSkeleton)

MAX.TRANSFORMS <- 9

execute <- function(cf) {
    if (input.defined(cf, 'csv1')) {
        csv1 <- CSV.read(get.input(cf, 'csv1'))
        matrix1 <- make.matrix(csv1)
    }
    if (input.defined(cf, 'csv2')) {
        csv2 <- CSV.read(get.input(cf, 'csv2'))
        matrix2 <- make.matrix(csv2)
    }
    if (input.defined(cf, 'array')) {
        array.temp    <- Array.read(cf, 'array')
        for(i in 1:Array.size(array.temp)) {
            key <- Array.getKey(array.temp, i)
            file <- Array.getFile(array.temp, key)
            eval(parse(text=paste("csv.",key,"<-CSV.read(file)",sep="")))
            eval(parse(text=paste("matrix.",key,"<-make.matrix(csv.",key,")",sep="")))
        }
    }
    bind.function <- eval(parse(text=trim(get.parameter(cf, 'combineFunction'))))
    transformed <- NULL
    for (i in 1:MAX.TRANSFORMS) {
        param.name <- sprintf('transform%d', i)
        trans <- trim(get.parameter(cf, param.name))
        if (nchar(trans) == 0) next
        
        result <- eval(parse(text=trans))
        if (is.null(colnames(result))) {
            if (!is.data.frame(result) && !(is.matrix(result))) {
                result <- as.data.frame(result, stringsAsFactors=FALSE)
            }
            colnames(result) <- sprintf('transform%d_%d', i, 1:ncol(result))
        }
        
        if (is.null(transformed)) {
            transformed <- result
        } else {
            transformed <- bind.function(transformed, result)
        }
    }

    col.names.expr <- trim(get.parameter(cf, 'columnNames'))
    if (nchar(col.names.expr) > 0 || input.defined(cf, 'columnNames')) {
        if (nchar(col.names.expr) > 0){
            col.names <- eval(parse(text=col.names.expr))
        } else {
            col.names <- as.character(as.matrix(CSV.read(get.input(cf, 'columnNames'))))
        }
        if (length(col.names) != ncol(transformed)) {
            write.error(cf,
                'columnNames expression must return a vector of the same length as there are columns in result')
            write.error(cf, paste('Got:', paste(col.names)))
            return(PARAMETER_ERROR)
        }
        colnames(transformed) <- col.names
    }
    
    CSV.write(get.output(cf, 'transformed'), transformed)
    return(0)
}

make.matrix <- function(dataframe) {
    columns <- integer()
    for (i in 1:ncol(dataframe)) {
        if (is.numeric(dataframe[[i]])) columns <- c(columns, i) 
    }
    if (length(columns) > 0) return(as.matrix(dataframe[,columns,drop=FALSE]))
    else return(matrix())
}

main(execute)
