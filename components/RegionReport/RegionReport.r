library(componentSkeleton)

execute <- function(cf) {
  myName  <- get.metadata(cf,  'instanceName')
  out.dir <- get.output(cf,    'report')
  pTitle  <- get.parameter(cf, 'title')
  pChr    <- get.parameter(cf, 'chrColumn')
  pStart  <- get.parameter(cf, 'startColumn')
  pEnd    <- get.parameter(cf, 'endColumn')
  data    <- CSV.read(get.input(cf, 'regions'));
  dir.create(out.dir, recursive=TRUE)

  tex <- c('\\clearpage{}',
           sprintf('\\%s{%s}\\label{sec:%s}', get.parameter(cf, 'sectionType'), pTitle, myName))

  if (nrow(data) < 1) {
     tex <- c(tex, '\\textit{This dataset is completely empty.}')
     latex.write.main(cf, 'report', tex)
     return(0)
  }

  if (input.defined(cf, 'chromosomes')) {
     cLengths <- AnnotationTable.read(get.input(cf, 'chromosomes'))
     pLength  <- get.parameter(cf, 'lengthColumn')
  } else {
     cLengths <- NULL
  }

  chrs <- as.character(sort(unique(data[,pChr])))

  tex <- c(tex,
           sprintf('Chromosome specific statistics are shown in Table~\\ref{tab:%s}.',         myName),
           sprintf('A histogram of sequence lengths is shown in Figure~\\ref{fig:%s-length}.', myName))

  chrFreqs <- as.data.frame(matrix(ncol=7,nrow=length(chrs)))
  for (chr in 1:length(chrs)) {
      chrData         <- data[data[,pChr]==chrs[chr],]
      rLengths        <- chrData[,pEnd]-chrData[,pStart]
      chrFreqs[chr,1] <- paste('\\textit{',latex.quote(chrs[chr]),'}',sep='')
      chrFreqs[chr,2] <- nrow(chrData)
      chrFreqs[chr,3] <- min(rLengths)
      chrFreqs[chr,4] <- round(mean(rLengths))
      chrFreqs[chr,5] <- max(rLengths)
      chrFreqs[chr,6] <- sum(rLengths)
      if (!is.null(cLengths)) {
         chrFreqs[chr,7] <- round(chrFreqs[chr,6]/cLengths[cLengths[,1]==chrs[chr],pLength], 6)
      }
  }
  rLengths <- data[,pEnd]-data[,pStart]
  rTotal   <- sum(rLengths)
  rMean    <- mean(rLengths)
  captText <- 'Chromosome specific distribution of the regions. The last line represents the overall statistics.'
  if (is.null(cLengths)) {
     tex <- c(tex,
              '\\begin{table}[!h]{\\begin{center}\\footnotesize\\begin{tabular}{rrrrrr}',
              '\\cline{3-6} & & \\multicolumn{4}{|c|}{length}\\\\\\cline{1-2}',
              'chromosome & frequency & min & mean & max & total \\\\\\hline',
              paste(chrFreqs[,1],chrFreqs[,2],chrFreqs[,3],chrFreqs[,4],chrFreqs[,5],chrFreqs[,6],
                    sep=' & ',collapse=' \\\\\n'),
              sprintf('\\\\\\hline\nall %d & %d & %d & %s & %d & %d',
                      length(chrs), nrow(data), min(rLengths), round(rMean), max(rLengths), rTotal))
  } else {
     tex <- c(tex,
              '\\begin{table}[!h]{\\begin{center}\\footnotesize\\begin{tabular}{rrrrrrl}',
              '\\cline{3-7} & & \\multicolumn{5}{|c|}{length}\\\\\\cline{1-2}',
              'chromosome & frequency & min & mean & max & total & coverage \\\\\\hline',
              paste(chrFreqs[,1],chrFreqs[,2],chrFreqs[,3],chrFreqs[,4],chrFreqs[,5],chrFreqs[,6],chrFreqs[,7],
                    sep=' & ',collapse=' \\\\\n'),
              sprintf('\\\\\\hline\nall %d & %d & %d & %s & %d & %d & %s',
                      length(chrs), nrow(data), min(rLengths), round(rMean), max(rLengths), rTotal,
                      round(rTotal/sum(as.numeric(cLengths[,pLength])),6)))
  }
  tex <- c(tex,
           '\\\\\\hline\\end{tabular}\\end{center}}',
           sprintf('\\caption{%s}\\label{tab:%s}', captText, myName),
           '\\end{table}')

  fname <- sprintf('%s-length.pdf', myName)
  tex <- c(tex,
           latex.figure(fname,
                        caption    = 'Sequence length distribution. Dashed line represents a normal distribution approximation that is based on the mean and the standard deviation of the sequece lengths.',
                        quote.capt = FALSE,
                        fig.label  = sprintf('fig:%s-length',myName)),
           '\\clearpage{}')
  pdf(file.path(out.dir, fname), paper='special', width=6, height=4)
  h <- hist(rLengths, main='', xlab='length (base pairs)')
  curve(dnorm(x, mean=rMean, sd=sd(rLengths))*diff(h$mids[1:2])*length(rLengths), add=TRUE, lty=2)
  dev.off()

  latex.write.main(cf, 'report', tex)
  return(0)
}

main(execute)
