library(componentSkeleton)

execute <- function(cf) {
    indata <- CSV.read(get.input(cf, 'csv'))
    
    n <- get.parameter(cf, 'N', 'float')
    if (n < 2) {
        write.error(cf, sprintf('Bin N must be over 1'))
        return(1)
    }
    nonnumeric<-c()
    numerics<-c()
    datacolnames <- colnames(indata)
    for (i in 1:ncol(indata)) {
        if (is.numeric(indata[,i])) {
        numerics<-c(numerics,i)
        this.data=(indata[,i]-mean(indata[,i], na.rm=TRUE))/sd(indata[,i], na.rm=TRUE)
        indata[this.data<= (-3+6/n),i]<-1
        if (n>2) { for (c in 2:(n-1)) {
            indata[(this.data> (-3+6*(c-1)/n)) & (this.data<= (-3+6*(c)/n)),i]<-c
        } }
        indata[this.data> (-3+6*(n-1)/n),i]<-n
        datacolnames[i]<-paste(datacolnames[i],'Bin')
        } else { 
        nonnumeric<-c(nonnumeric, i)
        }
        
    }
    # writeout data before cluster id:s
    colnames(indata)<-datacolnames
    CSV.write(get.output(cf, 'binned'), indata)
    clusters<-as.matrix(apply(indata[,numerics],1,function(x) paste(x,collapse=',')),nrow=nrow(indata))
    for (i in 1:length(numerics)) {
        indata[,numerics[i]]<-(indata[,numerics[i]]-1)*n^(length(numerics)-i)
    }
    clustersum<-apply(indata[,numerics],1,sum)+1
    clusters<-cbind(indata[,nonnumeric],clusters,clustersum)
    colnames(clusters)<-c(datacolnames[nonnumeric],'clusterString','clusterId')
    
    CSV.write(get.output(cf, 'cluster'), clusters)

    return(0)
}

main(execute)
