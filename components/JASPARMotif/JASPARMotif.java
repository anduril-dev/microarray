import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

/**
 * An Anduril component for fetching motifs from the JASPAR database.
 * <p>
 * Uses a FlatFileDir version of JASPAR (which means a flat file version of the
 * database). Should only be used within Anduril!
 * 
 * @author Viljami Aittomaki
 * @version 2.0.3
 */
public class JASPARMotif extends SkeletonComponent {

    /* Component interface names */
    public static final String PARAMETER_FLAT_FILE_URL = "flatFileURL";
    public static final String PARAMETER_ID_TYPE = "idType";
    public static final String PARAMETER_SPECIES = "species";
    public static final String PARAMETER_CLASS = "class";
    public static final String PARAMETER_COLLECTION = "collection";
    public static final String INPUT_IDs = "IDs";
    public static final String OUTPUT_MOTIFS = "motifs";
    public static final String OUTPUT_ANNOTATIONS = "annotations";

    /** Columns in annotation file */
    public static final String[] OUTPUT_ANNOTATION_COLUMNS = { "name", "id", "version", "class", "family",
            "species", "collection", "acc", "medline", "pazar_tf_id", "type", "comment" };
    
    /** Species value in species parameter corresponding to motifs with no annotated species */
    public static final String COMPONENT_PSEUDO_SPECIES_ID = "-";

    /* Database formats and filenames */
    /** Species value in database corresponding to motifs with no annotated species */
    public static final String DATABASE_PSEUDO_SPECIES_ID = "";
    /** Format of motif annotation lines in database annotation file (regex) */
    public static final String DATABASE_ANNOTATION_FILE_FORMAT = "\\S+\\.\\d+\\t\\d*\\.?\\d*\\t[\\S ]+\\t[\\S ]+\\t(\\s?; [\\S ^\"]+ \"[\\S ]+\")*";
    /** Filename of motif annotations file in flat file database directory */
    public static final String DATABASE_ANNOTATION_FILE_NAME = "matrix_list.txt";
    /** Extension of motif files in flat file database */
    public static final String DATABASE_MOTIF_FILE_EXTENSION = ".pfm";
    /** Separator between motif id and version in motif file names in flat file database */
    public static final String DATABASE_MOTIF_FILE_VERSION_SEPARATOR = ".";

    /* Output formats for motif matrix files */
    /** Base names in motif matrix header */
    public static final String[] MOTIF_BASES = { "A", "C", "G", "T" };
    /** Separator between elements in motif matrix. */
    public static final String MOTIF_ELEMENT_SEPARATOR = " ";

    /** Criteria for filtering motifs */
    private Map<String, Set<String>> filters;
    /** Contains annotations for accepted motifs */
    private Map<String, Map<String, String>> acceptedMotifs;
    /** Pattern for checking annotation file line format */
    private Pattern annotationPattern;
    /** URL of flat file database (JASPAR) */
    private String databaseUrl;
    /** Path for outputting motif matrices */
    private File outputPath;
    /** File for outputting motif annotations */
    private File annotationFile;
    /** Command file for access by methods */
    private CommandFile comf;

    protected ErrorCode runImpl(CommandFile cf) throws Exception {

        this.comf = cf;

        /* Get parameters and input and output files */
        this.databaseUrl = cf.getParameter(PARAMETER_FLAT_FILE_URL);
        String idType = cf.getParameter(PARAMETER_ID_TYPE);
        String species = cf.getParameter(PARAMETER_SPECIES);
        String classes = cf.getParameter(PARAMETER_CLASS);
        String collection = cf.getParameter(PARAMETER_COLLECTION);
        File idFile = cf.getInput(INPUT_IDs);
        this.outputPath = cf.getOutput(OUTPUT_MOTIFS);
        this.annotationFile = cf.getOutput(OUTPUT_ANNOTATIONS);

        /* Put filtering options into a hash map */
        filters = new HashMap<String, Set<String>>();
        /* Check that species values are either numbers or a dash ('-') */
        if (species != null && species.length() > 0) {
            String[] speciesArray = species.split(",");
            for (String s : speciesArray) {
                try {
                    Integer.parseInt(s);
                } catch (NumberFormatException e) {
                    if (!s.equals(COMPONENT_PSEUDO_SPECIES_ID)) {
                        comf.writeError("Incorrect species ID " + s
                            + "! Valid species IDs are NCBI taxonomy IDs (integers) or a dash (\"-\").");
                        return ErrorCode.PARAMETER_ERROR;
                    }
                }
            }
            Set<String> speciesSet = new HashSet<String>(Arrays.asList(speciesArray));
            /* Annotation file has different representation for pseudo species */
            if(speciesSet.remove(COMPONENT_PSEUDO_SPECIES_ID))
                speciesSet.add(DATABASE_PSEUDO_SPECIES_ID);
            filters.put("species", speciesSet);
        }
        /* Class filter */
        if (classes != null && classes.length() > 0)
            filters.put("class", new HashSet<String>(Arrays.asList(classes.split(","))));
        /* Collection filter */
        if (collection != null && collection.length() > 0)
            filters.put("collection", new HashSet<String>(Arrays.asList(collection
                    .toUpperCase().split(","))));
        /* ID file filter */
        if (idFile != null) {
            if (idType == null || idType.length() == 0) {
                comf.writeError("No " + PARAMETER_ID_TYPE + " specified! The parameter "
                        + PARAMETER_ID_TYPE + "must be specified if the input " + INPUT_IDs
                        + "is present.");
                return ErrorCode.PARAMETER_ERROR;
            }
            try {
                filters.put(idType.toLowerCase(), new HashSet<String>(this.readInputList(idFile)));
            } catch (IOException e) {
                comf.writeError("Could not read input " + INPUT_IDs + " :");
                comf.writeError(e.getMessage());
                return ErrorCode.INPUT_IO_ERROR;
            }
        }

        /* Store info of accepted motifs in a (sorted) tree map */
        acceptedMotifs = new TreeMap<String, Map<String, String>>();

        /* Open connection to annotation file */
        URL address = new URL(new URL(databaseUrl), DATABASE_ANNOTATION_FILE_NAME);
        URLConnection con;
        BufferedReader in;
        try {
            con = address.openConnection();
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        } catch (MalformedURLException e) {
            comf.writeError("Malformed URL in parameter " + PARAMETER_FLAT_FILE_URL + ":");
            comf.writeError(e.getMessage());
            return ErrorCode.PARAMETER_ERROR;
        } catch (IOException e) {
            comf.writeError("Error connecting to database annotation file at: "+address);
            comf.writeError(e.getMessage());
            comf.writeError("Database URL is possibly incorrect.");
            return ErrorCode.ERROR;
        }

        /* Parse flat file */
        cf.writeLog("Parsing motif annotations and filtering.");
        this.annotationPattern = Pattern.compile(DATABASE_ANNOTATION_FILE_FORMAT);
        String line;
        while ((line = in.readLine()) != null) {
            this.parseMotifAnnotationLine(line.trim());
        }
        in.close();

        /* Get motif matrices */
        cf.writeLog("Fetching and writing motif matrices.");
        try {
            fetchAndWriteMotifMatrices();
        } catch (IOException e) {
            return ErrorCode.OUTPUT_IO_ERROR;
        }

        /* Write motif annotations */
        cf.writeLog("Writing motif annotations.");
        try {
            this.writeMotifAnnotations();
        } catch (IOException e) {
            comf.writeError("Error writing annotation file.");
            return ErrorCode.OUTPUT_IO_ERROR;
        }

        /* All done */
        cf.writeLog(acceptedMotifs.keySet().size() + " motifs written.");
        return ErrorCode.OK;

    }

    /**
     * Private method for reading IDs from (the first column of) a
     * <code>File</code> into a <code>List</code> of <code>String</code>s.
     * Assumes a header line and skips it! (Uses CSVReader.)
     * 
     * @param inFile a <code>File</code> to read.
     * @return a <code>List&lt;String&gt;</code> that has the lines from the
     *         file inFile, or <code>null</code> if inFile is null or the file
     *         inFile is empty or has no proper id lines.
     * @throws <code>IOException</code> if an I/O exception occurs when reading
     *         inFile.
     */
    private List<String> readInputList(File inFile) throws IOException {

        if (inFile == null)
            return null;

        /* Open parser and initialize list */
        CSVParser parser = new CSVParser(inFile);
        List<String> ls = new ArrayList<String>();

        /* Parse IDList file */
        String[] ln;
        while (parser.hasNext()) {
            ln = parser.next();
            /* Append first value from line if it is proper */
            if ((ln.length > 0) && (ln[0] != null) && !ln[0].isEmpty()) {
                ls.add(ln[0]);
            }
        }
        return ls.isEmpty() ? null : ls;
    }

    /**
     * Parses a line (one motif) from a flat file motif annotation file. If the
     * motif fills the filtering criteria, it is added to the list of accepted
     * motifs and <code>true</code> is returned. Else returns <code>false</code>
     * .
     * <p>
     * The line is assumed to have the following format:
     * id.version|infoContent|name|class|; tag1 "value1" ; tag2 "value2" ; ... ;
     * tagN "valueN" ; where the | represent tab characters and there can be any
     * number of tags.
     * 
     * @param line Line (<code>String</code>) from annotation file to parse.
     * @return <code>true</code> if parsed motif info fills filtering criteria
     *         and was added to accepted motifs list, <code>false</code>
     *         otherwise.
     * @throws IOException if there is a I/O error when writing to component
     *         log.
     */
    public boolean parseMotifAnnotationLine(String line) throws IOException {
        Map<String, String> motifAnnot = new HashMap<String, String>();

        /* Check if annotation line has presumed format */
        if (!annotationPattern.matcher(line).matches()) {
            comf.writeLog("WARNING: Malformatted line in database annotation file. Line:");
            comf.writeLog(line);
            return false;
        }

        String[] entries = line.split("\t");

        /* Parse annotations for this motif and test if they match filters */
        /* Entries 0-3 should be id.version, totalInfo, name, class */
        String motifId = entries[0].substring(0, entries[0].lastIndexOf('.')).trim(); // id
        if (filters.containsKey("id") && !filters.get("id").contains(motifId)) {
            return false;
        }
        motifAnnot.put("id", motifId);
        String motifVersion = entries[0].substring(entries[0].lastIndexOf('.') + 1); // version
        motifAnnot.put("version", motifVersion);
        String motifInfoContent = entries[1]; // total info content
        motifAnnot.put("totalInformation", motifInfoContent);
        String motifName = entries[2].trim(); // name
        motifName = motifName.replace('/','-');
        if (filters.containsKey("name") && !filters.get("name").contains(motifName)) {
            return false;
        }
        motifAnnot.put("name", motifName);
        String motifClass = entries[3]; // class
        if (filters.containsKey("class") && !filters.get("class").contains(motifClass)) {
            return false;
        }
        motifAnnot.put("class", motifClass);

        /* Entry 4 should have tags like: ; tagName "tagValue" ; tagName "tagValue" ... */
        String[] tags = entries[4].split("\\s?;\\s?");
        for (String tagPair : tags) {
            String[] tag = tagPair.split("\\s?\"");
            /* Special prehandling for species (could be empty or list) */
            boolean speciesListMatch = false;
            if (tag[0].equals("species")) {
                if (tag.length == 1 || tag[1].isEmpty()) {
                    tag = new String[2];
                    tag[0] = "species";
                    tag[1] = DATABASE_PSEUDO_SPECIES_ID;
                } else if(tag[1].contains(",")) {
                    for (String s : tag[1].split(",")) {
                        if (filters.containsKey("species") && filters.get("species").contains(s))
                            speciesListMatch = true;
                    }
                }
            }
            if (tag.length == 2) {
                if (filters.containsKey(tag[0]) && !filters.get(tag[0]).contains(tag[1]) && !speciesListMatch)
                    return false;
                motifAnnot.put(tag[0], tag[1]);
            }
        }

        /* Check if species or collection tag was required and was present */
        if (filters.containsKey("species") && !motifAnnot.containsKey("species")) {
            return false;
        }
        if (filters.containsKey("collection") && !motifAnnot.containsKey("collection")) {
            return false;
        }

        /*
         * Check if another motif has the same name (keep only first added) or a
         * different version of this motif has already been accepted (keep
         * latest version).
         */
        if (acceptedMotifs.containsKey(motifName)) {
            if (!acceptedMotifs.get(motifName).get("id").equals(motifId)) {
                comf.writeLog("WARNING: Two motifs with matching names but different ids ("
                        + acceptedMotifs.get(motifName).get("id") + " and " + motifId + "). "
                        + motifId + " was excluded.");
                return false;
            }
            if (acceptedMotifs.get(motifName).get("version").compareTo(motifVersion) > 0) {
                if (acceptedMotifs.get(motifName).get("version").compareTo(motifVersion) == 0)
                    comf.writeLog("WARNING: Duplicate entries of motif " + motifId + " version "
                            + motifVersion + ".");
                return false;
            }
        }
        acceptedMotifs.put(motifName, motifAnnot);
        return true;
    }

    /**
     * Fetches motifs that have been accepted and writes them to output path.
     * 
     * @throws IOException if an I/O exception occurs when attempting to fetch a
     *         motif.
     */
    public void fetchAndWriteMotifMatrices() throws IOException {

        /* Check if output directory exists */
        if (!outputPath.exists())
            outputPath.mkdir();

        for (Map<String, String> motif : acceptedMotifs.values()) {

            /* Construct filename for motif */
            String motifFilename = motif.get("id") + DATABASE_MOTIF_FILE_VERSION_SEPARATOR
                    + motif.get("version") + DATABASE_MOTIF_FILE_EXTENSION;

            /* Open URL connection */
            URLConnection con;
            try {
                con = new URL(databaseUrl + "/" + motifFilename).openConnection();
            } catch (IOException e) {
                /*
                 * This matrix does not seem to exist in the database, skip this
                 * motif
                 */
                comf.writeLog("WARNING: Could not read motif file " + motifFilename
                        + " from database (" + databaseUrl + "). Motif " + motif.get("id") + " "
                        + motif.get("name") + " removed from results.");
                acceptedMotifs.remove(motif.get("name"));
                continue;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

            /* Parse file, should have 4 lines corresponding to 4 bases */
            String line = in.readLine();
            if (line == null) {
                /* File didn't have 4 lines, skip this motif */
                comf.writeLog("WARNING: Malformed motif file at " + databaseUrl + "/"
                        + motifFilename + ". Motif " + motif.get("id") + " " + motif.get("name")
                        + " removed from results.");
                in.close();
                acceptedMotifs.remove(motif.get("name"));
                continue;
            }
            int[][] pfm = new int[4][line.trim().split("\\s+").length];
            for (int i = 0; i < 4; i++) {
                if (line == null) {
                    /* File didn't have 4 lines, skip this motif */
                    comf.writeLog("WARNING: Malformed motif file at " + databaseUrl + "/"
                            + motifFilename + ". Motif " + motif.get("id") + " "
                            + motif.get("name") + " removed from results.");
                    in.close();
                    acceptedMotifs.remove(motif.get("name"));
                    continue;
                }
                String[] freqs = line.trim().split("\\s+");
                for (int j = 0; j < freqs.length; j++)
                    pfm[i][j] = Integer.valueOf(freqs[j]);
                line = in.readLine();
            }
            in.close();

            /* Write matrix */
            try {
                PrintWriter out = new PrintWriter(new FileWriter(new File(outputPath, motif
                        .get("name")
                        + ".motif")));
                for (int i = 0; i < MOTIF_BASES.length; i++) {
                    out.print(MOTIF_BASES[i]);
                    if (i < 3)
                        out.print(MOTIF_ELEMENT_SEPARATOR);
                }
                out.println();
                for (int j = 0; j < pfm[0].length; j++) {
                    for (int i = 0; i < pfm.length; i++) {
                        out.print(pfm[i][j]);
                        if (i < 3)
                            out.print(MOTIF_ELEMENT_SEPARATOR);
                    }
                    out.println();
                }
                out.close();
            } catch (IOException e) {
                comf.writeError("Could not write motif matrix " + motif.get("name") + ".motif");
                throw e;
            }
        }
    }

    /**
     * Writes motif annotations for accepted motifs into output annotation file.
     * 
     * @throws IOException if an I/O exception occurs when attempting to write
     *         to annotation file.
     */
    public void writeMotifAnnotations() throws IOException {
        CSVWriter writer = new CSVWriter(OUTPUT_ANNOTATION_COLUMNS, this.annotationFile);
        for (Map<String, String> motif : acceptedMotifs.values()) {
            for (String annot : OUTPUT_ANNOTATION_COLUMNS) {
                if (motif.containsKey(annot))
                    writer.write(motif.get(annot));
                else
                    writer.skip();
            }
        }
        writer.close();
    }

    /**
     * Main method for running component.
     * 
     * @param args NOT USED!
     */
    public static void main(String[] args) {
        new JASPARMotif().run(args);
    }
}
