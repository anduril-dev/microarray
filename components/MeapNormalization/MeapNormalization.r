library(componentSkeleton)
library(meap)

execute <- function(cf){

	affyDir <- get.input(cf, 'affy')
        sampleNames.dir <- get.input(cf, 'sampleNames')
	annotation <- get.parameter(cf,'annotation')
	metaData <- get.parameter(cf,'metaData')
        if (identical(metaData,'')) {
            metaData <- get.temp.dir(cf)
        }
        nprocess <- get.parameter(cf,'NProcess',type="int")
	array.type <- get.parameter(cf,'arrayType','string')

	# Check empty files
        if(length(list.files(affyDir))==0){
		out <- data.frame(File=c())
		CSV.write(get.output(cf,"normalized"),out)
		return(0)
	}

	# Change execution directory
        cur.dir <- tempdir()
        setwd(cur.dir)
        write.log(cf,paste("Current working directory is set to ",cur.dir,sep=""))

	write.log(cf,"Background correction is running ...")
        bg.obj <- meap.background.correct(affyDir,sampleNames.dir,metaData,annotation,
                                          array.type=array.type,interval=0.1,probe.seq.length=25,
                                          nprocess)

        write.log(cf,"Normalization is running ...")
        norm.obj <- meap.quantile.normalize(bg.obj, metaData, nprocess)

	out.table <- data.frame(FilePath=as.vector(norm.obj))
	write.table(out.table,get.output(cf,"normalized"),sep="\t",row.names=FALSE)
}

main(execute)
	
