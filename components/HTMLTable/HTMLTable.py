from anduril.args import *
from anduril.args import cf
#from anduril.converters import tojson as j
from anduril.converters import tohtml as h
import os


pager_div='''<div id="pager" class="pager">
	<form>
		<span class="first">[&lt;</span>
		<span class="prev">&lt;</span>
		<input type="text" class="pagedisplay"/>
		<span class="next">&gt;</span>
		<span class="last">&gt;]</span>
		<select class="pagesize">
			<option selected="selected" value="50">50</option>
			<option value="100">100</option>
			<option  value="500">500</option>
			<option  value="1000">1000</option>
		</select>
	</form>
</div>
'''

stylefile='style.css'
if style!=None:
    stylefile=style

header_start=open('header_start.htmls','rU').read()
header_end=open('jquery-1.9.1.min.js','rU').read()+\
    open('jquery.tablesorter.min.js','rU').read()+\
    open('header_end.htmls','rU').read()
for param in cf.get_parameter_list():
    header_start=header_start.replace('@'+param.upper()+'@',cf.get_parameter(param))
    header_end=header_end.replace('@'+param.upper()+'@',cf.get_parameter(param))

header_start=header_start.replace('@STYLE@',open(stylefile,'rU').read())

outfile=open(table,'w')
outfile.write(header_start)
outfile.write(header_end)

h.from_csv(data,output=outfile,t_id="HTMLTable",t_class="tablesorter")

footer=open('footer.htmls','rU').read()
pager_code=""
if pager:
    pager_code='.tablesorterPager({container: $("#pager")})'
    outfile.write(pager_div)

footer=footer.replace('@pager_code@',pager_code)

outfile.write(footer)
outfile.close()
