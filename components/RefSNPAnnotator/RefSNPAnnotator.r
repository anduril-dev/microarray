# RefSNPAnnotator.r
# Anduril component for getting annotations for SNP rs ID's with biomaRt.
# Version 1.1.3
# Author Viljami Aittomaki, Marko Laakso

library(componentSkeleton)
library(biomaRt)

# Function for writing empty output
writeEmptyOut <- function(annotations.path, peptide.path,
                          annotations.attributes, peptide.attributes)
{
    emptyMatrix           = matrix(nrow=0, ncol=length(annotations.attributes))
    colnames(emptyMatrix) = annotations.attributes
    CSV.write(annotations.path, emptyMatrix)
    emptyMatrix           = matrix(nrow=0, ncol=length(peptide.attributes))
    colnames(emptyMatrix) = peptide.attributes
    CSV.write(peptide.path, emptyMatrix)
}

# Function for writing databases Properties file
writeDatabases <- function(databases.path, martname, dataset)
{
    martList = listMarts()
    ensemblVersion = as.character(martList[martList['biomart']=='ensembl','version'])
    snpVersion = as.character(martList[martList['biomart']=='snp','version'])
    datasetList = listDatasets(useMart(martname))
    datasetVersion = datasetList[datasetList['dataset']==dataset,'description']
    keys = c('ensembl', 'snp', 'dataset')
    values = c(ensemblVersion, snpVersion, datasetVersion)
    cat(paste(keys, values, sep='=', collapse='\n'), file=databases.path)
}

# Function for creating log message for lost SNP ID's
lostIDsMessage <- function(annotations.all, idlist)
{
     lostIDs = character(0)
     for(id in idlist) {
        if(!any(grep(id,annotations.all$refsnp_id)))
            lostIDs[length(lostIDs)+1] = id
    }
    logMessage = paste(lostIDs, collapse="\n")
    logMessage = paste("Following rsID's were not recognized and no annotation was returned:", logMessage, sep="\n")
    logMessage
}

# Function for removing annotations with strange chromosome names
removeStrangeChromosomes <- function(annotations.all)
{
    acceptedChromosomes <- c(1:22, 'X', 'Y', 'MT')
    keep = (annotations.all$chr_name %in% acceptedChromosomes)
    annotations.all = annotations.all[keep,]
    annotations.all
}

# Function for collapsing multiple annotation lines per rsID into single line
collapseMultipleAnnotations <- function(annotations.all, annotations.attributes)
{
    annotations = annotations.all[,annotations.attributes]
    if(nrow(annotations) > 0)
        annotations = aggregate(. ~ refsnp_id, data=annotations,
                                function(x) paste(sort(unique(x)), collapse=','))
    annotations
}


# Main program
execute <- function(cf)
{
    # Set internal parameters
    martname               = "snp"
    annotations.attributes = c("refsnp_id", "allele", "chr_name", "chrom_start",
                                "chrom_strand", "ensembl_gene_stable_id", "validated")
    peptide.attributes     = c("refsnp_id", "ensembl_gene_stable_id",
                                "ensembl_peptide_allele","consequence_type_tv","consequence_allele_string")
    usedFilters            = c("snp_filter")

    # Get input and output files and parameters
    annotations.path = get.output(cf, 'annotations')
    peptide.path     = get.output(cf, 'geneChanges')
    databases.path   = get.output(cf, 'databases')
    idcolumn         = get.parameter(cf, 'keyColumn')
    dataset          = get.parameter(cf, 'dataset')
    idlist.path      = get.input(cf, 'rsIDs')
    idlist.frame     = CSV.read(idlist.path)

    # If ID list was empty, write an empty files (with proper headers) and return
    if(nrow(idlist.frame) < 1) {
        writeEmptyOut(annotations.path, peptide.path, annotations.attributes, peptide.attributes)
        writeDatabases(databases.path, martname, dataset)
        return(0)
    }

    # Get list of SNP ID's from correct column of input
    if(nchar(idcolumn) < 1) {
        idlist = as.character(idlist.frame[[1]])
    } else {
        # If keyColumn isn't found in input list, report error and return
        if(!any(match(idcolumn,names(idlist.frame), nomatch=0))) {
            write.error(cf, paste("Key column", idcolumn, "not found in input rsIDs."))
            return(PARAMETER_ERROR)
        }
        idlist = as.character(idlist.frame[[idcolumn]])
    }


    # Open SNP mart
    snpmart = try(useMart(martname, dataset=dataset))
    if (class(snpmart) == 'try-error') {
        write.error(cf, paste("Invalid parameter value: no such dataset", dataset))
        return(PARAMETER_ERROR)
    }

    # Get annotations. Very long lists of id's must be split
    # into several queries of 50000 id's, so we don't hit biomart query limit
    for(i in 1:(length(idlist)/50000)) {
        if(i == length(idlist)/50000) {
            indices <- ((i-1)*50000+1):(length(idlist))
        } else {
            indices <- ((i-1)*50000+1):(i*50000)
        }


        annotations.all = getBM(unique(c(annotations.attributes, peptide.attributes)),
                    filters=usedFilters,
                    values =idlist[indices],
                    mart   =snpmart)


        # Log all ID's that returned NULL (id not recognized by mart query)
        write.log(cf, lostIDsMessage(annotations.all, idlist))

        # Remove annotations with strange chromosome names
        #annotations.all = removeStrangeChromosomes(annotations.all)

        # Only unique ID's for general annotations (multiple annotation values as comma sep. lists)
        annotations.unique  = collapseMultipleAnnotations(annotations.all, annotations.attributes)
        # Peptide shift annotation for SNPs causing a peptide shift
        inds.peptide        = which(annotations.all[["ensembl_peptide_allele"]] != "")
        annotations.peptide = annotations.all[inds.peptide, peptide.attributes]

        # Write output for current subset of ID's
        if(i == 1) {
            CSV.write(annotations.path, annotations.unique, append=F)
            CSV.write(peptide.path, annotations.peptide, append=F)
        } else {
            CSV.write(annotations.path, annotations.unique, append=T)
            CSV.write(peptide.path, annotations.peptide, append=T)
        }
    }
    # Write databases file
    writeDatabases(databases.path, martname, dataset)
    return(0)
}

main(execute)

