#!usr/bin/perl

# Set ensembl API environment variable

BEGIN{
	$ENV{'PERL5LIB'} = "/opt/ensembl_API_58/src/bioperl-1.2.3:/opt/ensembl_API_58/src/ensembl/modules:/opt/ensembl_API_58/src/ensembl-compara/modules:/opt/ensembl_API_58/src/ensembl-functgenomics/modules:/opt/ensembl_API_58/src/ensembl-variation/modules:/opt/pipeline/perl/componentSkeleton";
}

use lib split(':',$ENV{'PERL5LIB'});
use strict;
use Bio::EnsEMBL::Exon;
use Bio::EnsEMBL::Registry;
use Bio::EnsEMBL::Transcript;
use DBI;
use componentSkeleton;

sub execute {
    my ($cf_ref) = @_;
    my $eListPath = get_input($cf_ref,"ExonList");
    my $dbConnectPath = get_input($cf_ref,"connection");
    my $outputPath_exonSeq = get_output($cf_ref,"ExonSeq");
    my $outputPath_peptideSeq = get_output($cf_ref,"PeptideSeq");
    my $seqType = get_parameter($cf_ref,"SeqType");
    my $remove = get_parameter($cf_ref,"remove");
    my $ExonColumn = get_parameter($cf_ref,"ExonColumn");
    my $version = get_parameter($cf_ref, "version");

    write_log($cf_ref,"Reading exons from input file ...");
    my @tmp = read_table($eListPath,"\t");
    my @eList = FetchColumnContent(\@tmp,$ExonColumn,1);
    
    write_log($cf_ref,"Connecting to database ...");
    my %dbConnect = read_property($dbConnectPath);
    my $db_ad;
    if($dbConnect{port} eq ""){
    	$db_ad = Bio::EnsEMBL::DBSQL::DBAdaptor->new(-user   => $dbConnect{user},
						    -dbname => $dbConnect{database},
						    -host   => $dbConnect{host},
						    -driver =>$dbConnect{driver});
    }else{
	$db_ad = Bio::EnsEMBL::DBSQL::DBAdaptor->new(-user   => $dbConnect{user},
						     -dbname => $dbConnect{database},
						     -host   => $dbConnect{host},
						     -port   => $dbConnect{port},							
						     -driver =>$dbConnect{driver});
    }
    my $exon_ad = $db_ad->get_ExonAdaptor();

    write_log($cf_ref,"Fetching sequences ...");
    if($seqType eq "exon-only"){
	my $len = @eList;
	open(FILE_OUT,">".$outputPath_exonSeq);
	for(my $i = 0; $i < $len; $i++){
		my $exon = $exon_ad->fetch_by_stable_id($eList[$i]);
		if(!defined $exon){
			next;
		}
		my $seq = $exon->seq->seq();
		if($remove eq "true"){
			if(length($seq)==0){next;}	
		}
		print FILE_OUT ">".$eList[$i]."\n".$seq."\n";
	}
	close(FILE_OUT);
	open(FILE_OUT_PEP,">".$outputPath_peptideSeq);
	print FILE_OUT_PEP "Parameter SeqType was set as ".$seqType;
	close(FILE_OUT_PEP);
    }else{
	my $dbh = DBI->connect("DBI:mysql:$dbConnect{database}:$dbConnect{host}:$dbConnect{port}",$dbConnect{user},$dbConnect{password}) or die "Counld not connect to the database" . DBI->errstr;
	my $statement;
        if($version<67){
            $statement = "SELECT exon.exon_id, exon_stable_id.stable_id, transcript.transcript_id, transcript_stable_id.stable_id
	    	             FROM exon
			     JOIN exon_stable_id on exon.exon_id = exon_stable_id.exon_id
		             JOIN exon_transcript on exon.exon_id = exon_transcript.exon_id
		             JOIN transcript on exon_transcript.transcript_id = transcript.transcript_id
		             JOIN transcript_stable_id on transcript_stable_id.transcript_id = transcript.transcript_id
		             WHERE exon_stable_id.stable_id in ('";
       }else{
            $statement = "SELECT exon.exon_id, exon.stable_id, transcript.transcript_id, transcript.stable_id
                             FROM exon
                             JOIN exon_transcript on exon.exon_id = exon_transcript.exon_id
                             JOIN transcript on exon_transcript.transcript_id = transcript.transcript_id
                             WHERE exon.stable_id in ('";
       }
	my $str_eList = join("','", @eList);
	$statement = $statement . $str_eList . "')";
	my $sth = $dbh->prepare($statement) or die "Counldn't prepare $statement : $dbh->errstr\n";
	$sth->execute() or die "Couldn't execute statement : " . $sth->errstr;
	my @ref_tr_exon;
	my %id_link;
	my $ref;
	while($ref = $sth->fetchrow_arrayref){
		push @ref_tr_exon, $ref;
		$id_link{$ref->[1]} = $ref->[3];
	}

	my $transcript_adaptor = $db_ad->get_TranscriptAdaptor();
	my $len = @eList;

	open(FILE_OUT_PEP,">".$outputPath_peptideSeq);
	open(FILE_OUT_EXON,">".$outputPath_exonSeq);
	for(my $i = 0; $i < $len; $i++){
		my $exon = $exon_ad->fetch_by_stable_id($eList[$i]);
		if(!defined $exon){
			next;
		}
		if($seqType eq "both"){
			my $seq = $exon->seq->seq();
			if($remove eq "true"){
				if(length($seq)==0){next;}	
			}
			print FILE_OUT_EXON ">".$eList[$i]."\n".$seq."\n";
		}
		
		my $transcript = $transcript_adaptor->fetch_by_stable_id($id_link{$eList[$i]});
		if(!defined $transcript){
			next;
		}
		my $pep = $exon->peptide($transcript)->seq();
		if($remove eq "true"){
			if(length($pep)==0){next;}	
		}
		print FILE_OUT_PEP ">".$eList[$i]."\n".$pep."\n";
	}
	if($seqType eq "peptide-only"){
		print FILE_OUT_EXON "Parameter SeqType was set as ".$seqType;
	}
	close(FILE_OUT_EXON);
	close(FILE_OUT_PEP);
    }
}

if(scalar @ARGV == 0){
    print "NO_COMMAND_FILE";exit;
}
my %cf = parse_command_file($ARGV[0]);
execute(\%cf);
