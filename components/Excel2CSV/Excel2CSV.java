import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class Excel2CSV extends SkeletonComponent {
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
    	boolean          isXLS     = cf.getBooleanParameter("xls");
        CSVWriter        writer    = null;
        Sheet            curSheet;
        Workbook         wb;

        if (isXLS) {
        	wb = new HSSFWorkbook(new FileInputStream(cf.getInput("excelFile")));
        } else {
        	wb = new XSSFWorkbook(new FileInputStream(cf.getInput("excelFile")));
        }
        FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

        String  sheet   = cf.getParameter("sheet");
        boolean skip    = cf.getBooleanParameter("skipBadRows");
        boolean skipped = false;
        if (sheet.isEmpty()) {
            curSheet = wb.getSheetAt(0);
        } else {
            curSheet = wb.getSheet(sheet);
        }

        int colRange = 0;
        try {
            for (Row row : curSheet){
                if (colRange != 0 && row.getLastCellNum() != colRange) {
                    System.out.println("Skipped row "+row.getRowNum()+". " +
                    		           "Errors in spreadsheet: row has "+row.getLastCellNum()+" columns. " +
                    		           "Should have "+colRange+".");
                    skipped = true;
                    continue;
                } else {
                    colRange = row.getLastCellNum(); //Save first cell range
                }
                String[] outRow = new String[row.getLastCellNum()];
                for (Cell cell : row) {
                    String outCell = null;
                    if (cell != null){
                        if (cell.getCellType() == Cell.CELL_TYPE_FORMULA){
                            CellValue cellValue = evaluator.evaluate(cell);
                            if (cellValue.getCellType() == Cell.CELL_TYPE_NUMERIC){
                                outCell = Double.toString(cellValue.getNumberValue());
                            }
                        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
                            outCell = Double.toString(cell.getNumericCellValue());
                        } else if (cell.getCellType() == Cell.CELL_TYPE_STRING){
                            outCell = cell.getStringCellValue().trim();
                        }
                    }
                    outRow[cell.getColumnIndex()] = outCell;
                }
                if (row.getRowNum() > curSheet.getFirstRowNum()) {
                    writer.writeRow(outRow);
                } else {
                    writer = new CSVWriter(outRow, cf.getOutput("csv"), false);
                }
            }
        } finally {
            if (writer != null) writer.close();
        }

        if (skipped && !skip){
            return ErrorCode.INVALID_INPUT;
        } else {
            return ErrorCode.OK;
        }
    }

    public static void main(String[] args) {
        new Excel2CSV().run(args);
    }

}
