import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.nfunk.jep.JEP;
import org.nfunk.jep.Node;
import org.nfunk.jep.Variable;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class AgilentReader extends SkeletonComponent {

    final class Probe implements Comparable<Probe> {
        public static final int INITIAL_CAPACITY = 4;
        
        public final String probeName;
        public final int pos;
        public final ArrayDoubleList[] localValues;
        public final double[] commonValues;
        
        public Probe(String probeName, int pos) {
            this.probeName = probeName;
            this.pos = pos;
            this.localValues = new ArrayDoubleList[numMatrices];
            for (int i=0; i<numMatrices; i++) {
                this.localValues[i] = new ArrayDoubleList(INITIAL_CAPACITY);
            }
            
            final int N = numSamples*numMatrices;
            this.commonValues = new double[N];
            for (int i=0; i<N; i++) this.commonValues[i] = Double.NaN;
        }
        
        public int compareTo(Probe other) {
            final int oth = other.pos;
            if (pos < oth) return -1;
            else if (pos == oth) return 0;
            else return 1;
        }
        
        public void add(int matrixNum, double value) {
            this.localValues[matrixNum].add(value);
        }
        
        public void localToCommon(int sampleNum) {
            for (int matrixNum=0; matrixNum<numMatrices; matrixNum++) {
                final int pos = matrixNum*numSamples + sampleNum;
                final int size = this.localValues[matrixNum].size();
                if (size == 1) {
                    this.commonValues[pos] = this.localValues[matrixNum].get(0);
                } else if (size > 1) {
                    /* Take median */
                    double[] values = this.localValues[matrixNum].toArray();
                    this.commonValues[pos] = new Median().evaluate(values);
                }
                this.localValues[matrixNum].clear();                
            }
        }
        
        public boolean hasValues() {
            for (double d: this.commonValues) {
                if (!Double.isNaN(d)) return true;
            }
            return false;
        }
    }
    
    public static final int MAX_HEADER_LINES = 50;
    public static final int MAX_MATRICES = 4;

    public String idColumn;
    public String startPattern;
    public String[] channelColumns;
    public int[] channelOutputs;
    public String[] probeAnnColumns;
    public String[] sampleAnnTargetColumns;
    public String[] sampleAnnColumnsChannel1;
    public String[] sampleAnnColumnsChannel2;
    
    public boolean colNameMatch;
    
    private JEP filterImpl;
    private Map<String, Probe> probes;
    private int numMatrices;
    private int numSamples;
    private boolean combineProbes;
    
    
    public AgilentReader() {
        this.probes = new HashMap<String, Probe>();
    }
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        if (!readParameters(cf)) return ErrorCode.PARAMETER_ERROR;
        
        List<Sample> samples = readSampleDefinitions(cf);
        if (samples == null) return ErrorCode.INVALID_INPUT;
        this.numSamples = samples.size();

        String[] columns;
        if (this.combineProbes) {
            columns = new String[probeAnnColumns.length+1];
            columns[0] = idColumn;
            for (int i=0; i<probeAnnColumns.length; i++) {
                columns[i+1] = probeAnnColumns[i];
            }
        } else {
            columns = new String[probeAnnColumns.length+2];
            columns[0] = "InternalProbe";
            columns[1] = idColumn;
            for (int i=0; i<probeAnnColumns.length; i++) {
                columns[i+2] = probeAnnColumns[i];
            }
        }
        CSVWriter probeAnnWriter = new CSVWriter(columns, cf.getOutput("probeAnnotation"));
        
        columns = new String[sampleAnnTargetColumns.length+3];
        columns[0] = "SampleID";
        columns[1] = idColumn;
        columns[2] = "Index";
        for (int i=0; i<sampleAnnTargetColumns.length; i++) {
            columns[i+3] = sampleAnnTargetColumns[i];
        }
        CSVWriter sampleAnnWriter = new CSVWriter(columns, cf.getOutput("sampleAnnotation"));
        
        boolean ok = true;
        try {
            for (Sample sample: samples) {
                ok = ok && readArray(cf, sample, probeAnnWriter, sampleAnnWriter);
                if (!ok) break;
            }
        } finally {
            probeAnnWriter.close();
            sampleAnnWriter.close();
        }
        
        System.out.println("Writing matrices");
        writeMatrix(cf.getOutput("green"), 0, samples, true);
        writeMatrix(cf.getOutput("green2"), 1, samples, true);
        writeMatrix(cf.getOutput("red"), 2, samples, false);
        writeMatrix(cf.getOutput("red2"), 3, samples, false);
        writeGroups(cf.getOutput("groups"), samples);
        
        return ok ? ErrorCode.OK : ErrorCode.INVALID_INPUT;
    }
    
    private static String[] split(String s, String pattern) {
        if (s == null || s.isEmpty()) return new String[] {};
        else return s.split(pattern);
    }
    
    private boolean readParameters(CommandFile cf) {
        this.idColumn = cf.getParameter("idColumn");
        this.startPattern = cf.getParameter("startPattern");
        this.combineProbes = "true".equals(cf.getParameter("combineProbes"));
        
        this.colNameMatch = cf.getBooleanParameter("useColumnNameMatch");
        
        this.channelColumns = Arrays.copyOf(split(cf.getParameter("channelColumns"), ","),
                MAX_MATRICES);
        this.channelOutputs = new int[channelColumns.length];
        this.numMatrices = 0;
        for (int i=0; i<this.channelColumns.length; i++) {
            String col = this.channelColumns[i];
            if (col != null && !col.isEmpty()) {
                this.channelOutputs[i] = this.numMatrices;
                this.numMatrices++;
            } else {
                this.channelOutputs[i] = -1;
                this.channelColumns[i] = null;
            }
        }

        this.probeAnnColumns = split(cf.getParameter("probeAnnotation"), ",");
        this.sampleAnnTargetColumns = split(cf.getParameter("sampleAnnotation"), ",");
        this.sampleAnnColumnsChannel1 = split(cf.getParameter("sampleAnnotationChannel1"), ",");
        this.sampleAnnColumnsChannel2 = split(cf.getParameter("sampleAnnotationChannel2"), ",");
        
        if (this.sampleAnnColumnsChannel1.length > 0 &&
                this.sampleAnnTargetColumns.length != this.sampleAnnColumnsChannel1.length) {
            cf.writeError("sampleAnnotation and sampleAnnotationChannel1 must have same length");
            return false;
        }
        if (this.sampleAnnColumnsChannel2 != null &&
                this.sampleAnnColumnsChannel2.length > 0 &&
                this.sampleAnnTargetColumns.length != this.sampleAnnColumnsChannel2.length) {
            cf.writeError("sampleAnnotation and sampleAnnotationChannel2 must have same length");
            return false;
        }
        
        return true;
    }
    
    private List<Sample> readSampleDefinitions(CommandFile cf) throws IOException {
        CSVParser parser = new CSVParser(cf.getInput("sampleNames"));
        List<Sample> samples = new ArrayList<Sample>();
        
        try {
            int number = 0;
            for (String[] row: parser) {
                Sample sample;
                if (row.length == 3) {
                    sample = new Sample(number, row[0], row[1], null, null, row[2]);
                } else if (row.length == 5) {
                    sample = new Sample(number, row[0], row[1], row[2], row[3], row[4]);
                } else {
                    cf.writeError("Invalid number of columns in sampleNames");
                    return null;
                }
                
                samples.add(sample);
                File file = sample.getFile(cf);
                if (!file.exists()) {
                    cf.writeError("File does not exist: "+file.getAbsolutePath());
                    return null;
                }
                
                number++;
            }
        } finally {
            parser.close();
        }
        
        return samples;
    }
    
    private boolean findHeader(BufferedReader reader) throws IOException {
        int lineNumber = 0;
        Pattern pattern = Pattern.compile(startPattern);
        while (lineNumber < MAX_HEADER_LINES) {
            reader.mark(4096);
            String line = reader.readLine();
            lineNumber++;
            
            if (line == null) return false;
            else if (this.colNameMatch){
            	boolean found = true;
                for(String col : this.channelColumns){
                	if(col != null && line.indexOf(col) < 0){
                		found = false;
                		break;
                	}
                }
            	if(found){
            		reader.reset();
                    return true;
            	}
            	
            }else if (pattern.matcher(line).lookingAt()) {
            	reader.reset();
                return true;
            }
        }
        
        return false;
    }
    
    private boolean readArray(CommandFile cf, Sample sample, CSVWriter probeAnnWriter, CSVWriter sampleAnnWriter)
            throws IOException {
        File file = sample.getFile(cf);
        System.out.println("Reading "+file.getAbsolutePath());
        
        final BufferedReader reader = new BufferedReader(new FileReader(file));
        
        try {
            boolean ok = findHeader(reader);
            if (!ok) {
                cf.writeError("Could not find CSV header in "+sample.filename);
                return false;
            }
            final CsvListReader csvReader = new CsvListReader(reader, new CsvPreference('"', '\t', "\n"));
            
            List<String> header = new ArrayList<String>();
            for (String s: csvReader.getCSVHeader(false)) header.add(s);
            
            final int probeIndex = matchIndex(
                    idColumn, header, cf, sample, true);
            final int[] channelIndices = matchIndices(
                    channelColumns, header, cf, sample, true);
            final int[] probeAnnIndices = matchIndices(
                    probeAnnColumns, header, cf, sample, false);
            final int[] sampleAnnIndices1 = matchIndices(
                    sampleAnnColumnsChannel1, header, cf, sample, false);
            final int[] sampleAnnIndices2 = matchIndices(
                    sampleAnnColumnsChannel2, header, cf, sample, false);
            if (probeIndex < 0 || channelIndices == null || probeAnnIndices == null ||
                    sampleAnnIndices1 == null || sampleAnnIndices2 == null) {
                return false;
            }
            
            Map<Integer, String> filterMap = setupFilter(header, cf, sample);
            if (filterMap == null && this.filterImpl != null) return false;

            final double LOG2 = Math.log(2);

            List<String> row;
            while ((row = csvReader.read()) != null) {

            	final String realProbeName = row.get(probeIndex);
                final String probeName = this.combineProbes ? realProbeName :
                            String.format("Internal_%s_%d", realProbeName, csvReader.getLineNumber()-1);
                Probe probe = probes.get(probeName);
                
                if (probe == null) {
                    probe = new Probe(probeName, probes.size());
                    probes.put(probeName, probe);

                    /* Write probe annotation for the novel probe */
                    probeAnnWriter.write(probeName);
                    if (!this.combineProbes) probeAnnWriter.write(realProbeName);
                    for (int index: probeAnnIndices) {
                        if (index >= 0) probeAnnWriter.write(row.get(index));
                        else probeAnnWriter.write(null);
                    }
                }

                if (filterMap != null) {
                    for (Map.Entry<Integer, String> entry: filterMap.entrySet()) {
                        final String strValue = row.get(entry.getKey());
                        try {
                            final double doubleValue = Double.parseDouble(strValue);
                            this.filterImpl.setVarValue(entry.getValue(), doubleValue);
                        } catch (NumberFormatException e) {
                            this.filterImpl.setVarValue(entry.getValue(), strValue);
                        }
                    }
                    double result = this.filterImpl.getValue();
                    if (Math.abs(result) >= 0.5) continue; /* Skip row */
                }
                
                for (int i=0; i<channelIndices.length; i++) {
                    final int index = channelIndices[i];
                    if (index < 0) continue;
                    try {
                        final double value = Math.log(Double.parseDouble(row.get(index))) / LOG2;
                        probe.add(channelOutputs[i], value);
                    } catch (NumberFormatException e) {}
                }

                final int lineNumber = csvReader.getLineNumber() - 1;
                if (sampleAnnIndices1.length > 0) {
                    sampleAnnWriter.write(sample.greenID);
                    sampleAnnWriter.write(probeName);
                    sampleAnnWriter.write(lineNumber);
                    for (int index: sampleAnnIndices1) {
                        if (index >= 0) sampleAnnWriter.write(row.get(index));
                        else sampleAnnWriter.write(null);
                    }
                }
                if (sampleAnnIndices2.length > 0) {
                    sampleAnnWriter.write(sample.redID);
                    sampleAnnWriter.write(probeName);
                    sampleAnnWriter.write(lineNumber);
                    for (int index: sampleAnnIndices2) {
                        if (index >= 0) sampleAnnWriter.write(row.get(index));
                        else sampleAnnWriter.write(null);
                    }
                }
            }
            
            csvReader.close();

            final int sampleNum = sample.number;
            for (Probe probe: probes.values()) {
                probe.localToCommon(sampleNum);
            }
        }finally {
            reader.close();
        }
        
        return true;
    }
    
    private int[] matchIndices(String[] columnNames, List<String> header, CommandFile cf,
            Sample sample, boolean signalError) {
        int[] indices = new int[columnNames.length];
        for (int i=0; i<columnNames.length; i++) {
            final String name = columnNames[i];
            if (name == null || name.isEmpty()) {
                indices[i] = -1;
                continue;
            }
            final int index = header.indexOf(name);
            if (index < 0) {
                String msg = String.format("Column %s not found in %s",
                        columnNames[i], sample.filename);
                if (signalError) {
                    cf.writeError(msg);
                    return null;
                } else System.out.println("Warning: "+msg);
            }
            indices[i] = index;
        }
        return indices;
    }
    
    private int matchIndex(String columnName, List<String> header, CommandFile cf,
            Sample sample, boolean signalError) {
        int[] indices = matchIndices(new String[] {columnName}, header, cf,
                sample, signalError);
        if (indices == null || indices.length < 1) return -1;
        else return indices[0];
    }
    
    private Map<Integer, String> setupFilter(List<String> header, CommandFile cf, Sample sample) {
        String filter = cf.getParameter("filter");
        if (!filter.isEmpty()) {
            this.filterImpl = new JEP();
            this.filterImpl.setAllowUndeclared(true);
            Node node = this.filterImpl.parseExpression(filter);
            if (node == null) {
                cf.writeError("Invalid filter expression");
                return null;
            }
        } else {
            this.filterImpl = null;
            return null;
        }
        
        Map<Integer, String> varMap = new HashMap<Integer, String>();
        for (Object obj: this.filterImpl.getSymbolTable().values()) {
            Variable var = (Variable)obj;
            int index = header.indexOf(var.getName());
            if (index < 0) {
                cf.writeError(String.format("Column %s not found in %s",
                        var.getName(), sample.filename));
                return null;
            }
            varMap.put(index, var.getName());
        }
        
        return varMap;
    }
    
    private void writeMatrix(File output, int matrixIndex, List<Sample> samples, boolean channel1) throws IOException {
        if (channelOutputs[matrixIndex] < 0) {
            FileOutputStream out = new FileOutputStream(output);
            out.close();
            return;
        }
        
        String[] columns = new String[numSamples+1];
        columns[0] = idColumn;
        for (int i=0; i<samples.size(); i++) {
            Sample sample = samples.get(i);
            columns[i+1] = channel1 ? sample.greenID : sample.redID;
        }
        
        CSVWriter writer = new CSVWriter(columns, output);
        try {
            if (channelOutputs[matrixIndex] >= 0) {
                final int start = channelOutputs[matrixIndex] * numSamples;
                List<Probe> probeList = new ArrayList<Probe>(probes.values());
                Collections.sort(probeList);
                for (Probe probe: probeList) {
                    if (!probe.hasValues()) continue;
                    writer.write(probe.probeName);
                    for (int i=0; i<numSamples; i++) {
                        final double value = probe.commonValues[start+i];
                        if (Double.isNaN(value)) writer.write(null);
                        else writer.write(value);
                    }
                }
            }
        } finally {
            writer.close();
        }
    }
    
    private void writeGroups(File output, List<Sample> samples) throws IOException {
        String[] COLUMNS = {"ID", "Members", "Type", "Description"};
        CSVWriter writer = new CSVWriter(COLUMNS, output);
        try {
            for (Sample sample: samples) {
                if (sample.greenID != null && !sample.greenID.isEmpty()) {
                    writer.write(sample.greenID);
                    writer.write(sample.greenID);
                    writer.write("sample");
                    writer.write(sample.greenDesc);
                }
                if (sample.redID != null && !sample.redID.isEmpty()) {
                    writer.write(sample.redID);
                    writer.write(sample.redID);
                    writer.write("sample");
                    writer.write(sample.redDesc);
                }
            }
        } finally {
            writer.close();
        }
    }
    
    public static void main(String[] args) {
        new AgilentReader().run(args);
    }
}

class Sample {
    public int number;
    public String greenID;
    public String greenDesc;
    public String redID;
    public String redDesc;
    public String filename;
    public Sample(int number, String greenID, String greenDesc, String redID, String redDesc, String filename) {
        this.number = number;
        this.greenID = greenID;
        this.greenDesc = greenDesc;
        this.redID = redID;
        this.redDesc = redDesc;
        this.filename = filename;
    }
    
    public File getFile(CommandFile cf) {
        return new File(cf.getInput("agilent"), this.filename);
    }
}
