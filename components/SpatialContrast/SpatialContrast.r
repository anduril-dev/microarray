library(componentSkeleton)

get.spatial.matrix <- function(expr, col.index, probe.rows, probe.cols) {
    ids <- rownames(expr)
    rows <- probe.rows[ids]
    cols <- probe.cols[ids]
    matr <- matrix(NA, max(rows), max(cols))
    
    for (row.num in 1:nrow(expr)) {
        matr[rows[row.num], cols[row.num]] <- expr[row.num, col.index]
    }
    
    return(matr)
}

# Return list(low, high), where low and high are
# list(intensity, upperLeftX, upperLeftY, lowerRightX, lowerRightY)
find.squares <- function(spatial.matrix, min.area, margin) {
    side <- ceiling(sqrt(min.area))
    col.limit <- ncol(spatial.matrix) - side - margin
    row.limit <- nrow(spatial.matrix) - side - margin
    
    if (col.limit < 1 || row.limit < 1) return(list(low=NULL, high=NULL))
    
    low <- list(intensity=9999999, upperLeftX=NA, upperLeftY=NA, lowerRightX=NA, lowerRightY=NA)
    high <- list(intensity=-9999999, upperLeftX=NA, upperLeftY=NA, lowerRightX=NA, lowerRightY=NA)
    
    for (col.index in (1+margin):col.limit) {
        for (row.index in (1+margin):row.limit) {
            sub.matrix <- spatial.matrix[row.index:(row.index+side), col.index:(col.index+side)]
            intensity <- median(sub.matrix, na.rm=TRUE)
            
            if (intensity < low$intensity) {
                low <- list(intensity=intensity,
                    upperLeftX=col.index, upperLeftY=row.index,
                    lowerRightX=col.index+side-1, lowerRightY=row.index+side-1)
            }
            
            if (intensity > high$intensity) {
                high <- list(intensity=intensity,
                    upperLeftX=col.index, upperLeftY=row.index,
                    lowerRightX=col.index+side-1, lowerRightY=row.index+side-1)
            }
        }
    }
    
    return(list(low=low, high=high))
}

execute <- function(cf) {
    expr <- LogMatrix.read(get.input(cf, 'expr'))
    annotation <- AnnotationTable.read(get.input(cf, 'geneAnnotation'))
    region.area <- get.parameter(cf, 'regionArea', 'float')
    margin <- get.parameter(cf, 'margin', 'int')
    
    out.dir <- get.output(cf, 'report')
    dir.create(out.dir, recursive=TRUE)
    
    internal.ids <- rownames(expr)
    probe.rows <- AnnotationTable.get.vector(annotation, 'Row', internal.ids)
    probe.cols <- AnnotationTable.get.vector(annotation, 'Col', internal.ids)

    tex <- character(0)
    
    empty <- rep('', ncol(expr))
    fr <- data.frame(Sample=colnames(expr), ExpressionContrast=empty,
        LowPosition=empty, HighPosition=empty, stringsAsFactors=FALSE)
    rownames(fr) <- colnames(expr)

    for (i in 1:ncol(expr)) {
        spatial.matrix <- get.spatial.matrix(expr, i, probe.rows, probe.cols)
        squares <- find.squares(spatial.matrix, region.area, margin)
        low <- squares$low
        high <- squares$high
        if (is.null(low) || is.null(high)) next
        
        row.name <- colnames(expr)[i]
        fr[row.name, 'ExpressionContrast'] <- sprintf('%.2f - %.2f = %.2f',
            high$intensity, low$intensity, high$intensity-low$intensity)
        fr[row.name, 'LowPosition'] <- sprintf('(%d, %d) - (%d, %d)',
            low$upperLeftX, low$upperLeftY, low$lowerRightX, low$lowerRightY)
        fr[row.name, 'HighPosition'] <- sprintf('(%d, %d) - (%d, %d)',
            high$upperLeftX, high$upperLeftY, high$lowerRightX, high$lowerRightY)
    }
    
    caption <- 'Spatial regions with highest expression contrast.'
    tex <- c(tex,
        latex.table(fr, 'llll', caption=caption))
    
    latex.write.main(cf, 'report', tex)	
    return(0)
}

main(execute)
