library(componentSkeleton)
library(igraph)

execute <- function(cf) {
    nameAsID  <- get.parameter(cf, 'nameAsID', 'boolean')
    vcPrefix  <- get.parameter(cf, 'idPrefix')
    nameDelim <- get.parameter(cf, 'nameDelim')
    eeAttrs   <- trim(strsplit(get.parameter(cf, 'equalEAttr'), ',')[[1]])
    evAttrs   <- trim(strsplit(get.parameter(cf, 'equalVAttr'), ',')[[1]])
    labelAttr <- get.parameter(cf, 'nameAttr')
    joins     <- data.frame(ID               = character(0),
                            Members          = character(0),
                            nodesIn          = character(0),
                            stringsAsFactors = FALSE)
    graph     <- read.graph(get.input(cf, 'graph'), format='graphml')
    vNames    <- function(x) { get.vertex.attribute(graph, labelAttr, index=x) }
    vTypes    <- function(x) { sapply(evAttrs, function(y){get.vertex.attribute(graph, y, index=x)}) }
    eTypes    <- function(fromN,toN) {
                    sapply(eeAttrs,
                           function(y,t=toN,f=fromN) {
                              eAttr <- get.edge.attribute(graph, y, index=E(graph)[f %->% t])
                              if (is.null(eAttr))
                                 return(NULL)
                              else
                                 paste(sort(eAttr),sep=',')
                           })
                 }

    joined <- numeric()
    verts  <- V(graph)
    oSize  <- length(verts)
    vcNbr  <- 1
    for (n in sort(as.numeric(verts))) {
         inList  <- neighborhood(graph, 1, nodes=n, mode= "in")[[1]][-1]
         outList <- neighborhood(graph, 1, nodes=n, mode="out")[[1]][-1]

         #cat(vNames(n),'is between',vNames(inList),'and',vNames(outList),'\n')
         if (length(inList) < 1)
            cand <- neighborhood(graph, 1, nodes=outList, mode="in")
         else
            cand <- neighborhood(graph, 1, nodes=inList,  mode="out")
         cand  <- lapply(cand, function(x){x[-1]})
         cand  <- unique(unlist(cand))
         cand  <- setdiff(cand[cand>n], joined)
         jList <- numeric()

         equalEdges <- function(n1, n2) {
            for (f in inList) {
                if (!setequal(eTypes(f,n1),eTypes(f,n2))) return(FALSE)
            }
            for (t in outList) {
                if (!setequal(eTypes(n1,t),eTypes(n2,t))) return(FALSE)
            }
            return(TRUE)
         }
         #cat(' possible candidates are',vNames(cand),'\n')
         for (i in cand) {
             nIn  <- neighborhood(graph, 1, nodes=i, mode= "in")[[1]][-1]
             nOut <- neighborhood(graph, 1, nodes=i, mode="out")[[1]][-1]
             #cat(' check',vNames(i),'between',vNames(nIn),'and',vNames(nOut),'\n')
             if (setequal(   inList, nIn)       &&
                 setequal(  outList, nOut)      &&
                 setequal(vTypes(n), vTypes(i)) &&
                 equalEdges(n, i)) {
                #cat(' join',vNames(n),'to',vNames(i),'\n')
                joined <- append(joined, i)
                jList  <- append(jList,  i)
                for (param in setdiff(list.vertex.attributes(graph), c(evAttrs,labelAttr,'id'))) {
                    pnValue <- get.vertex.attribute(graph, param, index=n)
                    piValue <- get.vertex.attribute(graph, param, index=i)
                    if (!is.nan(pnValue) && (is.nan(piValue) || (pnValue != piValue))) {
                       if (is.numeric(pnValue))
                          graph <- set.vertex.attribute(graph, param, index=n, NaN)
                       else
                          graph <- set.vertex.attribute(graph, param, index=n, "")
                    }
                }
             }
         }
         if (length(jList) > 0) {
            memberList    <- c(n,jList)
            jList         <- vNames(memberList)
            nID           <- sprintf('%s%d', vcPrefix, vcNbr)
            nName         <- if (nameAsID) nID else paste(jList,collapse=nameDelim)
            graph         <- set.vertex.attribute(graph, labelAttr, index=verts[n], nName)
            joins[vcNbr,] <- c(nID,
                               paste(jList,collapse=','),
                               paste(get.vertex.attribute(graph,'id',index=verts[memberList]),collapse=','))
            vcNbr         <- vcNbr+1
         }
    }
    graph <- delete.vertices(graph, verts[joined])
    write.log(cf, sprintf('Reduced %d vertices to %d vertices.', oSize, oSize-length(joined)))

    # Remove special id attribute from vertices
    graph <- my.remove.vertex.attr(graph, 'id')
    
    # Bringing labels and IDs of complex verticies into canonical form for joins CSV
    if (nrow(joins) > 0)
    for(r in 1:nrow(joins)) {
	labels     <- unlist(strsplit(joins[r,2],','))
	IDs        <- unlist(strsplit(joins[r,3],','))
	sorted     <- order(labels)
	joins[r,2] <- paste(labels[sorted],collapse=',')
	joins[r,3] <- paste(IDs[sorted],collapse=',')
    }

    # Doubling backslashes in the delimeter
    nameDelim2=gsub("\\","\\\\",nameDelim,fixed=T)

    # Bringing labels of complex verticies into canonical form for GRAPHML
    verts  <- V(graph)
    for (n in sort(as.numeric(verts))) {
	label         <- get.vertex.attribute(graph, labelAttr, index=verts[n])
	sorted_label  <- paste(sort(unlist(strsplit(label,nameDelim2))), collapse=nameDelim)
	graph         <- set.vertex.attribute(graph, labelAttr, index=verts[n], sorted_label)
    }

    write.graph(graph, get.output(cf, 'graph'), format='graphml')
    CSV.write(get.output(cf, 'joins'), joins)
    return(0)
}

my.remove.vertex.attr <- function(graph, attr.name) {
    while (attr.name %in% list.vertex.attributes(graph)) {
        graph <- remove.vertex.attribute(graph, attr.name)
    }
    return(graph)
}

main(execute)
