<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>DEGReport</name>
    <version>1.1.1</version>
    <doc>
    Creates a LaTeX report on differentially expressed genes (DEGs).
    The report has two parts: a summary section that lists the number
    of over- and underexpressed genes in each sample group; and a gene list
    section that lists all the gene names. The latter usually goes
    to appendix.
    </doc>
    <author email="kristian.ovaska@helsinki.fi">Kristian Ovaska</author>
    <author email="Marko.Laakso@Helsinki.FI">Marko Laakso</author>
    <category>DEG</category>
    <category>Latex</category>
    <launcher type="R">
        <argument name="file" value="DEGReport.r" />
    </launcher>
    <requires URL="http://www.r-project.org/" type="manual">R</requires>
    <inputs>
        <input name="deg" type="SetList">
            <doc>Sets of differentially expressed genes. If expression
            values are given (expr), there must be an annotation column
            SampleGroup that binds the set to an expression column.</doc>
        </input>
        <input name="geneAnnotation" type="AnnotationTable">
            <doc>Annotation for genes. Must contain the column
            named by geneColumn.</doc>
        </input>
        <input name="expr" type="Matrix" optional="true">
            <doc>Expression values. Used to sort gene lists.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="summary" type="Latex">
            <doc>Summary section that lists the number of over-
            and underexpressed genes in each sample.</doc>
        </output>
        <output name="genelist" type="Latex">
            <doc>Gene list section that lists all the
            gene names.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="setPattern" type="string" default="">
        	<doc>R regular expression for matching set names.
        	If empty, all sets are included.</doc>
        </parameter>
        <parameter name="geneColumn" type="string" default="GeneName">
            <doc>Column name in geneAnnotation that contains gene
            names. If empty, gene names are not printed.</doc>
        </parameter>
            <parameter name="columns" type="int" default="4">
            <doc>Number of columns in gene list table.</doc>
        </parameter>
        <parameter name="sectionTitle" type="string" default="Summary of differentially expressed genes">
            <doc>Title for a new latex section. If none is given, a new section is not created.</doc>
        </parameter>
        <parameter name="sectionType" type="string" default="section">
            <doc>Type of LaTeX section: usually one of section, subsection or subsubsection. No section statement is written if sectionTitle is empty.</doc>
        </parameter>
    </parameters>
</component>
