library(componentSkeleton)

summary.report <- function(cf, idlist, section.title, section.type) {
    if(section.title != "") {
        tex <- sprintf('\\%s{%s}\\label{sec:%s-summary}',
                       section.type,
                       latex.quote(section.title),
                       get.metadata(cf, 'instanceName'))
    } else {
        tex <- character()
    }
    tex <- c(tex, '\\begin{tabular}{lrr}', 'Gene set & Size \\\\')

    for (i in 1:nrow(idlist)) {
        set.id <- idlist$ID[i]
        tex <- c(tex, sprintf('%s & %d \\\\', latex.quote(set.id), length(SetList.get.members(idlist, set.id))))
    }

    tex <- c(tex, '\\end{tabular}', '')

    out.dir <- get.output(cf, 'summary')
    if (!file.exists(out.dir)) dir.create(out.dir, recursive=TRUE)
    docfile <- file.path(out.dir, LATEX.DOCUMENT.FILE)
    textfile.write(docfile, paste(tex, collapse='\n'))
}

get.sorted.genes <- function(gene.ids, group.id, logratio, gene.annotation, gene.column) {
    gene.name          <- AnnotationTable.get.vector(gene.annotation, gene.column, gene.ids)
    no.name            <- is.na(gene.name)

    if (is.null(logratio) || is.null(group.id)) {        
        gene.name[no.name] <- gene.ids[no.name]
        gene.name         <- sort(gene.name, decreasing=F)
        gene.name          <- latex.quote(gene.name)
        gene.name[no.name] <- paste('\\textit{', gene.name[no.name], '}', sep='')
        return(gene.name)
    }else {
        gene.name          <- latex.quote(gene.name)
        gene.name[no.name] <- paste('\\textit{',latex.quote(gene.ids[no.name]),'}', sep='')
        this.logratio      <- logratio[,group.id]
        gene.logratio      <- this.logratio[gene.ids]
        fr                 <- data.frame(gene=gene.name, logratio=gene.logratio, stringsAsFactors=FALSE)
        min.value          <- min(Inf, gene.logratio, na.rm=TRUE)
        reverse            <- (is.finite(min.value) && min.value > 0)
        fr                 <- fr[order(fr$logratio, decreasing=reverse),]
        return(fr$gene)
    }
}

format.column.table <- function(values, columns=4) {
    tex <- c(
        '\\begin{footnotesize}',
        sprintf('\\begin{longtable}{%s}', paste(rep('l', columns), collapse=''))
    )

    rows <- ceiling(length(values) / columns)

    for (row.num in 1:rows) {
        indices                       <- seq(row.num, by=rows, length.out=columns)
        row.values                    <- values[indices]
        row.values[is.na(row.values)] <- ''
        string                        <- sprintf('%s \\\\', paste(row.values, collapse=' & '))
        tex                           <- c(tex, string)
    }

    tex <- c(tex,
        '\\end{longtable}',
        '\\end{footnotesize}')
    return(tex)
}

genelist.report <- function(cf, idlist, logratio, gene.annotation, gene.column, columns) {
    tex <- c('\\section{List of differentially expressed genes}',
             sprintf('\\label{sec:%s-list}', get.metadata(cf, 'instanceName')), '')

    if(!is.null(logratio)) {
        tex <- c(tex, 'Overexpressed genes are sorted with the most overexpressed first and',
                 'underexpressed genes with the most underexpressed first.',
                 'Genes go by the column first and then by row.', '')
    }else {
           tex <- c(tex, 'Genes are in alphabetical order.',  '')
    }

    for (i in 1:nrow(idlist)) {
        set.id <- idlist$ID[i]
        genes <- SetList.get.members(idlist, set.id)

        if (!is.null(logratio)) {
            group.id <- idlist[i, 'SampleGroup']
            if (is.na(group.id)) {
                write.error(cf,
                    sprintf('Set does not have SampleGroup annotation defined: %s', set.id))
                return()
            }
        }        
        genes <- get.sorted.genes(genes, group.id, logratio, gene.annotation, gene.column)
        
        tex   <- c(tex, sprintf('\\subsection{Gene set: %s}', latex.quote(set.id)))
        tex   <- c(tex, sprintf('Number of genes: %d', length(genes)), '')
        tex   <- c(tex, format.column.table(genes, columns))
    }

    out.dir <- get.output(cf, 'genelist')
    if (!file.exists(out.dir)) dir.create(out.dir, recursive=TRUE)
    docfile <- file.path(out.dir, LATEX.DOCUMENT.FILE)
    textfile.write(docfile, paste(tex, collapse='\n'))
}

execute <- function(cf) {
    if (input.defined(cf, 'expr')) {
        logratio <- Matrix.read(get.input(cf, 'expr'))
    } else {
        logratio <- NULL
    }
    
    idlist          <- SetList.read(get.input(cf, 'deg'))
    gene.annotation <- AnnotationTable.read(get.input(cf, 'geneAnnotation'))
    gene.column     <- get.parameter(cf, 'geneColumn')
    colCount        <- get.parameter(cf, 'columns', type='int');
    section.title   <- get.parameter(cf, 'sectionTitle')
    section.type    <- get.parameter(cf, 'sectionType')

    set.pattern     <- get.parameter(cf, 'setPattern')
    if (nchar(set.pattern) > 0) {
        idlist <- idlist[grep(set.pattern, SetList.get.ids(idlist)),]
    }
    
    if (input.defined(cf, 'expr') && !('SampleGroup' %in% colnames(idlist))) {
        write.error(cf, 'SetList does not have SampleGroup annotation column')
        return(INVALID_INPUT)
    }
    
    summary.report(cf, idlist, section.title, section.type)
    genelist.report(cf, idlist, logratio, gene.annotation, gene.column, colCount)
    
    if (has.errors())
       return(INVALID_INPUT)
    else
       return(0)
}

main(execute)
