#!/bin/bash

[ $UID -ne 0 ] && [ ! -d "$R_LIBS" ] && {
    echo Not run as root or no R_LIBS variable set.
    exit 
}

echo '
setwd("/tmp/")

testpackage <- function(x)
{
  if (!require(x,character.only = TRUE))
  {
    source("http://bioconductor.org/biocLite.R")
    biocLite(x)
  }
}
for (pkg in c("methods", "DBI", "preprocessCore", "sqldf", "Rmpi", "affy", "R2HTML", "gplots", "fields", "limma", "oligo"))
{
    testpackage(pkg)
}
if (!require("meap",character.only = TRUE) || packageDescription("meap")$Version != "2.0.2" )
{
    install.packages("meap", dep=TRUE, repos="http://anduril.org/pub/R/", type = "source")
}
' | R --slave --vanilla 

