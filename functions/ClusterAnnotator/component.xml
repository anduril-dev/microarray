<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>ClusterAnnotator</name>
    <version>1.0</version>
    <doc>Produces a hierarchical clustering of the samples and assigns
         the provided annotations to the brances of the clustering tree.
         The clustering is based on the provided data matrix.

         <img src="examplePlot.png" width="%90"/>
    </doc>
    <author email="Marko.Laakso@Helsinki.FI">Marko Laakso</author>
    <category>Clustering</category>
    <inputs>
        <input name="data" type="Matrix">
            <doc>Clustering data</doc>
        </input>
        <input name="annotations" type="AnnotationTable">
            <doc>Sample annotations</doc>
        </input>
    </inputs>
    <outputs>
        <output name="originalPlot" type="Latex">
            <doc>Original clustering report</doc>
        </output>
        <output name="annotPlot" type="Latex">
            <doc>Annotated clustering report</doc>
        </output>
        <output name="splits" type="SetList">
            <doc>Details about the annotation assignments</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="annots" type="string">
            <doc>A comma separated list of annotation name=column pairs
                 representing visible labels and the corresponding
                 annotations columns selected for the annotations.</doc>
        </parameter>
        <parameter name="showLabel" type="boolean" default="true">
            <doc>Include sample labels to the output visualization.</doc>
        </parameter>
        <parameter name="minMI" type="float" default="0.2">
            <doc>The lower limit of the mutual information to be used.</doc>
        </parameter>
        <parameter name="maxP" type="float" default="0.05">
            <doc>P-value threshold to be used.</doc>
        </parameter>
        <parameter name="useNA" type="boolean" default="false">
            <doc>If true, the missing data (=NA values) is used also to calculate
                 the MI and missing data can thus be enriched in the branches of
                 the tree. If false, the random process to select a node uniformly
                 random, which is used to define the MI, is modified in the way
                 that a node with missing data cannot be selected.</doc>
        </parameter>
    </parameters>
</component>
