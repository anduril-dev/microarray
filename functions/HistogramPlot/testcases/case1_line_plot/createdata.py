#!/usr/bin/python

import csv

d=dict()
d['Value']=[]
d['ValueRev']=[]
for i in range(0,10):
    d['Value'].extend([i for t in range(0,i)])
    d['ValueRev'].extend([float(15-i)/2 for t in range(0,i)])

fields=('Value','ValueRev')
writer = csv.DictWriter(open('csv.csv', 'wt'),
                        fieldnames=fields,
                        delimiter='\t',
                    quotechar='"', quoting=csv.QUOTE_MINIMAL)

writer.writerow(dict(zip(fields, fields)))
for i in range(0,len(d['Value'])):
    row={'Value': d['Value'][i], 'ValueRev': d['ValueRev'][i]}
    writer.writerow(row)
#writer.writerow(d)