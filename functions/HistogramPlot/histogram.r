cbind.fill<-function(...){
    # Function to bind different lenghts of columns to one matrix
    nm <- list(...) 
    nm<-lapply(nm, as.matrix)
    n <- max(sapply(nm, nrow)) 
    do.call(cbind, lapply(nm, function (x) 
    rbind(x, matrix(, n-nrow(x), ncol(x))))) 
}
remove.outliers<-function(values,limits){
    # Remove values outside the edges (limits)
    if (length(limits)==1) { return(values) }
    
    values <- values[which(values>=min(limits))]
    values <- values[which(values<=max(limits))]
    return(values)
}

# value columns
param1<-split.trim(param1, ',')
if (identical(param1, '*')) param1 <- colnames(table1)

# data for the histogram
coldata <- table1[,param1,drop=FALSE]
# set common bins
commonScale <- param5=="true"

skip <- integer()
for (i in 1:length(param1)) {
    if (!is.numeric(coldata[,param1[i]])) {
        write.log(cf, sprintf('Skipping column %s in y: not numeric', param1[i]))
        skip <- c(skip, i)
    }
}
if (length(skip) > 0) param1 <- param1[-skip]
coldata <- coldata[,param1,drop=FALSE]
values<-as.vector(data.matrix(coldata))
values[!is.na(values)]
# number of bins 
breaks <- eval(parse(text=param2))
# create breaks for common scale
if (commonScale) {
    values <- remove.outliers(values, breaks)
    y<-hist(values,breaks=breaks,plot=FALSE)
    breaks<-y$breaks
    #mids<-format(y$mids,digits=param3)
}
# Empty result frames
table.out<-data.frame()
optOut1<-data.frame()
optOut2<-data.frame()
optOut3<-data.frame()
cols<-c()
midcols<-c()

# for each column, calculate histogram and midpoints
for (i in 1:ncol(coldata)) {
    values<-coldata[,i]
    values<-values[!is.na(values)]
    if (!commonScale) {
        breaks <- eval(parse(text=param2))
    }
    values <- remove.outliers(values, breaks)
    y<-hist(values,breaks=breaks,plot=FALSE)
    table.out<-cbind.fill(table.out,(y$counts))
    optOut1<-cbind.fill(optOut1,(format(y$mids,digits=param3)))
    optOut2<-cbind.fill(optOut2,y$mids)
    optOut3<-cbind.fill(optOut3,y$breaks)
    if (param4) { # Change absolute frequencies to ratio if needed
        table.out[,i]<-table.out[,i]/sum(table.out[,i],na.rm=TRUE)
    }
    cols<-c(cols,param1[i])
    midcols<-c(midcols,paste('MidPoint',param1[i]))
}

colnames(table.out)<-cols
colnames(optOut1)<- midcols
colnames(optOut2)<- midcols
colnames(optOut3)<- cols

file=get.output(cf,'optOut1')
CSV.write(file,optOut1)
file=get.output(cf,'optOut2')
CSV.write(file,optOut2)
file=get.output(cf,'optOut3')
CSV.write(file,optOut3)

rm(optOut1)
rm(optOut2)
rm(optOut3)
