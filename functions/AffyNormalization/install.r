
main <- function(argv) {
    if (length(argv) != 1) {
        cat('Usage: install.r <component-root-dir>\n')
        cat('  Copies a CEL file from the affydata package into test directory.\n')
        quit(status=1)
    }
    target=rep(0,4)
    for (i in 1:4) {
      target[i] <- file.path(argv[[1]], 'testcases', 'case1', 'input', 'affy', sprintf('chip%d.cel',i))
    }
    if (any( !file.exists(target)) ) {
    library(affy)    
    celpath  <- system.file('celfiles', package='affydata')
    filename <- list.celfiles(path=celpath, full.names=TRUE)[[1]]

    for (i in 1:4) {
      file.copy(filename, target[i])
    }
    }
    return(invisible(NULL))
}

if (!interactive()) main(commandArgs(TRUE))
