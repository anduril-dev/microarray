package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

/**
 * This component extracts a single column from the given table
 * and calculates the frequencies of each value with more than zero
 * occurrences.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 2.06
 */
public class IDDistribution extends CSV2IDListComponent {

    protected Map<String, int[]> counters;

    @Override
    protected void startInputProcessing() throws IOException {
        counters = new TreeMap<String, int[]>();
    }

    /**
     * Print output.
     */
    @Override
    protected void endInputProcessing() throws IOException {
        CSVWriter out = new CSVWriter(new String[] { colOut, "freq" }, outputFile);
        for (String key : counters.keySet()) {
            int[] counter = counters.get(key);
            out.write(key, quotation);
            out.write(counter[0]);
        }
        out.close();
    }

    @Override
    protected void add(String id) {
        String value = id.trim();
        if (value.isEmpty())
            return;

        int[] counter = counters.get(value);
        if (counter == null) {
            counter = new int[] { 1 };
            counters.put(value, counter);
        } else {
            counter[0]++;
        }
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new IDDistribution().run(argv);
    }

}
