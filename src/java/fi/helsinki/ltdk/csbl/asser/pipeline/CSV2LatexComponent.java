package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.LatexTools;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.ArgumentEncoding;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVCleaner;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.asser.text.TextUtil;

/**
 * This component generates a LaTeX table that represents the
 * selected columns of the input file. The input files are
 * comma separated values used in Pipeline.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 1.53
 */
public class CSV2LatexComponent extends SkeletonComponent {

    /** Regular expression for the link target identifier substitution site. */
    static public final String LINK_ID_TAG = "\\$ID\\$";

	static public final String INPUT_TABLE_DATA     = "tabledata";
    static public final String INPUT_REF_RULES      = "refs";
	static public final String OUTPUT_FOLDER        = "report";
    static public final String PARAM_RULER_STYLE    = "ruler";
    static public final String PARAM_EVEN_COLOR     = "evenColor";
	static public final String PARAM_COLUMNS        = "columns";
	static public final String PARAM_COLUMN_FORMAT  = "colFormat";
	static public final String PARAM_COLUMN_SPLIT   = "listCols";
	static public final String PARAM_LIST_DELIMITER = "listDelim";
	static public final String PARAM_CAPTION        = "caption";
	static public final String PARAM_HEAD_ROTATE    = "hRotate";
	static public final String PARAM_CLEARPAGE      = "pageBreak";
	static public final String PARAM_SECTION        = "section";
    static public final String PARAM_SECTION_TYPE   = "sectionType";
    static public final String PARAM_NUMBER_FORMAT  = "numberFormat";
    static public final String PARAM_DROP_MISSING   = "dropMissing";
    static public final String PARAM_SKIP_EMPTY     = "skipEmpty";
    static public final String PARAM_ATTACH         = "attach";
    static public final String PARAM_COUNT_ROWS     = "countRows";
    static public final String PARAM_RENAME         = "rename";

	protected ErrorCode runImpl(CommandFile cf) throws IOException { 
		boolean     skipEmpty = cf.getBooleanParameter(PARAM_SKIP_EMPTY);
		File        inputFile = cf.getInput(INPUT_TABLE_DATA);
		CSVParser   in        = new CSVParser(inputFile);
		File        outputDir = cf.getOutput(OUTPUT_FOLDER);
		String      component = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
		PrintWriter out;

		if (!outputDir.exists() && !outputDir.mkdirs())
			throw new IOException("Cannot create output folder: "+outputDir.getCanonicalPath());
		if (skipEmpty && !in.hasNext()) {
			out = new PrintWriter(new FileWriter(new File(outputDir, LatexTools.DOCUMENT_FILE)));
			out.print("%\n% Skipped an empty table: ");
			out.print(component);
			out.print(".\n%\n");
			out.close();
			return ErrorCode.OK;
		}

		String    cFormat     = cf.getParameter(PARAM_COLUMN_FORMAT);
		String[]  lists       = AsserUtil.split(cf.getParameter(PARAM_COLUMN_SPLIT));
		String    pCols       = cf.getParameter(PARAM_COLUMNS).trim();
		String    caption     = cf.getParameter(PARAM_CAPTION);
        String    ruler       = cf.getParameter(PARAM_RULER_STYLE);
		String    section     = cf.getParameter(PARAM_SECTION).trim();
		boolean   hRotate     = cf.getBooleanParameter(PARAM_HEAD_ROTATE);
		boolean   clearpage   = cf.getBooleanParameter(PARAM_CLEARPAGE);
        boolean   dropMissing = cf.getBooleanParameter(PARAM_DROP_MISSING);
        boolean   attach      = cf.getBooleanParameter(PARAM_ATTACH);
        boolean   countRows   = cf.getBooleanParameter(PARAM_COUNT_ROWS);
        String    evenColor   = cf.getParameter(PARAM_EVEN_COLOR);
        CSVWriter dataOut     = null;
        String[]  dataCols    = null;
        File      dataFile    = null;
		int[]     cols;

		if (evenColor.equals("1,1,1")) {
			evenColor = AsserUtil.EMPTY_STRING;
		} else {
		    evenColor = String.format("\\rowcolor[rgb]{%s}", cf.getParameter(PARAM_EVEN_COLOR));
		}

		String[] cn = in.getColumnNames();
		String   tn = component+"-Table.tex";
		if (pCols.length() < 1) {
			cols = new int[cn.length];
			for (int i=1; i<cols.length; i++) {
				cols[i] = i;
			}
		} else {
			cols = AsserUtil.indicesOf(AsserUtil.split(pCols), cn, true, true);
		}
		boolean[]            isList    = new boolean[cols.length];
        boolean[]            isRagged  = new boolean[cols.length];
        Format[]             numFormat = prepareNumberFormat(cols, cn, cf.getParameter(PARAM_NUMBER_FORMAT));
        Map<String, RefRule> refRules  = loadReferenceRules(cf.getInput(INPUT_REF_RULES), cn, lists);

		out = new PrintWriter(new FileWriter(new File(outputDir, tn)));
		out.print("{ % This table is generated by CSV2Latex.\n");
		out.print("\\tiny{\\begin{longtable}{");
		if ("center".equals(cFormat)) {
			for (int h=0; h<cn.length; h++) out.print('c');
		} else
		if ("left".equals(cFormat)) {
			for (int h=0; h<cn.length; h++) out.print('l');
		} else
		if ("right".equals(cFormat)) {
			for (int h=0; h<cn.length; h++) out.print('r');
		} else {
			out.print(cFormat);
            selectRaggedCols(cFormat, isRagged, cf);
		}
		out.println('}');

        if (countRows) {
            // Count rows...
            int rowCount = 0;
            for (@SuppressWarnings("unused") String[] line : in) {
                rowCount++;
            }
            in.close();
            in = new CSVParser(inputFile);
            if (rowCount == 0) {
                caption += " There are no rows in this table.";
            } else
            if (rowCount == 1) {
                caption += " This table has only one row.";
            } else {
                caption += String.format(" This table has %d rows.", rowCount);
            }
        }

		if (attach) {
			dataFile = new File(outputDir, component+".txt");
			caption += " \\textattachfile[description={"+
			           dataFile.getName()+
			           "}, print=false, color={0.9 0.0 0.3}, mimetype={text/tab-delimited-values}]{"+
			           dataFile.getName()+
			           "}{Extract content of this table.}";
			dataCols = new String[cols.length];
		}

		if (caption.length() > 0) {
			out.print(String.format("\\caption{%s}\n", caption));
		}
		if (hRotate)
           out.print(String.format("\\label{table:%s}\\\\\n", component));
        else
		   out.print(String.format("\\label{table:%s}\\\\\\hline\n", component));
		StringBuffer head = new StringBuffer(1024);
		String[]     cnN  = CSVCleaner.renameColumns(cn, cf.getParameter(PARAM_RENAME));
		for (int h=0; h<cols.length; h++) {
			String value = cnN[cols[h]];
			if (AsserUtil.indexOf(cn[cols[h]], lists, true, false) >= 0)
				isList[h] = true;
			if (h > 0) head.append(" & ");
			if (hRotate) head.append("\\begin{sideways}");
			head.append("\\textbf{");
			head.append(LatexTools.quote(value));
			head.append('}');
			if (hRotate) head.append("\\end{sideways}");
			if (attach) dataCols[h] = value;
		}
		if (attach) {
			dataOut = new CSVWriter(dataCols, dataFile);
		}
		head.append("\\\\\\hline");
		out.print(head);
		out.println("\n\\endfirsthead{}%");
		out.print(head);
		out.print("\n\\endhead\\hline\\multicolumn{");
		out.print(cols.length);
		out.print("}{r}{\\textit{Continued on next page\\ldots\\/}}\n"+
				  "\\endfoot\\hline\\endlastfoot{}%\n");
		boolean evenLine = true;
		for (String[] line : in) {
			for (int i=0; i<cols.length; i++) {
				if (i > 0) out.print('&');
				if (dataOut != null) dataOut.write(line[cols[i]]);
				String   value  = line[cols[i]];
				String[] values = null;
				if (value != null) {
                    if (numFormat[i] == null) {
					   if (isList[i]) {
						  values = AsserUtil.split(value);
						  for (int c=0; c<values.length; c++) {
							  values[c] = LatexTools.quote(values[c]);
						  }
                       } else {
					      value = LatexTools.quote(value);
                       }
                    } else {
                       if (isList[i]) {
                    	  values = AsserUtil.split(value);
 						  for (int c=0; c<values.length; c++) {
 							  values[c] = TextUtil.format(values[c], numFormat[i]);
 						  }
                       } else {
                          value = TextUtil.format(value, numFormat[i]);
                       }
                    }
                }
                // Links to external sites
                if ((refRules != null) && (!dropMissing || (value != null))) {
                    RefRule rule = refRules.get(cn[cols[i]]);
                    if (rule != null) {
                        if (isList[i]) {
                            String[] rIDs = AsserUtil.split(line[rule.column]);
                            if ((rIDs.length != values.length) && (rIDs.length != 1)) {
                                cf.writeLog("Invalid number of link names in "+Arrays.toString(values));
                                values = rIDs;
                            }
                            for (int r=0; r<values.length; r++) {
                                values[r] = createLink(rule.urlPattern, rIDs[r%rIDs.length], values[r]);
                            }
                        } else {
                            String linkRef = line[rule.column];
                            if (rule.isList && (linkRef != null)) {
                                linkRef = AsserUtil.split(linkRef)[0];
                            }
                            value = createLink(rule.urlPattern, linkRef, value);
                        }
                    }
                }
                if (value != null) {
                    if (isList[i]) {
                       String listDelim = ArgumentEncoding.decode(cf.getParameter(PARAM_LIST_DELIMITER));
                       value = AsserUtil.collapse(values, listDelim);
                    }
                    if (isRagged[i]) {
                        out.append("\\raggedright{")
                           .append(value)
                           .append('}');
                    } else {
                        out.print(value);
                    }
                }
			}
			if (in.hasNext()) {
			   out.print(" \\tabularnewline");
			   if (evenLine) {
				   out.print(evenColor);
			   }
			   evenLine = !evenLine;
               out.print(ruler);
			}
            out.print("%\n");
		}
		out.print("\\end{longtable}}\n} % End of table\n");
		out.close();
		in.close();
		if (dataOut != null) dataOut.close();

		// Create parent document for the table fragment.
		out = new PrintWriter(new FileWriter(new File(outputDir, LatexTools.DOCUMENT_FILE)));
		if (!section.isEmpty()) {
		   out.print(String.format("\\%s{%s}\\label{%s}\n",
                                   cf.getParameter(PARAM_SECTION_TYPE),
                                   section,
                                   component));
		}
		out.append("\\input{").append(tn);
		out.print("} % Table include\n");
		if (clearpage)
			out.print("\\clearpage\n");
		out.close();

		return ErrorCode.OK;
	}

    static private Map<String, RefRule> loadReferenceRules(File     file,
                                                           String[] colNames,
                                                           String[] lists) throws IOException {
        if (file == null) return null;

        Map<String, RefRule> map  = new HashMap<String, RefRule>();
        CSVParser            in   = new CSVParser(file);
        int[]                cols = AsserUtil.indicesOf(new String[] { "URL", "refCol", "valueCol" },
                                                        in.getColumnNames(),
                                                        false, true);
        while (in.hasNext()) {
            String[] line = in.next();
            int      col  = AsserUtil.indexOf(line[cols[1]], colNames, false, true);
            boolean  list = (AsserUtil.indexOf(line[cols[1]], lists, true, false) >= 0);
            map.put(line[cols[2]], new RefRule(line[cols[0]], col, list));
        }
        in.close();
        return map;
    }

    static private String createLink(String url, String id, String text) {
        if (id == null) return text;

        String value = text;
        if (value == null) {
            value = id.trim();
        } else {
           value = value.trim();
           if (value.length() < 1)
               value = id.trim();
        }
        if (value.length() < 1)
            value = "link";

        String link;
        if (url.startsWith("latex:"))
            link = "\\hyperref["+url.substring(6).replaceAll(LINK_ID_TAG, id)+"]{"+value+'}';
        else if (url.startsWith("link:"))
            link = "\\hyperlink{"+url.substring(5).replaceAll(LINK_ID_TAG, id)+"}{"+value+'}';
        else if (url.startsWith("anchor:"))
            link = "\\hyperdef{tablecell}{"+url.substring(7).replaceAll(LINK_ID_TAG, id)+"}{"+value+'}';
        else
            link = "\\href{"+url.replaceAll(LINK_ID_TAG, id)+"}{"+value+'}';
        return link;
    }

    /**
     * Generate number formats for the output columns.
     *
     * @param cols     Column indices for the output columns
     * @param colNames Column names of the input data
     * @param formats  A comma separated list of column names and their number formats
     *                  (column1=rule1,column2=rule2)
     */
    static private Format[] prepareNumberFormat(int[]    cols,
                                                String[] colNames,
                                                String   formats) {
        Format[]             rules   = new Format[cols.length];
        String[][]           tokens  = AsserUtil.prepareMapping(formats);
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US); // US English is CSBL standard for Anduril outputs
        final Format         nullFrm = new MessageFormat("{0}", Locale.US);

        for (String[] token : tokens) {
            if (token[1] == null) {
               throw new IllegalArgumentException("Invalid number format rule: "+token);
            }

            int c = AsserUtil.indexOf(token[0], colNames, true, true);
            Format rule;
            if ("RAW_LATEX".equals(token[1])) {
                rule = nullFrm;
            } else {
                DecimalFormat df = new DecimalFormat(token[1], symbols);
                df.setRoundingMode(RoundingMode.HALF_UP);
                rule = df;
            }
            for (int i=0; i<rules.length; i++) {
                if (cols[i] == c) {
                    rules[i] = rule;
                }
            }
        }
        return rules;
    }

    static private void selectRaggedCols(String      style,
                                         boolean[]   ragged,
                                         CommandFile cf) throws IOException {
        boolean ignore = false;
        int     cCount = 0;
        int     length = style.length();

        for (int i=0; i<length; i++) {
            char c = style.charAt(i);
            if (ignore) {
                if (c == '}')
                    ignore = false;
                continue;
            }
            switch (c) {
                case 'p': if (cCount < ragged.length) ragged[cCount] = true;
                case 'c':
                case 'r':
                case 'l': cCount++;
                          break;
                case '{': ignore = true;
                default : break;
            }
        }
        if (ignore || (cCount != ragged.length)) {
            cf.writeLog("Warning: estimated "+cCount+" out of "+ragged.length+
                        " expected columns from the formatting rule: "+style);
            Arrays.fill(ragged, false);
        }
    }

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new CSV2LatexComponent().run(argv);
	}

}

final class RefRule {

    final String  urlPattern;
    final int     column;
    final boolean isList;

    RefRule(String url, int col, boolean list) {
        urlPattern = url.trim();
        column     = col;
        isList     = list;
    }

}
