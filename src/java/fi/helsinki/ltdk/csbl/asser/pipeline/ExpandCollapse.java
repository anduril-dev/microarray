package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.IOException;
import java.util.*;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.ArgumentEncoding;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

/**
 * This component converts between the two possible representations of a relations with
 * multivalued columns. By default this component splits rows with multivalued (comma separated lists)
 * columns into multiple rows each representing a single value.
 * The input may contain several columns with comma separated values and all
 * permutations of column values are shown as individual single-valued rows.
 * The alternative mode recovers the original comma separated values from the
 * expanded relations. This is performed by joining values of the their list columns into comma
 * separated lists.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 2.42
 */
public class ExpandCollapse extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
    	boolean   duplic  = cf.getBooleanParameter("duplicates");
        boolean   expand  = cf.getBooleanParameter("expand");
        int       maxPerm = cf.getIntParameter("maxPerms");
        CSVParser in      = new CSVParser(cf.getInput("relation"));
        String[]  cNames  = in.getColumnNames();
        String[]  lColArg = AsserUtil.split(cf.getParameter("listCols"));
        File      outFile = cf.getOutput("relation");
        CSVWriter out     = new CSVWriter(cNames, outFile);
        String    delim   = ArgumentEncoding.decode(cf.getParameter("delim"));
        int[]     lCols;

        if ("*".equals(lColArg[0])) {
            lCols = new int[cNames.length];
            for (int i=0; i<cNames.length; i++)
                lCols[i] = i;
        } else if (lColArg[0].length() < 1) {
            lCols = new int[0];
        } else {
            lCols = AsserUtil.indicesOf(lColArg, cNames, false, true);
            Arrays.sort(lCols);
        }

        if (expand) {
            expand(in, out, lCols, delim, maxPerm, !duplic);
        } else {
            collapse(in, out, lCols, delim, duplic);
        }
        return ErrorCode.OK;
    }

    static private void expand(CSVParser in,
                               CSVWriter out,
                               int[]     lCols,
                               String    delim,
                               int       maxPerms,
                               boolean   unique) {
        final String[] MISSING_VALUE = new String[1];
        String[][]     values        = new String[in.getColumnCount()][];
        int[]          steps         = new int[values.length];

        for (String[] row : in) {
            int perms = 1;

            for (int i : lCols) {
                if (row[i] == null) {
                   values[i] = MISSING_VALUE;
                } else {
                   values[i] = AsserUtil.split(row[i], delim);
                   if (unique) values[i] = unique(values[i]);
                }
                perms *= values[i].length;
            }
            if (perms > maxPerms)
            	throw new IllegalStateException(String.format("Row %d would expand to %d (>%d) output rows.",
            			                                      in.getLineNumber(), perms, maxPerms));
            if (lCols.length > 0)
                steps[lCols[lCols.length-1]] = values[lCols[lCols.length-1]].length;
            for (int i=lCols.length-2; i>= 0; i--) {
                steps[lCols[i]] = values[lCols[i]].length*steps[lCols[i+1]];
            }

            for (int p=0; p<perms; p++) {
                for (int c=0; c<row.length; c++) {
                    if (values[c] == null) {
                        out.write(row[c]);
                    } else {
                        int s = (p/(steps[c]/values[c].length)) % values[c].length;
                        out.write(values[c][s]);
                    }
                }
            }
        }
        in.close();
        out.close();
    }

    static private String[] unique(String[] list) {
    	if (list.length < 2) return list;

    	Set<String> set = new TreeSet<String>();
    	for (int i=0; i<list.length; i++) {
    		set.add(list[i]);
    	}

    	int size = set.size();
    	if (size == list.length) {
            return list;
    	} else {
    		return set.toArray(new String[size]);
    	}
    }

    static private void collapse(CSVParser in,
                                 CSVWriter out,
                                 int[]     lCols,
                                 String    delim,
                                 boolean   duplicates) {
        List<Object[]> rows     = new LinkedList<Object[]>();
        boolean[]      listCols = new boolean[in.getColumnCount()];

        for (int c : lCols) {
            listCols[c] = true;
        }

        for (String[] row : in) {
            add(row, rows, listCols, duplicates);
        }
        in.close();

        for (Object[] item : rows) {
            for (int i=0; i<listCols.length; i++) {
                if (listCols[i] && (item[i] != null) && !(item[i] instanceof String)) {
                    out.write(AsserUtil.collapse((List<?>)item[i], delim));
                } else {
                    out.write((String)item[i]);
                }
            }
        }
        out.close();
    }

    @SuppressWarnings("unchecked")
    static private void add(String[]       row,
                            List<Object[]> rows,
                            boolean[]      listCols,
                            boolean        duplicates) {
        itemLoop: for (Object[] item : rows) {
            for (int i=0; i<listCols.length; i++) {
                if (!listCols[i] &&
                    (((item[i] == null) && (row[i] != null)) ||
                     ((item[i] != null) && !item[i].equals(row[i])))) {
                   continue itemLoop;
                }
            }
            for (int i=0; i<listCols.length; i++) {
                if (listCols[i] && (row[i] != null)) {
                    if ((item[i] == null) || (item[i] == row[i])) {
                        item[i] = row[i];
                    } else {
                        List<String> list;
                        if (item[i] instanceof String) {
                            list = new LinkedList<String>();
                            list.add((String)item[i]);
                            item[i] = list;
                        } else {
                            list = (List<String>)item[i];
                        }
                        if (duplicates || !list.contains(row[i]))
                           list.add(row[i]);
                    }
                }
            }
            return;
        }
        Object[] item = new Object[row.length];
        System.arraycopy(row, 0, item, 0, row.length);
        rows.add(item);
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new ExpandCollapse().run(argv);
    }

}
