package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.IOException;
import java.util.*;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

/**
 * This Anduril component combines multiple CSV files into a single
 * table. The result file will contain a union of all columns of the
 * files plus an additional column for the original name of the source
 * file.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 2.26
 */
public class CSVListJoin extends SkeletonComponent {

	static public final String INPUT_FOLDER = "folder";
	static public final String INPUT_ARRAY  = "files";

	static public final String PARAM_COLUMN_FILE = "fileCol";
	static public final String PARAM_STRIP_EXT   = "stripExt";
    static public final String PARAM_FILE_EXT    = "acceptExt";
    static public final String PARAM_USE_KEYS    = "useKeys";

	static public final String OUTPUT_JOIN = "join";

	protected final ErrorCode runImpl(CommandFile cf) throws IOException {
        String        colFile  = cf.getParameter(PARAM_COLUMN_FILE).trim();
        boolean       stripExt = cf.getBooleanParameter(PARAM_STRIP_EXT);
        boolean       useKeys  = cf.getBooleanParameter(PARAM_USE_KEYS);
        List<File>    files    = new LinkedList<File>();
        List<String>  colNames = new ArrayList<String>(100);
        Set<String>   fnUsed   = new HashSet<String>();
        String        extDef   = cf.getParameter(PARAM_FILE_EXT).trim();
        boolean       checkExt = !AsserUtil.onlyWhitespace(extDef);
        boolean       useName  = !colFile.isEmpty();
        CSVParser     in;
        CSVWriter     out;
        String[]      cols;
        File          inF;

		// Check folder input
        inF = cf.getInput(INPUT_FOLDER);
		if (inF != null) {
		    if (!inF.isDirectory()) {
		        cf.writeError(inF.getAbsolutePath()+" ("+INPUT_FOLDER+") is not a folder.");
		        return ErrorCode.INVALID_INPUT;
		    }
			for (File f : inF.listFiles()) {
				files.add(f);
			}
		}
        final Map<File,String> aliases;
		// Check array input
		if (cf.inputDefined(INPUT_ARRAY)) {
			IndexFile index = cf.readInputArrayIndex(INPUT_ARRAY);
			aliases = useKeys ? new HashMap<File,String>(index.size()*2) : null;
			for (String key : index) {
				File f = index.getFile(key);
				files.add(f);
				if (useKeys) aliases.put(f, key);
			}
		} else {
			aliases = null;
		}
		// Check file inputs
		for (int i=1; i<10; i++) {
			inF = cf.getInput("file"+i);
			if (inF != null) {
				files.add(inF);
			}
		}

		File[] folder = files.toArray(new File[files.size()]);
		Arrays.sort(folder, new Comparator<File>() {
			@Override
			public int compare(File f0, File f1) {
				String s0 = null;
				String s1 = null;
				if (aliases != null) {
					s0 = aliases.get(f0);
					s1 = aliases.get(f1);
				}
				if (s0 == null) s0 = f0.getName();
				if (s1 == null) s1 = f1.getName();
				return s0.compareToIgnoreCase(s1);
			}
		});
		if (useName)
			colNames.add(colFile);
		for (int i=0; i<folder.length; i++) {
			if (folder[i].isDirectory() ||
                (checkExt && !folder[i].getName().endsWith(extDef))) {
                folder[i] = null;
				continue;
            }
			in = new CSVParser(folder[i]);
			for (String col : in.getColumnNames()) {
				if (!colNames.contains(col)) {
					colNames.add(col);
				}
			}
            in.close();
		}
		cols = colNames.toArray(new String[colNames.size()]);

		out = new CSVWriter(cols, cf.getOutput(OUTPUT_JOIN));
		for (int i=0; i<folder.length; i++) {
			if (folder[i] == null)
				continue;
			String file = null;
			if (useName) {
				if (aliases != null) file = aliases.get(folder[i]);
				if (file == null)    file = folder[i].getName();
                if (stripExt) {
                	int extI = file.lastIndexOf('.');
                    if (extI > 0) file = file.substring(0, extI);
                }
                if (fnUsed.contains(file)) {
                   int fc = 2;
    			   while (fnUsed.contains(file+'_'+fc)) {
    				  fc++;
    			   }
    			   file = file+'_'+fc;
                }
                fnUsed.add(file);
			}
			try {
	            in = new CSVParser(folder[i]);
				int[] values = AsserUtil.indicesOf(cols, in.getColumnNames(), true, false);
				for (String[] line : in) {
				    if (file != null) out.write(file);
				    for (int c=useName?1:0; c<cols.length; c++) {
				    	if (values[c] < 0)
				    		out.skip();
				    	else
					        out.write(line[values[c]]);
	                }
				}
				in.close();
			} catch (IllegalStateException ex) {
				out.close();
				cf.writeError("Error while processing "+folder[i]+": "+
				              ex.getMessage());
				//cf.writeError(ex);
				return ErrorCode.INVALID_INPUT;
			}
		}
		out.close();
		return ErrorCode.OK;
	}

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new CSVListJoin().run(argv);
	}

}
