package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;

/**
 * This component does a search and replace for the input file and returns
 * results as a new file of the same type.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 2.03
 */
public class SearchReplaceComponent extends SkeletonComponent {

    static public final String INPUT_ORIGINAL = "file";
    static public final String INPUT_RULES    = "rules";
    static public final String OUTPUT_TARGET  = "file";

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        File    fileIn    = cf.getInput(INPUT_ORIGINAL);
        File    fileRules = cf.getInput(INPUT_RULES);
        File    fileOut   = cf.getOutput(OUTPUT_TARGET);
        boolean fixed     = cf.getBooleanParameter("fixed");
        String  text;

        text = Tools.readFile(fileIn);

        if (fileRules != null) {
            Properties rules = new Properties();
            rules.load(new FileReader(fileRules));

            Enumeration<Object> keys = rules.keys();
            while (keys.hasMoreElements()) {
                String key   = keys.nextElement().toString();
                String value = rules.getProperty(key, AsserUtil.EMPTY_STRING);

                if (fixed) {
                   text = Pattern.compile(key, Pattern.LITERAL)
                		         .matcher(text).replaceAll(value);
                } else {
                   text = text.replaceAll(key, value);
                }
            }
        }

        for (int p=0; p<10; p++) {
            String key = cf.getParameter("key0"+p);
            if (key.length() < 1) continue;
            String value = cf.getParameter("value0"+p);
            if (fixed) {
                text = Pattern.compile(key, Pattern.LITERAL)
                              .matcher(text).replaceAll(value);
             } else {
                text = text.replaceAll(key, value);
             }
        }

        Tools.writeString(fileOut, text);
        return ErrorCode.OK;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new SearchReplaceComponent().run(argv);
    }

}
