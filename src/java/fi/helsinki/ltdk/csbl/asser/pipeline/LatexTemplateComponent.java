package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fi.helsinki.ltdk.csbl.anduril.component.*;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;

/**
 * This component generates a LaTeX-fragment that represents the
 * given SQL statement.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 1.56
 */
public class LatexTemplateComponent extends SkeletonComponent {

	static public final String INPUT_REF_DB         = "bibtex";
	static public final String INPUT_ABSTRACT       = "abstract";
	static public final String OUTPUT_HEADER        = "header";
	static public final String OUTPUT_FOOTER        = "footer";
	static public final String PARAM_BIBSTYLE       = "bibstyle";
	static public final String PARAM_TITLE          = "title";
	static public final String PARAM_AUTHORS        = "authors";
	static public final String PARAM_PACKAGES       = "usepackage";
	static public final String PARAM_TOC            = "printTOC";
	static public final String PARAM_BASE_LINE_SKIP = "baselineskip";

	/**
	 * Contains the functional logic of the component.
	 */
	protected ErrorCode runImpl(CommandFile cf) throws IOException {
		File       fileH    = cf.getOutput(OUTPUT_HEADER);
		File       fileF    = cf.getOutput(OUTPUT_FOOTER);
		File       fileBib  = cf.getInput(INPUT_REF_DB);
		File       fileAbst = cf.getInput(INPUT_ABSTRACT);
		String     engine   = cf.getMetadata(CommandFile.METADATA_ENGINE);
		String     bibstyle = cf.getParameter(PARAM_BIBSTYLE);
		String     authors  = cf.getParameter(PARAM_AUTHORS).trim();
		String     title    = cf.getParameter(PARAM_TITLE).trim();
		String     packages = cf.getParameter(PARAM_PACKAGES).trim();
		boolean    hasTOC   = cf.getBooleanParameter(PARAM_TOC);
		double     baseskip = cf.getDoubleParameter(PARAM_BASE_LINE_SKIP);
		String     bibName  = "document";
		List<File> bibs     = new ArrayList<File>();

        if (engine == null) {
            engine = "[unknown pipeline engine]";
        }

		if (fileBib == null) {
            fileBib = new File(System.getProperty("user.dir", "."),
                               "../report-BibTeX/microarray.bib");
            if (!fileBib.exists()) {
               cf.writeError("Default reference database does not exist: "+fileBib.getAbsolutePath());
               return ErrorCode.INPUT_IO_ERROR;
            }			
		}
	    bibs.add(fileBib);
	    for (int b=1; b<=5; b++) {
	    	fileBib = cf.getInput("bibtex"+b);
	    	if (fileBib != null) {
	    		bibs.add(fileBib);
	    	}
	    }
	    if (bibs.size() == 1) {
	    	bibName = bibtexName(bibs.get(0));
	    }

		// Generate file contents...
        String header = generateHeader(engine, fileAbst, authors, title, packages, baseskip, hasTOC);
        String footer = generateFooter(bibstyle, bibName);

        // Create output files...
		writeDocument(header, fileH);
		writeDocument(footer, fileF);
		fileBib = new File(fileF, File.separator+bibName+".bib");
		Tools.copyFile(bibs.get(0), fileBib);
		for (int b=1; b<bibs.size(); b++) {
			Tools.copyFile(bibs.get(b), fileBib, true);
		}
		return ErrorCode.OK;
	}

	static private String bibtexName(File f) {
		String name = f.getName();
		int    pos  = name.lastIndexOf('.');
		return (pos > 0) ? name.substring(0, pos) : name;
	}

	/**
	 * Create a LaTeX-fragment for the given content.
	 */
	static private void writeDocument(String content, File folder) throws IOException {
		if (!folder.exists() && !folder.mkdirs())
			throw new IOException("Cannot create output folder: "+folder.getCanonicalPath());
		Tools.writeString(new File(folder, LatexTools.DOCUMENT_FILE), content);
	}

	static private String generateHeader(String  engine,
                                         File    fileAbst,
			                             String  author,
			                             String  titleParam,
			                             String  packages,
			                             double  baselineskip,
			                             boolean hasTOC) throws IOException {
		StringBuffer buf      = new StringBuffer(1024);
		String       template = Tools.readFile(new File("headerTemplate.tex"));
		String       title    = titleParam;
		String       myLibs;

		buf.append(template);

		if (packages.length() > 0) {
			StringBuffer pacB = new StringBuffer(256);
			pacB.append("% Pipeline specific packages\n");
			for (String p : AsserUtil.split(packages)) {
				pacB.append("\\usepackage{").append(p).append("}\n");
			}
			myLibs = pacB.toString();
		} else {
			myLibs = "% No custom packages";
		}

        StringBuffer cover = new StringBuffer(2048);
		if (!author.isEmpty()) {
			cover.append("\\author{");
			String[] auths = AsserUtil.split(author);
			for (int i=0; i<auths.length; i++) {
				if (i > 0)
					cover.append(" \\and ");
				cover.append(auths[i].trim());
			}
		    cover.append("}\n");
		    if (title.isEmpty()) title = "\\Pipeline Output Report";
		}
		if (!title.isEmpty()) {
			cover.append(String.format("\\title{%s}\n", title));
			cover.append("\\maketitle{}\n");
			File abstTemplate;
			if (fileAbst == null)
				abstTemplate = new File("abstractTemplate.tex");
			else
				abstTemplate = new File(fileAbst, LatexTools.DOCUMENT_FILE);
			cover.append(Tools.readFile(abstTemplate));
			cover.append("\\pagebreak[4]{}\n");
		}
        AsserUtil.replaceAll(buf, "${cover}", cover.toString());

		if (hasTOC) {
            AsserUtil.replaceAll(buf, "${tableofcontents}", "\\tableofcontents \\pagebreak[4]");
        } else {
            AsserUtil.replaceAll(buf, "${tableofcontents}", "% Table of contents can be placed here.");
        }

		AsserUtil.replaceAll(buf, "${line.skip}", String.valueOf(baselineskip));
		AsserUtil.replaceAll(buf, "${my.libs}",   myLibs);
		AsserUtil.replaceAll(buf, "${engine}",    engine);
		AsserUtil.replaceAll(buf, "${time}",      new Date().toString());
		AsserUtil.replaceAll(buf, "${user}",      System.getProperty("user.name", "[anonymous]"));
        AsserUtil.replaceAll(buf, "${author}",    (author.length() > 0) ? author : System.getProperty("user.name", "[anonymous]"));
        AsserUtil.replaceAll(buf, "${title}",     (title.length()  > 0) ? title  : "Anduril report");

		return buf.toString();
	}

	static private String generateFooter(String bibstyle,
			                             String bibName) throws IOException {
		StringBuffer buf      = new StringBuffer(1024);
		String       template = Tools.readFile(new File("footerTemplate.tex"));

		buf.append(template);
		AsserUtil.replaceAll(buf, "${bibstyle}",       bibstyle);
		AsserUtil.replaceAll(buf, "${bibliographies}", "\\bibliography{"+bibName+'}');
		return buf.toString();
	}

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new LatexTemplateComponent().run(argv);
	}

}
