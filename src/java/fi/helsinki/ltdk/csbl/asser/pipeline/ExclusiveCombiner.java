package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.IOException;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;

/**
 * Selects one set of inputs to form the set of outputs.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 2.53
 */
public class ExclusiveCombiner extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        final int PORT_COUNT = 5;
        final int SET_COUNT  = 3;
        String    exclude    = cf.getParameter("exclude");
        int       set        = -1;
        int       prefer     = cf.getIntParameter("prefer");

        if ((prefer == 0) || (prefer > PORT_COUNT))
            throw new IllegalArgumentException("Invalid preference set of inputs: "+prefer);
        prefer--;

        if (prefer >= 0) {
            int tmpSet = -1;
            portLoop: for (int p=0; p<PORT_COUNT; p++) {
                char portId = (char)('A'+p);
    
                for (int s=0; s<SET_COUNT; s++) {
                    String inputName  = "item"+(s+1)+portId;
                    File   input      = cf.getInput(inputName);

                    if (input == null) continue;
                    if (s     == prefer) {
                        set = prefer;
                        break portLoop;
                    }
                    if ((tmpSet >= 0 ) && (tmpSet != s)) {
                        cf.writeLog("Content in datasets "+(tmpSet+1)+" and "+(s+1)+".");
                        set = s;
                    }
                    tmpSet = s;
                }
            }
            if ((set != prefer) && (set >= 0)) {
               throw new IllegalArgumentException("Content in multiple sets but not in "+(prefer+1)+'.');
            }
        }

        int minSet = set<0 ? 0         : set;
        int maxSet = set<0 ? SET_COUNT : set+1;
        for (int p=0; p<PORT_COUNT; p++) {
            boolean portUsed   = false;
            char    portId     = (char)('A'+p);
            String  outputName = "item"+portId;
            File    output     = cf.getOutput(outputName);

            for (int s=minSet; s<maxSet; s++) {
                String inputName = "item"+(s+1)+portId;
                File   input     = cf.getInput(inputName);

                if (input == null) continue;
                if ((set >= 0 ) && (set != s))
                   throw new IllegalStateException(inputName+" contains data but dataset "+(set+1)+" is also in use.");
                set      = s;
                portUsed = true;

                if (input.isDirectory()) {
                    Tools.copyDirectory(input, output, true, exclude);
                } else {
                    Tools.copyFile(input, output);
                }
            }
            if (!portUsed) {
                cf.writeLog("No content for the output port "+outputName+'.');
                Tools.writeString(output, "There was no content for this output port.");
            }
        }
        return ErrorCode.OK;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new ExclusiveCombiner().run(argv);
    }

}
