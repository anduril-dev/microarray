package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

/**
 * This component extracts columns from the given tables and
 * represents them as lists within a SetList.
 * All row duplicates will be removed for each list.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 2.87
 */
public class CSV2SetListComponent extends SkeletonComponent {

	protected ErrorCode runImpl(CommandFile cf) throws IOException {
        File         outputFile = cf.getOutput("lists");
        Set<String>  idSet      = new TreeSet<String>();
        StringBuffer idBuf      = new StringBuffer(1024);
        File         tablesF    = cf.getInputArrayIndex("tables");
        File         inputFile;
        CSVParser    in;

        Map<String,String> cols  = AsserUtil.parseMap(cf.getParameter("cols"));
        Map<String,String> lists = AsserUtil.parseMap(cf.getParameter("lists"));

        IndexFile tables;
        if (tablesF == null) {
        	tables = new IndexFile(); 
        } else {
        	tables = IndexFile.read(tablesF);
        }
        for (int i=1; i<10; i++) {
            String portName  = "table"+i;
            inputFile = cf.getInput(portName);
            if (inputFile != null)
                tables.add(portName, inputFile);
        }

        CSVWriter out = new CSVWriter(new String[]{"ID","Members"}, outputFile);
        for (String portName : tables) {
            inputFile = tables.getFile(portName);
            in = new CSVParser(inputFile);
            String[] colNames = in.getColumnNames();
            int      inCol    = getColumn(portName, cols, colNames);
            String   listID   = getValue(portName, portName, lists);
            idSet.clear();
            while (in.hasNext()) {
                String[] row = in.next();
                if (row[inCol] != null)
                   idSet.add(row[inCol]);
            }
            idBuf.setLength(0);
            boolean hasIDs = false;
            for (String id : idSet) {
                if (hasIDs) {
                    idBuf.append(',');
                } else {
                    hasIDs = true;
                }
                idBuf.append(id);
            }
            in.close();
            out.write(listID);
            out.write(idBuf.toString());
        }
        out.close();

		return ErrorCode.OK;
	}

    static private int getColumn(String             port,
                                 Map<String,String> cols,
                                 String[]           colNames) {
        String colDef = cols.get(port);
        if (colDef == null)
           return 0;
        return AsserUtil.indexOf(colDef, colNames, true, true);
    }

    static private String getValue(String             defValue,
                                   String             port,
                                   Map<String,String> settings) {
        String value = settings.get(port);
        return (value == null) ? defValue : value;
    }

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new CSV2SetListComponent().run(argv);
	}

}
