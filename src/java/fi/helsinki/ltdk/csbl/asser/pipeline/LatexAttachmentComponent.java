package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;

import fi.helsinki.ltdk.csbl.anduril.component.*;

/**
 * This component generates a LaTeX fragment that includes attachments
 * for the given files.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 2.66
 */
public class LatexAttachmentComponent extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        File         outputDir = cf.getOutput("report");
        String       component = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        String       section   = cf.getParameter("sectionTitle");
        StringBuffer tex       = new StringBuffer(4096);

        if (!outputDir.exists() && !outputDir.mkdirs())
            throw new IOException("Cannot create output folder: "+outputDir.getCanonicalPath());

        tex.append(cf.getParameter("head"));
        tex.append("\n\n% Set of attachments:");
        if (!section.isEmpty()) {
           tex.append(String.format("\n\\%s{%s}\\label{%s}\n",
                                    cf.getParameter("sectionType"),
                                    section,
                                    component));
        }

        // Try all file input ports
        for (int i=1; i<10; i++) {
            String capt  = cf.getParameter("caption"+i).trim();
            String pname = "file"+i;
            File   ifile = cf.getInput(pname);
            File   ofile;
            String mime;
            String icon;

            if (ifile == null) continue;
            ofile = new File(outputDir, getFilename(ifile.getName(), component, i));
            Tools.copyFile(ifile, ofile);
            mime = getMimeType(ifile);

            icon = getIcon(mime);
            tex.append("\n\\attachfile[description={")
               .append(ifile.getName())
               .append("}, print=true, author, color={0.9 0.0 0.3}, icon={")
               .append(icon)
               .append("}, mimetype={")
               .append(mime)
               .append("}, zoom=false]{")
               .append(ofile.getName())
               .append("} ");

            if (capt.isEmpty()) {
               capt = "This is a file attachment ("+ifile.getName()+").";
            }
            tex.append(capt).append("\\\\\n");
        }

        tex.append("\n% end of attachment block\n");
        tex.append(cf.getParameter("tail"));
        Tools.writeString(new File(outputDir, LatexTools.DOCUMENT_FILE), tex.toString());
        return ErrorCode.OK;
    }

    static public String getIcon(String mime) {
       String type = mime.substring(0, mime.indexOf('/'));
       if (  "image".equals(type)) return "Graph"; 
       if ("message".equals(type)) return "Tag";
       if (   "text".equals(type)) return "Paperclip";
       return "PushPin";
    }

    static public String getMimeType(File file) {
       String mime = URLConnection.getFileNameMap().getContentTypeFor(file.getName());
       if (mime == null) mime = "application/octet-stream";
       return mime;
    }

    static private String getFilename(String name, String component, int port) {
       int    pos = name.lastIndexOf('.');
       String extension;
       if (pos > 0) {
          extension = name.substring(pos);
       } else {
          extension = ".dat";
       }
       return component+port+extension;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new LatexAttachmentComponent().run(argv);
    }

}
