package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

/**
 * This component extracts a single column from the given table.
 * All row duplicates will be removed.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 1.62
 */
public class CSV2IDListComponent extends SkeletonComponent {

	static public final String INPUT_FILE       = "table";
	static public final String INPUT_ARRAY      = "array";
	static public final String OUTPUT_FILE      = "ids";
	static public final String PARAM_COLUMN_IN  = "columnIn";
	static public final String PARAM_REGEXP_ARR = "regexpArr";
	static public final String PARAM_COLUMN_ARR = "columnInArray";
	static public final String PARAM_COLUMN_OUT = "columnOut";
	static public final String PARAM_CONSTANTS  = "constants";
	static public final String PARAM_QUOTATION  = "quotation";
	static public final String PARAM_IS_LIST    = "isList";
	static public final String PARAM_MISSING_OK = "acceptMissing";

	private boolean     initialized = false;
    private CSVWriter   out;
    private Set<String> idSet;

    protected File    outputFile;
    protected boolean quotation;
    protected String  colOut;

	protected final ErrorCode runImpl(CommandFile cf) throws IOException {
		boolean  isListCol = cf.getBooleanParameter(PARAM_IS_LIST);
		boolean  missingOK = cf.getBooleanParameter(PARAM_MISSING_OK);
		String[] consts    = AsserUtil.split(cf.getParameter(PARAM_CONSTANTS));
        String[] colIn     = AsserUtil.split(cf.getParameter(PARAM_COLUMN_IN));

        outputFile = cf.getOutput(OUTPUT_FILE);
        colOut     = cf.getParameter(PARAM_COLUMN_OUT);
        quotation  = cf.getBooleanParameter(PARAM_QUOTATION);

        for (int i=0; i<10; i++) {
            File inputFile = cf.getInput(INPUT_FILE+(i+1));
            if (inputFile == null) continue;

            processFile(inputFile,
                        ((i >= colIn.length) || colIn[i].isEmpty()) ? null : colIn[i],
                        cf.getParameter("regexp"+(i+1)),
                        isListCol,
                        missingOK,
                        cf);
        }

        if (cf.inputDefined(INPUT_ARRAY)) {
            IndexFile          index     = cf.readInputArrayIndex(INPUT_ARRAY);
            String             regexpArr = cf.getParameter(PARAM_REGEXP_ARR);
            Map<String,String> colInArr  = AsserUtil.parseMap(cf.getParameter(PARAM_COLUMN_ARR));

            for (String key : index) {
                File   inputFile = index.getFile(key);
                String col       = colInArr.get(key);
    
                processFile(inputFile,
                            col,
                            regexpArr,
                            isListCol,
                            missingOK,
                            cf);
            }
        }

        // Process constants...
        if (!initialized) fireStartInput("ID");
        for (String c : consts) {
            if (c.isEmpty())  continue;
            add(c);
        }

        endInputProcessing();
		return ErrorCode.OK;
	}

	private void processFile(File        inputFile,
	                         String      colIn,
	                         String      regexp,
	                         boolean     isListCol,
	                         boolean     missingOK,
	                         CommandFile cf) throws IOException {
	    int colI;
        CSVParser in   = new CSVParser(inputFile);
        String[]  cols = in.getColumnNames();
        if (colIn == null) {
            colI = 0;
        } else {
            colI = AsserUtil.indexOf(colIn, cols, true, !missingOK);
            if (colI < 0) {
                cf.writeLog(inputFile+" does not have "+colIn+" column.");
                fireStartInput(colIn);
                return;
            }
        }
        fireStartInput(cols[colI]);

        String[][] regexMap = prepareRegexMap(regexp);
        int[]      regexCols;
        if (regexMap==null) {
            regexCols = null;
        } else {
            regexCols = new int[regexMap.length];
            for (int c=0; c<regexCols.length; c++) {
                regexCols[c] = AsserUtil.indexOf(regexMap[c][0], cols, true, true);
            }
        }

        for (String[] line : in) {
            if ((line[colI] == null) ||
                !regexMatch(line, regexCols, regexMap))
                continue;
            if (isListCol) {
                for (String term : AsserUtil.split(line[colI])) {
                    add(term);
                }
            } else {
                add(line[colI]);
            }
        }
        in.close();
	}

	/**
	 * Singleton initialization of the output writer.
	 *
	 * @param  columnIn    Name of the current input column.
	 * @throws IOException If the output writer cannot be initialized properly.
	 */
	private void fireStartInput(String columnIn) throws IOException {
        if (!initialized) {
            if (colOut.isEmpty())
               colOut = columnIn;
            startInputProcessing();
            initialized = true;
        }
	}
	
    /**
     * This method is invoked before the input reading starts.
     */
    protected void startInputProcessing() throws IOException {
        idSet = new HashSet<String>(10000);
        out   = new CSVWriter(new String[] { colOut }, outputFile);
    }

    /**
     * This method is called after the input reading.
     */
    protected void endInputProcessing() throws IOException {
        out.close();
    }

    /**
     * This method is invoked for each ID found during the input parsing.
     */
    protected void add(String id) {
        String value = id.trim();
        if (value.isEmpty())
            return;
        if (idSet.add(value)) {
            out.write(value, quotation);
        }
    }

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new CSV2IDListComponent().run(argv);
	}

	static String[][] prepareRegexMap(String param) {
		String value = param.trim();
		if (value.isEmpty()) return null;

		Map<String,String> map   = AsserUtil.parseMap(value);
		String[][]         array = new String[map.size()][2];
		int                k     = 0;
		for (String key : map.keySet()) {
			array[k  ][0] = key;
			array[k++][1] = map.get(key);
		}
		return array;
	}

	static boolean regexMatch(String[] line, int[] cols, String[][] regexMap) {
		if (regexMap == null) return true;
		for (int i=0; i<cols.length; i++) {
			String value = line[cols[i]];
			if ((value == null) || !value.matches(regexMap[i][1]))
				return false;
		}
		return true;
	}

}
