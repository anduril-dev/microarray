package fi.helsinki.ltdk.csbl.asser.pipeline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

/**
 * Calculates a cumulative score for each gene having some regions
 * assigned to it. The score is based on the distance of the region.
 * The closer or the more assigned regions a
 * gene has the better score it will obtain.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 2.73
 */
public class PeakScoreComponent extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
    	double                     s        = cf.getDoubleParameter("s");
    	double                     limit    = cf.getDoubleParameter("minScore");
    	CSVParser                  in       = new CSVParser(cf.getInput("genes"));
    	String                     fParam   = cf.getParameter("formula");
    	String                     scParam  = cf.getParameter("scoreColumn").trim();
    	String                     scOut    = cf.getParameter("scoreColOut").trim();
    	String                     pcOut    = cf.getParameter("peaksColOut").trim();
    	int                        idCol    = AsserUtil.indexOf(cf.getParameter("idColumn"),   in.getColumnNames(), true, true);
    	int                        distCol  = AsserUtil.indexOf(cf.getParameter("distColumn"), in.getColumnNames(), true, true);
    	int                        scoreCol = -1;
    	final Map<String,double[]> scores   = new HashMap<String,double[]>(20000);
    	boolean                    isExp;

    	if ("linear".equals(fParam)) {
    		isExp = false;
    	} else
        if ("exponential".equals(fParam)) {
            isExp = true;
        } else throw new IllegalArgumentException("Invalid score function: "+fParam);
    	if (!scParam.isEmpty()) {
    		scoreCol = AsserUtil.indexOf(scParam, in.getColumnNames(), true, true);
        }

    	for (String[] line : in) {
    		if ((line[idCol]==null) || (line[distCol]==null)) continue;

    		double[] gs = scores.get(line[idCol]);
    		if (gs == null) {
    			gs = new double[2];
    			scores.put(line[idCol], gs);
    		}
    		double ns = Math.abs(Double.parseDouble(line[distCol]));
    		if (isExp) {
    		   ns = Math.exp(-1*ns/s);
    		} else {
    		   ns = s - ns;
    		}
    		if (ns > 0) {
    			if (scoreCol >= 0) ns *= Double.parseDouble(line[scoreCol]);
    			gs[0] += ns;
    			gs[1] += 1;
    		}
    	}
    	in.close();

    	ArrayList<String> genes = new ArrayList<String>(scores.size());
    	for (String id : scores.keySet()) {
    		double[] gs = scores.get(id);
    		if (!isExp) gs[0] /= s;
    		if (gs[0] >= limit) {
    			genes.add(id);
    		}
    	}
    	cf.writeLog(genes.size()+" out of "+scores.size()+" input genes reached the threshold of "+limit+'.');
    	Collections.sort(genes, new Comparator<String>() {
			@Override
			public int compare(String arg0, String arg1) {
				double d0 = scores.get(arg0)[0];
				double d1 = scores.get(arg1)[0];
				if (d0 == d1) return 0;
				return (d0 < d1) ? 1 : -1;
			}
    	});

    	CSVWriter out = new CSVWriter(new String[] {in.getColumnNames()[idCol], scOut, pcOut},
    			                      cf.getOutput("scores"));
    	for (String id : genes) {
    		double[] row = scores.get(id);
  			out.write(id);
   			out.write(AsserUtil.round(row[0], 6), false);
   			out.write((long)row[1]);
    	}    	
    	out.close();
    	return ErrorCode.OK;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new PeakScoreComponent().run(argv);
    }

}
