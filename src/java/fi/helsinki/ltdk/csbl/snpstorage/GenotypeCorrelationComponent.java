package fi.helsinki.ltdk.csbl.snpstorage;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.asser.io.HelistinOutputReader;
import fi.helsinki.ltdk.csbl.asser.sequence.Nucleotide;
import fi.helsinki.ltdk.csbl.asser.sequence.SNPCall;

/**
 * This component compares genotypes calls between the markers of
 * {@link fi.helsinki.ltdk.csbl.snpstorage.SNPHelistinComponent} outputs.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 1.55
 */
public class GenotypeCorrelationComponent extends SkeletonComponent {

	static public final String INPUT_GENOTYPES    = "genotypes";
	static public final String OUTPUT_GROUPS      = "markerGroups";
	static public final String OUTPUT_KEY_MARKERS = "keyMarkers";

	protected ErrorCode runImpl(CommandFile cf) throws IOException {
		File                 inputFile  = cf.getInput(INPUT_GENOTYPES);
		File                 outputFile = cf.getOutput(OUTPUT_GROUPS);
		File                 keySetFile = cf.getOutput(OUTPUT_KEY_MARKERS);
		int[]                syncTop;
		HelistinOutputReader reader;
		CSVWriter            writer;
        CSVWriter            keyOut;          
		String[]             markers;
		SNPCall[]            calls;
		Nucleotide[]         firstN;

		reader  = new HelistinOutputReader(inputFile);
		syncTop = new int[reader.getColumnCount()];
		firstN  = new Nucleotide[syncTop.length];

		Arrays.fill(syncTop, syncTop.length-1);
		sampleIn: while (reader.hasMoreMarkers()) {
			calls = reader.markersLeft();
			int openTasks = 0;
			for (int i=0; i<syncTop.length; i++) {
				if ((calls[i] == null) || (syncTop[i] == i))
					continue; // This marker is not known or it does not correlate
				openTasks++;
				boolean      hz = calls[i].isHomozygous();
				boolean      fc = false;
				Nucleotide[] cn = hz ? calls[i].getNucleotides() : null;
				if (hz) {
					if (firstN[i] == null) {
  					   firstN[i] = cn[0];
  					   fc        = true;
					} else {
					   fc = (cn[0] == firstN[i]);
					}
				}
				for (int t=i+1; t<=syncTop[i]; t++) {
					if (calls[t] == null)
						continue;
					if (!hz && calls[t].isHomozygous()) {
						syncTop[i] = t-1;
						break;
					} else
					if (hz) {
						Nucleotide[] cn2 = calls[t].getNucleotides();
						boolean      fc2;
						if (firstN[t] == null) {
							firstN[t] = cn2[0];
							fc2       = true;
						} else {
							fc2 = (cn2[0] == firstN[t]);
						}
						if (fc != fc2) {
							syncTop[i] = t-1;
							break;
						}
					}
				}
			}
			if (openTasks < 1)
				break sampleIn; // All markers are uncorrelated and we can stop. 
		}
		markers = reader.getColumnNames();
		reader.close();

		// Write outputs...
		keyOut = new CSVWriter(new String[] { "marker" }, keySetFile);
		writer = new CSVWriter(new String[] {
		  "ID", "Members"	
		}, outputFile);
		int          gId = 1;
		StringBuffer buf = new StringBuffer(1024);
		for (int i=0; i<syncTop.length; i++) {
			keyOut.write(markers[i]);
			if (syncTop[i] == i)
				continue; // This marker does not correlate
			writer.write("Group"+gId,   false);
			for (int j=i; j<=syncTop[i]; j++) {
				if (j>i)
					buf.append(',');
				buf.append(markers[j]);
			}
			writer.write(buf.toString());
			buf.setLength(0);
			gId++;
			i = syncTop[i];
		}
		keyOut.close();
		writer.close();

		return ErrorCode.OK;
	}

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new GenotypeCorrelationComponent().run(argv);
	}

}