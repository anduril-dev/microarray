package fi.helsinki.ltdk.csbl.snpstorage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

/**
 * This component calculates genotype frequencies for SNPMatrix files.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 1.51
 */
public class AlleleCounterComponent extends SkeletonComponent {

	static public final String INPUT_GENOTYPES    = "genotypes";
	static public final String OUTPUT_FREQUENCIES = "frequencies";
	static public final String OUTPUT_NA_DIST     = "naDist";
	static public final String PARAM_SKIP         = "skip";
	static public final String PARAM_DELS         = "dels";
    static public final String PARAM_NA_LIMIT     = "naLimit";

	protected ErrorCode runImpl(CommandFile cf) throws IOException {
		File    inputFile  = cf.getInput(INPUT_GENOTYPES);
		File    outputFile = cf.getOutput(OUTPUT_FREQUENCIES);
		File    naDistFile = cf.getOutput(OUTPUT_NA_DIST);
		boolean dels       = cf.getBooleanParameter(PARAM_DELS);

		PrintStream  out  = new PrintStream(new FileOutputStream(outputFile));
		List<String> args = new ArrayList<String>();
		args.add(inputFile.getCanonicalPath());
		args.add("-skip");
		args.add(cf.getParameter(PARAM_SKIP));
		args.add("-naDist");
		args.add(naDistFile.getCanonicalPath());
        args.add("-naLimit");
        args.add(cf.getParameter(PARAM_NA_LIMIT));
		if (dels) args.add("-dels");
		int status = AlleleCounter.run(args.toArray(new String[args.size()]), out);
		out.close();
		return (status==0) ? ErrorCode.OK : ErrorCode.ERROR;
	}

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new AlleleCounterComponent().run(argv);
	}

}
