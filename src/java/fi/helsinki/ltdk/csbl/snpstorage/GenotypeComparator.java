package fi.helsinki.ltdk.csbl.snpstorage;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.asser.sequence.SNPCall;

/**
 * This component compares genotype frequencies between two sets of
 * {@link fi.helsinki.ltdk.csbl.snpstorage.AlleleCounterComponent} outputs.
 *
 * @author Marko Laakso
 * @since  {@link fi.helsinki.ltdk.csbl.asser.Version Version} 1.52
 */
public class GenotypeComparator extends SkeletonComponent {

    static public final String INPUT_FREQUENCIES_CASES    = "caseF";
    static public final String INPUT_FREQUENCIES_CONTROLS = "controlF";
    static public final String OUTPUT_STATISTICS          = "statistics";
    static public final String PARAM_CI_COVERAGE          = "CILimit";
    static public final String PARAM_P_LIMIT              = "PLimit";
    static public final String PARAM_RISK_TYPE            = "RiskType";
    static public final String PARAM_SKIP_CI_ONE          = "skipCI1";

    /** A set of supported definitions for the risk. */
    public enum RiskType { RiskRatio, OddsRatio };

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        File                        caseFile    = cf.getInput(INPUT_FREQUENCIES_CASES);
        File                        controlFile = cf.getInput(INPUT_FREQUENCIES_CONTROLS);
        File                        outFile     = cf.getOutput(OUTPUT_STATISTICS);
        RiskType                    riskType    = RiskType.valueOf(cf.getParameter(PARAM_RISK_TYPE));
        double                      risk        = 1.0-cf.getDoubleParameter(PARAM_CI_COVERAGE);
        double                      pLimit      = cf.getDoubleParameter(PARAM_P_LIMIT);
        boolean                     skipCI1     = cf.getBooleanParameter(PARAM_SKIP_CI_ONE);
        Map<String, FrequencyTable> freqs       = new HashMap<String, FrequencyTable>();
        CSVParser                   reader;
        CSVWriter                   writer;
        String                      markerType;

        // Column indices
        final int markerCol = 0;
        final int col_AA    = 3;
        final int col_Aa    = 5;
        final int col_aa    = 7;
        final int col_AAn   = 2;
        final int col_Aan   = 4;
        final int col_aan   = 6;

        // Read control frequencies...
        reader     = new CSVParser(controlFile);
        markerType = reader.getColumnNames()[markerCol];
        while (reader.hasNext()) {
            String[] line = reader.next();
            if (line[col_AAn] == null) {
               cf.writeLog("No control data for: "+line[markerCol]);
               continue;
            }
            char A = line[col_AAn].charAt(0);
            char a = (line[col_aan] == null) ? 0 : line[col_aan].charAt(0);
            FrequencyTable item = new FrequencyTable(A, a,
                                                     Integer.parseInt(line[col_AA]),
                                                     Integer.parseInt(line[col_Aa]),
                                                     Integer.parseInt(line[col_aa]));
            freqs.put(line[markerCol], item);
        }
        reader.close();

        reader = new CSVParser(caseFile);
        writer = new CSVWriter(new String[] { // Column names:
           markerType,     "al1",     "al2",            // marker, nucleotides
              "Nal1c",   "Nal2c",   "Nal1r",   "Nal2r", // allele frequencies (cases+references)
              "fcAl1",   "fcAl2",                       // fold change of allele frequencies
                 "aR",   "aCI_l",   "aCI_u",   "aChi2",   "aP",
               "homR", "homCI_l", "homCI_u", "homChi2", "homP",
               "hetR", "hetCI_l", "hetCI_u", "hetChi2", "hetP",
                "g1R",  "g1CI_l",  "g1CI_u",  "g1Chi2",  "g1P",
                "g2R",  "g2CI_l",  "g2CI_u",  "g2Chi2",  "g2P",
                "tHWp"                                  // Hardy-Weinberg
        }, outFile);
        while (reader.hasNext()) {
            String[]       line = reader.next();
            FrequencyTable ref  = freqs.get(line[markerCol]);
            if (ref == null) {
                cf.writeLog("No control values for "+line[markerCol]+".");
                continue;
            }
            if (line[col_AAn] == null) {
                cf.writeLog("No genotype calls for "+line[markerCol]+".");
                continue;
            }
            if (SNPCall.SYMBOL_MISSING_VALUE.equals(line[col_Aan])) {
                if (ref.a == 0) {
                    cf.writeLog("Only one allele available for "+line[markerCol]+": "+ref.A);
                    continue;
                }
                cf.writeLog("No variation in case samples at "+line[markerCol]+".");
            }
            char A = line[col_AAn].charAt(0);

            int        AA   = Integer.parseInt((A == ref.A) ? line[col_AA] : line[col_aa]);
            int        Aa   = Integer.parseInt(line[col_Aa]);
            int        aa   = Integer.parseInt((A == ref.A) ? line[col_aa] : line[col_AA]);
            double     Nc   = AA+Aa+aa;
            double     Nr   = ref.AA+ref.Aa+ref.aa;
            double     fcA  = ((2*AA+Aa)/Nc)/((2*ref.AA+ref.Aa)/Nr);
            double     fca  = ((2*aa+Aa)/Nc)/((2*ref.aa+ref.Aa)/Nr);
            TestResult tst  = chiSquare(2*    AA+    Aa, 2*    aa+    Aa,
                                        2*ref.AA+ref.Aa, 2*ref.aa+ref.Aa,
                                        risk, riskType);
            TestResult tHom = chiSquare(    AA,     aa,
                                        ref.AA, ref.aa,
                                        risk, riskType);
            TestResult tHet = chiSquare(    AA,     Aa,
                                        ref.AA, ref.Aa,
                                        risk, riskType);
            TestResult tGA  = chiSquare(    AA+    Aa,     aa,
                                        ref.AA+ref.Aa, ref.aa,
                                        risk, riskType);
            TestResult tGa  = chiSquare(    AA,     Aa+    aa,
                                        ref.AA, ref.Aa+ref.aa,
                                        risk, riskType);
            if ((skipCI1 &&
                 (tst .CI_lower <= 1) && (tst .CI_upper >= 1) &&
                 (tHom.CI_lower <= 1) && (tHom.CI_upper >= 1) &&
                 (tHet.CI_lower <= 1) && (tHet.CI_upper >= 1) &&
                 (tGA .CI_lower <= 1) && (tGA .CI_upper >= 1) &&
                 (tGa .CI_lower <= 1) && (tGa .CI_upper >= 1)) ||
                ((tst.p  > pLimit) &&
                 (tHom.p > pLimit) && (tHet.p > pLimit) &&
                 (tGA.p  > pLimit) && (tGa.p  > pLimit))) {
                continue;
            }
            double hwp = AlleleCounter.compareHardyWeinberg(AA+ref.AA,
                                                            Aa+ref.Aa,
                                                            aa+ref.aa);
            writer.write(line[markerCol]);
            if (ref.A == 0) writer.skip(); else writer.write(ref.A);
            if (ref.a == 0) writer.skip(); else writer.write(ref.a);
            writer.write(2*AA+Aa);
            writer.write(2*aa+Aa);
            writer.write(2*ref.AA+ref.Aa);
            writer.write(2*ref.aa+ref.Aa);
            writer.write(AsserUtil.round(fcA, 3), false);
            writer.write(AsserUtil.round(fca, 3), false);
            printTestResults(writer, tst);
            printTestResults(writer, tHom);
            printTestResults(writer, tHet);
            printTestResults(writer, tGA);
            printTestResults(writer, tGa);
            writer.write(AsserUtil.round(hwp, 4), false);
        }
        writer.close();
        reader.close();

        return ErrorCode.OK;
    }

    static private void printTestResults(CSVWriter writer, TestResult tst) {
        writer.write(AsserUtil.round(tst.risk,     5), false);
        writer.write(AsserUtil.round(tst.CI_lower, 5), false);
        writer.write(AsserUtil.round(tst.CI_upper, 5), false);
        writer.write(AsserUtil.round(tst.chii2,    5), false);
        writer.write(AsserUtil.round(tst.p,        6), false);
    }

    /**
     * Calculates the absolute value for the inverse of cumulative probability
     * for a two-sided test.
     *
     * @param  prob Probability
     */
    static public double getNormalLimit(double prob) {
        NormalDistribution nDist = new NormalDistribution();
        double             l;

        l = nDist.inverseCumulativeProbability(1.0-prob/2.0);
        return l;
    }

    /**
     * Chi-square test for two data sets with binary frequencies. This
     * method can be used for allele and genotype comparisons.
     *
     * @param cW    Case wild type
     * @param cM    Case mutation
     * @param rW    Control (reference) wild type
     * @param rM    Control (reference) mutation
     * @param risk  Risk level for the confidence intervals
     */
    static public TestResult chiSquare(int      cW,
                                       int      cM,
                                       int      rW,
                                       int      rM,
                                       double   risk,
                                       RiskType type) {
        double cN   = cW+cM;
        double rN   = rW+rM;
        double wN   = cW+rW;
        double mN   = cM+rM;
        double nVal = getNormalLimit(risk);
        double R;
        double SE_ln;

        switch (type) {
            case OddsRatio:
                 R     = (cM/(double)cW) / (rM/(double)rW);
                 SE_ln = Math.sqrt(1/(double)cW+1/(double)cM+1/(double)rW+1/(double)rM);
                 break;
            case RiskRatio:
                 R     = (cM/mN) / (cW/wN);
                 SE_ln = Math.sqrt(1/(double)cM+1/(double)cW-1/(double)mN-1/(double)wN);
                 break;
            default: throw new UnsupportedOperationException(type.name());
        }
        double logR = Math.log(R);
        double ciL  = logR-nVal*SE_ln;
        double ciU  = logR+nVal*SE_ln;
        ciL         = Math.exp(ciL);
        ciU         = Math.exp(ciU);
        double E    = mN*cN/(cN+rN);
        double V    = (cN*rN*mN*wN)/((mN+wN)*(mN+wN)*(mN+wN-1));
        double chi2 = ((cM-E)*(cM-E))/V;
        double p;

        if (chi2 > 500) {
            p = 1;
        } else {
            ChiSquaredDistribution xDist = new ChiSquaredDistribution(1);
            p = xDist.cumulativeProbability(chi2);
			p = 1-p;
		}
		return new TestResult(R, ciL, ciU, chi2, p);
	}

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new GenotypeComparator().run(argv);
	}

	/**
	 * This structure encapsulates the risk calculation results for one single marker.
	 * The content of the structure is immutable.
	 *
	 * @author Marko Laakos
	 */
	static public class TestResult {

		final double risk;
		final double CI_lower;
		final double CI_upper;
		final double chii2;
		final double p;
		
		TestResult(double r, double ciL, double ciU, double chiiSq, double pValue) {
			this.risk      = r;
			this.CI_lower  = ciL;
			this.CI_upper  = ciU;
			this.chii2     = chiiSq;
			this.p         = pValue;
		}

	}

	/**
	 * Immutable tuple that represents genotype frequencies for one
	 * biallelic SNP marker.
	 */
	static private class FrequencyTable {

		final int  AA, Aa, aa;
		final char A,  a;

		FrequencyTable(char A, char a,
				       int AA, int Aa, int aa) {
			this.A  = A;
			this.a  = a;
			this.AA = AA;
			this.Aa = Aa;
			this.aa = aa;
		}

	}

}
