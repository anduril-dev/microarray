package fi.helsinki.ltdk.csbl.snpstorage;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.helsinki.ltdk.csbl.snpstorage.GenotypeComparator.TestResult;

/**
 * Unit tests for {@link fi.helsinki.ltdk.csbl.snpstorage.AlleleCounter}.
 *
 * @author Marko Laakso
 */
public class GenotypeComparatorTest {

    // These test have been copied from:
    // Cancer Epidemiology: Principles and Methods
    // IARC/WHO, Isabel dos Santos Silva, 1999

	@Test
	public void testNormalLimit() {
		assertEquals(1.96, GenotypeComparator.getNormalLimit(0.05), 0.005);
	}

	@Test
	public void testChiiSquareRR() {
        // p.122-123
		TestResult res = GenotypeComparator.chiSquare(45, 60, 1455, 940, 0.05,
				                                      GenotypeComparator.RiskType.RiskRatio);
		assertEquals(2.0,   res.risk,     0.0001);
		assertEquals(1.37,  res.CI_lower, 0.001);
		assertEquals(2.92,  res.CI_upper, 0.001);
		assertEquals(13.42, res.chii2,    0.01);
	}

	@Test
	public void testChiiSquareOR() {
        // p.126-127
		TestResult res = GenotypeComparator.chiSquare(26, 457, 85, 362, 0.05,
				                                      GenotypeComparator.RiskType.OddsRatio);
		assertEquals(4.13, res.risk,     0.003);
		assertEquals(2.64, res.CI_lower, 0.05);
		assertEquals(6.49, res.CI_upper, 0.06);
		assertEquals(41,   res.chii2,    0.01);

        // p.207
        res = GenotypeComparator.chiSquare(265, 125, 305, 74, 0.05, GenotypeComparator.RiskType.OddsRatio);
        assertEquals(1.94, res.risk, 0.005);

        // p.207
        res = GenotypeComparator.chiSquare(265, 46, 305, 8, 0.05, GenotypeComparator.RiskType.OddsRatio);
        assertEquals(6.62, res.risk, 0.005);
	}

}
