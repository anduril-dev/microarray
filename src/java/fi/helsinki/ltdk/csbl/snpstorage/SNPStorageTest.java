package fi.helsinki.ltdk.csbl.snpstorage;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;

/**
 * Unit tests for {@link fi.helsinki.ltdk.csbl.snpstorage.SNPStorage}.
 *
 * @author Marko Laakso
 */
public class SNPStorageTest {

    private File   tmpDir;
    private File   cfgFile;
    private File   outFile;
    private String testData;

    @Before
    public void setUp() throws IOException {
        tmpDir = new File(System.getProperty("java.io.tmpdir", SNPStorageTest.class.getName()),
                          "SNPStorageTest_"+System.currentTimeMillis());
        if (!tmpDir.mkdirs())
            throw new IOException("Could not create "+tmpDir.getAbsolutePath()+".");
        tmpDir.deleteOnExit();

        testData = ClassLoader.getSystemResource("fi/helsinki/ltdk/csbl/snpstorage/testdata.snpr").getFile();

        outFile = new File(tmpDir, "output.snpr");
        cfgFile = new File(tmpDir, "snpstorage.properties");
        Tools.writeString(cfgFile,
                "version         = 2.22\n"+
                "file.samplenames="+tmpDir.getAbsolutePath()+"/samplenames.txt\n"+
                "dir.genotypes   ="+tmpDir.getAbsolutePath()+"/data\n"+
                "file.markernames="+tmpDir.getAbsolutePath()+"/markernames.txt\n"+
                "file.output     ="+outFile.getAbsolutePath()
        );
        Tools.writeString(new File(tmpDir, "samplenames.txt"), "");
        Tools.writeString(new File(tmpDir, "markernames.txt"), "");

        insertData();
    }

    private void insertData() throws IOException {
        int status = SNPStorage.run(new String[] {"-S", "-M", "-d", "SAVE", "-i", testData},
                                    false,
                                    cfgFile.getAbsolutePath());
        if (status != 0)
            throw new IllegalStateException("SNPHelistin insert returned: "+status);
    }

    @Test
    public void testReading() throws IOException {
        int status = SNPStorage.run(new String[] {"-i", testData}, false, cfgFile.getAbsolutePath());
        assertEquals(0, status);
        File   refFile    = new File(ClassLoader.getSystemResource("fi/helsinki/ltdk/csbl/snpstorage/refOut1.snpr").getFile());
        String refContent = Tools.readFile(refFile);
        String newContent = Tools.readFile(outFile);
        assertEquals(refContent, newContent);
    }

    @Test
    public void testRemove() throws IOException {
        int status;

        String rmvData = ClassLoader.getSystemResource("fi/helsinki/ltdk/csbl/snpstorage/removedata.snpr").getFile();
        status = SNPStorage.run(new String[] {"-d", "REMOVE", "-i", rmvData}, false, cfgFile.getAbsolutePath());
        assertEquals(0, status);

        status = SNPStorage.run(new String[] {"-i", testData}, false, cfgFile.getAbsolutePath());
        assertEquals(0, status);
        File   refFile    = new File(ClassLoader.getSystemResource("fi/helsinki/ltdk/csbl/snpstorage/refOut2.snpr").getFile());
        String refContent = Tools.readFile(refFile);
        String newContent = Tools.readFile(outFile);
        assertEquals(refContent, newContent);
    }

    @After
    public void cleanUp() throws IOException {
        if (!IOTools.rmdir(tmpDir, true)) {
            throw new IOException("Could not remove "+tmpDir.getAbsolutePath()+".");
        }
    }

}
