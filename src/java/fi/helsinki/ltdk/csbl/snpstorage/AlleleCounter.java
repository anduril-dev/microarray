package fi.helsinki.ltdk.csbl.snpstorage;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.MainArgument;
import fi.helsinki.ltdk.csbl.asser.Version;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.asser.io.HelistinOutputReader;
import fi.helsinki.ltdk.csbl.asser.sequence.Nucleotide;
import fi.helsinki.ltdk.csbl.asser.sequence.SNPCall;

/**
 * This program can be used to calculate genotype frequencies from the outputs
 * of {@link fi.helsinki.ltdk.csbl.snpstorage.SNPStorage}.
 *
 * @author Marko Laakso
 */
public final class AlleleCounter {

	/** No instantiation for this executable **/
	private AlleleCounter() {}

	/**
	 * Calculates chii square test for Hardy-Weinberg equilibrium.
	 *
	 * @param AA Frequency of wildtype homozygotes
	 * @param Aa Frequency of heterozygotes
	 * @param aa Frequency of mutate homozygotes
	 * @return P-value for more extreme distribution than the given one
	 */
	static public double compareHardyWeinberg(int AA, int Aa, int aa) {
		double n     = AA+Aa+aa;
		double p     = (2.0*AA+Aa) / (2.0*n);
		double expAA = p*p*n;
		double expAa = 2.0*p*(1.0-p)*n;
		double expaa = (1.0-p)*(1.0-p)*n;
		double chii2 = ((AA-expAA)*(AA-expAA))/expAA+
		               ((Aa-expAa)*(Aa-expAa))/expAa+
		               ((aa-expaa)*(aa-expaa))/expaa;
		if (chii2 > 500)
			return 1;
		ChiSquaredDistribution xDist = new ChiSquaredDistribution(1);
		p = xDist.cumulativeProbability(chii2);
		p = 1-p;
		if (p < 0) p = 0; // rounding from -0.0 to +0.0
		return p;
	}

	/**
	 * Command line executable
	 */
	public static void main(String[] argv) {
		int status = run(argv, System.out);
		System.exit(status);
	}

	/**
	 * Command line executable
	 */
	public static int run(String[] argv, PrintStream out) {
	    // Read command line arguments...
		Map<String, MainArgument> args = new HashMap<String, MainArgument>();
		MainArgument argCountSM    = MainArgument.add("count",   "Count samples and markers", args);
        MainArgument argVerbose    = MainArgument.add("v",       "Verbose output mode", args);
        MainArgument argDel        = MainArgument.add("dels",    "Include null alleles into results", args);
        MainArgument argNALimit    = MainArgument.add("naLimit", "maf",  "Skip markers where one minus call rate exceeds this limit", "1.0", args);
        MainArgument argSkipFew    = MainArgument.add("skip",    "maf",  "Skip markers with too low minor allele frequency", args);
        MainArgument argFileSNPMap = MainArgument.add("map",     "file", "Generate marker value map to the given file", args);
        MainArgument argFileNADist = MainArgument.add("naDist",  "file", "Generate distribution of missing values to the given file", args);
		String[] argLeft;
        try { argLeft = MainArgument.parseInput(argv, args, 1, 1); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
	        System.err.println();
	        printSynopsis(args);
            return 1;
        }
		String genotypeFile = argLeft[0];

		boolean verbose = argVerbose.isUsed();
		boolean useDel  = argDel.isUsed();
        double  naLimit = argNALimit.getDouble();

		double mafLimit = -1;
		if (argSkipFew.isUsed()) {
			try { mafLimit = argSkipFew.getDouble(); }
			catch (NumberFormatException e) {
				System.err.println("Invalid minor allele frequency: "+argSkipFew.getValue());
				return 1;
			}
			if (mafLimit > 1) {
				System.err.println("Minor allele frequency must be less than 1: "+argSkipFew.getValue());
				return 1;
			}
		}

		HelistinOutputReader in;
		try { in = new HelistinOutputReader(genotypeFile); }
		catch (Exception e) {
			System.err.println("Cannot read genotype calls from "+genotypeFile+": "+e.getLocalizedMessage());
			return 1;
		}
		if (verbose)
			System.err.println(genotypeFile+" contains "+in.getColumnCount()+" markers.");

		int[] naDist = null;
		if (argFileNADist.isUsed()) {
			naDist = new int[in.getColumnCount()+1];
		}

		int         sampleC     = 0;
		int[][]     counts      = new int[in.getColumnCount()][3];
		SNPCall[][] calls       = new SNPCall[in.getColumnCount()][3];
		String[]    markerNames = in.getColumnNames();
		while (in.hasMoreMarkers()) {
			sampleC++;
			if (verbose)
			   System.err.println("- Processing markers from: "+in.getSample());
			int naCount = 0;
			markerLoop: for (int m=0; m<counts.length; m++) {
				SNPCall c = in.nextMarker();
				if ((c == null)           ||
                    (c == SNPCall.FAILED) ||
                    (!useDel && (c == SNPCall.NN))) {
					naCount++;
					continue;
				}
				for (int ct=0; ct<calls[m].length; ct++) {
					if (calls[m][ct] == null) {
						calls[m][ct] = c;
						counts[m][ct]++;
						continue markerLoop;
					}
					if (calls[m][ct] == c) {
						counts[m][ct]++;
						continue markerLoop;
					}
				}
				System.err.println("There are more than "+calls[m].length+
						           " genotypes "+Arrays.toString(calls[m])+
						           " for "+markerNames[m]+": "+in.getSample()+" contains "+c);
			}
			if (naDist != null)
				naDist[naCount]++;
		}
		in.close();

		// Print results...
		if (argCountSM.isUsed()) {
			out.println("# "+markerNames.length+" markers");
			out.println("# "+sampleC+" samples");
		}
		out.println("marker\tHWp\tAA\tfrq_AA\tAa\tfrq_Aa\taa\tfrq_aa");
		PrintWriter vMap = null;
		if (argFileSNPMap.isUsed()) {
		    try { vMap = new PrintWriter(new FileWriter(argFileSNPMap.getValue())); }
		    catch (IOException e) {
		       System.err.println("Cannot write value mappings into "+argFileSNPMap.getValue()+": "+e.getLocalizedMessage());
		    }
		}
		StringBuffer buf = new StringBuffer(256);
		printMarker: for (int m=0; m<calls.length; m++) {
			// Count alleles...
			Nucleotide a1 = null;
			Nucleotide a2 = null;
			int        n1 = 0;
			int        n2 = 0;
			for (int v=0; v<calls[m].length; v++) {
				if (calls[m][v] == null) {
					if (mafLimit >= 0) {
					   continue printMarker;
					}
					continue;
				}
				for (Nucleotide mv : calls[m][v].getNucleotides()) {
					if (mv == null)
						continue; // Skip missing alleles.
				    if (a1 == null || a1 == mv) {
					    a1  = mv;
					    n1 += counts[m][v];
				    } else
				    if (a2 == null || a2 == mv) {
				    	a2  = mv;
				    	n2 += counts[m][v];
				    } else {
				        System.err.println("Ignoring "+mv.getFullName()+" alleles at "+markerNames[m]+
                                           " (accepted "+a1.getFullName()+" and "+a2.getFullName()+").");
				    }
				}
			}
            double naRatio = (sampleC-(n1+n2)/2.0)/sampleC;
            if (naLimit < naRatio) {
                System.err.println("Missing value ratio of "+markerNames[m]+" is "+AsserUtil.round(naRatio, 3));
                continue printMarker;
            }
			if (n1 < n2) {
				Nucleotide tmp = a1;
				a1             = a2;
				a2             = tmp;
			}

            // Sort genotypes...
			changeLocation(calls[m], counts[m], SNPCall.toSNPCall(a1, a1), 0);
			changeLocation(calls[m], counts[m], SNPCall.toSNPCall(a1, a2), 1);
			changeLocation(calls[m], counts[m], SNPCall.toSNPCall(a2, a2), 2);
			if ((calls[m][2] == null) && (calls[m][1] != null)) {
				if (calls[m][1].isHomozygous()) {
					calls [m][2] = calls [m][1];
					calls [m][1] = SNPCall.toSNPCall(calls[m][0].getNucleotides()[0],
							                         calls[m][2].getNucleotides()[0]);
					counts[m][2] = counts[m][1];
					counts[m][1] = 0;
				} else {
					Nucleotide[] II = calls[m][0].getNucleotides();
					Nucleotide[] Ii = calls[m][1].getNucleotides();
					if (Ii[0] == II[0]) {
						calls[m][2] = SNPCall.toSNPCall(Ii[1], Ii[1]);
					} else {
						calls[m][2] = SNPCall.toSNPCall(Ii[0], Ii[0]);
					}
					counts[m][2] = 0;
				}
			}

			// Value mappings for SNPHelistin and genotype counts...
			int AA, Aa, aa;
			AA = Aa = aa = 0;
            if (vMap != null) vMap.append(markerNames[m]).append("\tA=").print(calls[m][0]);
            AA = counts[m][0];
            if (calls[m][1] != null) {
               if (calls[m][1].isHomozygous()) {
            	  if (vMap != null) vMap.append("\tB=").print(calls[m][1]);
                  aa = counts[m][1];
               } else {
            	  if (vMap != null) vMap.append("\tX=").print(calls[m][1]);
                  Aa = counts[m][1];
                  if (calls[m][2] != null) {
                	 if (vMap != null) vMap.append("\tB=").print(calls[m][2]);
                     aa = counts[m][2];
                  }
               }
            }
            if (vMap != null) vMap.println();

            if (mafLimit >= 0) {
	            double maf = (Aa+2.0*aa)/(2.0*AA+Aa+2.0*aa);
	            if (maf < mafLimit) {
	            	if (verbose) System.err.println("MAF("+markerNames[m]+") = "+maf);
	            	continue printMarker;
	            }
            }

			// Test for Hardy-Weinberg...
			double hwP = compareHardyWeinberg(AA, Aa, aa);

			buf.setLength(0);
			buf.append(markerNames[m])
			   .append('\t')
			   .append(Double.isNaN(hwP) ? SNPCall.SYMBOL_MISSING_VALUE : AsserUtil.round(hwP, 3)); 
			for (int v=0; v<calls[m].length; v++) {
				if (calls[m][v] == null) {
					buf.append('\t').append(SNPCall.SYMBOL_MISSING_VALUE).append("\t0");
					continue;
				}
			    buf.append('\t')
			       .append(SNPCall.toString(calls[m][v]))
			       .append('\t')
			       .append(counts[m][v]);
			}
			out.println(buf.toString());
		}
		if (vMap != null) {
		   vMap.close();
        }

		// Print NA distribution
		if (naDist != null) {
		   try {
		     CSVWriter naOut = new CSVWriter(new String[] { "numberOfNAs", "freq" },
			                                 argFileNADist.getValue());
		     for (int i=0; i<naDist.length; i++) {
		    	 if (naDist[i] < 1)
		    		 continue; // Skip 0 counts.
		    	 naOut.write(i);
		    	 naOut.write(naDist[i]);
		     }
		     naOut.close();
		   } catch (Exception e) {
			 System.err.println("Cannot write NA distribution to "+argFileNADist.getValue()+": "+e.toString());
			 return 1;
		   }
		}

		return 0;
	}

	static private void changeLocation(SNPCall[] calls, int[] counts, SNPCall c, int to) {
		if (calls[to] == null) return;

		int from;
		for (from=0; (calls[from] != c); from++) {
			if (from+1 == calls.length)
				return;
		}
		if (from == to)
			return;
		calls[from]  = calls[to];
		calls[to]    = c;
		int tmp      = counts[from];
		counts[from] = counts[to];
		counts[to]   = tmp;
	}
	
    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("AlleleCounter v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("------------------------------------");
        System.err.println("AlleleCounter [options] genotypefile");
        MainArgument.printArguments(args, System.err);
    }

}
