package fi.helsinki.ltdk.csbl.snpstorage;

import java.io.*;
import java.text.Collator;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import fi.helsinki.ltdk.csbl.asser.ArgumentEncoding;
import fi.helsinki.ltdk.csbl.asser.MainArgument;
import fi.helsinki.ltdk.csbl.asser.Version;
import fi.helsinki.ltdk.csbl.asser.io.KeyMapParser;
import fi.helsinki.ltdk.csbl.asser.sequence.SNPCall;

/**
 * Provides the management for a file based genotype data storage.
 */
public class SNPStorage {

    static public final String FILE_SUFFIX_DB = ".calls";

    static private final String FILE_CONFIGURATIONS = "snpstorage.properties";

    /** The oldest configuration format accepted **/
    static private final String MINIMUM_CFG_VERSION = "2.22";

    static private final String CFG_VERSION           = "version";
    static private final String CFG_MARKER_COUNT      = "marker.count";
    static private final String CFG_DIR_GENOTYPES     = "dir.genotypes";
    static private final String CFG_SUFFIX_LENGTH     = "dir.suffix.length";
    static private final String CFG_FILE_INPUT        = "file.input";
    static private final String CFG_FILE_OUTPUT       = "file.output";
    static private final String CFG_FILE_VALUE        = "file.markervalues";
    static private final String CFG_FILE_SAMPLE_NAMES = "file.samplenames";
    static private final String CFG_FILE_MARKER_NAMES = "file.markernames";

    /** Access mode for the genotype database. **/
    static public enum DIRECTION { SAVE, LOAD, REMOVE }

    /** No instantiation of this stand-alone program **/
    private SNPStorage() {}

	/**
	 * Executable method of the program
	 */
	public static void main(String[] argv) {
		int status = run(argv, false, null);
		System.exit(status);
	}

	/**
	 * Actual implementation of the executable.
	 *
	 * @param  argv     Command line arguments
	 * @param  pipeline Indicator flag for the pipeline mode
	 * @return Status code
	 */
    public static int run(String[] argv,
    		              boolean  pipeline,
    		              String   cfgFile) {
        // Load configurations...
    	InputStream cIn;
    	if (cfgFile == null) {
	        cIn = ClassLoader.getSystemResourceAsStream(FILE_CONFIGURATIONS);
	        if (cIn == null) {
	           System.err.println("Sorry, "+FILE_CONFIGURATIONS+" is not in the CLASSPATH.");
	           System.err.println("Current CLASSPATH is: "+System.getProperty("java.class.path"));
	           return 1;
	        }
    	} else {
    		try { cIn = new FileInputStream(cfgFile); }
    		catch (IOException e) {
    			System.err.println("Cannot load configurations from "+cfgFile+": "+e.toString());
    			return 1;
    		}
    	}
    	Properties cfg;
        try {
           cfg = new Properties();
           cfg.load(cIn);
           cIn.close();
        }
        catch (IOException e) {
           System.err.println("Cannot read configurations ("+FILE_CONFIGURATIONS+"):"+e.toString());
           return 1;
        }
        String cfgVersion = cfg.getProperty(CFG_VERSION);
        if (cfgVersion == null) {
           System.err.println("Warning: Configurations have no definition for the program version.");
        }
        if (Version.compare(MINIMUM_CFG_VERSION, cfgVersion) < 0) {
        	System.err.println("Configurations of version "+cfgVersion+" are not compatible with this installation. "+
        			           "The oldest supported configuration format is "+MINIMUM_CFG_VERSION+".");
        	return 1;
        }

	    // Read command line arguments...
		Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argEsc        = MainArgument.add("escapes", "Print escape sequences for separators and symbols", args);
        MainArgument argShowConfig = MainArgument.add("cfg",     "Print configurations", args);
        MainArgument argListM      = MainArgument.add("lstM",    "List all markers that are available", args);
        MainArgument argListS      = MainArgument.add("lstS",    "List all samples that are available", args);
        MainArgument argVerbose    = MainArgument.add("v",       "Verbose output mode", args);
        MainArgument argIgnoreS    = MainArgument.add("igS",     "Ignore invalid sample names and do not process them", args);
	    MainArgument argAddMarkers = MainArgument.add("M",       "Insert new markers into the marker list", args);
	    MainArgument argAddSamples = MainArgument.add("S",       "Insert new samples into the sample list", args);
        MainArgument argOutSep     = MainArgument.add("outSep",  "c",         "Add custom separator between the nucleotide calls", args);
        MainArgument argSep        = MainArgument.add("sep",     "pattern",   "Input file separator", args);
        MainArgument argISPrefix   = MainArgument.add("addP",    "prefix",    "Prefix input sample names before processing", args);
	    MainArgument argSuffixL    = MainArgument.add("pLength", "n",         "Length of the sample suffix used as a repository folder", cfg.getProperty(CFG_SUFFIX_LENGTH, "2"), args);
	    MainArgument argColumnSkip = MainArgument.add("skip",    "n",         "Export this many input columns as such", "0", args);
        MainArgument argFileInput  = MainArgument.add("i",       "file",      "Name of the input file", cfg.getProperty(CFG_FILE_INPUT), args); // NOPMD
        MainArgument argFileOutput = MainArgument.add("o",       "file",      "Name of the output file", cfg.getProperty(CFG_FILE_OUTPUT), args);
        MainArgument argFileMarker = MainArgument.add("marker",  "file",      "Name of the marker name file (if not input columns)", args);
        MainArgument argFileValues = MainArgument.add("value",   "file",      "Name of the value mapping file", cfg.getProperty(CFG_FILE_VALUE), args);
        MainArgument argDirection  = MainArgument.add("d",       "direction", "Input operation: "+Arrays.toString(DIRECTION.values()), DIRECTION.LOAD.name(), args);
        MainArgument argSymbolNA   = MainArgument.add("na",      "text",      "Missing value symbol", SNPCall.SYMBOL_MISSING_VALUE, args);
        MainArgument argSymbolERR  = MainArgument.add("err",     "text",      "Symbol for failed measurements", SNPCall.SYMBOL_EXPERIEMENT_FAILURE, args);
        try { MainArgument.parseInput(argv, args, 0, 0); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
	        System.err.println();
	        printSynopsis(args);
            return 1;
        }

        if (argShowConfig.isUsed()) {
           printConfigurations(cfg, System.out);
           return 0;
        }

        if (argEsc.isUsed()) {
        	System.out.println("Escape sequences that may be used within the arguments:");
            for (ArgumentEncoding rep : ArgumentEncoding.values()) {
                System.out.println(rep.symbol+" = "+rep.description);
            }
            return 0;
        }

        int prefixL;
        try { prefixL = argSuffixL.getInt(); }
        catch (Exception e) {
        	System.err.println("Invalid suffix length for sample folders: "+argSuffixL.getValue());
        	return 1;
        }

        if (argListM.isUsed()) {
           try { printMarkerList(cfg.getProperty(CFG_DIR_GENOTYPES), System.out); }
           catch (IOException e) {
             e.printStackTrace();
             return 1;
           }
           return 0;
        }

        if (argListS.isUsed()) {
           printSampleList(cfg.getProperty(CFG_DIR_GENOTYPES), System.out);
           return 0;
        }

        if (argFileInput.getValue() == null) {
        	System.err.println("Input file has not been specified.\n");
        	printSynopsis(args);
        	return 1;
        }

        Pattern iSep = null;
        if (argSep.isUsed()) {
           try { iSep = Pattern.compile(ArgumentEncoding.decode(argSep.getValue())); }
           catch (PatternSyntaxException e) {
              System.err.println("Input file pattern does not match the specification of "+
                                 Pattern.class.getCanonicalName()+": "+argSep.getValue());
              return 1;
           }
        }

        boolean isLoading;
        try { isLoading = DIRECTION.valueOf(argDirection.getValue()) == DIRECTION.LOAD; }
        catch (Exception e) {
        	System.err.println("Invalid input operation: "+argDirection.getValue());
        	return 1;
        }
        if (isLoading && (argFileOutput.getValue() == null)) {
        	System.err.println("Output file has not been specified.");
        	return 1;
        }
        
        boolean isRemoving;
        try { isRemoving = !isLoading && DIRECTION.valueOf(argDirection.getValue()) == DIRECTION.REMOVE; }
        catch (Exception e) {
        	System.err.println("Invalid input operation: "+argDirection.getValue());
        	return 1;
        }

        String outSep = argOutSep.getValue();

        // Actual processing...
        // ---------------------------
        if (argVerbose.isUsed()) {
           if (isLoading)
              System.out.println("Loading the queried data based on "+argFileInput.getValue()+":");
           else if (isRemoving)
        	  System.out.println("Removing genotype calls of "+argFileInput.getValue()+":");
           else
              System.out.println("Saving the content of "+argFileInput.getValue()+":");
        }

	    // Load sample names...
  	    if (argVerbose.isUsed()) System.out.println("- Loading sample name mappings...");
        Map<String, String> samplenames;
        KeyMapParser        kmp = new KeyMapParser();
        kmp.setIncludeKey(true);
        try { samplenames = kmp.read(cfg.getProperty(CFG_FILE_SAMPLE_NAMES)); }
        catch (IOException e) {
           System.err.println("Cannot read sample name mappings from '"+cfg.getProperty(CFG_FILE_SAMPLE_NAMES)+"':");
           System.err.println(e.toString());
           return 1;
        }
        if (argVerbose.isUsed()) System.out.println("  "+samplenames.size()+" mappings loaded.");

	    // Load marker names...
	    if (argVerbose.isUsed()) System.out.println("- Loading marker name mappings...");
	    int mCount;
	    try { mCount = Integer.parseInt(cfg.getProperty(CFG_MARKER_COUNT, "580000")); }
	    catch (RuntimeException e) {
	    	System.err.println("Invalid value of "+CFG_MARKER_COUNT+": "+cfg.getProperty(CFG_MARKER_COUNT));
	    	return 1;
	    }
        Map<String, String> markernames;
        try { markernames = kmp.read(cfg.getProperty(CFG_FILE_MARKER_NAMES), mCount); }
        catch (IOException e) {
           System.err.println("Cannot read marker name mappings from '"+cfg.getProperty(CFG_FILE_MARKER_NAMES)+"'.");
           e.printStackTrace();
           return 1;
        }
        if (argVerbose.isUsed()) System.out.println("  "+markernames.size()+" mappings loaded.");

        // Load marker value mappings...
        Map<String, MarkerValue[]> markervalues = null;
        if (argFileValues.getValue() != null) {
	  	    if (argVerbose.isUsed()) System.out.println("- Loading marker value mappings...");
	  	    markervalues = new HashMap<String, MarkerValue[]>(markernames.size());
	  	    try {
	  	      BufferedReader         in   = new BufferedReader(new FileReader(argFileValues.getValue()));
	  	      ArrayList<MarkerValue> list = new ArrayList<MarkerValue>();
	  	      String                 iLine;
	  	      while ((iLine=in.readLine()) != null) {
	  	         StringTokenizer tok    = new StringTokenizer(iLine);
	  	         String          marker = tok.nextToken();
	  	         if (markernames.get(marker) == null) {
	  	            System.err.println("  Warning: "+marker+" is not a valid marker name.");
	  	         }
	  	         while (tok.hasMoreTokens()) {
	  	            list.add(new MarkerValue(marker, tok.nextToken()));
	  	         }
	  	         markervalues.put(marker, list.toArray(new MarkerValue[list.size()]));
	  	         list.clear();
	  	      }
	  	      in.close();
	  	    } catch (Exception e) {
	  	      System.err.println("Cannot read marker value mappings from '"+argFileValues.getValue()+"'.");
	  	      e.printStackTrace();
	  	      return 1;
	  	    }
        }

        // Process input file...
        int lineC     = 0;
        int sampleCnt = 0;
        try {
            BufferedReader       input      = new BufferedReader(new FileReader(argFileInput.getValue()));
            List<String>         markers    = (isLoading) ? null: new ArrayList<String>(markernames.size());
            Map<String, Integer> markerMap  = (isLoading || isRemoving) ? new HashMap<String, Integer>() : null;
            List<Integer>        markerInd  = (isLoading) ? new ArrayList<Integer>(markernames.size()) : null;
            List<String>         allMarkers = (isLoading) ? null : new ArrayList<String>(markernames.size());
            List<SNPCall>        allCalls   = (isLoading) ? null : new ArrayList<SNPCall>(markernames.size());
            PrintWriter          queryOut   = (isLoading) ? new PrintWriter(new FileWriter(argFileOutput.getValue())) : null;
	        MarkerValue[][]      valueList  = (markervalues == null) ? null : new MarkerValue[markernames.size()][];
	        PrintWriter          markerOut  = null;
	        PrintWriter          sampleOut  = null;
	        final String         NA         = ArgumentEncoding.decode(argSymbolNA.getValue());
	        final String         ERR        = ArgumentEncoding.decode(argSymbolERR.getValue());
	        final int            colSkip    = argColumnSkip.getInt();
	        String               isPrefix   = argISPrefix.getValue();
            String               iLine;
            String               sample;
            String               sampleKey;
            SNPCall[]            calls;

            // Read marker names...
            Scanner tok = null;
            if (argFileMarker.isUsed()) { // Separate file for column names
               tok = new Scanner(new File(argFileMarker.getValue()));
               if (pipeline && tok.hasNext())
            	   tok.nextLine(); // Skip the header line from Pipeline inputs
               if (isLoading) queryOut.print("Sample");
            } else {                      // Column names are given on the first line of the matrix
               while ((iLine=input.readLine()) != null) {
            	 lineC++;
            	 iLine = iLine.trim();
            	 if ((iLine.length() < 1) || (iLine.charAt(0) == '#'))
    				continue;
            	 tok = new Scanner(iLine);
            	 if (iSep != null) tok.useDelimiter(iSep);
            	 break; // Skip after header row 
               }
               if (tok == null) {
                  System.err.println("Input contains no marker information.");
                  return 1;
               }
               String m = tok.next(); // The sample column
               if (isLoading) queryOut.print(m);
            }
        	for (int c=0; c<colSkip; c++) {
        	    String t = tok.next();
        	    if (isLoading) { queryOut.print('\t'); queryOut.print(t); }
        	}
        	int col = 0;
        	while (tok.hasNext()) {
        	  String m = tok.next();
              if (m.charAt(0) == '"') { // Remove quotations of input markers
                  int qPos = m.length()-1;
                  if (m.charAt(qPos) == '"')
                      m = m.substring(1, qPos);
              }
        	  String realMarkerName = markernames.get(m);
        	  if (realMarkerName == null) {
        		  if (!argAddMarkers.isUsed()) {
        			  System.err.println("Unknown marker name: "+m);
        	          return 1;
        		  }
        		  markernames.put(m, m);
        		  System.out.println("- Adding new marker: "+m);
        		  if (markerOut == null) {
        			  markerOut = new PrintWriter(new FileWriter(cfg.getProperty(CFG_FILE_MARKER_NAMES), true));
        			  markerOut.println("# Markers added by "+
        					  			System.getProperty("user.name", "[anonymous]")+" - "+
        					  			new Date());
        		  }
        		  markerOut.println(m);
        		  realMarkerName = m;
        	  }
        	  if (isLoading) {
        	     if (markerMap.put(realMarkerName, Integer.valueOf(col)) != null) {
        	        System.err.println("Multiple declarations of "+m+".");
        	        return 1;
        	     }
        	     queryOut.print('\t'); queryOut.print(m);
        	  } else {
        	     markers.add(realMarkerName);
        	  }
     	      if (valueList != null) {
                 if (col >= valueList.length) {
                    MarkerValue[][] smallVL = valueList;
                    valueList = new MarkerValue[smallVL.length+smallVL.length/2+1][];
                    System.arraycopy(smallVL, 0, valueList, 0, smallVL.length);
                 }
    	    	 valueList[col] = markervalues.get(realMarkerName);
              }
        	  col++;
        	}
            tok.close();
            if (!isLoading)
            	markervalues = null;
            markernames = null;
	        if (markerOut != null)
	            markerOut.close();
            if (isRemoving)
               calls = null;
            else
               calls = new SNPCall[isLoading ? markerMap.size() : markers.size()];

            if (pipeline) {
            	input.readLine(); // Skip the header line
            	lineC++;
            }

            // Read samples...
            while ((iLine=input.readLine()) != null) {
            	lineC++;
            	iLine = iLine.trim();
            	if ((iLine.length() < 1) || (iLine.charAt(0) == '#'))
    				continue;
            	tok    = new Scanner(iLine); if (iSep != null) tok.useDelimiter(iSep);
            	sample = tok.next().trim();
                if (sample.charAt(0) == '"') { // Remove quotations of input samples
                    int qPos = sample.length()-1;
                    if (sample.charAt(qPos) == '"')
                        sample = sample.substring(1, qPos);
                }
            	if (isPrefix != null) {
            		sample = isPrefix+sample;
            	}
            	sampleKey = samplenames.get(sample);
            	if (sampleKey == null) {
            	   if (!argAddSamples.isUsed()) {
                	  printError("Invalid sample name: "+sample, argFileInput.getValue(), lineC);
                	  if (argIgnoreS.isUsed()) {
                	      continue; // Just skip this sample
                	  }
        			  return 1; // Terminate the whole process
        		   }
            	   samplenames.put(sample, sample);
             	   System.out.println("- Adding new sample: "+sample);
            	   if (sampleOut == null) {
            	       sampleOut = new PrintWriter(new FileWriter(cfg.getProperty(CFG_FILE_SAMPLE_NAMES), true));
            		   sampleOut.println("# Samples added by "+
            					  		 System.getProperty("user.name", "[anonymous]")+" - "+
            					  	     new Date());
            	   }
            	   sampleOut.println(sample);
                   sampleKey = sample;
            	}
            	if (isLoading) {
	            	queryOut.println();
	            	queryOut.print(sample);
            	}
            	for (int c=0; c<colSkip; c++) {
            	    String t = tok.next();
            	    if (isLoading) { queryOut.print('\t'); queryOut.print(t); }
            	}
            	if (isLoading) {
            		Arrays.fill(calls, null); // Reset values.
            	} else
            	if (!isRemoving) {
	            	for (int i=0; i<calls.length; i++) {
	            		if (!tok.hasNext()) {
	            			if (i+1 == calls.length) {
	            				if ( NA.length() == 0) { calls[i] = null;           continue; }
	            				if (ERR.length() == 0) { calls[i] = SNPCall.FAILED; continue; }
	            			}
	            			printError("Too few ("+i+'/'+calls.length+") genotypes in "+sample+".", argFileInput.getValue(), lineC);
	            			return 1;
	            		}
	            		String marker = tok.next();
	            		try {
	            		    calls[i] = null;
	            		    if (valueList != null) {
	            		       MarkerValue[] mvs = valueList[i];
	            		       if (mvs != null) for (MarkerValue mv : mvs) {
	            		           if (mv.name.equals(marker)) {
	            		              calls[i] = mv.call;
	            		              // System.out.println("Converting "+marker+" to "+calls[i]);
	            		              break;
	            		           }
	            		       }
	            		    }
	            		    if (calls[i] == null) {
	            		       calls[i] = SNPCall.parse(marker, NA, ERR);
	            		    }
	            		}
	            		catch (IllegalArgumentException e) {
	            			printError(e.getMessage()+
	            					   "\n  Value='"+marker+"' Sample="+sample+" ("+sampleKey+") Column="+(i+1)
	            					   , argFileInput.getValue(), lineC);
	            			return 1;
	            		}
	            	}
	            	if (tok.hasNext()) {
	            		printError("Too many genotypes in "+sample+".", argFileInput.getValue(), lineC);
	        			return 1;
	            	}
            	}

            	File genoF = getSampleFile(cfg.getProperty(CFG_DIR_GENOTYPES), sampleKey, prefixL);
            	if (genoF.exists()) {
            	    if (argVerbose.isUsed()) {
            	       System.out.println("- Reading genotypes from "+genoF.getName()+"..."); // NOPMD
            	    }
            	    Reader genoIn = new BufferedReader(new FileReader(genoF));
                    int    sLen   = -1;
            	    if (!genoIn.ready()) {
            	    	System.err.println("- No marker headers for "+genoF.getCanonicalPath()+'.');
            	    	return 1;
            	    }
                    StringBuffer inTBuf = new StringBuffer(256);
                    markerLoop: for (;;) {
                        int ic = genoIn.read();
                        switch (ic) {
                          case '\t':
                          case '\n': String inT = inTBuf.toString();
                                     inTBuf.setLength(0);
                                     if (sLen >= 0) {
                                         if (isLoading) {
                                             markerInd.add(markerMap.get(inT));
                                         } else {
                                             allMarkers.add(inT);
                                         }
                                         sLen++;                                         
                                     } else {
                                         if (!sampleKey.equals(inT)) {
                                             System.err.println("- Unexpected sample name in "+genoF.getCanonicalPath()+'.');
                                             return 1;
                                         }
                                         sLen = 0;
                                     }
                                     if (ic == '\n') break markerLoop;
                                     break;
                          default:   inTBuf.append((char)ic);
                        }
                    }
            	    if (!genoIn.ready()) {
            	    	System.err.println("No genotypes in "+genoF.getAbsolutePath());
            	    	return 1;
            	    }
            	    Iterator<Integer> it = isLoading ? markerInd.listIterator() : null;   
            	    for (int mi=0; mi<sLen; mi++) {
            	    	try {
            	    	   SNPCall call = SNPCall.parse((byte)genoIn.read());
            	    	   if (isLoading) {
            	    	      Integer loc = it.next();
            	    	      if (loc != null) {
            	    	         calls[loc] = call;
            	    	      }
            	    	   } else {
            	    	      allCalls.add(call);
            	    	   }
            	    	}
            	    	catch (IllegalStateException e) {
            	    		System.err.println("- Invalid content in "+genoF+": "+e.getMessage());
            	    		return 1;
            	    	}
            	    }
            	    genoIn.close();
            	}

            	boolean isDBModified = false;
            	if (isLoading) {
	            	// Write output line...
	            	if (argVerbose.isUsed()) {
	                   System.out.println("- Listing genotypes for "+sample+"...");
	            	}
	            	printCall: for (int i=0; i<calls.length; i++) {
	            		queryOut.print('\t');
	            		if ((markervalues == null) || (outSep != null)) {
	            			queryOut.print(SNPCall.toString(calls[i], outSep, NA, ERR));
	            		} else {
	            			MarkerValue[] mvs = valueList[i];
	            			if (mvs != null) for (int c=0; c<mvs.length; c++) {
	            				if (mvs[c].call == calls[i]) {
	            			       queryOut.print(mvs[c].name);
	            			       continue printCall;
	            				}
	            			}
	            			String expF;
	            			if (calls[i] == null) {
	            				expF = NA;
	            		    } else if (calls[i] == SNPCall.FAILED) {
	            				expF = ERR;
	            			} else {
	            			    expF = SNPCall.toString(calls[i], outSep, NA, ERR);
	            			    System.err.println("  Warning: No export value for "+calls[i]+". Using "+expF+" instead.");
	            			}
	            			queryOut.print(expF);
	            		}
	            	}

            	   // Prepare for the next sample file...
            	   markerInd.clear();
            	} else {
            	    // Add new values to the existing ones...
	            	int c = 0;
	            	if (isRemoving) {
		            	for (String marker : markers) {
		            		int i = Collections.binarySearch(allMarkers, marker);
		            		if (i >= 0) {
		           			    allCalls.remove(i);
		           			    allMarkers.remove(i);
		           			    isDBModified = true;
		            		}
		            		c++;
		            	}
	            	} else {
		            	for (String marker : markers) {
		            		SNPCall newCall = calls[c];
		            		if (newCall != null) {
			            		int i = Collections.binarySearch(allMarkers, marker);
			            		if (i < 0) {
			            			i = -1*i-1;
			            			allMarkers.add(i, marker);
			            			allCalls  .add(i, newCall);
			            			isDBModified = true;
			            		} else {
			           			    if ((newCall != SNPCall.FAILED) && (newCall != allCalls.get(i))) {
			           			    	allCalls.set(i, newCall);
			           			    	isDBModified = true;
			           			    }
			            		}
		            		}
		            		c++;
		            	}
	            	}

	            	// Save new marker values...
	            	if (isDBModified) {
	            	    if (isRemoving && allMarkers.isEmpty()) {
	            	        if (genoF.exists()) {
	            	           if (argVerbose.isUsed()) {
			                      System.out.println("- Removing "+genoF.getName()+"...");
			            	   }
	            	           if (!genoF.delete()) {
	            	              System.err.println("Could not delete "+genoF.getCanonicalPath());
	            	              return 1;
	            	           }
	            	        }
	            	    } else {
			            	if (argVerbose.isUsed()) {
			                   System.out.println("- Writing genotypes to "+genoF.getName()+"...");
			            	}
			            	if (!genoF.exists()) {
			            	   File sDir = genoF.getParentFile();
			            	   if (!sDir.exists()) {
			            	      if (argVerbose.isUsed()) {
			                          System.out.println("  Creating new folder: "+sDir.getCanonicalPath()+"...");
			            	      }
			            	      sDir.mkdirs();
			            	   }
			            	}
			            	Writer genoOut = new FileWriter(genoF);
			            	genoOut.write(sampleKey);
			            	for (String marker : allMarkers) {
			            		genoOut.write('\t');
			            		genoOut.write(marker);
			            	}
			            	genoOut.write('\n');
			            	for (SNPCall cc : allCalls) {
			            		genoOut.write(SNPCall.getCode(cc));
			            	}
			            	genoOut.close();
		            	}
	            	} else {
	            		if (argVerbose.isUsed()) {
			                System.out.println("- No modifications to "+genoF.getName());
			            }
	            	}

					// Prepare for the next sample file...
            	    allCalls.clear();
            	    allMarkers.clear();
            	}
            	sampleCnt++;
            }
            input.close();
            if (sampleOut != null)
               sampleOut.close();
            if (queryOut != null) {
                queryOut.close();
                if (argVerbose.isUsed()) System.out.println("- Output file ("+argFileOutput.getValue()+") is ready.");
            }
        }
        catch (IOException e) {
        	printError("File processing failed.", argFileInput.getValue(), lineC);
        	e.printStackTrace();
        	return 1;
        }
        if (argVerbose.isUsed()) {
        	System.out.println("- Total of "+sampleCnt+" samples were processed.");
        	System.out.println("Done!");
        }
        return 0;
	}

	static private void printError(String message, String filename, int line) {
		System.err.println("- Error in "+filename+" at line "+line);
		System.err.println("  "+message);
	}

    static private void printConfigurations(Properties cfg, PrintStream out) {
      try { cfg.store(out, "Current configurations for SNPHelistin v."+Version.getVersion()); }
      catch (IOException err) { err.printStackTrace(); }
    }

    static private void printMarkerList(String dir, PrintStream out) throws IOException {
      List<File>  files   = getSampleFiles(dir);
      Set<String> markers = new HashSet<String>();
      String[]    array;

      for (File f : files) {
          BufferedReader in   = new BufferedReader(new FileReader(f));
          Scanner        line = new Scanner(in.readLine());
          in.close();

          if (line.hasNext()) // Skip sample column.
        	  line.next(); 
          while (line.hasNext()) {
            markers.add(line.next());
          }
      }
      array = markers.toArray(new String[markers.size()]);
      Arrays.sort(array, Collator.getInstance());
      out.println("# Marker list ("+dir+"):");
      for (String m : array) {
          out.println(m);
      }
    }

    static private void printSampleList(String dir, PrintStream out) {
      List<File>      files = getSampleFiles(dir);
      TreeSet<String> names = new TreeSet<String>(); 
      int             sLen  = FILE_SUFFIX_DB.length();

      // Read and sort file names
      for (File f : files) {
          String name = f.getName();
          names.add(name.substring(0, name.length()-sLen));
      }

      out.println("# Sample list ("+dir+"):");
      for (String name : names) {
    	  out.println(name);
      }
    }

    static private List<File> getSampleFiles(String folder) {
      File       db      = new File(folder);
      List<File> samples = new LinkedList<File>();

      if (db.exists())
	      for (File f : db.listFiles()) {
	          if (f.isDirectory()) {
	             for (File ff : f.listFiles()) {
	                 if (ff.getName().endsWith(FILE_SUFFIX_DB))
	                    samples.add(ff);
	             }
	          } else {
	             if (f.getName().endsWith(FILE_SUFFIX_DB))
	                samples.add(f);
	          }
	      }
      else
    	  System.err.println("Repository does not exist!");
      return samples;
    }

    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("SNPHelistin v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("----------------------------------");
        System.err.println("snpstorage [options]");
        MainArgument.printArguments(args, System.err);
    }

    static public File getSampleFile(String repository,
    		                         String sample,
    		                         int    suffixLength) {
    	StringBuffer name = new StringBuffer(100);
    	int sLen = sample.length(); 
    	if (sLen > suffixLength) {
    		name.append(sample.substring(sLen-suffixLength));
    		name.append(File.separatorChar);
    	}
    	name.append(sample).append(FILE_SUFFIX_DB);
    	return new File(repository, name.toString());
    }

}

/** Simple structure of the input label and the actual SNPCall **/
class MarkerValue {

  SNPCall call;
  String  name;

  MarkerValue(String marker, String entry) {
    int pos = entry.indexOf('=');
	if (pos < 1)
	    throw new RuntimeException("Invalid marker value mapping for "+marker+": "+entry);
	name = entry.substring(0, pos);
	call = SNPCall.parse(entry.substring(pos+1)); 
  }

} 
