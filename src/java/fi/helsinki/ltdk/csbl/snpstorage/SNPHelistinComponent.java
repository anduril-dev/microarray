package fi.helsinki.ltdk.csbl.snpstorage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

/**
 * This component retrieves data from
 * {@link fi.helsinki.ltdk.csbl.snpstorage.SNPStorage} database.
 *
 * @author Marko Laakso
 */
public class SNPHelistinComponent extends SkeletonComponent {

	static public final String INPUT_SAMPLE_NAMES   = "sampleNames";
	static public final String INPUT_MARKER_NAMES   = "markerNames";
	static public final String INPUT_CONFIGURATIONS = "config";
	static public final String OUTPUT_GENOTYPES     = "genotypes";
    static public final String PARAM_IGNORE_UNKNOWN = "skipUnknown";
    static public final String PARAM_ERR_SYMBOL     = "errorSymbol";

	protected ErrorCode runImpl(CommandFile cf) throws IOException {
		File         sampleNames = cf.getInput(INPUT_SAMPLE_NAMES);
		File         markerNames = cf.getInput(INPUT_MARKER_NAMES);
		File         cfgFile     = cf.getInput(INPUT_CONFIGURATIONS);
		File         outputFile  = cf.getOutput(OUTPUT_GENOTYPES);
        boolean      igS         = cf.getBooleanParameter(PARAM_IGNORE_UNKNOWN);
        List<String> args        = new ArrayList<String>();

        args.add("-i");      args.add(sampleNames.getCanonicalPath());
        args.add("-marker"); args.add(markerNames.getCanonicalPath());
        args.add("-o");      args.add(outputFile.getCanonicalPath());
        args.add("-err");    args.add(cf.getParameter(PARAM_ERR_SYMBOL));
        if (igS)             args.add("-igS");

		int status = SNPStorage.run(args.toArray(new String[args.size()]),
				                    true,
                                    cfgFile.getCanonicalPath());
		return (status==0) ? ErrorCode.OK : ErrorCode.ERROR;
	}

	/**
	 * Executes this component from the command line.
	 *
	 * @param argv Pipeline arguments
	 */
	static public void main(String[] argv) {
		new SNPHelistinComponent().run(argv);
	}

}
